<?php
/*
Plugin Name: Brand Map Addon
Plugin URI: https://mobile-marketing.agency
Description: Brand Landing Pages Plugin
Author: MM
Version:1.0.3
Author URI: https://mobile-marketing.agency
*/

require_once(ABSPATH . "wp-includes/pluggable.php");
require_once(ABSPATH . "/wp-load.php");
include(dirname(__FILE__) . '/helper.php');

global $wpdb;
$website = json_decode(get_option('website_json'));


register_deactivation_hook(__FILE__, 'brand_map_addon_deactivate');

function brand_map_addon_deactivate() {
    wp_clear_scheduled_hook('mohawk_landing_pages_integration_cronjob');
    wp_clear_scheduled_hook('shaw_landing_pages_integration_cronjob');
    wp_clear_scheduled_hook('mohawk_shaw_template_autoimport_cronjob');
}

/** Theme global js **/
function brand_theme_global()
{
    // wp_enqueue_script('script-js', plugins_url('js/script.min.js', __FILE__));

}
add_action('wp_head', 'brand_theme_global');

function brandadmin_style()
{
     wp_enqueue_style('brand-admin-styles', plugins_url('css/admin.min.css', __FILE__));

}
add_action('admin_enqueue_scripts', 'brandadmin_style');


function brand_theme_add_headers()
{

    wp_enqueue_style('brand-frontend-styles', plugins_url('css/styles.min.css', __FILE__));
}

add_action('init', 'brand_theme_add_headers');



function add_brand_menu()
{
    
    add_menu_page(
        'Brand Map Settings', // page title 
        'Brand Map', // menu title
        'manage_options', // capability
        'brand-map-settings',  // menu-slug
        'brand_menu_page',   // function that will render its output
        plugins_url('/brand-image.png', __FILE__)   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );

    add_submenu_page(
        'brand-map-settings',
        'Featured Products Settings',   // Page title
        'Featured Product Settings',      // Menu title
        'manage_options',   // Capability
        'featured-products-settings',   // Menu slug
        'featured_products_settings', // Callback function
         20 // Position in menu
    );
}
add_action('admin_menu', 'add_brand_menu');


function brand_menu_page(){   

    $website = json_decode(get_option('website_json'));

    $content = '<div class="page-holder"> 
                    <div class="page-title-holder">
                        <h2>Brand Map Settings</h2>
                    </div>
                    <nav class="nav-tab-wrapper" style="margin-bottom: 20px;">
                        <a href="?page=brand-map-settings" class="nav-tab nav-tab-active">Brand Map Setting</a>
                        <a href="?page=featured-products-settings" class="nav-tab">Featured Products Setting</a>
                    </nav>
                    <div class="brand-main-outer">
                        <div class="tab-content-title">
                            <h4><strong>Selected Brand From CDE</strong></h4> 
                        </div>';
                        
        $content .= '<div class="brand-main-inner-row">';
        
        if ($website->options->mohawkBrandPage == 'true') {
            $content .= '<div class="brand-list-holder"><h4><strong>Mohawk Brands</strong></h4>';
            $content .= '<ul>';
            $mohawkbrands = json_decode($website->options->mohawkSubBrands);
            foreach ($mohawkbrands as $mohawk_brand) {
                $content .= '<li>' . $mohawk_brand . '</li>';
            }
            $content .= '</ul></div>';
        }

        if ($website->options->shawBrandPage == 'true') {
            $content .= '<div class="brand-list-holder"><h4><strong>Shaw Brands</strong></h4>';
            $content .= '<ul>';
                $shawbrands = json_decode($website->options->shawSubBrands);
                foreach ($shawbrands as $shaw_brand) {
                    $content .= '<li>' . $shaw_brand . '</li>';
                }
            $content .= '</ul></div>';
        }

        if ($website->options->karastanBrandPage == 'true') {
            $content .= '<div class="brand-list-holder"><h4><strong>Karastan Brands</strong></h4>';
            $content .= '<ul>';
                $karastanSubBrands = json_decode($website->options->karastanSubBrands);
                foreach ($karastanSubBrands as $kara_brand) {
                    $content .= '<li>' . $kara_brand . '</li>';
                }
            $content .= '</ul></div>';
        }

        if ($website->options->godfreyBrandPage == 'true') {
            $content .= '<div class="brand-list-holder"><h4><strong>Godfrey Brands</strong></h4>';
            $content .= '<ul>';
                $godfreySubBrands = json_decode($website->options->godfreySubBrands);
                foreach ($godfreySubBrands as $god_brand) {
                    $content .= '<li>' . $god_brand . '</li>';
                }
            $content .= '</ul></div>';
        }

        if ($website->options->pergoBrandPage == 'true') {
            $content .= '<div class="brand-list-holder"><h4><strong>Pergo Brands</strong></h4>';
            $content .= '<ul>';
                $pergoSubBrands = json_decode($website->options->pergoSubBrands);
                foreach ($pergoSubBrands as $pergo_brand) {
                    $content .= '<li>' . $pergo_brand . '</li>';
                }
            $content .= '</ul></div>';
        }

        //$content .= '</div>';
    $content .= '</div></div></div>';

    $content .= '<div class="divider_container"></div>';

    $content .= '<div class="brand-main-outer">';


    $content .= '<div class="tab-content environment_section">
                    <table class="environment-main-table">
                        <tbody>
                            <tr>
                                <td width="50%">
                                    <h4>Environment Settings</h4>
                                    <form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                                        <table class="form-table environment-table">
                                            <tbody>
                                            <tr>
                                                <td width="150px">
                                                    <label for="siteid">Select Environment <strong>*</strong></label>
                                                </td><td class="form-group">
                                                    <select name="retailer_env">
                                                        <option value="react" ' . (get_option('retailer_env') == 'react' ? 'selected=selected' : null) . '>React</option>
                                                        <option value="grand-child" ' . (get_option('retailer_env') == 'grand-child' ? 'selected=selected' : null) . '>Grand Child</option>                                        
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="150px">
                                                    <label for="siteid">Select Flooring Page <strong>*</strong></label>
                                                </td><td class="form-group">
                                                    <select name="flooring_page">
                                                    <option value="">Select page</option>';
                                                    $pages = get_pages(); 
                                                    foreach ( $pages as $page ) {
                                                        $content .=  '<option value="' . wp_make_link_relative(get_page_link( $page->ID )) . '" ' . (get_option('flooring_page') == wp_make_link_relative(get_page_link( $page->ID )) ? 'selected=selected' : null) . '>'.$page->post_title.'</option>';
                                                    }
                                         $content .= '</select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="form-group adp20">
                                                    <button id="syncdata" type="submit" class="button button-primary button-hero">SAVE</button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </td>
                                <td width="50%">
                                    <h4>Tempalte Override Settings</h4>
                                    <form name="pluginname" action="/wp-admin/admin.php?page=brand-map-settings" method="POST">
                                        <table class="form-table environment-table">
                                            <tbody>
                                                <tr>
                                                    <td width="180px">
                                                        <label for="sel1">Please Select template name you want override:</label>
                                                    </td>
                                                    <td class="form-group">
                                                        <select id="brand_templates" name="brand_templates">
                                                            <option value="mohawkBrandPage">Mohawk Main Page</option>
                                                            <option value="mohawk_smartstrand">Mohawk Smartstrand</option>
                                                            <option value="mohawk_everstrand">Mohawk everstrand</option>
                                                            <option value="mohawk_petpremier">Mohawk Petpremier</option>
                                                            <option value="mohawk_revwood">Mohawk Revwood</option>
                                                            <option value="mohawk_solidtech">Mohawk Solidtech</option>
                                                            <option value="mohawk_solidtech_r">Mohawk_Solidtech R</option>
                                                            <option value="mohawk_puretech">Mohawk Puretech</option>
                                                            <option value="mohawk_ultimateflex">Mohawk Ultimateflex</option>
                                                            <option value="mohawk_tecwood">Mohawk TecWood</option>
                                                            <option value="shawBrandPage">Shaw Main Page</option>
                                                            <option value="anso_colorwall">Anso Colorwall</option>
                                                            <option value="pet_perfect">Pet Perfect</option>
                                                            <option value="anderson_tuftex">Anderson Tuftex</option>
                                                            <option value="coretec">Coretec</option>
                                                            <option value="floorte">Floorte</option>
                                                            <option value="philadelphia_commercial">Philadelphia Commercial</option>
                                                            <option value="shaw_grass">Shaw Grass</option>
                                                            <option value="karastanBrandPage">Karastan Main Page</option>
                                                            <option value="karastan_smartstrand">Karastan Smartstrand</option>
                                                            <option value="karastan_luxecraft">Karastan Luxecraft</option>
                                                            <option value="karastan_belleluxe">Karastan Belleluxe</option>
                                                            <option value="godfreyBrandPage">Godfrey Main Page</option>
                                                            <option value="godfrey_hirst_Smartstrand">Godfrey Hirst Smartstrand</option>
                                                            <option value="godfrey_hirst_wool">Godfrey Hirst Wool</option>
                                                            <option value="godfrey_hirst_everLux">Godfrey Hirst EverLux</option>
                                                            <option value="pergoBrandPage">Pergo Main Page</option>
                                                            <option value="pergo_elements">Pergo Elements</option>
                                                            <option value="pergo_extreme">Pergo Extreme</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td class="form-group adp20">
                                                        <button id="syncdata" type="submit" class="button button-primary button-hero">SAVE</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>';

    $content .= '</div>';

    $content .= '';

    echo $content;
}
 

function save_plugin_settings()
{

    if (isset($_POST['retailer_env'])) {

        (get_option('retailer_env') !== null) ? update_option('retailer_env', $_POST['retailer_env']) : add_option('retailer_env', $_POST['retailer_env']);
    }
    if (isset($_POST['flooring_page'])) {

        $url  = wp_make_link_relative($_POST['flooring_page']);

        (get_option('flooring_page') !== null) ? update_option('flooring_page', $url) : add_option('flooring_page', $url);
    }
    if (isset($_POST['brand_templates'])) {      

           $upload_dir_xml = plugin_dir_path( __FILE__ ) . '/autoimport/'.$_POST['brand_templates'].'.xml' ;

            import_beaver_builder_template($upload_dir_xml);

    }
}
add_action('admin_init', 'save_plugin_settings');

/**  Plugin updater checker integration start **/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/mobilemarketingllc/brand-map-addon',
    __FILE__,
    'brand-map-addon'
);

$myUpdateChecker->setAuthentication(array(
    'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
    'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));


$myUpdateChecker->setBranch('main');

/**  Plugin updater checker integration  end**/

// Mohawk Omnify Landing pages Integration

if (!wp_next_scheduled('mohawk_landing_pages_integration_cronjob')) {

    wp_schedule_event(time(), 'daily', 'mohawk_landing_pages_integration_cronjob');
}
add_action('mohawk_landing_pages_integration_cronjob', 'do_mohawk_landing_pages_integration_cronjob');

function do_mohawk_landing_pages_integration_cronjob()
{

    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);

    $binputs = array();
    $headers = array('authorization' => "bearer " . $result['access_token']);
    $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $binputs, $headers);
    $website_json = json_encode($website['result']);
    get_option('website_json') ? update_option('website_json', $website_json) : add_option('website_json', $website_json);
    $parent_url = get_option('flooring_page');

    $source_file = WP_PLUGIN_DIR . '/brand-map-addon/template-landing-page.php'; // Path to the file in the plugin
    $destination_file = get_stylesheet_directory() . '/template-landing-page.php'; // Path in the theme directory
    copy($source_file, $destination_file); // Copy the file

    $website = json_decode(get_option('website_json'));

    if ($website->options->mohawkBrandPage == 'true' || $website->options->shawBrandPage == 'true' || $website->options->karastanBrandPage == 'true' || $website->options->godfreyBrandPage == 'true' || $website->options->pergoBrandPage == 'true' ) {

        

        if(get_page_by_path($parent_url.'brands/', OBJECT, 'page') == ''){

            $flooring_page =    get_page_by_path($parent_url, OBJECT, 'page');

            $brand_main_data = array(
                'post_title'    => 'Brands',
                'post_name'     => 'brands',
                'post_content'  => '[fl_builder_insert_layout slug="brand_template_main"]',
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_author'  => 1,
                'post_parent'  =>  $flooring_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_brand_page = wp_insert_post($brand_main_data);
            add_post_meta($add_brand_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_brand_page, '_fl_builder_enabled', '1');
            //write_log('brand page created');

        }else{

            $brand_main_page = get_page_by_path($parent_url.'brands/', OBJECT, 'page');
            $brandmainpub = array('ID' => $brand_main_page->ID, 'post_status' => 'publish');
            wp_update_post($brandmainpub);
            //write_log('brand page published');

        }
    }elseif ($website->options->mohawkBrandPage == 'false' && $website->options->shawBrandPage == 'false' && $website->options->karastanBrandPage == 'false' && $website->options->godfreyBrandPage == 'false' && $website->options->pergoBrandPage == 'false' ) {        

            $brand_main_page = get_page_by_path($parent_url.'brands/', OBJECT, 'page');
            $brandmainpub = array('ID' => $brand_main_page->ID, 'post_status' => 'draft');
            wp_update_post($brandmainpub);
            //write_log('brand page drafted');
    }

    if ($website->options->mohawkBrandPage == 'true') {

        $mohawkbrands = json_decode($website->options->mohawkSubBrands);
        update_option('options_mohawkBrandPage', '1');
        (in_array("mohawk_everstrand", $mohawkbrands)) ? update_option('options_mohawk_everstrand', '1') : update_option('options_mohawk_everstrand', '0');
        (in_array("mohawk_smartstrand", $mohawkbrands)) ? update_option('options_mohawk_smartstrand', '1') : update_option('options_mohawk_smartstrand', '0');
        (in_array("mohawk_petpremier", $mohawkbrands)) ? update_option('options_mohawk_petpremier', '1') : update_option('options_mohawk_petpremier', '0');
        (in_array("mohawk_revwood", $mohawkbrands)) ? update_option('options_mohawk_revwood', '1') : update_option('options_mohawk_revwood', '0');
        (in_array("mohawk_solidtech", $mohawkbrands)) ? update_option('options_mohawk_solidtech', '1') : update_option('options_mohawk_solidtech', '0');
        (in_array("mohawk_solidtech_r", $mohawkbrands)) ? update_option('options_mohawk_solidtech_r', '1') : update_option('options_mohawk_solidtech_r', '0');
        (in_array("mohawk_puretech", $mohawkbrands)) ? update_option('options_mohawk_puretech', '1') : update_option('options_mohawk_puretech', '0');
        (in_array("mohawk_ultimateflex", $mohawkbrands)) ? update_option('options_mohawk_ultimateflex', '1') : update_option('options_mohawk_ultimateflex', '0');
        (in_array("mohawk_tecwood", $mohawkbrands)) ? update_option('options_mohawk_tecwood', '1') : update_option('options_mohawk_tecwood', '0');
    } else {

        update_option('options_mohawkBrandPage', '0');
        update_option('options_mohawk_everstrand', '0');
        update_option('options_mohawk_smartstrand', '0');
        update_option('options_mohawk_petpremier', '0');
        update_option('options_mohawk_revwood', '0');
        update_option('options_mohawk_solidtech', '0');
        update_option('options_mohawk_solidtech_r', '0');
        update_option('options_mohawk_puretech', '0');        
        update_option('options_mohawk_ultimateflex', '0');
        update_option('options_mohawk_tecwood', '0');
        
    }

    if ($website->options->pergoBrandPage == 'true') {

        $pergobrands = json_decode($website->options->pergoSubBrands);
        update_option('options_pergoBrandPage', '1');
        (in_array("pergo_elements", $pergobrands)) ? update_option('options_pergo_elements', '1') : update_option('options_pergo_elements', '0');
        (in_array("pergo_extreme", $pergobrands)) ? update_option('options_pergo_extreme', '1') : update_option('options_pergo_extreme', '0');
    } else {

        update_option('options_pergoBrandPage', '0');
        update_option('options_pergo_elements', '0');
        update_option('options_pergo_extreme', '0');
    }

    if ($website->options->godfreyBrandPage == 'true') {

        $godfreybrands = json_decode($website->options->godfreySubBrands);
        update_option('options_godfreyBrandPage', '1');
        (in_array("godfrey_hirst_wool", $godfreybrands)) ? update_option('options_godfrey_hirst_wool', '1') : update_option('options_godfrey_hirst_wool', '0');
        (in_array("godfrey_hirst_Smartstrand", $godfreybrands)) ? update_option('options_godfrey_hirst_Smartstrand', '1') : update_option('options_godfrey_hirst_Smartstrand', '0');
        (in_array("godfrey_hirst_everLux", $godfreybrands)) ? update_option('options_godfrey_hirst_everLux', '1') : update_option('options_godfrey_hirst_everLux', '0');
    } else {

        update_option('options_godfreyBrandPage', '0');
        update_option('options_godfrey_hirst_wool', '0');
        update_option('options_godfrey_hirst_Smartstrand', '0');
        update_option('options_godfrey_hirst_everLux', '0');
    }

    if ($website->options->karastanBrandPage == 'true') {

        $karastanbrands = json_decode($website->options->karastanSubBrands);
        update_option('options_karastanBrandPage', '1');
        (in_array("karastan_smartstrand", $karastanbrands)) ? update_option('options_karastan_smartstrand', '1') : update_option('options_karastan_smartstrand', '0');
        (in_array("karastan_luxecraft", $karastanbrands)) ? update_option('options_karastan_luxecraft', '1') : update_option('options_karastan_luxecraft', '0');
        (in_array("karastan_belleluxe", $karastanbrands)) ? update_option('options_karastan_belleluxe', '1') : update_option('options_karastan_belleluxe', '0');
    } else {

        update_option('options_karastanBrandPage', '0');
        update_option('options_karastan_smartstrand', '0');
        update_option('options_karastan_luxecraft', '0');
        update_option('options_karastan_belleluxe', '0');
    }

    $mohawkmapping = array(
        "mohawk_everstrand" => "EverStrand",
        "mohawk_smartstrand" => "Mohawk SmartStrand",
        "mohawk_petpremier" => "PETPremier",
        "mohawk_revwood" => "RevWood",
        "mohawk_solidtech" => "SolidTech",
        "mohawk_solidtech_r" => "SolidTech R",
        "mohawk_puretech" => "PUREtech",       
        "mohawk_ultimateflex" => "UltimateFlex",
        "mohawk_tecwood" => "TecWood",
        "karastan_smartstrand" => "Karastan SmartStrand",
        "karastan_luxecraft" => "LuxeCraft",
        "karastan_belleluxe" => "BelleLuxe",
        "godfrey_hirst_Smartstrand" => "Godfrey Hirst SmartStrand",
        "godfrey_hirst_wool" => "Godfrey Hirst Wool",
        "godfrey_hirst_everLux" => "Godfrey Hirst EverLux",
        "pergo_elements" => "Pergo Elements",
        "pergo_extreme" => "Pergo Extreme"
    );

    //////write_log($website->options->mohawkBrandPage);

    if ($website->options->mohawkBrandPage == 'true') {


        if (get_page_by_path($parent_url.'brands/mohawk/', OBJECT, 'page') == '') {

            $brand_page =    get_page_by_path($parent_url.'brands/', OBJECT, 'page');

            $mohawklanding_data = array(
                'post_title'    => 'Mohawk',
                'post_name'     => 'mohawk',
                'post_content'  => '[fl_builder_insert_layout slug="mohawkBrandPage"]',
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_excerpt'   => 'mohawkBrandPage',
                'post_author'  => 1,
                'post_parent'  =>  $brand_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_mohawk_page = wp_insert_post($mohawklanding_data);
            add_post_meta($add_mohawk_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_mohawk_page, '_fl_builder_enabled', '1');
        } else {

            $mohawk_page = get_page_by_path($parent_url.'brands/mohawk/', OBJECT, 'page');
            $mohawkmainpub = array('ID' => $mohawk_page->ID, 'post_status' => 'publish');
            wp_update_post($mohawkmainpub);
        }


        $mohawkbrands = json_decode($website->options->mohawkSubBrands);

        ////write_log($mohawkbrands);

        $mohawk_page = get_page_by_path($parent_url.'brands/mohawk/', OBJECT, 'page');

        foreach ($mohawkbrands as $mohawk_brand) {

            //  ////write_log($mohawk_brand);

            $brand_title  = $mohawkmapping[$mohawk_brand];
            // ////write_log($brand_title);


            if (get_page_by_path($parent_url.'brands/mohawk/' . sanitize_title($brand_title) . '/', OBJECT, 'page') == '') {



                $mohawksubbrandlanding_data = array(
                    'post_title'    => $brand_title,
                    'post_name'     => sanitize_title($brand_title),
                    'post_content'  => "[fl_builder_insert_layout slug='" . $mohawk_brand . "_brandmap']",
                    'post_type'     => 'page',
                    'post_status'   => 'publish',
                    'post_author'  => 1,
                    'post_excerpt' => $mohawk_brand,
                    'post_parent'  =>  $mohawk_page->ID,
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'

                );

                ////write_log($mohawksubbrandlanding_data);

                $subbrand_mohawk_page = wp_insert_post($mohawksubbrandlanding_data);
                add_post_meta($subbrand_mohawk_page, '_wp_page_template', 'template-landing-page.php');
                update_post_meta($subbrand_mohawk_page, '_fl_builder_enabled', '1');
            }
        }


        $child_args = array(
            'post_parent' => $mohawk_page->ID, // The parent id.
            'post_type'   => 'page'
        );

        $children = get_children($child_args);
        //////write_log($children);

        foreach ($children as $child) {

            ////write_log('child->'.$child->post_title);


            if (in_array($child->post_excerpt, $mohawkbrands)) {

                ////write_log('Assiged from cde');
                $mohawkpostpub = array('ID' => $child->ID, 'post_status' => 'publish');
                wp_update_post($mohawkpostpub);
            } else {

                ////write_log('Not Assiged from cde');
                $mohawkpostdraft = array('ID' => $child->ID, 'post_status' => 'draft');
                wp_update_post($mohawkpostdraft);
            }
        }
    } elseif ($website->options->mohawkBrandPage == 'false' || !isset($website->options->mohawkBrandPage)) {

        $mohawk_page = get_page_by_path($parent_url.'brands/mohawk/', OBJECT, 'page');

        if($mohawk_page){

            $mohawkmaindraft = array('ID' => $mohawk_page->ID, 'post_status' => 'draft');
            wp_update_post($mohawkmaindraft);

            $child_args = array(
                'post_parent' => $mohawk_page->ID, // The parent id.
                'post_type'   => 'page'
            );

            $children = get_children($child_args);

            foreach ($children as $child) {

                $mohawkpostdraft = array('ID' => $child->ID, 'post_status' => 'draft');
                wp_update_post($mohawkpostdraft);
            }
        }
    }

    /// pergo pages integration


    if ($website->options->pergoBrandPage == 'true') {


        if (get_page_by_path($parent_url.'brands/pergo/', OBJECT, 'page') == '') {

            $brand_page =    get_page_by_path($parent_url.'brands/', OBJECT, 'page');

            $pergolanding_data = array(
                'post_title'    => 'Pergo',
                'post_name'     => 'pergo',
                'post_content'  => '[fl_builder_insert_layout slug="pergoBrandPage"]',
                'post_type'     => 'page',
                'post_excerpt'   => 'pergoBrandPage',
                'post_status'   => 'publish',
                'post_author'  => 1,
                'post_parent'  =>  $brand_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_pergo_page = wp_insert_post($pergolanding_data);
            add_post_meta($add_pergo_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_pergo_page, '_fl_builder_enabled', '1');
        } else {

            $pergo_page = get_page_by_path($parent_url.'brands/pergo/', OBJECT, 'page');
            $pergomainpub = array('ID' => $pergo_page->ID, 'post_status' => 'publish');
            wp_update_post($pergomainpub);
        }


        $pergobrands = json_decode($website->options->pergoSubBrands);

        ////write_log($pergobrands);

        $pergo_page = get_page_by_path($parent_url.'brands/pergo/', OBJECT, 'page');

        foreach ($pergobrands as $pergo_brand) {

            //  ////write_log($mohawk_brand);

            $brand_title  = $mohawkmapping[$pergo_brand];
            // ////write_log($brand_title);


            if (get_page_by_path($parent_url.'brands/pergo/' . sanitize_title($brand_title) . '/', OBJECT, 'page') == '') {



                $pergosubbrandlanding_data = array(
                    'post_title'    => $brand_title,
                    'post_name'     => sanitize_title($brand_title),
                    'post_content'  => "[fl_builder_insert_layout slug='" . $pergo_brand . "_brandmap']",
                    'post_type'     => 'page',
                    'post_status'   => 'publish',
                    'post_author'  => 1,
                    'post_excerpt' => $pergo_brand,
                    'post_parent'  =>  $pergo_page->ID,
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'

                );

                ////write_log($pergosubbrandlanding_data);

                $subbrand_pergo_page = wp_insert_post($pergosubbrandlanding_data);
                add_post_meta($subbrand_pergo_page, '_wp_page_template', 'template-landing-page.php');
                update_post_meta($subbrand_pergo_page, '_fl_builder_enabled', '1');
            }
        }


        $pergochild_args = array(
            'post_parent' => $pergo_page->ID, // The parent id.
            'post_type'   => 'page'
        );

        $pergochildren = get_children($pergochild_args);
        //////write_log($children);

        foreach ($pergochildren as $pergochild) {

            ////write_log('child->'.$pergochild->post_title);


            if (in_array($pergochild->post_excerpt, $pergobrands)) {

                ////write_log('Assiged from cde');
                $pergopostpub = array('ID' => $pergochild->ID, 'post_status' => 'publish');
                wp_update_post($pergopostpub);
            } else {

                ////write_log('Not Assiged from cde');
                $pergopostdraft = array('ID' => $pergochild->ID, 'post_status' => 'draft');
                wp_update_post($pergopostdraft);
            }
        }
    } elseif ($website->options->pergoBrandPage == 'false' || !isset($website->options->pergoBrandPage)) {

        $pergo_page = get_page_by_path($parent_url.'brands/pergo/', OBJECT, 'page');

        if($pergo_page){

            $pergomaindraft = array('ID' => $pergo_page->ID, 'post_status' => 'draft');
            wp_update_post($pergomaindraft);

            $pergochild_args = array(
                'post_parent' => $pergo_page->ID, // The parent id.
                'post_type'   => 'page'
            );

            $pergochildren = get_children($pergochild_args);

            foreach ($pergochildren as $pergochild) {

                $pergopostdraft = array('ID' => $pergochild->ID, 'post_status' => 'draft');
                wp_update_post($pergopostdraft);
            }
        }
    }


    /// karastan pages integration


    if ($website->options->karastanBrandPage == 'true') {


        if (get_page_by_path($parent_url.'brands/karastan/', OBJECT, 'page') == '') {

            $brand_page =    get_page_by_path($parent_url.'brands/', OBJECT, 'page');

            $karastanlanding_data = array(
                'post_title'    => 'Karastan',
                'post_name'     => 'karastan',
                'post_content'  => '[fl_builder_insert_layout slug="karastanBrandPage"]',
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_excerpt'   => 'karastanBrandPage',
                'post_author'  => 1,
                'post_parent'  =>  $brand_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_karastan_page = wp_insert_post($karastanlanding_data);
            add_post_meta($add_karastan_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_karastan_page, '_fl_builder_enabled', '1');
        } else {

            $karastan_page = get_page_by_path($parent_url.'brands/karastan/', OBJECT, 'page');
            $karastanmainpub = array('ID' => $karastan_page->ID, 'post_status' => 'publish');
            wp_update_post($karastanmainpub);
        }


        $karastanbrands = json_decode($website->options->karastanSubBrands);

        ////write_log($karastanbrands);

        $karastan_page = get_page_by_path($parent_url.'brands/karastan/', OBJECT, 'page');

        foreach ($karastanbrands as $karastan_brand) {

            //  ////write_log($mohawk_brand);

            $brand_title  = $mohawkmapping[$karastan_brand];
            // ////write_log($brand_title);


            if (get_page_by_path($parent_url.'brands/karastan/' . sanitize_title($brand_title) . '/', OBJECT, 'page') == '') {



                $karastansubbrandlanding_data = array(
                    'post_title'    => $brand_title,
                    'post_name'     => sanitize_title($brand_title),
                    'post_content'  => "[fl_builder_insert_layout slug='" . $karastan_brand . "_brandmap']",
                    'post_type'     => 'page',
                    'post_status'   => 'publish',
                    'post_author'  => 1,
                    'post_excerpt' => $karastan_brand,
                    'post_parent'  =>  $karastan_page->ID,
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'

                );

                ////write_log($karastansubbrandlanding_data);

                $subbrand_karastan_page = wp_insert_post($karastansubbrandlanding_data);
                add_post_meta($subbrand_karastan_page, '_wp_page_template', 'template-landing-page.php');
                update_post_meta($subbrand_karastan_page, '_fl_builder_enabled', '1');
            }
        }


        $karastanchild_args = array(
            'post_parent' => $karastan_page->ID, // The parent id.
            'post_type'   => 'page'
        );

        $karastanchildren = get_children($karastanchild_args);
        //////write_log($children);

        foreach ($karastanchildren as $karastanchild) {

            ////write_log('child->'.$karastanchild->post_title);


            if (in_array($karastanchild->post_excerpt, $karastanbrands)) {

                ////write_log('Assiged from cde');
                $karastanpostpub = array('ID' => $karastanchild->ID, 'post_status' => 'publish');
                wp_update_post($karastanpostpub);
            } else {

                ////write_log('Not Assiged from cde');
                $karastanpostdraft = array('ID' => $karastanchild->ID, 'post_status' => 'draft');
                wp_update_post($karastanpostdraft);
            }
        }
    } elseif ($website->options->karastanBrandPage == 'false' || !isset($website->options->karastanBrandPage)) {

        $karastan_page = get_page_by_path($parent_url.'brands/karastan/', OBJECT, 'page');

        if($karastan_page){

            $karastanmaindraft = array('ID' => $karastan_page->ID, 'post_status' => 'draft');
            wp_update_post($karastanmaindraft);

            $karastanchild_args = array(
                'post_parent' => $karastan_page->ID, // The parent id.
                'post_type'   => 'page'
            );

            $karastanchildren = get_children($karastanchild_args);

            foreach ($karastanchildren as $karastanchild) {

                $karastanpostdraft = array('ID' => $karastanchild->ID, 'post_status' => 'draft');
                wp_update_post($karastanpostdraft);
            }

        }
    }

    /// godfrey pages integration


    if ($website->options->godfreyBrandPage == 'true') {


        if (get_page_by_path($parent_url.'brands/godfrey-hirst/', OBJECT, 'page') == '') {

            $brand_page =    get_page_by_path($parent_url.'brands/', OBJECT, 'page');

            $godfreylanding_data = array(
                'post_title'    => 'Godfrey Hirst',
                'post_name'     => 'godfrey-hirst',
                'post_content'  => '[fl_builder_insert_layout slug="godfreyBrandPage"]',
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_excerpt'   => 'godfreyBrandPage',
                'post_author'  => 1,
                'post_parent'  =>  $brand_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_godfrey_page = wp_insert_post($godfreylanding_data);
            add_post_meta($add_godfrey_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_godfrey_page, '_fl_builder_enabled', '1');
        } else {

            $godfrey_page = get_page_by_path($parent_url.'brands/godfrey-hirst/', OBJECT, 'page');
            $godfreymainpub = array('ID' => $godfrey_page->ID, 'post_status' => 'publish');
            wp_update_post($godfreymainpub);
        }


        $godfreybrands = json_decode($website->options->godfreySubBrands);

        ////write_log($godfreybrands);

        $godfrey_page = get_page_by_path($parent_url.'brands/godfrey-hirst/', OBJECT, 'page');

        foreach ($godfreybrands as $godfrey_brand) {

            //  ////write_log($mohawk_brand);

            $brand_title  = $mohawkmapping[$godfrey_brand];
            // ////write_log($brand_title);


            if (get_page_by_path($parent_url.'brands/godfrey-hirst/' . sanitize_title($brand_title) . '/', OBJECT, 'page') == '') {



                $godfreysubbrandlanding_data = array(
                    'post_title'    => $brand_title,
                    'post_name'     => sanitize_title($brand_title),
                    'post_content'  => "[fl_builder_insert_layout slug='" . $godfrey_brand . "_brandmap']",
                    'post_type'     => 'page',
                    'post_status'   => 'publish',
                    'post_author'  => 1,
                    'post_excerpt' => $godfrey_brand,
                    'post_parent'  =>  $godfrey_page->ID,
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'

                );

                ////write_log($godfreysubbrandlanding_data);

                $subbrand_godfrey_page = wp_insert_post($godfreysubbrandlanding_data);
                add_post_meta($subbrand_godfrey_page, '_wp_page_template', 'template-landing-page.php');
                update_post_meta($subbrand_godfrey_page, '_fl_builder_enabled', '1');
            }
        }


        $godfreychild_args = array(
            'post_parent' => $godfrey_page->ID, // The parent id.
            'post_type'   => 'page'
        );

        $godfreychildren = get_children($godfreychild_args);
        //////write_log($children);

        foreach ($godfreychildren as $godfreychild) {

            ////write_log('child->'.$godfreychild->post_title);


            if (in_array($godfreychild->post_excerpt, $godfreybrands)) {

                ////write_log('Assiged from cde');
                $godfreypostpub = array('ID' => $godfreychild->ID, 'post_status' => 'publish');
                wp_update_post($godfreypostpub);
            } else {

                ////write_log('Not Assiged from cde');
                $godfreypostdraft = array('ID' => $godfreychild->ID, 'post_status' => 'draft');
                wp_update_post($godfreypostdraft);
            }
        }
    } elseif ($website->options->godfreyBrandPage == 'false' || !isset($website->options->godfreyBrandPage)) {

        $godfrey_page = get_page_by_path($parent_url.'brands/godfrey-hirst/', OBJECT, 'page');

        if($godfrey_page){

            $godfreymaindraft = array('ID' => $godfrey_page->ID, 'post_status' => 'draft');
            wp_update_post($godfreymaindraft);

            $godfreychild_args = array(
                'post_parent' => $godfrey_page->ID, // The parent id.
                'post_type'   => 'page'
            );

            $godfreychildren = get_children($godfreychild_args);

            foreach ($godfreychildren as $godfreychild) {

                $godfreypostdraft = array('ID' => $godfreychild->ID, 'post_status' => 'draft');
                wp_update_post($godfreypostdraft);
            }
        }
    }
}

// Shaw Landing pages Integration

if (!wp_next_scheduled('shaw_landing_pages_integration_cronjob')) {

    wp_schedule_event(time(), 'daily', 'shaw_landing_pages_integration_cronjob');
}
add_action('shaw_landing_pages_integration_cronjob', 'do_shaw_landing_pages_integration_cronjob');

function do_shaw_landing_pages_integration_cronjob()
{

    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);
    $parent_url = get_option('flooring_page');
    $shaw_page = get_page_by_path($parent_url.'brands/shaw/', OBJECT, 'page');
    $binputs = array();
    $headers = array('authorization' => "bearer " . $result['access_token']);
    $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $binputs, $headers);
    $website_json = json_encode($website['result']);
    get_option('website_json') ? update_option('website_json', $website_json) : add_option('website_json', $website_json);

    $source_file = WP_PLUGIN_DIR . '/brand-map-addon/template-landing-page.php'; // Path to the file in the plugin
    $destination_file = get_stylesheet_directory() . '/template-landing-page.php'; // Path in the theme directory
    copy($source_file, $destination_file); // Copy the file

    $website = json_decode(get_option('website_json'));

    //add optionfor each brand
    //write_log('shaw_page');
    //write_log($shaw_page);
    

    if ($website->options->shawBrandPage == 'true') {

        $shawbrands = json_decode($website->options->shawSubBrands);
        update_option('options_shawBrandPage', '1');
        (in_array("anso_colorwall", $shawbrands)) ? update_option('options_anso_colorwall', '1') : update_option('options_anso_colorwall', '0');
        (in_array("pet_perfect", $shawbrands)) ? update_option('options_pet_perfect', '1') : update_option('options_pet_perfect', '0');
        (in_array("anderson_tuftex", $shawbrands)) ? update_option('options_anderson_tuftex', '1') : update_option('options_anderson_tuftex', '0');
        (in_array("shaw_grass", $shawbrands)) ? update_option('options_shaw_grass', '1') : update_option('options_shaw_grass', '0');
        (in_array("philadelphia_commercial", $shawbrands)) ? update_option('options_philadelphia_commercial', '1') : update_option('options_philadelphia_commercial', '0');
        (in_array("floorte", $shawbrands)) ? update_option('options_floorte', '1') : update_option('options_floorte', '0');
        (in_array("coretec", $shawbrands)) ? update_option('options_coretec', '1') : update_option('options_coretec', '0');
    } else {

        update_option('options_shawBrandPage', '0');
        update_option('options_anso_colorwall', '0');
        update_option('options_pet_perfect', '0');
        update_option('options_anderson_tuftex', '0');
        update_option('options_shaw_grass', '0');
        update_option('options_philadelphia_commercial', '0');
        update_option('options_floorte', '0');
        update_option('options_coretec', '0');
    }


    $shawmapping = array(
        "anso_colorwall" => "Anso Colorwall",
        "pet_perfect" => "Pet Perfect",
        "anderson_tuftex" => "Anderson Tuftex",
        "shaw_grass" => "Shaw Grass",
        "philadelphia_commercial" => "Philadelphia Commerical",
        "floorte" => "Floorte",
        "coretec" => "COREtec"
    );

    //////write_log($website->options->shawBrandPage);

    if ($website->options->mohawkBrandPage == 'true' || $website->options->shawBrandPage == 'true' || $website->options->karastanBrandPage == 'true' || $website->options->godfreyBrandPage == 'true' || $website->options->pergoBrandPage == 'true' ) {

        if(get_page_by_path($parent_url.'brands/', OBJECT, 'page') == ''){

            $flooring_page =    get_page_by_path($parent_url, OBJECT, 'page');

            $brand_main_data = array(
                'post_title'    => 'Brands',
                'post_name'     => 'brands',
                'post_content'  => '[fl_builder_insert_layout slug="brand_template_main"]',
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_author'  => 1,
                'post_parent'  =>  $flooring_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_brand_page = wp_insert_post($brand_main_data);
            add_post_meta($add_brand_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_brand_page, '_fl_builder_enabled', '1');
            //write_log('brand page created');

        }else{

            $brand_main_page = get_page_by_path($parent_url.'brands/', OBJECT, 'page');
            $brandmainpub = array('ID' => $brand_main_page->ID, 'post_status' => 'publish');
            wp_update_post($brandmainpub);
            //write_log('brand page published');

        }
    }elseif ($website->options->mohawkBrandPage == 'false' && $website->options->shawBrandPage == 'false' && $website->options->karastanBrandPage == 'false' && $website->options->godfreyBrandPage == 'false' && $website->options->pergoBrandPage == 'false' ) {        

            $brand_main_page = get_page_by_path($parent_url.'brands/', OBJECT, 'page');
            $brandmainpub = array('ID' => $brand_main_page->ID, 'post_status' => 'draft');
            wp_update_post($brandmainpub);
            //write_log('brand page drafted');
    }

    if ($website->options->shawBrandPage == 'true') {


        if (get_page_by_path($parent_url.'brands/shaw/', OBJECT, 'page') == '') {

            $brand_page =    get_page_by_path($parent_url.'brands/', OBJECT, 'page');

            $shawlanding_data = array(
                'post_title'    => 'Shaw',
                'post_name'     => 'shaw',
                'post_content'  => '[fl_builder_insert_layout slug="shawBrandPage"]',
                'post_type'     => 'page',
                'post_status'   => 'publish',
                'post_excerpt'   => 'shawBrandPage',
                'post_author'  => 1,
                'post_parent'  =>  $brand_page->ID,
                'comment_status' => 'closed',   // if you prefer
                'ping_status' => 'closed'

            );

            $add_shaw_page = wp_insert_post($shawlanding_data);
            add_post_meta($add_shaw_page, '_wp_page_template', 'template-landing-page.php');
            update_post_meta($add_shaw_page, '_fl_builder_enabled', '1');

            ////write_log(get_permalink($add_shaw_page));
        } else {

            $shaw_page = get_page_by_path($parent_url.'brands/shaw/', OBJECT, 'page');
            $shawmainpub = array('ID' => $shaw_page->ID, 'post_status' => 'publish');
            wp_update_post($shawmainpub);
        }


        $shawbrands = json_decode($website->options->shawSubBrands);        

        $shaw_page = get_page_by_path($parent_url.'brands/shaw/', OBJECT, 'page');

      
       

        foreach ($shawbrands as $shaw_brand) {

            //  ////write_log($shaw_brand);

            $brand_title  = $shawmapping[$shaw_brand];
            // ////write_log($brand_title);


            if (get_page_by_path($parent_url.'brands/shaw/' . sanitize_title($brand_title) . '/', OBJECT, 'page') == '') {



                $shawsubbrandlanding_data = array(
                    'post_title'    => $brand_title,
                    'post_name'     => sanitize_title($brand_title),
                    'post_content'  => "[fl_builder_insert_layout slug='" . $shaw_brand . "_brandmap']",
                    'post_type'     => 'page',
                    'post_status'   => 'publish',
                    'post_author'  => 1,
                    'post_excerpt' => $shaw_brand,
                    'post_parent'  =>  $shaw_page->ID,
                    'comment_status' => 'closed',   // if you prefer
                    'ping_status' => 'closed'

                );

                ////write_log($shawsubbrandlanding_data);

                $subbrand_shaw_page = wp_insert_post($shawsubbrandlanding_data);
                add_post_meta($subbrand_shaw_page, '_wp_page_template', 'template-landing-page.php');
                update_post_meta($subbrand_shaw_page, '_fl_builder_enabled', '1');
                ////write_log(get_permalink($subbrand_shaw_page));
            }
        }


        $child_args = array(
            'post_parent' => $shaw_page->ID, // The parent id.
            'post_type'   => 'page'
        );

        $children = get_children($child_args);
        //////write_log($children);

        foreach ($children as $child) {

            ////write_log('child->'.$child->post_title);


            if (in_array($child->post_excerpt, $shawbrands)) {

                ////write_log('Assiged from cde');
                $shawpostpub = array('ID' => $child->ID, 'post_status' => 'publish');
                wp_update_post($shawpostpub);
            } else {

                ////write_log('Not Assiged from cde');
                $shawpostdraft = array('ID' => $child->ID, 'post_status' => 'draft');
                wp_update_post($shawpostdraft);
            }
        }
    } elseif ($website->options->shawBrandPage == 'false' || !isset($website->options->shawBrandPage)) {

        //write_log('false condition');        
        $shaw_page = get_page_by_path($parent_url.'brands/shaw/', OBJECT, 'page');

        if($shaw_page){

                $shawmaindraft = array('ID' => $shaw_page->ID, 'post_status' => 'draft');
                wp_update_post($shawmaindraft);

                $child_args = array(
                    'post_parent' => $shaw_page->ID, // The parent id.
                    'post_type'   => 'page'
                );

                $children = get_children($child_args);

                foreach ($children as $child) {

                    $shawpostdraft = array('ID' => $child->ID, 'post_status' => 'draft');
                    wp_update_post($shawpostdraft);
                }

        }
    }
}

// Brand Product slider
add_shortcode('BRAND_PRODUCT_LOOP', 'brand_product_loop_function');
function brand_product_loop_function()
{
    global $wpdb;
    $table_posts = $wpdb->prefix . 'posts';
    $products = array();
    $atts = array();

    $brand_name = get_the_excerpt(get_the_ID());
    $short_param =  get_option('shortcode_'.$brand_name, array());
    //write_log($short_param);
    if(!empty( $short_param)){

        if(isset($short_param['category'])){
            $atts['category'] = $short_param['category'];
        }
        if(isset($short_param['brand'])){
            $atts['brand_facet'] = $short_param['brand'];
        }
        if(isset($short_param['collection']) ){
            $atts['collection'] = $short_param['collection'];
        }
        if(isset($short_param['material']) ){
            $atts['material'] = $short_param['material'];
        }
    }
    
    //write_log($atts);
   
    if (get_option('retailer_env') == 'grand-child') {
        $postmapping = array(
            "carpet" => "carpeting",
            "hardwood" => "hardwood_catalog",
            "laminate" => "laminate_catalog",
            "lvt" => "luxury_vinyl_tile",
            "tile" => "tile_catalog",
            "paint" => "paint_catalog",
            "rugs" => "area_rugs"
        );

        $meta_query = ['relation' => 'AND'];

            if (!empty($atts['brand_facet'])) {
                $meta_query[] = [
                    'key'     => 'brand_facet',
                    'value'   => sanitize_text_field($atts['brand_facet']),
                    'compare' => '='
                ];
            } elseif (!empty($atts['material'])) {
                $meta_query[] = [
                    'key'     => 'material',
                    'value'   => $atts['material'],
                    'compare' => 'LIKE'
                ];
            }elseif (!empty($atts['collection'])) {
                $meta_query[] = [
                    'key'     => 'collection_facet',
                    'value'   => $atts['collection'],
                    'compare' => 'LIKE'
                ];
            }

        $args = array(
            "post_type" => $postmapping[$atts['category']],
            "post_status" => "publish",
            "orderby" => "title",
            "order" => "rand",
            "posts_per_page" => 10,
            'meta_query' => $meta_query,

        );
        $the_query = new WP_Query($args);

        if ($the_query->have_posts()) {
            $i = 0;
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $post_id = get_the_ID();
                $products[$i]['image'] = swatch_image_product_thumbnail($post_id, '400', '400');
                $products[$i]['style'] = "padding: 5px;";
                $products[$i]['link'] = get_the_permalink();
                $products[$i]['title'] = get_the_title();
                $products[$i]['collection'] = get_field('collection',$post_id);
                $products[$i]['color'] = get_field('color',$post_id);
                $products[$i]['brand'] = get_field('brand',$post_id);
                $i++;
            }
        }
        wp_reset_postdata();
    } else if (get_option('retailer_env') == 'react') {
        $limit = isset($atts['limit']) ? $atts['limit'] : 10;

        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        $data = json_decode($redis->get($atts['category']), true);

        if (!is_array($data) || count($data) == 0) {
            $json_filename = @$atts['category'] . '.json';
            $upload_dir = wp_upload_dir();
            $json_url = $upload_dir['baseurl'] . '/sfn-data/' . $json_filename;
            $response_first = wp_remote_get($json_url);

            if (is_wp_error($response_first)) {
                $error_message = $response_first->get_error_message();
                echo "There was an error: $error_message";
            } else {
                $json_data = wp_remote_retrieve_body($response_first);
            }

            $data = json_decode($json_data, true);

            if (is_array($data) && count($data) > 0) {
                $redis->set($atts['category'], json_encode($data));
            }
        }
       
        if (is_array($data) && count($data) == 0) {
            return false;
        }

        $products = array();
        $i = 0;
        foreach ($data as $product) {
            if ($i >= $limit) {
                break;
            }
            if (empty($product['swatch']) || empty($product['status']) || $product['status'] == "inactive") {
                continue;
            }

            if(isset($atts['brand_facet']) && !empty($atts['brand_facet'])){

                if ((empty($product['brand_facet']) || trim(strtolower($product['brand_facet'])) != trim(strtolower($atts['brand_facet'])))) {
                    continue;
                }
            }

            if(isset($atts['material']) && !empty($atts['material'])){

                if ((empty($product['material']) || trim(strtolower($product['material'])) != trim(strtolower($atts['material'])))) 
                {
                    continue;
                }
            }

            if(isset($atts['collection']) && !empty($atts['collection'])){

                if ((empty($product['collection_facet']) || !str_contains(trim(strtolower($product['collection_facet'])), trim(strtolower($atts['collection']))) )) 
            {
                continue;
            }

            }
            

            $name = trim($product['name']) . " " . trim($product['sku']);
            $page = $wpdb->get_row($wpdb->prepare("select guid from $table_posts where post_title='" . $name . "'"));
            if (!isset($page->guid)) {
                continue;
            }


            $image =
                "https://mm-media-res.cloudinary.com/image/fetch/h_400,w_400,c_limit/https://" . trim($product['swatch']);

            $products[$i]['image'] = $image;
            $products[$i]['style'] = "padding: 5px;";
            $products[$i]['link'] = $page->guid;
            $products[$i]['title'] = $name;
            $products[$i]['collection'] =  trim($product['collection_facet']);
            $products[$i]['color'] = trim($product['color']);
            $products[$i]['brand'] = trim($product['brand']);
            $i++;
        }
        if (count($products) == 0) {
            return false;
        }
    }
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper landing_page_wrapper '.get_option('retailer_env').'">				
				<div class="row product-row brand_product_slider slider responsive">';
    if (count($products) > 0) {
        foreach ($products as $product) {
            $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="' . $product['link'] . '" title="' . $product['title'] . '">
                    <img class="list-pro-image" src="' . $product['image'] . '" alt="' . $product['title'] . '">';



            $content .= '</a></div>';

            $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="' . $product['link'] . '" title="' . $product['title'] . '" class="collection_text">' . $product['collection'] . '</a> 
                            <a href="' . $product['link'] . '" title="' . $product['title'] . '"><span class="color_text"> 
                            ' . $product['color'] . ' </span></a>
                                <span class="brand_text">' . $product['brand'] . ' </span>
                                <a href="' . $product['link'] . '" target="_self" class="view-product fl-button" role="button" aria-label="VIEW PRODUCT">
							<span class="uabb-button-text uabb-creative-button-text">VIEW PRODUCT</span></a>
                            </h4>
                        </div>            
                </div>
            </div>';
        }
    } else {
        $content .= "<h2>No products available</h2>";
    }
    $content .= '</div>
			</div>';
    return $content;
}

function product_brand_slider_js()
{
?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery(".brand_product_slider > div").length > 4 &&
                jQuery(".brand_product_slider").slick({
                    slidesToScroll: 4,
                    slidesToShow: 4,
                    arrows: !0,
                    infinite: !0,
                    mobileFirst: !1,
                    prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-up"></i></a>',
                    nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-down"></i></a>',
                    responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        }
                    ],
                });
        });
    </script>
<?php
}
add_action('wp_footer', 'product_brand_slider_js');



/**  Auto Import Function for mohawk/shaw Templates **/

if (!wp_next_scheduled('mohawk_shaw_template_autoimport_cronjob')) {

    wp_schedule_event(time(), 'daily', 'mohawk_shaw_template_autoimport_cronjob');
}
//add_action('mohawk_shaw_template_autoimport_cronjob', 'do_mohawk_shaw_template_autoimport_cronjob');

function do_mohawk_shaw_template_autoimport_cronjob()
{
    $Brandmapping = array(
        "brandmap-main" => "Brand Main Page",
        "mohawkBrandPage" => "Mohawk Main Page",
        "mohawk_everstrand" => "EverStrand",
        "mohawk_smartstrand" => "Mohawk SmartStrand",
        "mohawk_petpremier" => "PETPremier",
        "mohawk_revwood" => "RevWood",
        "mohawk_solidtech" => "SolidTech",
        "mohawk_solidtech_r" => "SolidTech R",
        "mohawk_puretech" => "PUREtech",       
        "mohawk_ultimateflex" => "UltimateFlex",
        "mohawk_tecwood" => "TecWood",
        "karastanBrandPage" => "Karastan Main Page",
        "karastan_smartstrand" => "Karastan SmartStrand",
        "karastan_luxecraft" => "LuxeCraft",
        "karastan_belleluxe" => "BellaLuxe",
        "godfreyBrandPage" => "Godfrey Main Page",
        "godfrey_hirst_Smartstrand" => "Godfrey Hirst SmartStrand",
        "godfrey_hirst_wool" => "Godfrey Hirst Wool",
        "godfrey_hirst_everLux" => "Godfrey Hirst EverLux",
        "pergoBrandPage" => "Pergo Main Page",
        "pergo_elements" => "Pergo Elements",
        "pergo_extreme" => "Pergo Extreme",
        "shawBrandPage" => "Shaw Main Page",
        "anso_colorwall" => "Anso Colorwall",
        "pet_perfect" => "Pet Perfect",
        "anderson_tuftex" => "Anderson Tuftex",
        "shaw_grass" => "Shaw Grass",
        "philadelphia_commercial" => "Philadelphia Commerical",
        "floorte" => "Floorte",
        "coretec" => "COREtec"
    );

        foreach ($Brandmapping as $key => $value) {

            
            $xml_file_path = plugin_dir_path( __FILE__ ) . '/autoimport/'.$key.'.xml' ;

            //write_log($xml_file_path);

            if (file_exists($xml_file_path)) {  
                //write_log("XML file found.");    
             
                $xml = simplexml_load_file($xml_file_path);
                if ($xml !='') {   

                    foreach ($xml->channel->item as $item) {
                        $post_type = (string) $item->children('wp', true)->post_type;
                
                        if ($post_type === 'fl-builder-template') {
                            $post_title = (string) $item->title;
                            $post_name = (string) $item->children('wp', true)->post_name;
                            $post_content = (string) $item->children('content', true);
                            $post_id = (int) $item->children('wp', true)->post_id;
                            
                
                            $post_data = [
                                'post_title'   => $post_title,
                                'post_name'    => $post_name,
                                'post_content' => $post_content,
                                'post_status'  => 'publish',
                                'post_type'    => 'fl-builder-template'
                            ];
                
                            $postmeta = $item->children('wp', true)->postmeta;
                            $fl_builder_data = '';
                
                            foreach ($postmeta as $meta) {
                                $meta_key = (string) $meta->meta_key;
                                if ($meta_key === '_fl_builder_data') {
                                    $fl_builder_data = (string) $meta->meta_value;
                                    break;
                                }
                            }
                
                            $existing_post = get_page_by_path($post_name, OBJECT, 'fl-builder-template');
                
                            if ($existing_post) {
                                
                                $post_data['ID'] = $existing_post->ID;
                                wp_update_post($post_data);
                                update_post_meta($existing_post->ID, '_fl_builder_data', maybe_unserialize($fl_builder_data));
                                update_post_meta($existing_post->ID, '_fl_builder_enabled', 1);  
                                //write_log("Updated existing template");
                            } else {
                            
                                $new_post_id = wp_insert_post($post_data);
                                update_post_meta($new_post_id, '_fl_builder_data', maybe_unserialize($fl_builder_data));
                                update_post_meta($new_post_id, '_fl_builder_enabled', 1);                
                                //write_log("Imported new template");
                            }
                        }
                    }
                }
          }
        }   
}

//import brand landing page template
function import_beaver_builder_template($xml_file_path) {

    //write_log($xml_file_path);

    if (!file_exists($xml_file_path)) {
        //write_log("XML file not found.");
        die();
    }

    $xml = simplexml_load_file($xml_file_path);
    if (!$xml) {
        
        //write_log("Error loading XML file.");
        die();
    }

    global $wpdb;

    foreach ($xml->channel->item as $item) {
        $post_type = (string) $item->children('wp', true)->post_type;

        if ($post_type === 'fl-builder-template') {
            $post_title = (string) $item->title;
            $post_name = (string) $item->children('wp', true)->post_name;
            $post_content = (string) $item->children('content', true);
            $post_id = (int) $item->children('wp', true)->post_id;
            

            $post_data = [
                'post_title'   => $post_title,
                'post_name'    => $post_name,
                'post_content' => $post_content,
                'post_status'  => 'publish',
                'post_type'    => 'fl-builder-template'
            ];

            $postmeta = $item->children('wp', true)->postmeta;
            $fl_builder_data = '';

            foreach ($postmeta as $meta) {
                $meta_key = (string) $meta->meta_key;
                if ($meta_key === '_fl_builder_data') {
                    $fl_builder_data = (string) $meta->meta_value;
                    break;
                }
            }

            $existing_post = get_page_by_path($post_name, OBJECT, 'fl-builder-template');

            if ($existing_post) {
                
                $post_data['ID'] = $existing_post->ID;
                wp_update_post($post_data);
                update_post_meta($existing_post->ID, '_fl_builder_data', maybe_unserialize($fl_builder_data));
                update_post_meta($existing_post->ID, '_fl_builder_enabled', 1);  
                //write_log("Updated existing template");
            } else {
               
                $new_post_id = wp_insert_post($post_data);
                update_post_meta($new_post_id, '_fl_builder_data', maybe_unserialize($fl_builder_data));
                update_post_meta($new_post_id, '_fl_builder_enabled', 1);                
                //write_log("Imported new template");
            }
        }
    }
}


//import gravity form

if (!wp_next_scheduled('import_gravity_form_from_json_cronjob')) {

    wp_schedule_event(time(), 'daily', 'import_gravity_form_from_json_cronjob');
}
add_action('import_gravity_form_from_json_cronjob', 'do_import_gravity_form_from_json');

function do_import_gravity_form_from_json() {

    $file_path = plugin_dir_path( __FILE__ ) . '/autoimport/brand-map.json'; // Update path if needed 

    if (!class_exists('GFAPI')) {
        //write_log("GFAPI class not found. Make sure Gravity Forms is activated.");
    }

    if (!file_exists($file_path)) {
        //write_log("File not found: " . $file_path);
    }
   
    $json_data = file_get_contents($file_path);
    $form_data = json_decode($json_data, true);

    
    if (!$form_data || !isset($form_data['0'])) {
        //write_log( "Invalid JSON format.");
    }
    
    $form_title = $form_data['0']['title'] ?? '';

    if (!$form_title) {
        //write_log ("Form title not found in JSON.");
    }

    // Check if a form with the same title already exists
    $existing_forms = GFAPI::get_forms();
    foreach ($existing_forms as $existing_form) {
        if ($existing_form['title'] === $form_title) {
            //write_log ("Gravity Form '{$form_title}' already exists with ID: " . $existing_form['id']);
            return;
        }
    }

    // Import form
    $form_id = GFAPI::add_form($form_data['0']); // The form data is under index '0'
    
    if ($form_id) {
        //write_log( "imported successfully'. $form_id");
    } else {
        //write_log( "Failed.");
    }
}

//brand list shortcode [brandlist] 
function brand_page_list_shortcode() {  
    
    $website = json_decode(get_option('website_json'));
    $parent_url = get_option('flooring_page');
    $cde_brands = json_decode(json_encode($website->options), true);  

    $brands_list = array(
        array(
            "cde" => "mohawkBrandPage",
            "name" => "Mohawk",
            "slug" => "mohawk",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1656011761/BrandLogos/mohawk.jpg",
            "desc" => "With a proud history of excellence, Mohawk innovates with state-of-the-art, performance-driven flooring products."
        ),
      array(
            "cde" => "shawBrandPage",
            "name" => "Shaw",
            "slug" => "shaw",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1739589634/brand-map/shaw/Shaw-Logo-new.webp",
            "desc" => "From first homes to forever homes, Shaw Floors have been providing whole home solutions tailored for each stage of life's journey since 1967."
        ),
      array(
            "cde" => "karastanBrandPage",
            "name" => "Karastan Flooring",
            "slug" => "karastan",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1656011759/BrandLogos/karastan-logo.jpg",
            "desc" => "Synonymous with luxury for more than 95 years, Karastan transforms homes with lush carpet and exceptional wood floors."
        ),
       array(
            "cde" => "godfreyBrandPage",
            "name" => "Godfrey Hirst",
            "slug" => "godfrey-hirst",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1656011756/BrandLogos/godfrey-hirst.jpg",
            "desc" => "Relish unmatched comfort with Godfrey Hirst carpets, even made with wool sourced directly from the farm."
        ),
         array(
            "cde" => "pergoBrandPage",
            "name" => "Pergo",
            "slug" => "pergo",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1739967586/BrandLogos/pergo-black-logo.webp",
            "desc" => "From engineering to design to installation and your peace of mind, Pergo floors are truly built for life without compromise."
        )
    );
 $content = '<div class="brands-logos-wrap">';

  foreach($brands_list as $branditem){   

    if ($cde_brands[$branditem['cde']] == 'true') {
     

        $content .= '<div class="brand-logo-single '.$branditem['cde'].'">
                        <div class="logo-image-wrap">
                            <img decoding="async" class="uabb-photo-img" src="'.$branditem['img'].'" alt="" title="" itemprop="image">
                        </div>
                        <div class="brand-logo-title-wrap">
                            <h3 class="brand-logo-title">'.$branditem['name'].'</h3>
                        </div>			
                        <div class="brand-link-text-wrap">
                            <div class="brand-text">
                            <p>'.$branditem['desc'].'</p>
                            </div>
                            <a href="'.$parent_url.'brands/'.$branditem['slug'].'/" target="_self" class="fl-button">SHOP BRAND</a>			
                        </div>
                </div>';
    }
  }

$content .= '</div>';

return $content;
}
add_shortcode('brandlist', 'brand_page_list_shortcode');

//brand list shortcode [brandlist] 
function mohawkbrandlist_list_shortcode() {  
    
    $website = json_decode(get_option('website_json'));

    $cde_brands = json_decode($website->options->mohawkSubBrands);
    $parent_url = get_option('flooring_page');
  
    $brands_list = array(
        array(
            "cde" => "mohawk_solidtech",
            "name" => "SolidTech",
            "slug" => "solidtech",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1738907476/brand-map/mohawk/Solidtech_lockup_logo.webp",
            "desc" => "Experience the joy of waterproof, stain-resistant, and pet-friendly floors with beautiful hardwood visuals, minus the maintenance and expense of real wood."
        ),
      array(
            "cde" => "mohawk_smartstrand",
            "name" => "SmartStrand",
            "slug" => "mohawk-smartstrand",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1738907476/brand-map/mohawk/smartstrand_logo_color.webp",
            "desc" => "The softest, most durable, easiest-to-clean carpet on the planet. Fibers feature permanent built-in stain resistance and Nanoloc spill and soil protection."
        ),
      array(
            "cde" => "mohawk_everstrand",
            "name" => "EverStrand",
            "slug" => "everstrand",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1738907476/brand-map/mohawk/everstrand_logo_color.webp",
            "desc" => "Stain-resistant, easy-to-clean carpet in beautiful colors and patterns for your home. Sustainability never looked so good."
        ),
       array(
            "cde" => "mohawk_petpremier",
            "name" => "PET Premier",
            "slug" => "petpremier",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1739451470/brand-map/mohawk/PetPremier-Mohawk-Color-Tagline-Lockup-Logo1.webp",
            "desc" => "Soft, stain-resistant carpet sustainably made with recycled bottles, perfect for life indoors with our favorite creatures."
        ),
         array(
            "cde" => "mohawk_revwood",
            "name" => "RevWood",
            "slug" => "revwood",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1738907476/brand-map/mohawk/mhk_revwood_logo.webp",
            "desc" => "Boasting authentic hardwood visuals and the performance-driven nature of laminate flooring, RevWood is a flooring that is sure to turn heads in your home."
        ),
        array(
            "cde" => "mohawk_puretech",
            "name" => "PUREtech",
            "slug" => "puretech",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1739453684/brand-map/mohawk/mhk_puretech_lockup_color2000px1.webp",
            "desc" => "Planet-friendly waterproof resilient flooring where sustainability meets performance."
        ),
        array(
            "cde" => "mohawk_solidtech_r",
            "name" => "SolidTech R",
            "slug" => "solidtech-r",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1738927487/brand-map/mohawk/solidtech_r_tagline_color_logo.webp",
            "desc" => "SolidTech® R flooring is built for the people, pets, and planet you love. PVC-free and made with natural stone and recycled single-use plastic for scratchproof, floodproof protection, SolidTech R is the heart of performance flooring."
        ),
        array(
            "cde" => "mohawk_ultimateflex",
            "name" => "UltimateFlex",
            "slug" => "ultimateflex",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1738931363/brand-map/mohawk/Mohawk-UltimateFlex-Logo.webp",
            "desc" => "This flexible vinyl is 100% waterproof and features EasyClean® stain protection. It withstands heavy rolling loads and high foot traffic and offers full glue-down, perimeter-glue, or loose-lay installation methods for easy and inexpensive individual plank replacement."
        ),
        array(
            "cde" => "mohawk_tecwood",
            "name" => "TecWood",
            "slug" => "tecwood",
            "img" => "https://mobilemarketing-res.cloudinary.com/image/upload/v1739455821/brand-map/mohawk/mhk_tecwood_color_tagline_logo1.webp",
            "desc" => "Like every tree is unique, so is every TecWood™ floor. Each plank carries the defining features that make TecWood engineered hardwood a natural masterpiece."
        )
    );
 $content = '<div class="brands-logos-wrap">';

  foreach($brands_list as $branditem){   

    //write_log($branditem['cde']);
    //write_log($cde_brands);
   
    if (in_array($branditem['cde'],$cde_brands)) {
     

        $content .= '<div class="brand-logo-single '.$branditem['cde'].'">
                        <div class="logo-image-wrap">
                            <img decoding="async" class="uabb-photo-img" src="'.$branditem['img'].'" alt="" title="" itemprop="image">
                        </div>
                        <div class="brand-logo-title-wrap">
                            <h3 class="brand-logo-title">'.$branditem['name'].'</h3>
                        </div>			
                        <div class="brand-link-text-wrap">
                            <div class="brand-text">
                            <p>'.$branditem['desc'].'</p>
                            </div>
                            <a href="'.$parent_url.'brands/mohawk/'.$branditem['slug'].'/" target="_self" class="fl-button">SHOP BRAND</a>			
                        </div>
                </div>';
    }
  }

$content .= '</div>';

return $content;
}
add_shortcode('mohawkbrandlist', 'mohawkbrandlist_list_shortcode');

//hook to set form for landing pages
add_action('fl_builder_before_render_module', function ($module) {

    global $post; 
    $form_mapping = array("mohawkBrandPage","pergoBrandPage","karastanBrandPage","godfreyBrandPage","shawBrandPage","mohawk_everstrand","mohawk_smartstrand","mohawk_petpremier","mohawk_revwood","mohawk_solidtech","mohawk_solidtech_r",       "mohawk_puretech","mohawk_ultimateflex","karastan_smartstrand","karastan_luxecraft","karastan_belleluxe",      "godfrey_hirst_Smartstrand","godfrey_hirst_wool","godfrey_hirst_everLux","pergo_elements","pergo_extreme",
    "anso_colorwall","pet_perfect","anderson_tuftex","shaw_grass","philadelphia_commercial","floorte","coretec");  

    if ($module->slug === 'uabb-gravity-form' && in_array($post->post_excerpt, $form_mapping)) {

        $form_id = get_gravity_form_id_by_title('Brand Form'); // Change form title here

       // //write_log('Change form title here');
      
        if ($form_id) {         
            
            $module->settings->form_id = $form_id;

            if ($post && $post->post_type === 'page') {               

                    $module->settings->form_id = $form_id;               
                
            }
           
        }
    }
   
}, 10);

// Function to get Gravity Form ID by Title
function get_gravity_form_id_by_title($form_title) {
    $forms = GFAPI::get_forms();
    foreach ($forms as $form) {
        if ($form['title'] === $form_title) {
            return $form['id'];
        }
    }
    return false;
}

function shortcode_one_time_cron_function() {
    //write_log('One-time cron job executed!');
    $website = json_decode(get_option('website_json'));
    $mohawkbrands = json_decode($website->options->mohawkSubBrands);

   // (in_array("mohawk_everstrand", $mohawkbrands)) ? update_option('shortcode_mohawk_everstrand', 'a:4:{s:8:\"category\";s:6:\"carpet\";s:5:\"brand\";s:6:\"Mohawk\";s:10:\"collection\";s:0:\"\";s:8:\"material\";s:10:\"Everstrand\";}') : update_option('shortcode_mohawk_everstrand', '');
}

// Schedule the one-time cron job if it hasn't been scheduled already
if (!wp_next_scheduled('shortcode_one_time_cron_hook')) {
    wp_schedule_single_event(time() + 10, 'shortcode_one_time_cron_hook'); // Runs in 60 seconds
}

// Hook the function to the scheduled event
add_action('shortcode_one_time_cron_hook', 'shortcode_one_time_cron_function');

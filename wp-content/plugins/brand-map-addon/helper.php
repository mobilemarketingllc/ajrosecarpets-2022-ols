<?php
// Callback function for the menu page
function featured_products_settings() {
    $website = json_decode(get_option('website_json'));
    $content ='<div id="wpcontent1">
                    <div class="wrap">
                        <div class="page-title-holder">
                            <h2>Featured Products Settings</h2>
                        </div>
                        <nav class="nav-tab-wrapper" style="margin-bottom: 20px;">
                            <a href="?page=brand-map-settings" class="nav-tab">Brand Map Setting</a>
                            <a href="?page=featured-products-settings" class="nav-tab nav-tab-active">Featured Products Setting</a>
                        </nav>';

    $content .= '<div class="brand-main-outer"> 
                    <div class="tab-content-title">
                        <h3><strong>Fetched Brand From CDE</strong></h3>
                    </div>';
            if (isset($_GET['success']) && $_GET['success'] == '1') { ?>
                <div class="updated"><p>Form submitted successfully!</p></div>
    <?php  }             
    $content .= '<div class="brand-main-inner-row brands-edit-row">';

    
    if ($website->options->mohawkBrandPage == 'true') {
        $content .= '<div class="brand-list-holder">';
            $content .= '<h4>
                <img src="https://mobilemarketing-res.cloudinary.com/image/upload/v1656011761/BrandLogos/mohawk.jpg" alt="Mohawk" title="Mohawk" />
                <strong>Mohawk Brands</strong>
            </h4>';
            $content .= '<ul>';
            $mohawkbrands = json_decode($website->options->mohawkSubBrands);

            foreach ($mohawkbrands as $mohawk_brand) {

                $style ='';
                $saved_value =  get_option('shortcode_'.$mohawk_brand, array());   

                write_log($saved_value);
                
                if (count($saved_value) === 0) { $check = ''; }else{ $check = 'checked'; /*$style='style="display:block"';*/}

                $content .= '<li data-cat="' . $mohawk_brand . '" class="toggle-checkbox" data-target="' . $mohawk_brand . '"><div class="label-holder"><input type="checkbox" name="' . $mohawk_brand . '" id="' . $mohawk_brand . '-id" value="' . $mohawk_brand . '" '.$check.' /><label for="' . $mohawk_brand . '-id">' . $mohawk_brand . '</label></div>
                <div class="form-container" id="' . $mohawk_brand . '">
                    <form method="POST" action="'.admin_url('admin-post.php').'" >
                    '.wp_nonce_field('my_custom_action', 'my_custom_nonce').'
                    <label for="name">Category:</label>
                        <input type="text" name="category" value="'.$saved_value['category'].'" >
                        <label for="name">Brand:</label>
                        <input type="text" name="brand" value="'.$saved_value['brand'].'" >
                        <label for="collection">Collection:</label>
                        <input type="text" name="collection" value="'.$saved_value['collection'].'" >
                        <label for="material">Material:</label>
                        <input type="text" name="material" value="'.$saved_value['material'].'" >
                        <input type="hidden" name="brand_name" value="' . $mohawk_brand . '">
                        <input type="hidden" name="action" value="my_custom_form_action">
                        <button type="submit" name="submit" class="button button-primary button-hero">Submit</button>
                    </form>
                </div></li>';
            }
            $content .= '</ul>';
        $content .= '</div>';
    }


    if ($website->options->shawBrandPage == 'true') {
        $content .= '<div class="brand-list-holder">';
            $content .= '<h4>
                <img src="https://mobilemarketing-res.cloudinary.com/image/upload/v1656011761/BrandLogos/mohawk.jpg" alt="Shaw" title="Shaw" />
                <strong>Shaw Brands</strong>
            </h4>';
            $content .= '<ul>';
            $shawbrands = json_decode($website->options->shawSubBrands);

            foreach ($shawbrands as $shaw_brand) {                
                $style ='';
                $saved_value =  get_option('shortcode_'.$shaw_brand, array());   

                write_log($saved_value);
                
                if (count($saved_value) === 0) { $check = ''; }else{ $check = 'checked'; /*$style='style="display:block"';*/}

                $content .= '<li data-cat="' . $shaw_brand . '" class="toggle-checkbox" data-target="' . $shaw_brand . '"><div class="label-holder"><input type="checkbox" name="' . $shaw_brand . '" value="' . $shaw_brand . '" '.$check.' >' . $shaw_brand . '</div>
                <div class="form-container" id="' . $shaw_brand . '">
                    <form method="POST" action="'.admin_url('admin-post.php').'" >
                    '.wp_nonce_field('my_custom_action', 'my_custom_nonce').'
                    <label for="name">Category:</label>
                        <input type="text" name="category" value="'.$saved_value['category'].'" >
                        <label for="name">Brand:</label>
                        <input type="text" name="brand" value="'.$saved_value['brand'].'" >
                        <label for="collection">Collection:</label>
                        <input type="text" name="collection" value="'.$saved_value['collection'].'" >
                        <label for="material">Material:</label>
                        <input type="text" name="material" value="'.$saved_value['material'].'" >
                        <input type="hidden" name="brand_name" value="' . $shaw_brand . '">
                        <input type="hidden" name="action" value="my_custom_form_action">
                        <button type="submit" name="submit" class="button button-primary button-hero">Submit</button>
                    </form>
                </div></li>';
            }
            $content .= '</ul>';
        $content .= '</div>';
    }

    if ($website->options->karastanBrandPage == 'true') {
        $content .= '<div class="brand-list-holder">';
            $content .= '<h4>
                <img src="https://mobilemarketing-res.cloudinary.com/image/upload/v1656011759/BrandLogos/karastan-logo.jpg" alt="Karastan" title="Karastan" />
                <strong>Karastan Brands</strong>
            </h4>';
            $content .= '<ul>';

            $karastanSubBrands = json_decode($website->options->karastanSubBrands);

            foreach ($karastanSubBrands as $kara_brand) {

            
                $style ='';
                $saved_value =  get_option('shortcode_'.$kara_brand, array());   

                write_log($saved_value);
                
                if (count($saved_value) === 0) { $check = ''; }else{ $check = 'checked'; $style='style="display:block"';}

                $content .= '<li data-cat="' . $kara_brand . '" class="toggle-checkbox" data-target="' . $kara_brand . '"><div class="label-holder"><input type="checkbox" name="' . $kara_brand . '" value="' . $kara_brand . '" '.$check.' />' . $kara_brand . '</div>
                <div class="form-container" id="' . $kara_brand . '">
                    <form method="POST" action="'.admin_url('admin-post.php').'" >
                    '.wp_nonce_field('my_custom_action', 'my_custom_nonce').'
                    <label for="name">Category:</label>
                        <input type="text" name="category" value="'.$saved_value['category'].'" >
                        <label for="name">Brand:</label>
                        <input type="text" name="brand" value="'.$saved_value['brand'].'" >
                        <label for="collection">Collection:</label>
                        <input type="text" name="collection" value="'.$saved_value['collection'].'" >
                        <label for="material">Material:</label>
                        <input type="text" name="material" value="'.$saved_value['material'].'" >
                        <input type="hidden" name="brand_name" value="' . $kara_brand . '">
                        <input type="hidden" name="action" value="my_custom_form_action">
                        <button type="submit" name="submit" class="button button-primary button-hero">Submit</button>
                    </form>
                </div></li>';
            }
            $content .= '</ul>';
        $content .= '</div>';
    }

    if ($website->options->godfreyBrandPage == 'true') {
        $content .= '<div class="brand-list-holder">';
            $content .= '<h4>
                <img src="https://mobilemarketing-res.cloudinary.com/image/upload/v1656011756/BrandLogos/godfrey-hirst.jpg" alt="Godfrey" title="Godfrey" />
                <strong>Godfrey Brands</strong>
            </h4>';
            $content .= '<ul>';

            $godfreySubBrands = json_decode($website->options->godfreySubBrands);

            $style ='';
            foreach ($godfreySubBrands as $god_brand) {

                
                $saved_value =  get_option('shortcode_'.$god_brand, array());   

                write_log($saved_value);
                
                if (count($saved_value) === 0) { $check = ''; }else{ $check = 'checked'; $style='style="display:block"';}

                $content .= '<li data-target="' . $god_brand . '" class="toggle-checkbox" data-target="' . $god_brand . '"><div class="label-holder"><input type="checkbox" name="' . $god_brand . '" value="' . $god_brand . '" '.$check.' />' . $god_brand . '</div>
                <div class="form-container" id="' . $god_brand . '">
                    <form method="POST" action="'.admin_url('admin-post.php').'" >
                    '.wp_nonce_field('my_custom_action', 'my_custom_nonce').'
                    <label for="name">Category:</label>
                        <input type="text" name="category" value="'.$saved_value['category'].'" >
                        <label for="name">Brand:</label>
                        <input type="text" name="brand" value="'.$saved_value['brand'].'" >
                        <label for="collection">Collection:</label>
                        <input type="text" name="collection" value="'.$saved_value['collection'].'" >
                        <label for="material">Material:</label>
                        <input type="text" name="material" value="'.$saved_value['material'].'" >
                        <input type="hidden" name="brand_name" value="' . $god_brand . '">
                        <input type="hidden" name="action" value="my_custom_form_action">
                        <button type="submit" name="submit" class="button button-primary button-hero">Submit</button>
                    </form>
                </div></li>';
            }
            $content .= '</ul>';
        $content .= '</div>';
    }

    if ($website->options->pergoBrandPage == 'true') {
        $content .= '<div class="brand-list-holder">';
            $content .= '<h4>
                <img src="https://mobilemarketing-res.cloudinary.com/image/upload/v1739509224/brand-map/mohawk/Pergo_logo_1.webp" alt="Pergo" title="Pergo" />
                <strong>Pergo Brands</strong></h4>';
            $content .= '<ul>';

            $pergoSubBrands = json_decode($website->options->pergoSubBrands);

        
            foreach ($pergoSubBrands as $pergo_brand) {

                $style ='';
                $saved_value =  get_option('shortcode_'.$pergo_brand, array());   

                write_log($saved_value);
                
                if (count($saved_value) === 0) { $check = ''; }else{ $check = 'checked'; $style='style="display:block"';}

                $content .= '<li data-cat="' . $pergo_brand . '" class="toggle-checkbox" data-target="' . $pergo_brand . '"><div class="label-holder"><input type="checkbox" name="' . $pergo_brand . '" value="' . $pergo_brand . '" '.$check.' />' . $pergo_brand . '</div>
                <div class="form-container" id="' . $pergo_brand . '">
                    <form method="POST" action="'.admin_url('admin-post.php').'" >
                    '.wp_nonce_field('my_custom_action', 'my_custom_nonce').'
                    <label for="name">Category:</label>
                        <input type="text" name="category" value="'.$saved_value['category'].'" >
                        <label for="name">Brand:</label>
                        <input type="text" name="brand" value="'.$saved_value['brand'].'" >
                        <label for="collection">Collection:</label>
                        <input type="text" name="collection" value="'.$saved_value['collection'].'" >
                        <label for="material">Material:</label>
                        <input type="text" name="material" value="'.$saved_value['material'].'" >
                        <input type="hidden" name="brand_name" value="' . $pergo_brand . '">
                        <input type="hidden" name="action" value="my_custom_form_action">
                        <button type="submit" name="submit" class="button button-primary button-hero">Submit</button>
                    </form>
                </div></li>';
            }
            $content .= '</ul>';
        $content .= '</div>';
    }

    $content .= '</div></div><script>
        document.addEventListener("click", function(event) {
            if (event.target.classList.contains("toggle-checkbox")) {
                let targetFormId = event.target.getAttribute("data-target");
                let formContainer = document.getElementById(targetFormId);
                
                if (formContainer) {
                    formContainer.classList.toggle("show", event.target.checked);
                    event.target.classList.toggle("active");
                }
            }
        });
    </script>';
               echo $content;
}

function featured_brand_slider_js()
{
?>
    <script>
        document.addEventListener('click', function(event) {
            if (event.target.classList.contains('toggle-checkbox')) {
                let targetFormId = event.target.getAttribute('data-target');
                let formContainer = document.getElementById(targetFormId);
                
                if (formContainer) {
                    formContainer.classList.toggle('show', event.target.checked);
                    event.target.classList.toggle("active");
                }
            }
        });
    </script>
<?php
}
add_action('wp_footer', 'featured_brand_slider_js');



// Handle form submission
add_action('admin_post_my_custom_form_action', 'my_custom_form_handler');

function my_custom_form_handler() {

    write_log('herere');
   
    if (!isset($_POST['my_custom_nonce']) || !wp_verify_nonce($_POST['my_custom_nonce'], 'my_custom_action')) {
        wp_die('Security check failed');
    }

    // Check user permission
    if (!current_user_can('manage_options')) {
        wp_die('You do not have permission to perform this action');
    }

    write_log('fsdfsdf');
    // Get form input
    $category = isset($_POST['category']) ? sanitize_text_field($_POST['category']) : '';
    $brand_name = isset($_POST['brand_name']) ? sanitize_text_field($_POST['brand_name']) : '';
    $brand = isset($_POST['brand']) ? sanitize_text_field($_POST['brand']) : '';
    $collection = isset($_POST['collection']) ? sanitize_text_field($_POST['collection']) : '';
    $material = isset($_POST['material']) ? sanitize_text_field($_POST['material']) : '';

    $values = array(
        'category' => $category,
        'brand' => $brand,
        'collection' => $collection,
        'material' => $material
    );

    // Save to database
    update_option('shortcode_'.$brand_name, $values);

    // Redirect back with success message
    wp_redirect(admin_url('admin.php?page=featured-products-settings&success=1'));
    exit;
}

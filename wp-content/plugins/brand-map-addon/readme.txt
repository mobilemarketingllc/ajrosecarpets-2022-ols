=== Brand Mapping ===
Contributors: mmteam
Tags: Brand mapping
Requires at least: 4.6
Tested up to: 8.0
Stable tag: 1.0.3
Requires PHP: 8
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Changelog ==
1.0.3 =

* Landing pages template integration.

1.0.2 =

* Landing pages and Shortcode integration.

1.0.1 =

* Initial Launch



== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
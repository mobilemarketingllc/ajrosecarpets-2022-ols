<?php

/** 
 * Plugin Name: Flooring Finder Addon
 * Text Domain: flooring-finder-addon 
 * Description: Flooring Finder Functionlity Add-Ons For MM Retailer Plugin by Mobile Marketing
 * Author: Mobile Marketing
 * Version: 1.0.0 
 * Author URI: https://mobile-marketing.agency/
 */
if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly 
}
require_once(ABSPATH . "wp-includes/pluggable.php");
require_once(ABSPATH . "/wp-load.php");

if (! defined('MY_PLUGIN_DIR')) {
    define('MY_PLUGIN_DIR', plugin_dir_path(__FILE__));
}
$plugin_dir_url = str_replace('/var/www/html', '', MY_PLUGIN_DIR);
define('plugin_dirs', $plugin_dir_url);

// Include Sripts / CSS here
function enqueue_ff_script()
{
    wp_enqueue_script('flooringfinder', plugin_dirs . 'js/flooringfinder.js', array('jquery'), 1.3, true);
    wp_enqueue_style('flooringfinder', plugin_dirs . 'css/flooringfinder.css', false, '1.0', 'all'); // Inside a plugin
}
add_action('wp_enqueue_scripts', 'enqueue_ff_script');

// Admin Menu Creation
function add_admin_ff_menu()
{
    add_menu_page(
        'Flooring Finder', // page title 
        'Flooring Finder Settings', // menu title
        'manage_options', // capability
        'flooring-finder-addon',  // menu-slug
        'flooring_finder_addon_fn',   // function that will render its output
        //plugins_url('/img/ip-icon.png', __FILE__)   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );
}
add_action('admin_menu', 'add_admin_ff_menu');

function flooring_finder_addon_fn()
{
    echo "test";
}

function copy_template_folder_theme()
{
    //Flooring Finder - Plugin Template folder to Child theme
    $plugin_dir_ff_template = plugin_dir_path(__FILE__) . 'templates/flooring-finder.php';
    $theme_dir_ff_template = get_stylesheet_directory() . '/flooring-finder.php';

    //Flooring Finder - if error was found while copy
    if (!copy($plugin_dir_ff_template, $theme_dir_ff_template)) {
        echo "failed to copy $plugin_dir_ff_template to $theme_dir_ff_template...\n";
    }

    //Create a Flooring Finder Page with Template Assign 
    $flooirng_page_create = array('Flooring Finder');
    foreach ($flooirng_page_create as $ffpage) {
        $page_exists = get_page_by_title($ffpage);

        if (!$page_exists) {
            // Create the post
            $ffpage_id = wp_insert_post(array(
                'post_title'     => $ffpage,
                'post_type'      => 'page',
                'post_status'    => 'publish',
                'page_template'  => 'flooring-finder.php'
            ));
        }
    }
}
add_action('wp_loaded', 'copy_template_folder_theme');

// Final Result - Flooring Finders Products  
add_action('wp_ajax_nopriv_check_out_flooring', 'check_out_flooring');
add_action('wp_ajax_check_out_flooring', 'check_out_flooring');

function overwritefolder()
{
    global $module_update;
    // echo $wp_root_path = str_replace('/wp-content/themes', '', get_theme_root());echo "<br>";

    $theme_directory = get_template_directory() . "-child";
    ///var/www/html/wp-content/themes/astra-child
    $plugin_directory = plugin_dir_path(__FILE__); ///var/www/html/wp-content/plugins/grand-child/

    //check child directory is created for not

    if (is_dir($theme_directory)) {
        // Check templates modules is exist for not        '

        if (!is_dir($theme_directory . "/templates")) {
            var_dump(xcopy($plugin_directory . "templates", $theme_directory . "/templates"));
        }
    }
}

add_action('admin_init', 'overwritefolder');

function check_out_flooring()
{
    global $wpdb;
    $table_posts = $wpdb->prefix . 'posts';
    $arg = array();
    if ($_POST['product'] == 'carpeting') {
        $posttype = 'carpet';
    } elseif ($_POST['product'] == 'hardwood_catalog,laminate_catalog,luxury_vinyl_tile') {
        $posttype = 'hardwood,laminate,lvt';
    } elseif ($_POST['product'] == 'tile_catalog' || $_POST['product'] == 'tile') {
        $posttype = 'tile';
    }
    $site_url = get_site_url();
    $find_posts = array();

    $parameters =  explode(",", $_POST['imp_thing']);

    foreach ($parameters as $para) {
        if ($para == 'hypoallergenic') {
            $arg = 'category=' . $posttype;
        } elseif ($para == 'diy_friendly') {
            $arg = 'category=' . $posttype . '&installation_facet=locking';
        } elseif ($para == 'aesthetic_beauty') {
            $arg = 'category=' . $posttype . '&brand=anderson tuftex,karastan';
        } elseif ($para == 'eco_friendly') {
            $arg = 'category=' . $posttype . '&lifestyle=eco';
        } elseif ($para == 'pet_friendly') {
            $arg = 'category=' . $posttype . '&lifestyle=pet';
        } elseif ($para == 'easy_to_clean') {
            $arg = 'category=' . $posttype . '&fiber=smartstrand,r2x';
        } elseif ($para == 'durability') {
            $arg = 'category=' . $posttype . '&fiber=smartstrand,r2x,stainmaster';
        } elseif ($para == '') {
            $arg = 'category=all';
        } elseif ($para == 'stain_resistant') {
            $arg = 'category=' . $posttype . '&lifestyle=stain resistant&fiber=r2x,stainmaster,smartstrand&collection=bellera';
        } elseif ($para == 'budget_friendly') {
            $arg = 'category=' . $posttype . '&fiber=smartstrand,stainmaster,bellera,pet,r2x';
        } elseif ($para == 'color') {
            $arg = 'category=' . $posttype;
        }


        $api_url = $site_url . '/wp-json/products/v1/list?group_by=color_facet&limit=50&' . $arg;
        $response = wp_remote_get($api_url);

        if (is_wp_error($response)) {
            error_log('API Request Failed: ' . $response->get_error_message());
            continue;
        } else {
            $content = wp_remote_retrieve_body($response);
            $product_list = json_decode($content, true);
            $products = isset($product_list['products_group_by']) ? $product_list['products_group_by'] : array();
            $find_posts = array_merge($find_posts, $products);
        }
    }

    if (!$find_posts) {
        $api_url = $site_url . '/wp-json/products/v1/list?group_by=color_facet&category=' . $posttype . '&limit=50';
        $response = wp_remote_get($api_url);
        if (is_wp_error($response)) {
            error_log('API Request Failed: ' . $response->get_error_message());
        } else {
            $content = wp_remote_retrieve_body($response);
            $product_list = json_decode($content, true);
            $find_posts = $product_list['products_group_by'];
        }
    }

    if (count($find_posts) > 0) {
        $content = "";
        foreach ($find_posts as $post) {
            $title = trim($post[0]['name']) . " " . trim($post[0]['sku']);

            $page = $wpdb->get_row($wpdb->prepare("select ID,guid from $table_posts where post_title='" . $title . "'"));

            $page_id = $page->ID;
            if (!$page_id) {
                continue;
            }
            $image =
                "https://mm-media-res.cloudinary.com/image/fetch/h_400,w_400,c_limit/https://" . trim($post[0]['swatch']);

            $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                <div class="card">
                    <div class="productImgWrap">
                        <a href="' . get_permalink($page_id) . '"><img src="' . $image . '" alt="Product Image" style="width:100%!important"></a>
                    </div>
                    <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                    <div class="pro_title">
                        <h4>' . $post[0]['collection'] . '</h4> 
                        <h3>' . $post[0]['color'] . '</h3>
                        <p class="brand"><small><i>' . $post[0]['brand'] . '</i></small></p>
                        <a class="prodLink" href="' . get_permalink($page_id) . '">See info</a>
                    </div>
                    
                </div>
            </div>';
        }

        $content .= '<div class="text-center buttonWrap" data-search="' . $_POST['imp_thing'] . '">
            <a class="button" href="javascript:void(0)">view all results</a>
            <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
        </div>';
    }
    echo $content;
    wp_die();
}

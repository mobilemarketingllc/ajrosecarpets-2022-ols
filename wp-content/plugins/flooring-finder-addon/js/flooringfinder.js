//floor finder js start here
addEvent();
const data = {};

function addEvent(e) {
    let getActive = document.querySelector(".flooringFinder > section.active");
    if (getActive) {
        let getCards = getActive.querySelectorAll(".card:not(.deactivate)");
        for (let i = 0; i < getCards.length; i++) {
            getCards[i].addEventListener("click", showNext);
        }
    }
}

function addEventToNextBtn(e) {
    let getActive = document.querySelector(".flooringFinder section.active .next > a");
    getActive.addEventListener("click", showNext);

}

bootstrap_alert = function() {}
bootstrap_alert.warning = function(message) {
    $('.flooringFinder section.active .next #alert_placeholder').html('<div class="alert alert-danger" role="alert">' + message + '</div>')
}

function showNext(e) {

   
    
    let getSec = document.querySelector(".flooringFinder section.active");

    if (getSec) {
        let getIndex = getSec.getAttribute("data-index-value");
        
        if (getIndex <= 6 && getIndex > 1 && getSec.querySelectorAll("input:checked").length < 1) {
            return bootstrap_alert.warning('Please Select Any One of Above');
        } else if (getIndex <= 6 && getIndex > 1) {
            let inputData = getSec.querySelectorAll("input:checked");
            let gettype = getSec.getAttribute("data-section");
            if (getSec.querySelectorAll("input:checked").length > 1) {
                let arr = [];
                for (val of inputData) {
                    arr.push(val.value);
                }
                data[gettype] = arr;
            } else {
                data[gettype] = inputData[0].value;
            }

            $('html, body').animate({
                scrollTop: $("#FlooringFinder").offset().top
            }, 200);
        } else if (getIndex == 1) {
            if (e.currentTarget.getAttribute("data-look") === 'carpet') {
                let card = [`.card[data-room="bathroom"]`, `.card[data-room="kitchen"]`, `.card[data-room="laundryRoom"]`, `.card[data-color-carpet="reds"]`, `.card[data-color-carpet="violets"]`];
                let cards = getElements(card);
                deactivateCard(cards);

                let getActive = document.querySelectorAll(".flooringFinder .typeOfCarpet");
                getActive.forEach(function(item) {
                    item.classList.remove('hidden');
                    item.classList.add('show');
                });
                data['typeOfLook'] = 'carpeting';

            } else if (e.currentTarget.getAttribute("data-look") === 'woodLook') {
                let getActive = document.querySelectorAll(".flooringFinder .typeOfWood");
                getActive.forEach(function(item) {
                    item.classList.remove('hidden');
                    item.classList.add('show');
                });
                data['typeOfLook'] = ['hardwood_catalog', 'laminate_catalog', 'luxury_vinyl_tile'];

            } else if (e.currentTarget.getAttribute("data-look") === 'tileOrStone') {
                let getActive = document.querySelectorAll(".flooringFinder .typeOfTile");
                getActive.forEach(function(item) {
                    item.classList.remove('hidden');
                    item.classList.add('show');
                });
                data['typeOfLook'] = 'tile';
            }
            $('html, body').animate({
                scrollTop: $("#FlooringFinder").offset().top
            }, 200);

        }
        if (getIndex <= 6) {
            // alert(getIndex);
            let nextSection = document.querySelector(`section[data-index-value='${1 + + getIndex}']`);
            let getActive = document.querySelector(".flooringFinder section.active");
            getActive.classList.remove('active');
            if (getIndex < 6) {
                nextSection.classList.add('active');
                nextSection.classList.contains('multiselect') ? multiselect() : singleselect();
                getIndex < 5 ? addEventToNextBtn() : console.log('not applying');

            } else {
                document.querySelector(".resultWrap").classList.add('active');
                //console.log(data);
            }
            $('html, body').animate({
                scrollTop: $("#FlooringFinder").offset().top
            }, 200);
        }
    } else {
        alert('somthing wrong');
    }


    function getElements(ele) {
        let arr = [];
        for (let i = 0; i < ele.length; i++) {
            arr.push(document.querySelector(ele[i]));
        }
        return arr;
    }

    function deactivateCard(cards) {
        cards.forEach(function(item) {
            item.classList.add('deactivate');
        });
    }

}

function multiselect() {
    let getActive = document.querySelectorAll(".flooringFinder section.active.multiselect");
    let getCards = getActive[0].querySelectorAll(".card:not(.deactivate)");
    for (let i = 0; i < getCards.length; i++) {
        getCards[i].addEventListener("click", function() {
            let getChec = getCards[i].querySelector("input[type=checkbox]");
            if (getChec.checked) {
                getCards[i].classList.remove('shadow');
                getChec.checked = false;
            } else {
                getCards[i].classList.add('shadow');
                getChec.checked = true;
            }
        });
    }
}

function singleselect() {
    let getActive = document.querySelector(".flooringFinder section.active.singleselect");

    let getCards = getActive.querySelectorAll(".card:not(.deactivate)");
    for (let i = 0; i < getCards.length; i++) {
        getCards[i].addEventListener("click", function() {

            let getChec = getCards[i].querySelector("input[type=checkbox]");
            if (getChec.checked) {
                getCards[i].classList.remove('shadow');
                getChec.checked = false;
            } else {
                let chec = getActive.querySelectorAll("input:checked");
                if (chec.length < 1) {
                    getCards[i].classList.add('shadow');
                    getChec.checked = true;
                } else if (chec.length === 1) {
                    chec[0].checked = false;
                    chec[0].parentElement.classList.remove('shadow');
                    getCards[i].classList.add('shadow');
                    getChec.checked = true;
                }
            }
        });
    }
}

    /*store location end*/

    function loadMoreData() {
        let allPr = document.querySelectorAll(".resultWrap.active .flooringFinderajax > .singlePro");
        let count = 0;
        jQuery(allPr).each(function(index, ele) {
            count++;
            count > 5 ? jQuery(ele).css("display", "none") : jQuery(ele).css("display", "block");

        });


        document.querySelector(".resultWrap.active .flooringFinderajax > .buttonWrap > .button").addEventListener("click", function(e) {
            let allPr = document.querySelectorAll(".resultWrap.active .flooringFinderajax > .singlePro");
            jQuery(allPr).each(function(index, ele) {
                jQuery(ele).css("display", "block");
            });
            jQuery(".resultWrap.active .flooringFinderajax > .buttonWrap > .button").css("display", "none");
        });



    }

// Get flooring finder data

jQuery('.flooringFinder section .getProData > a').on('click', function() {
    showNext();
    var mySelectedData = data;

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=check_out_flooring&product=' + mySelectedData.typeOfLook + '&style=' + mySelectedData.typeOf + '&design_style=' + mySelectedData.designStyle + '&room=' + mySelectedData.room + '&imp_thing=' + mySelectedData.impThing + '&color=' + mySelectedData.color,
        success: function(data) {
            jQuery(".flooringFinderajax ").html(data);
            if (data) {
                loadMoreData();
            }
        }

    });

});


const target = jQuery("#FlooringFinder");
if (target.length) {
    jQuery('html, body').animate({
        scrollTop: target.offset().top
    }, 200);
}

//floor finder js End here
(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
	/* VARIABLES DEFINED */
	//$gfb_paynm_submit = $('#gfb_paynm_submit'),
		//$gfb_paynm = $('#gfb_paynm'),
	var	$customer_filter_payment = $('.gfb-filter-customer-payment'),
		$gfb_paytrans_submit = $('#gfb_paytrans_submit'),
		$gfb_paytrans = $('#gfb_paytrans'),
		$appointment_datepicker = $('.appointment_datepicker');
		
		/* Appointment Filter Datepicker */
		$appointment_datepicker.datepicker({
			dateFormat: 'dd MM yy',
		});

		var availibility = $( '#gfb_calendar_availability' ).val();
		availibility_checker( availibility );


		$( '#gfb_calendar_availability' ).on( 'change', function() {
			var availibility = $( this ).val();
			availibility_checker( availibility );
		});

		function availibility_checker( val ) {
			if ( val == 'global' ) {
				$( '.global_calendar_settings_tab' ).css('display', 'block');
			} else {
				$( '.global_calendar_settings_tab' ).css('display', 'none');
			}
		}
		
		/* Do not allow user to enter special characters */
		$document.on("keypress", ".notallowspecial", function(e){
			
			var specialKeys = new Array();
			specialKeys.push(8, 9, 46, 35, 36, 37, 39, 20);
		 
			var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ( keyCode == 45 || keyCode == 46 || keyCode == 32 || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            $(".notallowspecial").append('<span class="error">Special characters are not allowed</span>');
            return ret;
		});
		
		/* Do not allow user to enter special characters & alphabets */
		$document.on("keypress", ".notallowspecialalpha", function(e){
			
			var specialKeys = new Array();
			specialKeys.push(8, 9, 46, 35, 36, 37, 39, 20);
		 
			var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
			//console.log(keyCode);
            var ret = ( keyCode == 190 || keyCode == 46 || keyCode == 45 || keyCode == 32 || (keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) );
            $(".notallowspecialalpha").append('<span class="error">Special characters are not allowed</span>');
            return ret;
		});
		
		
		/* Change event for timepicker */
		$document.on('change', '.timespicker', function(e){
			
			var startTimeVal = $(".start_time_picker").val();
			var endTimeVal = $(".end_time_picker").val();
			
			if(startTimeVal != "" && endTimeVal != "")
			{
				var startArray = startTimeVal.split(':'); 
				var endArray = endTimeVal.split(':'); 
				var startSeconds = startArray[0] * 60 * 60 + startArray[1] * 60;
				var endSeconds = endArray[0] * 60 * 60 + endArray[1] * 60;
				
				if(startSeconds == endSeconds)
				{
					swal({ title:"Sorry! Start Time and End Time cannotbe same", type:"error" });
					setTimeout(function(){ $(e.target).val('').trigger('click'); }, 500);
					return false;
				}
				else if(startSeconds>endSeconds)
				{
					swal({ title:"Sorry! Start Time can not be greater than End Time", type:"error" });
					setTimeout(function(){ $(e.target).val('').trigger('click'); }, 500);
					return false;
				}
				
			}
			
			e.preventDefault();	
		});
		
		/* Customer Filter jQuery */
		$customer_filter_payment.on('change', function(){
			var paynm = $(this).val();
			if( paynm != '' ){
				if( paynm == '*' ){
					document.location.href = 'admin.php?page=gravity-form-booking-payment';    
				}
				else {
					document.location.href = 'admin.php?page=gravity-form-booking-payment&paynm-filter="'+paynm+'"'; 
				}
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-payment';     
			}
		});
		
		/* Transaction Id filter */
		$gfb_paytrans_submit.on("click", function(e){
			
			var paytrans = $gfb_paytrans.val();
			
			if( paytrans != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-payment&paytrans-filter='+paytrans;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-payment';    
			}
			
			e.preventDefault();
		});
				
	});
	
	
}(jQuery));
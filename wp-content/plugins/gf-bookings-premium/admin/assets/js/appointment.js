(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
	var $appointmentModal = $(".add-appointment-modal"),
		$appointmentDatepicker = $('#appointment_date'),
		$category_id = $('#appointmentcategory'),
		$service = $('#service'),
		$staff = $('#staff'),
		$new_customer = $('#new-customer'),
		$customer = $('#customer'),
		$customer_name = $('#customer_name'),
		$customer_email = $('#customer_email'),
		$customer_contact = $('#customer_contact'),
		$customer_form = $('.customer-form'),
		$exist_customer_form = $('.exist-customer-form'),
		$search_datepicker = $('.search-datepicker'),
		$search_appointments = $("#search_appointments"),
		$appointment_form = $('#appointment_form'),
		$time_slot = $('#time_slot'),
		$customer_filter = $('.gfb-filter-customer'),
		$service_filter = $('.gfb-filter-service'),
		$staff_filter = $('.gfb-filter-staff'),
		$status_filter = $('.gfb-filter-status'),
		$gfb_appointment_date_submit = $('#gfb_appointment_date_submit'),
		$gfb_appointment_start_date = $('#gfb_appointment_start_date'),
		$gfb_appointment_end_date = $('#gfb_appointment_end_date'),
		$gfb_appointment_booking_ref_num =$('#gfb_appointment_booking_ref_num');
		$gfb_delete_appointment = $(".gfb_delete_appointment"),
		$appointment_cancel = $(".appointment-cancel"),
		$appointment_visit = $(".appointment-visit"),
		$appointment_conformed = $(".appointment-conformed"),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_maintab_content = $(".gfb_maintab-content"),
		$pop_up_img = $(".popup-block-main-body");
		$gfb_appointment_bookingnum_submit=$('#gfb_appointment_bookingnum_submit');
		$gfb_appointment_bookingnum_form=$('#gfb_appointment_bookingnum_form');


		// change of payment status
		$('.payment_status_change').on( 'change', function() {
			var current = $(this);

			swal({
			  	title: "Are you sure you want to change the payment status?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel) {
				
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel ) {
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				} else {

					var payment_status = current.val();
					var payment_id = current.siblings('.payment_id').val();								
					
					var dataVal = {
						action 		   : 'gfb_payment_status_change',
						payment_id     : payment_id,
						payment_status : payment_status
					};
					
					jQuery.post(
						
						js_appointment_js.ajaxurl, dataVal, function(response) {
													
							console.log( response );
							if ( response > 0 ) {									
								location.reload();								
							} else {
								swal({ title:'Status not changed. Please try again!', type:"error" });	
							}

							$gfb_maintab_content.removeClass("loading-block");					
						}
					);				
				}
				
			});	
			
			return false;
		});
		
		/* Add class to tr */
		$("td.appointment_date").parents("tr").addClass("delete-appointment-fade");
		
		/* Appointment Form Magnific Popup */
		$appointmentModal.magnificPopup({
			type: "inline",
			preloader: true,
		});
		
		/* hide new & existing customer form */
		$customer_form.hide();
		$exist_customer_form.hide();
		
		if( $new_customer.val() != '' ){
			if($new_customer.val()==1)
			{
				$customer_form.show();
				$exist_customer_form.hide();
				$customer.val('');
			}
			else if($new_customer.val()==2)
			{
				$customer_form.hide();
				$exist_customer_form.show();
			}	
		}
		
		/* Show new Customer input boxes */
		$new_customer.on('change',function(){
			
			if($(this).val()==1)
			{
				$customer_form.show();
				$exist_customer_form.hide();
				$customer.val('');
			}
			else if($(this).val()==2)
			{
				$customer_form.hide();
				$exist_customer_form.show();
			}
		});
		
		
		/* Holiday Array */
		var disableddates = $.parseJSON(js_appointment_js.holidayarr);
		
		/* Disable Holiday Dates */
		function gfbDisableHolidayDates(date) {
			
			var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];			
			
			dmy = ("0" + date.getDate()).slice(-2) + " " + (monthNames[date.getMonth()]) + ", " + date.getFullYear();
			
			// We will use the noWeekends function
			if ($.inArray(dmy, disableddates) == -1) {
				valdate = [true, ""];
			} 
			else {
				valdate = [false, ""];
			}
			
			return valdate;
		}
		
		/* Appointment Form Datepicker */
		$appointmentDatepicker.datepicker({
			dateFormat: 'dd MM, yy',
			minDate: js_appointment_js.mindate,
			maxDate: js_appointment_js.maxdate,
			beforeShowDay: gfbDisableHolidayDates,
			onSelect: function(dateText) {
				$category_id.prop('selectedIndex',0);
				$service.prop('selectedIndex',0);
				$staff.prop('selectedIndex',0);
				$time_slot.prop('selectedIndex',0);
				$customer_name.val('');
				$customer_email.val('');
				$customer_contact.val('');
				$customer.prop('selectedIndex',0);
			}
		}).datepicker('setDate', new Date(js_appointment_js.mindate));
		
		/* get service data */
		$category_id.on('change',function(e){
			var data = {
				action		: 'gfbListServicesByCategory',
				category_id : $(this).val(),
			};
			
			$service.before('<img src="'+js_appointment_js.adloader+'" alt="loading" class="inline-loader">');
			
			jQuery.post(
				js_appointment_js.ajaxurl, data, function(response) {					
					$service.html(response);
					$('.inline-loader').remove();
					$staff.empty();
					$staff.append('<option value="">--Select Staff--</option>');
					$time_slot.empty();
					$time_slot.append('<option value="">--Select Time Slot--</option>');
					$customer_name.val('');
					$customer_email.val('');
					$customer_contact.val('');
					$customer.prop('selectedIndex',0);	
				}
			);
			
		});
		
		/* get staff data */
		$service.on('change',function(e){
			
			var data = {
				action	   : 'gfbListStaff',
				service_id : $(this).val(),
			};
			
			$staff.before('<img src="'+js_appointment_js.adloader+'" alt="loading" class="inline-loader">');
			
			jQuery.post(
				js_appointment_js.ajaxurl, data, function(response) {
					$staff.html(response);	
					$('.inline-loader').remove();
					$time_slot.empty();
					$time_slot.append('<option value="">--Select Time Slot--</option>');
					$customer_name.val('');
					$customer_email.val('');
					$customer_contact.val('');
					$customer.prop('selectedIndex',0);												
				}
			);
			
		});
		
		/* get time slot data */
		$staff.on('change',function(e){
			
			var appointment_date = $('#appointment_date').val();
			
			$time_slot.before('<img src="'+js_appointment_js.adloader+'" alt="loading" class="inline-loader">');
			
			var data = {
				action			 : 'gfbGetStaffTimeSlot',
				staff_id    	 : $(this).val(),
				appointment_date : appointment_date
			};			

			jQuery.post(
				js_appointment_js.ajaxurl, data, function(response) {					
					$time_slot.html(response);
					$('.inline-loader').remove();
					$customer_name.val('');
					$customer_email.val('');
					$customer_contact.val('');
					$customer.prop('selectedIndex',0);	
				}
			);
			
		});
		
		
		//book an appointment
		$appointment_form.validate({	
			rules: {
				appointmentcategory : "required",
				service : "required",
				staff : "required",
				time_slot : "required",
				customer : {
					required: {
						depends: function(element) {
							if( $("#new-customer").val() == 2 ){
								return true;
							}
						}
					}
				},
				customer_name : {
					required: {
						depends: function(element) {
							if( $("#new-customer").val() == 1 ){
								return true;
							}
						}
					}
				},
				customer_email : {
					required:true,
					email:true
				},
				customer_contact : {
					required:true,
					digits:true,
					minlength:10,
					maxlength:15	
				}				
			},
			messages: {
				appointmentcategory : "Please select category",
				service : "Please select service",
				staff : "Please select staff",
				time_slot : "Please select appointment timing slot",
				customer : {
					required: "Please select customer name",
				},
				customer_name : {
					required: "Please enter person name",
				},
				customer_email : {
					required:"Please enter person email address",
					email:"Please enter person valid email address"
				},
				customer_contact : {
					required:"Please enter person contact number",
					digits:"Please enter only numbers"	,
					minlength:"You have to enter atleast 10 digits",
					maxlength:"You can enter atmost 15 digits"
				}	
			},
			submitHandler: function() {
				
				$pop_up_img.addClass("loading-block");
				
				if( $time_slot.val() == 'notimeslot' ) {
					swal({title : "Sorry! Please select another staff or date as currenting no slots are available" , type:"error"});	
				}
				else {		
				
					var form_data = new FormData($('form')[0]);
					form_data.append("action", "gfb_add_appointment"); 
							
					$.ajax({
						url : js_appointment_js.ajaxurl,
						type: "POST",
						data: form_data,				
						cache: false,
						contentType: false,
						processData: false,
						success:function(response){
							var obj=$.parseJSON(response);
							
							if (obj.msg != 'false') {	
								swal({ title:obj.text, type:"success" }, function(){
									window.location.replace(obj.url);
								});
							}	
							else {
								swal({ title:obj.text, type:"error" });	
							}
							$pop_up_img.removeClass("loading-block");
						}
					});
					
				}
				return false;
			}
		});
		
		/* Customer Filter jQuery */
		$customer_filter.on('change', function(){
			var catFilter = $(this).val();
			if( catFilter != '' ){
				if( catFilter == '*' ){
					document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
				}
				else {
					document.location.href = 'admin.php?page=gravity-form-booking-appointments&customer-filter="'+catFilter+'"';    
				}
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
			}
		});
		
		/* Service Filter jQuery */
		$service_filter.on('change', function(){
			var serFilter = $(this).val();
			if( serFilter != '' ){
				if( serFilter == '*' ){
					document.location.href = 'admin.php?page=gravity-form-booking-appointments';     
				}
				else {
					document.location.href = 'admin.php?page=gravity-form-booking-appointments&service-filter="'+serFilter+'"';    
				}
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
			}
		});
		
		/* Staff Filter jQuery */
		$status_filter.on('change', function(){
			var statusFilter = $(this).val();
			if( statusFilter != '' ){
				if( statusFilter == "*" ) {
					document.location.href = 'admin.php?page=gravity-form-booking-appointments';
				}
				else {
					document.location.href = 'admin.php?page=gravity-form-booking-appointments&status-filter="'+statusFilter+'"';    
				}
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
			}
		});
		
		/* Status Filter jQuery */
		$staff_filter.on('change', function(){
			var staffFilter = $(this).val();
			
			if( staffFilter != '' ){				
				if( staffFilter == "*" ){
					document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
				}
				else {				
					document.location.href = 'admin.php?page=gravity-form-booking-appointments&staff-filter="'+staffFilter+'"';    
				}
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
			}
		});
		
		/* Appointment Date filter */
		$gfb_appointment_date_submit.on("click", function(e){
			
			var appointment_start_date = $gfb_appointment_start_date.val();
			var appointment_end_date = $gfb_appointment_end_date.val();
			
			if( appointment_date != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-appointments&appointment-stdt-filter='+appointment_start_date+'&appointment-enddt-filter='+appointment_end_date+'';    
			}
			
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
			}
			
			e.preventDefault();
		});
		
		/* Appointment booking number filter */
		$gfb_appointment_bookingnum_form.on("submit", function(e){
			
			var appointment_booking_num = $gfb_appointment_booking_ref_num.val();
			if( appointment_booking_num != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-appointments&appointment-bookingnum-filter='+appointment_booking_num;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-appointments';    
			}
			
			e.preventDefault();
		});
		
		
		/* Delete Appointment */
		$gfb_delete_appointment.on("click", function(e){
			e.preventDefault();
			var $this = $(this);
			var appointment_id = $(this).data('appointment_id');	
			swal({
			  	title: "Are you sure you want to delete this appointment?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{	
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
				else
				{	
					var dataVal = {
						action 			: 'gfb_delete_appointment',
						appointment_id  : appointment_id,
					};
					
					jQuery.post(
						js_appointment_js.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
							
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.reload();
								});									
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}
							$gfb_maintab_content.removeClass("loading-block");							
						}
					);				
				}
				
			});	
		});
		
		/* Cancelled appointment */
		$appointment_cancel.on("click", function(e){
			e.preventDefault();
			var appointment_id = $(this).data('appointment_id');	
			
			swal({
			  	title: "Are you sure you want to cancel this appointment?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
				else
				{								
					var dataVal = {
						action 			: 'gfb_cancel_appointment',
						appointment_id  : appointment_id,
					};
					
					jQuery.post(
						js_appointment_js.ajaxurl, dataVal, function(response) {
							var obj=$.parseJSON(response);
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.reload(obj.url);
								});									
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}		
							$gfb_maintab_content.removeClass("loading-block");					
						}
					);				
				}
				
			});	
			
			return false;
		});
		
		
		/* Visit appointment */
		$appointment_visit.on("click", function(e){
			e.preventDefault();
			var appointment_id = $(this).data('appointment_id');
			
			swal({
			  	title: "Are you sure you want to make this appointment as visited?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;	
				}
				else
				{				
					var dataVal = {
						action 			: 'gfb_visit_appointment',
						appointment_id  : appointment_id,
					};
					
					jQuery.post(
						js_appointment_js.ajaxurl, dataVal, function(response) {

							$gfb_maintab_content.removeClass("loading-block");
							
							var obj=$.parseJSON(response);
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.reload(obj.url);
								});									
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);				
				}
				
			});	
			
			return false;
		});
		
		/* Conformed appointment */
		$appointment_conformed.on("click", function(e){
			e.preventDefault();
			var appointment_id = $(this).data('appointment_id');
			
			swal({
			  	title: "Are you sure you want to make this appointment as confirmed?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#2ebc2e',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
				else
				{				
					var dataVal = {
						action 			: 'gfb_conformed_appointment',
						appointment_id  : appointment_id,
					};
					
					jQuery.post(
						js_appointment_js.ajaxurl, dataVal, function(response) {

							$gfb_maintab_content.removeClass("loading-block");
							
							var obj=$.parseJSON(response);
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.reload(obj.url);
								});									
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);				
				}
				
			});	
			
			return false;
		});
	});
}(jQuery));
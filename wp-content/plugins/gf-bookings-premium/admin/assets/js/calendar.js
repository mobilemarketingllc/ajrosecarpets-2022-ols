(function( $ ){
	
	var $document = $(document),
		$window = $(window);		
	
	$document.ready(function(){
	
	var $appointmentDatepicker = $('#appointment_date'),
		$calendar = $('#calendar'),
		$get_staff = $('#get_staff'),
		$gfb_loader_img = $("#gfb_loader_img");
		
		function reset_form()
		{
			$('#appointment_form input[type=text]').val('');
			$('#appointment_form input[type=email]').val('');
			$('#appointment_form select').val('');
			$("#appointment_form select").trigger("chosen:updated");
			$('#appointment_form textarea').val('');
	
		}
	
		/* Holiday Array */
		var disableddates = $.parseJSON(js_calendar_js.holidayarr);
		
		/* Disable Holiday Dates */
		function gfbDisableHolidayDates(date) {
			
			var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];			
			
			dmy = ("0" + date.getDate()).slice(-2) + " " + (monthNames[date.getMonth()]) + ", " + date.getFullYear();
			
			if ($.inArray(dmy, disableddates) == -1) {
				valdate = [true, ""];
			} 
			else {
				valdate = [false, ""];
			}
			
			return valdate;
		}
		
		/*Calendar */
		if( $calendar.length > 0 ){
			
			$gfb_loader_img.show();
			
			var date = new Date();
			var staff_id = $get_staff.val();
			var mindate = new Date(date.getFullYear(), date.getMonth(), 1);
			var maxdate = js_calendar_js.maxdate;
			
			var data = {
				action   : 'get_total_appointments',
				staff_id : staff_id
			};

			
			jQuery.post( js_calendar_js.ajaxurl, data, function(response) {
				$gfb_loader_img.hide();
				
				/* Calendar defined */
				$calendar.fullCalendar({						
					events: $.parseJSON(response),
					header: {
						left: 'title',
						right: 'prev,next'
					},
					defaultView: 'month',
					selectable: false,
					selectHelper: false,
					editable: false,
					dayNamesShort: ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday', 'Friday', 'Saturday'],
					eventLimit: false,
					dayRender: function(date, cell){
						if(date > moment(""+maxdate+"")){
							$(cell).addClass('disabled');
						}
						if(date < moment(""+mindate+"")){
							$(cell).addClass('disabled');
						}
						if(date > moment(""+mindate+"") && date < moment(""+maxdate+"")){
							$(cell).addClass('allowToBook');
						}
					},
					eventRender: function(event, element){
						
						var eventString = '';
						
						
						/* Holidays Events */
						if (event.holiday == true ) {
							if (event.holiday == '1' ) {							
								eventString += '<span class="event-holiday" title="Not Working">Not Working</span>';	
							}
						}
						else {
						
							/* Pending Events */
							if (event.pending != '0' ) {
								eventString += '<a href="'+event.pending_url+'"><span class="event-pending" title="Pending Appointments">'+event.pending+' Pending Appointments</span></a>';
							} 
							
							/* Awaiting Events */
							if (event.awaiting != '0' ) {
								eventString += '<a href="'+event.awaiting_url+'"><span class="event-awaiting" title="Awaiting Appointments">'+event.awaiting+' Awaiting Appointments</span></a>';
							} 
							
							/* Completed Events */
							if (event.completed != '0') {
								eventString += '<a href="'+event.completed_url+'"><span class="event-completed" title="Completed Appointments">'+event.completed+' Completed Appointments</span></a>';
							}	
											
							
							/* Cancelled Events */
							if (event.cancelled != '0') {
								eventString += '<a href="'+event.cancelled_url+'"><span class="event-cancelled" title="Cancelled Appointments">'+event.cancelled+' Cancelled Appointments</span></a>';
							}	
	
						}
						$(element).html(eventString);
					},
				});					
			});
		}
	
		/* get staff id from on change event */
		$get_staff.on("change", function(){
			
			$gfb_loader_img.show();
			
			var date = new Date();
			var staff_id = $get_staff.val();
			console.log(staff_id);
			var mindate = new Date(date.getFullYear(), date.getMonth(), 1);
			var maxdate = js_calendar_js.maxdate;
			
			var data = {
				action   : 'get_total_appointments',
				staff_id : staff_id
			};
			
			jQuery.post( js_calendar_js.ajaxurl, data, function(response) {
				
				var json_events = $.parseJSON(response);
				$calendar.fullCalendar('removeEvents');
				
				$gfb_loader_img.hide();
				
				$calendar.fullCalendar('addEventSource', json_events);         
				$calendar.fullCalendar('rerenderEvents' );	
								
			});
		});	
		
		
		
	});
}(jQuery));
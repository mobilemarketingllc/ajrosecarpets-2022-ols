(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
	var $categoryModal = $(".add-category-modal"),
		$editCatModal = $(".edit-category-modal"),
		$category_form = $('#category_form'),
		$edit_category_form = $('#edit_category_form'),
		$gfb_delete_category = $(".gfb_delete_category"),
		$gfb_category_nm_submit = $('#gfb_category_nm_submit'),
		$gfb_category_nm_filter = $("#gfb_category_nm_filter"),
		$gfb_maintab_content = $(".gfb_maintab-content"),
		$gfb_loader_img = $("#gfb_loader_img");
		
		
		
		/* Add class to tr */
		$("td.category_name").parents("tr").addClass("delete-category-fade");
		
		// Category Form Magnific Popup
		$categoryModal.magnificPopup({
			type: 'inline',
			preloader: true,
			focus: '#category_name'
		});	
		
		// Category Form Magnific Popup
		$editCatModal.magnificPopup({
			type: 'inline',
			preloader: true,
			focus: '#category_name',
			callbacks: {
				open: function(){
					var catid = this.st.el['0'].dataset.catid;
					
					var dataVal = {
						action		: 'gfb_display_edit_category',
						category_id : catid,
					};
					
					jQuery.post(
						js_category_ajax.ajaxurl, dataVal, function(response) {

							$('#editCategoryHtml .popup-block-main-body').html( response );		
							
							/* add edit form */
							$('#edit_category_form').validate({
								rules: {
									category_name: "required",
								},
								messages: {
									category_name: "Please enter category name",
								}, 
								submitHandler: function() {
									
									$(".mfp-content").addClass("loading-block");
									
									var data = {
										action		  : 'gfb_update_category',
										category_form : $("#edit_category_form").serialize()
									};	
										
									jQuery.post(
										js_category_ajax.ajaxurl, data, function(response) {
											
											$(".mfp-content").removeClass("loading-block");
											
											var obj=$.parseJSON(response);
											
											if (obj.msg != 'false') {	
												swal({ title:obj.text, type:"success" }, function(){
													window.location.replace(obj.url);
												});
											}	
											else {
												swal({ title:obj.text, type:"error" });		
											}							
										}
									);
									
									return false;
								}
							});					
						}
					);	
				}
			}
		});	
		
		$category_form.validate({
			rules: {
				category_name: "required",
			},
			messages: {
				category_name: "Please enter category name",
			}, 
			submitHandler: function() {
				
				$(".mfp-content").addClass("loading-block");
				
				var data = {
					action			 : 'gfb_add_category',
					category_form    : $category_form.serialize(),
				};	
					
				jQuery.post(
					js_category_ajax.ajaxurl, data, function(response) {
						
						var obj=$.parseJSON(response);
						
						$(".mfp-content").removeClass("loading-block");
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								window.location.replace(obj.url);
							});
						}	
						else {
							swal({ title:obj.text, type:"error" });		
						}						
					}
				);
				
				return false;
			}
		});
		
		/* Delete Category */
		$gfb_delete_category.on("click", function(e){
			e.preventDefault();
			var $this = $(this);
			var catid = $(this).data('catid');	
			
			swal({
			  	title: "Are you sure you want to delete this category?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;	
				}
				else
				{	
					var dataVal = {
						action      : 'gfb_delete_category',
						category_id : catid,
					};
					
					jQuery.post(
						js_category_ajax.ajaxurl, dataVal, function(response) {
							
							var obj=$.parseJSON(response);
							
							$gfb_maintab_content.removeClass("loading-block");
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.reload();
								});									
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);				
				}
				
			});	
		});
		
		/* Service filter */
		$gfb_category_nm_submit.on("click", function(e){
			
			var categorynmFilter = $gfb_category_nm_filter.val();
						
			if( categorynmFilter != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-categories&categorynm-filter='+categorynmFilter;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-categories';    
			}
			
			e.preventDefault();
		});
		
	});
}(jQuery));
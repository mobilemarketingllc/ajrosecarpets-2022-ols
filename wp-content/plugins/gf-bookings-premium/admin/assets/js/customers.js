(function( $ ){
	
	var $document = $(document),
		$window = $(window);
		
	$document.ready(function(){
		
	/* VARIABLES DEFINED */
	var $edit_customer_modal = $('.edit-customer-modal'),
		$gfb_customer_nm_submit = $('#gfb_customer_nm_submit'),
		$gfb_customer_nm = $('#gfb_customer_nm'),
		$gfb_customer_email_submit = $('#gfb_customer_email_submit'),
		$gfb_customer_email = $('#gfb_customer_email'),
		$gfb_customer_contact_submit = $('#gfb_customer_contact_submit'),
		$gfb_customer_contact = $('#gfb_customer_contact'),
		$gfb_delete_customer = $('.gfb_delete_customer'),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_maintab_content = $(".gfb_maintab-content");
		
		
		/* Add class to tr */
		$("td.customer_name").parents("tr").addClass("delete-customer-fade");
		
		// Customer Form Magnific Popup
		$edit_customer_modal.magnificPopup({
			type: 'inline',
			preloader: true,
			focus: '#category_name',
			callbacks: {
				open: function(){
					var custid = this.st.el['0'].dataset.customer_id;
					
					var dataVal = {
						action		: 'gfb_display_customer_editform',
						customer_id : custid,
					};
					
					jQuery.post(
						js_customers_ajax.ajaxurl, dataVal, function(response) {

							$('#editCustomerHtml .popup-block-main-body').html( response );	
							
							$("#edit_customer_form").validate({
								rules: {
									customer_name: "required",
									customer_email: {
										required:true,
										email: true
									},
									customer_contact: {
										required:true,
									},
								},
								messages: {
									customer_name: "Please enter customer name",
									customer_email: {
										required:"Please enter customer email",
										email: "Please enter valid email address"
									},
									customer_contact: {
										required:"Please enter customer contact no",
									},
								},								
								submitHandler: function() {
									
									$(".mfp-content").addClass("loading-block");
									
									var data = {
										action				: 'gfb_edit_customer',
										customer_form		: $("#edit_customer_form").serialize(),
										edit_customer_nonce	: $('#edit_customer_nonce').val(),
									};	
										
									jQuery.post(
										js_customers_ajax.ajaxurl, data, function(response) {

											var obj=$.parseJSON(response);
											$(".mfp-content").removeClass("loading-block");
											
											if (obj.msg != 'false') {	
												swal({ title:obj.text, type:"success" }, function(){
													window.location.replace(obj.url);
												});
											}	
											else {
												swal({ title:obj.text, type:"error" });	
											}							
										}
									);
									
									return false;
								}
							});						
						}
					);	
				}
			}
		});	
		
		
		/* Customer name filter */
		$gfb_customer_nm_submit.on("click", function(e){
			
			var custnm = $gfb_customer_nm.val();
			
			if( custnm != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-customers&custnm-filter='+custnm;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-customers';    
			}
			
			e.preventDefault();
		});
		
		/* Customer email filter */
		$gfb_customer_email_submit.on("click", function(e){
			
			var custemail = $gfb_customer_email.val();
			
			if( custemail != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-customers&custemail-filter='+custemail;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-customers';    
			}
			
			e.preventDefault();
		});
		
		/* Customer contact filter */
		$gfb_customer_contact_submit.on("click", function(e){
			
			var custcontact = $gfb_customer_contact.val();
			
			if( custcontact != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-customers&custcontact-filter='+custcontact;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-customers';    
			}
			
			e.preventDefault();
		});
		
		/* Delete Customer */
		$gfb_delete_customer.on("click", function(e){
			e.preventDefault();
			var $this = $(this);
			var customer_id = $(this).data('customer_id');	
			
			swal({
			  	title: "Are you sure you want to delete this customer?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
				else
				{			
					var dataVal = {
						action 		: 'gfb_delete_customer',
						customer_id : customer_id,
					};
					
					jQuery.post(
						js_customers_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
							$gfb_maintab_content.removeClass("loading-block");
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									//$this.closest(".delete-customer-fade").fadeOut(1000);
									window.location.reload();
								});									
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);				
				}
				
			});	
		});
	
	});
}(jQuery));
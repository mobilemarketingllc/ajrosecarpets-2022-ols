(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
	var $stafflist = $("#stafflist"),
		$edit_staff = $(".edit-staff-modal"),
		$gfp_add_staff = $(".add-staff-modal"),
		$add_staff_form = $("#add_staff_form"),
		$detail_staff_form = $("#detail_staff_form"),
		$timespicker = $(".timespicker"),
		$staff_dayoff_calendar = $('#gfb_staff_dayoff'),
		$imageUploadBtn = $('.gfb-image-upload-button'),
		$delete_staff = $(".gfb_delete_staff"),
		$transfar_appointments_msg = $(".transfar_appointments_msg"),
		$clear_staff = $(".gfb-clear-staff-timeslots"),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_maintab_content = $(".staffmember-box"),
		$staffmember_box_tab_content = $(".staffmember-box-tab-content");	

		let days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];		

		$.each( days, function( i, day ) {
			//console.log( day );

			if ( $(`.global_time_picker_${day}_start`).val() != '' || $(`.global_time_picker_${day}_end`).val() != '' ) {
				$(`.global_time_picker_${day}_end`).removeAttr('disabled');
			}

			//for start time
			$(`.global_time_picker_${day}_start`).bootstrapMaterialDatePicker({ 
				date: false,
				shortTime: true,
				format: 'hh:mm a',				
				time: true,
				switchOnClick: true,
				clearButton: true,
				cancelText: 'Back',
				okText: 'Done',
				clearText: 'Remove',				
			}).on('change', function(e, date) {					
					$(`.global_time_picker_${day}_end`).val('').trigger('change');
					$(`.global_time_picker_${day}_end`).removeAttr('value');

					if ( $(this).val() != '' ) {					
						$(`.global_time_picker_${day}_end`).removeAttr('disabled');
					} else {						
						$(`.global_time_picker_${day}_end`).attr('disabled', true);
					}

					$(`.global_time_picker_${day}_end`).bootstrapMaterialDatePicker('setMinDate', date);					
			});

			//for end time
			$(`.global_time_picker_${day}_end`).bootstrapMaterialDatePicker({ 
				date: false,
				shortTime: true,
				format: 'hh:mm a',				
				time: true,
				switchOnClick: true,		
				clearButton: true,
				cancelText: 'Back',
				okText: 'Done',
				clearText: 'Remove'
			}).on('change', function(e, date) {
				$(`.global_time_picker_${day}_start`).bootstrapMaterialDatePicker('setMaxDate', date);
			});

			// simple multiple select for breaks			
			$(`#break_for_${day}`).select2({		        
		        placeholder: "Select Break Time",
		        allowClear: true,
		    });		    

		    $( document ).on( 'select2:select', `#break_for_${day}`, function () {
		    	var breaks = $( this ).val();		    	
		    	$( '.gfb_wrap' ).addClass( 'loading-block' );
		    	addTimeSlotBreaks( i, day, breaks, 'add' );
		    });

		    $( document ).on( 'select2:unselect', `#break_for_${day}`, function () {		    	
		    	var breaks = $( this ).val();		    	
		    	$( '.gfb_wrap' ).addClass( 'loading-block' );
		    	addTimeSlotBreaks( i, day, breaks, 'remove' );
		    });

		});

		$( document ).on( 'click', '#all_clear', function(e) {
			e.preventDefault();
			$.each( days, function( i, day ) {
				$(`.global_time_picker_${day}_start`).val('').trigger('change');
				$(`.global_slot_capacity_${day}`).val('').trigger('change');
			});
		});


		$( document ).on( 'click', '#all_submit', function (e) {
			e.preventDefault();
			saveTimeSlots( days );
		});

		function addTimeSlotBreaks( i, d, breaks, type='' ) {			

			var data = {
				action: 'gfp_add_staff_timeslot_breaks',
				staff_id: $("#staff_id").val(),
				day_name: d,
				day_key: i,
				breaks: breaks,
				type: type
			};			

			jQuery.post(
				js_staff_ajax.ajaxurl, data, function(response) {					
					var obj=$.parseJSON(response);

					$( '.gfb_wrap' ).removeClass( 'loading-block' );				
					
					if (obj.msg != 'false') {
						swal({ title:obj.text, type:"success" }, function(){
							if ( obj.tab == 'timings-tab' ) {
								//window.location.reload( obj.url + '&tab=' + obj.tab );
							}							
						});
					}	
					else {
						swal({ title:obj.text, type:"error" });		
					}									
				}
			);
		}


		function saveTimeSlots( days ) {
			
			var data = {
				action: 'gfp_add_staff_timeslot',
				staff_id: $("#staff_id").val(),
				duration: $( '#gfb_time_duration' ).val(),
				interval: $( '#gfb_time_interval' ).val(),				
				start_time_sunday: $( `.global_time_picker_sunday_start` ).val(),
				end_time_sunday: $( `.global_time_picker_sunday_end` ).val(),
				slot_cap_sunday: $( `.global_slot_capacity_sunday` ).val(),
				start_time_monday: $( `.global_time_picker_monday_start` ).val(),
				end_time_monday: $( `.global_time_picker_monday_end` ).val(),
				slot_cap_monday: $( `.global_slot_capacity_monday` ).val(),
				start_time_tuesday: $( `.global_time_picker_tuesday_start` ).val(),
				end_time_tuesday: $( `.global_time_picker_tuesday_end` ).val(),
				slot_cap_tuesday: $( `.global_slot_capacity_tuesday` ).val(),
				start_time_wednesday: $( `.global_time_picker_wednesday_start` ).val(),
				end_time_wednesday: $( `.global_time_picker_wednesday_end` ).val(),
				slot_cap_wednesday: $( `.global_slot_capacity_wednesday` ).val(),
				start_time_thursday: $( `.global_time_picker_thursday_start` ).val(),
				end_time_thursday: $( `.global_time_picker_thursday_end` ).val(),
				slot_cap_thursday: $( `.global_slot_capacity_thursday` ).val(),
				start_time_friday: $( `.global_time_picker_friday_start` ).val(),
				end_time_friday: $( `.global_time_picker_friday_end` ).val(),
				slot_cap_friday: $( `.global_slot_capacity_friday` ).val(),
				start_time_saturday: $( `.global_time_picker_saturday_start` ).val(),
				end_time_saturday: $( `.global_time_picker_saturday_end` ).val(),
				slot_cap_saturday: $( `.global_slot_capacity_saturday` ).val(),
				days: days,				
			};			

			jQuery.ajax({
				type: 'POST',
				url: js_staff_ajax.ajaxurl,
				data: data,
				async:true,
				beforeSend: function () {
					jQuery( '.gfb_wrap' ).addClass( 'loading-block' );
				},
				success: function ( response ) {
					//debugger;
					var obj=$.parseJSON(response);						
					
					$( '.gfb_wrap' ).removeClass( 'loading-block' );
					
					if (obj.msg != 'false') {

						var msg = '';			
						
						if ( 'created_slots' in obj ) {
							$.each( days, function( i, d ) {								
								var select = document.getElementById(`break_for_${d}`);								
								if ( select.options.length <= 0 && obj.created_slots != null  ) {									
									jQuery.each( obj.created_slots[d]['slots'], function(i, v) {
										select.options[select.options.length] = new Option(v, i);
									});
								}								
							});
						}

						var arr = Object.values(obj.message);

						if ( jQuery.inArray( 'true', arr ) >= 0 ) {
							swal({ title: 'Timings', text: 'Slot created/Updated successfully.', type:"success" }, function() {
								window.location.href = obj.url;
							});
						} else {
							swal({ title: 'Timings', text: 'No slot created.', type:"error" }, function() {
								window.location.href = obj.url;
							});
						}

						if ( 'true' != obj.message.sunday && 'false' != obj.message.sunday ) {
							msg += obj.message.sunday + '<br>';	
						}

						if ( 'true' != obj.message.monday && 'false' != obj.message.monday ) {
							msg += obj.message.monday + '<br>';	
						}

						if ( 'true' != obj.message.tuesday && 'false' != obj.message.tuesday ) {
							msg += obj.message.tuesday + '<br>';	
						}

						if ( 'true' != obj.message.wednesday && 'false' != obj.message.wednesday ) {
							msg += obj.message.wednesday + '<br>';	
						}

						if ( 'true' != obj.message.thursday && 'false' != obj.message.thursday ) {
							msg += obj.message.thursday + '<br>';	
						}

						if ( 'true' != obj.message.friday && 'false' != obj.message.friday ) {
							msg += obj.message.friday + '<br>';	
						}

						if ( 'true' != obj.message.saturday && 'false' != obj.message.saturday ) {
							msg += obj.message.saturday + '\n';	
						}						

						if ( msg != '' ) {
							jQuery('#gfb-timing-response').html(`<div class="gfb-alert alert-danger alert-dismissible" role="alert">
		                        ${msg}		                        
		                    </div>`);
		                }

					} else {

						swal({ title: 'Timings', text:obj.text, type:"error" });

					}
				}
			});		

			// jQuery.post(
			// 	js_staff_ajax.ajaxurl, data, function(response) {
			// 		//debugger;
			// 		var obj=$.parseJSON(response);	

			// 		console.log( d );
			// 		console.log( obj );				

			// 		$(".mfp-content").removeClass("loading-block");	

			// 		var select = document.getElementById(`break_for_${d}`);					
					
			// 		if (obj.msg != 'false') {
			// 			swal({ title:obj.text, type:"success" }, function() {

			// 				if ( 'created_slots' in obj && typeof obj.created_slots != null ) {								
			// 					//console.log( select );
			// 					if ( select.options.length <= 0 ) {
			// 						jQuery.each( obj.created_slots['slots'], function(i, v) {
			// 							select.options[select.options.length] = new Option(v, i);
			// 						});
			// 					}

			// 					jQuery(select).parent( '.global_breaktime_slot_wrapper' ).addClass('show_break');
			// 				}						
			// 			});
			// 		} else {
			// 			swal({ title:obj.text, type:"error" });		
			// 		}									
			// 	}
			// );	
			/* Add Time Slot Form New by Developer53 End */
		}

		$( document ).on( 'click', '#all_fullday_submit', function (e) {
			e.preventDefault();

			$( '.gfb_wrap' ).addClass( 'loading-block' );

			var form = $('#gfb_add_staff_fullday_time_slot_new');

			var data = {
				action: 'gfp_add_fullday_staff_timeslot',
				staff_id: $("#staff_id").val(),
				form_data: form.serialize()
			};	

			jQuery.ajax({
				type: 'POST',
				url: js_staff_ajax.ajaxurl,
				data: data,
				async:true,
				success: function ( response ) {
					//debugger;
					
					var obj=$.parseJSON(response);

					$( '.gfb_wrap' ).removeClass( 'loading-block' );

					if (obj.msg != 'false') {

						swal({ title:obj.text, type:"success" }, function() {
						});
					} else {
						swal({ title:obj.text, type:"error" });
					}
				}
			});
		});
		
		$('.chosen-select').chosen();

		$document.on('click', '.staffmember-box-tab-titles ul li a', function(e){
			var tgt = $(this).attr('href');
			$('.staffmember-box-tab.active').removeClass('active');
			$(tgt).addClass('active');
			$('.staffmember-box-tab-titles ul li.active').removeClass('active');
			$(this).closest('li').addClass('active');
			e.preventDefault();
		});
		
		/* VARIABLES DEFINED */
		var yearfirstdate = new Date(new Date().getFullYear(), 0, 1);
		var eventDates = {};
		
		/* View Add Staff Form Popup */
		$gfp_add_staff.magnificPopup({
			type: "inline",
			preloader: true,
			focus: "#staff_name",
		});
				
		
		/* Submit form to add staff */
		$add_staff_form.validate({
			
			rules: {
				staff_name: "required",
				staff_username: {
					required:true,
					maxlength:15
				},
				staff_email: {
					required:true,
					email:true	
				},
				staff_password: {
					required:true,
					maxlength:10
				},
			},
			messages: {
				staff_name: "Please enter staff name",
				staff_username: {
					required: "Please enter staff username",
					maxlength:"Username should be maximum 15 character long"
				},
				staff_email: {
					required:"Please enter staff email",
					email:"Please enter valid email address"	
				},
				staff_password: {
					required:"Please enter password",
					maxlength:"Username should be maximum 10 character long"
				},
			},
			submitHandler: function() {
				
				$(".mfp-content").addClass("loading-block");
			
				var dataVal = {
					action	    : 'gfp_add_staff',
					staff_name  : $add_staff_form.serialize(),
				};
				
				jQuery.post(
					js_staff_ajax.ajaxurl, dataVal, function(response) {

						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								window.location.reload(obj.url);	
							});
						}	
						else {
							swal({ title:obj.text, type:"error" });	
								
						}	
						$(".mfp-content").removeClass("loading-block");								
					}
				);	
					
				return false;
			}
		});
		
		/* Submit form to update staff details */
		$document.on('submit', '#detail_staff_form', function(e){
			
			
			
			$staffmember_box_tab_content.addClass("loading-block");
			
			var dataVal = {
				action	    : 'gfb_update_staff',
				staff_data  : $('#detail_staff_form').serialize()
			};
			
			jQuery.post(
				js_staff_ajax.ajaxurl, dataVal, function(response) {

					var obj=$.parseJSON(response);
					
					$staffmember_box_tab_content.removeClass("loading-block");
					
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" }, function(){
							window.location.replace(obj.url);	
						});
					}	
					else {
						swal({ title:obj.text, type:"error" });		
					}									
				}
			);	
				
            e.preventDefault();	
		});

		/* Submit form to update staff timezone */
		$document.on('submit', '#add_staff_timezone', function(e){
			
			var timezone = $("#staff_timezone").val();			
			var staffId = $("#staff_id").val();
			
			$staffmember_box_tab_content.addClass("loading-block");
						
			var dataVal = {
				action	   	   : 'gfp_add_staff_timezone',
				staff_timezone : timezone,				
				staff_id 	   : staffId,
			};
						
			jQuery.post(
				js_staff_ajax.ajaxurl, dataVal, function(response) {

					var obj=$.parseJSON(response);
					
					$staffmember_box_tab_content.removeClass("loading-block");
					
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" });
					}	
					else {
						swal({ title:obj.text, type:"error" });		
					}									
				}
			);
				
            e.preventDefault();	
		});
		
		/* Submit form to update staff services */
		$document.on('submit', '#add_staff_service', function(e){
			
			var checkedServices = [];
			var unCheckedServices = [];
			var newprice = '';
			var staffId = $("#staff_id").val();
			
			$staffmember_box_tab_content.addClass("loading-block");
			
			$.each($("input[name='staff_name_service']:checked"), function(){ 
			
				var price = $(("#staff_service_price_"+$(this).val())).val();
				var oldprice = $(("#staff_service_price_"+$(this).val())).data('oldprice');
			
				if( price == '' ) {
					newprice = oldprice;
				}
				else {
					newprice = price;
				}
			 
                checkedServices.push({ serid:$(this).val(), price:newprice });
            });
			
			$.each($("input[name='staff_name_service']:not(:checked)"), function(){  
                unCheckedServices.push($(this).val());
            });
						
			var dataVal = {
				action	   				 : 'gfp_add_staff_service',
				staff_checked_services   : checkedServices,
				staff_unchecked_services : unCheckedServices,
				staff_id 				 : staffId,
			};
						
			jQuery.post(
				js_staff_ajax.ajaxurl, dataVal, function(response) {

					var obj=$.parseJSON(response);
					
					$staffmember_box_tab_content.removeClass("loading-block");
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" });
					}	
					else {
						swal({ title:obj.text, type:"error" });		
					}									
				}
			);	
				
            e.preventDefault();	
		});
		
		
		/* Open Time Slot Popup */
		$document.on('click', '.gfb-add-staff-timeslot', function() {		
			$.magnificPopup.open({
				items: {
					src: '#addStaffTimeSlotModal' 
				},
				type: 'inline'
			});			
			
			$('.current-custom-day').removeClass('current-custom-day');
			$(this).addClass('current-custom-day');
			
		});		

		/* Add Time Slot Form old method */
		// $('#staff_time_slot_form').validate({
		// 	rules: {
		// 		time_slot_name: "required",
		// 		slot_start_time: "required",
		// 		slot_end_time: "required",
		// 		max_appointment_capacity: {
		// 			required:true,
		// 			digits: true
		// 		},
		// 	},
		// 	messages: {
		// 		time_slot_name: "Please enter time slot name",
		// 		slot_start_time: "Please select starting time of slot",
		// 		slot_end_time: "Please select ending time of slot",
		// 		max_appointment_capacity: {
		// 			required:"Enter maximum no. of appointment you wish to book in particular time slot",
		// 			digits: "Please enter numbers only"
		// 		},
		// 	},
		// 	submitHandler: function(){
				
		// 		$(".mfp-content").addClass("loading-block");
					
		// 		var wday =  $(".current-custom-day").data('weekday');
		// 		var staffId = $("#staff_id").val();
				
		// 		var dataVal = {
		// 			action	  : 'gfp_add_staff_timeslot',
		// 			slot_data : $("#staff_time_slot_form").serialize(),
		// 			slotday   : wday,
		// 			staff_id  : staffId
		// 		};
								
		// 		jQuery.post(
		// 			js_staff_ajax.ajaxurl, dataVal, function(response) {

		// 				var obj=$.parseJSON(response);
		// 				$(".mfp-content").removeClass("loading-block");	
		// 				if (obj.msg != 'false') {	
		// 					swal({ title:obj.text, type:"success" }, function(){
		// 						window.location.reload(obj.url);	
		// 					});
		// 				}	
		// 				else {
		// 					swal({ title:obj.text, type:"error" });		
		// 				}									
		// 			}
		// 		);	
					
		// 		return false;
		// 	}
  //       });
					
		/* STAFF DAYSOFF WITH HOLIDAY LIST FUNCTION  */
		if( $("#gfb_staff_dayoff").length > 0 ) {
			//debugger;
			var datecontent = {
				action   : 'gfb_display_staff_holiday',
				staff_id : $stafflist.val(),
			};
						
			$.post( js_staff_ajax.ajaxurl, datecontent, function(response) {					
					
				/* remove loader */
				$(".gfb-ajax-loader-dayoff").remove();
				
				eventDates = {};
				
				objArr=$.parseJSON(response);
				
				$.each( objArr, function( key, value ) {						
					eventDates[ new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value+'T00:00:00' ) )+"" ) ] = new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value+'T00:00:00' ) )+"" ).toString();
				});

				//console.log( eventDates );

				let holiday_selected_days = [];
				let holiday_removed_days = [];	
							
				/* Holiday Calendar */
				$('#gfb_staff_dayoff').datepicker({
					dateFormat: 'yy-mm-dd',
					minDate: '0',					
					changeYear:true,				
					yearRange:"0:+50",
					numberOfMonths:[4,3],
					todayHighlight: true,
					beforeShowDay: function( date ) {	
						//console.log( eventDates );
						//debugger;
						var highlight = eventDates[date];
						
						if( highlight ) {
							 return [true, "event", highlight];
						} else {
							 return [true, '', ''];
						} 			
					},					
				}).datepicker('setDate', yearfirstdate);


				/** add holiday ajax start **/
				
				$( document ).on( 'click', '#add_holdiays', function (e) {
					e.preventDefault();
					var current = $(this);
					$staffmember_box_tab_content.addClass("loading-block");
					/* add holiday */					 
					var dateData = {
					 	action		 : 'gfp_add_staff_holiday',
					 	holiday_date : holiday_selected_days,
						staff_id 	 : $stafflist.val(),
					};

					//console.log( dateData );
					
												
					$.post( js_staff_ajax.ajaxurl, dateData, function(response) {
				 		//debugger;
				 		var obj=$.parseJSON(response);
				 		holiday_selected_days = [];
				 		current.attr( 'disabled', true );

				 		if (obj.msg != 'false') {	

				 			$( '#gfb_staff_dayoff td.select-event' ).each( function (){
								$(this).removeClass( 'select-event' );
								$(this).addClass( 'event' );
							});
				 			
				 			swal({ title:obj.text, type:"success" });
				 			//$(".ui-datepicker-current-day").addClass('event');
							
				 			/* Change update holiday array */	
				 			var datecontent = {
				 				action   : 'gfb_display_staff_holiday',
				 				staff_id : $stafflist.val(),
				 			};
							
				 			$.post( js_staff_ajax.ajaxurl, datecontent, function(response) {					
																				
				 				objArr=$.parseJSON(response);
				 				eventDates = {};
																			
				 				$.each( objArr, function( key, value ) {						
				 					eventDates[ new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value+'T00:00:00' ) )+"" ) ] = new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value+'T00:00:00' ) )+"" ).toString();
				 				});
								
				 				$staffmember_box_tab_content.removeClass("loading-block");
								
				 			});
				 		} else {
				 			$( '#gfb_staff_dayoff td.select-event' ).each( function (){
								$(this).removeClass( 'select-event' );								
							});
				 			swal({ title:obj.text, type:"error" });	
				 		}
					});

				});

				/** add holiday ajax end **/


				/** remove holiday ajax start **/
				
				$( document ).on( 'click', '#remove_holdiays', function (e) {
					e.preventDefault();
					var current = $(this);
					$staffmember_box_tab_content.addClass("loading-block");
					/* remove holiday */					 
					var dateData = {
					 	action		 : 'gfb_remove_staff_holiday',
					 	holiday_date : holiday_removed_days,
						staff_id 	 : $stafflist.val(),
					};

					//console.log( dateData );
					
												
					$.post( js_staff_ajax.ajaxurl, dateData, function(response) {
				 		var obj=$.parseJSON(response);
				 		holiday_removed_days = [];
				 		$( '#gfb_staff_dayoff td.remove-event' ).each( function (){
							$(this).removeClass( 'remove-event' );
						});

						current.attr( 'disabled', true );

						$staffmember_box_tab_content.removeClass("loading-block");

						if (obj.msg != 'false') {								
							swal({ title:obj.text, type:"success" });
							
							/* Change update holiday array */	
							var datecontent = {
								action   : 'gfb_display_staff_holiday',
								staff_id : $stafflist.val(),
							};
							
							$.post( js_staff_ajax.ajaxurl, datecontent, function(response) {					
									
								objArr=$.parseJSON(response);
								eventDates = {};
								
								$.each( objArr, function( key, value ) {						
									eventDates[ new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value+'T00:00:00' ) )+"" ) ] = new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value+'T00:00:00' ) )+"" ).toString();
								});								
								
							});
						} else {							
							swal({ title:obj.text, type:"error" });	
						}
					});

				});

				/** remove holiday ajax end **/

				initCalendar();

				//creating draging datepicker
				function initCalendar() {

				    $('#gfb_staff_dayoff td a').click(function ( e ) {  // Click or start of dragging
				    	e.preventDefault();
				    	//debugger;
				    	//thisDates = [];		    	
				    	if ( ! $(this).parent('td').hasClass('select-event') && ! $(this).parent('td').hasClass('event') && ! $(this).parent('td').hasClass('remove-event') ) {	       
					        $(this).parent('td').addClass('select-event');
					        $(this).addClass('ui-state-active');					        
					        var month = parseInt( $(this).parent().attr('data-month') ) + 1;
				        	month = month < 10 ? '0' + month : month;		        	
				            holiday_selected_days.push( `${$(this).parent().attr('data-year')}-${month}-${$(this).html()}` );
					    } else if ( $(this).parent('td').hasClass('select-event') ) {
					    	$(this).parent('td').removeClass('event');
					    	$(this).parent('td').removeClass('select-event');
					    	var month = parseInt( $(this).parent().attr('data-month') ) + 1;
					        month = month < 10 ? '0' + month : month;
					    	holiday_selected_days = holiday_selected_days.filter( item => item !== `${$(this).parent().attr('data-year')}-${month}-${$(this).html()}` );
					    	holiday_removed_days.push( `${$(this).parent().attr('data-year')}-${month}-${$(this).html()}` );
					    } else if ( $(this).parent('td').hasClass('event') ) {
					    	$(this).parent('td').removeClass('event');
					    	$(this).parent('td').removeClass('select-event');
					    	$(this).parent('td').addClass('remove-event');
					        $(this).removeClass('ui-state-active');
					    	var month = parseInt( $(this).parent().attr('data-month') ) + 1;
					        month = month < 10 ? '0' + month : month;
					    	holiday_selected_days = holiday_selected_days.filter( item => item !== `${$(this).parent().attr('data-year')}-${month}-${$(this).html()}` );
					    	holiday_removed_days.push( `${$(this).parent().attr('data-year')}-${month}-${$(this).html()}` );
					    }

					    if ( holiday_removed_days.length === 0 ) {
					    	$( '#remove_holdiays' ).attr( 'disabled', true );
					    } else {
					    	$( '#remove_holdiays' ).removeAttr( 'disabled');
					    }

					    if ( holiday_selected_days.length === 0 ) {
					    	$( '#add_holdiays' ).attr( 'disabled', true );
					    } else {
					    	$( '#add_holdiays' ).removeAttr( 'disabled');
					    }
			        	
			        	console.log( 'selected day' );
				        console.log( holiday_selected_days );
				        console.log( 'removed day' );
				        console.log( holiday_removed_days );
				        return false;
				    });

				    $('.ui-datepicker-year').on( 'change', function() {				        
				        initCalendar();
				    });				    
				}

			});	
			
		}
		
		/* Delete Staff Slot */
		$document.on('click', '.delete-staff-slot', function(e){
			e.preventDefault();
			var $this = $(this);
			var slotid = $(this).data('time_slot_id');
			var staffId = $("#staff_id").val();
			
			swal({
				title: "Are you sure you want to delete this time slot?",
				text: "You will not be able to recover this time slot!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Clear It!",
				cancelButtonText: "No, Cancel It!",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				
				$gfb_loader_img.show();
				
				if (isConfirm) {				
					
					var dataVal = {
						action 		 : 'gfb_delete_staff_timeslot',
						time_slot_id : slotid,
						staff_id     : staffId,
					};
					
					jQuery.post(
						js_staff_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									$gfb_loader_img.hide();
									window.location.reload();
								});							
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);			
				}
				else {
					$gfb_loader_img.hide();
					return false;
				}
			});
			
		});
		
		/* Edit Time Slot for Staff */
		$document.on('click', '.edit-staff-slot', function(e) {	
			e.preventDefault();
			var staffId = $("#staff_id").val();	
			var slotid = $(this).data('time_slot_id')
			$.magnificPopup.open({
				items: {
					src: '#editStaffTimeSlotModal' ,
					type: 'inline',
				},
				callbacks: {
					open: function(){
						
						var time_slot_id = slotid;
						var dataVal = {
							action		 : 'gfb_display_edit_stafftimeslot',
							time_slot_id : time_slot_id,
							staff_id     : staffId,
						};
												
						jQuery.post(
							js_staff_ajax.ajaxurl, dataVal, function(response) {

								$('#editStaffTimeSlotModal .popup-block-main-body').html( response );	
								$(".timespicker").timepicker({
									'timeFormat': 'H:i',
									'step': js_time_slot_ajax.slotinterval,
									'disableTextInput':true,
									'scrollDefault': 'now',
								});		
								
								$('#edit_staff_timeslot_form').validate({
									rules: {
										time_slot_name: "required",
										slot_start_time: "required",
										slot_end_time: "required",
										max_appointment_capacity: {
											required:true,
											digits: true
										},
									},
									messages: {
										time_slot_name: "Please enter time slot name",
										slot_start_time: "Please select starting time of slot",
										slot_end_time: "Please select ending time of slot",
										max_appointment_capacity: {
											required:"Enter maximum no. of appointment you wish to book in particular time slot",
											digits: "Please enter numbers only"
										},
									},
									submitHandler: function(){
										
										$(".mfp-content").addClass("loading-block");
												
										var data = {
											action		  	: 'gfb_edit_stafftimeslot',
											timeslot_form 	: $('#edit_staff_timeslot_form').serialize(),
										};
																					
										jQuery.post(
											js_staff_ajax.ajaxurl, data, function(response) {

												var obj=$.parseJSON(response);
												$(".mfp-content").removeClass("loading-block");
												
												if (obj.msg != 'false') {	
													swal({ title:obj.text, type:"success" }, function(){
														window.location.reload(obj.url);	
													});
												}	
												else {
													swal({ title:obj.text, type:"error" });
												}							
											}
										);
			
										return false;
									}
								});				
							}
						);	
					}
				},
				
			});			
			
		});
		
		
		// Function Upload Media (Image)
		$imageUploadBtn.on("click", function (e) {
			var url;
			var el = $(this).parent();
			var button = $(this);
			e.preventDefault();
			var uploader = wp.media({
				title : button.data('upload-title'),
				button : {
					text : button.data('upload-button')
				},
				multiple : false
			})
			.on('select', function () {
				var selection = uploader.state().get('selection');
				var attachment = selection.first().toJSON();
				url = attachment.url;
				
				/* save staff pic */
				var dataurl = {
					action  : 'gfb_save_staff_pic',
					imgurl  : url,
					sid		: $imageUploadBtn.data('sid')
				};
									
				jQuery.post(
					js_staff_ajax.ajaxurl, dataurl, function(response) { console.log(response);  }
				);
			
				$('input[type=hidden]', el).val(attachment.url);
				if (!el.hasClass('upload_file')) {
					if ($('img', el).length > 0) {
						$('.image-preview', el).attr('src', attachment.url);
					} else {
						$('<img src="'+ attachment.url +'" class="image-preview" width="" height="">').insertBefore($(':first-child', el));
					}
				}
			})
			.open();
		});
		
		/* Clear time slot */		
		$clear_staff.on('click', function(e){
			e.preventDefault();
			var $this = $(this);
			var slotid = $(this).data('weekday');
			var staffid = $(this).data('staff');
			
			swal({
				title: "Are you sure you want to clear all time slots?",
				text: "You will not be able to recover this time slots!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Clear It!",
				cancelButtonText: "No, Cancel It!",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) 
			{
				$gfb_loader_img.show();
				
				if (isConfirm) {				
					
					var dataVal = {
						action 	: 'gfb_clear_stafftimeslot',
						weekday : slotid,
						staff	: staffid
					};
					
					jQuery.post(
						js_staff_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									$gfb_loader_img.hide();
									window.location.replace(obj.url);
								});
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);			
				}
				else {
					$gfb_loader_img.hide();
					return false;
				}
			});
	
		});
		
		
		$delete_staff.on('click', function(e){
			e.preventDefault();
			//var slotid = $(this)[0].dataset.stfid;
			var staffId = $(this)[0].dataset.stfid;
			
			swal({
				title: "Are you sure you want to delete the staff member",
				text: "You will not be able to recover it!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete It!",
				cancelButtonText: "No, Cancel It!",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				
				$gfb_maintab_content.addClass("loading-block");
				
				if (isConfirm) {				
					
					var dataVal = {
						action 		 : 'gfb_delete_staff',
						staff_id     : staffId,
					};
					
					jQuery.post(
						js_staff_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
							
							$gfb_maintab_content.removeClass("loading-block");	
							
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.replace(obj.url);
								});							
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);	
					
					return false;	
				}
				else {
					$gfb_maintab_content.removeClass("loading-block");	
					return false;
				}
			});
	
		});
		
		/* Transfar Appointments MSG */
		$transfar_appointments_msg.on('click', function(e){
			e.preventDefault();
			//var slotid = $(this)[0].dataset.stfid;
			var staffId = $(this)[0].dataset.stfid;
			
			swal({
				title: "Unable to delete this staff member",
				text: "First you need to assign his appointments to other staff member!",
				type: "warning",
				showCancelButton: false,
				confirmButtonColor: "#F8BB86",
				confirmButtonText: "OK",
				closeOnCancel: true
			},
			function (isConfirm) {
			});
	
		});
		
		/* GET STAFF DETAILS FROM ID */ 
		$stafflist.on('change', function(){			
			
			var staffId = $stafflist.val();
			$gfb_maintab_content.addClass("staff-loading-block");

			if( staffId != '' ) {	
				$gfb_maintab_content.removeClass("staff-loading-block");		
				var current_url = window.location.href;
				var location = current_url.split('&');
				$url = location[0] + '&state='+encodeURIComponent(staffId);
				window.location = $url;
			}
			else {
				$gfb_maintab_content.removeClass("staff-loading-block");
				var current_url = window.location.href;
				var location = current_url.split('&');				
				$url = location[0];				
				window.location = $url;	
			}
			
		});
		
		
		/* GET STAFF DETAILS FROM ID */ 
		$edit_staff.on('click', function(e){			
			e.preventDefault();
			var staffId = $(this)[0].dataset.stfid;
			$gfb_maintab_content.addClass("staff-loading-block");
						
			if( staffId != '' ) {	
				$gfb_maintab_content.removeClass("staff-loading-block");		
				var current_url = window.location.href;				
				var location = current_url.split('&');
				$url = location[0] + '&state='+encodeURIComponent(staffId);
				window.location = $url;
			}
			else {
				$gfb_maintab_content.removeClass("staff-loading-block");
				var current_url = window.location.href;
				var location = current_url.split('&');				
				$url = location[0];				
				window.location = $url;	
			}
			
		});
		
		/* Delete Staff */
		$document.on('click', '.staff-delete', function(e){
			e.preventDefault();
			var staffId = $('.staff-delete').data("staffid");
			
			swal({
				title: "Are you sure you want to delete the staff member",
				text: "You will not be able to recover it!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete It!",
				cancelButtonText: "No, Cancel It!",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				
				$gfb_maintab_content.addClass("loading-block");
				
				if (isConfirm) {				
					
					var dataVal = {
						action 		 : 'gfb_delete_staff',
						staff_id     : staffId,
					};
					
					jQuery.post(
						js_staff_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
							
							$gfb_maintab_content.removeClass("loading-block");
							
							if (obj.msg != 'false') {
								swal({ title:obj.text, type:"success" }, function(){
									window.location.replace(obj.url);
								});
							}
							else {
								swal({ title:obj.text, type:"error" });
							}
						}
					);
					
					return false;
				}
				else {
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
			});
			
		});
		
		
	});
}(jQuery));
<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; } 

class GravityFormBookingActivation {
	
	public function gfbTextDomain(){
		
		 if ( ! function_exists( 'get_plugin_data' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}
		
		$plugin_data = get_plugin_data( GFB_DIR . '/gravity-form-booking.php' );
		
		load_plugin_textdomain( $plugin_data['Version'], false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}
	
	public function gfbActivation() {
		
		$file = $this->gfbCheckFileExist( GFB_ADMIN_CLASS . 'gravityformbooking.check.verification.cls.php' );
		
		if($file == true){
			
			$class = $this->gfbCheckClassExist( 'GravityFormBookingCheckVerification' );
			
			if($class == true){				
				
				$res = GravityFormBookingCheckVerification::gfbGetCheckPluginVerification();
				
				if($res == true){	
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		
	}
	
	public function gfbAddOptions() {
		$optionArguments = array(
			'gfb_installation_time' => time(),
		);
		
		foreach($optionArguments as $options => $value){
			add_option( $options, $value );	
		}
	}
	
	public function gfbCheckFileExist($file){
		
		if( file_exists( $file ) ) {
	
			require_once( $file );
			return true;
		}
		else {
			return false;
		}
	}
	
	public function gfbCheckClassExist($class){
		
		if( class_exists( $class ) ) {
	
			return true;
		}
		else {
			return false;
		}
	}
	
	/* public function gfbAddLicenseMenu() {
		
		add_menu_page( __( 'Gravity Forms Booking License', 'gfb' ), __( 'Gravity Forms Booking License', 'gfb' ), 'read', 'gravity-form-booking-license',  array( &$this , 'gfbLicense'), 'dashicons-calendar-alt', 23 );				
	} */
	
	public function gfbAddPluginMenu() {		

		add_menu_page( __( 'Gravity Forms Booking', 'gfb' ), __( 'Gravity Forms Booking', 'gfb' ), 'manage_options', 'gravity-form-booking-menu',  array( &$this , 'gfbMenuPages'), 'dashicons-calendar-alt', 23 );
		
		/* Dashboard Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Dashboard', 'gfb' ), __( 'Dashboard', 'gfb' ), 'manage_options', 'gravity-form-booking-menu', array( $this, 'gfbMenuPages' ) );	
		
		/* Calendar Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Calendar', 'gfb' ), __( 'Calendar', 'gfb' ), 'manage_options', 'gravity-form-booking-calendar', array( $this, 'gfbMenuPages' ) );
		
		/* Appointments Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Appointments', 'gfb' ), __( 'Appointments', 'gfb' ), 'manage_options', 'gravity-form-booking-appointments', array( $this, 'gfbMenuPages' ) );
		
		/* Time Slot Menu */
		//add_submenu_page( 'gravity-form-booking-menu', __( 'Time Slot', 'gfb' ), __( 'Time Slot', 'gfb' ), 'manage_options', 'gravity-form-booking-time-slot', array( $this, 'gfbMenuPages' ) );
		
		/* Holiday Menu */
		//add_submenu_page( 'gravity-form-booking-menu', __( 'Holiday', 'gfb' ), __( 'Holiday', 'gfb' ), 'manage_options', 'gravity-form-booking-holiday', array( $this, 'gfbMenuPages' ) );
		
		/* Category Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Service Categories', 'gfb' ), __( 'Service Categories', 'gfb' ), 'manage_options', 'gravity-form-booking-categories', array( $this, 'gfbMenuPages' ) );
		
		/* Services Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Services', 'gfb' ), __( 'Services', 'gfb' ), 'manage_options', 'gravity-form-booking-services', array( $this, 'gfbMenuPages' ) );
		
		/* Staff Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Staff', 'gfb' ), __( 'Staff', 'gfb' ), 'manage_options', 'gravity-form-booking-staff', array( $this, 'gfbMenuPages' ) );
		
		/* Customers Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Customers', 'gfb' ), __( 'Customers', 'gfb' ), 'manage_options', 'gravity-form-booking-customers', array( $this, 'gfbMenuPages' ) );
		
		/* Payment Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Payment', 'gfb' ), __( 'Payment', 'gfb' ), 'manage_options', 'gravity-form-booking-payment', array( $this, 'gfbMenuPages' ) );
		
		/* Email Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Email Notification', 'gfb' ), __( 'Email Notification', 'gfb' ), 'manage_options', 'gravity-form-booking-email', array( $this, 'gfbMenuPages' ) );
		
		/* Settings Menu */
		add_submenu_page( 'gravity-form-booking-menu', __( 'Settings', 'gfb' ), __( 'Settings', 'gfb' ), 'manage_options', 'gravity-form-booking-settings', array( $this, 'gfbMenuPages' ) );

	}
	
	public function gfbMenuPages() {
		
		if( $_GET['page'] == 'gravity-form-booking-menu' ) {
			/* Dashboard */
			require_once( GFB_ADMIN_TAB . 'dashboard/dashboard.php' );
		}		
		elseif( $_GET['page'] == 'gravity-form-booking-calendar' ) {
			/* Calendar */
			require_once( GFB_ADMIN_TAB . 'calendar/calendar.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-appointments' ) {
			/* Appointment */
			require_once( GFB_ADMIN_TAB . 'appointments/listAppointments.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-time-slot' ) {
			/* Time Slot */
			//require_once( GFB_ADMIN_TAB . 'timeSlots/timeSlotController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-holiday' ) {
			/* Category */
			//require_once( GFB_ADMIN_TAB . 'holiday/holidayController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-categories' ) {
			/* Category */
			require_once( GFB_ADMIN_TAB . 'categories/listCategory.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-services' ) {
			/* Service */
			require_once( GFB_ADMIN_TAB . 'services/listService.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-staff' ) {
			/* Staff */
			require_once( GFB_ADMIN_TAB . 'staff/staffController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-customers' ) {
			/* Customer */
			require_once( GFB_ADMIN_TAB . 'customers/listCustomers.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-payment' ) {
			/* Payment */
			require_once( GFB_ADMIN_TAB . 'payment/listPayments.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-email' ) {
			/* Email */
			require_once( GFB_ADMIN_TAB . 'email/emailController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-settings' ) {
			/* Settings */
			require_once( GFB_ADMIN_TAB . 'settings/settingsController.php' );
		}	
		
	}
	
	/* Generate Dynamic PAges */
	function gfbAutoGeneratePage(){
		
		$new_page_title = 'Success';
		$new_page_content = '';
		$new_page_template = GFB_INCLUDES . '/success.php'; 
		
		//don't change the code bellow, unless you know what you're doing
		$page_check = get_page_by_title($new_page_title);
		$new_page = array(
			'post_type' => 'page',
			'post_title' => $new_page_title,
			'post_content' => $new_page_content,
			'post_status' => 'publish',
			'post_author' => 1,
		);
		 
		if(!isset($page_check->ID)){
			
			echo $new_page_template; exit;
			if(!empty($new_page_template)){
				echo "asdas";
			}
			exit;
		}
	}

}
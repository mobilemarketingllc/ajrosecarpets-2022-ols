<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

	require_once(GFB_ADMIN_DIR.'classes/gravityformbooking.google.calendar.api.php');
	
	class GravityFormBookingAppointments {
		
		function __construct() {
			
			add_action( 'wp_ajax_gfb_add_appointment', array(&$this, 'gfbAddAppoinmentFromAdminSide') );
			add_action( 'wp_ajax_get_appointment_list', array(&$this, 'gfbGetAppointmentListByValue') );
			
			add_action( 'wp_ajax_gfbListServicesByCategory',  array(&$this, 'gfbListServicesByCategory') );
			add_action( 'wp_ajax_nopriv_gfbListServicesByCategory',  array(&$this, 'gfbListServicesByCategory') );
			
			add_action( 'wp_ajax_gfbListStaff',  array(&$this, 'gfbListStaff') );	
			add_action( 'wp_ajax_nopriv_gfbListStaff',  array(&$this, 'gfbListStaff') );
			
			add_action( 'wp_ajax_gfbGetStaffTimeSlot',  array(&$this, 'gfbGetStaffTimeSlot') );	
			
			add_action( 'wp_ajax_gfb_delete_appointment',  array(&$this, 'gfbDeleteAppointment') );
			add_action( 'wp_ajax_gfb_cancel_appointment',  array(&$this, 'gfbCancelAppointment') );
			add_action( 'wp_ajax_gfb_visit_appointment',  array(&$this, 'gfbVisitAppointment') );
			add_action( 'wp_ajax_gfb_conformed_appointment',  array(&$this, 'gfbConformedAppointment') );
			add_action( 'init', array(&$this, 'gfbExportAppointmentToCsv') );
			add_action( 'init', array(&$this, 'gfbExportAppointmentToPdf') );
		}
		
		/* get service list from category */
		function syncAppointmentWithGoogleCal($headers){
			
			global $wpdb;
			global $gfbStaff;
			
			$X_Goog_Channel_ID = $headers['X-Goog-Channel-Id'];
			$X_Goog_Resource_ID = $headers['X-Goog-Resource-Id'];
			$X_Goog_Channel_Expiration = $headers['X-Goog-Channel-Expiration'];
			$X_Goog_Resource_State = $headers['X-Goog-Resource-State'];
			
			$capi = new GFB_GoogleCalendarApi();
			
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_staff_gcal_watch = $wpdb->prefix . 'gfb_staff_gcal_watch';
			
			$watchData = $wpdb->get_row( "SELECT staff_gcal_id,staff_id,staff_gcal_data,gcal_resource_id,gcal_next_sync_token FROM wp_gfb_staff_mst where gcal_channel_id='$X_Goog_Channel_ID' and gcal_resource_id='$X_Goog_Resource_ID' and is_deleted=0", ARRAY_A );
			//$watchData = $wpdb->get_row( "SELECT staff_gcal_id,staff_id,staff_gcal_data,gcal_resource_id,gcal_next_sync_token FROM wp_gfb_staff_mst where gcal_resource_id='$X_Goog_Resource_ID' and is_deleted=0", ARRAY_A );
			
			if(empty($watchData))
			{
				return json_encode( array('date'=>current_time("Y-m-d H:i:s"),'text' => __("Sorry! No data exists with $X_Goog_Channel_ID and $X_Goog_Resource_ID.", 'gfb')));
				
			}
			
			
			//print_r($watchData);exit;
			$syncToken = $watchData['gcal_next_sync_token'];
			$resourceId = $watchData['gcal_resource_id'];
			$calendarId = $watchData['staff_gcal_id'];
			$staffId = $watchData['staff_id'];
			
			if($watchData['staff_gcal_data']!='')
			{
				$accessToken = json_decode($watchData['staff_gcal_data'])->access_token;
			}
			
			
			if( $gfbStaff->gfbIsTokenExpire($staffId)) {
				$jsontoken = json_decode( $gfbStaff->gfbStaffGoogleDataEncode($staffId ));
				if( !empty( $jsontoken ) ) {						
					$gfbStaff->gfbRefreshToken($jsontoken->refresh_token,$staffId);
					$gcalJSONData = $gfbStaff->gfbStaffGoogleDataEncode($staffId);
					$gcalData = json_decode($gcalJSONData);
					//print_r($gcalData);exit;
					$accessToken = $gcalData->access_token;
				}				
			}
			//echo $accessToken;exit;
			
			if($syncToken=='')
			{
				//perform full sync
				$limitEvents = 0;
				$syncToken = $this->gfbGCalFullSync($accessToken, $calendarId, $limitEvents);
				
				//echo $syncToken;exit;
			}
			else
			{
				//perform incremental sync
				
				//get event updates from google calendar
				$eventUpdates = $capi->getEventUpdates($accessToken,$syncToken,'primary');
				$syncToken = $eventUpdates['nextSyncToken'];
				//print_r($eventUpdates);exit;
				
				if(isset($eventUpdates['error']) && $eventUpdates['error']['code']==410)
				{
					$limitEvents = 0;
					$syncToken = $this->gfbGCalFullSync($accessToken, $calendarId, $limitEvents);
					$eventUpdates = $capi->getEventUpdates($accessToken,$syncToken,'primary');
					//print_r($eventUpdates);exit;
				}
				
				if(!empty($eventUpdates['items']))
				{
					$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
					$staff_schedule_mapping_tbl = $wpdb->prefix . 'gfb_staff_schedule_mapping';	
					$time_slot_mst_tbl= $wpdb->prefix . 'gfb_time_slot_mst';
					
					foreach($eventUpdates['items'] as $item)
					{
						//get appointment details with eventid
						$appointmentDetails = $wpdb->get_row("SELECT appointment_id, customer_id, appointment_date, staff_slot_mapping_id, service_id, staff_id, booking_ref_no, slot_count, google_event_id, status FROM $gfb_appointments_mst where google_event_id='".$item['id']."' and is_deleted=0");
						
						if(!empty($appointmentDetails))
						{
							$appointmentStatus = $item['status'];
							if($appointmentStatus=='confirmed')
							{
								$status = 2;
								$eventStart = explode('T',$item['start']['dateTime']);
								$eventEnd = explode('T',$item['end']['dateTime']);
								//print_r($eventStart);exit;
								$appointmentDate = $eventStart[0];
								$weekDay = date('w', strtotime($appointmentDate));
								$startTimeString = explode('+',$eventStart[1]);
								$endTimeString = explode('+',$eventEnd[1]);
								$timeZone = $startTimeString[1];
								$startTime = date('H:i:s',strtotime($startTimeString[0]));
								$endTime = date('H:i:s',strtotime($endTimeString[0]));
								
								//get timeslot id for given time interval
								$timeSlotData = $wpdb->get_row("SELECT time_slot_id,max_appointment_capacity,is_custom_slot FROM $time_slot_mst_tbl 
		where ('$startTime' between slot_start_time and slot_end_time ) and ('$endTime' between slot_start_time and slot_end_time) and is_deleted=0 and weekday=$weekDay");
								if(!empty($timeSlotData))
								{
									$timeSlotId = $timeSlotData->time_slot_id;
								
									//get staff schedule mapping id
									$staffScheduleData = $wpdb->get_row("SELECT staff_slot_mapping_id,max_appointment_capacity FROM $staff_schedule_mapping_tbl where is_deleted=0 and staff_id=$staffId and time_slot_id=$timeSlotId");
									
									$staffSlotMappingId = $staffScheduleData->staff_slot_mapping_id;
									/* Check wheather time slot space is available or not */			
									$time_slot_result = $wpdb->get_results( "Select ssm.max_appointment_capacity, count(am.appointment_id) as TotalAppointment, ssm.staff_slot_mapping_id
							from ".$staff_schedule_mapping_tbl." ssm
							join ".$time_slot_mst_tbl." tm on ssm.time_slot_id=tm.time_slot_id  
							left join ".$gfb_appointments_mst." am on ssm.staff_slot_mapping_id=am.staff_slot_mapping_id and am.is_deleted=0  and am.appointment_date='".$appointmentDate."' and am.status = 2 
							where ssm.is_deleted=0  and ssm.staff_id='".$staffId."' and weekday='".$weekDay."' and tm.is_deleted=0 and ssm.staff_slot_mapping_id =$staffSlotMappingId	group by ssm.staff_slot_mapping_id,ssm.time_slot_id,tm.slot_start_time,tm.slot_end_time,ssm.max_appointment_capacity
							having count(am.appointment_id)<ssm.max_appointment_capacity", ARRAY_A );
									//print_r($time_slot_result);exit;
									if(empty($time_slot_result))
									{
										return json_encode( array('date'=>current_time("Y-m-d H:i:s"),'text' => __('Space Is Not Available. Please Try Different Date Or Time Slot.', 'gfb')));
									}
									
									//update appointment details
									$updateAppintmentData = array(
										'appointment_date' 		=> ''.wp_kses_post(trim($appointmentDate)).'',
										'staff_slot_mapping_id' => ''.wp_kses_post($staffSlotMappingId).'',
										'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
										'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
									);
									//print_r($updateAppintmentData);exit;
									$whereAppointmentId = array(
										'appointment_id'=>$appointmentDetails->appointment_id
									);
									
									$wpdb->update($gfb_appointments_mst, $updateAppintmentData,$whereAppointmentId);
								}
								else
								{
									return json_encode( array('date'=>current_time("Y-m-d H:i:s"),'text' => __('Sorry! No date and time available.', 'gfb')));
								}
							}
							else if($appointmentStatus=='cancelled')
							{
								$status = 3;
								$updateAppintmentData = array(
									'status' 		=> ''.wp_kses_post($status).'',
									'staff_slot_mapping_id' => ''.wp_kses_post($staffSlotMappingId).'',
									'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
								);
								
								$whereAppointmentId = array(
									'appointment_id'=>$appointmentDetails->appointment_id
								);
								
								$wpdb->update($gfb_appointments_mst, $updateAppintmentData,$whereAppointmentId);
							}
							
						}
					}
				}
			}
			
			$edit_staff_arg = array(
				'gcal_next_sync_token' => ''.wp_kses_post($syncToken).'',
				'modified_by' => ''.wp_kses_post(get_current_user_id()).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
			);
			
			$where_arg = array(
				'staff_id' 	=> ''.wp_kses_post($staffId).'',
			);							
			if(!$wpdb->update($gfb_staff_mst, $edit_staff_arg, $where_arg))
			{
				return json_encode( array('date'=>current_time("Y-m-d H:i:s"),'text' => __('Sorry! Error occurred while saving watch response.', 'gfb')));
			}
			
			//save google push request
			$gfb_gcal_push_req = $wpdb->prefix . 'gfb_gcal_push_req';
			
			$gcalPushData = array(
				'resource_id' => ''.wp_kses_post($resourceId).'',
				'used_sync_token' => ''.wp_kses_post($syncToken).'',
				'created_by' => ''.wp_kses_post(get_current_user_id()).'',
				'created_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
			);
			
			if(!$wpdb->insert($gfb_gcal_push_req, $gcalPushData))
			{
				return json_encode( array('date'=>current_time("Y-m-d H:i:s"), 'text' => __('Sorry! Error occurred while saving push request.', 'gfb')));
			}
			
			return json_encode( array('date'=>current_time("Y-m-d H:i:s"),'text' => __("Appointment updated with $X_Goog_Channel_ID and $X_Goog_Resource_ID.", 'gfb')));
			
		}
		
		function gfbGCalFullSync($accessToken, $calendarId, $limitEvents)
		{
			global $wpdb;
			$capi = new GFB_GoogleCalendarApi();
			
			$events = $capi->getEvents($accessToken, $calendarId, $limitEvents);
			$syncToken = $events['nextSyncToken'];
			//print_r($events);exit;
			if(!empty($events['items']))
			{
				$gfb_staff_gcal_events = $wpdb->prefix . 'gfb_staff_gcal_events';
				foreach($events['items'] as $item)
				{
					//check exists
					$checkExists = $wpdb->get_results("SELECT gcalid FROM $gfb_staff_gcal_events where eventid='".$item['id']."' and is_deleted=0");
					
					if(empty($checkExists))
					{
						//save staff gcal events
						$eventArray = array(
							'eventid' => $item['id'],
							'summary' => $item['summary'],
							'start' => date('Y-m-d H:i:s',strtotime($item['start']['dateTime'])),
							'end' => date('Y-m-d H:i:s',strtotime($item['end']['dateTime'])),
							'status' => $item['status'],
							'htmllink' => $item['htmlLink'],
							'timezone' => $item['start']['timeZone'],
							'created_by' => ''.wp_kses_post(get_current_user_id()).'',
							'created_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
							'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).''
						);
						//print_r($eventArray);
						if(!$wpdb->insert($gfb_staff_gcal_events, $eventArray))
						{
							echo json_encode( array('msg' => 'false', 'text' => __('Sorry! Error occurred while saving google calendar events.', 'gfb') ) );
							die();
						}
					}
				}
			}
			return $syncToken;
		}
		
		/* get service list from category */
		function gfbListServicesByCategory(){
			
			global $wpdb;
			$service_tbl = $wpdb->prefix . 'gfb_services_mst';	
			$category_tbl = $wpdb->prefix . 'gfb_categories_mst';	
			
			$service_result = $wpdb->get_results( "SELECT sm.service_id, sm.service_title, sm.service_price, sm.service_color, sm.category_id, cm.category_name as cat_name FROM ".$service_tbl." as sm 
LEFT JOIN ".$category_tbl." as cm ON sm.category_id = cm.category_id 
WHERE sm.is_deleted=0 and sm.category_id=".$_POST['category_id']." and cm.is_deleted=0 ORDER BY sm.service_title DESC", ARRAY_A );
			
			echo '<option value="">--Select Service--</option>';
			if( empty($service_result) ) {
				echo '<option value="">'.__('No Service Found', 'gfb').'</option>';
			}
			else {			
				foreach($service_result as $service)
				{
					echo '<option value='.$service['service_id'].'>' .$service['service_title'].  '</option>';
				} 					
			}
			die() ;
		}
		
		/* Get List of staff from service */
		function gfbListStaff(){
			
			global $wpdb;
			$staff_service_mapping_tbl = $wpdb->prefix . 'gfb_staff_service_mapping';	
			$staff_master_tbl= $wpdb->prefix . 'gfb_staff_mst';	
				
			$staff_result = $wpdb->get_results( "SELECT ssm.staff_id,sm.staff_name,service_price FROM ".$staff_service_mapping_tbl." as ssm inner join ".$staff_master_tbl." as sm on sm.staff_id=ssm.staff_id where ssm.service_id='".$_POST['service_id']."' and sm.is_deleted=0 and ssm.is_deleted=0 ", ARRAY_A ); 
			
			echo '<option value="">--Select Staff--</option>';
			if( empty($staff_result) ) {
				echo '<option value="">'.__('No Staff Found', 'gfb').'</option>';
			}
			else {
				foreach($staff_result as $staff)				
				{
					echo '<option value='.$staff['staff_id'].'>' .$staff['staff_name'].  '</option>';
				}
			}
			die() ;


		}
		
		/* Get data from appointment table using appointment id */
		function gfbAppointmentInfoById($appointmentId) {
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			
			$appointment_res = $wpdb->get_results( "SELECT am.appointment_id, am.customer_id, cm.customer_name, DATE_FORMAT(am.appointment_date, '%d %M, %Y') AS appointment_date, am.staff_slot_mapping_id, CONCAT(DATE_FORMAT(tm.slot_start_time, '%H:%i'), ' - ', DATE_FORMAT(tm.slot_end_time, '%H:%i')) as time_slot, am.staff_id, sfm.staff_name, am.service_id, sm.service_title, am.booking_ref_no, am.slot_count, am.google_event_id, am.status 
			FROM ".$gfb_appointments_mst." AS am 
			INNER JOIN ".$gfb_customer_mst." AS cm ON am.customer_id=cm.customer_id 
			INNER JOIN ".$gfb_services_mst." AS sm ON am.service_id=sm.service_id 
			INNER JOIN ".$gfb_staff_mst." AS sfm ON am.staff_id=sfm.staff_id 
			INNER JOIN ".$gfb_staff_schedule_mapping." AS ssm ON am.staff_slot_mapping_id=ssm.staff_slot_mapping_id 
			INNER JOIN ".$gfb_time_slot_mst." AS tm ON ssm.time_slot_id=tm.time_slot_id 
			WHERE am.is_deleted=0 AND am.appointment_id=".$appointmentId."", ARRAY_A);
			return $appointment_res;
		}
		
		/* Get data from appointment table using appointment id */
		function gfbAppointmentInfo($appointmentId) {
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			
			$appointment_res = $wpdb->get_results( "SELECT am.appointment_id, am.customer_id, cm.customer_name, DATE_FORMAT(am.appointment_date, '%d %M, %Y') AS appointment_date, am.staff_slot_mapping_id, CONCAT(DATE_FORMAT(tm.slot_start_time, '%H:%i'), ' - ', DATE_FORMAT(tm.slot_end_time, '%H:%i')) as time_slot, am.staff_id, sfm.staff_name, am.service_id, sm.service_title, am.slot_count, am.booking_ref_no, am.google_event_id, am.status 
			FROM ".$gfb_appointments_mst." AS am 
			INNER JOIN ".$gfb_customer_mst." AS cm ON am.customer_id=cm.customer_id 
			INNER JOIN ".$gfb_services_mst." AS sm ON am.service_id=sm.service_id 
			INNER JOIN ".$gfb_staff_mst." AS sfm ON am.staff_id=sfm.staff_id 
			INNER JOIN ".$gfb_staff_schedule_mapping." AS ssm ON am.staff_slot_mapping_id=ssm.staff_slot_mapping_id 
			INNER JOIN ".$gfb_time_slot_mst." AS tm ON ssm.time_slot_id=tm.time_slot_id 
			WHERE am.is_deleted=0 AND am.appointment_id=".$appointmentId."", ARRAY_A);
			return $appointment_res;
		}
		
		/* Appointment List */
		function gfbListAppointments(){
			
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			
			$appointment_result = $wpdb->get_results( 'SELECT am.appointment_id, cm.customer_name, am.internal_notes, DATE_FORMAT(am.appointment_date, "%Y-%m-%d") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm. 	staff_name, 
			CASE am.status 
			WHEN 1 THEN "Pending"
			WHEN 2 THEN "Approved"
			WHEN 3 THEN "Cancelled"
			END AS status
			FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
			WHERE am.is_deleted=0 
			ORDER BY am.'.$orderby.' '.$order.'', ARRAY_A );				
			return $appointment_result;	
		}
		
		
		function gfbAddAppoinmentFromAdminSide() {
			
			global $wpdb;	
			global $gfbStaff;		
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$staff_schedule_mapping_tbl = $wpdb->prefix . 'gfb_staff_schedule_mapping';	
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';
			$time_slot_mst_tbl= $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$bookingreferenceno = wp_rand( 1, 99999 ).wp_kses_post(date("Ymd"));			
			$msg = '';
			$customer_id = '';
			$weekday = date('w', strtotime($_POST['appointment_date']));
			$appointment_datte = date('Y-m-d', strtotime($_POST['appointment_date']));
			
			if( empty(trim($appointment_datte)) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Please select appointment date.') );
				die();
			}
			elseif( empty(trim($_POST["appointmentcategory"])) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Please select category.') );
				die();
			}
			elseif( empty(trim($_POST["service"])) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Please select service.') );
				die();
			}
			elseif( empty(trim($_POST["staff"])) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Please select staff member.') );
				die();
			}
			elseif( empty(trim($_POST["time_slot"])) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Please select time slot.') );
				die();
			}
			else {
				
				/* Check wheather time slot space is available or not */			
				$time_slot_result = $wpdb->get_results( "Select ssm.max_appointment_capacity, count(am.appointment_id) as TotalAppointment, ssm.staff_slot_mapping_id
		from ".$staff_schedule_mapping_tbl." ssm
		join ".$time_slot_mst_tbl." tm on ssm.time_slot_id=tm.time_slot_id  
		left join ".$gfb_appointments_mst." am on ssm.staff_slot_mapping_id=am.staff_slot_mapping_id and am.is_deleted=0  and am.appointment_date='".$appointment_datte."' and am.status = 2 
		where ssm.is_deleted=0  and ssm.staff_id='".$_POST['staff']."' and weekday='".$weekday."' and tm.is_deleted=0 and ssm.staff_slot_mapping_id ='".$_POST['time_slot']."'
		group by ssm.staff_slot_mapping_id,ssm.time_slot_id,tm.slot_start_time,tm.slot_end_time,ssm.max_appointment_capacity
		having count(am.appointment_id)<ssm.max_appointment_capacity", ARRAY_A );
				
				if( empty($time_slot_result))
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Space Is Not Available. Please Try Different Date Or Time Slot.') );
					die();
				}
				else
				{			
					if( $_POST["new-customer"] == "2")
					{
						
						if( empty($_POST["customer"]) ) {
							echo json_encode( array('msg' => 'false', 'text' => 'Please select customer.') );
							die();
						}
						else {						
							$customer_id = $_POST["customer"];		
						}
						
					}
					elseif( $_POST["new-customer"] == "1")
					{
						
						if( empty(trim($_POST["customer_name"])) ) {
							echo json_encode( array('msg' => 'false', 'text' => 'Please enter customer name.') );
							die();
						}
						elseif( empty(trim($_POST["customer_email"])) ) {
							echo json_encode( array('msg' => 'false', 'text' => 'Please enter customer email id.') );
							die();
						}
						elseif( empty(trim($_POST["customer_contact"])) ) {
							echo json_encode( array('msg' => 'false', 'text' => 'Please enter customer contact no.') );
							die();
						}
						else 
						{
							//check customer if exists			
							$ifExists = $wpdb->get_results("SELECT customer_id FROM ".$gfb_customer_mst." where customer_email='".$_POST['customer_email']."' and customer_contact='".$_POST['customer_contact']."' and is_deleted=0", ARRAY_A);
													
							if(empty($ifExists))
							{
								$password="827ccb0eea8a706c4c34a16891f84e7b";		
								$user_id = wp_create_user( trim($_POST['customer_name']), trim($password), trim($_POST['customer_email']) );
										
								if( is_wp_error($user_id) ) {
									echo json_encode( array('msg' => 'false', 'text' => $user_id->get_error_message() ) );
									die();
								}else{
									// Add  role to new user 
									$user = new WP_User($user_id);
									$user->set_role('gfb_customer_role');
									
									$gfb_customer_array = array(
										/* 'user_id'      => ''.wp_kses_post($user_id).'', */
										'customer_name'    => ''.wp_kses_post(trim($_POST['customer_name'])).'',
										'customer_email'   => ''.wp_kses_post(trim($_POST['customer_email'])).'',
										'customer_contact' => ''.wp_kses_post(trim($_POST['customer_contact'])).'',
										'created_date'     => ''.current_time("Y-m-d H:i:s").'',
										'ip_address' 	   => ''.wp_kses_post($gfb_ip).'',
									);
									
									$insert_customer = $wpdb->insert($gfb_customer_mst, $gfb_customer_array);
									$customer_id = $wpdb->insert_id;
								}
							}
							else 
							{
								echo json_encode( array('msg' => 'false', 'text' => 'Customer Already Exists.') );
								die();		
							}						
						}
					}
					
								
					//check if appointment exists				
					$appointmentExists = $wpdb->get_results("SELECT appointment_id FROM ".$gfb_appointments_mst." where appointment_date='".$appointment_datte."' and staff_slot_mapping_id='".$_POST['time_slot']."' and is_deleted=0 and service_id='".$_POST['service']."' and staff_id='".$_POST['staff']."' and customer_id='".$customer_id."' and status=2", ARRAY_A);				
									
					if( empty($appointmentExists) ) 
					{
						/* Insert with customer */
						$add_appointments_arg = array(
							'customer_id' 			=> ''.wp_kses_post(trim($customer_id)).'',
							'appointment_date' 		=> ''.wp_kses_post(trim($appointment_datte)).'',
							'staff_slot_mapping_id' => ''.wp_kses_post(trim($_POST['time_slot'])).'',
							'service_id' 			=> ''.wp_kses_post(trim($_POST['service'])).'',
							'staff_id'  			=> ''.wp_kses_post(trim($_POST['staff'])).'',
							'status' 				=> '2',
							'created_by' 			=> ''.wp_kses_post($gfb_createdby).'',
							'created_date'  		=> ''.current_time("Y-m-d H:i:s").'',
							'ip_address' 			=> ''.wp_kses_post($gfb_ip).'',
							'booking_ref_no' 		=> ''.trim($bookingreferenceno).'',
						);
						
						$insert_appointment = $wpdb->insert($gfb_appointments_mst, $add_appointments_arg);	
						$appointment_id = $wpdb->insert_id;
	
						if($insert_appointment)
						{						
							$serviceprice = $wpdb->get_results("SELECT service_price FROM ".$gfb_staff_service_mapping." where is_deleted=0 and service_id='".$_POST['service']."' and staff_id='".$_POST['staff']."'", ARRAY_A);
													
							// Insert payment 
							$add_payment_arg = array(
								'appointment_id' => ''.wp_kses_post(trim($appointment_id)).'',
								'payment_date' 	 => ''.wp_kses_post(trim($appointment_datte)).'',
								'payment_type'   => '3',
								'total_amt' 	 => ''.wp_kses_post(ltrim($serviceprice[0]['service_price'],''.get_option('gfb_currency_symbol').'')).'',
								'paid_amt'  	 => ''.wp_kses_post(ltrim($serviceprice[0]['service_price'],''.get_option('gfb_currency_symbol').'')).'',
								'payment_status' => '0',
								'currency_code'  => ''.wp_kses_post(trim(get_option('gfb_currency_code'))).'',
								'created_by' 	 => ''.wp_kses_post($gfb_createdby).'',
								'created_date'   => ''.current_time("Y-m-d H:i:s").'',
								'ip_address' 	 => ''.wp_kses_post($gfb_ip).'',
							);
							
							
							$insert_payment = $wpdb->insert($gfb_payments_mst, $add_payment_arg);
							
							if( !empty( get_option("gfb_client_id") ) && !empty( get_option("gfb_client_secrete") ) )	
							{
								$staffGoogleData = $gfbStaff->gfbStaffGoogleData($_POST['staff']);	
								if($staffGoogleData[0]['staff_gcal_data'] != '')
								{		
									$google_event_id = $this->createGoogleCalendarEvent($add_appointments_arg);
									if(!empty($google_event_id))
									{
										$updateAppointment = array(
											'google_event_id' => $google_event_id
										);
										
										$whereAppointment = array( 'appointment_id' => $appointment_id  );
										
										$updateAppointment = $wpdb->update($gfb_appointments_mst, $updateAppointment, $whereAppointment);
									}
								}
							}
							
							$mail = GFB_Core::gfbReplaceCode($appointment_id, "gfb_user_appointment_confirmation", "gfb_staff_appointment_request", "gfb_admin_appointment_request");
							
						}
						
						if($insert_appointment)
						{	
							echo json_encode( array('msg' => 'true', 'text' => 'Appointment booked successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-appointments') );
							die();
						}
						else 
						{
							echo json_encode( array('msg' => 'false', 'text' => 'OOps!..Appointment not booked.') );
							die();	
						}
					}
					else 
					{	
						echo json_encode( array('msg' => 'false', 'text' => 'Appointment Already Exists.') );
						die();
					}	
				}
			}
			die();
		}
		
		function createGoogleCalendarEvent($appointment_data)
		{			
			global $wpdb;
			global $gfbStaff;
			global $gfbServiceObj;
			global $gfbTimeSlotObj;
			
			$capi = new GFB_GoogleCalendarApi();
			
			$staff_result = $gfbStaff->gfbStaffGoogleData($appointment_data['staff_id']);	
			
			$service = $gfbServiceObj->gfbGetServiceTitle($appointment_data['service_id']);
			
			$timeslot = $gfbTimeSlotObj->gfbGetTimeSlotValue($appointment_data['staff_slot_mapping_id']);
			
			$start_time = date('Y-m-d',strtotime($appointment_data['appointment_date'])).'T'.$timeslot[0]['slot_start_time'];
			$end_time = date('Y-m-d',strtotime($appointment_data['appointment_date'])).'T'.$timeslot[0]['slot_end_time'];
			
			$event_time = array(				
				'start_time' =>$start_time,
				'end_time'   =>$end_time,			
			);
			
		
			$servie_name = $service[0]['service_title'];
			
			$google_data = json_decode($staff_result[0]['staff_gcal_data'],true);
						
			$user_timezone = $capi->GetUserCalendarTimezone($google_data['access_token']);	
						
			$calendar_id = $capi->gfbGetCalendarId($appointment_data['staff_id']);	
			
			// Create event on primary calendar
			$event_id = $capi->CreateCalendarEvent( $calendar_id, $servie_name, $event_time, $user_timezone, $google_data['access_token']);
			
			return $event_id; 
		}
		
		/* Get Appointment List Page  on filter */
		function gfbGetAppointmentListByValue() {			
			if (isset($_POST['form_data'])):
				require(GFB_ADMIN_TAB . 'appointments/filteredAppointmentList.php');
			endif;
			die();
		}
		
		/* Get Appointment List  on filter */
		function gfbGetAppointmentListByDate($date = '', $type = '') {			
			
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			
			if( $type == '') {
				$stype = "(1, 2, 3, 4)";	
			}
			else {
				$stype = "(".$type.")";
			}
			
			$appointment_results = $wpdb->get_results('SELECT am.appointment_id, cm.customer_name,  DATE_FORMAT(am.appointment_date, "%d %M, %Y") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm.staff_name, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot , am.status as statusno, CASE am.status WHEN 1 THEN "Pending" WHEN 2 THEN "Awaiting" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Visited" END AS statustxt
			FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id AND cm.is_deleted=0
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id AND sm.is_deleted=0
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id AND sfm.is_deleted=0
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id AND ssm.is_deleted=0
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 AND am.appointment_date="'.$date.'" AND am.status in '.$stype.' ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A);
			
			return $appointment_results;
		}
		
		
		/* List TimeSlot From Weekday  & staff id */
		function gfbGetStaffTimeSlot(){
			
			global $wpdb;
			global $gfbStaff;
			
			if( !empty( get_option("gfb_client_id") ) && !empty( get_option("gfb_client_secrete") ) )	
			{
				/* Check token is expired or not */
				if( $gfbStaff->gfbIsTokenExpire( $_POST['staff_id'] ) ) {	
					$jsontoken = json_decode( $gfbStaff->gfbStaffGoogleDataEncode( $_POST['staff_id'] ) );
					if( !empty( $jsontoken ) ) {						
						$refreshtoken = $gfbStaff->gfbRefreshToken($jsontoken->refresh_token,$_POST['staff_id']);
					}				
				}
				
				/* if( $gfbStaff->gfbIsWatchRequestExpire($_POST['staff_id'])) {
					$staff_mst_tbl = $wpdb->prefix . 'gfb_staff_mst';	
					
					$googleData = $wpdb->get_row( "SELECT staff_gcal_data, staff_gcal_id , staff_id FROM ".$staff_mst_tbl." where staff_id='".$_POST['staff_id']."' and is_deleted=0");
					if(!empty($googleData->staff_gcal_data))
					{
						$capi = new GFB_GoogleCalendarApi();
						$accessToken = json_decode($googleData->staff_gcal_data)->access_token;
						$calendarId = $googleData->staff_gcal_id;
						//$staff_id = $googleData->staff_id;
						$wathResponse = $capi->createWatchRequest($accessToken, $calendarId);
						//print_r($wathResponse);exit;
						$channel_id = json_decode($wathResponse)->id;
						$resource_id = json_decode($wathResponse)->resourceId;
						//update staff data
						$edit_staff_arg = array(
							'gcal_channel_id' => ''.wp_kses_post($channel_id).'',
							'gcal_resource_id' => ''.wp_kses_post($resource_id).'',
							'staff_gcal_watch_data' => ''.wp_kses_post($wathResponse).'',
							'modified_by' => ''.wp_kses_post(get_current_user_id()).'',
							'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
							'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
						);
						
						$where_arg = array(
							'staff_id' 	=> ''.wp_kses_post($_POST['staff_id']).'',
						);							
						if(!$wpdb->update($staff_mst_tbl, $edit_staff_arg, $where_arg))
						{
							echo json_encode( array('msg' => 'false', 'text' => __('Sorry! Error occurred while saving watch response.', 'gfb')));
							die();
						}
					}
					
				} */
			}
			/* call time slot */	
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$staff_schedule_mapping_tbl = $wpdb->prefix . 'gfb_staff_schedule_mapping';	
			$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';	
			$time_slot_mst_tbl= $wpdb->prefix . 'gfb_time_slot_mst';	
			$weekday = date('w', strtotime($_POST['appointment_date']));
			$appointment_datte = date('Y-m-d', strtotime($_POST['appointment_date']));
		

			/* Holiday exists */
			$holiday_exists = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_staff_holiday_mapping." hm WHERE is_deleted=0 and staff_id='".$_POST['staff_id']."' and holiday_date='".$appointment_datte."' ");
			
			if( empty( $holiday_exists ) ) {
				
				$time_slot_result = $wpdb->get_results( "Select ssm.staff_slot_mapping_id,ssm.time_slot_id,concat(  DATE_FORMAT(tm.slot_start_time, '%H:%i'),'-', DATE_FORMAT(tm.slot_end_time, '%H:%i') ) as time_slot,ssm.max_appointment_capacity,count(am.appointment_id) as TotalAppointment
	from ".$staff_schedule_mapping_tbl." ssm
	join ".$time_slot_mst_tbl." tm on ssm.time_slot_id=tm.time_slot_id  
	left join ".$gfb_appointments_mst." am on ssm.staff_slot_mapping_id=am.staff_slot_mapping_id and am.is_deleted=0  and am.appointment_date='".$appointment_datte."' and status = 2
	where ssm.is_deleted=0  and ssm.staff_id='".$_POST['staff_id']."' and weekday='".$weekday."' and tm.is_deleted=0
	group by ssm.staff_slot_mapping_id,ssm.time_slot_id,tm.slot_start_time,tm.slot_end_time,ssm.max_appointment_capacity
	having count(am.appointment_id)<ssm.max_appointment_capacity", ARRAY_A );
				
				echo '<option value="">--Select Time Slots--</option>';
				if( empty($time_slot_result) ) {
					echo '<option value="notimeslot">'.__('No Time Slots Found', 'gfb').'</option>';
				}
				else {
					foreach($time_slot_result as $time_slot)				
					{
						echo '<option value='.$time_slot['staff_slot_mapping_id'].'>' .$time_slot['time_slot'].  '</option>';
					}
				}
			
			}
			else {
				echo '<option value="notimeslot">'.__('Staff Member is on a Dayoff', 'gfb').'</option>';	
			}
			die() ;		
		}
		
		/* Delete Appointment */
		function gfbDeleteAppointment() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			$delete_array = array(
				'is_deleted'    => 1,
				'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			$delete_where = array(
				'appointment_id' => $_POST["appointment_id"]
			);

			$delete_appointment = $wpdb->update($gfb_appointments_mst, $delete_array, $delete_where);

			if($delete_appointment)
			{	
				if( !empty( get_option("gfb_client_id") ) && !empty( get_option("gfb_client_secrete") ) )	
				{
					$calendar_res = $this->gfbGetCalendarDetails(wp_kses_post($_POST["appointment_id"]));
					
					if($calendar_res[0]["google_event_id"])
					{					
						/* delete from calendar */
						$capi = new GFB_GoogleCalendarApi();
					
						$access_token_data = json_decode($calendar_res[0]["staff_gcal_data"]);				
					
						$delete_event = $capi->gfbDeleteCalendarEvent($calendar_res[0]["google_event_id"], $calendar_res[0]["staff_gcal_id"], $access_token_data->access_token);					
					}
				}
				$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
				$paymentData = $wpdb->get_row("SELECT payment_id FROM $gfb_payments_mst where appointment_id=".$_POST["appointment_id"]." and is_deleted=0 and payment_status =0 and payment_type=3");
				//print_r($paymentData);exit;
				
				if(!empty($paymentData))
				{
					
					$delete_array_payment = array(
						'is_deleted'    => 1,
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$delete_where = array(
						'payment_id' => $paymentData->payment_id
					);
					$wpdb->update($gfb_payments_mst, $delete_array_payment, $delete_where);
				}
		
				echo json_encode( array('msg' => 'true', 'text' => 'Appointment is deleted.', 'url' => admin_url().'admin.php?page=gravity-form-booking-appointments') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Oops!..Appointment not deleted.') );
				die();
			}	
		}
		
		/* Cancel Appointment */
		function gfbCancelAppointment() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
						
			$delete_array = array(
				'status'    	=> 3,
				'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			$delete_where = array(
				'appointment_id' => ''.wp_kses_post($_POST["appointment_id"]).'',
			);
			
			$delete_appointment = $wpdb->update($gfb_appointments_mst, $delete_array, $delete_where);
			
			if($delete_appointment)
			{	
				if( !empty( get_option("gfb_client_id") ) && !empty( get_option("gfb_client_secrete") ) )	
				{
					$calendar_res = $this->gfbGetCalendarDetails(wp_kses_post($_POST["appointment_id"]));
					
					if($calendar_res[0]["google_event_id"])
					{					
						/* delete from calendar */
						$capi = new GFB_GoogleCalendarApi();
					
						$access_token_data = json_decode($calendar_res[0]["staff_gcal_data"]);						
					
						$delete_event = $capi->gfbDeleteCalendarEvent($calendar_res[0]["google_event_id"], $calendar_res[0]["staff_gcal_id"], $access_token_data->access_token);					
					}
				}
		
				$mail = GFB_Core::gfbReplaceCode($_POST["appointment_id"], "gfb_user_appointment_cancellation", "gfb_staff_appointment_cancelled", "gfb_admin_appointment_cancellation");
									
				echo json_encode( array('msg' => 'true', 'text' => 'Appointment cancelled.') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'OOps!..Appointment is not cancelled.') );
				die();
			}	
		}
		
		/* Visited Appointment */
		function gfbVisitAppointment() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			$delete_array = array(
				'status'    	=> 4,
				'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			$delete_where = array(
				'appointment_id' => $_POST["appointment_id"]
			);
			
			$delete_appointment = $wpdb->update($gfb_appointments_mst, $delete_array, $delete_where);
			
			if($delete_appointment)
			{	
				/* $gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
				$paymentData = $wpdb->get_row("SELECT payment_id FROM $gfb_payments_mst where appointment_id=".$_POST["appointment_id"]." and is_deleted=0 and payment_status=0 and payment_type=3");
				//print_r($paymentData);exit;
				
				if(!empty($paymentData))
				{
					
					$delete_array = array(
						'payment_status'    	=> 1,
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$delete_where = array(
						'payment_id' => $paymentData->payment_id
					);
					$wpdb->update($gfb_payments_mst, $delete_array, $delete_where);
				} */
				
				
				$mail = GFB_Core::gfbReplaceCode($_POST["appointment_id"], "gfb_user_visit_confirmation", "", "gfb_admin_customer_visit");
									
				echo json_encode( array('msg' => 'true', 'text' => 'Appointment Status Changed as "Visited".') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'OOps! Status is not changed.') );
				die();
			}	
		}
		
		
		
		/* Conformed Appointment */
		function gfbConformedAppointment() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			$delete_array = array(
				'status'    	=> 2,
				'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			$delete_where = array(
				'appointment_id' => $_POST["appointment_id"]
			);
			
			$delete_appointment = $wpdb->update($gfb_appointments_mst, $delete_array, $delete_where);
			
			if($delete_appointment)
			{	
				/* $gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
				$paymentData = $wpdb->get_row("SELECT payment_id FROM $gfb_payments_mst where appointment_id=".$_POST["appointment_id"]." and is_deleted=0 and payment_status=0 and payment_type=3");
				//print_r($paymentData);exit;
				
				if(!empty($paymentData))
				{
					
					$delete_array = array(
						'payment_status'    	=> 1,
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$delete_where = array(
						'payment_id' => $paymentData->payment_id
					);
					$wpdb->update($gfb_payments_mst, $delete_array, $delete_where);
				} */
				
				/* Send Email */
				$mail = GFB_Core::gfbReplaceCode($_POST["appointment_id"], "gfb_user_appointment_confirmation","gfb_staff_appointment_request", "gfb_admin_appointment_request");
				
				/* $mail = GFB_Core::gfbReplaceCode($_POST["appointment_id"], "gfb_user_visit_confirmation", "", "gfb_admin_customer_visit");*/
				
				echo json_encode( array('msg' => 'true', 'text' => 'Appointment Status Changed as "Awaiting".') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'OOps! Status is not changed.') );
				die();
			}	
		}
		
		
		
		
		/* Export To CSV for appointment list */
		
		function gfbExportAppointmentToCsv() {
						
			if(isset($_REQUEST['gfb_appointment_export_to_csv']) && $_REQUEST['gfb_appointment_export_to_csv'] == 'gfb_export_csv') {
				
				global $wpdb;
				
				$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
				$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
				$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
				$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
				$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
				$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';	
				
				if( isset($_GET["status"]) && base64_decode($_GET["status"]) == '1' ) { 
						echo"Ahsan";
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 2 THEN "Awating" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '2' ) { 
				
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 2 THEN "Awating" END AS status FROM '.$gfb_appointments_mst.' AS am 
					INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
					INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id
					INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
					INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id
					INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
					WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '3' ) { 
				
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 3 THEN "Cancelled" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '4' ) { 
				
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 4 THEN "Visited" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id 
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}
				else {
					
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 1 THEN "Pending" WHEN 2 THEN "Awating" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Visited" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id 
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );	
			
				}
			
				header('Content-type: text/csv');
				header('Content-Disposition: attachment; filename="appoinment_list.csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
	
				$file = fopen('php://output', 'w');
				
				$column_list = array( "Booking Number",  "Slot", "Customer Name", "Customer Email", "Service Name", "Staff Name", "Appointment Date", "Appointment Time", "Appointment Status");
	
				fputcsv($file, $column_list);
						
				foreach ($appointmentList as $appointment) {
					fputcsv($file, $appointment);
				}
	
				exit();
			}
		}
		
		public function gfbGetAppointmentData($appointment_id) {
			global $wpdb;				
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';	
			$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';	
			
			$appointment_result = $wpdb->get_results( 'SELECT am.staff_id, am.appointment_id, am.appointment_date, am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, cm.customer_contact, sm.service_title, sm.service_price, sfm.staff_name, staff_email, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, pm.total_amt
FROM '.$gfb_appointments_mst.' AS am
INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id=cm.customer_id AND cm.is_deleted=0
INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id=sm.service_id AND sm.is_deleted=0
INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id=sfm.staff_id AND sfm.is_deleted=0
INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id=ssm.staff_slot_mapping_id AND ssm.is_deleted=0
INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id=tm.time_slot_id AND tm.is_deleted=0
INNER JOIN '.$gfb_payments_mst.' AS pm ON am.appointment_id=pm.appointment_id
WHERE am.is_deleted=0 AND am.appointment_id="'.$appointment_id.'"', ARRAY_A );
			return $appointment_result;
		}
		
		public function gfbGetCalendarDetails($appointment_id) {
			global $wpdb;				
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';	
			
			$calendar_result = $wpdb->get_results( 'SELECT am.appointment_id, am.staff_id, am.google_event_id, sfm.staff_gcal_data, sfm.staff_gcal_id 
FROM '.$gfb_appointments_mst.' AS am
INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id=sfm.staff_id AND sfm.is_deleted=1
WHERE am.is_deleted=1 AND am.appointment_id="'.$appointment_id.'"', ARRAY_A );
			return $calendar_result;	
		}

		
		/* Export To PDF */		
		function gfbExportAppointmentToPdf() {
			
			if(isset($_REQUEST['gfb_appointment_export_to_pdf']) && $_REQUEST['gfb_appointment_export_to_pdf'] == 'gfb_export_pdf') {
				
				global $wpdb;
				global $gfbFpdfObj;
				global $title;
				
				$title = 'Appointment List';
				$gfbFpdfObj->SetTitle($title);				
				$gfbFpdfObj->AddPage('L', 'Legal'); 
				
				$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
				$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
				$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
				$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
				$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
				$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';	
				// $gfb_slot_quant = $wpdb->prefix . 'slot_count';	
				
				
				if( isset($_GET["status"]) && base64_decode($_GET["status"]) == '1' ) { 
					
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 2 THEN "Awating" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id 
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '2' ) { 
				
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, cm.customer_name, am.slot_count, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 3 THEN "Cancelled" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id 
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '3' ) { 
				
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 3 THEN "Cancelled" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id 
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '4' ) { 
				
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 4 THEN "Visited" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 and am.status="'.base64_decode($_GET["status"]).'" and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );
			
				}
				else {
					
					$appointmentList = $wpdb->get_results( 'SELECT am.booking_ref_no, am.slot_count, cm.customer_name, cm.customer_email, sm.service_title,  sfm.staff_name, DATE_FORMAT(am.appointment_date, "%M %d,%Y") as appointment_date, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, CASE am.status WHEN 1 THEN "Pending" WHEN 2 THEN "Awating" WHEN 3 THEN "Cancelled" WHEN 4 THEN "Visited" END AS status FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A );	
			
				}
			
				if($appointmentList) {
					
					$width_cell=array(6,43, 43, 43, 43, 43, 43, 9, 40, 30);
					$gfbFpdfObj->SetFont(apply_filters( 'gfb_change_pdf_font', 'Arial'),'B',10);
					
					$gfbFpdfObj->SetFillColor(255,255,255); // Background color of header 
					// Header starts /// 
					
					$gfbFpdfObj->Cell($width_cell[0],10,'ID',1,0,'C',true);
					$gfbFpdfObj->Cell($width_cell[1],10,'Booking Number',1,0,'C',true);
					$gfbFpdfObj->Cell($width_cell[2],10,'Customer Name',1,0,'C',true);
					$gfbFpdfObj->Cell($width_cell[3],10,'Customer Email',1,0,'C',true);
					$gfbFpdfObj->Cell($width_cell[4],10,'Service Name',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[5],10,'Staff Name',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[6],10,'Appointment Date',1,0,'C',true); 				
					$gfbFpdfObj->Cell($width_cell[7],10,'Slot',1,0,'C',true); 				
					$gfbFpdfObj->Cell($width_cell[8],10,'Appointment Time',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[9],10,'Status',1,0,'C',true); 
					$gfbFpdfObj->Ln();
					//// Header ends ///////
					
					$gfbFpdfObj->SetFont(apply_filters( 'gfb_change_pdf_font', 'Arial'),'',9);
					$gfbFpdfObj->SetFillColor(235,236,236); // Background color of header 
					$fill=false; // to give alternate background fill color to rows 
					$countid=1;
					
					/// each record is one row  ///
					foreach ($appointmentList as $row) {
						$gfbFpdfObj->Cell($width_cell[0],10,$countid,1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[1],10,$row['booking_ref_no'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[2],10,$row['customer_name'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[3],10,$row['customer_email'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[4],10,$row['service_title'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[5],10,$row['staff_name'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[6],10,$row['appointment_date'],1,0,'C',$fill);					
						$gfbFpdfObj->Cell($width_cell[7],10,$row['slot_count'],1,0,'C',$fill);					
						$gfbFpdfObj->Cell($width_cell[8],10,$row['time_slot'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[9],10,$row['status'],1,0,'C',$fill);
						$gfbFpdfObj->Ln();
						$fill = !$fill; // to give alternate background fill  color to rows
						$countid++;
					}
					/// end of records ///
				
				}
				
				$gfbFpdfObj->Output("appointment_list.pdf", "D");
			}
		}
	
	}
	
	global $gfbAppointmentObj;	
	$gfbAppointmentObj = new GravityFormBookingAppointments();
	

<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingCustomer {
		
		function __construct() {
			
			add_action( 'wp_ajax_gfb_delete_customer', array( &$this, 'gfbDeleteCustomer' ) );
			add_action( 'wp_ajax_gfb_edit_customer', array( &$this, 'gfbUpdateCustomer' ) );
			add_action( 'wp_ajax_gfb_display_customer_editform', array( &$this, 'gfbCustomerEditForm' ) );
			add_action( 'init', array(&$this, 'gfbExportCustomerToCsv') );
			add_action( 'init', array(&$this, 'gfbExportCustomerToPdf') );
		}
		
		/* Export To CSV */		
		function gfbExportCustomerToCsv() {
			
			if(isset($_REQUEST['gfb_customer_export_to_csv']) && $_REQUEST['gfb_customer_export_to_csv'] == 'gfb_export_csv') {
				
				global $wpdb;
				
				$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";	
				
				$customerList = $wpdb->get_results( "SELECT customer_name, customer_email, customer_contact FROM ".$gfb_customer_mst." WHERE is_deleted=0 ORDER BY customer_name asc", ARRAY_A );					
		
				header('Content-type: text/csv');
				header('Content-Disposition: attachment; filename="customer_list.csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
	
				$file = fopen('php://output', 'w');
				
				$column_list = array("Customer Name", "Customer Email", "Customer Contact No");
	
				fputcsv($file, $column_list);
						
				foreach ($customerList as $customer) {
					fputcsv($file, $customer);
				}
	
				exit();
			}
		}
		
		/* Export To PDF */		
		function gfbExportCustomerToPdf() {
			
			if(isset($_REQUEST['gfb_customer_export_to_pdf']) && $_REQUEST['gfb_customer_export_to_pdf'] == 'gfb_export_pdf') {
				
				global $wpdb;
				global $gfbFpdfObj;
				global $title;
				
				$title = 'Customer List';
				$gfbFpdfObj->SetTitle($title);				
				$gfbFpdfObj->AddPage();
				
				$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";			
					
				$customerList = $wpdb->get_results( "SELECT customer_name, customer_email, customer_contact FROM ".$gfb_customer_mst." WHERE is_deleted=0 ORDER BY customer_name asc", ARRAY_A );	
				
				if($customerList) {
				
					$width_cell=array(20,60, 60, 40);
					$gfbFpdfObj->SetFont(apply_filters( 'gfb_change_pdf_font', 'Arial'),'B',14);
					
					$gfbFpdfObj->SetFillColor(255,255,255); // Background color of header 
					// Header starts /// 
					
					$gfbFpdfObj->Cell($width_cell[0],10,'ID',1,0,'C',true);
					$gfbFpdfObj->Cell($width_cell[1],10,'NAME',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[2],10,'EMAIL ID',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[3],10,'CONTACT NO',1,0,'C',true); 
					$gfbFpdfObj->Ln();
					//// Header ends ///////
					
					$gfbFpdfObj->SetFont(apply_filters( 'gfb_change_pdf_font', 'Arial'),'',12);
					$gfbFpdfObj->SetFillColor(235,236,236); // Background color of header 
					$fill=false; // to give alternate background fill color to rows 
					$countid=1;
					
					/// each record is one row  ///
					foreach ($customerList as $row) {
						$gfbFpdfObj->Cell($width_cell[0],10,$countid,1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[1],10,$row['customer_name'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[2],10,$row['customer_email'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[3],10,$row['customer_contact'],1,0,'C',$fill);
						$gfbFpdfObj->Ln();
						$fill = !$fill; // to give alternate background fill  color to rows
						$countid++;
					}
					/// end of records ///
				
				}
				
				$gfbFpdfObj->Output("customer_list.pdf", "D");
			}
		}
		
		/* Edit Customer Form */
		function gfbCustomerEditForm(){
			
			if (isset($_POST['customer_id'])):
				require( GFB_ADMIN_TAB . 'customers/editCustomer.php' );
			endif;
			die();	
		}
		

		function gfbListCustomer(){
			
			global $wpdb;
			$customer_tbl = $wpdb->prefix . 'gfb_customer_mst';	
				
			 $customer_result = $wpdb->get_results( "SELECT customer_id,customer_name,customer_email,customer_contact FROM ".$customer_tbl." where is_deleted=0 ", ARRAY_A ); 
			 			 
			 return $customer_result;
		}
		
		function gfbCustomerNameList(){
			
			global $wpdb;
			$customer_tbl = $wpdb->prefix . 'gfb_customer_mst';
			$customer_result = $wpdb->get_results( "SELECT customer_id, customer_name FROM ".$customer_tbl." where is_deleted=0 ", ARRAY_A );
			return $customer_result;
		}

		/* Get Customer Details from ID */
		function gfbCustomerDetails($customerid) {
			global $wpdb;
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';			
			$customer_result = $wpdb->get_results( 'SELECT customer_id, customer_name, customer_email, customer_contact FROM '.$gfb_customer_mst.' WHERE is_deleted=0 AND customer_id='.$customerid.'', ARRAY_A );				
			return $customer_result;	
		}
		
		/* Update Customer Details */
		function gfbUpdateCustomer() {
						
			parse_str($_POST['customer_form'], $customer);
						
			if ( ! isset( $_POST['edit_customer_nonce'] ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Cannot find nonce.') );
				die();
			}
		
			if ( ! wp_verify_nonce( $_POST['edit_customer_nonce'], 'edit_customer_nonce_field' ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Invalid nonce value.') );
				die();
			}
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( $customer['customer_name'] == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Customer name should not be blank.') );
				die();
			}
			elseif( $customer['customer_email'] == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Customer email should not be blank.') );
				die();
			}
			elseif( $customer['customer_contact'] == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Customer contact no should not be blank.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT customer_id, customer_name, customer_email, customer_contact FROM ".$gfb_customer_mst." WHERE is_deleted=0 AND customer_id='".$customer['customer_id']."'", ARRAY_A );
								
				if( !empty( $check_exists ) ) 
				{
					$update_customer_arg = array(
						'customer_name' => ''.wp_kses_post($customer['customer_name']).'',
						'customer_email' => ''.wp_kses_post($customer['customer_email']).'',
						'customer_contact' => ''.wp_kses_post($customer['customer_contact']).'',
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$customer_where = array(
						'customer_id' => ''.$customer['customer_id'].'',
					);
					
					$update_customer = $wpdb->update($gfb_customer_mst, $update_customer_arg, $customer_where);	
					
					if($update_customer)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Customer details updated successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-customers') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Customer details cannot not be updated successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'No customer found.') );
					die();	
				}
				
			}
			
			die();
		}
		
		/* Delete Customer */
		function gfbDeleteCustomer(){
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			$check_appointment = $wpdb->get_results("select appointment_id, DATE_FORMAT(appointment_date, '%d-%b-%Y') as appointment_date from ".$gfb_appointments_mst." where is_deleted=0 and status=1 and customer_id=".trim($_POST["customer_id"])." and appointment_date >= CURDATE()", ARRAY_A);
			
			//print_r($check_appointment); exit;
			
			if( !empty($check_appointment) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Appointment of this customer already exists on '.$check_appointment[0]["appointment_date"].'. So this customer cannot be deleted now.') );
				die();
			}
			else {
			
				$delete_array = array(
					'is_deleted'    => 1,
					'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
					'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
					'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
				);
				
				$delete_where = array(
					'customer_id' => trim($_POST["customer_id"])
				);
				
				$delete_customer = $wpdb->update($gfb_customer_mst, $delete_array, $delete_where);
			}
			
			if($delete_customer)
			{		
				echo json_encode( array('msg' => 'true', 'text' => 'Customer deleted successfully.') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Customer not deleted successfully.') );
				die();
			}
					
		}
	}
	
	global $gfbCustomerObj;
	$gfbCustomerObj = new GravityFormBookingCustomer;
	

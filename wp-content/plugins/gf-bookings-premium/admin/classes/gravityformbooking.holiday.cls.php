<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingHoliday {
		
		function __construct() {
			add_action( 'wp_ajax_gfb_remove_holiday', array(&$this, 'gfbDeleteHoliday') );
			add_action( 'wp_ajax_gfb_add_holiday', array(&$this, 'gfbAddHoliday') );
			add_action( 'wp_ajax_gfb_view_holiday', array(&$this, 'gfbViewHolidayForm') );
			add_action( 'wp_ajax_gfb_display_holiday_list', array(&$this, 'gfbListHolidayAry') );
		}
		
		/* Display Holiday Form */
		function gfbViewHolidayForm() {
			require(GFB_ADMIN_TAB . 'holiday/holidayModalForm.php');
			die();
		}
		
		/* Get Holiday Array From DB */
		function gfbListHolidayAry(){
			global $wpdb;
			$holiday_arg=array();
			$holiday_tbl = $wpdb->prefix . 'gfb_holiday_mst';			
			$holiday_result = $wpdb->get_results( 'SELECT holiday_date FROM '.$holiday_tbl.' WHERE is_deleted=0', ARRAY_A );		
			
			foreach($holiday_result as $holiday){
				array_push($holiday_arg, $holiday['holiday_date']);
			}	
				
			echo json_encode($holiday_arg);
			die();
		}
		
		/* Get Holiday Array From DB for front end */
		function gfbListHolidayFrontEnd(){
			global $wpdb;
			$holiday_arg=array();
			$holiday_tbl = $wpdb->prefix . 'gfb_holiday_mst';			
			$holiday_result = $wpdb->get_results( 'SELECT holiday_date FROM '.$holiday_tbl.' WHERE is_deleted=0', ARRAY_A );		
			
			foreach($holiday_result as $holiday){
				array_push($holiday_arg, date('d F, Y', strtotime($holiday['holiday_date'])));	
			}	
				
			return json_encode($holiday_arg);
		}
		
		function gfbAddHoliday() {
						
			$holiday_date = date('Y-m-d',strtotime($_POST['holiday_date']));

			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_holiday_mst = $wpdb->prefix . 'gfb_holiday_mst';
			$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( $holiday_date == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => __('Please select holiday date.', 'gfb') ) );
				die();
			}
			else
			{
				/* CHECK WHEATHER DATE EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_holiday_mst." WHERE holiday_date='".$holiday_date."' AND is_deleted=0", ARRAY_A );
								
				if( empty( $check_exists ) ) 
				{
					$insert_holiday = $this->gfbInsertHoliday( $holiday_date, $gfb_createdby, $gfb_ip, $gfb_holiday_mst );	
					$insert_holiday_staff = $this->gfbStaffHoliday( $holiday_date, $gfb_createdby, $gfb_ip, $gfb_staff_holiday_mapping );
										
					if( $insert_holiday && $insert_holiday_staff )
					{		
						echo json_encode( array('msg' => 'true', 'text' => __('Holiday added successfully.', 'gfb') ) );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => __('Holiday not added successfully.', 'gfb') ) );
						die();
					}
				}
			}
			
			die();
		}
		
		/* ADD HOLIDAY IN STAFF HOLIDAY TABLE */
		function gfbStaffHoliday( $holiday_date, $gfb_createdby, $gfb_ip, $gfb_staff_holiday_mapping ) {
			
			global $wpdb;
			global $gfbStaff;
			
			/* Staff Id List */
			$staffList = $gfbStaff->getStaffNameList();
			
			if(	count($staffList) ) {	
			
				foreach( $staffList as $staff ){
					
					/* Add Holiday in Master */
					$add_holiday_arg = array(
						'staff_id' 		=> ''.wp_kses_post($staff['staff_id']).'',
						'holiday_date'  => ''.wp_kses_post($holiday_date).'',
						'created_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'created_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);	
					
					/* Add Holiday in Staff Holidat Mapping Table */
					$insert_holiday = $wpdb->insert($gfb_staff_holiday_mapping, $add_holiday_arg);
				}
			
			}
			
			return true;
			
		}
		
		/* CODE TO INSERT HOLIDAY IN HOLIDAY MASTER */
		function gfbInsertHoliday($holiday_date, $gfb_createdby, $gfb_ip, $gfb_holiday_mst) {
			
			global $wpdb;
			
			/* Add Holiday in Master */
			$add_holiday_arg = array(
				'holiday_date' => ''.$holiday_date.'',
				'created_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'created_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			/* Add Holiday in Holiday Table */
			$insert_holiday = $wpdb->insert($gfb_holiday_mst, $add_holiday_arg);	
			
			return $insert_holiday;	
		}
			
		/* DELETE HOILDAY FROM LIST */
		function gfbDeleteHoliday() {
						
			$holiday_date = date('Y-m-d',strtotime($_POST['holiday_date']));

			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_holiday_mst = $wpdb->prefix . 'gfb_holiday_mst';
			$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK WHEATHER DATE EXISTS OR NOT */
			$check_exists = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_holiday_mst." WHERE is_deleted=0 AND holiday_date='".$holiday_date."'", ARRAY_A );
			
			if( !empty( $check_exists ) ) 
			{
				$add_holiday_arg = array(
					'is_deleted' => 1,
					'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
					'modified_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
					'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
				);
				
				$where_holiday = array(
					'holiday_date' => ''.$holiday_date.'',
				);
				
				$update_holiday = $wpdb->update($gfb_holiday_mst, $add_holiday_arg, $where_holiday);
				$update_staff_holiday = $wpdb->update($gfb_staff_holiday_mapping, $add_holiday_arg, $where_holiday);	
								
				if( empty($update_staff_holiday) ) {
					
					if($update_holiday)
					{		
						echo json_encode( array('msg' => 'true', 'text' => __('Holiday deleted successfully.', 'gfb') ) );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => __('Holiday not deleted successfully.', 'gfb') ) );
						die();
					}
				}
				else {
					if($update_holiday && $update_staff_holiday)
					{		
						echo json_encode( array('msg' => 'true', 'text' => __('Holiday deleted successfully.', 'gfb') ) );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => __('Holiday not deleted successfully.', 'gfb') ) );
						die();
					}
					
				}
				
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => __('No holiday found.', 'gfb') ) );
				die();	
			}
							
			die();
		}	
	}
	
	global $gfbHolidays;
	$gfbHolidays = new GravityFormBookingHoliday;

<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingService {
		
		function __construct() {
			
			add_action( 'wp_ajax_gfb_add_service', array(&$this, 'gfbAddService') );
			add_action( 'wp_ajax_gfb_delete_service', array(&$this, 'gfbDeleteService') );
			add_action( 'wp_ajax_gfb_display_edit_service', array(&$this, 'gfbEditServiceForm') );
			add_action( 'wp_ajax_gfb_update_service', array(&$this, 'gfbUpdateService') );
			add_action( 'init', array(&$this, 'gfbExportServiceToCsv') );
			add_action( 'init', array(&$this, 'gfbExportServiceToPdf') );
		}
		
		/* Export To CSV */		
		function gfbExportServiceToCsv() {
			
			if(isset($_REQUEST['gfb_service_export_to_csv']) && $_REQUEST['gfb_service_export_to_csv'] == 'gfb_export_csv') {
				
				global $wpdb;
				
				$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";
				$gfb_services_mst = $wpdb->prefix . "gfb_services_mst";	
				
				$serviceList = $wpdb->get_results( "SELECT cm.category_name, sm.service_title,  sm.service_price, sm.service_color  FROM ".$gfb_services_mst." as sm LEFT JOIN ".$gfb_categories_mst." as cm ON sm.category_id = cm.category_id WHERE sm.is_deleted=0 ORDER BY sm.service_title asc", ARRAY_A );					
		
				header('Content-type: text/csv');
				header('Content-Disposition: attachment; filename="service_list.csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
	
				$file = fopen('php://output', 'w');
				
				$column_list = array("Category Name", "Service Name", "Service Price", "Service Color");
	
				fputcsv($file, $column_list);
						
				foreach ($serviceList as $service) {
					fputcsv($file, $service);
				}
	
				exit();
			}
		}
		
		/* Export To PDF */		
		function gfbExportServiceToPdf() {
			
			if(isset($_REQUEST['gfb_service_export_to_pdf']) && $_REQUEST['gfb_service_export_to_pdf'] == 'gfb_export_pdf') {
				
				global $wpdb;
				global $gfbFpdfObj;
				global $title;
				
				$title = 'Service List';
				$gfbFpdfObj->SetTitle($title);				
				$gfbFpdfObj->AddPage();
				
				$service_tbl = $wpdb->prefix . 'gfb_services_mst';	
				$category_tbl = $wpdb->prefix . 'gfb_categories_mst';		
					
				$service_result = $wpdb->get_results( "SELECT sm.service_id, sm.service_title, cm.category_name, sm.service_price FROM ".$service_tbl." as sm LEFT JOIN ".$category_tbl." as cm ON sm.category_id = cm.category_id WHERE sm.is_deleted=0 ORDER BY sm.service_title DESC", ARRAY_A );
				
				if($service_result) {
				
					$width_cell=array(20,60, 60, 40);
					$gfbFpdfObj->SetFont(apply_filters( 'gfb_change_pdf_font', 'Arial'),'B',14);
					
					$gfbFpdfObj->SetFillColor(255,255,255); // Background color of header 
					// Header starts /// 
					
					$gfbFpdfObj->Cell($width_cell[0],10,'ID',1,0,'C',true);
					$gfbFpdfObj->Cell($width_cell[1],10,'CATEGORY',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[2],10,'SERVICE NAME',1,0,'C',true); 
					$gfbFpdfObj->Cell($width_cell[3],10,'PRICE',1,0,'C',true); 
					$gfbFpdfObj->Ln();
					//// Header ends ///////
					
					$gfbFpdfObj->SetFont(apply_filters( 'gfb_change_pdf_font', 'Arial'),'',12);
					$gfbFpdfObj->SetFillColor(235,236,236); // Background color of header 
					$fill=false; // to give alternate background fill color to rows 
					$countid=1;
					
					/// each record is one row  ///
					foreach ($service_result as $row) {
						$gfbFpdfObj->Cell($width_cell[0],10,$countid,1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[1],10,$row['category_name'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[2],10,$row['service_title'],1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[3],10,$row['service_price'],1,0,'C',$fill);
						$gfbFpdfObj->Ln();
						$fill = !$fill; // to give alternate background fill  color to rows
						$countid++;
					}
					/// end of records ///
				
				}
				
				$gfbFpdfObj->Output("service_list.pdf", "D");
			}
		}
		
		/* Service List */
		function gfbListServices(){
			
			global $wpdb;
			$service_tbl = $wpdb->prefix . 'gfb_services_mst';	
			$category_tbl = $wpdb->prefix . 'gfb_categories_mst';		
				
			$service_result = $wpdb->get_results( "SELECT sm.service_id, sm.service_title, sm.service_price, sm.service_color, sm.category_id, cm.category_name as cat_name FROM ".$service_tbl." as sm LEFT JOIN ".$category_tbl." as cm ON sm.category_id = cm.category_id WHERE sm.is_deleted=0 ORDER BY sm.service_title DESC", ARRAY_A );
							
			return $service_result;	
		}
		
		/* Service Detail */
		function gfbServiceDetails($service_id){
			global $wpdb;
			$service_tbl = $wpdb->prefix . 'gfb_services_mst';			
			$service_result = $wpdb->get_results( 'SELECT service_id, service_title, REPLACE(service_price, "'.get_option('gfb_currency_symbol').'", "") as service_price, service_color, category_id FROM '.$service_tbl.' WHERE is_deleted=0 AND service_id='.trim($service_id).'', ARRAY_A );				
			return $service_result;	
		}
		
		/* Service Name List */
		function gfbServiceNameList(){
			global $wpdb;
			$service_tbl = $wpdb->prefix . 'gfb_services_mst';
			$service_result = $wpdb->get_results( 'SELECT service_id, service_title FROM '.$service_tbl.' WHERE is_deleted=0 ORDER BY service_title ASC', ARRAY_A );
			return $service_result;	
		}
		
		/* Edit Service Form */
		function gfbEditServiceForm(){
			//echo $_POST['service_id'];die();
			if (isset($_POST['service_id'])):
				include(GFB_ADMIN_TAB . 'services/editService.php');
			endif;
			die();	
		}
		
		/* AJAX FUNCTION TO ADD CATEGORIES */		
		function gfbAddService() {
						
			parse_str($_POST['service_form'], $service);
						
			if ( ! isset( $service['service_nonce'] ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Cannot find nonce.') );
				die();
			}
		
			if ( ! wp_verify_nonce( $service['service_nonce'], 'service_nonce_field' ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Invalid nonce value.') );
				die();
			}
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($service['category_id']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select category.') );
				die();
			}
			elseif( trim($service['service_title']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please add service title.') );
				die();
			}
			elseif( trim($service['service_color']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please add service color.') );
				die();
			}
			elseif( trim($service['service_price']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please add service price.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT service_id FROM ".$gfb_services_mst." WHERE is_deleted=0 AND service_title='".trim($service['service_title'])."' and category_id=".trim($service['category_id'])."", ARRAY_A );
				
				if( empty( $check_exists ) ) 
				{
					$add_service_arg = array(
						'service_title' 	=> ''.wp_kses_post(trim($service['service_title'])).'',
						'category_id'		=> ''.wp_kses_post(trim($service['category_id'])).'',
						'service_price' 	=> ''.wp_kses_post(ltrim($service['service_price'], get_option('gfb_currency_symbol'))).'',
						'service_color' 	=> ''.wp_kses_post(ltrim($service['service_color'],'#')).'',
						'created_by' 		=> ''.wp_kses_post($gfb_createdby).'',
						'created_date'  	=> ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 		=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$insert_service = $wpdb->insert($gfb_services_mst, $add_service_arg);	
					
					if($insert_service)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Service added successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-services') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Service not added successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Service already exists.') );
					die();	
				}
				
			}
			
			die();
			
		}
		
		/* AJAX FUNCTION TO UPDATE CATEGORIES */
		function gfbUpdateService() {
						
			parse_str($_POST['service_form'], $service);
						
			if ( ! isset( $service['edit_service_nonce'] ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Cannot find nonce.') );
				die();
			}
		
			if ( ! wp_verify_nonce( $service['edit_service_nonce'], 'edit_service_nonce_field' ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Invalid nonce value.') );
				die();
			}
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($service['category_id']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select category.') );
				die();
			}
			elseif( trim($service['service_title']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please add service title.') );
				die();
			}
			elseif( trim($service['service_color']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please add service color.') );
				die();
			}
			elseif( trim($service['service_price']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please add service price.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT service_id FROM ".$gfb_services_mst." WHERE is_deleted=0 AND service_id='".trim($service['service_id'])."'", ARRAY_A );
				
				if( !empty( $check_exists ) ) 
				{
					$set_service_arg = array(
						'service_title' 	=> ''.wp_kses_post(trim($service['service_title'])).'',
						'category_id'		=> ''.wp_kses_post(trim($service['category_id'])).'',
						'service_price' 	=> ''. (float) trim($service['service_price']).'',
						'service_color' 	=> ''.wp_kses_post(ltrim($service['service_color'],'#')).'',
						'modified_by' 		=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date'  	=> ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 		=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$service_where = array(
						'service_id' 	=> ''.wp_kses_post($service['service_id']).'',
					);
					
					$update_service = $wpdb->update($gfb_services_mst, $set_service_arg, $service_where);	
					
					if($update_service)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Service updated successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-services') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Service not updated successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Service not found.') );
					die();	
				}
				
			}
			
			die();
			
		}
		
		/* Get service title from given id */
		function gfbGetServiceTitle($service_id) {
			
			global $wpdb;	
			$service_mst_tbl = $wpdb->prefix . 'gfb_services_mst';			
			$service_result = $wpdb->get_results("SELECT service_title FROM ".$service_mst_tbl." where service_id='".$service_id."' and is_deleted=0",ARRAY_A);				
			return $service_result;	
		}
		
		/* Delete Service */
		function gfbDeleteService() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			$delete_array = array(
				'is_deleted'    => 1,
				'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			$delete_where = array(
				'service_id' => $_POST["serid"]
			);
			
			$delete_service = $wpdb->update($gfb_services_mst, $delete_array, $delete_where);
			
			if($delete_service)
			{		
				echo json_encode( array('msg' => 'true', 'text' => 'Service deleted successfully.') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Service not deleted successfully.') );
				die();
			}
		}
	}
	
	global $gfbServiceObj;
	$gfbServiceObj = new GravityFormBookingService;
	

<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingStaff {
		public $mainCalssFunction;
		function __construct() {
			$this->mainCalssFunction = new gfb_bookings_addon();	
			add_action( 'wp_ajax_gfp_add_staff', array( &$this, 'gfbAddNewStaff' ) );
			add_action( 'wp_ajax_gfb_update_staff', array( &$this, 'gfbUpdateNewStaff' ) );
			add_action( 'wp_ajax_gfb_display_edit_staff_form', array( &$this, 'gfbViewStaffForm' ) );
			add_action( 'wp_ajax_gfb_save_staff_pic', array( &$this, 'gfbSavePic' ) );
			add_action( 'wp_ajax_gfb_delete_staff', array( &$this, 'gfbDeleteStaff' ) );
			add_action( 'wp_ajax_gfb_display_staff_holiday', array( &$this, 'gfbListStaffHolidayAry' ) );
			add_action( 'wp_ajax_gfp_add_staff_holiday', array( &$this, 'gfbAddStaffHoliday' ) );
			add_action( 'wp_ajax_gfb_remove_staff_holiday', array( &$this, 'gfbRemoveStaffHoliday' ) );
			
			add_action( 'wp_ajax_gfp_add_staff_service', array( &$this, 'gfbAddStaffService' ) );
			add_action( 'wp_ajax_gfp_add_staff_timeslot', array( &$this, 'gfbAddStaffTimeSlot' ) );
			add_action( 'wp_ajax_gfb_delete_staff_timeslot', array( &$this, 'gfbDeleteStaffTimeSlot' ) );
			add_action( 'wp_ajax_gfb_display_edit_stafftimeslot',array(&$this, 'gfbEditStaffTimeSlotForm') );			
			add_action( 'wp_ajax_gfb_edit_stafftimeslot', array(&$this, 'gfbUpdateStaffTimeSlot') );			
			add_action( 'wp_ajax_gfb_clear_stafftimeslot', array(&$this, 'gfbClearStaffTimeSlot') );
			add_action( 'wp_ajax_gfp_add_staff_timezone', array( &$this, 'gfp_add_staff_timezone' ) );	
			add_action( 'wp_ajax_gfp_add_staff_timeslot_breaks', array( &$this, 'gfbAddStaffBreak' ) );	
			add_action( 'wp_ajax_gfp_add_fullday_staff_timeslot', array( &$this, 'gfbAddFulldayStaffTimeslot' ) );			
		}
		function gfbAddFulldayStaffTimeslot() {			

    		if ( isset( $_POST['form_data'] ) && isset( $_POST['staff_id'] ) ) {
    			$params = array();
    			$staff_id = sanitize_text_field( $_POST['staff_id'] );
    			parse_str($_POST['form_data'], $params);

    			$gfb_fullday_booking = get_option( 'gfb_fullday_booking_slots', array() );

    			$gfb_fullday_booking['staff_'. $staff_id] = $params;

    			update_option( 'gfb_fullday_booking_slots', $gfb_fullday_booking );		

				/* VARIABLE DEFINED */
				global $wpdb;
				$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
				$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
				$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
				$gfb_createdby = get_current_user_id();
				$gfb_ip = GFB_Core::gfbIpAddress();
				$msg = '';

				$weekdays = array(
					'0' => 'sunday',
					'1' => 'monday',
					'2' => 'tuesday',
					'3' => 'wednesday',
					'4' => 'thursday',
					'5' => 'friday',
					'6' => 'saturday'
				);

				foreach ( $weekdays as $day_key => $day ) {
					
					if ( is_array( $params ) ) {
						
						$slot_cap = $params[$day]['slot_cap'];

						//Delete time slot and staff schedule slot from tables if no appointment created with that slot.
						$check_exists_appointment_for_timeslot = $wpdb->get_results( "SELECT ssm.staff_slot_mapping_id, ssm.time_slot_id FROM ".$gfb_staff_schedule_mapping." ssm INNER JOIN ".$gfb_time_slot_mst." tm ON ssm.time_slot_id=tm.time_slot_id WHERE tm.fullday=1 and ssm.staff_id=".trim($staff_id)." and tm.weekday=".trim($day_key)."", ARRAY_A );					

						if ( ! empty( $check_exists_appointment_for_timeslot ) ) {
							$staff_slot_mapping_id_array = $this->mainCalssFunction->gfb_array_column($check_exists_appointment_for_timeslot, 'staff_slot_mapping_id');							

							foreach ( $staff_slot_mapping_id_array as $staff_slot_mapping_id ) {

								//check if appointment exisit
								$check_appointment = $wpdb->get_results( "SELECT appointment_id FROM ".$gfb_appointments_mst." WHERE staff_id=".trim($staff_id)." and staff_slot_mapping_id=".trim($staff_slot_mapping_id)."", ARRAY_A );

								if ( empty( $check_appointment ) ) { //no appointment exist in the appointemnt table.
									
									$wpdb->query( "DELETE FROM ".$gfb_staff_schedule_mapping." WHERE staff_id=".trim($staff_id)." and staff_slot_mapping_id=".$staff_slot_mapping_id."" );

									$wpdb->query( "DELETE FROM ".$gfb_time_slot_mst." WHERE time_slot_id=".$staff_slot_mapping_id." and weekday=".trim($day_key)."" );								
								}

							}

						}


						$check_exists = $wpdb->get_results( "SELECT ssm.time_slot_id FROM ".$gfb_staff_schedule_mapping." ssm INNER JOIN ".$gfb_time_slot_mst." tm ON ssm.time_slot_id=tm.time_slot_id WHERE tm.fullday=1 and ssm.is_deleted=0 AND tm.is_deleted=0 AND ssm.staff_id=".trim($staff_id)." and tm.weekday=".trim($day_key)."", ARRAY_A );

						if ( !empty( $check_exists ) ) {
							$time_slot_array = implode(', ', $this->mainCalssFunction->gfb_array_column($check_exists, 'time_slot_id'));
							
							$update_stimeslot = $wpdb->query( "UPDATE ".$gfb_staff_schedule_mapping." SET is_deleted=1, modified_by=".wp_kses_post($gfb_createdby).", modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.") AND staff_id=".trim($staff_id)."" );
							
							/* Clear time slots from master */
							$update_timeslot = $wpdb->query( "UPDATE ".$gfb_time_slot_mst." SET is_deleted=1, modified_by=".wp_kses_post($gfb_createdby).", modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.") AND is_custom_slot=1 AND weekday=".trim($day_key)."" );
						}


						if ( isset( $params[$day]['enabled'] ) ) {

							/* INSERT INTO TIMESLOT MASTER */
							$add_timeslot_arg = array(
								'time_slot_name'           => 'Full Day',
								'slot_start_time'          => '00:00:00',
								'slot_end_time'			   => '24:00:00',
								'max_appointment_capacity' => ''.wp_kses_post(trim($slot_cap)).'',
								'weekday'                  => ''.wp_kses_post(trim($day_key)).'',
								'is_custom_slot'		   => 1,
								'created_by' 	           => ''.wp_kses_post($gfb_createdby).'',
								'created_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
								'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
								'fullday'                  => 1
							);

							$insert_timeslot = $wpdb->insert($gfb_time_slot_mst, $add_timeslot_arg);
					
							/* Last Insert Timeslot id */
							$lastStaffSlotId = $wpdb->insert_id;
							
							/* INSERT INTO STAFF SLOT MAPPING TABLE */
							$add_staff_slot_arg = array(
								'staff_id'			       => ''.wp_kses_post(trim($staff_id)).'',
								'time_slot_id'  		   => ''.wp_kses_post(trim($lastStaffSlotId)).'',
								'max_appointment_capacity' => ''.wp_kses_post(trim($slot_cap)).'',
								'created_by' 	           => ''.wp_kses_post($gfb_createdby).'',
								'created_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
								'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
							);	
							
							/* Add Slot in Staff Slots Mapping Table */
							$insert_staff_slot = $wpdb->insert($gfb_staff_schedule_mapping, $add_staff_slot_arg);
						}	
					}
				}

				echo json_encode( array('msg' => 'true', 'text' => 'Full day booking updated successfully.', 'tab' => 'timings-tab', 'url' => admin_url().'admin.php?page=gravity-form-booking-staff&state='.base64_encode($staff_id) ) );
				die();
    		}

			wp_die();

		}

		function gfp_add_staff_timezone() {
			if ( isset( $_POST['staff_id'] ) && isset( $_POST['staff_timezone'] ) && ! empty( $_POST['staff_id'] ) && ! empty( $_POST['staff_timezone'] )  ) {

				if ( update_option( 'gfb_staff_timezone_' . $_POST['staff_id'], $_POST['staff_timezone'] ) ) {
					echo json_encode( array('msg' => 'true', 'text' => __('Timezone updated successfully.', 'gfb') ) );
				} else {
					echo json_encode( array('msg' => 'false', 'text' => __('Timezone not updated.', 'gfb') ) );
				}

			} else {
				echo json_encode( array('msg' => 'false', 'text' => __('Please select timezone.', 'gfb') ) );
			}

			wp_die();
		}
		
		/* ADD NEW STAFF */
		function gfbAddNewStaff() {

			parse_str($_POST["staff_name"], $staff_post);
			
			if ( ! isset( $staff_post['staff_nonce'] ) ) {
				echo json_encode( array('msg' => 'false', 'text' => __('Cannot find nonce.', 'gfb') ) );
				die();
			}
		
			if ( ! wp_verify_nonce( $staff_post['staff_nonce'], 'staff_nonce_field' ) ) {
				echo json_encode( array('msg' => 'false', 'text' => __('Invalid nonce value.', 'gfb') ) );
				die();
			}
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_holiday_mst = $wpdb->prefix . 'gfb_holiday_mst';
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if ( trim($staff_post['staff_name']) == '' ) {
				echo json_encode( array('msg' => 'false', 'text' => __('Please enter staff full name.', 'gfb') ) );
				die();
			} elseif ( trim($staff_post['staff_username']) == '' ) {
				echo json_encode( array('msg' => 'false', 'text' => __('Please enter staff username.', 'gfb') ) );
				die();
			} elseif ( trim($staff_post['staff_email']) == '' ) {
				echo json_encode( array('msg' => 'false', 'text' => __('Please enter staff email address.', 'gfb') ) );
				die();
			} elseif ( trim($staff_post['staff_password']) == '' ) {
				echo json_encode( array('msg' => 'false', 'text' => __('Please enter staff password.', 'gfb') ) );
				die();
			} else {
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT staff_id FROM ".$gfb_staff_mst." WHERE is_deleted=0 AND staff_name='".trim($staff_post['staff_name'])."' AND staff_email='".trim($staff_post['staff_email'])."'", ARRAY_A );
				
				if( empty( $check_exists ) ) {					
					// Create WP User 
					$user_id = wp_create_user( trim($staff_post['staff_username']), trim($staff_post['staff_password']), trim($staff_post['staff_email']) );
										
					if( is_wp_error($user_id) ) {
						echo json_encode( array('msg' => 'false', 'text' => $user_id->get_error_message() ) );
						die();
					} else {	
						// Add  role to new user 
						$user = new WP_User($user_id);
						$user->set_role('gfb_staff_role');	
						
						/* Add Staff */					
						$add_staff_arg = array(
							'staff_name'   => ''.wp_kses_post(trim($staff_post['staff_name'])).'',
							'user_id'      => ''.wp_kses_post($user_id).'',
							'staff_email'  => ''.wp_kses_post(trim($staff_post['staff_email'])).'',
							'created_by'   => ''.wp_kses_post($gfb_createdby).'',
							'created_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
							'ip_address'   => ''.wp_kses_post($gfb_ip).'',
						);
						
						$insert_staff = $wpdb->insert($gfb_staff_mst, $add_staff_arg);	
						$last_staff_id = $wpdb->insert_id;
												
						/* Add Holiday */
						$get_holiday_list = $wpdb->get_results("SELECT holiday_date FROM ".$gfb_holiday_mst." WHERE is_deleted=0", ARRAY_A);
												
						if( !empty($get_holiday_list) ){
							foreach($get_holiday_list as $hol_lst) {
								$staff_holi_arg = array(
									'staff_id'     => trim($last_staff_id),
									'holiday_date' => trim($hol_lst["holiday_date"]),
									'created_by'   => ''.wp_kses_post($gfb_createdby).'',
									'created_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address'   => ''.wp_kses_post($gfb_ip).'',
								);
								
								$insert_staff_holiday = $wpdb->insert($gfb_staff_holiday_mapping, $staff_holi_arg);
							}
						}
						
						
						/* Add Time Slot */
						$get_timeslot_list = $wpdb->get_results("SELECT time_slot_id, time_slot_name, weekday, slot_start_time, slot_end_time, max_appointment_capacity FROM ".$gfb_time_slot_mst." WHERE is_deleted=0 AND is_custom_slot=0", ARRAY_A);
												
						if ( !empty($get_timeslot_list) ) {
							foreach($get_timeslot_list as $timeslot_lst) {
								$staff_slot_arg = array(
									'staff_id'     				=> trim($last_staff_id),
									'time_slot_id' 				=> trim($timeslot_lst["time_slot_id"]),
									'max_appointment_capacity'  => trim($timeslot_lst["max_appointment_capacity"]),
									'created_by'   				=> ''.wp_kses_post($gfb_createdby).'',
									'created_date' 				=> ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address'  				=> ''.wp_kses_post($gfb_ip).'',
								);
								
								$insert_staff_slot = $wpdb->insert($gfb_staff_schedule_mapping, $staff_slot_arg);
							}
						}
					
						if($insert_staff) {		
							echo json_encode( array('msg' => 'true', 'text' => __('Staff added successfully.', 'gfb') , 'url' => admin_url().'admin.php?page=gravity-form-booking-staff') );
							die();
						} else {
							echo json_encode( array('msg' => 'false', 'text' => __('Staff not added successfully.', 'gfb') ) );
							die();
						}
					}
				}
				else {
					echo json_encode( array('msg' => 'false', 'text' => __('Staff already exists.', 'gfb') ) );
					die();	
				}
				
			}
			
			die();
		}
		
		/* Save PIc */
		function gfbSavePic() {
			
			
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			$update_pic = array(
				"staff_pic" => trim($_POST["imgurl"])
			);
			
			$where_pic = array(
				"staff_id" => trim($_POST["sid"])
			);
			
			$update = $wpdb->update($gfb_staff_mst, $update_pic, $where_pic);
			
			if($update){
				return true;	
			}
			else {
				return false;	
			}
		}
		
		/* UPDATE NEW STAFF */
		function gfbUpdateNewStaff() {
			
			parse_str($_POST['staff_data'], $staff);
						
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$staff_schedule_mapping_tbl = $wpdb->prefix . 'gfb_staff_schedule_mapping';	
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';
			$time_slot_mst_tbl= $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($staff['staff_name']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => __('Please enter staff full name.', 'gfb') ) );
				die();
			}
			elseif( trim($staff['staff_email']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => __('Please enter staff email address.', 'gfb') ) );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT staff_id ,user_id FROM ".$gfb_staff_mst." WHERE is_deleted=0 AND staff_id='".trim($staff['staff_id'])."'", ARRAY_A );
				
				if( !empty( $check_exists ) ) 
				{
					$edit_staff_arg = array(
						'staff_name'   		 => ''.wp_kses_post(trim($staff['staff_name'])).'',
						'staff_email'  		 => ''.wp_kses_post(trim($staff['staff_email'])).'',
						'staff_phone'  		 => ''.wp_kses_post(trim($staff['staff_phone'])).'',
						'staff_designation'  => ''.wp_kses_post(trim($staff['staff_designation'])).'',
						'staff_info'   		 => ''.wp_kses_post(trim($staff['staff_info'])).'',
						/* 'staff_pic'    		 => ''.wp_kses_post(trim($staff['staff_pic'])).'', */
						'created_by'   		 => ''.wp_kses_post($gfb_createdby).'',
						'created_date' 		 => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address'   		 => ''.wp_kses_post($gfb_ip).'',
					);
					
					$where_arg = array(
						'staff_id' 	=> ''.wp_kses_post(trim($staff['staff_id'])).'',
					);
					
					$update_staff = $wpdb->update($gfb_staff_mst, $edit_staff_arg, $where_arg);	
					
					$user_id = $check_exists[0]['user_id'];
					$transfarStaffId = $staff['transfar_staff_id'];
					//Check User meta
					$transfar_staff_id = get_user_meta($user_id,'transfar_staff_id');
					
					/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
					
					
					$get_transfar_staff_user_id = $wpdb->get_results( "SELECT staff_id ,user_id FROM ".$gfb_staff_mst." WHERE is_deleted=0 AND staff_id='".trim($transfarStaffId)."'", ARRAY_A );
					$check_booking_already = $wpdb->get_results( "SELECT appointment_date FROM ".$gfb_appointments_mst." WHERE is_deleted=0 AND staff_id='".trim($transfarStaffId)."'", ARRAY_A );

					if(empty($transfar_staff_id) && empty($check_booking_already)){
						if(!empty($transfarStaffId)){
							$transfar_staff_user_id = $get_transfar_staff_user_id[0]['user_id'];
							add_user_meta($transfar_staff_user_id, 'transfar_staff_id','');
							
							
							add_user_meta($user_id, 'transfar_staff_id', $transfarStaffId);
							$edit_transfar_staff_arg = array(
								'staff_id'=> $transfarStaffId,
							);
							$where_staff_arg = array(
								'staff_id' 	=> ''.wp_kses_post(trim($staff['staff_id'])).'',
							);
							
							$wpdb->update($gfb_appointments_mst, $edit_transfar_staff_arg, $where_staff_arg);

						}
					}else if(!empty($transfar_staff_id) && empty($check_booking_already)){
						
						if(!empty($transfarStaffId)){

							$transfar_staff_user_id = $get_transfar_staff_user_id[0]['user_id'];
							update_user_meta($transfar_staff_user_id, 'transfar_staff_id','');
							
							update_user_meta( $user_id, 'transfar_staff_id', $transfarStaffId);
							
							$edit_transfar_staff_arg = array(
								'staff_id'=> $transfarStaffId,
							);
							$where_staff_arg = array(
								'staff_id' 	=> ''.wp_kses_post(trim($staff['staff_id'])).'',
							);
							
							$wpdb->update($gfb_appointments_mst, $edit_transfar_staff_arg, $where_staff_arg);	
						}
					}
					
					if(!empty($check_booking_already) && $update_staff){
						echo json_encode( array('msg' => 'false', 'text' => __('Appointments not transferred. Staff Details Updated.', 'gfb') , 'url' => admin_url().'admin.php?page=gravity-form-booking-staff&state='.base64_encode($staff['staff_id'])) );
						die();
					}
					else if($update_staff)
					{		
						echo json_encode( array('msg' => 'true', 'text' => __('Staff details updated successfully.', 'gfb') , 'url' => admin_url().'admin.php?page=gravity-form-booking-staff&state='.base64_encode($staff['staff_id'])) );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => __('Staff not added successfully.', 'gfb') ) );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => __('No Staff Found.', 'gfb') ) );
					die();	
				}
				
			}
			
			die();
		}
		
		/* GET STAFF LIST WITH NAME */
		function getStaffNameList() {
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';			
			$staff_result = $wpdb->get_results( 'SELECT staff_id, user_id, staff_name FROM '.$gfb_staff_mst.' WHERE is_deleted=0', ARRAY_A );return $staff_result;	
		}
		
		/* GET STAFF FULL DETAILS BY ID */
		function getStaffFullDetails($staff_id) {
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';			
			$staff_result = $wpdb->get_results( 'SELECT staff_id, user_id, staff_name, staff_designation, staff_email, staff_pic, staff_phone, staff_info, staff_gcal_id FROM '.$gfb_staff_mst.' WHERE is_deleted=0 AND staff_id="'.trim($staff_id).'"', ARRAY_A );				
			return $staff_result;	
		}	
		
		/* GET STAFF NAME DETAILS BY ID */
		function getStaffNameById($user_id) {
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';			
			$staff_result = $wpdb->get_results( 'SELECT staff_id, user_id, staff_name FROM '.$gfb_staff_mst.' WHERE is_deleted=0 AND user_id="'.trim($user_id).'"', ARRAY_A );				
			return $staff_result;	
		}		
		
		/* VIEW STAFF DETAIL FORM */
		function gfbViewStaffForm(){
			if (isset($_POST['staff_id'])):
			
				require( GFB_ADMIN_TAB . 'staff/staffDetail.php' );
			endif;
			die();	
		}
		
		/* Get Holiday Array From DB related to Staff */
		function gfbListStaffHolidayAry(){
			
			global $wpdb;
			$holiday_arg = array();
			$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';			
				
			$holiday_result = $wpdb->get_results( 'SELECT holiday_date, staff_id, staff_holiday_mapping_id FROM '.$gfb_staff_holiday_mapping.' WHERE is_deleted=0 AND staff_id="'.base64_decode($_POST['staff_id']).'"', ARRAY_A );		
			
			foreach($holiday_result as $holiday){
				array_push($holiday_arg, $holiday['holiday_date']);	
			}	
			
			echo json_encode($holiday_arg);			
			die();
		}
		
		/* Add New Holiday According to Staff */ 
		function gfbAddStaffHoliday() {			

			if ( isset( $_POST['holiday_date'] ) && is_array( $_POST['holiday_date'] ) && count( $_POST['holiday_date'] ) > 0 ) {
				
				/* VARIABLE DEFINED */
				global $wpdb;
				$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
				$gfb_createdby = get_current_user_id();
				$gfb_ip = GFB_Core::gfbIpAddress();
				$msg = '';
				$flag = false;

				foreach ( $_POST['holiday_date'] as $holiday_date ) {
					
					$holiday_date = date( 'Y-m-d',strtotime( $holiday_date ) );
					
					/* CHECK VALIDATION */
					if( $holiday_date == '' ) {
						echo json_encode( array('msg' => 'false', 'text' => __('Please select holiday date.', 'gfb') ) );
						die();
					} else {
						/* CHECK WHEATHER DATE EXISTS OR NOT */
						$check_exists = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_staff_holiday_mapping." WHERE holiday_date='".$holiday_date."' AND staff_id='".base64_decode($_POST['staff_id'])."' AND is_deleted=0", ARRAY_A );
										
						if ( empty( $check_exists ) ) {
							/* Add Holiday in Master */
							$add_holiday_arg = array(
								'staff_id' 		=> ''.trim(base64_decode($_POST['staff_id'])).'',
								'holiday_date'  => ''.trim($holiday_date).'',
								'created_by' 	=> ''.wp_kses_post($gfb_createdby).'',
								'created_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
								'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
							);
							
							/* Add Holiday in Staff Table */
							$insert_holiday = $wpdb->insert($gfb_staff_holiday_mapping, $add_holiday_arg);
					
							if ( $insert_holiday ) {		
								$flag = true;
								//die();
							} else {
								$flag = false;
								//die();
							}
						}
						
					}
				}

				if ( $flag ) {	
					echo json_encode( array('msg' => 'true', 'text' => __('Holiday added successfully.', 'gfb')  ) );
					//die();
				} else {
					echo json_encode( array('msg' => 'false', 'text' => __('Holiday not added successfully.', 'gfb') ) );
					//die();
				}
			}
			
			die();	
		}
		
		/* Remove Staff Holiday */
		function gfbRemoveStaffHoliday() {
			
			if ( isset( $_POST['holiday_date'] ) && is_array( $_POST['holiday_date'] ) && count( $_POST['holiday_date'] ) > 0 ) {				

				/* VARIABLE DEFINED */
				global $wpdb;
				$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
				$gfb_createdby = get_current_user_id();
				$gfb_ip = GFB_Core::gfbIpAddress();
				$msg = '';
				$flag = false;

				foreach ( $_POST['holiday_date'] as $holiday_date ) {

					$holiday_date = date('Y-m-d',strtotime($holiday_date) );
				
					/* CHECK WHEATHER DATE EXISTS OR NOT */
					$check_exists = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_staff_holiday_mapping." WHERE is_deleted=0 AND holiday_date='".$holiday_date."' AND staff_id='".base64_decode($_POST['staff_id'])."'", ARRAY_A );
					
					if ( ! empty( $check_exists ) ) {
						$add_holiday_arg = array(
							'is_deleted' => 1,
							'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
							'modified_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
							'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
						);
						
						$where_holiday = array(
							'holiday_date' => ''.$holiday_date.'',
							'staff_id' => ''.base64_decode($_POST['staff_id']).'',
						);
						
						$update_staff_holiday = $wpdb->update($gfb_staff_holiday_mapping, $add_holiday_arg, $where_holiday);	
						
						if ($update_staff_holiday) {		
							$flag = true;
						} else {
							$flag = false;
						}
					} else {
						echo json_encode( array('msg' => 'false', 'text' => __('No holiday found.', 'gfb') ) );
						die();	
					}
				}

				if ( $flag ) {	
					echo json_encode( array('msg' => 'true', 'text' => __('Holiday deleted successfully.', 'gfb') ) );
					die();
				} else {
					echo json_encode( array('msg' => 'false', 'text' => __('Holiday not deleted successfully.', 'gfb') ) );
					die();
				}
			}
			
			echo json_encode( array('msg' => 'false', 'text' => __('Holiday not deleted successfully.', 'gfb') ) );
			die();
		}
		
		/* Add Service to Staff */ 
		function gfbAddStaffService() {
						
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = ''; 
			
			$staff_service_exists = $wpdb->get_results( "SELECT service_id, staff_id FROM ".$gfb_staff_service_mapping." WHERE staff_id='".trim($_POST['staff_id'])."' AND is_deleted=0", ARRAY_A );
			
			$existsService = $this->mainCalssFunction->gfb_array_column( $staff_service_exists, 'service_id' );
						
			/* Unchecked List */			
			foreach($_POST['staff_unchecked_services'] as $unchecked) {
				
				if( in_array($unchecked, $existsService) === true ) {
					
					/* Delete Service from Staff List */
					$del_ser_arg = array(
						'is_deleted'    => 1,
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);	
					
					$del_where = array(
						'service_id' => ''.wp_kses_post($unchecked).'',
						'staff_id'   => ''.wp_kses_post($_POST['staff_id']).'',
					);
					
					$del_service_staff = $wpdb->update( $gfb_staff_service_mapping, $del_ser_arg, $del_where );
				}	
			}
			
			/* Checked List */
			foreach($_POST['staff_checked_services'] as $checked) {
				
				if( in_array($checked["serid"], $existsService) === true ) {
					
					/* Update Service from Staff List */
					$upd_ser_arg = array(
						'service_price' => ''.wp_kses_post($checked["price"]).'',
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);	
					
					$upd_where = array(
						'service_id' => ''.wp_kses_post($checked["serid"]).'',
						'staff_id'   => ''.wp_kses_post($_POST['staff_id']).'',
					);
					
					$upd_service_staff = $wpdb->update( $gfb_staff_service_mapping, $upd_ser_arg, $upd_where );
				}	
				else
				{
					
					/* Insert Service from Staff List */
					$ins_ser_arg = array(
						'service_id'    => ''.wp_kses_post($checked["serid"]).'',
						'staff_id'      => ''.wp_kses_post($_POST['staff_id']).'',
						'service_price' => ''.wp_kses_post($checked["price"]).'',
						'created_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'created_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);	
					
					$ins_service_staff = $wpdb->insert( $gfb_staff_service_mapping, $ins_ser_arg );	
				}			
				
			}	
			
			echo json_encode( array('msg' => 'true', 'text' => __('Services updated successfully.', 'gfb') ) );			
			die();	
		}
		
		/* Get service from staff service mapping table */
		function gfbStaffServiceList($staff_id) {
			
			global $wpdb;
			
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';	
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';				
					
			$staff_service_result = $wpdb->get_results( 'SELECT sm.service_id, sm.service_title, sm.service_price, ss.staff_id, ss.service_id as staff_service_id, REPLACE(ss.service_price, "'.get_option('gfb_currency_symbol').'", "") as staff_service
FROM '.$gfb_services_mst.' as sm 
LEFT JOIN '.$gfb_staff_service_mapping.' as ss ON sm.service_id = ss.service_id AND ss.staff_id="'.$staff_id.'" AND ss.is_deleted=0
WHERE sm.is_deleted=0
GROUP BY sm.service_id', ARRAY_A );		
			
			return $staff_service_result;
		}
		
		/* List TimeSlot From Weekday */
		function gfbListStaffTimeSlot($weekday, $staff_id){
			
			global $wpdb;			
						
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			
			$time_slot_result = $wpdb->get_results( 'SELECT st.staff_id, st.max_appointment_capacity as staff_max_appointments, tm.time_slot_name, tm.weekday, DATE_FORMAT(tm.slot_start_time, "%H:%i") as slot_start_time, DATE_FORMAT(tm.slot_end_time, "%H:%i") as slot_end_time, st.time_slot_id, tm.max_appointment_capacity, tm.is_custom_slot
FROM '.$gfb_staff_schedule_mapping.' AS st 
INNER JOIN '.$gfb_time_slot_mst.' as tm ON st.time_slot_id=tm.time_slot_id AND tm.is_deleted=0 and tm.weekday="'.$weekday.'"
WHERE st.is_deleted=0 AND st.staff_id="'.$staff_id.'"', ARRAY_A );
				
			return $time_slot_result;		
		}

		/* Add breaks for time slots */
		function gfbAddStaffBreak() {

			if ( isset( $_POST['staff_id'] ) && isset( $_POST['day_name'] ) ) {
				global $wpdb;
				$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
				$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
				$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
				$selected_breaks = isset( $_POST['breaks'] ) ? $_POST['breaks'] : array();
				$day = $_POST['day_name'];
				$day_key = $_POST['day_key'];
				$staff_id = $_POST['staff_id'];
				$breaks = get_option( 'gfb_staff_'.$staff_id.'_slots_for_'.$day );
				$type = $_POST['type'];

				if ( isset( $breaks['add_break'] ) && is_array( $breaks['add_break'] ) && count( $breaks['add_break'] ) > 0 ) {

					
					foreach ( $breaks['add_break'] as $key => $b ) {
						$breaks['add_break'][ $key ] = 'false';
					}				

					if ( count( $selected_breaks ) > 0 ) {
						foreach ( $selected_breaks as $break ) {
							$breaks['add_break'][ $break ] = 'true';
						}
					}		

					update_option( 'gfb_staff_'.$staff_id.'_slots_for_'.$day, $breaks );

					if ( 'add' == $type ) {
						$txt = 'Break added successfully.';
					} else {
						$txt = 'Break removed successfully.';
					}
					echo json_encode( array('msg' => 'true', 'text' => $txt) );
					die();
				} else {					
					echo json_encode( array('msg' => 'false', 'text' => 'No slots for break found.') );
					die();
				}
			}

			echo json_encode( array('msg' => 'false', 'text' => 'Staff id or day or breaks is missing.') );
			die();
		}
		
		/* Add New Time Slots for Staff */
		function gfbAddStaffTimeSlot() {
			
			if ( isset( $_POST['staff_id'] ) && isset( $_POST['duration'] ) && isset( $_POST['interval'] ) && ! empty( trim( $_POST['duration'] ) ) && ! empty( trim( $_POST['interval'] ) ) ) {				

				/* VARIABLE DEFINED */
				global $wpdb;
				$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
				$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
				$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
				$gfb_createdby = get_current_user_id();
				$gfb_ip = GFB_Core::gfbIpAddress();				
				$flag_time = false;
				$message = array();
				$result = array(
					'sunday' => array(),
					'monday' => array(),
					'tuesday' => array(),
					'wednesday' => array(),
					'thursday' => array(),
					'friday' => array(),
					'saturday' => array()
				);				

				$staff_id = $_POST['staff_id'];
				$duration = $_POST['duration'];
				$interval = $_POST['interval'];				

				if ( isset( $_POST['days'] ) && is_array( $_POST['days'] ) ) {
					foreach( $_POST['days'] as $day_key => $day ) {

						$startTime = '';
						$endTime = '';
						$slot_cap = '';

						if ( isset( $_POST['start_time_' . $day] ) && isset( $_POST['end_time_' . $day] ) && empty( $_POST['start_time_' . $day] ) && empty( $_POST['end_time_' . $day] ) ) {
							if ( empty($message[$day]) ) {
								//$message[$day] = strtoupper($day) . ' - Start Time and End Time are missing.';
								$message[$day] = '';
							}
						} elseif ( isset( $_POST['start_time_' . $day] ) && isset( $_POST['end_time_' . $day] ) && $_POST['start_time_' . $day] == $_POST['end_time_' . $day] ) {
							if ( empty($message[$day]) ) {
								$message[$day] = strtoupper($day) . ' - Start Time and End Time can\'t be same.';
							}
						} elseif ( isset( $_POST['start_time_' . $day] ) && empty( $_POST['start_time_' . $day] ) ) { 
							if ( empty($message[$day]) ) {
								$message[$day] = strtoupper($day) . ' - Start Time is missing.';
							}
						} elseif ( isset( $_POST['end_time_' . $day] ) && empty( $_POST['end_time_' . $day] ) ) {							
							if ( empty($message[$day]) ) {
								$message[$day] = strtoupper($day) . ' - End Time is missing.';
							}
						} elseif( isset( $_POST['slot_cap_' . $day] ) && empty( $_POST['slot_cap_' . $day] ) ) {
							if ( empty($message[$day]) ) {
								$message[$day] = strtoupper($day) . ' - Slot Capacity is missing.';
							}							
						}

						$prev_startTime = get_option( 'gfb_staff_'.$staff_id.'_'.$day.'_start_time' );
						$prev_endTime = get_option( 'gfb_staff_'.$staff_id.'_'.$day.'_end_time' );
						$prev_slot_cap = get_option( 'gfb_staff_'.$staff_id.'_'.$day.'_slot_cap' );

						$startTime = $_POST['start_time_' . $day];
						$endTime = $_POST['end_time_' . $day];
						$slot_cap = $_POST['slot_cap_' . $day];

						if ( empty($startTime) || empty($endTime) || $startTime != $endTime ) {
							// adding start & end time of each day in wp_option tbl.						
							update_option( 'gfb_staff_'.$staff_id.'_'.$day.'_start_time', $startTime );
							update_option( 'gfb_staff_'.$staff_id.'_'.$day.'_end_time', $endTime );
							update_option( 'gfb_staff_'.$staff_id.'_'.$day.'_slot_cap', $slot_cap );
						}

						// creating slots with start time & end time.
						if ( '' != $startTime && '' != $endTime && $startTime != $endTime ) {
							$created_slots = SplitTime( $startTime, $endTime, $duration, $interval );							
							$result[$day]['slots'] = $created_slots['slots'];

						} else {
							$created_slots = array();
						}



						$prev_created_slots = get_option( 'gfb_staff_'.$staff_id.'_slots_for_'.$day );

						$change_in_slots_detected = false;

						if ( isset( $prev_created_slots['slots'] ) && isset( $created_slots['slots'] ) ) {

							foreach ( $prev_created_slots['slots'] as $key => $slot ) {
								if ( $slot != $created_slots['slots'][$key] ) {
									$change_in_slots_detected = true;
									break;
								}
							}

						}

						if ( $prev_startTime != $startTime || $prev_endTime != $endTime || $change_in_slots_detected ) {
							update_option( 'gfb_staff_'.$staff_id.'_slots_for_'.$day, $created_slots );
						}

						//Delete time slot and staff schedule slot from tables if no appointment created with that slot.
						$check_exists_appointment_for_timeslot = $wpdb->get_results( "SELECT ssm.staff_slot_mapping_id, ssm.time_slot_id FROM ".$gfb_staff_schedule_mapping." ssm INNER JOIN ".$gfb_time_slot_mst." tm ON ssm.time_slot_id=tm.time_slot_id WHERE ssm.staff_id=".trim($staff_id)." and tm.weekday=".trim($day_key)."", ARRAY_A );

						if ( ! empty( $check_exists_appointment_for_timeslot ) ) {
							$staff_slot_mapping_id_array = $this->mainCalssFunction->gfb_array_column($check_exists_appointment_for_timeslot, 'staff_slot_mapping_id');

							foreach ( $staff_slot_mapping_id_array as $staff_slot_mapping_id ) {

								//check if appointment exisit
								$check_appointment = $wpdb->get_results( "SELECT appointment_id FROM ".$gfb_appointments_mst." WHERE staff_id=".trim($staff_id)." and staff_slot_mapping_id=".trim($staff_slot_mapping_id)."", ARRAY_A );

								if ( empty( $check_appointment ) ) { //no appointment exist in the appointemnt table.
									
									$wpdb->query( "DELETE FROM ".$gfb_staff_schedule_mapping." WHERE staff_id=".trim($staff_id)." and staff_slot_mapping_id=".$staff_slot_mapping_id."" );

									$wpdb->query( "DELETE FROM ".$gfb_time_slot_mst." WHERE time_slot_id=".$staff_slot_mapping_id." and weekday=".trim($day_key)."" );
								}

							}

						}


						$check_exists = $wpdb->get_results( "SELECT ssm.time_slot_id FROM ".$gfb_staff_schedule_mapping." ssm INNER JOIN ".$gfb_time_slot_mst." tm ON ssm.time_slot_id=tm.time_slot_id WHERE ssm.is_deleted=0 AND tm.is_deleted=0 AND ssm.staff_id=".trim($staff_id)." and tm.weekday=".trim($day_key)."", ARRAY_A );

						if ( !empty( $check_exists ) ) {
							$time_slot_array = implode(', ', $this->mainCalssFunction->gfb_array_column($check_exists, 'time_slot_id'));
							
							$update_stimeslot = $wpdb->query( "UPDATE ".$gfb_staff_schedule_mapping." SET is_deleted=1, modified_by=".wp_kses_post($gfb_createdby).", modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.") AND staff_id=".trim($staff_id)."" );
							
							/* Clear time slots from master */
							$update_timeslot = $wpdb->query( "UPDATE ".$gfb_time_slot_mst." SET is_deleted=1, modified_by=".wp_kses_post($gfb_createdby).", modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.") AND is_custom_slot=1 AND weekday=".trim($day_key)."" );
						}
										

						//adding each slots to slot table and map with staff schedules.
						if ( isset( $created_slots['slot_start_time'] ) && is_array( $created_slots['slot_start_time'] ) && count( $created_slots['slot_start_time'] ) > 0 ) {
							
							foreach ( $created_slots['slot_start_time'] as $key => $slot ) {

								/* INSERT INTO TIMESLOT MASTER */
								$add_timeslot_arg = array(
									'time_slot_name'           => '',
									'slot_start_time'          => ''.wp_kses_post(trim($slot)).'',
									'slot_end_time'			   => ''.wp_kses_post(trim($created_slots['slot_end_time'][$key])).'',
									'max_appointment_capacity' => ''.wp_kses_post(trim($slot_cap)).'',
									'weekday'                  => ''.wp_kses_post(trim($day_key)).'',
									'is_custom_slot'		   => 1,
									'created_by' 	           => ''.wp_kses_post($gfb_createdby).'',
									'created_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
								);

								$insert_timeslot = $wpdb->insert($gfb_time_slot_mst, $add_timeslot_arg);
						
								/* Last Insert Timeslot id */
								$lastStaffSlotId = $wpdb->insert_id;
								
								/* INSERT INTO STAFF SLOT MAPPING TABLE */
								$add_staff_slot_arg = array(
									'staff_id'			       => ''.wp_kses_post(trim($staff_id)).'',
									'time_slot_id'  		   => ''.wp_kses_post(trim($lastStaffSlotId)).'',
									'max_appointment_capacity' => ''.wp_kses_post(trim($slot_cap)).'',
									'created_by' 	           => ''.wp_kses_post($gfb_createdby).'',
									'created_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
								);	
								
								/* Add Slot in Staff Slots Mapping Table */
								$insert_staff_slot = $wpdb->insert($gfb_staff_schedule_mapping, $add_staff_slot_arg);
								
								if($insert_timeslot && $insert_staff_slot) {
									if ( empty($message[$day]) ) {
										$message[$day] = 'true';
									}									

								} else {
									if ( empty($message[$day]) ) {
										$message[$day] = 'false';
									}
								}
							}							

						} else {
							if ( empty($message[$day]) ) {
								$message[$day] = 'false';
							}							
						}
					}
				}				

			} else {
				echo json_encode( array('msg' => 'false', 'text' => 'Please set time interval and time duration from settings.') );
				wp_die();
			}

			echo json_encode( array('msg' => 'true', 'message' => $message, 'tab' => 'timings-tab', 'url' => admin_url().'admin.php?page=gravity-form-booking-staff&state='.base64_encode($staff_id).'&tab=timings-tab', 'created_slots' => $createdBreakSlots ) );
			wp_die();

		}
		
		/* Delete Staff Timeslot */
		function gfbDeleteStaffTimeSlot(){
									
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';			
		
			/* CHECK WHEATHER SLOT EXISTS OR NOT */
			$check_exists = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_staff_schedule_mapping." WHERE is_deleted=0 AND time_slot_id='".$_POST['time_slot_id']."' AND staff_id='".trim($_POST['staff_id'])."'", ARRAY_A );
			
			if( !empty( $check_exists ) ) 
			{
				$add_timeslot_arg = array(
					'is_deleted'    => 1,
					'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
					'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
					'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
				);
				
				$where_timeslot = array(
					'time_slot_id' => ''.$_POST['time_slot_id'].'',
					'staff_id'     => ''.$_POST['staff_id'].''
				);
				
				$delete_timeslot = $wpdb->update($gfb_staff_schedule_mapping, $add_timeslot_arg, $where_timeslot);	
				
				if($delete_timeslot)
				{		
					echo json_encode( array('msg' => 'true', 'text' => 'Time Slot deleted successfully.') );
					die();
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not deleted successfully.') );
					die();
				}
			}				
			
			die();		
		}
		
		/* Edit Staff Timeslot Form */
		function gfbEditStaffTimeSlotForm(){
			
			if (isset($_POST['time_slot_id'])):
				require(GFB_ADMIN_TAB . 'staff/editStaffTimeSlot.php');
			endif;
			die();	
		}
		
		/* Get TimeSlot Details from id */
		function gfbGetStaffTimeSlotWithId($slotid, $staffid) {
			
			global $wpdb;			
						
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			
			$time_slot_result = $wpdb->get_results( 'SELECT st.staff_id, st.max_appointment_capacity as staff_max_appointments, tm.time_slot_name, tm.weekday, DATE_FORMAT(tm.slot_start_time, "%H:%i") as slot_start_time, DATE_FORMAT(tm.slot_end_time, "%H:%i") as slot_end_time, st.time_slot_id, tm.max_appointment_capacity, tm.is_custom_slot
FROM '.$gfb_staff_schedule_mapping.' AS st 
INNER JOIN '.$gfb_time_slot_mst.' as tm ON st.time_slot_id=tm.time_slot_id AND tm.is_deleted=0 
WHERE st.is_deleted=0 AND st.staff_id="'.$staffid.'" AND st.time_slot_id="'.$slotid.'"', ARRAY_A );
				
			return $time_slot_result;
		}
		
		/* Update Function to edit staff custom time slot */
		function gfbUpdateStaffTimeSlot() {
			
			parse_str($_POST['timeslot_form'], $timeslot);			
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($timeslot['time_slot_name']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please enter time slot title.') );
				die();
			}
			if( trim($timeslot['slot_start_time']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select start time of time slot.') );
				die();
			}
			if( trim($timeslot['slot_end_time']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select end time of time slot.') );
				die();
			}
			if( trim($timeslot['max_appointment_capacity']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please enter maximum no. of appointments allowed to be booked in paticular time slot.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_staff_schedule_mapping." WHERE is_deleted=0 AND time_slot_id='".trim($timeslot['time_slot_id'])."' AND staff_id='".trim($timeslot['staff_id'])."'", ARRAY_A );
								
				if( !empty( $check_exists ) ) 
				{
					$update_timeslot_arg = array(
						'time_slot_name'           => ''.wp_kses_post(trim($timeslot['time_slot_name'])).'',
						'slot_start_time'          => ''.wp_kses_post(trim($timeslot['slot_start_time'])).'',
						'slot_end_time'			   => ''.wp_kses_post(trim($timeslot['slot_end_time'])).'',
						'max_appointment_capacity' => "".wp_kses_post(trim($timeslot['max_appointment_capacity']))."",
						'modified_by' 			   => ''.wp_kses_post($gfb_createdby).'',
						'modified_date'			   => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 			   => ''.wp_kses_post($gfb_ip).'',
					);
					
					$update_timeslot_where = array(
						'time_slot_id'   => ''.trim($timeslot['time_slot_id']).'',
						'is_custom_slot' => 1
					);
					
					$update_timeslot = $wpdb->update($gfb_time_slot_mst, $update_timeslot_arg, $update_timeslot_where);	
					
					if($update_timeslot)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Time Slot updated successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-staff&state='.base64_encode($timeslot['staff_id'])) );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not updated successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'No timeslot found.') );
					die();	
				}
				
			}
			
			die();	
		}
		
		/* Get staff google data */
		function gfbStaffGoogleData($staff_id) {
			
			global $wpdb;	
			$staff_mst_tbl = $wpdb->prefix . 'gfb_staff_mst';	
				
			$staff_result = $wpdb->get_results("SELECT staff_gcal_data, staff_gcal_id FROM ".$staff_mst_tbl." where staff_id='".trim($staff_id)."' and is_deleted=0",ARRAY_A);				
			return $staff_result;	
		}
		
		/* Get staff google data */
		function gfbStaffGoogleDataEncode($staff_id) {
			
			global $wpdb;	
			$staff_mst_tbl = $wpdb->prefix . 'gfb_staff_mst';	
				
			$staff_result = $wpdb->get_results("SELECT staff_gcal_data, staff_gcal_id FROM ".$staff_mst_tbl." where staff_id='".trim($staff_id)."' and is_deleted=0",ARRAY_A);
			
			return $staff_result[0]["staff_gcal_data"];
		}
		
		/* Check token expire */
		function gfbIsTokenExpire($staff_id) {
			
			global $wpdb;	
			$staff_mst_tbl = $wpdb->prefix . 'gfb_staff_mst';	
				
			$staff_result = $wpdb->get_results("SELECT staff_gcal_data, staff_gcal_id FROM ".$staff_mst_tbl." where staff_id='".trim($staff_id)."' and is_deleted=0",ARRAY_A);
						
			if( !empty( $staff_result[0]["staff_gcal_data"] ) ) {
			
				$token_data = json_decode( $staff_result[0]["staff_gcal_data"] );				
				$expired = ($token_data->createtime + ($token_data->expires_in - 30)) < time();
				return $expired;

			
			}
			else {
				return true;	
			}
			
		}
		
		/* Check watch request expire */
		function gfbIsWatchRequestExpire($staff_id) {
			
			global $wpdb;
			
			$staff_mst_tbl = $wpdb->prefix . 'gfb_staff_mst';	
				
			$staff_result = $wpdb->get_results("SELECT staff_gcal_watch_data FROM ".$staff_mst_tbl." where staff_id='".trim($staff_id)."' and is_deleted=0",ARRAY_A);
						
			if( !empty( $staff_result[0]["staff_gcal_watch_data"] ) ) {
				
				$data = json_decode( $staff_result[0]["staff_gcal_watch_data"] );
					// If the token is set to expire in the next 30 seconds.
				  $expired = (substr($data->expiration, 0, -3)-30) < time();
				  return $expired;
				
			}
			else {
				return true;	
			}
			
		}
		
		function gfbRefreshToken($refreshToken,$staffId) {
			
			$this->gfbRefreshTokenRequest(
				array(
				  'client_id' => get_option('gfb_client_id'),
				  'client_secret' => get_option('gfb_client_secrete'),
				  'refresh_token' => $refreshToken,
				  'grant_type' => 'refresh_token',
				  'staff_id' =>$staffId
				)
			);
		}	
		
		/* get new token from google */
		function gfbRefreshTokenRequest( $params ) {
			
			global $wpdb;			
			$wp_gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			
			$google = new GFB_GoogleCalendarApi;			
			$data = $google->GetNewAccessToken($params["client_id"], $params["client_secret"], $params["refresh_token"]);
			
			//print_r($data);exit;
			$data["createtime"] = time();
			$data["refresh_token"] = $params["refresh_token"];
						
			$google_data = json_encode($data);
			$staff_id = $params['staff_id'];
			$updateStaffData = array(
				'staff_gcal_data' => ''.wp_kses_post($google_data).'',
				'modified_by'	  => ''.wp_kses_post(get_current_user_id()).'',
				'modified_date'   => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	  => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
 			);
			
			
			$where_staff = array( 'staff_id' => $staff_id);
			
			$update = $wpdb->update($wp_gfb_staff_mst, $updateStaffData, $where_staff);
			
		}
		
		function gfbClearStaffTimeSlot() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
			
			$check_exists = $wpdb->get_results( "SELECT ssm.time_slot_id FROM ".$gfb_staff_schedule_mapping." ssm INNER JOIN ".$gfb_time_slot_mst." tm ON ssm.time_slot_id=tm.time_slot_id WHERE ssm.is_deleted=0 AND tm.is_deleted=0 AND ssm.staff_id=".trim($_POST['staff'])." and tm.weekday=".trim($_POST['weekday'])."", ARRAY_A );
						
			if( !empty( $check_exists ) ) 
			{
				$time_slot_array = implode(', ', $this->mainCalssFunction->gfb_array_column($check_exists, 'time_slot_id'));
				
				$update_stimeslot = $wpdb->query( "UPDATE ".$gfb_staff_schedule_mapping." SET is_deleted=1, modified_by=".wp_kses_post($gfb_createdby).", modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.") AND staff_id=".trim($_POST['staff'])."" );
				
				/* Clear time slots from master */
				$update_timeslot = $wpdb->query( "UPDATE ".$gfb_time_slot_mst." SET is_deleted=1, modified_by=".wp_kses_post($gfb_createdby).", modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.") AND is_custom_slot=1 AND weekday=".trim($_POST['weekday'])."" );
				
				if( $update_stimeslot > 0 )
				{		
					echo json_encode( array('msg' => 'true', 'text' => 'Time Slot Deleted successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-staff') );
					die();
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not Deleted successfully.') );
					die();
				}
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'No timeslot found.') );
				die();	
			}
				
			die();
		}
		
		/* Remove Staff  */
		function gfbDeleteStaff() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK WHEATHER DATE EXISTS OR NOT */
			$check_exists = $wpdb->get_results( "SELECT staff_id, user_id FROM ".$gfb_staff_mst." WHERE is_deleted=0 AND staff_id='".$_POST["staff_id"]."'", ARRAY_A );
			
			if( !empty( $check_exists ) ) 
			{
				$delete_staff_arg = array(
					'is_deleted' => 1,
					'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
					'modified_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
					'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
				);
				
				$where_staff = array(
					'staff_id' => ''.trim($_POST['staff_id']).'',
				);
				
				wp_delete_user( $check_exists[0]["user_id"] );
				$update_staff = $wpdb->update($gfb_staff_mst, $delete_staff_arg, $where_staff);
				$update_staff_holiday = $wpdb->update($gfb_staff_holiday_mapping, $delete_staff_arg, $where_staff);
				$update_staff_schedule = $wpdb->update($gfb_staff_schedule_mapping, $delete_staff_arg, $where_staff);
				$update_staff_service = $wpdb->update($gfb_staff_service_mapping, $delete_staff_arg, $where_staff);	
				
				if( $update_staff)
				{		
					echo json_encode( array('msg' => 'true', 'text' => __('Staff Member deleted successfully.', 'gfb'), 'url' => admin_url().'admin.php?page=gravity-form-booking-staff' ) );
					die();
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => __('Staff Member not deleted successfully.', 'gfb') ) );
					die();
				}
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => __('No staff member found.', 'gfb') ) );
				die();	
			}
							
			die();	
		}
		
	
	}
	
	global $gfbStaff;
	$gfbStaff = new GravityFormBookingStaff;
	

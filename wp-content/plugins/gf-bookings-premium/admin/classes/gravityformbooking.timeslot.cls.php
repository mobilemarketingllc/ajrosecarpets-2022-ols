<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingTimeSlot {
		public $mainCalssFunction;
		function __construct() {
			$this->mainCalssFunction = new gfb_bookings_addon();
			add_action( 'wp_ajax_gfb_add_new_timeslot', array( &$this, 'gfbAddTimeSlot' ) );
			add_action( 'wp_ajax_gfb_delete_timeslot', array( &$this, 'gfbDeleteTimeSlot' ) );
			add_action( 'wp_ajax_gfb_display_edit_timeslot', array(&$this, 'gfbEditTimeSlotForm') );
			add_action( 'wp_ajax_gfb_edit_timeslot', array(&$this, 'gfbUpdateTimeSlot') );
			add_action( 'wp_ajax_gfb_clear_slot',  array(&$this, 'gfbClearTimeSlot'));	
		}
		
		
		/* Edit Timeslot Form */
		function gfbEditTimeSlotForm(){
			
			if (isset($_POST['time_slot_id'])):
				require(GFB_ADMIN_TAB . 'timeSlots/editTimeSlot.php');
			endif;
			die();	
		}
		
		/* Add New Time Slots */
		function gfbAddTimeSlot() {
			
			parse_str($_POST['formdata'], $timeslot);			
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($timeslot['time_slot_name']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please enter time slot title.') );
				die();
			}
			if( trim($timeslot['slot_start_time']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select start time of time slot.') );
				die();
			}
			if( trim($timeslot['slot_end_time']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select end time of time slot.') );
				die();
			}
			if( trim($timeslot['max_appointment_capacity']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please enter maximum no. of appointments allowed to be booked in paticular time slot.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER TIME SLOTS ARE NOT OVERLAPPING WITH OTHER SLOTS */
				/* $timeslot_overlap = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_time_slot_mst." WHERE is_deleted=0  AND ((slot_start_time between '".trim($timeslot['slot_start_time'])."' AND '".trim($timeslot['slot_end_time'])."') or (slot_end_time between '".trim($timeslot['slot_start_time'])."' AND '".trim($timeslot['slot_end_time'])."')) AND weekday='".trim($_POST['slotday'])."'", ARRAY_A );

				if(!empty($timeslot_overlap))
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time Slot is over lapping.') );
					die();
				}
				 */
				/* CHECK WHEATHER SLOT NAME IS SAME OR NOT */
				$timeslot_name = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_time_slot_mst." WHERE is_deleted=0  AND time_slot_name='".trim($timeslot['time_slot_name'])."' AND weekday='".trim($_POST['slotday'])."'", ARRAY_A );

				if(!empty($timeslot_name))
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time slotname already exists.') );
					die();
				}
				
				/* CHECK WHEATHER TIME SLOT EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_time_slot_mst." WHERE is_deleted=0 AND time_slot_name='".trim($timeslot['time_slot_name'])."' AND slot_start_time='".trim($timeslot['slot_start_time'])."' AND slot_end_time='".trim($timeslot['slot_end_time'])."' AND weekday='".trim($_POST['slotday'])."'", ARRAY_A );
				
				if( empty( $check_exists ) ) 
				{
					$startTime  = date("H:i:00", strtotime(trim($timeslot['slot_start_time'])));

					$endTime  = date("H:i:59", strtotime(trim($timeslot['slot_end_time'])));
					
					/* INSERT INTO TIMESLOT MASTER */
					$add_timeslot_arg = array(
						'time_slot_name'           => ''.wp_kses_post(trim($timeslot['time_slot_name'])).'',
						'slot_start_time'          => ''.wp_kses_post(trim($startTime)).'',
						'slot_end_time'			   => ''.wp_kses_post(trim($endTime)).'',
						'max_appointment_capacity' => ''.wp_kses_post(trim($timeslot['max_appointment_capacity'])).'',
						'weekday'                  => ''.wp_kses_post(trim($_POST['slotday'])).'',
						'created_by' 	           => ''.wp_kses_post($gfb_createdby).'',
						'created_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
					);
					
					$insert_timeslot = $wpdb->insert($gfb_time_slot_mst, $add_timeslot_arg);
					
					/* Last Insert Timeslot id */
					$lastStaffSlotId = $wpdb->insert_id;
					
					/* INSERT INTO STAFF SLOT MAPPING TABLE */
					$insert_staff_slot = $this->gfbStaffSlot($lastStaffSlotId, $gfb_createdby, $gfb_ip, trim($timeslot['max_appointment_capacity']), $gfb_staff_schedule_mapping);
					
					if($insert_timeslot && $insert_staff_slot)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Time Slot added successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-time-slot') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not added successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time Slot already exists.') );
					die();	
				}
				
			}
			
			die();	
		}
		
		/* ADD TIMESLOT IN STAFF SLOT TABLE */
		function gfbStaffSlot($lastStaffSlotId, $gfb_createdby, $gfb_ip, $max_appointment_capacity, $gfb_staff_schedule_mapping){
			
			global $wpdb;
			global $gfbStaff;
			
			/* Staff Id List */
			$staffList = $gfbStaff->getStaffNameList();
			
			if(	count($staffList) ) {	
			
				foreach( $staffList as $staff ){
					
					/* Add Slots in Master */
					$add_staff_slot_arg = array(
						'staff_id'			       => ''.wp_kses_post(trim($staff['staff_id'])).'',
						'time_slot_id'  		   => ''.wp_kses_post(trim($lastStaffSlotId)).'',
						'max_appointment_capacity' => ''.wp_kses_post(trim($max_appointment_capacity)).'',
						'created_by' 	           => ''.wp_kses_post($gfb_createdby).'',
						'created_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
					);	
					
					/* Add Slot in Staff Slots Mapping Table */
					$insert_slot = $wpdb->insert($gfb_staff_schedule_mapping, $add_staff_slot_arg);
				}
			
			}
			
			return true;
			
		}
		
		/* List TimeSlot From Weekday */
		function gfbListTimeSlot($weekday){
			
			global $wpdb;			
						
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			
			$time_slot_result = $wpdb->get_results( 'SELECT time_slot_id, time_slot_name, weekday, DATE_FORMAT(slot_start_time, "%H:%i") as slot_start_time, DATE_FORMAT(slot_end_time, "%H:%i") as slot_end_time, max_appointment_capacity FROM '.$gfb_time_slot_mst.' WHERE is_deleted=0 AND is_custom_slot=0 AND weekday='.trim($weekday).'', ARRAY_A );
				
			return $time_slot_result;		
		}
		
		/* Get TimeSlot From ID */
		function gfbGetTimeSlot($timeslotid){
			
			global $wpdb;			
						
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			
			$time_slot_result = $wpdb->get_results( 'SELECT time_slot_id, time_slot_name, weekday, DATE_FORMAT(slot_start_time, "%H:%i") as slot_start_time, DATE_FORMAT(slot_end_time, "%H:%i") as slot_end_time, max_appointment_capacity FROM '.$gfb_time_slot_mst.' WHERE is_deleted=0 AND is_custom_slot=0 AND time_slot_id='.trim($timeslotid).'', ARRAY_A );
				
			return $time_slot_result;		
		}
		
		
		
		/* Delete Timeslot */
		function gfbDeleteTimeSlot(){
							
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';			
		
			/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
			$check_exists = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_time_slot_mst." WHERE is_deleted=0 AND time_slot_id='".trim($_POST['time_slot_id'])."'", ARRAY_A );
			
			if( !empty( $check_exists ) ) 
			{
				$add_timeslot_arg = array(
					'is_deleted'    => 1,
					'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
					'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
					'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
				);
				
				$where_timeslot = array(
					'time_slot_id' => ''.trim($_POST['time_slot_id']).''
				);
				
				$delete_timeslot = $wpdb->update($gfb_time_slot_mst, $add_timeslot_arg, $where_timeslot);	
				
				if($delete_timeslot)
				{		
					echo json_encode( array('msg' => 'true', 'text' => 'Time Slot deleted successfully.') );
					die();
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not deleted successfully.') );
					die();
				}
			}				
			
			die();		
		}
		
		/* UPDATE TIME SLOTS */
		function gfbUpdateTimeSlot() {
			
			parse_str($_POST['timeslot_form'], $timeslot);			
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($timeslot['time_slot_name']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please enter time slot title.') );
				die();
			}
			if( trim($timeslot['slot_start_time']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select start time of time slot.') );
				die();
			}
			if( trim($timeslot['slot_end_time']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please select end time of time slot.') );
				die();
			}
			if( trim($timeslot['max_appointment_capacity']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Please enter maximum no. of appointments allowed to be booked in paticular time slot.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_time_slot_mst." WHERE is_deleted=0 AND time_slot_id='".trim($timeslot['time_slot_id'])."'", ARRAY_A );
								
				if( !empty( $check_exists ) ) 
				{
					$update_timeslot_arg = array(
						'time_slot_name'           => ''.wp_kses_post(trim($timeslot['time_slot_name'])).'',
						'slot_start_time'          => ''.wp_kses_post(trim($timeslot['slot_start_time'])).'',
						'slot_end_time'			   => ''.wp_kses_post(trim($timeslot['slot_end_time'])).'',
						'max_appointment_capacity' => wp_kses_post(trim($timeslot['max_appointment_capacity'])),
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$update_timeslot_where = array(
						'time_slot_id' => ''.trim($timeslot['time_slot_id']).'',
					);
					
					$update_timeslot = $wpdb->update($gfb_time_slot_mst, $update_timeslot_arg, $update_timeslot_where);	
					
					/* Update Slots in Mapping */
					$update_staff_slot_arg = array(
						'max_appointment_capacity' => wp_kses_post(trim($timeslot['max_appointment_capacity'])),
						'modified_by' 	           => ''.wp_kses_post($gfb_createdby).'',
						'modified_date'             => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	           => ''.wp_kses_post($gfb_ip).'',
					);
					
					$update_staff_slot_where = array(
						'time_slot_id' => ''.trim($timeslot['time_slot_id']).'',
					);						
		
					$update_staff_slot = $wpdb->update($gfb_staff_schedule_mapping,$update_staff_slot_arg, $update_staff_slot_where);
					
					
					if($update_timeslot)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Time Slot updated successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-time-slot') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not updated successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'No timeslot found.') );
					die();	
				}
				
			}
			
			die();
			
		}
		
		function gfbClearTimeSlot() {
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
			$check_exists = $wpdb->get_results( "SELECT time_slot_id FROM ".$gfb_time_slot_mst." WHERE is_deleted=0 AND weekday='".trim($_POST['weekday'])."' and is_custom_slot=0", ARRAY_A );

			if( !empty( $check_exists ) ) 
			{
				$time_slot_array = implode(',', $this->mainCalssFunction->gfb_array_column($check_exists, 'time_slot_id'));
				
				/* Clear time slots from master */
				$update_timeslot_arg = array(
					'is_deleted'    => '1',
					'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
					'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
					'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
				);
				
				$update_timeslot_where = array(
					'weekday' => ''.trim($_POST['weekday']).'',
					'is_custom_slot' => 0
				);
				
				
				/* Clear time slots from staff */
				$update_stimeslot = $wpdb->get_results("UPDATE ".$gfb_staff_schedule_mapping." SET is_deleted=1, modified_by='".wp_kses_post($gfb_createdby)."', modified_date='".wp_kses_post(current_time("Y-m-d H:i:s"))."', ip_address='".wp_kses_post($gfb_ip)."' WHERE time_slot_id IN (".$time_slot_array.")");
				
				/* Clear time slots from master */
				$update_timeslot = $wpdb->update($gfb_time_slot_mst, $update_timeslot_arg, $update_timeslot_where);	
				
				
				if($update_timeslot)
				{	
					echo json_encode( array('msg' => 'true', 'text' => 'Time Slot Deleted successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-time-slot') );
					die();
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Time Slot not Deleted.') );
					die();
				}
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'No timeslot found.') );
				die();	
			}
				
			die();
		}
		
		/* Get time slot start and end time for event */
		function gfbGetTimeSlotValue($slot_mapping_id) {
			
			global $wpdb;	
			$staff_schedule_mapping_tbl = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$time_slot_mst_tbl = $wpdb->prefix . 'gfb_time_slot_mst';
					
			$timeslot_slot_result = $wpdb->get_results("SELECT slot_start_time, slot_end_time FROM ".$staff_schedule_mapping_tbl." as ssm inner join ".$time_slot_mst_tbl ." as tsm on tsm.time_slot_id = ssm.time_slot_id where staff_slot_mapping_id=".trim($slot_mapping_id)." and ssm.is_deleted=0 and tsm.is_deleted=0",ARRAY_A);
				
			return $timeslot_slot_result;				
		}
	}

	
	global $gfbTimeSlotObj;
	$gfbTimeSlotObj = new GravityFormBookingTimeSlot;
	

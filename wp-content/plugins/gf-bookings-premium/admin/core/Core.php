<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; } 



class GFB_Core {

	public function __construct() {} 
	
	public static function gfbIpAddress() {
		
		$ip_address = "";
	
		if (getenv("HTTP_CLIENT_IP")) 
		{
			$ip_address = getenv("HTTP_CLIENT_IP");
		}
		else if(getenv("HTTP_X_FORWARDED_FOR")) 
		{
			$ip_address = getenv("HTTP_X_FORWARDED_FOR");
		}
		else if(getenv("REMOTE_ADDR")) 
		{
			$ip_address = getenv("REMOTE_ADDR");
		}
		else 
		{
			$ip_address = "UNKNOWN";
		}
		
		return $ip_address;			
	}	
	
	public static function gfbReplaceCode( $appointmentid, $client_notify_id = '', $staff_notify_id = '', $admin_notify_id = '' ) {
		
		global $gfbAppointmentObj;
		global $gfbEmail;
		global $wpdb;
		
		$appointmentdata = $gfbAppointmentObj->gfbGetAppointmentData($appointmentid);

		$staff_timezone = '';

		if ( get_option( 'gfb_staff_timezone_' . $appointmentdata[0]["staff_id"] ) != '' ) {
			$staff_timezone = get_option( 'gfb_staff_timezone_' . $appointmentdata[0]["staff_id"] );
		} else {
			$staff_timezone = wp_timezone_string();
		}

		if ( $staff_timezone != '' ) {
			$staff_timezone = '<span>Timezone: ' . $staff_timezone . '</span>';
		}
		
		$replaceCodes = array(
			"{appointment_date}"    => $appointmentdata[0]["appointment_date"],
			"{appointment_time}"    => $appointmentdata[0]["time_slot"] . '<br>' . $staff_timezone,
			"{booking_ref_number}"  => $appointmentdata[0]["booking_ref_no"],
			"{client_email}"        => $appointmentdata[0]["customer_email"],
			"{client_name}"  		=> $appointmentdata[0]["customer_name"],
			"{client_phone}"		=> $appointmentdata[0]["customer_contact"],
			"{company_address}" 	=> get_option("gfb_company_address"),
			"{company_name}" 		=> get_option("gfb_company_name"),
			"{company_email}" 		=> get_option("gfb_company_email"),
			"{company_phone}" 		=> get_option("gfb_company_contact_no"),
			"{company_website}" 	=> get_option("gfb_company_website"),
			"{service_name}" 		=> $appointmentdata[0]["service_title"],
			"{staff_name}" 			=> $appointmentdata[0]["staff_name"],
			"{total_price}" 		=> $appointmentdata[0]["total_amt"],
		);
		
		$basicCodes = $gfbEmail->gfbCodes();
		$codes = array_keys($basicCodes);
		$replacecd = array_keys($replaceCodes);

		$notification = $gfbEmail->gfbGetNotificationDetails( $client_notify_id, $staff_notify_id, $admin_notify_id );
		$userMessage = $notification[0]["userMessage"];
		$adminMessage = $notification[0]["adminMessage"];
		$staffMessage = $notification[0]["staffMessage"];
				
		foreach($codes as $code) {
			if( array_key_exists( $code, $replaceCodes ) === true ) {
				$userMessage = str_replace($code, $replaceCodes[$code], $userMessage);
				$adminMessage = str_replace($code, $replaceCodes[$code], $adminMessage);
				$staffMessage = str_replace($code, $replaceCodes[$code], $staffMessage);
			}
		}
		
		if( !empty( $userMessage) ) {
			$client_mail = self::gfbClientMailer( $appointmentdata[0]["customer_email"], $notification[0]["userSubject"], $userMessage );
		}
		
		if( !empty( $staffMessage) ) {
			$staff_mail = self::gfbStaffMailer( $appointmentdata[0]["staff_email"], $notification[0]["staffSubject"], $staffMessage );
		}
		
		if( !empty( $adminMessage) ) {
			$admin_mail = self::gfbAdminMailer( $notification[0]["adminSubject"], $adminMessage );
		}
		
		if($admin_mail) {
			return true;	
		}
		else {
			return false;	
		}
	}	
	
	/* Client Mail Function */
	public static function gfbClientMailer( $client_mail, $subject, $message ) {
		
		$from_email = get_option('gfb_sender_email');
		$from_name = get_option('gfb_sender_name');
		$to = $client_mail;		
		$footer = get_option('gfb_company_website');
		
		$template = file_get_contents( GFB_URL . '/includes/email-templates/appointment.html', true );
		
		$filter = array('%content%','%title%','%footer%');
		$replace = array(wpautop($message),$subject,$footer);
		
		if ( $from_email )
		{
			$headers[] = 'From: ' . ( $from_name ? $from_name . ' <' . $from_email . '>' : $from_email );
		}
		
		//$rep_message = str_replace($filter, $replace, $template);
		if ( ! empty( $template ) ) {
            $rep_message = str_replace($filter, $replace, $template);
        } else {
            $rep_message = wpautop($message);
        }
		
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		
		// Start output buffering to grab smtp debugging output
		ob_start();
		
		$res_mail = wp_mail($to, $subject, $rep_message, $headers);		
		
		// Grab the smtp debugging output
		ob_get_clean();
		
		return $res_mail;	
	}
	
	/* Staff Mail Function */
	public static function gfbStaffMailer( $staff_mail, $subject, $message ) {
		$from_email = get_option('gfb_sender_email');
		$from_name = get_option('gfb_sender_name');
		$to = $staff_mail;		
		$footer = get_option('gfb_company_website');
		
		$template = file_get_contents( GFB_URL . '/includes/email-templates/appointment.html', true );
		
		$filter = array('%content%','%title%','%footer%');
		$replace = array(wpautop($message),$subject,$footer);
		
		if ( $from_email )
		{
			$headers[] = 'From: ' . ( $from_name ? $from_name . ' <' . $from_email . '>' : $from_email );
		}
		
		//$rep_message = str_replace($filter, $replace, $template);
		if ( ! empty( $template ) ) {
            $rep_message = str_replace($filter, $replace, $template);
        } else {
            $rep_message = wpautop($message);
        }
		
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		
		// Start output buffering to grab smtp debugging output
		ob_start();
		
		$res_mail = wp_mail($to, $subject, $rep_message, $headers);		
		
		// Grab the smtp debugging output
		ob_get_clean();
		
		return $res_mail;	
	}
	
	/* Administrator Mail Function */
	public static function gfbAdminMailer( $subject, $message ) {
		
		$from_email = get_option('gfb_sender_email');
		$from_name = get_option('gfb_sender_name');
		$to = get_option('gfb_sender_email');		
		$footer = get_option('gfb_company_website');
		
		$template = file_get_contents( GFB_URL . '/includes/email-templates/appointment.html', true );
		
		$filter = array('%content%','%title%','%footer%');
		$replace = array(wpautop($message),$subject,$footer);
		
		if ( $from_email )
		{
			$headers[] = 'From: ' . ( $from_name ? $from_name . ' <' . $from_email . '>' : $from_email );
		}
		
		//$rep_message = str_replace($filter, $replace, $template);
		if ( ! empty( $template ) ) {
            $rep_message = str_replace($filter, $replace, $template);
        } else {
            $rep_message = wpautop($message);
        }
		
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		
		// Start output buffering to grab smtp debugging output
		ob_start();
		
		$res_mail = wp_mail($to, $subject, $rep_message, $headers);		
		
		// Grab the smtp debugging output
		ob_get_clean();
		
		return $res_mail;	
	}
	
	public static function gfbCreateRole() {
		add_role( 'gfb_staff_role', __( 'Staff' ), array( 'read' => true, ) );		
		add_role( 'gfb_customer_role', __( 'Customer' ), array( 'read' => true, ) );		
	
	}
	
	public static function gfbAllowPermissionRole() {
		remove_role('user');
		add_role( 'gfb_staff_role', __( 'Staff' ), array( 'read' => true, ) );		
		add_role( 'gfb_customer_role', __( 'Customer' ), array( 'read' => true, ) );
	}
	
	
	
}

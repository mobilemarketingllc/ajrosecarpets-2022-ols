<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; } 

class GFB_Register {

	public function __construct() {} 
	
	public static function gfbRegisterPlugin() {
		
		$current_user = get_current_user_id();	
		$site_url = get_option("siteurl");
		require_once( GFB_ADMIN_CLASS . 'gravityformbooking.menu.cls.php' );
		global $gfbmenu;
		
		/* Core Class */
		require_once( GFB_ADMIN_CORE . 'Core.php' );
		
		/* CUSTOM TABLES */			
		require_once( GFB_ADMIN_CLASS . 'gravityformbooking.master.cls.php' );
		require_once( GFB_INCLUDES_CLASS . 'gravityformbooking.frontend.cls.php' );
		
		/* Add new role */
		GFB_Core::gfbCreateRole();
		GFB_Core::gfbAllowPermissionRole();
		add_action('admin_menu', array( &$gfbmenu, 'gfbCreateMenu' ) );
		return true;
	}	
	
}

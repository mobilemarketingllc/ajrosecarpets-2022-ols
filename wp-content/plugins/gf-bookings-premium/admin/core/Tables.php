<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) { exit; } 

$charset_collate = '';
global $wpdb;

function gfb_table_column_exists( $table_name, $column_name ) {

	global $wpdb;

	$column = $wpdb->get_results( $wpdb->prepare(
		"SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s ",
		DB_NAME, $table_name, $column_name
	) );

	if ( ! empty( $column ) ) {
		return true;
	}

	return false;
}

if ( ! empty($wpdb->charset))
	$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
if ( ! empty($wpdb->collate))
	$charset_collate .= " COLLATE $wpdb->collate";

require_once ABSPATH . 'wp-admin/includes/upgrade.php';
	
/* Custom Tables for Gravity Forms Booking Plugin */

$gfb_appointments_mst = $wpdb->prefix . "gfb_appointments_mst";
$gfb_holiday_mst = $wpdb->prefix . "gfb_holiday_mst";
$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";
$gfb_services_mst = $wpdb->prefix . "gfb_services_mst";
$gfb_payments_mst = $wpdb->prefix . "gfb_payments_mst";
$gfb_staff_mst = $wpdb->prefix . "gfb_staff_mst";
$gfb_time_slot_mst = $wpdb->prefix . "gfb_time_slot_mst";
$gfb_staff_service_mapping = $wpdb->prefix . "gfb_staff_service_mapping";
$gfb_staff_holiday_mapping = $wpdb->prefix . "gfb_staff_holiday_mapping";
$gfb_staff_schedule_mapping = $wpdb->prefix . "gfb_staff_schedule_mapping";
$gfb_email_configuration = $wpdb->prefix . "gfb_email_configuration";
$gfb_staff_gcal_watch = $wpdb->prefix . "gfb_staff_gcal_watch";
$gfb_gcal_push_req = $wpdb->prefix . "gfb_gcal_push_req";
$gfb_staff_gcal_events = $wpdb->prefix . "gfb_staff_gcal_events";


/* Staff Google calendar events*/

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_staff_gcal_events'") != $gfb_staff_gcal_events) {
	$gfb_staff_gcal_events = "CREATE TABLE $gfb_staff_gcal_events (
		gcalid int(11) NOT NULL AUTO_INCREMENT,
		eventid varchar(255) NOT NULL,	
		summary varchar(255) NOT NULL ,
		start datetime NOT NULL ,
		end datetime NOT NULL ,
		status varchar(255) NOT NULL ,
		htmllink text NOT NULL ,
		timezone varchar(255) ,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (gcalid)
	) $charset_collate;";
	
	dbDelta($gfb_staff_gcal_events);
}

/* Staff Google calendar watch  Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_gcal_push_req'") != $gfb_gcal_push_req) 
{
	$gfb_gcal_push_req = "CREATE TABLE $gfb_gcal_push_req (
		gcal_push_reqid int(11) NOT NULL AUTO_INCREMENT,
		resource_id varchar(255) NOT NULL,	
		used_sync_token varchar(255) NOT NULL ,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (gcal_push_reqid)
	) $charset_collate;";
	
	dbDelta($gfb_gcal_push_req);
}

/* Staff Google calendar watch  Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_staff_gcal_watch'") != $gfb_staff_gcal_watch) 
{
	$gfb_staff_gcal_watch = "CREATE TABLE $gfb_staff_gcal_watch (
		staff_resource_id int(11) NOT NULL AUTO_INCREMENT,
		staff_id int(11) NOT NULL,	
		resource_id varchar(255) NOT NULL,	
		next_sync_token varchar(255) NOT NULL ,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (staff_resource_id)
	) $charset_collate;";
	
	dbDelta($gfb_staff_gcal_watch);
}

/* Appointment Table */

if ( $wpdb->get_var("SHOW TABLES LIKE '$gfb_appointments_mst'") != $gfb_appointments_mst ) {
	$appointment_tbl_code = "CREATE TABLE $gfb_appointments_mst (
		appointment_id int(11) NOT NULL AUTO_INCREMENT,
		customer_id varchar(255) NULL,
		appointment_date date NULL,
		staff_slot_mapping_id int(11) NULL,
		service_id int(11) NULL,
		staff_id int(11) NULL,
		booking_ref_no varchar(255) NULL,
		google_event_id varchar(255) NULL,	
		status tinyint(1) NOT NULL DEFAULT '1',	
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (appointment_id)
	) $charset_collate;";
	
	dbDelta($appointment_tbl_code);
} else {	

	$colCheck = gfb_table_column_exists( $gfb_appointments_mst, 'slot_count');
	if ( ! $colCheck ) {
		$appointment_tbl_alter_code = "ALTER TABLE $gfb_appointments_mst ADD slot_count int(11) NOT NULL DEFAULT 1";	
		$wpdb->query( $appointment_tbl_alter_code );		
	}
}

/* Holiday Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_holiday_mst'") != $gfb_holiday_mst) 
{
	$holiday_tbl_code = "CREATE TABLE $gfb_holiday_mst (
		holiday_id int(11) NOT NULL AUTO_INCREMENT,
		holiday_date date NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (holiday_id)
	) $charset_collate;";
	
	dbDelta($holiday_tbl_code);
}

/* Customer Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_customer_mst'") != $gfb_customer_mst) 
{
	$customer_tbl_code = "CREATE TABLE $gfb_customer_mst (
		customer_id int(11) NOT NULL AUTO_INCREMENT,
		customer_name varchar(255) NULL,
		customer_email varchar(150) NULL,
		customer_contact varchar(15) NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (customer_id)
	) $charset_collate;";
	
	dbDelta($customer_tbl_code);
}


/* Service Category Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_categories_mst'") != $gfb_categories_mst) 
{
	$categories_tbl_code = "CREATE TABLE $gfb_categories_mst (
		category_id int(11) NOT NULL AUTO_INCREMENT,
		category_name varchar(255) NOT NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (category_id)
	) $charset_collate;";
	
	dbDelta($categories_tbl_code);
}

/* Service Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_services_mst'") != $gfb_services_mst) 
{
	$services_tbl_code = "CREATE TABLE $gfb_services_mst (
		service_id int(11) NOT NULL AUTO_INCREMENT,
		category_id int(11) NULL,
		service_title varchar(255) NULL,
		service_price varchar(255) NULL,
		service_color varchar(255) NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (service_id)
	) $charset_collate;";
	
	dbDelta($services_tbl_code);
}

/* Payment Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_payments_mst'") != $gfb_payments_mst) 
{
	$payment_tbl_code = "CREATE TABLE $gfb_payments_mst (
		payment_id int(11) NOT NULL AUTO_INCREMENT,
		appointment_id int(11) NULL,
		entry_id int(11) NULL,
		transaction_id varchar(255) NULL,
		payment_date datetime NOT NULL,
		payment_type tinyint(1) NOT NULL DEFAULT '1',	
		total_amt decimal(10,2) NULL,
		paid_amt decimal(10,2) NULL,
		payment_details text NULL,	
		payment_status tinyint(1) NOT NULL DEFAULT '0',	
		currency_code varchar(10) NULL,	
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (payment_id)
	) $charset_collate;";
	
	dbDelta($payment_tbl_code);
}

/* Staff Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_staff_mst'") != $gfb_staff_mst) 
{
	$staff_tbl_code = "CREATE TABLE $gfb_staff_mst (
		staff_id int(11) NOT NULL AUTO_INCREMENT,
		staff_name varchar(255) NOT NULL,
		staff_designation varchar(255) NULL,
		staff_email varchar(255) NULL,
		staff_phone varchar(15) NULL,
		staff_info text NULL,
		staff_pic text NULL,
		user_id int(11) NULL,
		staff_gcal_data text NULL,
		gcal_channel_id  varchar(255) NULL,
		gcal_resource_id varchar(255) NULL,
		gcal_next_sync_token varchar(255) NULL,
		staff_gcal_watch_data text NOT NULL,
		staff_gcal_id varchar(255) NOT NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (staff_id)
	) $charset_collate;";
	
	dbDelta($staff_tbl_code);
}

/* Time Slot Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_time_slot_mst'") != $gfb_time_slot_mst) {
	$timeslot_tbl_code = "CREATE TABLE $gfb_time_slot_mst (
		time_slot_id int(11) NOT NULL AUTO_INCREMENT,
		time_slot_name varchar(255) NULL,
		weekday tinyint(1) NULL ,
		slot_start_time time NULL,
		slot_end_time time NULL,
		max_appointment_capacity int(11) NULL,
		is_custom_slot tinyint(1) NOT NULL DEFAULT '0',
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NULL,
		fullday tinyint(2) NOT NULL DEFAULT '0',
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (time_slot_id)
	) $charset_collate;";
	
	dbDelta($timeslot_tbl_code);
} else {	

	$colCheck = gfb_table_column_exists( $gfb_time_slot_mst, 'fullday');

	// echo 'colcheck is ' . $colCheck;
	// die();
	if ( ! $colCheck ) {
		$time_slot_tbl_alter_code = "ALTER TABLE $gfb_time_slot_mst ADD fullday tinyint(1) NOT NULL DEFAULT 0";	
		$wpdb->query( $time_slot_tbl_alter_code );		
	}

}

/* Staff services table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_staff_service_mapping'") != $gfb_staff_service_mapping) 
{
	$staff_service_tbl_code = "CREATE TABLE $gfb_staff_service_mapping (
		staff_service_mapping_id int(11) NOT NULL AUTO_INCREMENT,
		staff_id int(11) NULL ,
		service_id int(11) NULL ,
		service_price varchar(255) NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (staff_service_mapping_id)
	) $charset_collate;";
	
	dbDelta($staff_service_tbl_code);
}

/* Staff holiday table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_staff_holiday_mapping'") != $gfb_staff_holiday_mapping) 
{
	$staff_holiday_tbl_code = "CREATE TABLE $gfb_staff_holiday_mapping (
		staff_holiday_mapping_id int(11) NOT NULL AUTO_INCREMENT,
		staff_id int(11) NULL ,
		holiday_date date NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (staff_holiday_mapping_id)
	) $charset_collate;";
	
	dbDelta($staff_holiday_tbl_code);
}

/* Staff scheduling table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_staff_schedule_mapping'") != $gfb_staff_schedule_mapping) 
{
	$staff_holiday_tbl_code = "CREATE TABLE $gfb_staff_schedule_mapping (
		staff_slot_mapping_id int(11) NOT NULL AUTO_INCREMENT,
		staff_id int(11) NULL ,
		time_slot_id int(11) NULL ,
		max_appointment_capacity int(11) NULL,
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (staff_slot_mapping_id)
	) $charset_collate;";
	
	dbDelta($staff_holiday_tbl_code);
}

/* Email Configuration Table */

if($wpdb->get_var("SHOW TABLES LIKE '$gfb_email_configuration'") != $gfb_email_configuration) 
{
	$email_tbl_code = "CREATE TABLE $gfb_email_configuration (
		email_config_id int(11) NOT NULL AUTO_INCREMENT,
		label varchar(255) NULL,
		title varchar(255) NULL,
		subject varchar(255) NULL,
		message text NULL,	
		created_by int(11) NOT NULL,
		created_date datetime NOT NULL,
		modified_by int(11) NULL,
		modified_date datetime NULL,
		ip_address varchar(100) NOT NULL,
		is_deleted tinyint(1) NOT NULL DEFAULT '0',
		PRIMARY KEY  (email_config_id)
	) $charset_collate;";
	
	dbDelta($email_tbl_code);
}

/* CHECK CURRENCY CODE */
if($wpdb->get_var("SHOW TABLES LIKE '$gfb_payments_mst'") == $gfb_payments_mst) 
{
	$dbname = $wpdb->dbname; 
	$column = $wpdb->get_results( $wpdb->prepare("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s ",$dbname, $gfb_payments_mst, "currency_code") );
	
	if ( ! empty( $column ) ) {
		return true;
	}
	else {
		 $wpdb->query("ALTER TABLE ".$gfb_payments_mst." ADD COLUMN currency_code VARCHAR(10) NULL");	
	}
}
<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	/* Get Category List */
	global $gfbCategoryObj; 
	$categories = $gfbCategoryObj->gfbListCategories();
	
	/* Get Customer List */
	global $gfbCustomerObj;
	$customers = $gfbCustomerObj->gfbCustomerNameList();
	
?> 
<div class="popup-block-main">

    <div class="popup-block-main-title">
        <?php _e('Book Appointment (Collect Cash At Time Of Visit)', 'gfb'); ?>
        
    </div>
    
    <div class="popup-block-main-body">
    
    	<div class="gfbAjaxLoader" id="gfb_loader_img">
            <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
        </div>
    
    	<form name="appointment_form" class="appointment-form" id="appointment_form" method="post">
        
            <div class="form-section">
            
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="date"><?php _e('Appointment Date', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" id="appointment_date" class="appointment-date"  autocomplete="off"  name="appointment_date" />
                    </div> 
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="category"><?php _e('Category', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <select class="" id="appointmentcategory" name="appointmentcategory">
                            <option value=""><?php _e('--Select Category--', 'gfb'); ?></option>
                            
                            <?php foreach($categories as $category) { ?>
                            
                                <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
                           
                           <?php } ?>
                        </select>
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="service"><?php _e('Service', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <select class="" id="service" name="service">
                            <option value=""><?php _e('--Select Service--', 'gfb'); ?></option>
                        </select>
                    </div>
                
                </div>
                
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="provider"><?php _e('Provider', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <select class="" id="staff" name="staff">
                            <option value=""><?php _e('--Select Provider--', 'gfb'); ?></option>
                            
                        </select>
                    </div> 
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="timeslot"><?php _e('Time Slot', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <select id="time_slot" name="time_slot">
                            <option value=""><?php _e('--Select Time Slot--', 'gfb'); ?></option>
                        </select>
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="customer"><?php _e('New Customers?', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                    	<select class="" id="new-customer" name="new-customer">                           
                            <option value="2"><?php _e('No', 'gfb'); ?></option>
                            <option value="1"><?php _e('Yes', 'gfb'); ?></option>
                        </select>
                    </div>
                   </div>
                   <div class="form-group-elements">
                    <div class="form-element">
                        <div class="exist-customer-form">
                           <select class="" id="customer" name="customer">
                                <option value=""><?php _e('--Select Customer--', 'gfb'); ?></option>
                                <?php foreach($customers as $customer) { ?>
                                <option value="<?php if(!empty($customer['customer_id'])) echo $customer['customer_id']; ?>"><?php if(!empty($customer['customer_name'])) echo $customer['customer_name']; ?></option>
                               
                               <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="customer-form">
                    
                    	<div class="form-group-elements">
                
                            <div class="form-label">                
                                <label class="label-main" for="customer_name"><?php _e('Customer Name', 'gfb'); ?></label>
                            </div>
                            
                            <div class="form-element">
                                <input type="text" value="" class="notallowspecial" id="customer_name" maxlength="100" name="customer_name" placeholder="<?php _e("Please enter customer full name", "gfb"); ?>">
                            </div>
                        
                        </div>
                        
                        <div class="form-group-elements">
                        
                            <div class="form-label">                
                                <label class="label-main" for="customer_email"><?php _e('Customer Email', 'gfb'); ?></label>
                            </div>
                            
                            <div class="form-element">
                                <input type="text" value="" maxlength="100" id="customer_email" name="customer_email" placeholder="<?php _e("Please enter customer email address", "gfb"); ?>">
                            </div>
                        
                        </div>
                        
                        <div class="form-group-elements">
                        
                            <div class="form-label">                
                                <label class="label-main" for="customer_contact"><?php _e('Customer Contact', 'gfb'); ?></label>
                            </div>
                            
                            <div class="form-element">
                                <input type="text" value="" class="notallowspecialalpha" id="customer_contact" name="customer_contact" maxlength="15" placeholder="<?php _e("Please enter customer contact number", "gfb"); ?>">
                            </div>
                    
                    	</div>
                    
                    </div>
                
                </div>
                
                <div class="form-group-elements">
                    <div class="form-element">
                        <?php wp_nonce_field('appointment_nonce_field', 'appointment_nonce', true, true); ?>
                        <?php submit_button('Book Appointment'); ?>
                    </div>
                </div>
            </div>
            
        </form>
    
        
    </div>

</div> 
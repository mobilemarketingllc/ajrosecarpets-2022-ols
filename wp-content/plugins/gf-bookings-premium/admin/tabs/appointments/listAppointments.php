<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



//print_r($_GET);exit;
	
?>
<div class="">

	<div class="gfb_wrap">
    
    	<div class="gfb_maintab-title">
			<h2>
            <?php
				if( isset($_GET["status"]) && base64_decode($_GET["status"]) == '2' ){
					
					echo sprintf('Awaiting Appointments of %s', date( 'F j, Y', strtotime( base64_decode($_GET["appdate"])) ) );
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '1' ){
					
					echo sprintf('Pending Appointments of %s', date( 'F j, Y', strtotime( base64_decode($_GET["appdate"])) ) );
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '3' ){
					
					echo sprintf('Cancelled Appointments of %s', date( 'F j, Y', strtotime( base64_decode($_GET["appdate"])) ) );
				}
				elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '4' ){
					
					echo sprintf('Visited Appointments of %s', date( 'F j, Y', strtotime( base64_decode($_GET["appdate"])) ) );
				}
				else {
					_e('View All Appointments', 'gfb');
				}
				
			?>            
            </h2>
            
			<div class="gfb_maintab-options">
                <a href="#appointmentHtml" name="add_appointment" id="add_appointment" class="add-appointment-modal button button-secondary"><?php _e('+ New Appointment', 'gfb'); ?></a>
            </div>
		</div>        
        
        <div id="appointmentHtml" class="popup-modal mfp-hide white-popup">
            <?php require_once( GFB_ADMIN_TAB . 'appointments/appointment-form.php' );  ?>
        </div>
        
                
        <div class="gfb_maintab-content-search filter-box">
        	
			<?php
			/* SERVICE FILTER */
			global $gfbServiceObj;
			$servicename = $gfbServiceObj->gfbServiceNameList();			
			?>
            <select name="service-filter" class="gfb-filter-service chosen-select">
                <option value=""><?php _e("Service Names", "gfb"); ?></option>
                <?php
                foreach( $servicename as $service ){
                    $selected = '';
                    if( base64_decode($_GET['service-filter']) == $service['service_id'] ){
                        $selected = ' selected = "selected"';   
                    }                    
                ?>
                <option value="<?php echo base64_encode($service['service_id']); ?>" <?php echo $selected; ?>><?php echo $service['service_title']; ?></option>
                <?php  
                }
                ?>
            </select>
                
			<?php 
			/* STAFF FILTER */
			global $gfbStaff;
			global $current_user;
			
			if ( $current_user->has_cap( 'administrator' ) ) {
				
				$staffname = $gfbStaff->getStaffNameList();			
			?>
				
				<select name="service-filter" class="gfb-filter-staff chosen-select" style="width: 200px;">
					<option value=""><?php _e("Staff Names", "gfb"); ?></option>
					<?php
					foreach( $staffname as $staff ){
						$selected = '';
						if( base64_decode($_GET['staff-filter']) == $staff['staff_id'] ){
							$selected = ' selected = "selected"';   
						}                   
					?>
					<option value="<?php echo base64_encode($staff['staff_id']); ?>" <?php echo $selected; ?>><?php echo $staff['staff_name']; ?></option>
					<?php  
					}
					?>
				</select>
            
            <?php 
			} 
			?>
            
            <!-- Appointment Status -->			
            <select name="status-filter" class="gfb-filter-status chosen-select">
                <option value=""><?php _e("Status", "gfb"); ?></option>               
                <option value="<?php echo base64_encode(1); ?>" <?php if( isset($_GET['status-filter']) && base64_decode($_GET['status-filter']) == 1 ){ ?> selected="selected" <?php } ?> ><?php _e("Pending", "gfb"); ?></option>                
                <option value="<?php echo base64_encode(2); ?>" <?php if( isset($_GET['status-filter']) && base64_decode($_GET['status-filter']) == 2 ){ ?> selected="selected" <?php } ?> ><?php _e("Awaiting", "gfb"); ?></option>                
                <option value="<?php echo base64_encode(3); ?>" <?php if( isset($_GET['status-filter']) && base64_decode($_GET['status-filter']) == 3 ){ ?> selected="selected" <?php } ?> ><?php _e("Cancelled", "gfb"); ?></option>                
                <option value="<?php echo base64_encode(4); ?>" <?php if( isset($_GET['status-filter']) && base64_decode($_GET['status-filter']) == 4 ){ ?> selected="selected" <?php } ?> ><?php _e("Visited", "gfb"); ?></option>
            </select>				
					
            <!-- Customer Filter -->
        	<?php
			/* CUSTOMER FILTER */
			global $gfbCustomerObj;
			$customername = $gfbCustomerObj->gfbCustomerNameList();			
			?>
            <select name="customer-filter" class="gfb-filter-customer chosen-select">
                <option value=""><?php _e("Customer Names", "gfb"); ?></option>
                <?php
                foreach( $customername as $customer ){
                    $selected = '';
					if( base64_decode($_GET['customer-filter']) == $customer['customer_id'] ){
						$selected = ' selected = "selected"';   
					}
                ?>
                <option value="<?php echo base64_encode($customer['customer_id']); ?>" <?php echo $selected; ?>><?php echo $customer['customer_name']; ?></option>
                <?php  
                }
                ?>
            </select>
            
            <!-- Export To CSV -->        	
            <form name="export_appointment" id="export_appointment" method="get"> 
                <input type="hidden" name="status" id="status" value="<?php if( isset($_GET["status"]) ) { echo $_GET["status"]; } ?>" />
                <input type="hidden" name="appdate" id="appdate" value="<?php if( isset($_GET["appdate"]) ) { echo $_GET["appdate"]; } ?>" />
                <button type="submit" name="gfb_appointment_export_to_csv" id="gfb_appointment_export_to_csv" class="gfb_export_csv" value="gfb_export_csv"><span class="dashicons dashicons-download"></span><?php _e('CSV', 'gfb'); ?></button>
                
                <button type="submit" name="gfb_appointment_export_to_pdf" id="gfb_appointment_export_to_pdf" class="gfb_export_pdf" value="gfb_export_pdf"><span class="dashicons dashicons-download"></span><?php _e('PDF', 'gfb'); ?></button>            	            
            </form>
            <br />
            
            <!-- Appointment Date Filter -->					
            <form name="gfb_appointment_date_form" id="gfb_appointment_date_form">                
            	<div class="">                
                    <input type="text" name="gfb_appointment_start_date" id="gfb_appointment_start_date" class="form-control appointment_datepicker" value="<?php if( isset($_GET['appointment-stdt-filter']) ) { echo $_GET['appointment-stdt-filter']; } ?>" placeholder="<?php _e("Appointment Start Date", "gfb"); ?>" autocomplete="off" readonly="readonly" />
                    
                    &nbsp; - &nbsp;
                    
                    <input type="text" name="gfb_appointment_end_date" id="gfb_appointment_end_date" class="form-control appointment_datepicker" value="<?php if( isset($_GET['appointment-enddt-filter']) ) { echo $_GET['appointment-enddt-filter']; } ?>" placeholder="<?php _e("Appointment End Date", "gfb"); ?>" autocomplete="off" readonly="readonly" />
                    &nbsp;
                    
                    <button class="gfb_search" type="button" name="gfb_appointment_date_submit" id="gfb_appointment_date_submit"><span class="dashicons dashicons-search"></span></button>
                </div>
            </form>
            <form name="gfb_appointment_bookingnum_form" id="gfb_appointment_bookingnum_form">                
            	<div class="">                
                    <input type="text" name="gfb_appointment_booking_ref_num" id="gfb_appointment_booking_ref_num" class="form-control" value="<?php if(isset($_GET['appointment-bookingnum-filter']) ) { echo $_GET['appointment-bookingnum-filter']; } ?>" placeholder="<?php _e("Booking Number", "gfb"); ?>" autocomplete="off" />
                      
                    <button class="gfb_search" type="submit" name="gfb_appointment_bookingnum_submit" id="gfb_appointment_bookingnum_submit"><span class="dashicons dashicons-search"></span></button>
                </div>
            </form>
            
        </div>
        
        <!-- Appointment List -->
		<div class="gfb_maintab-content"> 
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
			
			<form method="get" name="appointment_list_form" id="appointment_list_form">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                
                <?php                 	
					
					if( isset($_GET["status"]) && base64_decode($_GET["status"]) == '2' ) { 	
						//die( 'stop 2!' );
						require_once( GFB_ADMIN_TAB . 'appointments/awaitingAppointmentList.php');
                        $appointment_data = new Awaiting_Appointment_List_Tbl;
					}elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '1' ) { 
						//die( 'stop 1!' );
						require_once( GFB_ADMIN_TAB . 'appointments/pendingAppointmentList.php');
						$appointment_data = new Pending_Appointment_List_Tbl;
					}
					elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '3' ) { 
						//die( 'stop 3!' );	
						require_once( GFB_ADMIN_TAB . 'appointments/cancelledAppointmentList.php');
						$appointment_data = new Cancelled_Appointment_List_Tbl;
					}
					elseif( isset($_GET["status"]) && base64_decode($_GET["status"]) == '4' ) { 
						//die( 'stop 4!' );		
						require_once( GFB_ADMIN_TAB . 'appointments/visitedAppointmentList.php');
                        $appointment_data = new Visited_Appointment_List_Tbl;
					}
					else { 
						//die( 'stop Only!' );
						require_once( GFB_ADMIN_TAB . 'appointments/appointmentList.php');
						$appointment_data = new Appointment_List_Tbl;
					}
					
					$appointment_data->prepare_items();
					$appointment_data->display(); 
				?>
            </form>

		</div>

	</div>

</div><!-- /wrap -->
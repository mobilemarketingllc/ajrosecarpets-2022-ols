<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



global $current_user;
global $gfbStaff;

?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e("Calendar", "gfb"); ?></h2>
			<div class="gfb_maintab-options">
				<span style="color: #fff;"><?php echo current_time('g:i a'); ?> &bull; <?php echo current_time('F j, Y'); ?></span>
			</div>
		</div>        
        
        <div class="gfb_maintab-content" style="text-align:center;"> 

			<?php 			
			if ( $current_user->has_cap( 'administrator' ) ): 
			    /* get all staff list */     
				$staffList = $gfbStaff->getStaffNameList();
				
            ?>
                <select name="get_staff" id="get_staff" class="chosen-select" style="width:300px;"> 
                    <option value=""><?php _e("Select Staff", "gfb"); ?>
                    
                    <?php foreach($staffList as $staff) { ?>
                        <option value="<?php echo esc_attr($staff['staff_id']); ?>"><?php echo esc_attr($staff['staff_name']); ?></option>
                    <?php } ?>
                </select>
            
            <?php 			
			elseif ( $current_user->has_cap( 'gfb_staff_role' ) ): 
			
            	/* get staff name by user id */  
				$user_id = get_current_user_id();   
				$staffList = $gfbStaff->getStaffNameById($user_id);
			?>
            
            	<input type="hidden" name="get_staff" id="get_staff" class="input-main" style="width:300px;" value="<?php echo esc_attr($staffList[0]['staff_id']); ?>" /> 
            
            <?php endif; ?>
            

		</div><br />  
                     

		<div class="gfb_maintab-content">

			<div style="text-align: center; margin-bottom: -28px;">
				<span class="event-pending"></span> <?php _e("Pending", "gfb"); ?>
				<span class="event-awaiting"></span> <?php _e("Awaiting", "gfb"); ?>
				<span class="event-completed"></span> <?php _e("Completed", "gfb"); ?>
				<span class="event-cancelled"></span> <?php _e("Cancelled", "gfb"); ?>
                <span class="event-holiday"></span> <?php _e("Holiday", "gfb"); ?>
			</div>
			<div id="calendar">
            	<div class="gfbAjaxLoader" id="gfb_loader_img">
                    <img class="gfb-ajax-loader-holiday" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>
            </div>

		</div>
        
        

	</div>

</div><!-- /wrap -->
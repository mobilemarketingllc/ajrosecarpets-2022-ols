<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?> 
<div class="popup-block-main">

    <div class="popup-block-main-title"><?php _e('Add Category', 'gfb'); ?></div>
    
    <div class="popup-block-main-body">
    
        <form name="category_form" class="category-form" id="category_form" method="post"> 
        
            <div class="form-section">
            
            	<div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="category_name"><?php _e('Category', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="category_name" id="category_name" class="input-main notallowspecial" maxlength="100" />
               		</div>
                
                </div>
                
                <div class="form-group-elements">
                    <div class="form-element">
                        <?php wp_nonce_field('category_nonce_field', 'category_nonce', true, true); ?>
                        <?php submit_button('Save Category'); ?>
                    </div>                
                </div>                
            </div>
            
        </form>
    
    </div>

</div>  
<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Categories', 'gfb'); ?></h2>
            
			<div class="gfb_maintab-options">
				<a href="#categoryHtml" name="add_category" id="add_category" class="add-category-modal button button-secondary"><?php _e('+ New Category', 'gfb'); ?></a>
			</div>
            
            <div id="categoryHtml" class="popup-modal mfp-hide white-popup">
            	<div class="gfbAjaxLoader" id="gfb_loader_img">
                    <img class="gfb-ajax-loader-holiday" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>
                
                <?php require_once( GFB_ADMIN_TAB . 'categories/addCategory.php' );  ?>
            </div>
            
            <div class="popup-modal mfp-hide" id="editCategoryHtml">
            
            	<div class="gfbAjaxLoader" id="gfb_loader_img">
                    <img class="gfb-ajax-loader-holiday" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>
            
                <div class="popup-block-main">
                    <div class="popup-block-main-title"><?php _e('Edit Category', 'gfb'); ?></div>
                </div>
                
                <div class="popup-block-main-body">
                    <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>               

            </div>
		</div>
        
        <div class="gfb_maintab-content-serach filter-box">
        
        	<!-- Category Filter -->
            <form name="gfb_category_name_filter" id="gfb_category_name_filter">
                <div class="search-box">
                    <input type="text" name="gfb_category_nm_filter" id="gfb_category_nm_filter" class="form-control" placeholder="Category Name" value="<?php if( isset($_GET['categorynm-filter']) ) { echo $_GET['categorynm-filter']; } ?>" />
                    
                    <button class="gfb_search" type="button" name="gfb_category_nm_submit" id="gfb_category_nm_submit"><span class="dashicons dashicons-search"></span></button>
            	</div>
            </form>
            
        	<!-- Export to CSV -->
            <form name="export_category" id="export_category" method="get"> 
            
            	<button type="submit" name="gfb_category_export_to_csv" id="gfb_category_export_to_csv" class="gfb_export_csv" value="gfb_export_csv"><span class="dashicons dashicons-download"></span><?php _e('CSV', 'gfb'); ?></button>
                
                <button type="submit" name="gfb_category_export_to_pdf" id="gfb_category_export_to_pdf" class="gfb_export_pdf" value="gfb_export_pdf"><span class="dashicons dashicons-download"></span><?php _e('PDF', 'gfb'); ?></button>
            </form>
        </div>

		<div class="gfb_maintab-content">
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader-holiday" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
			
			<form method="post" name="category_list_form" id="category_list_form">
            
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php 
					require_once( GFB_ADMIN_TAB . 'categories/categoryListClass.php');
					$category_data = new Category_List_Tbl;
					$category_data->prepare_items();
					$category_data->display(); 
				?>
            </form>

		</div>

	</div>

</div><!-- /wrap -->
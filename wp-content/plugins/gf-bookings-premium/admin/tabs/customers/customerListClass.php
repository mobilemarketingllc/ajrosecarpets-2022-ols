<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	
	class Customer_List_Tbl extends WP_List_Table {
		
		function __construct()
		{
			global $status, $page;
					
			//Set parent defaults
			parent::__construct( array(
				'singular'  => 'customer',     //singular name of the listed records
				'plural'    => 'customers',    //plural name of the listed records
				'ajax'      => false
			) );		
		}
		
		function get_columns()
		{
			$columnNames = array(
				'cb' => '<input type="checkbox" />',
				'customer_name' => 'Customer Name',
				'customer_email' => 'Customer Email',
				'customer_contact' => 'Customer Contact No',
				'edit' => 'Edit',
				'delete' => 'Delete'				
			);
			return $columnNames;	
		}	
		
		function column_default( $item, $column_name ) 
		{
			switch( $column_name ) 
			{ 				
				case 'customer_name':
				case 'customer_email':
				case 'customer_contact':
					return $item[ $column_name ];
				default:
					return print_r( $item, true ) ; 
			}
		}
		
		function column_cb($item)
		{
			return sprintf( '<input type="checkbox" name="customer_id[]" value="%s">',$item['customer_id']);
		}
		
		function column_delete($item)
		{
			if( isset($_REQUEST['paged']) ) {
				$paged = $_REQUEST['paged'];	
			}
			else {
				$paged = 1;
			}
			
			$actions = array(
				'delete' => sprintf('<a data-customer_id="%s" href="#" class="gfb_grid-btn btn-red gfb_delete_customer"><span class="dashicons dashicons-no"></span></a>', $item['customer_id']),
			);
	
			return $this->row_actions($actions, true);
		}
		
		function column_edit($item)
		{
			$actions = array(
				'edit' => sprintf('<a data-customer_id="%s" href="#editCustomerHtml" name="edit_customer" id="edit_customer" class="gfb_grid-btn btn-blue edit-customer-modal"><span class="dashicons dashicons-edit"></span></a>', $item['customer_id']),
			);
			
			return sprintf('%1$s', $this->row_actions($actions, true) );
		}
		
		function get_bulk_actions() {}
		
		function process_bulk_action() {}
				
		function get_sortable_columns() {}
		
		function prepare_items() 
		{
			global $wpdb;
			$perPage = 10;
			$currentPage = $this->get_pagenum();
			
			$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
	
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			$this->process_bulk_action();
			
			if( isset($_GET['custnm-filter']) && $_GET['custnm-filter'] <> '' ) 
			{				
				$filter = " AND customer_name LIKE '".$_GET['custnm-filter']."%'";			
			}
			elseif( isset($_GET['custemail-filter']) && $_GET['custemail-filter'] <> '' ) 
			{
				$filter = " AND customer_email LIKE '".$_GET['custemail-filter']."%'";			
			}
			elseif( isset($_GET['custcontact-filter']) && $_GET['custcontact-filter'] <> '' ) 
			{
				$filter = " AND customer_contact LIKE '".$_GET['custcontact-filter']."%'";			
			}
			else {				
				$filter = '';
			}
			
			$orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($sortable))) ? $_REQUEST['orderby'] : 'customer_name';
			
			$order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';
			
			$customer_results = $wpdb->get_results("SELECT customer_id, customer_name, customer_contact, customer_email FROM ".$gfb_customer_mst." WHERE is_deleted=0 ".$filter." ORDER BY ".$orderby." ".$order."", ARRAY_A);
			
			$totalItems = count($customer_results);
		
			$customer_rows = array_slice($customer_results,(($currentPage-1)*$perPage),$perPage);
			
			$this->set_pagination_args(array(
				'total_items' => $totalItems, // total items defined above
				'per_page' => $perPage, // per page constant defined at top of method
				'total_pages' => ceil($totalItems / $perPage) // calculate pages count
			));
			
			$this->items = $customer_rows;
		}
	}

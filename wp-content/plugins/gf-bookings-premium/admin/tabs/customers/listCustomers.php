<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

	
?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Customers', 'gfb'); ?></h2>
		</div>
        
        <div class="popup-modal mfp-hide" id="editCustomerHtml">
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
                
            <div class="popup-block-main">
                <div class="popup-block-main-title"><?php _e('Edit Customer', 'gfb'); ?></div>
            </div>
            
            <div class="popup-block-main-body">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>                 

        </div>
        
        <div class="gfb_maintab-content-search filter-box">
        
        	<!-- Customer Name Filter -->
            <form name="gfb_customer_nm_form" id="gfb_customer_nm_form">  
            	<div class="search-box">                      
                    <input class="form-control" type="text" name="gfb_customer_nm" id="gfb_customer_nm" value="<?php if( isset($_GET['custnm-filter']) ) { echo $_GET['custnm-filter']; } ?>" placeholder="<?php _e("Customer Name", "gfb"); ?>" />                
                    <button class="gfb_search" type="button" name="gfb_customer_nm_submit" id="gfb_customer_nm_submit"><span class="dashicons dashicons-search"></span></button>
                </div>
            </form>
            
            <!-- Customer Email Filter -->
            <form name="gfb_customer_email_form" id="gfb_customer_email_form">   
            	<div class="search-box">                     
                    <input class="form-control" type="text" name="gfb_customer_email" id="gfb_customer_email" value="<?php if( isset($_GET['custemail-filter']) ) { echo $_GET['custemail-filter']; } ?>" placeholder="<?php _e("Customer Email", "gfb"); ?>" />                                
                    <button class="gfb_search" type="button" name="gfb_customer_email_submit" id="gfb_customer_email_submit"><span class="dashicons dashicons-search"></span></button>
                </div>
            </form>
                        
            <!-- Customer Contact Filter -->
            <form name="gfb_customer_contact_form" id="gfb_customer_contact_form">                        
                <div class="search-box">
                    <input class="form-control" type="text" name="gfb_customer_contact" id="gfb_customer_contact" value="<?php if( isset($_GET['custcontact-filter']) ) { echo $_GET['custcontact-filter']; } ?>" placeholder="<?php _e("Customer Contact No", "gfb"); ?>" />
                    <button class="gfb_search" type="button" name="gfb_customer_contact_submit" id="gfb_customer_contact_submit"><span class="dashicons dashicons-search"></span></button>
                </div>
            </form>
            
                    
        	<!-- Export To CSV -->
            <form name="export_customer" id="export_customer" method="get"> 
                
                <button type="submit" name="gfb_customer_export_to_csv" id="gfb_customer_export_to_csv" class="gfb_export_csv" value="gfb_export_csv"><span class="dashicons dashicons-download"></span><?php _e('CSV', 'gfb'); ?></button>
                
                <button type="submit" name="gfb_customer_export_to_pdf" id="gfb_customer_export_to_pdf" class="gfb_export_pdf" value="gfb_export_pdf"><span class="dashicons dashicons-download"></span><?php _e('PDF', 'gfb'); ?></button>           	            
            </form>
        </div>
        
        

		<div class="gfb_maintab-content">
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
			
			<form method="post" name="customer_list_form" id="customer_list_form">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php 
					require_once( GFB_ADMIN_TAB . 'customers/customerListClass.php');
					$customer_data = new Customer_List_Tbl;
					$customer_data->prepare_items();
					$customer_data->display(); 
				?>
            </form>

		</div>

	</div>

</div><!-- /wrap -->
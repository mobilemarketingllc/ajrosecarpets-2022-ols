<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


global $gfbEmail;
$notifyList = $gfbEmail->gfbGetNotifications();

?>	    	  
<form name="app_notification_form" id="app_notification_form" method="post"> 

	<div class="gfbAjaxLoader" id="gfb_loader_img">
        <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
    </div>
	
    <div id="accordion">

        <?php foreach( $notifyList as $notify ) { ?>
        
        	<h4 class="gfb_section-title"><?php echo esc_attr($notify["label"]); ?></h4>
            
            <div>
                <div class="form-section">
            
                    <div class="form-group-elements">
                        <input type="hidden" name="title_<?php echo esc_attr($notify["email_config_id"]); ?>" id="title_<?php echo esc_attr($notify["email_config_id"]); ?>" class="form-control" value="<?php echo esc_attr($notify["title"]); ?>" readonly="readonly" />
                    </div>
                    
                    <div class="form-group-elements">
                        <input type="hidden" name="email_config_id_<?php echo esc_attr($notify["email_config_id"]); ?>" id="email_config_id_<?php echo esc_attr($notify["email_config_id"]); ?>" class="form-control" value="<?php echo esc_attr(base64_encode($notify["email_config_id"])); ?>" readonly="readonly" />
                    </div>
                            
                    <div class="form-group-elements">
                        <label class=""><?php _e('Subject', 'gfb'); ?></label>
                        <input type="text" name="subject_<?php echo esc_attr($notify["email_config_id"]); ?>" id="subject_<?php echo esc_attr($notify["email_config_id"]); ?>" class="form-control" value="<?php echo esc_attr(stripslashes($notify["subject"])); ?>" />
                    </div>
                    
                    <div class="form-group-elements">
                        <label class=""><?php _e('Message', 'gfb'); ?></label>
                        <?php    
                            $content = wpautop( stripslashes( $notify["message"] ) );
                            $editor_id = 'message_'.esc_attr($notify["title"]); 
                            $name = 'message_'.esc_attr($notify["email_config_id"]); 
                            
                            $settings = array(
                                'textarea_name' => $name,
                                'media_buttons' => false,
                                'editor_height' => 384,
                            );
                                  
                            wp_editor( $content, $editor_id, $settings );                        
                        ?>
                    </div>
                
                </div>
            
            </div>
            
        <?php } ?>
    
    </div>
    
    <div class="form-group-elements">
    	<div class="form-element email-notify-submit">
        	<?php submit_button("Save Notifications"); ?>
        </div>
    </div>   
               
</form>
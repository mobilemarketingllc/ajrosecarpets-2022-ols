<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?>
<div class="wrap">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('DAYSOFF', 'gfb'); ?></h2>
            <div class="gfb_maintab-options">
				<span style="color: #fff;"><?php echo current_time('g:i a'); ?> &bull; <?php echo current_time('F j, Y'); ?></span>
            </div>
		</div>
        
        <div class="gfb_maintab-content-legends">
			<div class="holiday-legends">
				<div class="year-change-box">
                	<?php _e('Select year:', 'gfb'); ?> 
                </div>
                <div class="legend-box">
                	<?php _e('Legends: ', 'gfb'); ?>
                    <span class="event-pending"></span> <?php _e("DAYSOFF", "gfb"); ?>
                    <span class="event-completed" style="background: #eee;"></span> <?php _e("Working", "gfb"); ?>
                </div>
			</div>
		</div>
        
        
		<div class="gfb_maintab-content holidayCal" id="gfb-js-holidays-cal">		
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader-holiday" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>	
			<?php /*?><img class="gfb-ajax-loader-holiday" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            
            <div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div><?php */?>
		</div>
        
        

	</div>

</div><!-- /wrap -->
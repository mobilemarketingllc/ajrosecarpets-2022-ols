<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Payments', 'gfb'); ?></h2>
		</div>
        
        <div class="gfb_maintab-content filter-box">
        	
            <!-- Customer Filter -->
        	<?php
			/* CUSTOMER FILTER */
			global $gfbCustomerObj;
			$customername = $gfbCustomerObj->gfbCustomerNameList();			
			?>
            <select name="gfb_paynm" id="gfb_paynm" class="gfb-filter-customer-payment chosen-select">
                <option value="customer"><?php _e("Select Customer", "gfb"); ?></option>
                <option value="*"><?php _e("All", "gfb"); ?></option>
                <?php
                foreach( $customername as $customer ){
                    $selected = '';
                    if( isset($_GET['customer-filter']) == $customer['customer_id'] ){
                        $selected = ' selected = "selected"';   
                    }                    
                ?>
                <option value="<?php echo base64_encode($customer['customer_id']); ?>" <?php echo $selected; ?>><?php echo $customer['customer_name']; ?></option>
                <?php  
                }
                ?>
            </select>
            
            <!-- Transaction Id Filter -->
            <form name="gfb_paytrans_form" id="gfb_paytrans_form">
            	<!--<div class="search-box">                
                	<input type="text" name="gfb_paytrans" id="gfb_paytrans" class="form-control" value="<?php if( isset($_GET['paytrans-filter']) ) { echo $_GET['paytrans-filter']; } ?>" placeholder="<?php _e("Transaction Id", "gfb"); ?>" />                
               	<button class="gfb_search" type="button" name="gfb_paytrans_submit" id="gfb_paytrans_submit"><span class="dashicons dashicons-search"></span></button>
                </div>-->
            </form>
        
        	<!-- Export To Csv -->
            <form name="export_payment" id="export_payment" method="get"> 
                
                <button type="submit" name="gfb_payment_export_to_csv" id="gfb_payment_export_to_csv" class="gfb_export_csv" value="export_csv"><span class="dashicons dashicons-download"></span><?php _e('CSV', 'gfb'); ?></button>
                
                <button type="submit" name="gfb_payment_export_to_pdf" id="gfb_payment_export_to_pdf" class="gfb_export_pdf" value="gfb_export_pdf"><span class="dashicons dashicons-download"></span><?php _e('PDF', 'gfb'); ?></button>  	            
            </form>
        </div>

		<div class="gfb_maintab-content">
			
			<form method="post" name="payment_list_form" id="payment_list_form">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php 
					require_once( GFB_ADMIN_TAB . 'payment/paymentListClass.php');
					$payment_data = new Payment_List_Tbl;
					$payment_data->prepare_items();
					$payment_data->display(); 
				?>
            </form>

		</div>

	</div>

</div><!-- /wrap -->
<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



global $gfbCategoryObj; global $gfbServiceObj;
$categories = $gfbCategoryObj->gfbListCategories();

$serviceDetails = $gfbServiceObj->gfbServiceDetails($_POST['service_id']);

?>     
<form name="edit_service_form" class="service-form" id="edit_service_form" method="post"> 

    <div class="form-section">
    
        <div class="form-element">
            <input type="hidden" name="service_id" id="service_id" class="input-main" value="<?php echo esc_attr($serviceDetails[0]['service_id']); ?>" />
        </div>
    
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="category_id"><?php _e('Category', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <select name="category_id" id="category_id" class="input-main">                            	
                    <option value=""><?php _e("Select Category", "gfb"); ?></option>
                    <?php foreach( $categories as $category ) { ?>
                    
                        <option value="<?php echo $category['category_id']; ?>" <?php if( $serviceDetails[0]['category_id'] == $category['category_id'] ) { ?> selected="selected" <?php } ?>><?php echo $category['category_name']; ?></option>
                        
                    <?php } ?>
                </select>
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="service_title"><?php _e('Service Title', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="service_title" id="service_title" class="input-main notallowspecial" placeholder="Enter service name" value="<?php echo esc_attr($serviceDetails[0]['service_title']); ?>" maxlength="100" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="service_color"><?php _e('Service Color', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="service_color" id="edit_service_color" class="input-main edit_service_color_picker" value="#<?php if( !empty( $serviceDetails[0]['service_color']) ) { echo $serviceDetails[0]['service_color']; } else { echo '6d3bbf'; } ?>" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="service_price"><?php _e('Service Price ('.get_option('gfb_currency_symbol').')', 'gfb'); ?><p class="description"><?php _e('( in '.get_option('gfb_currency_symbol').' )', 'gfb'); ?></p></label>
            </div>
            
            <div class="form-element">                    	
                <input type="text" placeholder="Price" name="service_price" id="service_price" class="input-main notallowspecialalpha" value="<?php echo esc_attr($serviceDetails[0]['service_price']); ?>" maxlength="7" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
            <div class="form-element">
                <?php wp_nonce_field('edit_service_nonce_field', 'edit_service_nonce', true, true); ?>
                <?php submit_button('Update Service'); ?>
            </div>
        </div>
        
    </div>
    
</form>
<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

?>
<div class="tab-section">

    <div class="tab-title">
        <h4 class="gfb_section-title"><?php _e('Appointment Settings', 'gfb'); ?></h4>
    </div>
    
    <div class="tab-body"> 
        <form name="appointment_setting_form" id="appointment_setting_form" action="#" method="post">
            <input type="hidden" name="hdn_appointment_setting_form" value="update" />
            <div class="form-section">

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_time_interval"><?php _e('Start Day of Week: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <select name="gbf_start_of_week" id="gbf_start_of_week" class="input-main">
                             
                            <option value="sunday" <?php if( get_option('gbf_start_of_week') == 'sunday' ){ ?> selected="selected" <?php } ?> ><?php _e('Sunday', 'gfb'); ?></option>
                            
                            <option value="monday" <?php if( get_option('gbf_start_of_week') == 'monday' ){ ?> selected="selected" <?php } ?> ><?php _e('Monday', 'gfb'); ?></option>
                        </select>
                        <p class="description"><?php _e("This will set your calendar day starts with.", "gfb"); ?></p>
                    </div>
                
                </div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_time_duration"><?php _e('Time Duration : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <select name="gfb_time_duration" id="gfb_time_duration" class="input-main">
                            <?php
                            $durations = gfb_time_duration_opts();                            
                            if ( is_array( $durations ) && count( $durations ) > 0 ) {
                                foreach( $durations as $key => $duration ) {
                                    ?>
                                    <option value="<?php echo esc_attr( $key ); ?>" <?php selected( get_option('gfb_time_duration'), esc_attr( $key ), true ); ?> ><?php echo esc_html( $duration ); ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <p class="description"><?php _e("Each time slot duration.", "gfb"); ?></p>
                    </div>
    
                </div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_time_interval"><?php _e('Time Slot Interval : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <select name="gfb_time_interval" id="gfb_time_interval" class="input-main">
                            <?php
                            $intervals = gfb_time_interval_opts();                            
                            if ( is_array( $intervals ) && count( $intervals ) > 0 ) {
                                foreach( $intervals as $key => $interval ) {
                                    ?>
                                    <option value="<?php echo esc_attr( $key ); ?>" <?php selected( get_option('gfb_time_interval'), esc_attr( $key ), true ); ?> ><?php echo esc_html( $interval ); ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <p class="description"><?php _e("Gap between two time slots.", "gfb"); ?></p>
                    </div>
    
                </div>
        
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_prior_days_book_appointment"><?php _e('Prior Days to Book Appointment : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="gfb_prior_days_book_appointment" id="gfb_prior_days_book_appointment" class="input-main" placeholder="2" value="<?php echo get_option('gfb_prior_days_book_appointment'); ?>" maxlength="3" />
                        <p class="description"><?php _e("No. of days allowed from current day, after which appointments can be scheduled. Note: To allow current day booking from admin set this value to 0.", "gfb"); ?></p>
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_prior_months_book_appointment"><?php _e('Prior Months to Book Appointment : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="gfb_prior_months_book_appointment" id="gfb_prior_months_book_appointment" class="input-main" placeholder="2" value="<?php echo get_option('gfb_prior_months_book_appointment'); ?>" maxlength="3" />
                        <p class="description"><?php _e("No. of months allowed to schedule appointments. It cannot be 0.", "gfb"); ?></p>
                    </div>
                
                </div>

                <div class="form-group-elements">
                  <h4 class="gfb_section-title">
                    <?php _e('Full Day Booking', 'gfb'); ?>                    
                    <label class="toggleswitch">
                      <input type="checkbox" value="1" name="gfb_fullday_booking" id="gfb_fullday_booking" <?php if( get_option('gfb_fullday_booking') == 1 ) { ?> checked="checked" <?php } ?>>
                    </label>
                  </h4>
                </div>


                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_calendar_availability"><?php _e('Calendar Availability: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <select name="gfb_calendar_availability" id="gfb_calendar_availability" class="input-main">
                            <option value="service" <?php if( get_option('gfb_calendar_availability') == 'service' ){ ?> selected="selected" <?php } ?> ><?php _e('Service Based Appointment', 'gfb'); ?></option>
                            <option value="global" <?php if( get_option('gfb_calendar_availability') == 'global' ){ ?> selected="selected" <?php } ?> ><?php _e('Global Based Appointment', 'gfb'); ?></option>                            
                        </select>
                        <p class="description"><?php _e("1. Global based appointments will be independent of services and provider (Staff) <br/ > 2. Service based appointments will be dependent of services and providers (staff).", "gfb"); ?></p>
                    </div>
    
                </div>

                <div class="global_calendar_settings_tab" style="display:none;">

                    <h4 class="gfb_section-title"><?php _e("Global Settings", "gfb"); ?></h4>

                    <div class="form-group-elements">
                
                        <div class="form-label">                
                            <label class="label-main" for="gfb_global_service"><?php _e('Select Service: ', 'gfb'); ?></label>
                        </div>
                        
                        <div class="form-element">
                            <?php 
                            global $gfbStaff;
                            $staff_services = $gfbStaff->gfbStaffServiceList( @$staffDetail[0]['staff_id'] );
                            if( count($staff_services) > 0 ) {
                                ?>                       
                                <select name="gfb_global_service" id="gfb_global_service" class="input-main">
                                    <option value="">--Select Service--</option>
                                    <?php 
                                    foreach ( $staff_services as $key => $service ) {
                                        ?>
                                        <option value="<?php echo $service['service_id']; ?>" <?php selected( $service['service_id'], get_option( 'gfb_global_service' ), true ); ?> ><?php echo $service['service_title']; ?></option>
                                        <?php
                                    }
                                    ?>  
                                </select>
                                <?php
                            } else {
                                ?>
                                <p><?php _e('Please create service.', 'gfb'); ?></p>
                                <?php
                            }
                            ?>
                        </div>
        
                    </div>

                    <div class="form-group-elements">
                
                        <div class="form-label">                
                            <label class="label-main" for="gfb_global_service_provider"><?php _e('Select Staff: ', 'gfb'); ?></label>
                        </div>
                        
                        <div class="form-element">
                            <?php 
                            $staffList = $gfbStaff->getStaffNameList();
                            if( count($staffList) > 0 ) {
                                ?>                       
                                <select name="gfb_global_service_provider" id="gfb_global_service_provider" class="input-main">
                                    <option value="">--Select Staff--</option>
                                    <?php 
                                    foreach ( $staffList as $key => $staff ) {
                                        ?>
                                        <option value="<?php echo $staff['staff_id']; ?>" <?php selected( $staff['staff_id'], get_option( 'gfb_global_service_provider' ), true ); ?> ><?php echo $staff['staff_name']; ?></option>
                                        <?php
                                    }
                                    ?>  
                                </select>
                                <?php
                            } else {
                                ?>
                                <p><?php _e('Please create staff.', 'gfb'); ?></p>
                                <?php
                            }
                            ?>
                        </div>
        
                    </div>
                    
                </div>
                
                <div class="form-group-elements">                    
                    <div class="form-element">
                        <?php submit_button('Save Appointment Settings'); ?>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
   
</div> 
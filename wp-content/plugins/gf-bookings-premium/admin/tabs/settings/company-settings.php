<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?>
<div class="tab-section">

    <div class="tab-title">
        <h4 class="gfb_section-title"><?php _e('Company Settings', 'gfb'); ?></h4>
    </div>
    
    <div class="tab-body">  
        <form name="company_setting_form" id="company_setting_form" action="#" method="post">
            
            <div class="form-section">
        
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_company_name"><?php _e('Company Name : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="gfb_company_name" id="gfb_company_name" class="input-main notallowspecial" placeholder="Enter Company Name" value="<?php echo get_option('gfb_company_name'); ?>" maxlength="100" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_company_address"><?php _e('Company Address : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <textarea name="gfb_company_address" id="gfb_company_address" class="input-main" placeholder="Enter Company Address"><?php echo esc_attr(stripslashes(get_option('gfb_company_address'))); ?></textarea>
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_company_contact_no"><?php _e('Company Contact No : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="gfb_company_contact_no" id="gfb_company_contact_no" class="input-main notallowspecialalpha" placeholder="Enter Company's Contact No" value="<?php echo get_option('gfb_company_contact_no'); ?>" maxlength="15" />
                    </div>
                
                </div>
               
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_company_email"><?php _e('Company Email Id : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="email" name="gfb_company_email" id="gfb_company_email" class="input-main" placeholder="Enter Company Email Id" value="<?php echo get_option('gfb_company_email'); ?>" maxlength="150" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_company_website"><?php _e('Company Website Url : ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="gfb_company_website" id="gfb_company_website" class="input-main" placeholder="http://" value="<?php echo get_option('gfb_company_website'); ?>" maxlength="200" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
                    <div class="form-element">
                        <?php submit_button('Save Company Settings'); ?>
                    </div>
                </div> 
            </div>
        </form>
      
    </div>
   
</div> 
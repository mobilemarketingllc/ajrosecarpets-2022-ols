<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

?>
<div class="tab-section">

    <div class="tab-title">
        <h4 class="gfb_section-title"><?php _e('General Settings', 'gfb'); ?></h4>
    </div>
    
    <div class="tab-body"> 
        <form name="general_setting_form" id="general_setting_form" action="#" method="post">
            
            <div class="form-section">

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_time_interval"><?php _e('Price Policy: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <select name="gfb_price_policy" id="gfb_price_policy" class="input-main">
                        	<option value="staff" <?php if( get_option('gfb_price_policy') == 'staff' ){ ?> selected="selected" <?php } ?> ><?php _e('Staff', 'gfb'); ?></option>
                            <option value="admin" <?php if( get_option('gfb_price_policy') == 'admin' ){ ?> selected="selected" <?php } ?> ><?php _e('Admin', 'gfb'); ?></option>                            
                        </select>
                        <p class="description"><?php _e("Allow staff/admin to give rights to set the price for any service.", "gfb"); ?></p>
                    </div>
                
                </div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_time_duration"><?php _e('Appointment Policy: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <select name="gfb_appointment_policy" id="gfb_appointment_policy" class="input-main">
                        	<option value="staff" <?php if( get_option('gfb_appointment_policy') == 'staff' ){ ?> selected="selected" <?php } ?> ><?php _e('Staff', 'gfb'); ?></option>
                            <option value="admin" <?php if( get_option('gfb_appointment_policy') == 'admin' ){ ?> selected="selected" <?php } ?> ><?php _e('Admin', 'gfb'); ?></option>                            
                        </select>
                        <p class="description"><?php _e("Allow staff/admin to give rights to confirm appointments assigned to them..", "gfb"); ?></p>
                    </div>
    
                </div>
                
                <div class="form-group-elements">                    
                    <div class="form-element">
                        <?php submit_button('Save General Settings'); ?>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
</div>
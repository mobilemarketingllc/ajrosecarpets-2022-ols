<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?>

<div class="tab-section">
  <div class="tab-title">
    <h4 class="gfb_section-title">
      <?php _e('Google Calendar Settings', 'gfb'); ?>
    </h4>
  </div>
  <div class="tab-body">
    <form name="google_calendar_form" id="google_calendar_form" method="post">
      <div class="form-section">
        <div class="form-group-elements">
          <div class="form-label">
            <label class="label-main" for="gfb_client_id">
              <?php _e('Client ID', 'gfb'); ?>
            </label>
          </div>
          <div class="form-element">
            <input type="text" name="gfb_client_id" id="gfb_client_id" class="input-main" value="<?php echo get_option('gfb_client_id'); ?>" placeholder="Client ID" />
            <p class="description">
              <?php _e("The client ID obtained from the Developers Console", "gfb"); ?>
            </p>
          </div>
        </div>
        <div class="form-group-elements">
          <div class="form-label">
            <label class="label-main" for="gfb_client_secrete">
              <?php _e('Client Secret', 'gfb'); ?>
            </label>
          </div>
          <div class="form-element">
            <input type="text" name="gfb_client_secrete" id="gfb_client_secrete" class="input-main" value="<?php echo get_option('gfb_client_secrete'); ?>" placeholder="Client Secret" />
            <p class="description">
              <?php _e("The client secret obtained from the Developers Console", "gfb"); ?>
            </p>
          </div>
        </div>
        <div class="form-group-elements">
          <h4 class="gfb_section-title">
            <?php _e('2 way sync', 'gfb'); ?>
            <label class="toggleswitch">
              <input type="checkbox" value="1" name="gfb_gc_2way_sync" id="gfb_gc_2way_sync" <?php if( get_option('gfb_gc_2way_sync') == 1 ) { ?> checked="checked" <?php } ?>>
            </label>
          </h4>
        </div>
        <div class="form-group-elements">
          <div class="form-label">
            <label class="label-main" for="gfb_limit_gc_events">
              <?php _e('Limit number of fetched events', 'gfb'); ?>
            </label>
          </div>
          <div class="form-element">
            <select class="form-control" name="gfb_limit_gc_events" id="gfb_limit_gc_events">
              <option value="0" <?php if( get_option('gfb_limit_gc_events') == 0 ) { echo "selected"; } ?>>Disabled</option>
              <option value="25" <?php if( get_option('gfb_limit_gc_events') == 25 ) { echo "selected"; } ?>>25</option>
              <option value="50" <?php if( get_option('gfb_limit_gc_events') == 50 ) { echo "selected"; } ?>>50</option>
              <option value="100" <?php if( get_option('gfb_limit_gc_events') == 100 ) { echo "selected"; } ?>>100</option>
              <option value="250" <?php if( get_option('gfb_limit_gc_events') == 250 ) { echo "selected"; } ?>>250</option>
              <option value="500" <?php if( get_option('gfb_limit_gc_events') == 500 ) { echo "selected"; } ?>>500</option>
            </select>
            <p class="description">
              <?php _e("You can limit the number of fetched events here. This only works when 2 way sync is enabled.", "gfb"); ?>
            </p>
          </div>
        </div>
        <div class="form-group-elements">
          <div class="form-element">
            <?php submit_button('Save Calendar Settings'); ?>
          </div>
        </div>
      </div>
    </form>
    <div class="gfb-info">
      <h4>
        <?php _e("Instructions", "gfb"); ?>
      </h4>
      <p>
        <?php _e("To find your client ID and client secret, do the following:", "gfb"); ?>
      </p>
      <ol>
        <li>
          <?php _e("Go to the Google Developers Console.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Login to your gmail account.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Select a project, or create a new one.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Click in the upper left part to see a sliding sidebar. Next, click API Manager. In the list of APIs look for Calendar API and make sure it is enabled.", "gfb"); ?>
        </li>
        <li>
          <?php _e("In the sidebar on the left, select Credentials.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Go to OAuth consent screen tab and give a name to the product, then click Save.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Go to Credentials tab and in New credentials drop-down menu select OAuth client ID.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Select Web application and create your project's OAuth 2.0 credentials by providing the necessary information. For Authorized redirect URIs enter the Redirect URI '" . get_site_url() . "/wp-admin/admin.php?page=gravity-form-booking-staff'. Click Create.", "gfb"); ?>
        </li>
        <li><?php _e('If you want to enable add to my calendar functionality then you will need to insert another Authorized Redirect URI i.e. "' . get_site_url() . '/{url_where_form_placed}" ', 'gfb'); ?></li>
        <li>
          <?php _e("In the popup window look for the Client ID and Client secret. Use them in the form below on this page.", "gfb"); ?>
        </li>
        <li>
          <?php _e("Go to Staff Members, select a staff member and click Connect which is located at the bottom of the page.", "gfb"); ?>
        </li>
      </ol>
    </div>
  </div>
</div>

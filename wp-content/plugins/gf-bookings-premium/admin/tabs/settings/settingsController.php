<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


  
?>
<div class="">

  <div class="gfb_wrap">
    
    <div class="gfb_maintab-title">
      <h2><?php _e('Settings', 'gfb'); ?></h2>
    </div>

    <div class="gfb_maintab-content">

      <div class="main-table">        
            
               <div class="main-tab">
                    
                    <ul>
                       <li class="active-tab"><a href="#general-tab"><span class="dashicons dashicons-admin-settings"></span><?php _e("General", "gfb"); ?></a></li>
                       <li><a href="#display-tab"><span class="dashicons dashicons-text-page"></span><?php _e("Display", "gfb"); ?></a></li>
                       <li><a href="#appointment-tab"><span class="dashicons dashicons-admin-tools"></span><?php _e("Appointments", "gfb"); ?></a></li>
                       <li><a href="#company-tab"><span class="dashicons dashicons-building"></span><?php _e("Company", "gfb"); ?></a></li>
                       <li><a href="#email-config-tab"><span class="dashicons dashicons-email-alt"></span><?php _e("Email Configuration", "gfb"); ?></a></li>
                       <li><a href="#google-calendar-tab"><span class="dashicons dashicons-calendar"></span><?php _e("Google Calendar", "gfb"); ?></a></li>
                    </ul>
                    
                    
                    
                    <div class="all-tab-container">
                    
                      <div class="gfbAjaxLoader" id="gfb_loader_img">
                            <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                        </div>
                        <div id="general-tab" class="tab-container active-tab">
                            <?php require_once( GFB_ADMIN_TAB . 'settings/general-settings.php' ); ?>
                        </div>
                        <div id="display-tab" class="tab-container">
                            <?php require_once( GFB_ADMIN_TAB . 'settings/display-settings.php' ); ?>
                        </div>
                        <div id="appointment-tab" class="tab-container">
                            <?php require_once( GFB_ADMIN_TAB . 'settings/appointment-settings.php' ); ?>
                        </div>
                        <div id="company-tab" class="tab-container">
                            <?php require_once( GFB_ADMIN_TAB . 'settings/company-settings.php' ); ?>
                        </div>
                        
                        <div id="email-config-tab" class="tab-container">
                            <?php require_once( GFB_ADMIN_TAB . 'settings/email-settings.php' ); ?>
                        </div>
                        
                        <div id="google-calendar-tab" class="tab-container">
                            <?php require_once( GFB_ADMIN_TAB . 'settings/google-calendar-settings.php' ); ?>
                        </div>
                        
                    
                    </div>
                          
               </div>
            
            </div>

    </div>

  </div>

</div><!-- /wrap -->
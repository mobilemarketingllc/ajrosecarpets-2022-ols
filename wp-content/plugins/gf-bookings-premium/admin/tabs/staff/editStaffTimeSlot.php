<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	global $gfbStaff;
	$timeslotdt = $gfbStaff->gfbGetStaffTimeSlotWithId($_POST['time_slot_id'], $_POST['staff_id']);
		
?> 
<form name="edit_staff_timeslot_form" class="edit-timeslot-form" id="edit_staff_timeslot_form" method="post"> 

    <div class="form-section">
    
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="time_slot_name"><?php _e('Title', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="time_slot_name" id="time_slot_name" class="input-main notallowspecial" value="<?php echo esc_attr($timeslotdt[0]['time_slot_name']); ?>" maxlength="100" />
            </div>
            <div class="form-element">
                <input type="hidden" name="time_slot_id" id="time_slot_id" class="input-main notallowspecial" value="<?php echo esc_attr($timeslotdt[0]['time_slot_id']); ?>" />
            </div>
            <div class="form-element">
                <input type="hidden" name="weekday" id="weekday" class="input-main notallowspecial" value="<?php echo esc_attr($timeslotdt[0]['weekday']); ?>" />
            </div>
            <div class="form-element">
                <input type="hidden" name="staff_id" id="staff_id" class="input-main notallowspecial" value="<?php echo esc_attr($timeslotdt[0]['staff_id']); ?>"  />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="slot_start_time"><?php _e('Start Time', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="slot_start_time" id="slot_start_time" class="input-main timespicker" value="<?php echo esc_attr($timeslotdt[0]['slot_start_time']); ?>" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="slot_end_time"><?php _e('End Time', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="slot_end_time" id="slot_end_time" class="input-main timespicker" value="<?php echo esc_attr($timeslotdt[0]['slot_end_time']); ?>" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="max_appointment_capacity"><?php _e('Max Appointment Capacity', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="max_appointment_capacity" id="max_appointment_capacity" class="input-main notallowspecialalpha" value="<?php echo esc_attr($timeslotdt[0]['max_appointment_capacity']); ?>" maxlength="3" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
        	<div class="form-element">
            	<?php submit_button('Update Time Slot'); ?>
            </div>
        </div>
        
    </div>
    
</form>
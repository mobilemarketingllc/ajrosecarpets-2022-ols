<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

//Check Verification
$res = GravityFormBookingCheckVerification::gfbGetCheckPluginVerification();		
			
if( $res == true ) {
	
	$staffObj = new GravityFormBookingStaff();
	$staffDt = $staffObj->getStaffNameList();	
	

?> 
<table class="gfb-list-staff">

	<?php foreach($staffDt as $staff) { ?>
    
	<tr>
    	<td>
        	<a href="#" data-staffid="<?php echo esc_attr($staff['staff_id']); ?>" id="get_staff_id"><?php echo esc_attr($staff['staff_name']); ?></a>
        </td>
    </tr>
    
    <?php } ?>
    
</table>
<?php
}
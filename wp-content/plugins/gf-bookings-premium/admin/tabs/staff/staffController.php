<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	require_once(GFB_ADMIN_DIR.'classes/gravityformbooking.google.calendar.api.php');

	$client_id = get_option('gfb_client_id');
	$client_secrete = get_option('gfb_client_secrete');
	$redirect_url = admin_url().'admin.php?page=gravity-form-booking-staff';
	global $wpdb;
	global $gfbStaff;
	global $current_user;
	
	if(isset($_GET['code'])) {
		try {

			$capi = new GFB_GoogleCalendarApi();
			
			$data = $capi->GetAccessToken($client_id, $redirect_url, $client_secrete, $_GET['code']);
			
			$data["createtime"] = time();
						
			$google_data = json_encode($data);
			
			if($_GET['state'])
			{
				$staff_id = base64_decode( $_GET['state'] );
			}
						
			$calendar_id = $capi->gfbGetCalendarId($staff_id);
						
			$wp_gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			
			
			$updateStaffData = array(
				'staff_gcal_data' => $google_data,
				'staff_gcal_id' => wp_kses_post($calendar_id),
				'modified_by' 	=> 	wp_kses_post(get_current_user_id()),
				'modified_date' => 	wp_kses_post(current_time("Y-m-d H:i:s")),
				'ip_address' 	=> wp_kses_post(GFB_Core::gfbIpAddress()),
 			);
			
			$where = array( 'staff_id' => $staff_id);
			
			$update = $wpdb->update($wp_gfb_staff_mst, $updateStaffData, $where);
		
			// Redirect to the page where user can create event
			
		}
		catch(Exception $e) {
			echo $e->getMessage();
			exit();
		}
	}
	
	if(isset($_GET['google_logout']))
	{
		$staff_id = $_GET['google_logout'];
		global $gfb_objCalendar;
		$auth_url = $gfb_objCalendar->logout($staff_id);
				
	}
?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Staff Member', 'gfb'); ?></h2>
            
            <?php if ( $current_user->has_cap( 'administrator' ) ) { ?>
            
                <div class="gfb_maintab-options">
                    <a href="#staffHtml" name="add_staff" id="add_staff" class="add-staff-modal button button-secondary"><?php _e('+ New Staff Member', 'gfb'); ?></a>
                </div>
                
                <div id="staffHtml" class="popup-modal mfp-hide white-popup">
                
                	<div class="gfbAjaxLoader" id="gfb_loader_img">
                        <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                    </div>
                    <?php require_once( GFB_ADMIN_TAB . 'staff/addStaff.php' );  ?>
                </div>
            
            <?php } ?>
		</div>		
        
		<div class="gfb_maintab-content">
        
        <div class="gfbAjaxLoader" id="gfb_loader_img">
            <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
        </div>
        
        <?php if ( $current_user->has_cap( 'administrator' ) ): ?>

			<div class="staff-selector">
				<?php 
					
					$staffList = $gfbStaff->getStaffNameList();
				?>
				<select class="chosen-select" name="stafflist" id="stafflist">
                
                	<option value=""><?php _e("Select Staff Member", "gfb"); ?></option>
					<?php foreach($staffList as $staff) { ?>
                    
                    <option value="<?php echo base64_encode($staff['staff_id']); ?>" <?php if(isset($_GET['state'])) {  if( base64_decode($_GET["state"]) == $staff['staff_id']) { echo 'selected="selected"'; } } ?> ><?php echo esc_attr($staff['staff_name']); ?></option>
                    
                	<?php } ?>
				</select>
			<?php if(!isset($_GET['state']))
			{ ?>
				
				<form method="post" name="staff_list_form" id="staff_list_form">
					<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
					<?php 
						require_once( GFB_ADMIN_TAB . 'staff/staffListClass.php');
						$staff_data = new Staff_List_Tbl;
						$staff_data->prepare_items();
						$staff_data->display(); 
					?>
				</form>
			<?php } ?>
				
			</div>
                        
            <div id="staffDetailSection">
               
			   	
            	<?php 
				/* If not staff member */
				if( empty($staffList) ) { 
					_e("No Staff Member Found", "gfb");
				} 
				
				/* url has staff id */
				if(isset($_GET['state'])) {	
					
					?>
					<a href="<?php echo $redirect_url; ?>" class="button button-primary button-back" >Back</a> 
					<?php				
					if( $gfbStaff->gfbIsTokenExpire( base64_decode($_GET['state'])) ) {	
						$jsontoken = json_decode( $gfbStaff->gfbStaffGoogleDataEncode( base64_decode($_GET['state']) ) );
					
						if( !empty( $jsontoken ) ) {	

							$refreshtoken = $gfbStaff->gfbRefreshToken($jsontoken->refresh_token,base64_decode($_GET['state']));

							
							
						}
					}
					
					echo '<label id="gfb_staff_id" class="hidden">'.$_GET['state'].'</label>';
					require_once( GFB_ADMIN_TAB . 'staff/staffDetail.php' );
						
					
				}	
				?>
            </div>	
            
            <?php elseif ( $current_user->has_cap( 'gfb_staff_role' ) ): ?>
            
				<?php 
                $gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
                $users = $wpdb->prefix . 'users';
                        
                $get_staff_id = $wpdb->get_results("select sm.staff_id from ".$users." um inner join ".$gfb_staff_mst." as sm on um.ID=sm.user_id where sm.is_deleted=0 AND um.ID=".get_current_user_id(), ARRAY_A);
                                
               	unset($_GET['state']);
                $_GET['state'] = base64_encode($get_staff_id[0]["staff_id"]);
                ?>
                
                <div id="staffDetailSection">
                
                    <input type="hidden" name="stafflist" id="stafflist" value="<?php echo base64_encode($get_staff_id[0]["staff_id"]); ?>" readonly="readonly" />
                    <?php 
                    /* get staff id from current user email */
                    if(isset($_GET['state'])) {	
                                            
                        if( $gfbStaff->gfbIsTokenExpire( base64_decode($_GET['state'])) ) {	
                                
                            $jsontoken = json_decode( $gfbStaff->gfbStaffGoogleDataEncode( base64_decode($_GET['state']) ) );

                            if( !empty( $jsontoken ) ) {						
                                $refreshtoken = $gfbStaff->gfbRefreshToken($jsontoken->refresh_token,base64_decode($_GET['state']));
                            }
                        }
						echo '<label id="gfb_staff_id" class="hidden">'.$_GET['state'].'</label>';
                        require_once( GFB_ADMIN_TAB . 'staff/staffDetail.php' );
                    }	
                    ?>
                </div>            
            
            <?php endif; ?>		

		</div>

	</div>

</div><!-- /wrap -->
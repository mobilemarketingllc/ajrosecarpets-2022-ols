<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



?> 
<h4 class="gfb_section-title"><?php _e("Daysoff Tab", "gfb"); ?></h4>

<div class="gfb_maintab-content-legends">
    <div class="holiday-legends">
        <div class="year-change-box">
            Select year: 
        </div>
        <div class="legend-box">
            Legends: 
            <span class="event-pending"></span> <?php _e("DAYSOFF", "gfb"); ?>
            <span class="event-completed" style="background: #eee;"></span> <?php _e("WORKING", "gfb"); ?>
        </div>
        <div class="holiday-btn-wrapper">
            <button id="add_holdiays" class="button button-primary" disabled >Add Holidays</button>
            <button id="remove_holdiays" class="button button-primary" disabled >Remove Holidays</button>
        </div>
    </div>
</div>     
   
<div class="gfb_maintab-content holidayCal" id="gfb_staff_dayoff">		
    <img class="gfb-ajax-loader-dayoff" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
</div>
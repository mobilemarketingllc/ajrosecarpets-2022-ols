<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	global $gfbStaff;
	$staff_services = $gfbStaff->gfbStaffServiceList($staffDetail[0]['staff_id']);

?> 
<h4 class="gfb_section-title"><?php _e("Services Tab", "gfb"); ?></h4>

<?php if( count($staff_services) > 0 ) { ?>

<form name="add_staff_service" class="staff-form" id="add_staff_service" method="post"> 
    
    <div class="staff-table-row">
    	
        <?php foreach($staff_services as $service) { ?>
        
            <div class="staff-box search-fade">
                <div class="staff-box-title">
					<label><?php echo $service['service_title']; ?></label>
                    <input type="checkbox" name="staff_name_service" id="staff_name_service" class="staff-input input-main notallowspecial" value="<?php echo $service['service_id']; ?>" <?php if( $service['service_id'] == $service['staff_service_id'] ) { ?> checked="checked" <?php } ?> />
                </div>

                <div class="staff-box-meta">
                	<input type="text" <?php if ( 'admin' != get_option( 'gfb_price_policy' ) ) { ?> name="staff_service_price_<?php echo $service['service_id']; ?>" id="staff_service_price_<?php echo $service['service_id']; ?>" value="<?php if( !empty( $service['staff_service'] ) ) { echo ltrim($service['staff_service'],'$'); } ?>" <?php } else { echo 'disabled'; } ?> class="staff-price-input input-main notallowspecialalpha" placeholder="<?php echo ltrim($service['service_price'],'$'); ?>" data-oldprice="<?php if( empty( $service['staff_service'] ) ) { echo str_replace("".get_option('gfb_currency_symbol')."", "", $service['service_price']); } ?>" maxlength="7" />
                	<p class="description"><strong><?php _e(' ( in '.get_option('gfb_currency_symbol').' )', 'gfb'); ?></strong></p>
                </div>
                
               
            </div>
        
        <?php } ?>
        
    </div>
    
    <div class="form-group-elements">
    	<div class="form-element">
        	<?php submit_button('Save'); ?>
        </div>
    </div>
    
</form>

<?php } else { ?>

<div class="staff-table-row">
	<p><?php _e('No Services Found To This Staff Member', 'gfb'); ?></p>
</div>
<?php } ?>
<?php

defined( 'ABSPATH' ) or exit; // Exit if accessed directly

if ( class_exists( 'GFForms' ) ) {
	class GFB_Appointment_Booking_Cost extends GF_Field {
		public $type = 'gfb_appointment_cost';
		
		public function get_form_editor_field_title() {
			return esc_attr__( 'Booking Cost', 'gravityforms' );
		}
		

		/*
		* Where to assign this widget
		*/
		public function get_form_editor_button() {
			return array(
				//'group' => 'advanced_fields',
				'group' => 'gfb_appointment_calendar',
				'text'  => $this->get_form_editor_field_title()
			);
		}	
		
		/*
		* Add button to the group
		*/
		public function add_button( $field_groups ) {
			$field_groups = $this->gfb_gfb_appointment_services_gf_group( $field_groups );
			return parent::add_button( $field_groups );
		}
		
		/*
		* Add our group
		*/	
		public function gfb_gfb_appointment_services_gf_group( $field_groups ) {
			foreach ( $field_groups as $field_group ) {
				if ( $field_group['name'] == 'gfb_appointment_calendar' ) {
					return $field_groups;
				}
			}		
			$field_groups[] = array(
				'name'   => 'gfb_appointment_calendar',
				'label'  => __( 'Booking Fields', 'simplefieldaddon' ),
				'fields' => array(
				)
			);

			return $field_groups;
		}	
		
		/*
		* Widget settings
		*/			
		function get_form_editor_field_settings() {
			return array(
				'label_setting',
				'error_message_setting',			
				'label_placement_setting',
				'admin_label_setting',
				'description_setting',
				'css_class_setting',
				'conditional_logic_field_setting',				
			);
		}

		public function is_conditional_logic_supported() {
			return true;
		}

		/**
		 * Field Markup
		 */			
		public function get_field_input( $form, $value = '', $entry = null ) {
			$form_id         = absint( $form['id'] );
			$is_entry_detail = $this->is_entry_detail();
			$is_form_editor  = $this->is_form_editor();

			$id       = $this->id;
			$field_id           = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
//            $logic_event        = ! $is_form_editor && ! $is_entry_detail ? 'gfield' : '';
			$size               = $this->size;
			$class_suffix       = $is_entry_detail ? '_admin' : '';
			$class              = $size . $class_suffix;
			$css_class          = trim( esc_attr( $class ) . ' gfield_select' );
			$tabindex           = $this->get_tabindex();
			
			
			$output = '<div class="ginput_container">';
			
			if( $this->is_entry_edit() ) {
				$output .= 'This field is not editable';
				$output .= "<input type='hidden' name='input_{$id}' id='{$field_id}' value='{$value}'/>";
			} elseif( !$this->is_form_editor() ) {
				$output .= '<script>jQuery("body").on("change", ".ginput_gfb_appointment_cost_input", function() { jQuery( this ).prev( "span" ).text( gformFormatMoney( this.value, true ) ); });</script>';
				$output .= "<span class='ginput_{$this->type} ginput_product_price ginput_{$this->type}_{$form_id}_{$field_id}'>" . GFCommon::to_money( '0' ) . "</span>";
                $output .= "<input type='hidden' name='input_{$id}' id='{$field_id}' style='{$this->get_inline_price_styles()}' class='{$class} ginput_{$this->type}_input gform_hidden' value='{$value}' {$tabindex}/>";
			}
			
			$output .= '</div>';
			
			return $output;
		}

		/**
		 * Is Entry Edit
		 */			
		public function is_entry_edit() {
			if ( rgget( 'page' ) == 'gf_entries' && rgget( 'view' ) == 'entry' && rgpost( 'screen_mode' ) == 'edit' ) {
				return true;
			}

			return false;
		}		
		
		/**
		 * Returns TRUE if the current page is the form editor page. Otherwise, returns FALSE
		 */		
		// public function is_form_editor() {
		// 	if ( rgget( 'page' ) == 'gf_edit_forms' && ! rgempty( 'id', $_GET ) && rgempty( 'view', $_GET ) ) {
		// 		return true;
		// 	}
		// 	return false;
		// }				
		
		public function get_inline_price_styles() {
			return '';
		}		
		
		/**
		 * Validation Failed Message
		 */			
		private function validationFailed( $message = '' ) {
			$this->failed_validation = true;
			$message = esc_html__( $message, 'gravityforms' );
			$this->validation_message = empty( $this->errorMessage ) ? $message : $this->errorMessage;
		}		

		/**
		 * Validation
		 */			
		public function validate( $value, $form ) {
			 $form_id = absint( $form['id'] );
			
			if ( !gfb_field_type_exists( $form, 'gfb_appointment_services' ) && get_option( 'gfb_calendar_availability' ) != 'global' ) {
				$this->validationFailed( gfb_get_form_translated_error_message($form_id, 'error_services_form') );
				return;
			}

			if ( !gfb_field_type_exists( $form, 'gfb_appointment_services' ) && get_option( 'gfb_calendar_availability' ) == 'global' ) {
				$service_id  = get_option( 'gfb_global_service' );
			} else {
				$service_id  = gfb_get_field_type_value( $form, 'gfb_appointment_services' );	
			}
			
			
			if( $service_id != "" ) {
				# valid
			} else {
				$this->validationFailed( gfb_get_form_translated_error_message($form_id, 'error_service_valid') );
				return;				
			} 
			
		}	
		
		/**
		 * Save Cost with Currency Symbol
		 */
		public function get_value_save_entry( $value, $form, $input_name, $entry_id, $entry ) {
			if( gfb_field_type_exists( $form, 'gfb_appointment_services' ) ) {
				$form_id = $form['id'];				
				// Service ID
				$service_id = absint( gfb_get_field_type_value( $form, 'gfb_appointment_services' ) );
				// Provider ID
				$provider_id = absint( gfb_get_field_type_value( $form, 'gfb_appointment_providers' ) );
				// Service Price
				global $wpdb;
				$service_maping_tbl = $wpdb->prefix . 'gfb_staff_service_mapping';			
				$service_result = $wpdb->get_results( 'SELECT service_id, service_price FROM '.$service_maping_tbl.' WHERE service_id = "'.$service_id.'" AND staff_id = "'.$provider_id.'" AND is_deleted=0', ARRAY_A );				
					//return $service_result;	
				$slot_qty = ! empty( gform_get_meta( $entry_id, 'slot_qty' ) ) ? gform_get_meta( $entry_id, 'slot_qty' ) : 1;

				if ( 'admin' != get_option( 'gfb_price_policy' ) && isset( $service_result[0]['service_price'] ) && ! empty( $service_result[0]['service_price'] ) ) {				
					$value = gfb_to_money($service_result[0]['service_price'] * $slot_qty );
				}else{
					global $gfbServiceObj;
					$serviceDetails = $gfbServiceObj->gfbServiceDetails($service_id);
					if(!empty($serviceDetails)){
						$value= gfb_to_money($serviceDetails[0]['service_price']  * $slot_qty );
					}else{
						$value= gfb_to_money(0);
					}
				}
			}
			return $value;			
		}		
		
		/**
		* Show appointment cost on entry single & GP Preview Plugin
		*/		
		public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {		
			return $value;
		}			
		
		
		
	} // end class
	GF_Fields::register( new GFB_Appointment_Booking_Cost() );	
} // end if

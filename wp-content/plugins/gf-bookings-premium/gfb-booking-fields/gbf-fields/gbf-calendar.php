<?php
defined( 'ABSPATH' ) or exit; // Exit if accessed directly


class gfb_calendar {
    private $month;
    private $year;
    private $selected_date;
    private $selected_slot;
    private $days_of_week = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
    private $week_starts;
    private $num_days;
    private $date_info;
    private $day_of_week;
    private $time_zone;
    private $service_id;
    private $provider_id;
    private $form_id = 0;
    private $current_month_appointments;
    private $calendar_availability;
    private $fullday_booking;

    public function __construct( $form_id, $month, $year, $service_id, $provider_id, $selected_date = false, $selected_slot = false  ) {
        
        // FORM ID
        $this->form_id = $form_id ? $form_id : 0;
        // MONTH & DATE
        $this->month         = $month;
        $this->year          = $year;
        $this->selected_date = $selected_date ? $selected_date : false;
        $this->selected_slot = $selected_slot ? $selected_slot : false;

        $this->fullday_booking = get_option('gfb_fullday_booking', 0);		

        // SERVICE & PROVIDER ID
        $this->service_id  = (int) $service_id;
        $this->provider_id = (int) $provider_id;
        // TIMEZONE
        $this->time_zone = gfb_time_zone( $this->provider_id );
        
        // DATEINFO
        $date = new DateTime();
        $date->setTimezone( new DateTimeZone( $this->time_zone ) );
        $this->date_info = $date->setDate( (int) $this->year, (int) $this->month, 1 );

        $this->num_days = $date->format('t');
        $this->day_of_week = $this->date_info->format('w');

        // Days of week translated
        $this->days_of_week = gfb_get_form_translated_data($this->form_id, 'weeks');

        // Week starts on
        $start_day_of_calendar = get_option('gbf_start_of_week');
        $this->week_starts = ! empty( $start_day_of_calendar ) ? $start_day_of_calendar : 'sunday';		

        $this->calendar_availability = get_option( 'gfb_calendar_availability' );

        //tomorrow I will continue from here.
        if ( 'global' === $this->calendar_availability && empty( $this->service_id ) && empty( $this->provider_id ) ) {
            $this->service_id  = (int) get_option( 'gfb_global_service' );
            $this->provider_id = (int) get_option( 'gfb_global_service_provider' );
        }

        add_filter( 'gform_notification', array( $this, 'gfb_gform_notification_signature'), 99, 3 );		
    }

    public function gfb_gform_notification_signature( $notification, $form, $entry ) {

        $entry_id = $entry['id'];
        $slot = gform_get_meta($entry_id, 'slot_qty');

        $staff_timezone = '';

        //$css = 'style="display: inline-block;color: #000;font-weight: bold;"';

        if ( get_option( 'gfb_staff_timezone_' . $this->provider_id ) != '' ) {
            $staff_timezone = get_option( 'gfb_staff_timezone_' . $this->provider_id );
        } else {
            $staff_timezone = wp_timezone_string();
        }

        $td_css='style="font-family:sans-serif;font-size:12px"';

        $previous_content = $notification['message'];
        $content = '<table style="width:99%"><tbody><tr bgcolor="#EAF2FA"><td class="con" colspan="2"><font '.$td_css.'><strong>Slot Capacity</strong></font></td></tr><tr bgcolor="#FFFFFF"><td class="con" width="20">&nbsp;</td><td class="con"><font '.$td_css.'>'.$slot.'</font></td></tr><tr bgcolor="#EAF2FA"><td class="con" colspan="2"><font '.$td_css.'><strong>Timezone</strong></font></td></tr><tr bgcolor="#FFFFFF"><td class="con" width="20">&nbsp;</td><td class="con"><font '.$td_css.'>'.$staff_timezone.'</font></td></tr></tbody></table>';

        $notification['message'] .= $content;

        return $notification;
    }

    
    /**
     * Get Available Days from Schedule
     */
    private function get_available_days($array, $timestamp) {
        $weeks = array('sunday' ,'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        $dates = array();
        foreach( $array as $week ) {
            if( in_array($week, $weeks) ) {
                $date = new DateTime();
                $date->setTimezone( new DateTimeZone( $this->time_zone ) );
                $date->setTimestamp($timestamp);
                $date->modify("first $week of this month");
                $thisMonth = $date->format('m');
                while ($date->format('m') == $thisMonth) {
                    $dates[] = $date->format('Y-m-j');
                    $date->modify("next $week");
                }
            }
        }

        return $dates;
    }

    /**
     * Get Date Timestamp
     */
    private function get_date_timestamp($year, $month, $day = false) {
        $date = new DateTime();
        $date->setTimezone( new DateTimeZone( $this->time_zone ) );
        $day = $day ? $day : 1;
        $date->setDate( (int) $year, (int) $month, $day );
        return $date->getTimestamp();
    }

    /**
     * Display Calendar
     */
    public function show() {
        // Calendar caption header
        $output = '<div class="gfb_appointments_calendar_header">' . PHP_EOL;

        $staff_timezone = '';

        if ( get_option( 'gfb_staff_timezone_' . $this->provider_id ) != '' ) {
            $staff_timezone = get_option( 'gfb_staff_timezone_' . $this->provider_id );
        } else {
            $staff_timezone = wp_timezone_string();
        }

        if ( $staff_timezone != '' ) {
            $staff_timezone = '<span class="show_timezone">* Times are in ' . $staff_timezone . '</span>';
        }

        // Previous Month
        if( $this->previous_month() ) {
            $output .= '<a class="arrow-left" date-go="'. $this->date_info->format('Y-m') .'" service_id="'.$this->service_id.'" provider_id="'.$this->provider_id.'" id="gfb_calendar_prev_month"><i class="fa fa-caret-left" aria-hidden="true"></i></a>' . PHP_EOL;
        }

        // Translation: Month/Year Caption
        $month = $this->date_info->format('F');
        $year  = $this->date_info->format('Y');
        $lang  = gfb_get_form_translated_month( $this->form_id, $month, '' );

        $output .= '<h3><span>'. $lang . '</span> <span>' . $year .'</span></h3>' . PHP_EOL;

        // Next Month		
        if ( isset( $_POST['current_month'] ) ) {
            $ym = $this->year . '-' . $this->month . '-01';         
            $timestamp = strtotime($ym);            
            $max_months = get_option('gfb_prior_months_book_appointment');          
            $prior_next = date('Y-m', strtotime( date('Y-m-01') ." +$max_months months" ));         
            $next_month = date('Y-m', mktime(0, 0, 0, date('m', $timestamp) + 1, 1, date('Y', $timestamp)));            
            if ( $next_month <= $prior_next ) {
                $output .= '<a class="arrow-right" date-go="'. $this->date_info->format('Y-m') .'" service_id="'.$this->service_id.'" provider_id="'.$this->provider_id.'" id="gfb_calendar_next_month"><i class="fa fa-caret-right" aria-hidden="true"></i></a>' . PHP_EOL;
            }
        } else {
            // Next Month       
            if( $this->next_month() ) {
                $output .= '<a class="arrow-right" date-go="'. $this->date_info->format('Y-m') .'" service_id="'.$this->service_id.'" provider_id="'.$this->provider_id.'" id="gfb_calendar_next_month"><i class="fa fa-caret-right" aria-hidden="true"></i></a>' . PHP_EOL;
            }
        }

        $output .= '</div>' . PHP_EOL;

        // Table start
        $output .= '<table class="table_fixed has-background" width="100%">' . PHP_EOL;
        $output .= '<thead calss="my_color">' . PHP_EOL;
        $output .= '<tr>' . PHP_EOL;

        // Week starts on monday		
        if( $this->week_starts == 'monday' ) {			
            $sunday = $this->days_of_week['sun'];
            unset( $this->days_of_week['sun'] );
            $this->days_of_week['sun'] = $sunday;
            $this->day_of_week = $this->day_of_week - 1;
            if ($this->day_of_week < 0) {
                $this->day_of_week = 6;
            }
        }		

        // Days of the week header
        foreach( $this->days_of_week as $day ) {
            $output .= '<th class="gfb_header">' . $day . '</th>' . PHP_EOL;
        }

        // Close header row and open first row of days
        $output .= '</tr>' . PHP_EOL;
        $output .= '</thead>' . PHP_EOL;

        $output .= '<tbody id="service-working-days">' . PHP_EOL;
        $output .= '<tr>' . PHP_EOL;

        // If first day of a month does not fall on a Sunday, then we need to fill
        if( $this->day_of_week > 0 ) {
            $output .= str_repeat( '<td class="gfb_day_past"></td>', $this->day_of_week );
        }

        // start num_days counter
        $current_day = 1;
    
        if( $this->month_passed($year,$month) ) {
            $available_days = array();
        }
        else{
            // available days
            $available_days = $this->get_available_days( array_keys( $this->get_week_day_schedule() ), $this->get_date_timestamp($this->year, $this->month) );
            
        }

        $service_price = $this->service_price(); // false for array

        $slots_html = '';

        
        // Loop and build days
        while( $current_day <= $this->num_days ) {
            
            // Reset 'days of week' counter and close each row if end of row

            $current_date = new DateTime( $this->date_info->format("Y-m-$current_day"), new DateTimeZone( $this->time_zone ) );
            $time_slots = $this->calendar_time_slots( $current_date, $slot = false );

            // echo '<pre>$time_slots$time_slots';
            // print_r( $time_slots );
            // echo '</pre>';
            
            /* Check is Time Slot */
            $aaray = (array)$current_date;
            $my_date = $aaray['date'];
            $dayNumber = date('N', strtotime($my_date));
            $providerId = $this->provider_id;
            $myClass = $this->check_is_time_slot($providerId,$dayNumber);
                        
            if ( in_array( $current_date->format("Y-m-d"), $this->get_holidays() ) ) {
                $classes = 'day_unavailable gfb_time_slots';
            } else {
                $classes = 'day_available gfb_time_slots';
            }
            
            if( $time_slots !== false ) {
                $slots_html .= $time_slots;
            }

            
            // Date selected from form submission
            $selected = '';
            if( $this->selected_date && in_array($this->selected_date->format('Y-m-j'), $available_days) )  {
                // Add selected class
                if( $this->selected_date->format('Y-m-j') == $current_date->format('Y-m-j') )  {
                    $selected = ' selected';
                    $classes .= $selected;
                }

                // Date Slots Mode
                if( $this->available_times_mode() == 'no_slots' )  {
                    # do nothing
                } else {
                    // Time Slots Mode
                    $placed_it = clone $this->selected_date;
                    $placed_it->modify("next {$this->week_starts}");

                    if( $current_date->format('Y-m-j') == $placed_it->format('Y-m-j') && $this->day_of_week = 7 ) {
                        $output  .= '</tr><tr id="gfbgbfappointments_calendar_slots"><td colspan="7" class="calendar_slots"><div class="calendar_time_slots">';
                        $output  .= $this->calendar_time_slots($this->selected_date, $this->selected_slot);
                        $output  .= '</div></td>';
                    }
                }
            }
            // Date selected from form submission

            // Reset 'days of week' counter and close each row if end of row
            if( $this->day_of_week == 7 ) {
                $output .= '</tr><tr>' . PHP_EOL;
                $this->day_of_week = 0;
            }

            // Current date is today
            if( $this->is_today( $current_date ) ) {
                $today    = ' gfb_today';
                $classes .= $today;
            } else {
                $today = '';
            }
            
            /* Add Class for holiday */
            $holiday="";
            if($this->get_holidays()){
                $myDate = $current_date->format('Y-m-d');
                $holiday_array = $this->get_holidays();
                if(!empty($holiday_array)){
                    foreach($holiday_array as $cut_date){
                        if($cut_date == $myDate){
                            $holiday = " gfb-gbf-holiday";
                        } 
                    }	
                }
            }
             

            // Date Slots Mode
            if( $this->available_times_mode() == 'no_slots' && ! in_array( $current_date->format("Y-m-d"), $this->get_holidays() ) )  {                
                if( $this->is_date_available($current_date) && !$this->max_schedule_days($current_date) ) {
                    // Translation Support
                    $month = $current_date->format("F");
                    $day   = $current_date->format("j");
                    $year  = $current_date->format("Y");
                    $translate = gfb_get_form_translated_slots_date( $this->form_id,  $month, $day, $year );
                    $lang_slot = ' lang_slot="'.$translate.'"';
                    // Translation Support
                        
                    /* Check Time slot is multitime available */
                    
                        
                    // Date is available
                    $classes = "day_available{$today}  $myClass{$selected}";

                    // Multiple Slots selection
                    $multi_select = (string) get_post_meta($this->service_id, 'gfb_service_multiple_selection', true);
                    $max_bookings = gfb_get_service_max_bookings($this->service_id);
                    $max_total    = gfb_get_service_max_selection($this->service_id);
                    $double       = gfb_get_service_double_bookings($this->service_id);
                    $multiple     = $multi_select == 'yes' ? ' multi-select="enabled" select-total="'.$max_total.'"  no_double="'.$double.'"' : '';

                    // Capacity available
                    if ( $count = $this->date_capacity_text($current_date) ) {						
                        $capacity = $count == 1 ? gfb_get_form_translated_space($this->form_id, $count) : gfb_get_form_translated_spaces($this->form_id, $count);
                        $classes .= ' gfb_tooltip';

                        $output .= '<td class="'.$classes.'" gfb-tooltip="'.$capacity.'" date-go="'.$this->date_info->format("Y-m-$current_day").'" service_cost="'.$service_price.'" service_id="'.$this->service_id.'" provider_id="'.$this->provider_id.'"'. $multiple . $lang_slot . 'capacity="'.$count.'"><span>'. $current_day .'</span></td>' . PHP_EOL;
                    } else {
                        $output .= '<td class="'.$classes.'" date-go="'.$this->date_info->format("Y-m-$current_day").'" service_cost="'.$service_price.'" service_id="'.$this->service_id.'" provider_id="'.$this->provider_id.'"'. $multiple . $lang_slot .' capacity="1"><span>'. $current_day .'</span></td>' . PHP_EOL;

                    }


                } else {
                    // Date not available
                    $classes = "day_unavailable{$today} $myClass $holiday";
                    $output .= '<td class="'.$classes.'" data-text="developer53" date-go="'.$this->date_info->format("Y-m-$current_day").'" service_cost="'.$service_price.'"><span>'. $current_day .'</span></td>' . PHP_EOL;
                }

            } else {                
                // Time Slots Mode
                $output .= '<td class="'.$classes.'" date-go="'.$this->date_info->format("Y-m-$current_day").'" service_id="'.$this->service_id.'" provider_id="'.$this->provider_id.'" service_cost="'.$service_price.'"><span>'. $current_day .'</span></td>' . PHP_EOL;

            }			

            // Increment counters
            $current_day++;
            $this->day_of_week++;
        }


        // Once num_days counter stops, if day of week counter is not 7, then we
        // need to fill remaining space on the row using colspan
        if( $this->day_of_week != 7 ) {
            $remaining_days = 7 - $this->day_of_week;
            $output .= str_repeat( '<td class="gfb_day_future"></td>', $remaining_days );
        }

        // Date selected from form submission
        // Place at the end
        if( $this->available_times_mode() == 'no_slots' )  {
            # do nothing
        } else {
            if( $this->selected_date && in_array($this->selected_date->format('Y-m-j'), $available_days) )  {
                // Last day of month from selected date
                $month_end = clone $this->selected_date;
                $month_end->modify("last day of this month");

                // Next sunday from selected date
                $placed_end = clone $this->selected_date;
                $placed_end->modify("next {$this->week_starts}");


                if( $placed_end > $month_end ) {
                    $output  .= '</tr><tr id="gfbgbfappointments_calendar_slots"><td colspan="7" class="calendar_slots"><div class="calendar_time_slots">';
                    $output  .= $this->calendar_time_slots($this->selected_date, $this->selected_slot);
                    $output  .= '</div></td>';
                }
            }
        }
        // Date selected from form submission

        // Close final row and table
        $output .= '</tr>' . PHP_EOL;

        $output .= '</tbody>' . PHP_EOL;
        $output .= '</table>' . PHP_EOL;		
        $output .= sprintf( '<div id="gfb_slots_data">%s</div>', $slots_html );
        $output .= $staff_timezone;
        return $output;
    }

    

    

    /**
     * Is slot available
     */
    public function is_slot_available( $dateTime, $slot ) {
        
        if( $this->available_times_mode() == 'custom' ) {
            $capacity = $slot['capacity'];
        } else {
            $capacity = $this->service_capacity();
        }
        return true;
    }


    /**
     * Capacity Available Text
     */
    public function slot_capacity_text( $dateTime, $slot ) {
        $capacity = $slot['capacity'];
        return $capacity;
    }

    /**
     * Date Available Query
     */
    public function date_availability_query( $dateTime ) {
        if( $this->schedule_lead_time_days( $dateTime ) ) {
            # valid date
        } else {
            return false;
        }

        $date = $dateTime->format("Y-m-j");

        $appointments = new WP_QUERY(
            array(
                'post_type'         => 'gfb_appointments',
                'posts_per_page'    => -1,
                'post_status'       => array( 'completed', 'publish', 'payment', 'pending' ),
                'orderby'           => 'meta_value',
                'order'             => 'ASC',
                'meta_query'        => array( 'relation' => 'AND',
                    array( 'key'    => 'gfb_appointment_date', 'value' => $date ),
                    array( 'key'    => 'gfb_appointment_provider', 'value' => $this->provider_id ),
                ),
            )
        );
        
        return $appointments;
    }

    /**
     * Date Available
     */
    public function is_date_available( $dateTime ) {
        // Week day shedule is not out
        $week_day = (string) strtolower( $dateTime->format('l') );
        $year  = $dateTime->format('Y');
        $schedule = $this->get_week_day_schedule();
        
        if( isset($schedule[$week_day]) ) {
            $schedule = $schedule[$week_day];
        } else {
            return false;
        }
        
        $appointments = $this->date_availability_query( $dateTime );

        $timeSlots = $this->get_slots( $dateTime );
        
        if(!empty(end($timeSlots))){			
            if ( ! empty( explode(':',end($timeSlots)['start'])[0] ) ) {
                $dateTime->add(new DateInterval('PT'.explode(':',end($timeSlots)['start'])[0].'H00S'));
            } else {
                return false;
            }
            
        }
        
        if( $this->date_slot_passed($dateTime) ) {
            
            return false;
        }

        $available_days = $this->get_available_days( array_keys( $this->get_week_day_schedule() ), $this->get_date_timestamp($this->year, $this->month) );
        
        if( $this->month_passed($year,$this->month) ) {
            $available_days = array();
        }

        if( in_array($dateTime->format('Y-m-j'), $available_days) && !in_array($dateTime->format("Y-m-j"), $this->get_holidays()) ) {
            # valid
        } else {
            return false;
        }

        $capacity = $this->service_capacity();

        return true;
    }


    /**
     * Date Capacity Text
     */
    public function date_capacity_text( $dateTime ) {
        
        $dayNumber = (string) strtolower( $dateTime->format('N') );
        if($dayNumber ==7){
            $dayNumber = 0;
        }
        $day_name = $dateTime->format('l');
        $providerId = $this->provider_id;
        $serviceId = $this->service_id;
        $slot_date = $dateTime->format('Y-m-d');

        $available_slots = get_option( 'gfb_staff_'.$this->provider_id.'_slots_for_'.$day_name );
        $slot_cap = get_option( 'gfb_staff_'.$this->provider_id.'_'.$day_name.'_slot_cap' );

        global $wpdb;
        $schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';	//sm		
        $time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';			//tms
        $gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';

        if ( $this->fullday_booking == 1 ) {
            $weekday_result =$wpdb->get_results("SELECT sm.staff_id,sm.staff_slot_mapping_id,tsm.weekday ,tsm.slot_start_time,tsm.slot_end_time,sum(tsm.max_appointment_capacity) as max_appointment_capacity FROM $schedule_mapping as sm INNER JOIN $time_slot_mst as tsm ON sm.time_slot_id = tsm.time_slot_id WHERE tsm.fullday=1 AND sm.staff_id=$providerId AND tsm.weekday=$dayNumber AND sm.is_deleted=0 AND tsm.is_deleted=0", ARRAY_A );
        } else {
            $weekday_result =$wpdb->get_results("SELECT sm.staff_id,sm.staff_slot_mapping_id,tsm.weekday ,tsm.slot_start_time,tsm.slot_end_time,sum(tsm.max_appointment_capacity) as max_appointment_capacity FROM $schedule_mapping as sm INNER JOIN $time_slot_mst as tsm ON sm.time_slot_id = tsm.time_slot_id WHERE tsm.fullday=0 AND sm.staff_id=$providerId AND tsm.weekday=$dayNumber AND sm.is_deleted=0 AND tsm.is_deleted=0", ARRAY_A );
        }		
        
        $max_appointment_capacity = $weekday_result[0]['max_appointment_capacity'];
        $staff_slot_mapping_id = $weekday_result[0]['staff_slot_mapping_id'];
        $appointment_capacities = $wpdb->get_results( 'SELECT staff_slot_mapping_id, slot_count as total_slot FROM '.$gfb_appointments_mst.' WHERE appointment_date ="'.$slot_date.'" AND service_id = '.$serviceId.' AND staff_id = '.$providerId.' AND status !=3 AND is_deleted=0', ARRAY_A );

        if ( !empty( $appointment_capacities ) ) {
            $booked_capacities = 0;

            foreach ( $appointment_capacities as $key => $slot ) {
                //print_r( $slot );
                $check_staff_slot_mapp_id = $wpdb->get_results( 'SELECT is_deleted FROM '.$schedule_mapping.' WHERE staff_slot_mapping_id ="'.$slot['staff_slot_mapping_id'].'" AND staff_id = '.$providerId.'', ARRAY_A );
                if ( isset( $check_staff_slot_mapp_id[0]['is_deleted'] ) && 1 != $check_staff_slot_mapp_id[0]['is_deleted'] ) {
                    $booked_capacities += $slot['total_slot'];	
                }
                
            }
            
            $aviable_capacity = $max_appointment_capacity - $booked_capacities;
        } else {
            $aviable_capacity = $max_appointment_capacity;
        }

        //if no capacity is available;
        if(empty($aviable_capacity) || $aviable_capacity < 0){
            $aviable_capacity ="No";
            return $aviable_capacity;
        }


        // if any of the slots are in break we will remove count of capacity from total count.
        if ( $this->fullday_booking != 1 && isset( $available_slots['add_break'] ) ) {
            $break_count = @array_count_values( $available_slots['add_break'] )['true'];

            if ( $break_count >= 0 ) {
                $break_count = $break_count * $slot_cap;
            }

            return $aviable_capacity - $break_count;
        }

        
        return $aviable_capacity;
    
    }


    /**
     * New appointment lead time require for new appointments
     * @ returns true if valid
     */
    public function schedule_lead_time( $dateTime, $slot_start ) {
        // Lead time minutes
        $service_lead_time  = get_post_meta($this->service_id, 'gfb_service_schedule_lead_time_minutes', true);
        $lead_time          = $service_lead_time && array_key_exists( $service_lead_time, gfb_schedule_lead_time_minutes() ) ? $service_lead_time : '240';

        // Today's date
        $today = gfb_current_date_with_timezone();


        // Slot time with date
        $slot_time = new DateTime( $slot_start, new DateTimeZone( $this->time_zone ) );
        $slot_time->setDate( $dateTime->format("Y"), $dateTime->format("m"), $dateTime->format("j") );

        // If no lead time, is slot dateTime less or equal then today
        if( $lead_time == 'no' ) {
            if( $slot_time <= $today ) {
                return false;
            }
            return true;
        }

        $lead = $today;
        $lead = $lead->modify("+{$lead_time} minutes");


        if( $slot_time <= $lead ) {
            //echo "Not valid: {$slot_time->format('Y-m-j H:i')} | Lead end: {$lead->format('Y-m-j H:i')}<br>";
            return false;
        }

        return true;
    }


    /**
     * New appointment lead time require for bookable dates
     * @ returns true if valid
     */
    public function schedule_lead_time_days( $dateTime ) {
        // Lead time minutes
        $service_lead_time  = get_post_meta($this->service_id, 'gfb_service_schedule_lead_time_minutes', true);
        $lead_time          = $service_lead_time && array_key_exists( $service_lead_time, gfb_schedule_lead_time_minutes() ) ? $service_lead_time : '240';

        // Today's date
        $today = gfb_current_date_with_timezone();


        // If no lead time, is slot dateTime less or equal then today
        if( $lead_time == 'no' ) {
            $dateTime->setTime(23, 59);
            if( $dateTime <= $today ) {
                return false;
            }
            return true;
        }

        $lead = $today;
        $lead = $lead->modify("+{$lead_time} minutes");

        if( $dateTime <= $lead ) {
            //echo "Not valid: {$slot_time->format('Y-m-j H:i')} | Lead end: {$lead->format('Y-m-j H:i')}<br>";
            return false;
        }

        return true;
    }


    /**
     * Display Slots
     */
    public function get_slots( $date ) {
        return $this->get_custom_slots( $date );
    }

    /**
     * Get Date Schedule
     */
    public function get_date_schedule( $date ) {
        $week_day = (string) strtolower( $date->format('l') );
        $schedule = $this->get_week_day_schedule();

        $available_days = $this->get_available_days( array_keys( $this->get_week_day_schedule() ), $this->get_date_timestamp($this->year, $this->month) );
        if( !in_array($date->format('Y-m-j'), $available_days) || $this->date_passed($date) || $this->max_schedule_days($date) ) {
            return false;
        }

        if( isset($schedule[$week_day]) ) {
            return $schedule[$week_day];
        } else {
            return false;
        }
    }


    /**
     * Custom Time Slots
     */
    public function get_custom_slots( $date ) {
        $week_day = (string) strtolower( $date->format('l') );
        if( !is_array( $this->custom_slots($date) ) || $this->get_date_schedule( $date ) === false ) {
            return array();
        }

        $slots = array();
        foreach( $this->custom_slots($date) as $slot ) {
            if( isset($slot['availability']) && in_array($week_day, $slot['availability']) ) {
                if( $this->is_slot_available( $date, $slot ) ) {
                    $startTime = new DateTime( $slot['start'] );
                    $endTime   = new DateTime( $slot['end'] );

                    $slots[sprintf( '%s-%s', $slot['start'], $slot['end'] )] = array(
                        'start'        => $slot['start'],
                        'end'          => $slot['end'],
                        'text'         => !empty( $slot['name'] ) ? $slot['name'] : $this->slot_text( $startTime, $endTime ),
                        'name'         => $slot['name'],
                        'duration'     => $this->get_slot_duration( $startTime, $endTime ),
                        'availability' => $slot['availability'],
                        'capacity'     => $slot['capacity'],
                        'price'        => $slot['price'],
                        'slot_mapping_id'=> $slot['slot_mapping_id']						
                    );
                }
                ;
            }
        }
        
        //if ( empty( $slots ) ) {
            //$slots[] = "";
        //}

        //sort array by slot mapping id ascending
        //sort array by slot mapping id ascending
        if ( version_compare( PHP_VERSION, '7.0.0', '>=' ) ) {
            usort( $slots, function($a, $b) {
                    return $a['slot_mapping_id'] <=> $b['slot_mapping_id'];
            });
        }

        return $slots;
    }

    public function get_slot_duration( $startTime, $endTime ) {
        $diff    = $startTime->diff( $endTime );
        $minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
        return $minutes;
    }

    /**
     * Generate Time Slots
     */
    public function generate_time_slots( $date ) {
        $week_day = (string) strtolower( $date->format('l') );

        if( $this->get_date_schedule( $date ) === false  ) {
            return array();
        } else {
            $schedule = $this->get_date_schedule( $date );
        }

        $duration        = (int) get_post_meta($this->service_id, 'gfb_service_duration', true);
        $cleanup         = (int) get_post_meta($this->service_id, 'gfb_service_cleanup', true);
        $start           = (string) $schedule['begin'];
        $end             = (string) $schedule['end'];

        $start           = new DateTime($start);
        $end             = new DateTime($end);
        $interval        = new DateInterval("PT" . $duration . "M");
        $cleanupInterval = new DateInterval("PT" . $cleanup . "M");
        $slots           = array();
        $exclude_slots   = array();

        for ($intStart = $start; $intStart < $end; $intStart->add($interval)->add($cleanupInterval)) {
            $endPeriod = clone $intStart;
            $endPeriod->add($interval);

            if( $endPeriod > $end ) {
                break;
            }

            if( $this->get_breaks( $week_day ) ) {
                foreach( $this->get_breaks( $week_day ) as $break_start => $break_end ) {
                    $break_start = new DateTime( $break_start );
                    $break_end   = new DateTime( $break_end );

                    if( $intStart >= $break_end || $endPeriod <= $break_start ) {
                        // Slot available
                        if( $this->is_slot_available( $date, $this->generate_slot_data($intStart, $endPeriod) ) ) {
                            $slots[sprintf( '%s-%s', $intStart->format('H:i'), $endPeriod->format('H:i') )] = $this->generate_slot_data($intStart, $endPeriod);

                        }
                    } else {
                        $exclude_slots[sprintf( '%s-%s', $intStart->format('H:i'), $endPeriod->format('H:i') )] = $this->generate_slot_data($intStart, $endPeriod);

                        if( $this->reduce_gaps() ) {
                            $intStart = $break_end;
                            $resetInterval = new DateInterval("PT0M");
                            $intStart->add($resetInterval)->add($cleanupInterval);
                            $endPeriod = clone $intStart;
                            $endPeriod->add($interval);

                            // Reset interval greater than end time
                            if( $endPeriod > $end ) {
                                break;
                            }

                            // Slot available
                            if( $this->is_slot_available($date, $this->generate_slot_data($intStart, $endPeriod)) ) {
                                $slots[sprintf( '%s-%s', $intStart->format('H:i'), $endPeriod->format('H:i') )] = $this->generate_slot_data($intStart, $endPeriod);
                            }
                        }
                    }
                } // endforeach
            } else {
                // Slot available
                if( $this->is_slot_available($date, $this->generate_slot_data($intStart, $endPeriod)) ) {
                    $slots[sprintf( '%s-%s', $intStart->format('H:i'), $endPeriod->format('H:i') )] = $this->generate_slot_data($intStart, $endPeriod);
                }
            }
        }

        return array_diff_key($slots, $exclude_slots);
    }

    private function generate_slot_data( $start, $end ) {
        $slot = array();
        $slot['start']    = $start->format('H:i');
        $slot['end']      = $end->format('H:i');
        $slot['text']     = $this->slot_text($start, $end);
        $slot['duration'] = (int) get_post_meta($this->service_id, 'gfb_service_duration', true);
        $slot['price']    = get_post_meta($this->service_id, 'gfb_service_price', true);
        $slot['capacity'] = get_post_meta($this->service_id, 'gfb_service_capacity', true);
        return $slot;
    }

    /**
     * Schedule Available Week Days
     */
    public function get_week_day_schedule() {
        $provider_schedule = $this->get_calendar_schedule();
        //$provider_schedule = array('friday'=>array('begin'=>'9:00', 'end'=>'5:00'),'monday' =>array('begin' => '09:00','end' => '17:00'));	
        foreach( $provider_schedule as $key => $schedule ) {
            if( $schedule['begin'] == 'out' ) {
                unset( $provider_schedule[$key] );
            }
        } 
        return $provider_schedule;
    }

    /**
     * Schedule Available Breaks
     */
    private function get_breaks( $week_day ) {
        $sort     = array();
        $breaks = $this->get_calendar_breaks();

        if( $this->provider_id == 0 ) {
            # do nothing
        } else {
            $provider_calendar = (string) get_post_meta( $this->provider_id, 'gfb_provider_calendar', true );
            if( $provider_calendar == 'on' ) {
                $breaks = (array) get_post_meta( $this->provider_id, 'gfb_provider_breaks', true );
            }
        }
        
            
        
        if( isset( $breaks[$week_day] ) && count($breaks[$week_day]) > 0 ) {
            $breaks = $breaks[$week_day];
        } else {
            return array();
        }

        foreach($breaks as $key => $part) {
            $sort[$key] = new DateTime($part);
        }


        array_multisort($sort, SORT_ASC, $breaks);

        return $breaks;

    }

    /**
     * Holidays
     */
    public function get_holidays() {
        $provider_holidays = $this->get_calendar_holidays();
        return $provider_holidays;
    }

    private function get_calendar_schedule() {
        if($this->provider_id ==0) {

            $options = get_option( 'gfb_appointments_work_schedule' );
            $work_schedule = $options && is_array($options) ? $options : $this->get_calendar_defauls();

        }else{
            $providerId = $this->provider_id;
            global $wpdb;
            $schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';			
            $time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';			
            
            if ( $this->fullday_booking == 1 ) {
                $weekday_result =$wpdb->get_results("SELECT sm.staff_id,tsm.weekday ,tsm.slot_start_time,tsm.slot_end_time FROM $schedule_mapping as sm INNER JOIN $time_slot_mst as tsm ON sm.time_slot_id = tsm.time_slot_id WHERE tsm.fullday=1 and sm.staff_id=$providerId AND sm.is_deleted=0 AND tsm.is_deleted=0 GROUP BY tsm.weekday", ARRAY_A );
            } else {
                $weekday_result =$wpdb->get_results("SELECT sm.staff_id,tsm.weekday ,tsm.slot_start_time,tsm.slot_end_time FROM $schedule_mapping as sm INNER JOIN $time_slot_mst as tsm ON sm.time_slot_id = tsm.time_slot_id WHERE sm.staff_id=$providerId AND sm.is_deleted=0 AND tsm.is_deleted=0 GROUP BY tsm.weekday", ARRAY_A );
            }

            $work_schedule=[];
            $days = array('sunday', 'monday', 'tuesday', 'wednesday','thursday','friday', 'saturday');

            foreach ($weekday_result as $daysNumber){
                $day = $daysNumber['weekday'];
                $slot_start_time = $daysNumber['slot_start_time'];
                $slot_end_time = $daysNumber['slot_end_time'];
                $dayName = strtolower(date('l', strtotime($days[$day])));
                $work_schedule[$dayName]= array('begin'=>$slot_start_time, 'end'=>$slot_end_time);
            }
        }
        return $work_schedule;
    }
    
    /**
    * Calendar Holidays * 
    **/
    private function get_calendar_breaks() {
        $options = get_option( 'gfb_appointments_schedule_breaks' );
        $breaks = (array) $options;
        return $breaks;
    }
    
    /**
    * Calendar Holidays * 
    **/
    
    private function get_calendar_holidays() {
        $providerId= $this->provider_id;
        global $wpdb;
        $holiday_mapping_tbl = $wpdb->prefix . 'gfb_staff_holiday_mapping';	
            
        $holiday_result = $wpdb->get_results( 'SELECT holiday_date FROM '.$holiday_mapping_tbl.' WHERE staff_id = "'.$providerId.'" AND is_deleted=0', ARRAY_A );				
    
        $holidays =[]; 
        foreach($holiday_result as $result){
            $holidays[] = $result['holiday_date'];
        }
        return $holidays;
    }
    
    /**
     * Default Schedule
     */
    private function get_calendar_defauls() {
        $schedule = array( 'sunday'    => array('begin' => 'out',   'end' => 'out'),
                           'monday'    => array('begin' => '09:00', 'end' => '17:00'),
                           'tuesday'   => array('begin' => '09:00', 'end' => '17:00'),
                           'wednesday' => array('begin' => '09:00', 'end' => '17:00'),
                           'thursday'  => array('begin' => '09:00', 'end' => '17:00'),
                           'friday'    => array('begin' => '09:00',   'end' => '17:00'),
                           'saturday'  => array('begin' => 'out',   'end' => 'out'),
        );
        $schedule = array();
        return $schedule;
    }

    /**
     * Reduce gaps
     */
    private function reduce_gaps() {
        $reduce_gaps = get_post_meta( $this->service_id, 'gfb_service_reduce_gaps', true );

        // Reduce Gaps
        if( $reduce_gaps && in_array($reduce_gaps, array('yes', 'no')) ) {
            $gaps = $reduce_gaps;
        } else {
            $gaps = 'yes'; // days
        }

        if( $gaps == 'yes' ) {
            return true;
        }

        return false;
    }

    /**
     * Prior Days To Book Appointment
     */
    private function max_schedule_days( $date_input ) {
        //$max_days = get_post_meta( $this->service_id, 'gfb_service_schedule_max_future_days', true );
        $max_days = get_option('gfb_prior_days_book_appointment');
        //$max_days = $max_days && array_key_exists( $max_days, gfb_schedule_max_future_days() ) ? $max_days : 90;
        $max_days = ! empty( $max_days ) ? $max_days : 0;


        // SERVICE PERIOD TYPE
        $period_type = (string) get_post_meta($this->service_id, 'gfb_service_period_type', true);

        if( $period_type == 'date_range' ) {
            $range = (array) get_post_meta($this->service_id, 'gfb_service_date_range', true);
            if( isset($range['from']) && gfb_valid_date_format($range['from']) && isset($range['to']) && gfb_valid_date_format($range['to']) ) {
                $end_range = new DateTime($range['to'], new DateTimeZone( $this->time_zone ));
                $end_range->setTime(24,00);
                return $date_input > $end_range;
            }
            return true;
        }

        if( $period_type == 'custom_dates' ) {
            $custom_dates = (array) get_post_meta($this->service_id, 'gfb_service_custom_dates', true);
            if( is_array($custom_dates) && count($custom_dates) > 0 && gfb_valid_date_format(end($custom_dates)) ) {
                $end_custom_date = new DateTime(end($custom_dates), new DateTimeZone( $this->time_zone ));
                $end_custom_date->setTime(24,00);
                return $date_input > $end_custom_date;
            }

            return true;
        }

        // Future Days Period
        $date = new DateTime();
        $date->setTimezone( new DateTimeZone( $this->time_zone ) );
        $date->add(new DateInterval( "P" . $max_days . "D" ));
        $date->setTime(00, 00);

        $calendar = $date_input; // DateTime Object
        $calendar->setTimezone( new DateTimeZone( $this->time_zone ) );
        $calendar->setTime(00, 00);

        return $calendar < $date;

    }


	public function calendar_time_slots($date, $sel_slot = false) {
		
		
		
		// Translation Support
		$month = $date->format("F");
		$day_name  = $date->format("l");
		$day_name  = gfb_get_form_translated_data($this->form_id, 'long_weeks')[strtolower($day_name)];
		$day   = $date->format("j");
		$year  = $date->format("Y");
		$text  = gfb_get_form_translated_slots_date($this->form_id, $month, $day, $year);
		// Translation Support

		// Slot size
		$slots_size = $this->show_end_times() ? 'slot_large grid-lg-12 grid-md-12 grid-sm-12 grid-xs-12' : 'slot_small grid-lg-3 grid-md-3 grid-sm-3 grid-xs-6';

		// Time Format Display
		$time_display  = gfb_service_time_format_display($this->service_id);

		// Multiple Slots selection
		$multi_select  = (string) get_post_meta($this->service_id, 'gfb_service_multiple_selection', true);
		$max_bookings  = gfb_get_service_max_bookings($this->service_id);
		$max_total     = gfb_get_service_max_selection($this->service_id);
		$double        = gfb_get_service_double_bookings($this->service_id);
		$time_format   = (string) get_post_meta($this->service_id, 'gfb_service_time_format', true);
		$remove_am_pm  = (string) get_post_meta($this->service_id, 'gfb_service_remove_am_pm', true);
		$multiple      = $multi_select == 'yes' ? ' multi-select="enabled" select-max="'.$max_bookings.'" select-total="'.$max_total.'" time_format="'.$time_format.'" remove_am_pm="'.$remove_am_pm.'" no_double="'.$double.'"' : '';
		$out = '';

		$timeSlots = $this->get_slots( $date );
		$available_slots = get_option( 'gfb_staff_'.$this->provider_id.'_slots_for_'.$day_name );

		if( count($timeSlots) > 0  ) { 			
			$out .= sprintf( '<div id="%s">', $date->format('Y-m-j') ); // eg. February 24, 2018
				$out .= '<h3 class="slots-title">' .$text. ' - '.$day_name. '</h3>'; // eg. February 24, 2018
				$out .= '<div class="grid-row grid_no_pad">';
				$i = 0;
					foreach( $timeSlots as $key => $slot ) {

						//TODO: Possibly returns bad custom time slot price

						if ( $this->fullday_booking == 1 ||  'false' == $available_slots['add_break'][ $i ] ) {

							$sel_class = $sel_slot && $sel_slot == sprintf( '%s-%s', $slot['start'], $slot['end'] ) ? ' time_selected' : '';
							$slot_cost  = $this->available_times_mode() == 'custom' ? $slot['price'] : $this->service_price();

							// Slot Language
							if ( empty($slot) ) {
								$out .= '<div class="grid-lg-12 grid-md-12 grid-sm-12 grid-xs-12">No time slot is available</div>';
							} else {
								$slotID = sprintf( '%s-%s', $slot['start'], $slot['end'] );
								if( $this->slot_capacity_text( $date, $slot ) ) {
									$count    = $this->slot_capacity_text( $date, $slot );
									//echo 'mufaddal ' . $count;
									$capacity = $count == 1 ? gfb_get_form_translated_space($this->form_id, $count) : gfb_get_form_translated_spaces($this->form_id, $count);
									$slot_arr = explode("-",$slotID);
									$out .= '<div class="'.$slots_size.' grid_no_pad">
											<label class="time_slot gfb_tooltip'.$sel_class.'" time_slot="'.$slotID.'" gfb-tooltip="'.$count.' Appointment available"'.$multiple.' capacity="'.$count.'" service_id="'.$this->service_id.'" slot_cost="'.$slot_cost.'" slot_mapping_id="'.$slot['slot_mapping_id'].'"><div>'.ltrim(date("hA", strtotime($slot_arr[0])), 0).'-'.ltrim(date("hA", strtotime($slot_arr[1])), 0).'</div></label>
											<label class="slot_label">Select slot(s):</label>
											<input type="number" name="slot_qty" class="slot_qty" min="1" max="'.$count.'" step="1" value="1" />
										</div>';
								}
							}
						}

						$i++;
					}
				$out .= '</div>';
			$out .= '</div>';
		} else {
			$out = false;
			
		}

		return $out;
	}


    /**
     * Slot Text
     */
    private function slot_text($intStart, $endPeriod) {
        $remove_am_pm = $this->remove_am_pm() == 'no' ? 'A' : '';
        $time_format  = $this->time_format() == '24h' ? "G:i {$remove_am_pm}" : "g:i {$remove_am_pm}";

        $start = gfb_get_form_translated_am_pm($this->form_id, $intStart->format($time_format));
        $end   = gfb_get_form_translated_am_pm($this->form_id, $endPeriod->format($time_format));

        if($this->show_end_times()) {
            return $start . ' - ' . $end;
        } else {
            return  $start;
        }

    }

    /**
     * Show End Times
     */
    public function show_end_times() {
        $show_end_times = get_post_meta( $this->service_id, 'gfb_service_show_end_times', true );

        // Show End Times
        if( $show_end_times && in_array($show_end_times, array('yes', 'no')) ) {
            $end_times = $show_end_times;
        } else {
            $end_times = 'no'; // days
        }

        if( $end_times == 'yes'  ) {
            return true;
        }

        return false;

    }

    /**
     * Time Format
     */
    public function time_format() {
        $time_format = get_post_meta( $this->service_id, 'gfb_service_time_format', true );

        // Time Format
        if( $time_format && in_array($time_format, array('12h', '24h')) ) {
            $format = $time_format;
        } else {
            $format = '12h'; // 12h format
        }


        return $format;

    }

    /**
     * Time Format
     */
    public function remove_am_pm() {
        $remove_am_pm = get_post_meta( $this->service_id, 'gfb_service_remove_am_pm', true );

        // Time Format
        if( $remove_am_pm && in_array($remove_am_pm, array('no', 'yes')) ) {
            $remove = $remove_am_pm;
        } else {
            $remove = 'no'; // 12h format
        }
        return $remove;
    }


    /**
     * Current Date Today
     */
    private function is_today( $current_date ) {
        $today = gfb_current_date_with_timezone();
        return $today->format('Y-m-j') == $current_date->format('Y-m-j');
    }


    /**
    * Date Passed
    */
    private function date_passed($dateTime) {
        $day = $dateTime->format('j');
        
        $now = gfb_current_date_with_timezone();

        $calendar = new DateTime();
        $calendar->setTimezone( new DateTimeZone( $this->time_zone ) );
        $calendar->setDate( (int) $this->year, (int) $this->month, $day );

        return $now > $calendar;
    }

    /**
    * Date Slot Passed
    */
    private function date_slot_passed($dateTime) {
        $now = gfb_current_date_with_timezone();
        return $now > $dateTime;
    }

    /**
    * Month Passed
    */
    private function month_passed($year,$month) {
        
        if(!is_numeric($month)){

            $month = new DateTime($month);
            $month->setTimezone( new DateTimeZone( $this->time_zone ) );
            
            
            
            $month->setDate( $year, $month->format('n'), 1 );
            
        }

        if(is_numeric($month)){
            $n = $month;
            $now = new DateTime();
            $now->setTimezone( new DateTimeZone( $this->time_zone ) );
            $month = new DateTime($now->format('Y')."-$month-01");
            $month->setTimezone( new DateTimeZone( $this->time_zone ) );
            $month->setDate( $year, $n, 1 );
        }
        
        
        $now = gfb_current_date_with_timezone();
        
        $max_months = get_option('gfb_prior_months_book_appointment');
        // Future months Period
        $months = new DateTime();
        $months->setTimezone( new DateTimeZone( $this->time_zone ) );
        $months->add(new DateInterval( "P" . $max_months . "M" ));
        $months->setTime(00, 00);
        
        return $months < $month;
    }

    /**
    * Previous Month
    */
    private function previous_month() {
        $previous = new DateTime();
        $previous->setTimezone( new DateTimeZone( $this->time_zone ) );
        $previous->setDate( $this->date_info->format('Y'), $this->date_info->format('n'), 1 );
        $previous->modify( 'last day of previous month' );


        // SERVICE PERIOD TYPE
        $period_type = (string) get_post_meta($this->service_id, 'gfb_service_period_type', true);

        if( $period_type == 'date_range' ) {
            $range = (array) get_post_meta($this->service_id, 'gfb_service_date_range', true);
            if( isset($range['from']) && gfb_valid_date_format($range['from']) && isset($range['to']) && gfb_valid_date_format($range['to']) ) {
                $begin_range = new DateTime($range['from'], new DateTimeZone( $this->time_zone ));
                return $previous > $begin_range;
            }
            return false;
        }

        if( $period_type == 'custom_dates' ) {
            $custom_dates = (array) get_post_meta($this->service_id, 'gfb_service_custom_dates', true);
            if( is_array($custom_dates) && count($custom_dates) > 0 && gfb_valid_date_format(reset($custom_dates)) ) {
                $begin_custom_date = new DateTime(reset($custom_dates), new DateTimeZone( $this->time_zone ));
                return $previous > $begin_custom_date;
            }
            return false;
        }

        return $previous >= gfb_current_date_with_timezone();
    }


    /**
    * Next Month
    */
    private function next_month() {
        $next = new DateTime();
        $next->setTimezone( new DateTimeZone( $this->time_zone ) );
        $next->setDate( $this->date_info->format('Y'), $this->date_info->format('n'), 1 );
        $next->modify( 'first day of next month' );		

        if( $this->month_passed( $this->year, $this->month ) ) {
            return false;
        }
        
        return true;
    }

    /**
    * Month future
    */
    private function month_future() {
        $now = new DateTime();
        $now->setTimezone( new DateTimeZone( $this->time_zone ) );
        $now->setDate( $now->format('Y'), $now->format('n'), 1 );
        return $now > $this->date_info;
    }

    private function available_times_mode() {
        return (string) "no_slots";//get_post_meta( $this->service_id, 'gfb_service_available_times_mode', true );
    }

    private function service_capacity() {
        $a= "5";
        return $a; 
        //return (int) get_post_meta( $this->service_id, 'gfb_service_capacity', true );
    }

    private function custom_slots($date) {
        
        $dayNumber = (string) strtolower( $date->format('N') );
        if($dayNumber ==7){
            $dayNumber = 0;
        }
        $providerId = $this->provider_id;
        $serviceId = $this->service_id;
        $slot_date = $date->format('Y-m-d');



        global $wpdb;
        $staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
        $gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
        $time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';

        if ( 1 == $this->fullday_booking ) {			
            $result = $wpdb->get_results( 'SELECT * FROM '.$staff_schedule_mapping.' as sm LEFT JOIN '.$time_slot_mst.' as tsm on sm.time_slot_id = tsm.time_slot_id WHERE tsm.fullday=1 AND sm.staff_id = '.$providerId.' AND tsm.weekday= '.$dayNumber.' AND sm.is_deleted=0 AND tsm.is_deleted=0 order by sm.staff_slot_mapping_id asc', ARRAY_A );
        } else {			
            $result = $wpdb->get_results( 'SELECT * FROM '.$staff_schedule_mapping.' as sm LEFT JOIN '.$time_slot_mst.' as tsm on sm.time_slot_id = tsm.time_slot_id WHERE tsm.fullday=0 AND sm.staff_id = '.$providerId.' AND tsm.weekday= '.$dayNumber.' AND sm.is_deleted=0 AND tsm.is_deleted=0 order by sm.staff_slot_mapping_id asc', ARRAY_A );
        }
        
        $TimeSolts = [];
        foreach($result as $resultData){
            $staff_slot_mapping_id= $resultData['staff_slot_mapping_id'];
            $max_appointment_capacity= $resultData['max_appointment_capacity'];
            $appointment_capacities = $wpdb->get_results( 'SELECT slot_count as total_slot FROM '.$gfb_appointments_mst.' WHERE appointment_date ="'.$slot_date.'" AND staff_slot_mapping_id = '.$staff_slot_mapping_id.' AND service_id = '.$serviceId.' AND staff_id = '.$providerId.' AND status !=3 AND is_deleted=0', ARRAY_A );
            
            if ( !empty($appointment_capacities) ) {
                
                $booked_capacities = 0;

                foreach ( $appointment_capacities as $key => $slot_count ) {
                    $booked_capacities += $slot_count['total_slot'];
                    //$booked_capacities = $appointment_capacities[0]['total_slot'];
                    $aviable_capacity= $max_appointment_capacity - $booked_capacities;
                }
            }else{
                $aviable_capacity = $max_appointment_capacity;
            }

            if($aviable_capacity >= 1) {
                $start= $resultData['slot_start_time'];
                $end = $resultData['slot_end_time'];
                $slot_name = $resultData['time_slot_name'];
                $TimeSolts[$start." ".$end]= array('start' =>$start, 'end' => $end,'availability' => array('sunday' ,'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'),'capacity' => $aviable_capacity,'slot_mapping_id'=>$staff_slot_mapping_id,'price' => '00', 'name' => $slot_name );
            }
        }		
        
        return $TimeSolts;
    } 
    
    
    /**
    * Get Services Price 
    */
    private function service_price() {
        
        $serviceId = $this->service_id;
        $providerId = $this->provider_id;

        global $wpdb;
        $service_maping_tbl = $wpdb->prefix . 'gfb_staff_service_mapping';			
        $service_result = $wpdb->get_results( 'SELECT service_id, service_price FROM '.$service_maping_tbl.' WHERE service_id = "'.$serviceId.'" AND staff_id = "'.$providerId.'" AND is_deleted=0', ARRAY_A );		

        if ( 'admin' != get_option( 'gfb_price_policy' ) && isset( $service_result[0]['service_price'] ) && ! empty( $service_result[0]['service_price'] ) ) {
            $servicePrice = $service_result[0]['service_price'];
        }else{
            global $gfbServiceObj;
            $serviceDetails = $gfbServiceObj->gfbServiceDetails($this->service_id);
            if(!empty($serviceDetails)){
                $servicePrice= $serviceDetails[0]['service_price'];
            }else{
                $servicePrice="";
            }
        }

        return $servicePrice;//get_post_meta( $this->service_id, 'gfb_service_price', true );
    
    }

    
    /**
    *Check IS Time Slot 
    **/
    public function check_is_time_slot($providerId,$dayNumber){
                
        $MyClass =" gfb_time_slots";
        return $MyClass;
    }
    
    /**
    *Check IS Time Slot 
    **/
    
} // end class

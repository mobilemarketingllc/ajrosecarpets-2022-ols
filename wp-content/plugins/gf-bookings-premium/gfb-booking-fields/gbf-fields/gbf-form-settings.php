<?php 
defined( 'ABSPATH' ) or exit; // Exit if accessed directly
/**
 * Handle displaying content for our custom menu when selected
 */

GFForms::include_addon_framework();

class GFB_booking_addon extends GFAddOn {
	
	protected $_min_gravityforms_version = '1.0';
	protected $_slug = 'gfb_booking_addon';
	protected $_path = 'gfb-booking-fields/gbf-fields/gfb-booking-addon.php';
	protected $_full_path = __FILE__;
	protected $_title = 'Gravity Forms Booking';
	protected $_short_title = 'Gravity Forms Booking';

	private static $_instance = null;

	/**
	 * Get an instance of this class.
	 *
	 * @return GFB_booking_addon
	 */
	public static function get_instance() {
		if ( self::$_instance == null ) {
			self::$_instance = new GFB_booking_addon();
		}

		return self::$_instance;
	}

	/**
	 * Handles hooks and loading of language files.
	 */
	public function init() {
		parent::init();
		//add_action( 'gfb_custome_fields',array( $this, 'gfbgbfappointments_form_page' ),10);
		/*add_filter( 'gform_form_settings', array($this,'my_custom_function'), 10, 2 );
		 add_filter( 'gform_submit_button', array($this,'form_submit_button'), 10, 2 ); */
	}
	
	
	/* public function form_submit_button( $button, $form ) {
		return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
	} */
	
	/**
	 * Configures the settings which should be rendered on the Form Settings > Simple Add-On tab.
	 *
	 * @return array
	 */
	public function form_settings_fields( $form ) {
		//if( 0 >= did_action( 'gfb_custome_fields' ) )
		//do_action('gfb_custome_fields');
		
		return array(
			array(
				'title'  => esc_html__( 'General Settings', 'gfb_booking_addon' ),
				'fields' => array(
					array(
						'type' => 'gfb_general_form_opt'
					)
				)
			),
			array(
				'title'  => esc_html__( 'User Registration Settings', 'gfb_booking_addon' ),
				'fields' => array(
					array(
						'label' => esc_html__( 'Name (FULL)', 'gfb_booking_addon' ),
						'tooltip' => esc_html__( 'Select Customer Name Field', 'gfb_booking_addon' ),
						'type'  => 'field_select',
						'name'  => 'customer_name',
						'required'  => 'required',
					),
					array(
						'label' => esc_html__( 'Email', 'gfb_booking_addon' ),
						'tooltip' => esc_html__( 'Select Customer Email Field', 'gfb_booking_addon' ),
						'type'  => 'field_select',
						'name'  => 'customer_email',
						'required'  => 'required',
					),
					array(
						'label' => esc_html__( 'Number', 'gfb_booking_addon' ),
						'tooltip' => esc_html__( 'Select Customer Number Field', 'gfb_booking_addon' ),
						'type'  => 'field_select',
						'name'  => 'customer_number',
					)
				)
			),
		);
	}

	/**
	 * Build an array of choices containing fields which are compatible with conditional logic.
	 *
	 * @return array
	 */
	public function settings_gfb_general_form_opt() {
		/* GFFormSettings::page_header();  */
		$form_id = rgget( 'id' ); 
		$this->save_gfbgbfappointments_form_page($form_id);
		
		$form = GFAPI::get_form( $form_id );
	?>	
<!-- 			<h3><span><i class="fa fa-cogs"></i> General Settings</span></h3>
			<hr> -->
			<table class="form-table">		
				<tr>
					<th scope="row">
						<label for="clear_time">Calendar week days short names</label>
					</th>
					<td>
						<?php
							$weeks  = gfb_get_form_translated_data($form_id, 'weeks');
						?>
						<p class="gfb_translation_title">Sunday</p> <input type="text" name="gfbgbfappointments_translation[weeks][sun]" value="<?php echo $weeks['sun']; ?>"><br>
						<p class="gfb_translation_title">Monday</p> <input type="text" name="gfbgbfappointments_translation[weeks][mon]" value="<?php echo  $weeks['mon']; ?>"><br>
						<p class="gfb_translation_title">Tuesday</p> <input type="text" name="gfbgbfappointments_translation[weeks][tue]" value="<?php echo  $weeks['tue']; ?>"><br>
						<p class="gfb_translation_title">Wednesday</p> <input type="text" name="gfbgbfappointments_translation[weeks][wed]" value="<?php echo  $weeks['wed']; ?>"><br>
						<p class="gfb_translation_title">Thursday</p> <input type="text" name="gfbgbfappointments_translation[weeks][thu]" value="<?php echo  $weeks['thu']; ?>"><br>
						<p class="gfb_translation_title">Friday</p> <input type="text" name="gfbgbfappointments_translation[weeks][fri]" value="<?php echo  $weeks['fri']; ?>"><br>
						<p class="gfb_translation_title">Saturday</p> <input type="text" name="gfbgbfappointments_translation[weeks][sat]" value="<?php echo  $weeks['sat']; ?>">
					</td>
				</tr>



				<tr>
					<th scope="row">
						<label for="clear_time">Calendar week days long names</label>
					</th>
					<td>
						<?php
							$long_weeks  = gfb_get_form_translated_data($form_id, 'long_weeks'); 
						?>
						<p class="gfb_translation_title">Sunday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][sunday]" value="<?php echo $long_weeks['sunday']; ?>"><br>
						<p class="gfb_translation_title">Monday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][monday]" value="<?php echo  $long_weeks['monday']; ?>"><br>
						<p class="gfb_translation_title">Tuesday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][tuesday]" value="<?php echo  $long_weeks['tuesday']; ?>"><br>
						<p class="gfb_translation_title">Wednesday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][wednesday]" value="<?php echo  $long_weeks['wednesday']; ?>"><br>
						<p class="gfb_translation_title">Thursday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][thursday]" value="<?php echo  $long_weeks['thursday']; ?>"><br>
						<p class="gfb_translation_title">Friday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][friday]" value="<?php echo  $long_weeks['friday']; ?>"><br>
						<p class="gfb_translation_title">Saturday</p> <input type="text" name="gfbgbfappointments_translation[long_weeks][saturday]" value="<?php echo  $long_weeks['saturday']; ?>">
					</td>
				</tr>				
						
				<tr>
					<th scope="row">
						<label for="clear_time">Calendar heading month/year</label>
					</th>
					<td>
						<?php
							$january   = gfb_get_form_translated_data($form_id, 'january');
							$february  = gfb_get_form_translated_data($form_id, 'february');
							$march     = gfb_get_form_translated_data($form_id, 'march');
							$april     = gfb_get_form_translated_data($form_id, 'april');
							$may       = gfb_get_form_translated_data($form_id, 'may');
							$june      = gfb_get_form_translated_data($form_id, 'june');
							$july      = gfb_get_form_translated_data($form_id, 'july');
							$august    = gfb_get_form_translated_data($form_id, 'august');
							$september = gfb_get_form_translated_data($form_id, 'september');
							$october   = gfb_get_form_translated_data($form_id, 'october');
							$november  = gfb_get_form_translated_data($form_id, 'november');
							$december  = gfb_get_form_translated_data($form_id, 'december');
						?>
						<p class="gfb_translation_title">January</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[january]" value="<?php echo $january; ?>"><br>
						<p class="gfb_translation_title">February</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[february]" value="<?php echo $february; ?>"><br>
						<p class="gfb_translation_title">March</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[march]" value="<?php echo $march; ?>"><br>
						<p class="gfb_translation_title">April</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[april]" value="<?php echo $april; ?>"><br>
						<p class="gfb_translation_title">May</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[may]" value="<?php echo $may; ?>"><br>
						<p class="gfb_translation_title">June</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[june]" value="<?php echo $june; ?>"><br>
						<p class="gfb_translation_title">July</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[july]" value="<?php echo $july; ?>"><br>
						<p class="gfb_translation_title">August</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[august]" value="<?php echo $august; ?>"><br>
						<p class="gfb_translation_title">September</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[september]" value="<?php echo $september; ?>"><br>
						<p class="gfb_translation_title">October</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[october]" value="<?php echo $october; ?>"><br>
						<p class="gfb_translation_title">November</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[november]" value="<?php echo $november; ?>"><br>
						<p class="gfb_translation_title">December</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[december]" value="<?php echo $december; ?>">
						
						<p class="description">Shortcode to use: [year]</p>
					</td>
				</tr>

				
				<tr>
					<th scope="row">
						<label for="clear_time">Calendar slots date</label>
					</th>
					<td>
						<?php
							$slots_january   = gfb_get_form_translated_data($form_id, 'slots_january');
							$slots_february  = gfb_get_form_translated_data($form_id, 'slots_february');
							$slots_march     = gfb_get_form_translated_data($form_id, 'slots_march');
							$slots_april     = gfb_get_form_translated_data($form_id, 'slots_april');
							$slots_may       = gfb_get_form_translated_data($form_id, 'slots_may');
							$slots_june      = gfb_get_form_translated_data($form_id, 'slots_june');
							$slots_july      = gfb_get_form_translated_data($form_id, 'slots_july');
							$slots_august    = gfb_get_form_translated_data($form_id, 'slots_august');
							$slots_september = gfb_get_form_translated_data($form_id, 'slots_september');
							$slots_october   = gfb_get_form_translated_data($form_id, 'slots_october');
							$slots_november  = gfb_get_form_translated_data($form_id, 'slots_november');
							$slots_december  = gfb_get_form_translated_data($form_id, 'slots_december');
						?>
						<p class="gfb_translation_title">January</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_january]" value="<?php echo $slots_january; ?>"><br>
						<p class="gfb_translation_title">February</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_february]" value="<?php echo $slots_february; ?>"><br>
						<p class="gfb_translation_title">March</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_march]" value="<?php echo $slots_march; ?>"><br>
						<p class="gfb_translation_title">April</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_april]" value="<?php echo $slots_april; ?>"><br>
						<p class="gfb_translation_title">May</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_may]" value="<?php echo $slots_may; ?>"><br>
						<p class="gfb_translation_title">June</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_june]" value="<?php echo $slots_june; ?>"><br>
						<p class="gfb_translation_title">July</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_july]" value="<?php echo $slots_july; ?>"><br>
						<p class="gfb_translation_title">August</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_august]" value="<?php echo $slots_august; ?>"><br>
						<p class="gfb_translation_title">September</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_september]" value="<?php echo $slots_september; ?>"><br>
						<p class="gfb_translation_title">October</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_october]" value="<?php echo $slots_october; ?>"><br>
						<p class="gfb_translation_title">November</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_november]" value="<?php echo $slots_november; ?>"><br>
						<p class="gfb_translation_title">December</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[slots_december]" value="<?php echo $slots_december; ?>">
						
						<p class="description">Shortcodes to use: [day], [year]</p>
					</td>
				</tr>				
				
				<tr>
					<th scope="row">
						<label for="clear_time">Date & Time</label>
					</th>
					<td>
						<?php
							$date_time_january   = gfb_get_form_translated_data($form_id, 'date_time_january');
							$date_time_february  = gfb_get_form_translated_data($form_id, 'date_time_february');
							$date_time_march     = gfb_get_form_translated_data($form_id, 'date_time_march');
							$date_time_april     = gfb_get_form_translated_data($form_id, 'date_time_april');
							$date_time_may       = gfb_get_form_translated_data($form_id, 'date_time_may');
							$date_time_june      = gfb_get_form_translated_data($form_id, 'date_time_june');
							$date_time_july      = gfb_get_form_translated_data($form_id, 'date_time_july');
							$date_time_august    = gfb_get_form_translated_data($form_id, 'date_time_august');
							$date_time_september = gfb_get_form_translated_data($form_id, 'date_time_september');
							$date_time_october   = gfb_get_form_translated_data($form_id, 'date_time_october');
							$date_time_november  = gfb_get_form_translated_data($form_id, 'date_time_november');
							$date_time_december  = gfb_get_form_translated_data($form_id, 'date_time_december');
						?>
						<p class="gfb_translation_title">January</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_january]" value="<?php echo $date_time_january; ?>"><br>
						<p class="gfb_translation_title">February</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_february]" value="<?php echo $date_time_february; ?>"><br>
						<p class="gfb_translation_title">March</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_march]" value="<?php echo $date_time_march; ?>"><br>
						<p class="gfb_translation_title">April</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_april]" value="<?php echo $date_time_april; ?>"><br>
						<p class="gfb_translation_title">May</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_may]" value="<?php echo $date_time_may; ?>"><br>
						<p class="gfb_translation_title">June</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_june]" value="<?php echo $date_time_june; ?>"><br>
						<p class="gfb_translation_title">July</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_july]" value="<?php echo $date_time_july; ?>"><br>
						<p class="gfb_translation_title">August</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_august]" value="<?php echo $date_time_august; ?>"><br>
						<p class="gfb_translation_title">September</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_september]" value="<?php echo $date_time_september; ?>"><br>
						<p class="gfb_translation_title">October</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_october]" value="<?php echo $date_time_october; ?>"><br>
						<p class="gfb_translation_title">November</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_november]" value="<?php echo $date_time_november; ?>"><br>
						<p class="gfb_translation_title">December</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[date_time_december]" value="<?php echo $date_time_december; ?>">
						
						<p class="description">Shortcodes to use: [week_long], [day], [year], [time]</p>
					</td>
				</tr>				

				<tr>
					<th scope="row">
						<label for="clear_time">AM/PM</label>
					</th>
					<td>
						<?php
							$am  = gfb_get_form_translated_data($form_id, 'am');
							$pm  = gfb_get_form_translated_data($form_id, 'pm');
						?>
						<p class="gfb_translation_title">Am time</p> <input type="text" name="gfbgbfappointments_translation[am]" value="<?php echo $am; ?>"><br>
						<p class="gfb_translation_title">Pm time</p> <input type="text" name="gfbgbfappointments_translation[pm]" value="<?php echo $pm; ?>"><br>
					</td>
				</tr>				

				
				
				<tr>
					<th scope="row">
						<label for="clear_time">Capacity</label> 
					</th>
					<td>
						<?php
							$space   = gfb_get_form_translated_data($form_id, 'space');
							$spaces  = gfb_get_form_translated_data($form_id, 'spaces');
						?>					
						<p class="gfb_translation_title">Is one</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[space]" value="<?php echo $space; ?>"><br>
						<p class="gfb_translation_title">Is greater than one</p> <input type="text" class="regular-text" name="gfbgbfappointments_translation[spaces]" value="<?php echo $spaces; ?>"><br>
					</td>
				</tr>			
				
		
				<tr>
					<th scope="row">
						<label for="clear_time">Add to calendar links title</label> 
					</th>
					<td>
						<?php
							$client_service      = gfb_get_form_translated_data($form_id, 'client_service');
							$provider_service    = gfb_get_form_translated_data($form_id, 'provider_service');
						?>					
						<p>Client service title. Shortcodes to use: [service_name], [provider_name]</p>  
						<input type="text" class="regular-text" name="gfbgbfappointments_translation[client_service]" value="<?php echo $client_service; ?>"><br>	
						
						<p>Provider service title. Shortcodes to use: [service_name], [client_name]</p> 
						<input type="text" class="regular-text" name="gfbgbfappointments_translation[provider_service]" value="<?php echo $provider_service; ?>"><br>	
				
					</td>
				</tr>
		
				<tr>
					<th scope="row">
						<label for="clear_time">Appointment cost</label> 
					</th>
					<td>
						<?php
							$app_cost_text = gfb_get_form_translated_data($form_id, 'app_cost_text');
						?>					
						<input type="text" class="regular-text" name="gfbgbfappointments_translation[app_cost_text]" value="<?php echo $app_cost_text; ?>"><br>	
					</td>
				</tr>	
		
				<tr>
					<th scope="row">
						<label for="clear_time">Validation messages</label> 
					</th>
					<td>
						<?php
							$error_required           = gfb_get_form_translated_data($form_id, 'error_required');
							$error_reached_max        = gfb_get_form_translated_data($form_id, 'error_reached_max');
							$error_required_date      = gfb_get_form_translated_data($form_id, 'error_required_date');
							$error_max_bookings       = gfb_get_form_translated_data($form_id, 'error_max_bookings');
							$error_required_service   = gfb_get_form_translated_data($form_id, 'error_required_service');
							$error_booked_date        = gfb_get_form_translated_data($form_id, 'error_booked_date');
							$error_date_valid         = gfb_get_form_translated_data($form_id, 'error_date_valid');
							$error_slot_valid         = gfb_get_form_translated_data($form_id, 'error_slot_valid');
							$error_required_slot      = gfb_get_form_translated_data($form_id, 'error_required_slot');
							$error_services_form      = gfb_get_form_translated_data($form_id, 'error_services_form');
							$error_service_valid      = gfb_get_form_translated_data($form_id, 'error_service_valid');
							$error_required_provider  = gfb_get_form_translated_data($form_id, 'error_required_provider');
							$error_providers_service  = gfb_get_form_translated_data($form_id, 'error_providers_service');
							$error_no_services        = gfb_get_form_translated_data($form_id, 'error_no_services');
							$calendar_theme_color        = gfb_get_form_translated_data($form_id, 'calendar_theme_color');
						?>						
						<p># Field required</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_required]" value="<?php echo $error_required; ?>"><br>
						
						<p># Date maximum bookings reached. Shortcode to use: [date]</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_reached_max]" value="<?php echo $error_reached_max; ?>"><br>						
						
						<p># Date not selected</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_required_date]" value="<?php echo $error_required_date; ?>"><br>						
												
						<p># Date max bookings. Shortcodes to use: [total], [date]</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_max_bookings]" value="<?php echo $error_max_bookings; ?>"><br>						
						
						<p># Service not selected</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_required_service]" value="<?php echo $error_required_service; ?>"><br>						
					
						<p># Client already booked date. Shortcode to use: [date]</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_booked_date]" value="<?php echo $error_booked_date; ?>"><br>						
								
						<p># Date not valid. Shortcode to use: [date]</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_date_valid]" value="<?php echo $error_date_valid; ?>"><br>						
							
						<p># Time slot not valid. Shortcode to use: [date]</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_slot_valid]" value="<?php echo $error_slot_valid; ?>"><br>						
						
						<p># Time slot required</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_required_slot]" value="<?php echo $error_required_slot; ?>"><br>						
				
						<p># Services field not added to form</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_services_form]" value="<?php echo $error_services_form; ?>"><br>						
					
						<p># Service is not valid</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_service_valid]" value="<?php echo $error_service_valid; ?>"><br>						
						
						<p># Provider not selected</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_required_provider]" value="<?php echo $error_required_provider; ?>"><br>						
												
						<p># Providers service not valid</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_providers_service]" value="<?php echo $error_providers_service; ?>"><br>						
					
						<p># No services found</p>
						<input type="text" class="large-text" name="gfbgbfappointments_translation[error_no_services]" value="<?php echo $error_no_services; ?>"><br>						
													
					</td>
				</tr>				
				<tr>
					<th scope="row">
						<label for="clear_time">Calendar Theme</label> 
					</th>
					<td>
						<div class="form-element">
							<input type="color" name="gfbgbfappointments_translation[calendar_theme_color]" class="large-text" value="<?php if( !empty( $calendar_theme_color) ) { echo $calendar_theme_color; } else { echo '#00d0ac'; } ?>" />
						</div>
					</td>
				</tr>
				
			</table>
			<input type="hidden" id="form_id" name="form_id" value="<?php echo esc_attr( $form_id ); ?>" />
			<input type="submit" id="gform_save_gappointments" name="gform_save_gappointments" value="<?php _e( 'Update General Settings', 'gravityforms' ); ?>" class="button-primary gfbutton" />
		
				
		<?php  /* GFFormSettings::page_footer(); */
	
	
	}
	public function get_conditional_logic_fields() {
		
		$form   = $this->get_current_form();
		$fields = array();
		foreach ( $form['fields'] as $field ) {
			if ( $field->is_conditional_logic_supported() ) {
				$inputs = $field->get_entry_inputs();

				if ( $inputs ) {
					$choices = array();

					foreach ( $inputs as $input ) {
						if ( rgar( $input, 'isHidden' ) ) {
							continue;
						}
						$choices[] = array(
							'value' => $input['id'],
							'label' => GFCommon::get_label( $field, $input['id'], true )
						);
					}

					if ( ! empty( $choices ) ) {
						$fields[] = array( 'choices' => $choices, 'label' => GFCommon::get_label( $field ) );
					}

				} else {
					$fields[] = array( 'value' => $field->id, 'label' => GFCommon::get_label( $field ) );
				}

			}
		}

		return $fields;
	}

	/**
	 * Evaluate the conditional logic.
	 *
	 * @param array $form The form currently being processed.
	 * @param array $entry The entry currently being processed.
	 *
	 * @return bool
	 */
	public function is_custom_logic_met( $form, $entry ) {
		if ( $this->is_gravityforms_supported( '2.0.7.4' ) ) {
			// Use the helper added in Gravity Forms 2.0.7.4.

			return $this->is_simple_condition_met( 'custom_logic', $form, $entry );
		}

		// Older version of Gravity Forms, use our own method of validating the simple condition.
		$settings = $this->get_form_settings( $form );

		$name       = 'custom_logic';
		$is_enabled = rgar( $settings, $name . '_enabled' );

		if ( ! $is_enabled ) {
			// The setting is not enabled so we handle it as if the rules are met.

			return true;
		}

		// Build the logic array to be used by Gravity Forms when evaluating the rules.
		$logic = array(
			'logicType' => 'all',
			'rules'     => array(
				array(
					'fieldId'  => rgar( $settings, $name . '_field_id' ),
					'operator' => rgar( $settings, $name . '_operator' ),
					'value'    => rgar( $settings, $name . '_value' ),
				),
			)
		);

		return GFCommon::evaluate_conditional_logic( $logic, $form, $entry );
	}

	


	// # HELPERS -------------------------------------------------------------------------------------------------------

	/**
	 * The feedback callback for the 'mytextbox' setting on the plugin settings page and the 'mytext' setting on the form settings page.
	 *
	 * @param string $value The setting value.
	 *
	 * @return bool
	 */
	public function is_valid_setting( $value ) {
		return strlen( $value ) < 10;
	}
	/**
	*GFB- Admin Setting Page 
	**/
	
	public function save_gfbgbfappointments_form_page($form_id) {
		//$form = GFAPI::get_form( $form_id );
		if( isset( $_POST['gfbgbfappointments_translation'] ) ) {
			$form = GFAPI::get_form( $form_id );
			$form['gfbgbfappointments_translation'] = rgpost( 'gfbgbfappointments_translation' );
			$form['gfb_service_category']       = rgpost( 'gfb_service_category' );
			GFAPI::update_form( $form, $form_id );			
			if ( isset($_POST['gform_save_gappointments']) ) { ?>
				<div class="alert gforms_note_success" role="alert">Settings updated.</div>
		<?php }
		}
	}
}


/* $debug_tags = array();
add_action( 'all', function ( $tag ) {
    global $debug_tags;
    if ( in_array( $tag, $debug_tags ) ) {
        return;
    }
    echo "<pre>" . $tag . "</pre>";
    $debug_tags[] = $tag;
} ); */


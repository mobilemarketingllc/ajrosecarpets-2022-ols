<?php
/* CONSTANTS */
define( 'GFB_VERSION', '2.3' );
define( 'GFB_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'GFB_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'GFB_ASSET', GFB_URL . '/assets/' );

/* Includes */
define( 'GFB_INCLUDES', GFB_DIR . '/includes' );
define( 'GFB_INCLUDES_CLASS', GFB_INCLUDES . '/classes/' );
define( 'GFB_INCLUDES_SHORTCODE', GFB_INCLUDES . '/shortcodes/' );
/* Admin */
define( 'GFB_ADMIN_DIR', GFB_DIR . '/admin/' );
define( 'GFB_ADMIN_ASSET', GFB_URL . '/admin/assets/' );
define( 'GFB_ADMIN_TAB', GFB_ADMIN_DIR . 'tabs/' );
define( 'GFB_ADMIN_CLASS', GFB_ADMIN_DIR . 'classes/' );
define( 'GFB_ADMIN_CORE', GFB_ADMIN_DIR . 'core/' );
define( 'GFB_ADMIN_FPDF', GFB_ADMIN_DIR . 'fpdf/' );
define( 'GFB_AJAX_LOADER', GFB_ADMIN_ASSET . 'images/bars.svg' );
define( 'GFB_INLINE_LOADER', GFB_ADMIN_ASSET . 'images/inline-loader.svg' );
define( 'GFB_PLUGIN', __FILE__);
define( 'GFB_PATH_URL',    plugins_url('', GFB_PLUGIN));
define( 'GFB_ASSETS_URL',  GFB_PATH_URL . '/gfb-booking-fields/assets/');

require_once( GFB_ADMIN_CORE . 'Register.php' );
require_once( GFB_ADMIN_CLASS . 'gravityformbooking.appointment.cls.php' );

$res = GFB_Register::gfbRegisterPlugin();

if ( ! class_exists( 'gfb_bookings_addon')){
	
	class gfb_bookings_addon
	{
		private $assets_ver = GFB_VERSION;

		// CONSTANTS //
		public function gfb_define_constants()
		{	
			if (!defined('GFB_GFORM_AFFILIATE_URL')) define('GFB_GFORM_AFFILIATE_URL',  'https://www.gravityforms.com/');
		
		}

		function __construct() {
			add_action('init',array($this, 'gfb_plugin_init'));
			add_action('init', array($this, 'gfb_init'));
			add_action('gform_loaded', array( $this, 'gfb_load' ), 5 );
			add_action('rest_api_init',array($this, 'gfb_pushnotif'));			
			add_filter( 'gform_pre_render', array( $this, 'add_default_value_to_fields'), 99, 3 );
			add_action( 'admin_init', array( $this, 'gfb_woocommerce_prevent_admin_access'), 1); //gfb-173			
		}

		public function gfb_woocommerce_prevent_admin_access() {
			add_filter( 'woocommerce_prevent_admin_access', '__return_false', 100 );
		}

		
		public function add_default_value_to_fields( $form, $ajax, $field_values ) {			

			if ( is_user_logged_in() ) {

				$user_id = get_current_user_id();
				$user_details = get_user_by('ID',$user_id);
				$first_name = get_user_meta($user_id, 'first_name', true);
                $last_name = get_user_meta($user_id, 'last_name', true);
			
				$fields = $form['fields'];				

				$results = array_filter($fields, function( $item ) {
				   return ($item->type === 'gfb_appointment_calendar' );
				});

				if ( $results ) {

					foreach ($fields as &$field) {					

						if ( $field->type == 'email' ) {						
		                	$field->defaultValue = $user_details->user_email;
		                }

		                if ( $field->type == 'name' ) {	                	

		                	$field->inputs[1]['defaultValue'] = $first_name;
		                	$field->inputs[3]['defaultValue'] = $last_name;

		                }
					}
				}
				
			}

			return $form;
		}

		/**
		* INITIALIZE 
		**/
		public function gfb_init() {
			$this->gfb_define_constants();			

			if ($this->gfb_is_gravity_forms_active()) {

				// Helper Functions
				require_once('gfb-booking-fields/includes/functions.php');

				// Styles & Scripts
				add_action('wp_loaded', array($this, 'gfb_frontend_styles_scripts'));
				add_action('gform_enqueue_scripts', array($this, 'gfb_gform_enqueue_form_scripts'), 10, 2);
				$this->gfb_ajax_actions();
				// GFB CUSTOM FIELDS
				require_once('gfb-booking-fields/gbf-fields/gbf-form-settings.php');
				require_once('gfb-booking-fields/gbf-fields/gbf-calendar.php');
				require_once('gfb-booking-fields/gbf-fields/gbf-services-category.php');
				require_once('gfb-booking-fields/gbf-fields/gbf-booking-services.php');
				require_once('gfb-booking-fields/gbf-fields/gbf-booking-providers.php');
				require_once('gfb-booking-fields/gbf-fields/gbf-booking-calendar.php');
				require_once('gfb-booking-fields/gbf-fields/gbf-booking-cost.php');
			} else {
				add_action('admin_notices', array($this, 'gfb_gravity_not_active_message'));
			}
		}
		
		/**  
		* Plugin Init
		**/
		public function gfb_plugin_init() {
			load_plugin_textdomain( 'gfb', false, dirname(plugin_basename(__FILE__)).'/languages/' );
		}
		
		/**  
		* Push Notif Route Register
		**/
		public function gfb_pushnotif () {
			register_rest_route( 'gfb', '/pushnotif', array(
				'methods' => 'POST',
				'callback' =>  array($this, 'gfb_push_notif_handler'),
				'permission_callback' => '__return_true'
			) );
		}
		
		/**  
		* Push Notif Handler
		**/
		
		public function gfb_push_notif_handler() {
			$headers = getallheaders();
			$gfbAppointment = new GravityFormBookingAppointments();
			
			if(get_option('gfb_gc_2way_sync')==1)
			{
				$msg = $gfbAppointment->syncAppointmentWithGoogleCal($headers);
				$today = date("Y_m_d"); 
				$logfile = $today."_log.txt"; 
				$notifFile = $today."_notif_log.txt"; 
				$saveLocation=GFB_DIR.'/logs/'.$logfile;
				$saveLocationNotif=GFB_DIR.'/logs/'.$notifFile;
				//print_r(@fopen($saveLocation, "a"));exit;
				if(!$handle = @fopen($saveLocation, "a")) {
				  exit;
				}
				else {
					if (@fwrite($handle,"$msg\r\n") === FALSE) {
					   exit;
					}
				
				  @fclose($handle);
				}
				
				if(!$handle = @fopen($saveLocationNotif, "a")) {
				  exit;
				}
				else {
					if (@fwrite($handle,json_encode($headers)."\r\n") === FALSE) {
					   exit;
					}
				
				  @fclose($handle);
				}
			}
		}
		
		/**
		* IS GFB ACTIVE
		**/
		public function gfb_is_gravity_forms_active()
		{
			return class_exists('GFCommon');
		}

		/**
		* GFB NOT ACTIVE MESSAGE
		**/
		public function gfb_gravity_not_active_message()
		{
			$message = sprintf(
				__('Gravity Forms is required in order to use gravity booking fields. Activate it now or %1$spurchase it today!%2$s', 'gfb_appointments'),
				'<a href="' . GFB_GFORM_AFFILIATE_URL . '" target="_blank">',
				'</a>'
			);
			echo '<div id="gfb_error" class="error is-dismissible"><p>' . $message . '</p></div>';
		}

		/**
		 * Frontend scripts & styles
		 */
		public function gfb_frontend_styles_scripts()
		{
			// GF Chosen
			wp_enqueue_script('gform_chosen');
			// Frontend css
			wp_enqueue_style('gfb_appointments_calendar_css', plugins_url('gfb-booking-fields/assets/calendar.css', __FILE__), false, $this->assets_ver);

			// Schedule css
			wp_enqueue_style('gfb_appointments_schedule_css',  plugins_url('gfb-booking-fields/assets/schedule.css', __FILE__), false, $this->assets_ver);

			// Frontend grid css
			wp_enqueue_style('gfb_appointments_calendar_css_grid', plugins_url('gfb-booking-fields/assets/grid.css', __FILE__), false, $this->assets_ver);

			// WP Dashicons
			wp_enqueue_style('dashicons');

			// Font awesome
			wp_enqueue_style('gfb-gbf-font-awesome', plugins_url('gfb-booking-fields/assets/font-awesome.min.css', __FILE__), false, $this->assets_ver);

			// Schedule scripts
			wp_enqueue_script('gfb_appointments_schedule_script', plugins_url('gfb-booking-fields/assets/schedule.js', __FILE__), false, $this->assets_ver, true);

			// AJAX: Services
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_calendar_services_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // gfb_calendar_services_obj = for ajax reference
			// Main javascipt file
			wp_enqueue_script('gfb_appointments_calendar_script', plugins_url('gfb-booking-fields/assets/main.js', __FILE__), false, $this->assets_ver, true);
			// AJAX: staff
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_staff_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // gfb_calendar_services_obj = for ajax reference
			
			// AJAX: Providers
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_calendar_providers_obj', array('ajax_url' => admin_url('admin-ajax.php'), 'calendar_availability' => get_option( 'gfb_calendar_availability' ), 'global_service' => get_option( 'gfb_global_service' ), 'global_service_provider' => get_option( 'gfb_global_service_provider' ) ) ); // gfb_calendar_services_obj = for ajax reference

			// AJAX: Next Month
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_calendar_next_month_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // gfb_calendar_next_month_obj = for ajax reference

			// AJAX: Prev Month
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_calendar_prev_month_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // gfb_calendar_prev_month_obj = for ajax reference

			// AJAX: Time Slots
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_calendar_time_slots_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // gfb_calendar_time_slots_obj = for ajax reference

			// AJAX: Update Schedule
			wp_localize_script('gfb_appointments_schedule_script', 'gfb_calendar_schedule_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // gfb_calendar_time_slots_obj = for ajax reference

			// AJAX: Cancel/Update Appointment
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_update_appointment_status_obj', array('ajax_url' => admin_url('admin-ajax.php'))); // bb_appointments_calendar_obj = for ajax reference
			
			//For user ajax set appointment status to pending
			wp_localize_script('gfb_appointments_calendar_script', 'gfb_user_set_appointment_pending_obj', array('ajax_url' => admin_url('admin-ajax.php')) );
		}

		/**
		 * Add cost to total field
		 */
		public function gfb_gform_enqueue_form_scripts($form, $is_ajax)
		{

			if (gfb_field_type_exists($form, 'gfb_appointment_calendar') && gfb_field_type_exists($form, 'total')) {
				// Appointment Total Script
				wp_enqueue_script('gfb_appointments_calendar_total', plugins_url('gfb-booking-fields/assets/total.js', __FILE__), false, $this->assets_ver, true);
			}

			if (gfb_field_type_exists($form, 'gfb_appointment_cost')) {
				// Main javascipt file
				wp_enqueue_script('gform_gravityforms');
			}
			
		}

		/**
		 * Ajax Actions
		 */
		public function gfb_ajax_actions()
		{
			// Select Service
			add_action('wp_ajax_gfb_calendar_select_service', array($this, 'gfb_calendar_select_service'));
			add_action('wp_ajax_nopriv_gfb_calendar_select_service', array($this, 'gfb_calendar_select_service'));

			// Select Provider
			add_action('wp_ajax_gfb_calendar_select_provider', array($this, 'gfb_calendar_select_provider'));
			add_action('wp_ajax_nopriv_gfb_calendar_select_provider', array($this, 'gfb_calendar_select_provider'));

			// Next month
			add_action('wp_ajax_gfb_calendar_next_month', array($this, 'gfb_calendar_next_month'));
			add_action('wp_ajax_nopriv_gfb_calendar_next_month', array($this, 'gfb_calendar_next_month'));

			// Prev month
			add_action('wp_ajax_gfb_calendar_prev_month', array($this, 'gfb_calendar_prev_month'));
			add_action('wp_ajax_nopriv_gfb_calendar_prev_month', array($this, 'gfb_calendar_prev_month'));

			// Generate Time Slots
			add_action('wp_ajax_gfb_calendar_time_slots', array($this, 'gfb_calendar_time_slots'));
			add_action('wp_ajax_nopriv_gfb_calendar_time_slots', array($this, 'gfb_calendar_time_slots')); //
			
			// Staff 
			add_action( 'wp_ajax_gfb_select_service',  array($this, 'gfb_select_service'));
			add_action( 'wp_ajax_nopriv_gfb_select_service', array($this, 'gfb_select_service'));
			
		}

		/**
		* Array Column Function 
		**/
		public function gfb_array_column(array $input, $columnKey, $indexKey = null) {
			$array = array();
			foreach ($input as $value) {
				if ( !array_key_exists($columnKey, $value)) {
					trigger_error("Key \"$columnKey\" does not exist in array");
					return false;
				}
				if (is_null($indexKey)) {
					$array[] = $value[$columnKey];
				}
				else {
					if ( !array_key_exists($indexKey, $value)) {
					trigger_error("Key \"$indexKey\" does not exist in array");
					return false;
				}
				if ( ! is_scalar($value[$indexKey])) {
					trigger_error("Key \"$indexKey\" does not contain scalar value");
					return false;
				}
				$array[$value[$indexKey]] = $value[$columnKey];
				}
			}
			return $array;
		} 
		
		/**
		* Get Services 
		**/
		public function gfb_get_services_options($service_id)
		{
			// The Query
			$options = '';
			$options .= '<option value="">' . esc_html__( 'Select Service', 'gfb' ) . '</option>';
			global $wpdb;
				$service_tbl = $wpdb->prefix . 'gfb_services_mst';			
				$service_result = $wpdb->get_results( 'SELECT service_id, service_title, category_id FROM '.$service_tbl.' WHERE is_deleted=0 AND category_id='.trim($service_id).' ORDER BY service_title ASC', ARRAY_A);				
			
			foreach ($service_result as $resultData):
						$selected = $value == $resultData['service_id'] ? ' selected="selected"' : '';
						$options .= '<option value="'.$resultData['service_id'].'"'.$selected.'>'.$resultData['service_title'].'</option>';
			endforeach; 

			return $options;
		}
		
		/**
		* Get Staff 
		**/
		public function gfb_get_staff_options($service_id)
		{
			// The Query
			$options = '';
			$options .= '<option value="">' . esc_html__( 'Select Staff', 'gfb' ) . '</option>';
			global $wpdb;
			$gfb_staff_service_mapping = $wpdb->prefix . "gfb_staff_service_mapping";
			$gfb_staff_mst = $wpdb->prefix . "gfb_staff_mst";
			$staffList = $wpdb->get_results( "SELECT sm.staff_id,  sm.service_price ,s.staff_name FROM ".$gfb_staff_service_mapping." as sm LEFT JOIN ".$gfb_staff_mst." as s ON sm.staff_id = s.staff_id WHERE sm.service_id= '".$service_id."' AND sm.is_deleted=0 ORDER BY s.staff_name asc", ARRAY_A );					
			foreach ($staffList as $resultData):
						$selected = $value == $resultData['staff_id'] ? ' selected="selected"' : '';
						$options .= '<option value="'.$resultData['staff_id'].'"'.$selected.'>'.$resultData['staff_name'].'</option>';
			endforeach; 
			return $options; 
		}
		
		
		/**
		 * AJAX: Select Service
		 */
		public function gfb_calendar_select_service()
		{
			$response = array(
				'service_categories' => '<option value="">' . esc_html__( 'Service not found', 'gfb' ) . '</option>',
			);

			$category_service_id = isset($_POST['category_service_id']) ? (int) $_POST['category_service_id'] : 0;
			$form_id = isset($_POST['form_id']) ? (int) $_POST['form_id'] : 0;

			if ($category_service_id !="") {
				$response['service_categories'] =  $this->gfb_get_services_options($category_service_id);
				wp_send_json_success($response);
				wp_die();
			} else {
				wp_send_json_success($response);
				wp_die();
			}
		}

		 
		/**
		 * AJAX: Select Staff
		 */ 
		public function gfb_select_service()
		{
			$response = array(
				'staff' => '<option value="">' . esc_html__( 'Service not found', 'gfb' ) . '</option>',
			);
			  
			$service_id = isset($_POST['service_id']) ? (int) $_POST['service_id'] : 0;
			$form_id = isset($_POST['form_id']) ? (int) $_POST['form_id'] : 0;

			if ($service_id !="") {
				$response['staff'] =  $this->gfb_get_staff_options($service_id);
				wp_send_json_success($response);
				wp_die();
			} else {
			   wp_send_json_success($response);
				wp_die();
			} 
		}
		
		/**
		 * AJAX: Select Provider
		 */
		public function gfb_calendar_select_provider() {
			$service_id  = isset($_POST['service_id'])  ? (int) $_POST['service_id']  : 0;
			$provider_id  = isset($_POST['provider_id'])  ? (int) $_POST['provider_id']  : 0;
			$form_id = isset($_POST['form_id']) ? (int) $_POST['form_id'] : 0;
			
			if ($service_id !=="") { 
				$current_date = gfb_current_date_with_timezone();
				$gfb_calendar = new gfb_calendar($form_id, $current_date->format('n'), $current_date->format('Y'), $service_id, $provider_id);
				echo $gfb_calendar->show();
			} else {
				wp_die('Something went wrong.');
			}

			wp_die(); // Don't forget to stop execution afterward.
		}


		/**
		 * AJAX: Calendar Previous Month
		 */
		public function gfb_calendar_prev_month()
		{
			$current_date   = isset($_POST['current_month']) ? esc_html($_POST['current_month']) : '';
			$service_id     = isset($_POST['service_id'])    ? (int) $_POST['service_id']        : 0;
			$provider_id     = isset($_POST['provider_id'])    ? (int) $_POST['provider_id']        : 0;
			$form_id = isset($_POST['form_id']) ? (int) $_POST['form_id'] : 0;
			
			if ($service_id !==""  && $provider_id !=="" && gfb_valid_year_month_format($current_date)) { 	
				$timezone = gfb_time_zone();

				$date = new DateTime($current_date, new DateTimeZone($timezone));
				$date->modify('-1 month');

				$gfb_calendar = new gfb_calendar($form_id, $date->format('n'), $date->format('Y'), $service_id, $provider_id);
				echo $gfb_calendar->show();
			} else {
				wp_die("Something went wrong.");
			}

			wp_die(); // Don't forget to stop execution afterward.
		}

		/**
		 * AJAX: Calendar Next Month
		 */
		public function gfb_calendar_next_month()
		{	
			$current_date   = isset($_POST['current_month']) ? esc_html($_POST['current_month']) : '';
			$service_id     = isset($_POST['service_id'])    ? (int) $_POST['service_id']        : 0;
			$provider_id     = isset($_POST['provider_id'])    ? (int) $_POST['provider_id']        : 0;
			$form_id = isset($_POST['form_id']) ? (int) $_POST['form_id'] : 0;
			if ($service_id !="" && $provider_id !=="" && gfb_valid_year_month_format($current_date)) {
				$timezone = gfb_time_zone();

				$date = new DateTime($current_date, new DateTimeZone($timezone));
				$date->modify('+1 month');

				$gfb_calendar = new gfb_calendar($form_id, $date->format('n'), $date->format('Y'), $service_id, $provider_id);
				echo $gfb_calendar->show();
			} else {
				wp_die("Please Select Booking Services.");
			}

			wp_die(); // Don't forget to stop execution afterward.
		}

		/**
		 * AJAX: Generate Time Slots
		 */
		public function gfb_calendar_time_slots() {
			// Timezone
			$timezone = gfb_time_zone();
			
			// Service & Provider ID
			$current_date = isset($_POST['current_month']) ? esc_html($_POST['current_month']) : '';
			$service_id   = isset($_POST['service_id'])    ? (int) $_POST['service_id']        : 0;
			$provider_id   = isset($_POST['provider_id'])    ? (int) $_POST['provider_id']        : 0;
			$form_id = isset($_POST['form_id']) ? (int) $_POST['form_id'] : 0;
			if ($service_id !="" && gfb_valid_date_format($current_date)) {
				# ok
			} else {
				wp_die('Please Select Booking Services.');
			}
			
			// Date Caption
			$date = new DateTime($current_date, new DateTimeZone($timezone));
			
			// Generate Slots
			$gfb_calendar = new gfb_calendar($form_id, $date->format('n'), $date->format('Y'), $service_id, $provider_id);
			echo $gfb_calendar->calendar_time_slots($date);
			wp_die();
		}

		/* GFB Booking Addon */	
		public static function gfb_load() {
			if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
				return;
			}
			require_once( 'gfb-booking-fields/gbf-fields/gbf-form-settings.php' );
			GFAddOn::register( 'GFB_booking_addon' );
		}
	} // end class
	
}
/**
*load plugin
**/
new gfb_bookings_addon();

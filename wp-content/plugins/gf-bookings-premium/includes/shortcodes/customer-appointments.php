<?php

/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }
  
function gfb_customer_appointment_shortcode() {		

	if ( ! ( defined( 'REST_REQUEST' ) && REST_REQUEST ) ) {

		wp_enqueue_style('gfb_data-table-bootstrap-css', GFB_URL . '/gfb-booking-fields/assets/data-table/bootstrap.css');
		// Fiels For Bootstrap 4 DataTable
		wp_dequeue_script('gfb_appointments_calendar_script');
		wp_enqueue_script('gfb_data-table-jquery');
		wp_enqueue_script('gfb_data-table-dataTables');
		wp_enqueue_script('gfb_data-table-bootstrap4');
		wp_enqueue_script('gfb_data-table-responsive');
		
		if ( is_user_logged_in() ) {
			$user = wp_get_current_user();
			$customer_id= $user->ID;
			if ( in_array( 'gfb_customer_role', (array) $user->roles ) ) {
				global $wpdb;
				$customer_id = get_user_meta($customer_id,'customer_id',$single = false);
				if ( ! empty($customer_id) ) {
					$customer_id= $customer_id[0];
				} else {
					$customer_id = 0;
				}
				$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
				$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
				$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
				$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
				$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
				$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';	
				$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';	
				$users = $wpdb->prefix . 'users';
				
				$appointment_results = $wpdb->get_results('SELECT booking_ref_no,pm.payment_type,am.appointment_id, cm.customer_name, DATE_FORMAT(am.appointment_date, "%m/%d") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm.staff_name, CONCAT( DATE_FORMAT(tm.slot_start_time, "%h:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%h:%i") ) as time_slot, am.status FROM '.$gfb_appointments_mst.' AS am INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id LEFT JOIN '.$gfb_payments_mst.' as pm on pm.appointment_id=am.appointment_id INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id WHERE am.is_deleted=0 AND cm.customer_id="'.$customer_id.'" ORDER BY am.appointment_date desc, am.appointment_id desc');
				?>
				<table id="all-customer-appointments"  class="table table-striped table-bordered dt-responsive nowrap">
					<thead>
						<tr>
							<th class="nowrap"> Booking Number </th>
							<th class="nowrap"> Service </th>
							<th class="nowrap"> Staff Name </th>
							<th class="nowrap"> Appointment Date </th>
							<th class="nowrap"> Time </th>
							<th class="nowrap"> Status </th>
						</tr>
					</thead>

					<tbody>
			
					<?php 
					if ( ! empty( $appointment_results ) ) {
						foreach ($appointment_results as $appointment) {
							$appointment = json_decode(json_encode($appointment), true);
							?>
							<tr>
								<td nowrap><?= $appointment['booking_ref_no'];?></td>
								<td nowrap><?= $appointment['service_title'];?> </td>
								<td nowrap><?= $appointment['staff_name'];?></td>
								<td nowrap><?= $appointment['appointment_date'];?></td>
								<td nowrap><?= $appointment['time_slot'];?></td>
								<td nowrap>
								<?php 
								if ( $appointment['status'] == 1 ) {
									echo '<span class="gfb_badge badge-yellow">Pending</span>';
								} elseif ( $appointment['status'] == 2 ) {
									if ( $appointment["payment_type"]==3 ) {
										echo '<span class="gfb_badge badge-blue">Awaiting</span>';
									} else {
										echo '<span class="gfb_badge badge-blue">Awaiting</span>';
									}
								} elseif ( $appointment['status'] == 3 ) {
									echo '<span class="gfb_badge badge-red">Cancelled</span>';
								} elseif ( $appointment['status'] == 4 ) {
									echo '<span class="gfb_badge badge-green">Visited</span>';
								} else {

								}
								?>
								</td>
							</tr>
							<?php 
						}
					} else {
						?>
						<tr>
							<td class="nowrap" colspan="6" style="text-align: center;">No Appointments Found</td>
						</tr>
						<?php 
					}
					?>
					</tbody>
					<tfoot>
						<tr>
							<th class="nowrap">Booking Number</th>
							<th class="nowrap">Service</th>
							<th class="nowrap">Staff Name</th>
							<th class="nowrap">Appointment Date</th>
							<th class="nowrap">Time</th>
							<th class="nowrap">Status</th>
						</tr>
					</tfoot>
				</table>
		
				<script type="text/javascript">
				jQuery(document).ready(function() {
					jQuery('#all-customer-appointments').DataTable();
				});
				</script>

				<?php
			} else {
				?>
				<div class="popup-block-main">
					<div class="gfbAjaxLoader" id="gfb_loader_img">
						<img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
					</div>
					<div class="gfbUI">
						<?php
						$current_path = $_SERVER['REQUEST_URI'];
						echo "kindly login with customer id "; 
						wp_loginout($current_path);
						?>
					</div>
				</div>
				<?php
			} 
		} else {
			?>
			<div class="popup-block-main">
			  <div class="gfbAjaxLoader" id="gfb_loader_img"> <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" /> </div>
				<div class="gfbUI">
					<?php 
						$current_path = $_SERVER['REQUEST_URI'];
					echo"Please login to view your bookings "; wp_loginout($current_path);?>
				</div>
			</div>
			<?php
		}
	}
}

add_shortcode('customer-appointments', 'gfb_customer_appointment_shortcode');

add_action('init','crate_customer_appointment_page');
function crate_customer_appointment_page () {
	$mypost = get_option('customer_appointments_page');
	if(empty($mypost)){
		$PageContent = "[customer-appointments]";	
		$post_information = array(
			'post_title' =>  'Customer Appointments' ,
			'post_content' => $PageContent,
			'post_type' => 'page',
			'post_status' => 'publish'
		);
		$post_id = wp_insert_post( $post_information );
		update_option('customer_appointments_page',$post_id);
	}  
}

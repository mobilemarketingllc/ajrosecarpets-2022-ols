<?php

/*
Plugin Name: Gravity Forms Booking
Plugin URI: https://gravitymore.com/
Description: With this GravityForms addon you can implement complete booking and appointments system in your WordPress site.
Version: 9999.99
Update URI: https://api.freemius.com
Author: Gravity More
Author URI: https://gravitymore.com/
Text Domain: gfb 
*/
/* Exit if accessed directly */
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !function_exists( 'gfb_fs' ) ) {
    // Create a helper function for easy SDK access.
    function gfb_fs()
    {
        global  $gfb_fs ;
        
        if ( 'upgraded' == get_option( 'gfb_switch_to_new', 'no' ) || false == get_option( 'gfb_price_policy', false ) ) {
            $first_path = 'admin.php?page=gfb-report';
        } else {
            $first_path = 'admin.php?page=gravity-form-booking-menu';
        }
        
        
        if ( !isset( $gfb_fs ) ) {
            // Include Freemius SDK.
            require_once dirname( __FILE__ ) . '/freemius/start.php';
            $gfb_fs = fs_dynamic_init( array(
                'id'               => '6563',
                'slug'             => 'gf-bookings',
                'type'             => 'plugin',
                'public_key'       => 'pk_3217c2d06c8951e46d0289e1ae5b7',
                'is_premium'       => true,
                'is_premium_only'  => true,
                'has_addons'       => false,
                'has_paid_plans'   => true,
                'is_org_compliant' => false,
                'trial'            => array(
                'days'               => 14,
                'is_require_payment' => true,
            ),
                'has_affiliation'  => 'selected',
                'menu'             => array(
                'slug'       => 'gfb',
                'first-path' => $first_path,
            ),
                'is_live'          => true,
            ) );
        }
        
        return $gfb_fs;
    }
    
    // Init Freemius.
    gfb_fs();
    // Signal that SDK was initiated.
    do_action( 'gfb_fs_loaded' );
}

/**
 * Notice for admin For New Version
 */

if ( !function_exists( 'gfb_new_ver_plugin_notice' ) ) {
    add_action( 'admin_notices', 'gfb_new_ver_plugin_notice' );
    function gfb_new_ver_plugin_notice()
    {
        $headline = esc_html__( 'Introducing Gravity Booking v' . GFB_VERSION . ' - ', 'gfb' );
        $message = sprintf( esc_html__( 'New features, an easy-to-use user interface, and a whole lot more! Please read our %sdocumentation%s if you have any questions, concerns, or problems regarding the recent changes', 'gfb' ), '<a href="https://gravitymore.com/documentation/bookings-for-gravity-forms/" target="_blank">', '</a>' );
        $message_2 = esc_html__( '⚠️IMPORTANT: Upgrading to Gravity Booking Beta v2.3 will reset your previous staff, services, appointments, and calendar settings! You will need to reconfigure your settings after the update.', 'gfb' );
        $tech_text = 'documentation';
        $message_3 = esc_html__( 'I want to switch to', 'gfb' );
        $switchTo = __( 'Upgraded Version' );
        $message_4 = esc_html__( 'I don\'t want', 'gfb' );
        $user_id = get_current_user_id();
        $user = get_user_by( 'id', $user_id );
        if ( in_array( 'administrator', $user->roles ) ) {
            if ( !get_option( 'gfb_switch_to_new' ) && false != get_option( 'gfb_price_policy', false ) ) {
                printf(
                    '<div class="notice notice-warning"><p style="font-size: 14px"><strong>%1$s</strong> %2$s.<br/><br/><strong style="color:red">%3$s</strong> <br/><br/> %4$s <strong><a href="' . esc_url( admin_url( 'index.php?switch=upgraded' ) ) . '">%5$s</a></strong> %6$s <strong><a href="' . esc_url( admin_url( 'index.php?switch=no' ) ) . '">%7$s</a></strong></p></div>',
                    wp_kses_post( $headline ),
                    wp_kses_post( $message ),
                    wp_kses_post( $message_2 ),
                    wp_kses_post( $message_3 ),
                    wp_kses_post( $switchTo ),
                    esc_html__( 'or', 'gfb' ),
                    wp_kses_post( $message_4 )
                );
            }
        }
    }

}

/**
 * Check for click event. 
 */

if ( !function_exists( 'gfb_plugin_notice_dismissed' ) ) {
    add_action( 'admin_init', 'gfb_plugin_notice_dismissed' );
    function gfb_plugin_notice_dismissed()
    {
        $user_id = get_current_user_id();
        
        if ( isset( $_GET['switch'] ) && 'no' == sanitize_text_field( $_GET['switch'] ) ) {
            update_option( 'gfb_switch_to_new', 'no' );
            wp_safe_redirect( admin_url( 'admin.php?page=gravity-form-booking-menu' ) );
            exit;
        }
    
    }

}

if ( !function_exists( 'gfb_switch_to_new' ) ) {
    function gfb_switch_to_new()
    {
        
        if ( isset( $_GET['switch'] ) && 'upgraded' === sanitize_text_field( $_GET['switch'] ) ) {
            update_option( 'gfb_switch_to_new', 'upgraded' );
            wp_safe_redirect( admin_url( 'admin.php?page=gfb-report' ) );
            exit;
        }
    
    }

}
add_action( 'admin_init', 'gfb_switch_to_new' );

if ( 'upgraded' == get_option( 'gfb_switch_to_new', 'no' ) || false == get_option( 'gfb_price_policy', false ) ) {
    require __DIR__ . '/upgraded/gravity-form-booking-upgraded.php';
} else {
    require __DIR__ . '/gravity-form-booking.php';
}

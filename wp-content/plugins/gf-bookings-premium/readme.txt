-=== Gravity Bookings ===
-Plugin URI: https://wpexperts.io/
-Contributors: wpexpertsio
-Tags: Gravity forms, bookings, appointments, calendar, scheduling, wordpress forms, reservations, gravity bookings, events
-Requires at least: 4.5.0 
-Requires PHP: 7.2 
-Tested up to: 6.1.1
-Stable tag: 2.3
-License: GPLv2 or later
-License URI: http://www.gnu.org/licenses/gpl-2.0.html
-
-== Description ==
-
-Gravity Booking plugin provides a platform to manage appointments via google calendar. Appointment can be booked online from any listed category of services along with its type. Once a customer visit on his/her appointment so email will get routed to him/her and logs will be registered in admin panel.
-
-= Features: =
-
-* Easy installation and configuration
-* Proficient and simple booking process
-* Unlimited staff, services categories and their time slots can be added
-* Customer can select any ‘service category’ for appointment from the list of services 
-* Customer can select any ‘service type’ related to selected service category
-* Customer can select any ‘staff’ from list with whom service will be delivered
-* Variable ’time and day’ can be selected from available days of selected staff
-* Booked Appointment can be seen in Calendar as well as List View
-* Holiday’s can be marked against every individual staff
-* Payment will get done through ‘payment gateway’. All payment gateways of gravity form add-on can be utilized as payment gateway activation
-‘Emails triggers’ to admin and customers on specified cases 
-* Export all data of ‘Appointments’, ‘Service Categories’, ‘Services’, ‘Customers’ and ‘Payments’ in CSV & PDF file format
-* Gravity Forms Booking supports multistep forms as well.
-* Gravity Forms Booking also includes support for Gravity Forms Preview Add-on.
-* Gravity Forms Booking also supports RTL functionality.
-* Also Mobile Responsive works well on Mobile and Tablet devices.
-
-== Changelog ==

== 2.3 ==
* NEW - UI/UX features.
* NEW - Added location filter.
* NEW - Can add multiple services on multiple forms.
* NEW - Added compatibility with Gravity Forms Coupons.
* NEW - Now compatible with Square for Gravity Forms.
* NEW - Added compatibility with Gravity Form Preview.
* NEW - Dynamic email templates added for different events.
* NEW - Added cancel appointment feature for customers using shortcode.
* NEW - Now you have option to change the label for locations, services & staffs.
* NEW - Ability to add independent interval and duration for each staffs.
* TWEAK - Global based appointments have been moved into individual form field settings.
* IMPROVED - Seperate booking fields are now as group field.
* IMPROVED - Mobile Responsive appointment calendar.
* IMPROVED - Now compatible with Gravity Forms Stripe & Paypal.
* IMPROVED - View more scalable reports.
* IMPROVED - Add/Remove holidays in bulk.
-
-= 2.1.1 =
-
-TWEAK - Change hide/show method for slot qty to avoid theme css conflicts.
-FIX   - Time slots not visible in other languages.
-FIX   - If file_get_contents() get block by server. simple email will sent (GFB-199).
-FIX   - Add filter to change fonts for pdf apply_filters( 'gfb_change_pdf_font' 'Arial').
-FIX   - Fixed for appointment list admin for multisite.
-FIX   - Slot timing sorting issue.
-FIX   - Services order by title.
-FIX   - make string translable for calendar inside.
-
-= 2.1 =
-
-NEW - Manage Price and Appointment policies.
-NEW - Added Display tab in settings for calendar display colors.
-NEW - Added Fullday appointment.
-TWEAK - Allow decimal in price.
-
-
-= 2.0 =
-
-NEW – You can set a timezone for each staff.
-NEW – Option to select multiple dates at once to make it off (holiday).
-NEW – Set start time and end time will generate auto slots for you.
-NEW – You can add breaks to specific slots you want.
-NEW – Users can book more than one slot at a time.
-NEW – Now you can set a booking form as global or service-based.
-TWEAK – Admin can change appointment status.
-TWEAK – Admin can change payment status.
-TWEAK – You can add time slot with same name in different days.
-FIX – Admin can change appointment status.
-FIX – The timezone issue is fixed. It will pick the timezone from WordPress general settings.
-FIX – Google Calendar button issue fixed on form submit.
-FIX – Once the form is submitted without selecting the date, the calendar becomes unavailable.
-FIX – Once the appointment is canceled other users cannot schedule the same slot.
-
-= 1.0.4 =
-
-* FIX - General settings not showing for new GravityForms version
-* IMPROVE - User settings and general settings save for new GravityForms version.
-* Added - Compatibility With Gravity Form New Version 2.5
-
-= 1.0.2 =
-
-* FIX - Current date not selected in new appointment
-* NEW - Settings for appointments to select days prior to booking
-* NEW - Settings to set time interval
-
-= 1.0.1 =
-* Added - Conditional Logic Feature
-
-= 1.0 =
-* Initial release
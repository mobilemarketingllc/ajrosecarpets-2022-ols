const swalWithBootstrapButtons = Swal.mixin({
	customClass: {
	    confirmButton: 'gfb-btn gfb-btn-success',
	    cancelButton: 'gfb-btn gfb-btn-danger'
	},
	buttonsStyling: true
});

const datePickerAddHolidayField = document.getElementById('datepicker');
const datePickerRemoveHolidayField = document.getElementById('datepicker-2');

let dateRanges = [];

const datePickerAddHolidayInvalidDate = (date) => {
	var current_date = moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD');
	var flag = jQuery.inArray(current_date, dateRanges);
	if ( flag >= 0 ) {				
		return true;
	}
}

const datePickerAddHolidayInvalidDateClass = (date) => {
	var current_date = moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD');
	var flag = jQuery.inArray(current_date, dateRanges);
	if ( flag >= 0 ) {
		//console.log(current_date);
		return 'gfb-day-off';
	}
}

const datePickerRemoveHolidayInvalidDate = (date) => {
	var current_date = moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD');	
	if ( jQuery.inArray(current_date, dateRanges) < 0 ) {				
		return true;
	}
}

const datePickerAddHolidaySettings = {
	parentEl: jQuery('#add_holidays'),
	autoApply: false,
	showDropdowns: true,	
	alwaysShowCalendars: true,
	linkedCalendars: true,		
	opens: 'center',
	minDate: moment(),
	locale: {
		format: 'DD-MM-YYYY',
		separator: '_',
		applyLabel: 'Add Holiday'			
	},
	isInvalidDate: datePickerAddHolidayInvalidDate,
	isCustomDate: datePickerAddHolidayInvalidDateClass
};

const datePickerRemoveHolidaySettings = {
	parentEl: jQuery('#remove_holidays'),
	autoApply: false,
	showDropdowns: true,	
	alwaysShowCalendars: true,
	linkedCalendars: true,		
	opens: 'center',
	minDate: moment(),
	locale: {
		format: 'DD-MM-YYYY',
		separator: '_',
		applyLabel: 'Remove Holiday'			
	},
	isInvalidDate: datePickerRemoveHolidayInvalidDate,	
};

const days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};

jQuery( document ).ready(function($) {

	/*Email Template Accordion*/
	$( "#accordion" ).accordion({
		collapsible: true,
		heightStyle: "content",
		animate: 200	
	});

	//Email Template settings
	$('#app_notification_form').on('submit', function(event) {
		event.preventDefault();

		var action = 'gfb_email_template_settings';

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function() {
				$('body').append('<div id="gfb-overlay" />');				
			},
			success: function (response) {
				var response = $.parseJSON(response);
				// console.log(response);
				$('#gfb-overlay').remove();
				swalWithBootstrapButtons.fire(
		      		response.status,
		      		response.message,
		      		response.status
		    	);
			},
			complete: function() {
				
			}
		});
	});

	/*Dashboard Filters Starts*/

	var start = '';
    var end = '';

    function applyFilterRange(start = '', end = '', flag = false) {    	
    	$('#reportrange span').html(`Select date to filter appointments`);    	
    	if ( start != '' && end != '' ) {    		
	        if ( flag ) {	        	
	        	$('#reportrange span').html(`${moment(start, 'YYYY-MM-DD').format('MMMM D, YYYY')} - ${moment(end, 'YYYY-MM-DD').format('MMMM D, YYYY')}`);	        	
	        } else {	        	
	        	$('#reportrange span').html(`Select date to filter appointments`);	        	
	        }
	    }
        $('#reportrange').css({'visibility': 'visible', 'opacity': 1});      
    }

    $('#reportrange').daterangepicker({
    	start: moment(),
    	end: moment(),
        ranges: {           
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Today': [moment(), moment()],
           'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
           'This Week': [moment(), moment().add(6, 'days')],           
           'Last Week': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],           
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, applyFilterRange);

    $(document).on('click', '.ranges ul li', function() {

    });

    var url_start = $.trim(getUrlParameter('date_from'));
	var url_end = $.trim(getUrlParameter('date_to'));
	console.log('url_start', url_start);
	console.log('url_end', url_end);
	if ( url_start != '' && url_end != '' && url_start != 'true' && url_end != 'true' && url_start != 'false' && url_end != 'false' ) {
		console.log('test1');	
		applyFilterRange(url_start, url_end, true);
	} else {		
		console.log('test2');
		applyFilterRange(start, end, false);
	}    

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {    	
		$('#reportrange span').html(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
		$('input[name="date_from"]').val(picker.startDate.format('YYYY-MM-DD'));
		$('input[name="date_to"]').val(picker.endDate.format('YYYY-MM-DD'));
		$('#post-query-submit').click();
	});

	/*Dashboard Filters End*/

	// Add Placeholder To Search Box
	jQuery('.search-box input[type=search]').attr('placeholder', 'Search');

	if ( $(datePickerAddHolidayField).length > 0 ) {
		//Init calendar again
		$(datePickerAddHolidayField).daterangepicker(datePickerAddHolidaySettings);
		$(datePickerRemoveHolidayField).daterangepicker(datePickerRemoveHolidaySettings);
	}

	$(".close_icon").click(function(){
		$(".gf_booking_sidebar").removeClass("activesidebar");
		$(".gf_booking_overlayDiv").removeClass("active");
	});
	$("#gf_booking_cardOne").click(function(){
		$(".gf_booking_sidebar").addClass("activesidebar");
		$(".gf_booking_overlayDiv").addClass("active");
		$(".gf_booking_general--inner").removeClass("closed");
		$(".gf_booking_display--inner, .gf_booking_apointments_listing, .gf_booking_compnay_detail, .gf_booking_google_calander, .gf_booking_email_configuration").addClass("closed");
	});
	$("#gf_booking_cardtwo").click(function(){
		$(".gf_booking_sidebar").addClass("activesidebar");
		$(".gf_booking_overlayDiv").addClass("active");
		$(".gf_booking_general--inner, .gf_booking_apointments_listing, .gf_booking_email_configuration, .gf_booking_google_calander, .gf_booking_compnay_detail").addClass("closed");
		$(".gf_booking_display--inner").removeClass("closed");
	});
	$("#gf_bookings_appointments").click(function(){
		$(".gf_booking_sidebar").addClass("activesidebar");
		$(".gf_booking_overlayDiv").addClass("active");
		$(".gf_booking_general--inner, .gf_booking_display--inner, .gf_booking_email_configuration, .gf_booking_google_calander, .gf_booking_compnay_detail").addClass("closed");
		$(".gf_booking_apointments_listing").removeClass("closed");
	});
	$("#gf_booking_company_detail").click(function(){
		$(".gf_booking_sidebar").addClass("activesidebar");
		$(".gf_booking_overlayDiv").addClass("active");
		$(".gf_booking_general--inner, .gf_booking_display--inner, .gf_booking_apointments_listing, .gf_booking_email_configuration, .gf_booking_google_calander").addClass("closed");
		$(".gf_booking_compnay_detail").removeClass("closed");
	});

	$("#gf_booking_email_configuration").click(function(){
		$(".gf_booking_sidebar").addClass("activesidebar");
		$(".gf_booking_overlayDiv").addClass("active");
		$(".gf_booking_general--inner, .gf_booking_display--inner, .gf_booking_apointments_listing, .gf_booking_compnay_detail, .gf_booking_google_calander").addClass("closed");
		$(".gf_booking_email_configuration").removeClass("closed");
	});
	$("#gf_booking_google_calender").click(function(){
		$(".gf_booking_sidebar").addClass("activesidebar");
		$(".gf_booking_overlayDiv").addClass("active");
		$(".gf_booking_general--inner, .gf_booking_display--inner, .gf_booking_apointments_listing, .gf_booking_compnay_detail, .gf_booking_email_configuration, gf_booking_compnay_detail").addClass("closed");
		$(".gf_booking_google_calander").removeClass("closed");
	});

	$(".gf_booking_overlayDiv").click(function(){
		$(".gf_booking_overlayDiv").removeClass("active");
		$(".gf_booking_sidebar").removeClass("activesidebar");
	});

	//initializing select2
	$('#gfb-location').select2();

	//initializing staff time slot break select2
	$('.gfb-slot-break').select2();

	//staff time slot break div show hide
	$('.add-break-btn span').click(function () {
		var id = $(this).data('id');
		$(`#break-${id}`).toggleClass('form-time-slots-break-box-show');
	});

	//Trigger to open modal 
	$('.gfb-add-button').on('click', function(event) {
		event.preventDefault();
		var id = $(this).data('id');

		window.scrollTo({top: 0});
		$('body').addClass('gfb-modal-open');

		$('.gfb-response').html('').removeClass('success error');
		$('.modal--form')[0].reset();

		if ( $('#gfb-staff-form-details').length > 0 ) {
			
			if ( $('#gfb-location').length > 0 ) {
				$('#gfb-location').val(null).trigger('change');	
			}

			$('#gfb-staff-form-details')[0].reset();
		}

		if ( $('#gfb-staff-form-services').length > 0 ) {
			$('#gfb-staff-form-services')[0].reset();
		}

		if ( $('#gfb-staff-form-timings').length > 0 ) {
			$('#gfb-staff-form-timings')[0].reset();
		}

		$('input[type=checkbox]').prop('checked', false);

		// alert(id);
		if ( $('.post_id').length > 0 ) {
			$('.post_id').val('');
		}
		//modal to open
		$(`#${id}`).addClass('gfb-modal-show');

		if ( $(datePickerAddHolidayField).length > 0 ) {
			//destroy the calendar
	   		$(datePickerAddHolidayField).data('daterangepicker').remove();
	   		$(datePickerRemoveHolidayField).data('daterangepicker').remove();
	   		
	   		//Init calendar again
	   		$(datePickerAddHolidayField).daterangepicker(datePickerAddHolidaySettings);
	   		$(datePickerRemoveHolidayField).daterangepicker(datePickerRemoveHolidaySettings);

	   		//open calendar again
			$(datePickerAddHolidayField).trigger('click');
			$(datePickerRemoveHolidayField).trigger('click');
		}
	});

	//Trigger to close modal
	$('.modal__close').on('click', function (e) {
		e.preventDefault();
		if ( $('.post_id').length > 0 ) {
			$('.post_id').val('');
		}

		window.scrollTo({top: 0});
		$('body').removeClass('gfb-modal-open');

		// $(this).siblings('.modal--form')[0].reset();		
		if ( $('.modal--form').length > 0 ) {
			$('.gfb-response').html('').removeClass('success error');
			$('.modal--form')[0].reset();
			if ( $('#gfb-staff-form-details').length > 0 ) {
				if ( $('#gfb-location').length > 0 ) {
					$('#gfb-location').val(null).trigger('change');	
				}
				
				$('#gfb-staff-form-details')[0].reset();
			}

			if ( $('#gfb-staff-form-services').length > 0 ) {
				$('#gfb-staff-form-services')[0].reset();
			}

			if ( $('#gfb-staff-form-timings').length > 0 ) {
				$('#gfb-staff-form-timings')[0].reset();
			}

			$('input[type=checkbox]').prop('checked', false);
		}
		$('.modal').removeClass('gfb-modal-show');
	});

	$('#gfb-booking_type').on('change', function() {
		if ( 'fullday' == $(this).val() ) {
			$('.form-start-time-slots, .form-end-time-slots, .add-break-btn, #gfb-duration, #gfb-interval').addClass('form-time-slots-disabled');
		} else {
			$('.form-start-time-slots, .form-end-time-slots, .add-break-btn, #gfb-duration, #gfb-interval').removeClass('form-time-slots-disabled');
		}
	});

	// $('.form-time-slots-content-row .gfb_staff_time_toggle').on('click', function() {		
	// 	if ( $(this).is(':checked') ) {
	// 		$(this).parents('.form-time-slots-content-row').find('.form-slots-capacity').removeClass('form-time-slots-disabled');
	// 	} else {
	// 		$(this).parents('.form-time-slots-content-row').find('.form-slots-capacity').addClass('form-time-slots-disabled');
	// 	}
	// });

	$('.gfb-edit').on('click', function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var post_id = $(this).data('post_id');
		var type = $(this).data('type');
		var action = '';

		window.scrollTo({top: 0});
		$('body').addClass('gfb-modal-open');

		//resetting previous values.
		if ( $('.modal--form').length > 0 ) {
			$('.gfb-response').html('').removeClass('success error');
			$('.modal--form')[0].reset();
			if ( $('#gfb-staff-form-details').length > 0 ) {
				$('#gfb-staff-form-details')[0].reset();
			}

			if ( $('#gfb-staff-form-services').length > 0 ) {
				$('#gfb-staff-form-services')[0].reset();
			}

			if ( $('#gfb-staff-form-timings').length > 0 ) {
				$('#gfb-staff-form-timings')[0].reset();
			}

			$('input[type=checkbox]').prop('checked', false);
		}

		if ( post_id != '' && type != '' && id != '' ) {

			if ( 'location' == type ) {
				action = 'gfb_show_location_data';
			}

			if ( 'staff' == type ) {
				action = 'gfb_show_staff_data';
			}

			if ( 'customer' == type ) {
				action = 'gfb_show_customer_data';
			}

			if ( 'service' == type ) {
				action = 'gfb_show_service_data';
			}

			if ( 'appointment' == type ) {
				action = 'gfb_show_appointment_data';
			}

			$.ajax({
				url: gfb_admin.ajaxurl,
				type: 'POST',
				data: `post_id=${post_id}&action=${action}&nonce=${gfb_admin.nonce}`,
				beforeSend: function() {
					$('body').append('<div id="gfb-overlay" />');
				},
				success: function (response) {
					var response = $.parseJSON(response);
					if ( 'success' == response.status ) {
						if ( $('.post_id').length > 0 ) {
							$('.post_id').val(post_id);
						}
						
						if ( 'appointment' != type ) {
							$.each( response, function(i, v) {
								// console.log(i);
								// console.log(v);
								if ( $(`#gfb-${i}`).length > 0 ) {
									if ( 'duration' == i || 'interval' == i ) {
										$(`#gfb-${i}`).val(v);
									} else {
										$(`#gfb-${i}`).val(v).trigger('change');
									}
									
								}

								//if action is for staff so we will check for staff services
								if ( 'services' === i ) {
									// console.log(v);
									$.each( v, function(ii, vv) {
										if ( $(`#gfb_staff_service_aval_${ii}`).length > 0 ) {
											$(`#gfb_staff_service_aval_${ii}`).prop('checked', true);
											$(`#gfb_staff_service_price_${ii}`).val(vv.price);
										}
									});
								}

								//if action is for staff so we will check for staff services
								if ( 'timings' === i ) {
									//console.log(v);
									$('#gfb-booking_type').trigger('change');
									$.each( v, function(ii, vv) {								
										if ( $(`#gfb_staff_timing_${ii}_start`).length > 0 ) {
											if ( '1' == vv.switch ) {
												$(`#gfb_staff_timing_${ii}_switch`).prop('checked', true);
											}											
											$(`#gfb_staff_timing_${ii}_start`).val(vv.start);
											$(`#gfb_staff_timing_${ii}_end`).val(vv.end);
											$(`#gfb_staff_timing_${ii}_capacity`).val(vv.capacity);
											// $.each( vv.break, function( iii, vvv ) {
											// 	$(`#gfb_staff_timing_${ii}_break`).val(vv.capacity);
											// });										
										}
									});
								}

								if ( 'time_slots' === i ) {
									
									if ( $(`.gfb-slot-break`).length > 0 ) {
										$(`.gfb-slot-break`).html('');
									}

									$.each( v, function(ii, vv) {
										if ( $(`#gfb_staff_timing_${ii}_break`).length > 0 ) {											
											$.each( vv.slot, function( xx, yy ) {												
												var selected = false;
												if ( $.inArray(vv.start[xx], response.timings[ii].break) >= 0 ) {
													var selected = true;
												}
												console.log( `${ii} set as break ${vv.start[xx]} - ${selected}`);

												$(`#gfb_staff_timing_${ii}_break`).append($('<option>', {												
													value: vv.start[xx],
													text: yy,
													selected: selected
												}));												
											});
										}
									});
								}

								if ( 'holidays' === i ) {
									
									if ( $(datePickerAddHolidayField).length > 0 ) {
										//destroy the calendar
								   		$(datePickerAddHolidayField).data('daterangepicker').remove();
								   		$(datePickerRemoveHolidayField).data('daterangepicker').remove();

								   		dateRanges = v;
								   		//Init calendar again
								   		$(datePickerAddHolidayField).daterangepicker(datePickerAddHolidaySettings);
								   		$(datePickerRemoveHolidayField).daterangepicker(datePickerRemoveHolidaySettings);

								   		//open calendar again
										$(datePickerAddHolidayField).trigger('click');
										$(datePickerRemoveHolidayField).trigger('click');
									}
								}
							});
						} else { //only for appointment type
							console.log(response);
							$.each( response, function(i, v) {
								if ( 'status' !== i  ) {
									$(`#${i}`).html(v);
								}
							});
						}

						//modal to open
						$(`#${id}`).addClass('gfb-modal-show');

					} else {
						swalWithBootstrapButtons.fire(
				      		'No User Found!',
				      		'',
				      		response.status
				    	);
					}
				},
				complete: function() {
					$('#gfb-overlay').remove();
				}
			});
		}
	});

	$('.gfb-delete').on( 'click', function( event ) {
		event.preventDefault();
		var type = $(this).data('type');
		var post_id = $(this).data('post_id');
		var action = '';

		if ( 'location' == type ) {
			action = 'gfb_delete_location';
		}

		if ( 'staff' == type ) {
  			action = 'gfb_delete_staff';
  		}

  		if ( 'customer' == type ) {
  			action = 'gfb_delete_customer';
  		}

  		if ( 'service' == type ) {
  			action = 'gfb_delete_service';
  		}

  		if ( 'appointment' == type ) {
  			action = 'gfb_delete_appointment';
  		}

		swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
		  	icon: 'warning',
		  	showCancelButton: true,
		  	confirmButtonText: 'Yes, delete it!',
		  	cancelButtonText: 'No, cancel!',
		  	reverseButtons: true
		}).then((result) => {
		  	if (result.isConfirmed) {
		  		$.ajax({
					url: gfb_admin.ajaxurl,
					type: 'POST',
					data: `post_id=${post_id}&action=${action}&nonce=${gfb_admin.nonce}`,
					beforeSend: function() {
						$('body').append('<div id="gfb-overlay" />');
					},
					success: function (response) {
						var response = $.parseJSON(response);
						if ( 'success' == response.status ) {
							swalWithBootstrapButtons.fire(
					      		'Deleted!',
					      		response.message,
					      		response.status
					    	).then((result) => {
					    		location.reload();
					    	});
						} else {
							swalWithBootstrapButtons.fire(
					      		'Not deleted!',
					      		response.message,
					      		response.status
					    	);
						}
					},
					complete: function() {
						$('#gfb-overlay').remove();
					}
				});
		  	} else if (result.dismiss === Swal.DismissReason.cancel) {
		    	swalWithBootstrapButtons.fire(
		      		'Cancelled',
		      		'',
		      		'error'
		    	)
		  	}
		});

	});

	$('.gfb-appointment-status').on( 'change', function( event ) {
		event.preventDefault();		
		var post_id = $(this).data('post_id');
		var status = $(this).val();
		var action = 'gfb_change_appointment_status';

		if ( ''!== post_id ) {
			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to change the appointment status",
			  	icon: 'warning',
			  	showCancelButton: true,
			  	confirmButtonText: 'Yes, Change it!',
			  	cancelButtonText: 'No, cancel!',
			  	reverseButtons: true
			}).then((result) => {
			  	if (result.isConfirmed) {
			  		$.ajax({
						url: gfb_admin.ajaxurl,
						type: 'POST',
						data: `post_id=${post_id}&status=${status}&action=${action}&nonce=${gfb_admin.nonce}`,
						beforeSend: function() {
							$('body').append('<div id="gfb-overlay" />');
						},
						success: function (response) {
							var response = $.parseJSON(response);
							if ( 'success' == response.status ) {
								swalWithBootstrapButtons.fire(
						      		'Changed!',
						      		response.message,
						      		response.status
						    	).then((result) => {
						    		location.reload();
						    	});
							} else {
								swalWithBootstrapButtons.fire(
						      		'Not Changed!',
						      		response.message,
						      		response.status
						    	);
							}
						},
						complete: function() {
							$('#gfb-overlay').remove();
						}
					});
			  	} else if (result.dismiss === Swal.DismissReason.cancel) {
			    	swalWithBootstrapButtons.fire(
			      		'Cancelled',
			      		'',
			      		'error'
			    	)
			  	}
			});
		}	
	});

	$('.gfb_show_reason').on('click', function(e) {
		e.preventDefault();
		var reason = $(this).data('reason');
		if ( $.trim( reason ) != '' ) {
			swalWithBootstrapButtons.fire(
	      		'Reason of cancellation',
	      		reason	      		
	    	);
		}
	});

	// MODAIL FORM TABS SCRIPTS
	var generate_tabs = function( callback ) {
		'use strict';
		/*Activate default tab contents*/
		var leftPos, newWidth, $magicLine, defaultActive;

		defaultActive = $('.staff_tabs .tabs li.active a').attr('href');
		$(defaultActive).show();

		$('.staff_tabs .tabs').append("<li id='magic-line'></li>");
		$magicLine = $('.staff_tabs #magic-line');
		if ( $magicLine.length > 0 ) {
			$magicLine.width($('.staff_tabs .active').width())
				.css('left', $('.staff_tabs .active a').position().left)
				.data('origLeft', $magicLine.position().left)
				.data('origWidth', $magicLine.width());
		}

		$('.staff_tabs .tabs li a').click(function(){
			var $this,tabId,leftVal,$tabContent;
			$this = $(this);
			$tabContent = $('.staff_tabs .tabContent');
			$this.parent().addClass('active').siblings().removeClass('active');
			tabId = $this.attr('href');

			leftVal = $($tabContent).index($(tabId)) * $tabContent.width() * -1;
			$('.staff_tabs .tabWrapper').stop().animate({left:leftVal}, function() {
				callback($this);
			});

			$magicLine
				.data('origLeft',$this.position().left)
				.data('origWidth',$this.width() + 20);			

			return false;
		});

		/*Magicline hover animation*/
		$('.staff_tabs .tabs li').find('a').hover(function() {
			var $thisBar = $(this);
			leftPos = $thisBar.position().left;
			newWidth = $thisBar.parent().width();
			$magicLine.stop().animate({left:leftPos,width:newWidth});
		}, function() {
			$magicLine.stop().animate({left:$magicLine.data('origLeft'),width: $magicLine.data('origWidth')});
		});
	}

	generate_tabs(function( $this) {
		//console.log('tab generated');
	});

	//add Customer ajax
	$('#gfb-customer-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    fname: {
		    	required: true
		    },
		    lname: {
		    	required: true
		    },
		    field: {
		    	required: true,
		    	email: true
		    },	    
		},
		highlight: function(element, errorClass) {			
			$(element).addClass('errorBorder');	
		},
		success: function (label, element) {
			$(element).removeClass('errorBorder');
		},
		submitHandler: function( form ) {
			var This = $(form);
			var post_id = '';
			var action = '';

			if ( $(This).find('.post_id').length > 0 ) {
				post_id = $(This).find('.post_id').val();
			}

			if ( post_id != '' ) {
				action = 'gfb_edit_customer';
			} else {
				action = 'gfb_add_customer';
			}

			var formData = $(This).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
			$.ajax({
				url: gfb_admin.ajaxurl,
				type: 'POST',
				data: formData,
				beforeSend: function () {
					$('#gfb-customer-form').find('.errorBorder').removeClass('errorBorder');
					$('#gfb-customer-form').append('<div class="gfb-modal-overlay" />');
					$('#gfb-customer-form').find('.gfb-response').html('');
					$('.gfb-response').removeClass('error').removeClass('success');
				},
				success: function (response) {
					var response = $.parseJSON(response);
					//console.log(response);
					$('#gfb-customer-form').find('.gfb-response').addClass(response.status);
					$('#gfb-customer-form').find('.gfb-response').html(response.message);
					if ( 'success' == response.status ) {
						$('#gfb-customer-form')[0].reset();
						location.reload();
					}
				},
				complete: function () {
					$('.gfb-modal-overlay').remove();
				}
			});
		}

	});

	//add Staff Details ajax
	$('#gfb-staff-form-details').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    fname: {
		    	required: true
		    },
		    lname: {
		    	required: true
		    },
		    field: {
		    	required: true,
		    	email: true
		    },	    
		},
		highlight: function(element, errorClass) {			
			$(element).addClass('errorBorder');	
		},
		success: function (label, element) {
			$(element).removeClass('errorBorder');
		},
		submitHandler: function( form ) {
			var This = $(form);
			var post_id = '';
			var action = '';

			if ( $(This).find('.post_id').length > 0 ) {
				post_id = $(This).find('.post_id').val();
			}

			if ( post_id != '' ) {
				action = 'gfb_edit_staff';
			} else {
				action = 'gfb_add_staff';
			}

			var formData = $(This).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
			$.ajax({
				url: gfb_admin.ajaxurl,
				type: 'POST',
				data: formData,
				beforeSend: function () {
					$('#gfb-staff-form-details').find('.errorBorder').removeClass('errorBorder');
					$('#gfb-staff-form-details').append('<div class="gfb-modal-overlay" />');
					$('#gfb-staff-form-details').find('.gfb-response').html('');
					$('.gfb-response').removeClass('error').removeClass('success');
				},
				success: function (response) {
					var response = $.parseJSON(response);
					//console.log(response);
					$('#gfb-staff-form-details').find('.gfb-response').addClass(response.status);
					$('#gfb-staff-form-details').find('.gfb-response').html(response.message);
					if ( 'success' == response.status ) {
						$('#gfb-staff-form-details')[0].reset();
						location.reload();
					}
				},
				complete: function () {
					$('.gfb-modal-overlay').remove();
				}
			});
		}
	});

	/*$('#gfb-staff-form-details').on( 'submit', function( event ) {

		event.preventDefault();
		var post_id = '';
		var action = '';

		if ( $(this).find('.post_id').length > 0 ) {
			post_id = $(this).find('.post_id').val();
		}

		if ( post_id != '' ) {
			action = 'gfb_edit_staff';
		} else {
			action = 'gfb_add_staff';
		}

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function () {
				$('#gfb-staff-form-details').append('<div class="gfb-modal-overlay" />');
				$('#gfb-staff-form-details').find('.gfb-response').html('');
				$('.gfb-response').removeClass('error').removeClass('success');
			},
			success: function (response) {
				var response = $.parseJSON(response);
				//console.log(response);
				$('#gfb-staff-form-details').find('.gfb-response').addClass(response.status);
				$('#gfb-staff-form-details').find('.gfb-response').html(response.message);
				if ( 'success' == response.status ) {
					$('#gfb-staff-form-details')[0].reset();
					location.reload();
				}
			},
			complete: function () {
				$('.gfb-modal-overlay').remove();
			}
		});
	});*/


	//add Staff Services ajax
	$('#gfb-staff-form-services').on( 'submit', function( event ) {

		event.preventDefault();
		var post_id = '';
		var action = 'gfb_add_staff_services';

		if ( $(this).find('.post_id').length > 0 ) {
			post_id = $(this).find('.post_id').val();
		}		

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function () {
				$('#gfb-staff-form-services').append('<div class="gfb-modal-overlay" />');
				$('#gfb-staff-form-services').find('.gfb-response').html('');
				$('.gfb-response').removeClass('error').removeClass('success');
			},
			success: function (response) {
				var response = $.parseJSON(response);
				//console.log(response);
				$('#gfb-staff-form-services').find('.gfb-response').addClass(response.status);
				$('#gfb-staff-form-services').find('.gfb-response').html(response.message);
				if ( 'success' == response.status ) {
					//$('#gfb-staff-form-services')[0].reset();
					//location.reload();
				}
			},
			complete: function () {
				$('.gfb-modal-overlay').remove();
			}
		});
	});


	//add Staff Timings ajax
	$('#gfb-staff-form-timings').on( 'submit', function( event ) {

		event.preventDefault();
		var post_id = '';
		var action = 'gfb_add_staff_timings';

		if ( $(this).find('.post_id').length > 0 ) {
			post_id = $(this).find('.post_id').val();
		}		

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function () {
				$('#gfb-staff-form-timings').append('<div class="gfb-modal-overlay" />');
				$('#gfb-staff-form-timings').find('.gfb-response').html('');
				$('.gfb-response').removeClass('error').removeClass('success');
			},
			success: function (response) {
				var response = $.parseJSON(response);
				//console.log(response);
				$('#gfb-staff-form-timings').find('.gfb-response').addClass(response.status);
				$('#gfb-staff-form-timings').find('.gfb-response').html(response.message);
				if ( 'success' == response.status ) {
					//$('#gfb-staff-form-timings')[0].reset();
					//location.reload();
				}
			},
			complete: function () {
				$('.gfb-modal-overlay').remove();
			}
		});
	});


	//add location ajax
	$('#gfb-location-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    name: {
		    	required: true		    	
		    }		    
		},
		highlight: function(element, errorClass) {
			$(element).addClass('errorBorder');			
		},
		success: function (label, element) {
			$(element).removeClass('errorBorder');
		},
		submitHandler: function( form ) {
			var This = $(form);
			// event.preventDefault();
			var post_id = '';
			var action = '';

			if ( $(This).find('.post_id').length > 0 ) {
				post_id = $(This).find('.post_id').val();
			}

			if ( post_id != '' ) {
				action = 'gfb_edit_location';
			} else {
				action = 'gfb_add_location';
			}

			var formData = $(This).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
			$.ajax({
				url: gfb_admin.ajaxurl,
				type: 'POST',
				data: formData,
				beforeSend: function () {
					$('#gfb-location-form').find('.errorBorder').removeClass('errorBorder');
					$('#gfb-location-form').append('<div class="gfb-modal-overlay" />');
					$('#gfb-location-form').find('.gfb-response').html('');
					$('.gfb-response').removeClass('error').removeClass('success');
				},
				success: function (response) {
					var response = $.parseJSON(response);
					//console.log(response);
					$('#gfb-location-form').find('.gfb-response').addClass(response.status);
					$('#gfb-location-form').find('.gfb-response').html(response.message);
					if ( 'success' == response.status ) {
						$('#gfb-location-form')[0].reset();
						location.reload();
					}
				},
				complete: function () {
					$('.gfb-modal-overlay').remove();
				}
			});
		}
	});

	//add Service ajax
	$('#gfb-service-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    name: {
		    	required: true		    	
		    },
		    category: {
		    	required: true		    	
		    },		    
		},
		highlight: function(element, errorClass) {
			$(element).addClass('errorBorder');		
		},
		success: function (label, element) {
			$(element).removeClass('errorBorder');
		},
		submitHandler: function( form ) {
			var This = $(form);
			var post_id = '';
			var action = '';

			if ( $(This).find('.post_id').length > 0 ) {
				post_id = $(This).find('.post_id').val();
			}

			if ( post_id != '' ) {
				action = 'gfb_edit_service';
			} else {
				action = 'gfb_add_service';
			}

			var formData = $(This).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
			$.ajax({
				url: gfb_admin.ajaxurl,
				type: 'POST',
				data: formData,
				beforeSend: function () {
					$('#gfb-service-form').find('.errorBorder').removeClass('errorBorder');
					$('#gfb-service-form').append('<div class="gfb-modal-overlay" />');
					$('#gfb-service-form').find('.gfb-response').html('');
					$('.gfb-response').removeClass('error').removeClass('success');
				},
				success: function (response) {
					var response = $.parseJSON(response);
					//console.log(response);
					$('#gfb-service-form').find('.gfb-response').addClass(response.status);
					$('#gfb-service-form').find('.gfb-response').html(response.message);
					if ( 'success' == response.status ) {
						$('#gfb-service-form')[0].reset();
						location.reload();
					}
				},
				complete: function () {
					$('.gfb-modal-overlay').remove();
				}
			});
		}
	});

	//General settings
	$('.gfb_setting_form').on('submit', function(event) {
		event.preventDefault();

		var action = 'gfb_general_settings';		

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function() {
				$('body').append('<div id="gfb-overlay" />');
				// $('.gf_booking_sidebar').removeClass('.activesidebar');
				// $('.gf_booking_overlayDiv').removeClass('.active');
			},
			success: function (response) {
				var response = $.parseJSON(response);
				// console.log(response);
				swalWithBootstrapButtons.fire(
		      		response.status,
		      		response.message,
		      		response.status
		    	);
			},
			complete: function() {
				$('#gfb-overlay').remove();
			}
		});
	});

	//add Staff Holidays ajax
	$('#gfb-staff-form-add-holidays').on( 'submit', function( event ) {

		event.preventDefault();
		var post_id = '';
		var action = 'gfb_add_staff_holidays';

		if ( $(this).find('.post_id').length > 0 ) {
			post_id = $(this).find('.post_id').val();
		}		

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function () {
				$('#gfb-staff-form-add-holidays').append('<div class="gfb-modal-overlay" />');
				$('#gfb-staff-form-add-holidays').find('.gfb-response').html('');
				$('.gfb-response').removeClass('error').removeClass('success');
			},
			success: function (response) {
				var response = $.parseJSON(response);
				//console.log(response);
				$('#gfb-staff-form-add-holidays').find('.gfb-response').addClass(response.status);
				$('#gfb-staff-form-add-holidays').find('.gfb-response').html(response.message);
				if ( 'success' == response.status ) {
					//$('#gfb-staff-form-timings')[0].reset();
					//location.reload();

					if ( $(datePickerAddHolidayField).length > 0 ) {
						//destroy the calendar
				   		$(datePickerAddHolidayField).data('daterangepicker').remove();
				   		$(datePickerRemoveHolidayField).data('daterangepicker').remove();

				   		dateRanges = response.holidays;
				   		//Init calendar again
				   		$(datePickerAddHolidayField).daterangepicker(datePickerAddHolidaySettings);
				   		$(datePickerRemoveHolidayField).daterangepicker(datePickerRemoveHolidaySettings);

				   		//open calendar again
						$(datePickerAddHolidayField).trigger('click');
						$(datePickerRemoveHolidayField).trigger('click');
					}
				}
			},
			complete: function () {
				$('.gfb-modal-overlay').remove();
			}
		});
	});

	//add Staff Holidays ajax
	$('#gfb-staff-form-remove-holidays').on( 'submit', function( event ) {

		event.preventDefault();
		var post_id = '';
		var action = 'gfb_remove_staff_holidays';

		if ( $(this).find('.post_id').length > 0 ) {
			post_id = $(this).find('.post_id').val();
		}		

		var formData = $(this).serialize() + `&action=${action}&nonce=${gfb_admin.nonce}`;
		$.ajax({
			url: gfb_admin.ajaxurl,
			type: 'POST',
			data: formData,
			beforeSend: function () {
				$('#gfb-staff-form-remove-holidays').append('<div class="gfb-modal-overlay" />');
				$('#gfb-staff-form-remove-holidays').find('.gfb-response').html('');
				$('.gfb-response').removeClass('error').removeClass('success');
			},
			success: function (response) {
				var response = $.parseJSON(response);
				//console.log(response);
				$('#gfb-staff-form-remove-holidays').find('.gfb-response').addClass(response.status);
				$('#gfb-staff-form-remove-holidays').find('.gfb-response').html(response.message);
				if ( 'success' == response.status ) {
					//$('#gfb-staff-form-timings')[0].reset();
					//location.reload();

					if ( $(datePickerAddHolidayField).length > 0 ) {
						//destroy the calendar
				   		$(datePickerAddHolidayField).data('daterangepicker').remove();
				   		$(datePickerRemoveHolidayField).data('daterangepicker').remove();

				   		dateRanges = response.holidays;
				   		//Init calendar again
				   		$(datePickerAddHolidayField).daterangepicker(datePickerAddHolidaySettings);
				   		$(datePickerRemoveHolidayField).daterangepicker(datePickerRemoveHolidaySettings);

				   		//open calendar again
						$(datePickerAddHolidayField).trigger('click');
						$(datePickerRemoveHolidayField).trigger('click');
					}
				}
			},
			complete: function () {
				$('.gfb-modal-overlay').remove();
			}
		});
	});

	$(document).on( 'click', '#add_holidays .applyBtn', function(event) {

		event.preventDefault();
   		//submit dayoff form
   		$('#gfb-staff-form-add-holidays').submit();

    });

    $(document).on( 'click', '#remove_holidays .applyBtn', function(event) {

		event.preventDefault();
   		//submit dayoff form
   		$('#gfb-staff-form-remove-holidays').submit();

    });

	$.each( days, function( k, day ) {
		$(document).on('change', `#gfb_staff_timing_${day}_start`, function(e) {
			var time_slots = new Array();
			let start_time = parseTime($(`#gfb_staff_timing_${day}_start`).val()),
				end_time = parseTime($(`#gfb_staff_timing_${day}_end`).val()),
				duration = $('#gfb-duration').val(),
				interval = $('#gfb-interval').val();

			if ( duration == '' ) {
				duration = $('#gfb-duration').data('general');
			}

			if ( interval == '' ) {
				interval = $('#gfb-interval').data('general');
			}

			if ( start_time != '' && end_time != '' && start_time < end_time && interval != '' && duration != '' ) {
				time_slots = gfb_split_time(start_time, end_time, duration, interval);
			}

			$(`#gfb_staff_timing_${day}_break`).html('');

			$.each( time_slots, function( i, v ) {											
				$(`#gfb_staff_timing_${day}_break`).append($('<option>', {
					value: v,
					text: v				
				}));
			});
		});


		$(document).on('change', `#gfb_staff_timing_${day}_end`, function(e) {
			var time_slots = new Array();
			let start_time = parseTime($(`#gfb_staff_timing_${day}_start`).val()),
				end_time = parseTime($(`#gfb_staff_timing_${day}_end`).val()),
				duration = $('#gfb-duration').val(),
				interval = $('#gfb-interval').val();

			if ( duration == '' ) {
				duration = $('#gfb-duration').data('general');
			}

			if ( interval == '' ) {
				interval = $('#gfb-interval').data('general');
			}

			if ( start_time != '' && end_time != '' && start_time < end_time && interval != '' && duration != '' ) {
				time_slots = gfb_split_time(start_time, end_time, duration, interval);
			}

			$(`#gfb_staff_timing_${day}_break`).html('');

			$.each( time_slots, function( i, v ) {
				var val = v.split('-');
				val = $.trim(val[0]);								
				$(`#gfb_staff_timing_${day}_break`).append($('<option>', {
					value: val,
					text: v				
				}));
			});
		});

	});

	$('#gfb-duration, #gfb-interval').on('keyup copy paste change', function() {
		// console.log(`I triggered`);
		$('.form-start-time-slots input[type="time"]').trigger('change');
		$('.form-end-time-slots input[type="time"]').trigger('change');
	});

});

function getDatesInRange(startDate, endDate) {
	
	const dates = [];

	startDate = new Date(startDate);
	endDate = new Date(endDate);

	if ( jQuery.trim(startDate) == '' || jQuery.trim(endDate) == '' ) {
		return dates;
	}

	const date = new Date(startDate.getTime());	

	while (date <= endDate) {
	dates.push(moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD'));
	date.setDate(date.getDate() + 1);
	}

	return dates;
}

function parseTime(s) {
  var c = s.split(':');
  return parseInt(c[0]) * 60 + parseInt(c[1]);
}

function convertHours(mins){
  var hour = Math.floor(mins/60);
  var mins = mins%60;
  var converted = pad(hour, 2)+':'+pad(mins, 2);
  return converted;
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

function gfb_split_time(start_time, end_time, duration = 5, interval = 5 ) {
  
	var i = 1,
		formatted_time = '',
		start_time = start_time,
		end_time = end_time,  	
  		time_slots = new Array(),
  		duration = parseInt(duration),
  		interval = parseInt(interval);
  
  	while ( start_time < end_time ) {
  		if ( i > 1 ) {
			start_time += interval;
  		}
    
    	formatted_time = `${convertHours(start_time)} - ${convertHours(start_time + duration)}`;
    	
    	time_slots.push(formatted_time);

    	start_time += duration; //Endtime check

    	i++;
  	}
  
  	return time_slots;
}

jQuery( document ).ready(function($) {	
	const overlay = jQuery('<div id="gfb-cal-top-overlay"> </div>');
	const overlay_inside = jQuery('<div id="gfb-cal-overlay"> </div>');

	$(document).on('click', '#gfb-fancy-calendar a.gfb-month-changer', function(e) {
		e.preventDefault();

		var is_admin = 'false';
		var location_id = '';
		var service_id = '';
		var staff_id = '';
		var form_id = '';
		var month = $(this).data('month');
		var prior_months = 	gfb_calendar.prior_months;

		if ( $('body').hasClass('gravity-booking_page_gfb-calendar') ) {
			is_admin = '1';			
		}

		if ( $('.gfb_location').length > 0 ) {
			location_id = $('.gfb_location').val();
		}

		if ( $('.gfb_service').length > 0 ) {
			service_id = $('.gfb_service').val();
		}

		if ( $('.gfb_staff').length > 0 ) {
			staff_id = $('.gfb_staff').val();
		}

		if ( $('.gfb_form_id').length > 0 ) {
			form_id = $('.gfb_form_id').val();
		}
		

		var data = {
			action: 'gfb_month_changer',
			nonce: gfb_calendar.nonce,
			date: month,
			is_admin: is_admin,
			location_id: location_id,
			service_id: service_id,
			staff_id: staff_id,
			form_id: form_id,
			prior_months: prior_months
		}

		$.ajax({
			url: gfb_calendar.ajaxurl,			
			type: 'POST',
			data:data,
			dataType: 'HTML',
			beforeSend: function() {
				overlay.appendTo(document.body);
				overlay_inside.appendTo('div.gfb-cal-container');
			},
			success: function(response) {				
				$('#gfb-fancy-calendar').html(response);
			},
			error: function(jqXHR, status, error) {
				if (jqXHR.status == 500) {
                	alert('Internal error: ' + jqXHR.responseText);
              	} else {
                  	alert('Unexpected error.');
              	}
			},
			complete: function() {
				overlay.remove();
				overlay_inside.remove();
			}
		});

		// $.post(gfb_calendar.ajaxurl, data, function( success ) {
		// 	if ( '' != success ) {
				
		// 	}
		// });
	});

	$(document).on('click', 'ul.gfb-slot-list > li:not(.gfb-unavailable) > span', function() {

		var form_id = '';

		if ( $('.gfb_form_id').length > 0 ) {
			form_id = $('.gfb_form_id').val();
		}

		$('ul.gfb-slot-list > li > span').removeClass('gfb-selected');
		$('ul.gfb-slot-list > li > span ~ input.gfb-selected-capacity').removeClass('show');
		$(this).parent('li').find('span').addClass('gfb-selected');
		if ( $(this).siblings('input.gfb-selected-capacity').length > 0 ) {
			$(this).siblings('input.gfb-selected-capacity').addClass('show');
		}

		var time = '';
		if ( $(this).siblings('input.gfb-selected-time').length > 0 ) {
			time = $(this).siblings('input.gfb-selected-time').val();
		}

		var cost = 0.00;
		if ( $(this).siblings('input.gfb-selected-price').length > 0 ) {
			cost = $(this).siblings('input.gfb-selected-price').val();
		}

		var capacity = '';
		if ( $(this).siblings('input.gfb-selected-capacity').length > 0 ) {
			capacity = $(this).siblings('input.gfb-selected-capacity').val();
			cost = cost * capacity;			
		}

		var date = '';
		if ( $(this).siblings('input.gfb-selected-date').length > 0 ) {
			date = $(this).siblings('input.gfb-selected-date').val();
		}		

		$(`#gfb_date_${form_id}`).val(date);
		$(`#gfb_time_${form_id}`).val(time);
		$(`#gfb_slot_${form_id}`).val(capacity);
		$(`#gfb_cost_${form_id}`).val(cost);

		// Trigger gf total
 		if( typeof gformInitPriceFields == 'function' ) {
 			gformInitPriceFields(); // will trigger "gform_product_total" filter
  		}

	});

	/**
	 * Process Appointment Tax
	 */
	if( typeof gformInitPriceFields == 'function' ) {
		gform.addFilter( 'gform_product_total', function( total, formId ) {
			// debugger;
			var costField = $('.gfb_cost_input');			

			if ( $(costField).length > 0 && $(costField).val() >= 0 ) {
				var cost = $(costField).val();
				// Cost Fields
				total += Math.max( 0, parseFloat( cost ) );

				if ( jQuery( '#gf_total_no_discount_' + formId ).length > 0 ) {
					jQuery( '#gf_total_no_discount_' + formId ).val( total );
					window['new_total_' + formId] = total;
				}

				if ( $('.gfb_cost_input').length > 0 ) {
					$('.gfb_cost_input').val(total);
				}

				return total;
			}

			if ( jQuery( '#gf_total_no_discount_' + formId ).length > 0 ) {
				jQuery( '#gf_total_no_discount_' + formId ).val( total );
				window['new_total_' + formId] = total;
			}
			
			return total;
		}, 40 /* coupons applied at 50 */ );		
	}

	$(document).on('keyup copy paste', '.gfb-selected-capacity', function() {		
		$(this).parent('li').find('span.gfb-selected').trigger('click');
	});

	$(document).on('click', 'td.gfb-available:not(.gfb-selected)', function() {

		var current = $(this);
		var parent_row = $(this).parent('tr');
		var parent_table = $(this).parents('table.gfb-cal-table');
		$('table.gfb-cal-table td').removeClass('gfb-selected');
		$('table.gfb-cal-table td').removeClass('gfb-caret-down');
		$(this).addClass('gfb-selected');

		var location_id = '';
		var service_id = '';
		var staff_id = '';
		var form_id = '';
		var date = $(this).find('.gfb-hdn').val();		

		if ( $('.gfb_location').length > 0 ) {
			location_id = $('.gfb_location').val();
		}

		if ( $('.gfb_service').length > 0 ) {
			service_id = $('.gfb_service').val();
		}

		if ( $('.gfb_staff').length > 0 ) {
			staff_id = $('.gfb_staff').val();
		}

		if ( $('.gfb_form_id').length > 0 ) {
			form_id = $('.gfb_form_id').val();
		}

		$(`#gfb_date_${form_id}`).val(0);
		$(`#gfb_time_${form_id}`).val(0);
		$(`#gfb_slot_${form_id}`).val(0);
		$(`#gfb_cost_${form_id}`).val(0);
		$(`#gfb_booking_type_${form_id}`).val('');
		// if ( $(`#gfb_booking_type_${form_id}`).length > 0 ) {
		// 	$(`#gfb_booking_type_${form_id}`).remove();
		// }

		//alert(date);
		var data = {
			action: 'gfb_show_slots_by_date',
			nonce: gfb_calendar.nonce,
			date: date,			
			location_id: location_id,
			service_id: service_id,
			staff_id: staff_id,
			form_id: form_id
		}

		$.ajax({
			url: gfb_calendar.ajaxurl,			
			type: 'POST',
			data:data,
			dataType: 'HTML',
			beforeSend: function() {
				overlay.appendTo(document.body);
				overlay_inside.appendTo('div.gfb-cal-container');				
				$('#gfb-timeslots-table-container-tr').remove();
				$('#gfb-timeslots-table-container').remove();
			},
			success: function(response) {
				var is_json = true;

				try {
					var obj = $.parseJSON(response);
					console.log(obj);
				} catch(err) {
					is_json = false;
				}
				//debugger;
				console.log(response);
				if ( is_json && 'fullday' == obj.booking_type ) { //if booking type is full day
					$(`#gfb_date_${form_id}`).val(obj.date).trigger('change');
					$(`#gfb_time_${form_id}`).val(obj.time).trigger('change');
					$(`#gfb_slot_${form_id}`).val(1).trigger('change');
					$(`#gfb_cost_${form_id}`).val(obj.service_price).trigger('change');
					$(`#gfb_cost_${form_id}`).val(obj.service_price).trigger('change');
					$(`#gfb_booking_type_${form_id}`).val(obj.booking_type);				
					if ( 'right' == gfb_calendar.show_timeslot && ! (window.matchMedia("(max-width: 575px)").matches) ) {
						$(`<div id="gfb-timeslots-table-container">${obj.html}</div>`).insertAfter(parent_table);
					} else {
						$(current).addClass('gfb-caret-down');
						$(`<tr id="gfb-timeslots-table-container-tr"><td colspan="7">${obj.html}</td></tr>`).insertAfter(parent_row);	
					}
					// if ( $(`#gfb_booking_type_${form_id}`).length <= 0 ) {
					// 	$(`<input type="hidden" id="gfb_booking_type_${form_id}" name="gfb_booking_type" value="${obj.booking_type}" />`).insertAfter(`#gfb_cost_${form_id}`);
					// } else {
					// 	$(`#gfb_booking_type_${form_id}`).val(obj.booking_type);
					// }

					// Trigger gf total
			 		// if( typeof gformInitPriceFields == 'function' ) {
			 		// 	gformInitPriceFields(); // will trigger "gform_product_total" filter
			  		// }
				} else { //if booking type is custom
					if ( 'right' == gfb_calendar.show_timeslot && ! (window.matchMedia("(max-width: 575px)").matches) ) {
						$(`<div id="gfb-timeslots-table-container">${response}</div>`).insertAfter(parent_table);
					} else {
						$(current).addClass('gfb-caret-down');
						$(`<tr id="gfb-timeslots-table-container-tr"><td colspan="7">${response}</td></tr>`).insertAfter(parent_row);	
					}				
				}
			},
			error: function(jqXHR, status, error) {
				if (jqXHR.status == 500) {
                	alert('Internal error: ' + jqXHR.responseText);
              	} else {
                  	alert('Unexpected error.');
              	}
			},
			complete: function() {
				overlay.remove();
				overlay_inside.remove();
			}
		});
	});


});
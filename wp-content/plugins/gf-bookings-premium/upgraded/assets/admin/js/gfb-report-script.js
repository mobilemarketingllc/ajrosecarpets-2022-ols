const graph_1 = document.getElementById('graph-1');
const graph_2 = document.getElementById('graph-2');
const graph_3 = document.getElementById('graph-3');
const graph_4 = document.getElementById('graph-4');

jQuery( document ).ready(function($) {

	//load graph 1 data
	var data_1 = {
		action: 'gfb_report_for_appointment_status',
		nonce: gfb_report.nonce
	}

	$.post( gfb_report.ajaxurl, data_1, function( response ) {		
		var response = $.parseJSON(response);

		// console.log(response);		

		var myData = {
			labels: [ 
				response.status[0]['value'] +' '+ response.status[0]['label'], 
				response.status[1]['value'] +' '+ response.status[1]['label'], 
				response.status[2]['value'] +' '+ response.status[2]['label'], 
				response.status[3]['value'] +' '+ response.status[3]['label']
			],
			datasets: [{
				data: [response.status[0]['value'], response.status[1]['value'], response.status[2]['value'], response.status[3]['value']],
				backgroundColor:['#ffa800', '#00aeff', '#48f1a7', '#ff0000'],
			}]
		};
		
		var myChart = new Chart(graph_1, {
			type: 'doughnut',
			data: myData,
			options: {
			    responsive: true,		    
			}
		});

		$('#graph-1').siblings('#gfb-overlay').remove();
	});


	//load graph 2 data
	var data_2 = {
		action: 'gfb_report_for_staff_earning',
		nonce: gfb_report.nonce
	}

	$.post( gfb_report.ajaxurl, data_2, function( response ) {

		var response = $.parseJSON(response);

		console.log(response);
		var labels = [];
		var earnings = [];
		var colors = [];
		$.each(response.staff, function( i, v ) {
			labels.push( v.name );
			earnings.push( parseFloat(v.total_earning).toFixed(2) );
			colors.push( '#00aeff' );
		});

		var myData = {
			labels: labels,
			datasets: [{
				data: earnings,
				backgroundColor: colors,
			}]
		};
		
		var myChart = new Chart(graph_2, {
			type: 'bar',
			data: myData,
			options: {
			    responsive: true,
			    legend: {display: false}
			}
		});

		$('#graph-2').siblings('#gfb-overlay').remove();
	});


	//load graph 3 data
	var data_3 = {
		action: 'gfb_report_for_staff_appointment',
		nonce: gfb_report.nonce
	}

	$.post( gfb_report.ajaxurl, data_3, function( response ) {

		var response = $.parseJSON(response);

		console.log(response);
		var labels = [];
		var earnings = [];
		var colors = [];
		$.each(response.staff, function( i, v ) {
			labels.push( v.name );
			earnings.push( v.total_appointment );
			colors.push( '#48f1a7' );
		});

		var max_val = parseInt(Math.max.apply(Math,earnings)) + 15;

		var myData = {
			labels: labels,
			datasets: [{
				data: earnings,
				backgroundColor: colors,
			}]
		};
		
		var myChart = new Chart(graph_3, {
			type: 'bar',
			data: myData,
			precision: 0,
			options: {
			    responsive: true,
			    legend: {display: false},
			    scales: {
			     	yAxes: [{
	                    display: true,
	                    ticks: {
	                    	precision: 0,
	                        beginAtZero: true,
	                        steps: 10,
	                        stepValue: 5,
	                        max: max_val,
	                    }
                	}]
			    }
			}
		});

		$('#graph-3').siblings('#gfb-overlay').remove();
	});


	//load graph 2 data
	var data_4 = {
		action: 'gfb_report_for_service_earning',
		nonce: gfb_report.nonce
	}

	$.post( gfb_report.ajaxurl, data_4, function( response ) {

		var response = $.parseJSON(response);

		console.log(response);
		var labels = [];
		var earnings = [];		
		$.each(response.service, function( i, v ) {
			labels.push( v.name );
			earnings.push( parseFloat(v.total_earning).toFixed(2) );			
		});

		var myData = {
			labels: labels,
			datasets: [{
				data: earnings				
			}]
		};
		
		var myChart = new Chart(graph_4, {
			type: 'line',
			data: myData,
			fill: false,
			borderColor: 'rgb(75, 192, 192)',
			options: {
			    responsive: true,
			    legend: {display: false}
			}
		});

		$('#graph-4').siblings('#gfb-overlay').remove();
	});
});
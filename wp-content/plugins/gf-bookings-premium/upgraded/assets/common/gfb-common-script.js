jQuery( document ).ready(function($) {	
	const overlay = jQuery('<div id="gfb-overlay"> </div>');
	
	// alert(gfb_common.ajaxurl);
	$(document).on('change', '.gfb_location', function(e) {

		var post_id = '' == $(this).val() ? 'all' : $(this).val();
		var action = 'gfb_get_services_by_location';
		console.log( 'Location is triggered!' );

		if ( '' != post_id && 'all' != post_id ) {
			
			// $(`.gfb_location option`).removeAttr("selected");
			// $(`.gfb_location option[value='${post_id}']`).attr("selected","selected");
			// $(this).val(post_id);
			$('.gfb_location_input').val(post_id);

			$.ajax({
				url: gfb_common.ajaxurl,
				type: 'POST',
				data: `form_id=${gfb_common.form_id}&post_id=${post_id}&action=${action}&nonce=${gfb_common.nonce}`,
				beforeSend: function() {
					overlay.appendTo(document.body);
					$(`.gfb_service`).html(`<option>${gfb_common.loadingText}</option>`);
				},
				success: function (response) {
					var response = $.parseJSON(response);
					if ( 'success' == response.status ) {
						
						//add first option
						$(`.gfb_service`).html($('<option>', {
							value: 'all',
							text: gfb_common.all_service_text
						}));

						$.each( response.data, function(i, v) {
							let flag = false;
							
							if ( 'default_service' in gfb_common && v.ID == gfb_common.default_service ) {
								flag = true;
							} else if ( v.ID == response.selected_service_id ) {
								flag = true;
							}

							$(`.gfb_service`).append($('<option>', {
								value: v.ID,
								text: v.post_title,
								selected: flag
							}));
						});

					} else {
						//add first option
						$(`.gfb_service`).html($('<option>', {
							value: 'no',
							text: gfb_common.no_service_text
						}));
					}				
				},
				complete: function() {
					$('.gfb_service').trigger('change');
					$('.gfb_staff').trigger('change');
				}
			});
		}
	});

	$(document).on('change', '.gfb_service', function(e) {

		var post_id = '' == $(this).val() ? 'all' : $(this).val();
		var location_id = $('.gfb_location').val();
		var action = 'gfb_get_staff_by_service_and_location';		
		console.log( 'Service is triggered!' );

		if ( '' != post_id && 'all' != post_id ) {
			// debugger;
			// $(`.gfb_service option`).removeAttr("selected");
			// $(`.gfb_service option[value='${post_id}']`).attr("selected","selected");
			// $(this).val(post_id);
			$('.gfb_service_input').val(post_id);

			$.ajax({
				url: gfb_common.ajaxurl,
				type: 'POST',
				data: `form_id=${gfb_common.form_id}&post_id=${post_id}&location_id=${location_id}&action=${action}&nonce=${gfb_common.nonce}`,
				beforeSend: function() {
					overlay.appendTo(document.body);
					$(`.gfb_staff`).html(`<option>${gfb_common.loadingText}</option>`);
				},
				success: function (response) {
					var response = $.parseJSON(response);
					if ( 'success' == response.status ) {
						
						//add first option
						$(`.gfb_staff`).html($('<option>', {
							value: 'all',
							text: gfb_common.all_staff_text
						}));

						$.each( response.data, function(i, v) {
							let flag = false;

							if ( 'default_staff' in gfb_common && v.ID == gfb_common.default_staff ) {
								flag = true;
							} else if ( v.ID == response.selected_staff_id ) {
								flag = true;
							}

							$(`.gfb_staff`).append($('<option>', {
								value: v.ID,
								text: v.post_title,
								selected: flag
							}));
						});

					} else {
						//add first option
						$(`.gfb_staff`).html($('<option>', {
							value: 'no',
							text: gfb_common.no_staff_text
						}));
					}				
				},
				complete: function() {
					$('.gfb_staff').trigger('change');
				}
			});
		} else {
			overlay.remove();
		}
	});

	$(document).on('change', '.gfb_staff', function(e) {
		var post_id = '' == $(this).val() ? 'all' : $(this).val();
		var location_id = $('.gfb_location').val();
		var service_id = $('.gfb_service').val();
		console.log( 'staff is triggered!' );

		if ( 'no' === gfb_common.is_admin ) { // This will work for frontend only when staff value is triggered.			
			var prior_months = 	gfb_common.prior_months;
			if ( '' != post_id && 'all' != post_id ) {

				// $(`.gfb_staff option`).removeAttr("selected");
				// $(`.gfb_staff option[value='${post_id}']`).attr("selected","selected");
				// $(this).val(post_id);
				$('.gfb_staff_input').val(post_id);

				var data = {
					action: 'gfb_month_changer',
					nonce: gfb_calendar.nonce,				
					is_admin: gfb_common.is_admin,
					form_id: gfb_common.form_id,
					location_id: location_id,
					service_id: service_id,
					staff_id: post_id,
					prior_months: prior_months
				}

				$.ajax({
					url: gfb_calendar.ajaxurl,			
					type: 'POST',
					data:data,
					dataType: 'HTML',
					beforeSend: function() {
						overlay.appendTo(document.body);
					},
					success: function(response) {						
						$('#gfb-fancy-calendar').html(response);
					},
					error: function(jqXHR, status, error) {
						if (jqXHR.status == 500) {
		                	alert('Internal error: ' + jqXHR.responseText);
		              	} else {
		                  	alert('Unexpected error.');
		              	}
					},
					complete: function() {
						overlay.remove();
					}
				});
			} else {
				overlay.remove();	
			}
		} else { // This will work for backend only when staff value is triggered.			
			
			overlay.remove();			

			if ( '' != post_id ) {				 //&& 'all' != post_id
				var data = {
					action: 'gfb_month_changer',
					is_admin: gfb_common.is_admin,
					nonce: gfb_calendar.nonce,					
					location_id: location_id,
					service_id: service_id,
					staff_id: post_id
				}

				$.ajax({
					url: gfb_calendar.ajaxurl,			
					type: 'POST',
					data:data,
					dataType: 'HTML',
					beforeSend: function() {
						overlay.appendTo(document.body);
					},
					success: function(response) {						
						$('#gfb-fancy-calendar').html(response);
					},
					error: function(jqXHR, status, error) {
						if (jqXHR.status == 500) {
		                	alert('Internal error: ' + jqXHR.responseText);
		              	} else {
		                  	alert('Unexpected error.');
		              	}
					},
					complete: function() {
						overlay.remove();
					}
				});
			} else {
				overlay.remove();	
			}
		}
	});

	if ( 'default_location' in gfb_common && 'default_service' in gfb_common && 'default_staff' in gfb_common ) {

		if ( gfb_common.default_location != '' ) {
			$(`#gfb_location_${gfb_common.form_id}`).val(gfb_common.default_location).trigger('change');
		} else {
			$(`#gfb_location_${gfb_common.form_id}`).val('all').trigger('change');
		}
	}

	if ( '1' == gfb_common.is_admin ) {
		$('.gfb_staff').trigger('change');
	}	

});
const swalWithBootstrapButtons = Swal.mixin({
	customClass: {
	    confirmButton: 'gfb-btn gfb-btn-success',
	    cancelButton: 'gfb-btn gfb-btn-danger'
	},
	buttonsStyling: true
});

jQuery( document ).ready(function($) {
	$('.gfb-app-cancel-form').on('submit', function(event) {
		event.preventDefault();
		var app_id = $(this).find('input[name="gfb-app-id"]').val();
		var status = $(this).find('input[name="gfb-status"]').val();
		var reason = '';		
		swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
		  	icon: 'warning',
		  	input: 'textarea',
		  	showCancelButton: true,
		  	confirmButtonText: gfb_frontend.confirm,
		  	cancelButtonText: gfb_frontend.cancel,
		  	showLoaderOnConfirm: true,
		  	// reverseButtons: true
		}).then((result) => {
			if ( $.trim(result.value) != '' ) {
				reason = result.value;
			}

		  	if (result.isConfirmed) {
		  		$.ajax({
					url: gfb_frontend.ajaxurl,
					type: 'POST',
					data: `gfb_status=${status}&post_id=${app_id}&gfb_reason=${reason}&action=gfb_cancel_appointment_by_customer&nonce=${gfb_frontend.nonce}`,
					success: function (response) {
						var response = $.parseJSON(response);
						if ( 'success' == response.status ) {
							swalWithBootstrapButtons.fire(
					      		gfb_frontend.title,
					      		response.message,
					      		response.status
					    	).then((result) => {
					    		location.reload();
					    	});
						} else {
							swalWithBootstrapButtons.fire(
					      		gfb_frontend.title,
					      		response.message,
					      		response.status
					    	);
						}
					}
				});
		  	}
		});
	});
});
<?php
define( 'GFB_LIVE_MODE', FALSE ); // TRUE OR FALSE
define( 'GFB_URL', plugin_dir_url( __FILE__ ) );
define( 'GFB_PATH', plugin_dir_path( __FILE__ ) );
define( 'GFB_FONT_URL', plugin_dir_url( __FILE__ ) . 'assets/fonts/' );
define( 'GFB_IMAGE_URL', plugin_dir_url( __FILE__ ) . 'assets/images/' );
define( 'GFB_ADMIN_VIEW_PATH', plugin_dir_path( __FILE__ ) . 'views/admin/' );
define( 'GFB_ADMIN_JS_URL', plugin_dir_url( __FILE__ ) . 'assets/admin/js/' );
define( 'GFB_COMMON_URL', plugin_dir_url( __FILE__ ) . 'assets/common/' );
define( 'GFB_ADMIN_CSS_URL', plugin_dir_url( __FILE__ ) . 'assets/admin/css/' );
define( 'GFB_VIEW_PATH', plugin_dir_path( __FILE__ ) . 'views/frontend/' );
define( 'GFB_JS_URL', plugin_dir_url( __FILE__ ) . 'assets/frontend/js/' );
define( 'GFB_CSS_URL', plugin_dir_url( __FILE__ ) . 'assets/frontend/css/' );
define( 'GFB_FILE', __FILE__ );
define( 'GFB_VERSION', '2.3' );
define( 'GFB_SLUG', 'gfb' );

/**
 * Main class
 */

if ( ! class_exists( 'GFB_Main' ) ) {

	function gfb_randon_pass() {
	    $length = 6;
	    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $str = '';
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    
	    if ($max < 1) {
	        throw new Exception('$keyspace must be at least two characters long');
	    }
	    
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $keyspace[random_int(0, $max)];
	    }

	    return $str;
	}

	/**
	 * Restaurant main class
	 */
	class GFB_Main {

		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $classes = null;

		public static $gfb_currency = array();

		/**
		 * Class Constructor
		 */
		private function __construct() {

			/**
			 * Check for plugin dependencies
			 */
			require_once GFB_PATH . 'includes/classes/class-gfb-dependencies.php';

			

			if ( GFB_Dependencies::active_check( 'gravityforms/gravityforms.php' ) ) {		

				self::$classes = array(					
					'admin',
					'setting',
					'email',
					'appointment',			
					'location',
					'service',
					'staff',
					'customer',					
					'calendar',
					'report'					
				);

				add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );				

				/**
				 * Notice for admin For New Version
				 */
				add_action( 'admin_notices', array( $this, 'new_ver_plugin_notice' ) );

				add_action( 'admin_footer', array( $this, 'remove_notice_script' ) );

				add_action( 'admin_init', array( $this, 'plugin_notice_dismissed' ) );

				add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'add_action_links' ) );	

				add_action('gform_loaded', array($this, 'gfb_init'));				

			} else {

				/**
				 * Notice for admin
				 */
				add_action( 'admin_notices', array( $this, 'inactive_plugin_notice' ) );
			}
		}

		public function gfb_init() {

			// if ( class_exists('GFCommon') ) {}

			self::gfb_includes();
			self::includes();
		}

		public function add_action_links( $actions ) {
			$mylinks = array(
				'<a href="' . admin_url( 'admin.php?page=gfb' ) . '">' . esc_html__('Dashboard', 'gfb') . '</a>'
			);
			$actions = array_merge( $actions, $mylinks );
			return $actions;
		}

		public function plugin_notice_dismissed() {

			$user_id = get_current_user_id();

			if ( isset( $_GET['gfb-notice-dismissed'] ) ) {
				add_user_meta( $user_id, 'gfb_plugin_notice_dismissed', 'true', true );
			}
		}

		/**
		 * Notice for admin For Backward Compatibility
		 */
		public function new_ver_plugin_notice() {						
			$class     = 'notice notice-warning is-dismissible gfb-is-dismissible';
			$headline  = __( 'Gravity Booking version ' . GFB_VERSION, 'gfb' );
			$message   = __( 'is packed with a ton of new features an entirely new back-end. If you have any questions, concerns, or problems regarding the new changes, please read our', 'gfb' );
			$tech_text = 'technical documentation';

			$user_id = get_current_user_id();
			$user = get_user_by('id', $user_id);
			if ( in_array('administrator', $user->roles ) ) {
				if ( ! get_user_meta( $user_id, 'gfb_plugin_notice_dismissed' ) ) {
					printf( '<div class="%1$s"><h2>%2$s</h2><p>%3$s <a href="https://gravitymore.com/documentation/bookings-for-gravity-forms/" target="_blank">%4$s</a></p></div>', esc_attr( $class ), esc_html( $headline ), esc_html( $message ), esc_html( $tech_text ) );
				}
			}
		}

		public function remove_notice_script() {
			?>
			<script>
				jQuery(document).on( 'click', '.gfb-is-dismissible button', function(){
					window.location.href = '?gfb-notice-dismissed';
				});
			</script>
			<?php
		}

		/**
		 * Plugin Domain loaded
		 */
		public function load_textdomain() {
			load_plugin_textdomain( GFB_SLUG, false, basename( dirname( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Inactive plugin notice.
		 */
		public function inactive_plugin_notice() {
			$class    = 'notice notice-error';
			$headline = __( 'Gravity Booking requires Gravity Form to be install & active.', 'gfb' );
			$message  = __( 'Go to the plugins page to activate Gravity Form plugin', 'gfb' );
			printf( '<div class="%1$s"><h2>%2$s</h2><p>%3$s</p></div>', esc_attr( $class ), esc_html( $headline ), esc_html( $message ) );
		}

		/**
		 * Files to includes
		 */
		private static function includes() {

			require_once GFB_PATH . 'includes/helper/gfb-functions.php';

			if ( is_array( self::$classes ) && count( self::$classes ) > 0 ) {
				foreach( self::$classes as $class ) {
					require_once GFB_PATH . '/includes/classes/class-gfb-' . $class . '.php';
				}
			}
		}

		/**
		 * Files to includes GFB Related files
		 */
		private static function gfb_includes() {
			// wp_die('working!');
			// adding files related to GF Settings
			require_once GFB_PATH . 'includes/gravity-form/class-gfb-form-setting.php';
			require_once GFB_PATH . 'includes/gravity-form/gfb-fields/class-gfb-appointment-calendar-field.php';
		}

		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Main();
			}

			return self::$instance;
		}

	}

	GFB_Main::get_instance();

}

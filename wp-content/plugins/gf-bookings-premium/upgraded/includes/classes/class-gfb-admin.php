<?php
/**
 * GFB Admin Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Admin' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Admin {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			add_action( 'admin_menu', array( $this, 'register_main_menu') );

			add_action('admin_enqueue_scripts', array($this, 'register_admin_assets') );

		}

		public function register_admin_assets( $hook ) {

			global $current_screen;

// 			echo '<pre>$current_screen$current_screen';
// 			print_r( $current_screen );
// 			echo '</pre>';

			$_posttypes = array(
				'gfb-location',
				'gfb-service',
				'gfb-staff',
				'gfb-customer',
				'gfb-customer',
				'gfb-calendar',
				'gfb-appointment'
			);
			
			$_pages = array(
				'gravity-booking_page_gfb-report',
				'toplevel_page_gfb',
				'gravity-booking_page_gfb-email',
				'gravity-booking_page_gfb-account',
				'gravity-booking_page_gfb-affiliation',
				'gravity-booking_page_gfb-setting',
				'edit-gfb-service-category',
				'toplevel_page_gfb',
				'gravity-booking_page_gfb-calendar'				
			);			
			
			if ( GFB_LIVE_MODE ) {
				$minimized = '.min';
				$clear_cache = '';
			} else {
				$minimized = '';
				$clear_cache = '?t=' . gmdate('ymdhis');
			}

			?>
			<style>
				ul#adminmenu li > a[href="post-new.php?post_type=gfb-appointment"], 
				ul#adminmenu li > a[href="post-new.php?post_type=gfb-staff"] {
					display: none!important;
				}
			</style>
			<?php
			
			$cond1 = ( isset($current_screen->id) && 'toplevel_page_gf_edit_forms' === $current_screen->id && isset($_GET['page']) && 'gf_edit_forms' === sanitize_text_field($_GET['page']) && isset($_GET['id']) && ! empty( sanitize_text_field($_GET['id']) ) );			
			
			$cond2 = ( isset($current_screen->id) &&  in_array($current_screen->id, $_pages) );
			
			$cond3 = ( isset($current_screen->id) && 'toplevel_page_gf_edit_forms' === $current_screen->id && isset($_GET['subview']) && 'gfb_settings_page' === sanitize_text_field($_GET['subview']) );
			
			$cond4 = ( isset($current_screen->post_type) && in_array($current_screen->post_type, $_posttypes) );

			if (  $cond1 || $cond2 || $cond3 || $cond4 ) {				
				?>
				<link href="<?php echo GFB_FONT_URL . "Poppins-Bold.ttf"; ?>" rel="stylesheet">
			    <link href="<?php echo GFB_FONT_URL . "Poppins-Light.ttf"; ?>" rel="stylesheet">
			    <link href="<?php echo GFB_FONT_URL . "Poppins-Medium.ttf"; ?>" rel="stylesheet">
			    <link href="<?php echo GFB_FONT_URL . "Poppins-Regular.ttf"; ?>" rel="stylesheet">
			    <link href="<?php echo GFB_FONT_URL . "Poppins-SemiBold.ttf"; ?>" rel="stylesheet">		    
				<?php

				wp_enqueue_style( 'gfb-font-awesome-style', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), GFB_VERSION . $clear_cache, 'all' );

				wp_enqueue_style( 'gfb-posttype-style', GFB_ADMIN_CSS_URL . 'gfb-posttype-style' . $minimized . '.css', array(), GFB_VERSION . $clear_cache, 'all' );

				wp_enqueue_style( 'gfb-admin-style', GFB_ADMIN_CSS_URL . 'gfb-admin-style' . $minimized . '.css', array(), GFB_VERSION . $clear_cache, 'all' );

				// '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css'
				wp_enqueue_style( 'gfb-daterangepicker', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css', array(), GFB_VERSION . $clear_cache, 'all' );				

				//Add the Select2 CSS file
			    wp_enqueue_style( 'gfb-select2-style', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), GFB_VERSION . $clear_cache, 'all');

			    //Add the Select2 JavaScript file
			    wp_enqueue_script( 'gfb-select2-script', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array('jquery'), GFB_VERSION . $clear_cache, true);

			    //Add the jQuery Validation JavaScript file
			    wp_enqueue_script( 'gfb-jquery-validation', GFB_ADMIN_JS_URL . 'gfb-jquery-validate.min.js', array('jquery'), GFB_VERSION, true);

			    if ( isset($current_screen->id) && 'gravity-booking_page_gfb-report' === $current_screen->id || 'toplevel_page_gfb' === $current_screen->id ) {
					wp_enqueue_script( 'gfb-chart', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js', array('jquery'), GFB_VERSION . $clear_cache, true );
					wp_register_script( 'gfb-report-script', GFB_ADMIN_JS_URL . 'gfb-report-script' . $minimized . '.js', array('jquery', 'gfb-chart'), GFB_VERSION . $clear_cache, true );

					wp_localize_script( 'gfb-report-script', 'gfb_report', array(
						'ajaxurl'  => admin_url('admin-ajax.php'),
						'nonce' => wp_create_nonce('gfb')
					));				

					wp_enqueue_script('gfb-report-script');
				}

			    //Range date picker script
			    wp_enqueue_script( 'gfb-moment-script', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js', array('jquery'), GFB_VERSION . $clear_cache, true);
			    //'//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js'
			    wp_enqueue_script( 'gfb-daterangepicker', '//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', array('gfb-moment-script'), GFB_VERSION . $clear_cache, true);
							
				wp_enqueue_script( 'gfb-sweetalert-script', '//cdn.jsdelivr.net/npm/sweetalert2@11', array('jquery'), GFB_VERSION . $clear_cache, true );

				wp_register_script( 'gfb-admin-script', GFB_ADMIN_JS_URL . 'gfb-admin-script' . $minimized . '.js', array('jquery', 'jquery-ui-accordion', 'gfb-sweetalert-script', 'gfb-select2-script', 'gfb-daterangepicker', 'gfb-jquery-validation'), GFB_VERSION . $clear_cache, true );
				wp_localize_script( 'gfb-admin-script', 'gfb_admin', array(
					'ajaxurl'  => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('gfb')
				));				

				wp_enqueue_script('gfb-admin-script');			

				wp_register_script( 'gfb-common-script', GFB_COMMON_URL . 'gfb-common-script' . $minimized . '.js', array('jquery'), GFB_VERSION . $clear_cache, true );
				wp_localize_script( 'gfb-common-script', 'gfb_common', array(
					'ajaxurl'  => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('gfb'),
					'form_id' => isset( $_GET['id'] ) ? sanitize_text_field( $_GET['id'] ) : '',
					'is_admin' => is_admin(),
					'loadingText' => __('loading...', 'gfb'),
					'all_service_text' => __('All Service', 'gfb'),
					'no_service_text' => __('No Service Found', 'gfb'),
					'all_location_text' => __('All Location', 'gfb'),
					'no_location_text' => __('No Location Found', 'gfb'),
					'all_staff_text' => __('All Staff', 'gfb'),
					'no_staff_text' => __('No Staff Found', 'gfb'),
					'saved'         => __('Setting Saved', 'gfb'),
				));

				wp_enqueue_script('gfb-common-script');				

			}
		}

		public function register_main_menu() {

			add_menu_page( 
                __( 'Dashboard - Gravity Booking', 'gfb' ),
                __( 'Gravity Booking', 'gfb' ),
                'manage_options',
                'gfb',
                array( get_class($this), 'gfb_dashboard_page' ),
                'dashicons-calendar-alt',
                10
            );

            // add_submenu_page(
            // 	'gfb',
            //     __( 'Report - Gravity Booking', 'gfb' ),
            //     __( 'Dashboard', 'gfb' ),
            //     'manage_options',
            //     'gfb-report',
            //     array( get_class($this), 'gfb_dashboard_page' ),
            //     0
            // );

            do_action( 'gfb_after_menu_register' );         
		}

		public static function gfb_dashboard_page() {
			require_once GFB_ADMIN_VIEW_PATH . 'reports.php';
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Admin();
			}

			return self::$instance;
		}
	}

	GFB_Admin::get_instance();
}

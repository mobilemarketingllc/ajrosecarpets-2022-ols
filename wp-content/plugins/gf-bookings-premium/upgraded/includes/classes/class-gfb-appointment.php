<?php
/**
 * GFB Appointment Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Appointment' ) ) {
	
	class GFB_Appointment {
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $checkPostType = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			add_action('init', array($this, 'register_appointment_posttype'));

			add_filter( 'manage_gfb-appointment_posts_columns', array( $this, 'gfb_modify_column_names') );
			add_action( 'manage_gfb-appointment_posts_custom_column', array( $this, 'gfb_add_custom_column'), 9, 2 );

			self::$checkPostType = isset( $_REQUEST['post_type'] ) && ! empty( sanitize_text_field($_REQUEST['post_type']) ) ? sanitize_text_field($_REQUEST['post_type']) : '';

			if ( 'gfb-appointment' === self::$checkPostType ) {		

				add_action( 'admin_footer', array( $this, 'gfb_appointment_modal') );

				add_filter('post_row_actions', '__return_empty_array');				

				add_filter('months_dropdown_results', '__return_empty_array');

				add_filter( 'bulk_actions-edit-gfb-appointment', '__return_empty_array' );

				// HTML of the filter
				add_action( 'restrict_manage_posts', array( $this, 'gfb_add_filters' ) );

				// the function that filters posts
				add_action( 'pre_get_posts', array( $this, 'gfb_filter_query' ) );

				add_action( 'views_edit-gfb-appointment',  function ( $views ) {

					//add 4 block on top of the page.
					require_once GFB_ADMIN_VIEW_PATH . 'block_data_reports.php';

					unset($views['all']);
					unset($views['publish']);
    				unset($views['trash']);
    				unset($views['draft']);

					return $views;
				});
			}

			add_action( 'gform_after_submission', array( $this, 'create_appointment'), 10, 2 );

			add_filter( 'gform_confirmation', array($this, 'gfb_add_to_calendar_button'), 11, 3 );

			add_filter('gform_product_info', array($this, 'gfb_add_appointment_fee'), 4, 3);

			add_filter( 'gform_pre_render', array( $this, 'add_default_value_to_fields'), 99, 3 );

			//paypal specific to update status
			add_action( 'gform_post_payment_status', array( $this, 'gfb_paypal_payment_status'), 99, 8 );

			add_action( 'wp_ajax_gfb_delete_appointment', array( $this, 'gfb_delete_appointment' ) );
			add_action( 'wp_ajax_gfb_change_appointment_status', array( $this, 'gfb_change_appointment_status' ) );

			add_action('wp_ajax_gfb_show_appointment_data', array($this, 'gfb_show_appointment_data'));

			add_action('template_redirect', array($this, 'gfb_frontend_gcal_auth'));
		}

		public function gfb_frontend_gcal_auth() {			

			// Google passes a parameter 'code' in the Redirect Url
			if ( isset($_GET['code']) ) {

				global $wp;

				require_once GFB_PATH . '/includes/classes/class-gfb-gcal-api.php';

				$capi = new GFB_CAL_API();

				$redirect_url = home_url($wp->request);

				$gfb_settings = GFB_Setting::get_settings();

				try {
					// Get the access token 
					$data = $capi->GetAccessToken($gfb_settings['gfb_gcal_client_id'], $redirect_url, $gfb_settings['gfb_gcal_secret_key'], $_GET['code']);
					
					// Save the access token as a session variable
					if ( isset($data['access_token']) ) {
						$appointment_id = gform_get_meta( $_GET['state'], 'gfb_appointment' );
						//$wp_user = get_post_meta( $appointment_id, 'wp_user', true );
						$staff_id = get_post_meta( $appointment_id, 'staff_id', true );
						$staff_wp_user = get_post_meta( $staff_id, 'wp_user', true );
						$service_id = get_post_meta( $appointment_id, 'service_id', true );
						$date = get_post_meta( $appointment_id, 'date', true );
						$time = get_post_meta( $appointment_id, 'time', true );
						$slot = get_post_meta( $appointment_id, 'slot', true );

						/* ADD to Google Calendar START */						
						$userdata = get_user_by('id', $staff_wp_user);
						$staff_name = $userdata->first_name . ' ' . $userdata->last_name;						
						$summary = ('all' != $service_id) ? 'App# ' . $appointment_id . ' ' . get_the_title($service_id) : '';
						$description = sprintf('%1$s: %2$s <br/> %3$s: %4$s', __('Staff Name', 'gfb'), $staff_name, __('Slot Qty', 'gfb'), $slot );
						$appointmentTime = explode('-', $time);
						$startTime = isset($appointmentTime[0]) ? trim($appointmentTime[0]) : '';
						$endTime = isset($appointmentTime[1]) ? trim($appointmentTime[1]) : '';
						$event_time = array();
						$event_time['start_time'] = $date . ' ' . $startTime . ':00';
						$event_time['end_time'] = $date . ' ' . $endTime . ':00';
						$staff_data = GFB_Staff::get_staff_by_id( $staff_id );
						// Set your timezone
						if ( '' != $staff_data['timezone'] ) {
							$staff_timezone = gfb_timezone_string($staff_id); //Staff timezone.
						} else {
							$staff_timezone = gfb_timezone_string(); //WordPress timezone.
						}						
						
						$startTime = $event_time['start_time'];
						$endTime = $event_time['end_time'];

						date_default_timezone_set($staff_timezone);

						$startTime = strtotime($startTime);
						$endTime = strtotime($endTime);

						date_default_timezone_set('UTC');

						$startTime = date('Y-m-d H:i:s__', $startTime);
						$endTime = date('Y-m-d H:i:s__', $endTime);
						
						$startTime = str_replace(' ', 'T', $startTime);
						$startTime = str_replace('__', '.000Z', $startTime);

						$endTime = str_replace(' ', 'T', $endTime);
						$endTime = str_replace('__', '.000Z', $endTime);						

						$event_time['start_time'] = $startTime;
						$event_time['end_time'] = $endTime;						

						// Get user calendar timezone
						$user_timezone = $capi->GetUserCalendarTimezone($data['access_token']);

						// Create event on primary calendar
						$event_id = $capi->CreateCalendarEvent('primary', $summary, $description, $staff_timezone, $event_time, $user_timezone, $data['access_token'], 'customer');
						update_post_meta($appointment_id, 'gcal_customer_event_id', $event_id);
						/* ADD to Google Calendar END */
					}
					
				} catch(Exception $e) {
					echo $e->getMessage();
					exit();
				}
			}
		}

		public function gfb_add_to_calendar_button( $confirmation, $form, $entry ) {

			global $wp;		

			$gfb_settings = GFB_Setting::get_settings();

			if ( isset($gfb_settings['gfb_gcal_switch']) && 'enabled' === $gfb_settings['gfb_gcal_switch'] ) {
				$client_id = isset($gfb_settings['gfb_gcal_client_id']) && ! empty(trim($gfb_settings['gfb_gcal_client_id'])) ? $gfb_settings['gfb_gcal_client_id'] : '';
			} else {
				$client_id = '';
			}

			$redirect_url = home_url($wp->request);
			$state = 	$entry['id'];

			if ( ! empty( $client_id ) ) {
				
				$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($redirect_url) . '&response_type=code&client_id=' . $gfb_settings['gfb_gcal_client_id'] . '&access_type=offline&state=' . $state . '&prompt=consent';
			
				$confirmation .= '<a href="' . esc_url($login_url) . '" id="gfb-gcal-connect">' . esc_html__('Add To My Calendar', 'gfb') . '</a>';

			}

		    return $confirmation;
		    
		}

		public static function gfb_appointments_by_services( $service_id = '' ) {			
			
			if ( empty( $service_id ) ) {
				return false;
			}

			$args = array(
				'fields' => 'ids',
				'post_type' => 'gfb-appointment',
				'numberposts' => -1,
				'post_status' => 'draft',
				'meta_query' => array(
					'relation' => 'AND',				
					array(
						'key' => 'service_id',
						'value' => trim($service_id),
						'compare' => '='
					)
				)
			);

			return get_posts( $args );
		}

		public static function gfb_appointments_by_staff( $staff_id = '' ) {			
			
			if ( empty( $staff_id ) ) {
				return false;
			}

			$args = array(
				'fields' => 'ids',
				'post_type' => 'gfb-appointment',
				'numberposts' => -1,
				'post_status' => 'draft',
				'meta_query' => array(
					'relation' => 'AND',				
					array(
						'key' => 'staff_id',
						'value' => trim($staff_id),
						'compare' => '='
					)
				)
			);

			return get_posts( $args );
		}

		public static function gfb_appointments_by_status( $status = 'pending' ) {
			$args = array(
				'fields' => 'ids',
				'post_type' => 'gfb-appointment',
				'numberposts' => -1,
				'post_status' => 'draft',
				'meta_query' => array(
					'relation' => 'AND',				
					array(
						'key' => 'status',
						'value' => trim($status),
						'compare' => '='
					)
				)
			);

			return get_posts( $args );
		}

		public function gfb_show_appointment_data() {
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(			
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-appointment' == $post->post_type ) {
					$appointment_id = $post->ID;
					$location_id = get_post_meta($appointment_id, 'location_id', true);
					$location = ! empty( get_the_title( $location_id ) ) ? get_the_title( $location_id ) : get_post_meta($appointment_id, 'location_name', true);
					$service_id = get_post_meta($appointment_id, 'service_id', true);
					$service = ! empty( get_the_title( $service_id ) ) ? get_the_title( $service_id ) : get_post_meta($appointment_id, 'service_name', true);
					$staff_id = get_post_meta($appointment_id, 'staff_id', true);
					$staff = ! empty( get_the_title( $staff_id ) ) ? get_the_title( $staff_id ) : get_post_meta($appointment_id, 'staff_name', true);
					$user_id = get_post_meta($appointment_id, 'wp_user', true);
					$userdata = get_userdata( $user_id );
					if ( $userdata ) {
						$customer = $userdata->first_name . ' ' . $userdata->last_name;
					} else {
						$customer = '';
					}
					$form_id = get_post_meta($appointment_id, 'form_id', true);
					$entry_id = get_post_meta($appointment_id, 'entry_id', true);
					$date = get_post_meta($appointment_id, 'date', true);
					$date = apply_filters('gfb_staff_date_slot', $date);
					$time = get_post_meta($appointment_id, 'time', true);
					$time = apply_filters('gfb_staff_time_slot', $time);
					$slot = get_post_meta($appointment_id, 'slot', true);
					$cost = get_post_meta($appointment_id, 'cost', true);
					$cost = gfb_price_format($cost);
					$status = get_post_meta($appointment_id, 'status', true);
					$booking_type = ! empty(get_post_meta($appointment_id, 'booking_type', true)) ? get_post_meta($appointment_id, 'booking_type', true) : 'custom';

					$response['gfb-appointment_id'] = $appointment_id;
					$response['gfb-location_name'] = $location;
					$response['gfb-service_name'] = $service;
					$response['gfb-staff_name'] = $staff;
					$response['gfb-customer_name'] = $customer;
					$response['gfb-schedule'] = $date . ' ' . $time;
					$response['gfb-slot'] = $slot;
					$response['gfb-cost'] = $cost;
					$response['gfb-status'] = $status;
					$response['gfb-booking_type'] = $booking_type;
					$response['gfb-form_id'] = $form_id;
					$response['gfb-entry_id'] = $entry_id;
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_appointment_modal() {
			//adding customer modal view file
			require_once GFB_ADMIN_VIEW_PATH . 'modal/appointment-modal.php';
		}

		public static function gfb_show_appointments_on_calendar( $data ) {

			$appointmentData = array();

			// echo '<pre>$data$data';
			// print_r( $data );
			// echo '</pre>';			

			if ( isset($data['staff_id']) && ! empty($data['staff_id']) ) {

				$staff_meta = array();
				if ( isset($data['staff_id']) && 'all' != $data['staff_id'] ) {
					$staff_meta = array(
						'key' => 'staff_id',
						'value' => trim( $data['staff_id'] ),
						'compare' => '='
					);
				}

				$location_meta = array();
				if ( isset($data['location_id']) && 'all' != $data['location_id'] ) {
					$location_meta = array(
						'key' => 'location_id',
						'value' => trim( $data['location_id'] ),
						'compare' => '='
					);
				}

				$service_meta = array();
				if ( isset($data['service_id']) && 'all' != $data['service_id'] ) {
					$service_meta = array(
						'key' => 'service_id',
						'value' => trim( $data['service_id'] ),
						'compare' => '='
					);
				}

				$args = array(
					'fields' => 'ids',
					'post_type' => 'gfb-appointment',
					'numberposts' => -1,
					'post_status' => 'draft',
					'meta_query' => array(
						'relation' => 'AND',				
						// array(
						// 	'key' => 'staff_id',
						// 	'value' => trim( $data['staff_id'] ),
						// 	'compare' => '='
						// ),
						$staff_meta,
						$location_meta,
						$service_meta

					)
				);

				$appointmentData = get_posts( $args );		        
		    }

		    return $appointmentData;    
		}

		public static function gfb_total_collection() {
			$total_collection = 0;

			$args = array(
				'post_type' => 'gfb-appointment',
				'numberposts' => -1,
				'post_status' => 'draft',
				'meta_query' => array(
					'relation' => 'AND',				
					array(
						'key' => 'status',
						'value' => array('visited', 'confirmed'),
						'compare' => 'IN'
					)
				)
			);

			$appointmentData = get_posts( $args );			

			if ( is_array( $appointmentData ) && count( $appointmentData ) > 0 ) {
				foreach ($appointmentData as  $appointment) {
					$cost = get_post_meta( $appointment->ID, 'cost', true );
					$total_collection += $cost;
				}				
			}

			return $total_collection;
		}	

		public function gfb_filter_query( $admin_query ) {

			if ( is_admin() && $admin_query->is_main_query() ) {
				$data = $_GET;				

				if ( is_user_logged_in() ) {
					if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
						$current_user_id = get_current_user_id();
						$staff_id = get_user_meta($current_user_id, 'gfb_user', true);
						$cond_arr[] = array(
							array(
								'key'     => 'staff_id',
								'value'   => $staff_id,
								'compare' => '='								
							),
						);
					}
				}

				if ( isset($data['date_from']) && isset($data['date_to']) && !empty($data['date_from']) && !empty($data['date_to']) ) {

					$cond_arr[] = array(
						'date_clause' => array(
							'key'     => 'date',
							'value'   => array( $data['date_from'], $data['date_to'] ),
							'compare' => 'between',
							'type'    => 'date'
						),
					);				
				} else {
					$cond_arr[] = array(
						'date_clause' => array(
							'key'     => 'date',
							'value'   => array(),
							'compare' => 'between',
							'type'    => 'date'
						),
					);					
				}
				
				if( absint($admin_query->query_vars['s']) ) {
		            // Set the post id value
		            $admin_query->set('p', $admin_query->query_vars['s']);

		            // Reset the search value
		            $admin_query->set('s', '');
		        }

				if ( isset( $cond_arr ) && is_array($cond_arr) && count($cond_arr) > 0 ) {
					$admin_query->set(
						'meta_query',
						$cond_arr
					);

					// $orderby = array(
					// 	'date_clause' => 'DESC'
					// );

					//$admin_query->set('orderby', $orderby);
				}
			}

			return $admin_query;
		}

		public function gfb_add_filters( $post_type ) {
			require_once GFB_ADMIN_VIEW_PATH . 'appointment-filters.php';
		}		

		public function gfb_change_appointment_status() {
			
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Appointment status has not been changed.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( isset($data['post_id']) && isset( $data['status'] ) ) {
		        update_post_meta( $data['post_id'], 'status', $data['status'] );		        
		        
		        if ( 'confirmed' == $data['status'] ) {
		        	GFB_Email::send_notification( $data['post_id'], 'template_1', 'customer' ); # template_1
		        }

		        if ( 'visited' == $data['status'] ) {
		        	GFB_Email::send_notification( $data['post_id'], 'template_2', 'customer' ); # template_2
		        	GFB_Email::send_notification( $data['post_id'], 'template_4', 'admin' ); # template_4
		        }

		        if ( 'cancelled' == $data['status'] ) {
		        	GFB_Email::send_notification( $data['post_id'], 'template_3', 'customer' ); # template_3
		        	GFB_Email::send_notification( $data['post_id'], 'template_6', 'admin' ); # template_6
		        	GFB_Email::send_notification( $data['post_id'], 'template_7', 'staff' ); # template_7
		        }

		        $response['message'] = esc_html__( 'Appointment status has been changed successfully.', 'gfb' );
				$response['status'] = 'success';
		    }

		    echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_delete_appointment() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'appointment not deleted.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {
				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-appointment' == $post->post_type ) {
					$entry_id = get_post_meta( $post->ID, 'entry_id', true );
					if ( ! empty($entry_id) ) {
						\GFAPI::delete_entry( $entry_id );
					}
					wp_delete_post( $post->ID, true );
					$response['message'] = esc_html__( 'Appointment deleted successfully.', 'gfb' );
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_add_custom_column( $column, $postId ) {			

			$date = get_post_meta( $postId, 'date', true );
            $date = apply_filters('gfb_appointment_date_format', $date);
            $time = get_post_meta( $postId, 'time', true );
            $time = apply_filters('gfb_appointment_time_format', $time);
            $user_id = get_post_meta( $postId, 'wp_user', true );
            $userdata = get_user_by('id', $user_id);
            $customer = '';
            if ( $userdata ) {
            	$customer = $userdata->display_name;
            }
            $location = get_post_meta( $postId, 'location_id', true );
            $location = ! empty( get_the_title( $location ) ) ? get_the_title( $location ) : get_post_meta($postId, 'location_name', true);
            $service = get_post_meta( $postId, 'service_id', true );            
            $service = ! empty( get_the_title( $service ) ) ? get_the_title( $service ) : get_post_meta($postId, 'service_name', true);
            $staff = get_post_meta( $postId, 'staff_id', true );            
            $staff = ! empty( get_the_title( $staff ) ) ? get_the_title( $staff ) : get_post_meta($postId, 'staff_name', true);
            $capacity = get_post_meta( $postId, 'slot', true );
            $cost = get_post_meta( $postId, 'cost', true );
            $cost = gfb_price_format($cost);
            $status = get_post_meta( $postId, 'status', true );
            $cancel_reason = get_post_meta( $postId, 'cancel_reason', true );
            $booking_type = get_post_meta( $postId, 'booking_type', true );

            if ( 'fullday' == $booking_type ) {
            	$fullday = esc_html__('Full Day', 'gfb');
            } else {
            	$fullday = $time;
            }            
			
			switch ($column) {				

				case 'id':
					echo  '&nbsp;&nbsp;' . esc_attr($postId);
					break;

				case 'app_date_time':
					echo esc_attr($date) . '<br/>' . esc_attr($fullday);
					break;

				case 'customer':
					echo esc_attr($customer);
					break;

				case 'staff':
					echo esc_attr($staff);
					break;
				
				case 'service':
					echo esc_attr($service);
					break;

				case 'payment':
					echo esc_attr($cost);
					break;

				case 'status':
					if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
						$gfb_settings = GFB_Setting::get_settings();
						if ( ( isset($gfb_settings['gfb_appointment_policy']) && 'staff' == $gfb_settings['gfb_appointment_policy'] ) && 'cancelled' !== $status && 'visited' !== $status ) {
							echo '<select data-post_id="' . esc_attr($postId) . '" class="gfb-appointment-status">
								<option value="pending" ' . selected($status, 'pending', false) . '>'. esc_html__('Pending', 'gfb') .'</option>
								<option value="confirmed" ' . selected($status, 'confirmed', false) . '>'. esc_html__('Confirmed', 'gfb') .'</option>
								<option value="visited" ' . selected($status, 'visited', false) . '>'. esc_html__('Visited', 'gfb') .'</option>
								<option value="cancelled" ' . selected($status, 'cancelled', false) . '>'. esc_html__('Cancelled', 'gfb') .'</option>
							</select>';
						} else {
							echo esc_attr( strtoupper($status) );
							if ( ! empty(trim($cancel_reason)) ) {
								echo '<br/> <a href="#" class="gfb_show_reason" data-reason="' . esc_html($cancel_reason) . '">' . esc_html__('Reason', 'gfb') . '</a>';
							}							
						}
					} else {

						if ( 'cancelled' !== $status && 'visited' !== $status ) {
							echo '<select data-post_id="' . esc_attr($postId) . '" class="gfb-appointment-status">
								<option value="pending" ' . selected($status, 'pending', false) . '>'. esc_html__('Pending', 'gfb') .'</option>
								<option value="confirmed" ' . selected($status, 'confirmed', false) . '>'. esc_html__('Confirmed', 'gfb') .'</option>
								<option value="visited" ' . selected($status, 'visited', false) . '>'. esc_html__('Visited', 'gfb') .'</option>
								<option value="cancelled" ' . selected($status, 'cancelled', false) . '>'. esc_html__('Cancelled', 'gfb') .'</option>
							</select>';
						} else {
							echo esc_attr( strtoupper($status) );
							if ( ! empty(trim($cancel_reason)) ) {
								echo '<br/> <a href="#" class="gfb_show_reason" data-reason="' . esc_html($cancel_reason) . '">' . esc_html__('Reason', 'gfb') . '</a>';
							}
						}
					}
					break;
				
				case 'actions':
					echo '<a href="#" data-type="appointment" data-id="gfb-appointment-modal" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-edit"> <i class="fa fa-info-circle customer--icon"></i></a>';
					if ( 'cancelled' == $status ) {
						echo '<a href="#" data-type="appointment" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-delete" title="Delete"> <i class="fa fa-trash customer--icon"></i></a>';
					}				
					break;
			}
		}

		public function gfb_modify_column_names( $columns ) {			
			unset($columns['date']);
			unset($columns['title']);
			unset($columns['cb']);	

			$columns['id'] = __('&nbsp;&nbsp;Booking ID', 'gfb');
			$columns['app_date_time'] = __('Appointment Date', 'gfb');
			$columns['customer'] = __('Customer', 'gfb');
			$columns['staff'] = __('Staff', 'gfb');
			$columns['service'] = __('Service', 'gfb');
			$columns['payment'] = __('Payment', 'gfb');
			$columns['status'] = __('Status', 'gfb');
			$columns['actions'] = __('Actions', 'gfb');

			return $columns;
		}

		public static function gfb_get_appointments_by_customer( $user_id = '' ) {

			if ( empty( $user_id ) ) {
				return false;
			}

			$args = array(
				'post_type' => 'gfb-appointment',
				'numberposts' => -1,
				'post_status' => 'draft',
				'meta_query' => array(
					'relation' => 'AND',				
					array(
						'key' => 'wp_user',
						'value' => $user_id,
						'compare' => '='
					)
				)
			);

			return get_posts($args);

		}

		public static function gfb_all_appointment_by_date_time( $staff_id, $date, $formatted_slot, $type = 'custom' ) {

			$slot_count = 0;

			if ( 'fullday' == $type ) {

				if ( ! empty( $date ) ) {
					$args = array(
						'post_type' => 'gfb-appointment',
						'numberposts' => -1,
						'post_status' => 'draft',
						'meta_query' => array(
							'relation' => 'AND',
							array(
								'key' => 'staff_id',
								'value' => $staff_id,
								'compare' => '='
							),
							array(
								'key' => 'date',
								'value' => $date,
								'compare' => '='
							),
							array(
								'key' => 'booking_type',
								'value' => $type,
								'compare' => '='
							),
							array(
								'key' => 'status',
								'value' => 'cancelled',
								'compare' => '!='
							)
						)
					);

					$appointments = get_posts($args);
					if ( is_array( $appointments ) && count( $appointments ) > 0 ) {					
						foreach ( $appointments as $appointment ) {
							$slot = get_post_meta( $appointment->ID, 'slot', true );
							$slot_count += $slot;
						}
					}
				}

			} else {

				if ( ! empty( $date ) && ! empty( $formatted_slot ) ) {
					$args = array(
						'post_type' => 'gfb-appointment',
						'numberposts' => -1,
						'post_status' => 'draft',
						'meta_query' => array(
							'relation' => 'AND',
							array(
								'key' => 'staff_id',
								'value' => $staff_id,
								'compare' => '='
							),
							array(
								'key' => 'date',
								'value' => $date,
								'compare' => '='
							),
							array(
								'key' => 'time',
								'value' => $formatted_slot,
								'compare' => '='
							),
							array(
								'key' => 'status',
								'value' => 'cancelled',
								'compare' => '!='
							)
						)
					);

					$appointments = get_posts($args);
					if ( is_array( $appointments ) && count( $appointments ) > 0 ) {					
						foreach ( $appointments as $appointment ) {
							$slot = get_post_meta( $appointment->ID, 'slot', true );
							$slot_count += $slot;
						}
					}
				}

			}

			return $slot_count;
		}

		/**
		*
		* This function is used to passed amount as product so third party payment methods addon can process the payments.
		*/
		public function gfb_add_appointment_fee($product_info, $form, $lead) {			
			
			$form_id = $form['id'];
			$id = gfb_field_exist( $form );
			$lead_id = $lead['id'];

			$_POSTED = $_POST;

			if ( $id && isset( $_POSTED['input_' . $id]['service'] ) && isset( $_POSTED['input_' . $id]['staff'] ) && isset( $_POSTED['input_' . $id]['cost'] ) && isset( $_POSTED['input_' . $id]['slot'] ) ) {		

				$service_id = $_POSTED['input_' . $id]['service'];
				$staff_id = $_POSTED['input_' . $id]['staff'];
				$cost = (float) $_POSTED['input_' . $id]['cost'];				
				$slot = (int) $_POSTED['input_' . $id]['slot'];
				$service_title = get_the_title( $service_id );
				$staffData = GFB_Staff::get_staff_by_id( $staff_id );

				if ( isset( $staffData['services'] ) && is_array( $staffData['services'] ) && count( $staffData['services'] ) > 0 ) {
					if ( isset( $staffData['services'][ $service_id ]['available'] ) && 'yes' == $staffData['services'][ $service_id ]['available'] ) {
						$service_price = (float) $staffData['services'][ $service_id ]['price'];
						$service_price = ('' == $service_price) ? GFB_Service::get_service_by_id($service_id)['price'] : $service_price;
						$product_info['products']['appointment_cost'] = array('name' => $service_title, 'price' => $service_price, 'quantity' => $slot);
					}
				}
			} else {		

				$appointment_data = gform_get_meta( $lead_id, $id, true );		

				if ( $id && is_array( $appointment_data )  ) {
					$service_id = $appointment_data['service'];
					$staff_id = $appointment_data['staff'];
					$cost = (float) $appointment_data['cost'];
					$slot = (int) $appointment_data['slot'];
					$service_title = get_the_title( $service_id );
					$staffData = GFB_Staff::get_staff_by_id( $staff_id );
					if ( isset( $staffData['services'] ) && is_array( $staffData['services'] ) && count( $staffData['services'] ) > 0 ) {
						if ( isset( $staffData['services'][ $service_id ]['available'] ) && 'yes' == $staffData['services'][ $service_id ]['available'] ) {
							$service_price = (float) $staffData['services'][ $service_id ]['price'];
							$service_price = ('' == $service_price) ? GFB_Service::get_service_by_id($service_id)['price'] : $service_price;
							$product_info['products']['appointment_cost'] = array('name' => $service_title, 'price' => $service_price, 'quantity' => $slot);
						}
					}
				}
			}

			return $product_info;
		}

		public function gfb_paypal_payment_status( $feed, $entry, $status,  $transaction_id, $subscriber_id, $amount, $pending_reason, $reason ) {	
 		    
		 	$appointment_id = gform_get_meta( $entry['id'], 'gfb_appointment', true );		 	

		    if ( $status == 'Completed' || $status == 'Paid' ) {
		        update_post_meta( $appointment_id, 'status', 'confirmed' );
		        GFB_Email::send_notification( $appointment_id, 'template_1', 'customer' ); # template_1
		    } elseif( $status == 'Refunded' || $status == 'Reversed' || $status == 'Canceled_Reversal' ) {
		        update_post_meta( $appointment_id, 'status', 'cancelled' );
		        update_post_meta( $appointment_id, 'cancel_reason', $reason );
		        GFB_Email::send_notification( $appointment_id, 'template_3', 'customer' ); # template_3
		        GFB_Email::send_notification( $appointment_id, 'template_6', 'admin' ); # template_6
		        GFB_Email::send_notification( $appointment_id, 'template_7', 'staff' ); # template_6
		    }
		}

		public function add_default_value_to_fields( $form, $ajax, $field_values ) {			

			if ( is_user_logged_in() ) {

				$user_id = get_current_user_id();
				$user_details = get_user_by('ID',$user_id);
				$first_name = get_user_meta($user_id, 'first_name', true);
                $last_name = get_user_meta($user_id, 'last_name', true);
			
				$fields = $form['fields'];				

				$results = array_filter($fields, function( $item ) {
				   return ($item->type === 'gfb_appointment_calendar' );
				});

				if ( $results ) {

					foreach ($fields as &$field) {					

						if ( $field->type == 'email' ) {						
		                	$field->defaultValue = $user_details->user_email;
		                }

		                if ( $field->type == 'name' ) {		                	
		                	$field->inputs[1]['defaultValue'] = $first_name;
		                	$field->inputs[3]['defaultValue'] = $last_name;

		                }
					}
				}
				
			}

			return $form;
		}

		public function create_appointment( $entry, $form ) {

			$entry_id = $entry['id'];
			$form_id = $form['id'];
			$id = gfb_field_exist( $form );			
			$form_settings = get_option('gfb_form_settings_' . $form_id, true);
			$data = array();
			$payment_status = $entry['payment_status'];

			$_POSTED = $_POST;

			if ( ! empty($id) ) {

				if ( isset( $_POSTED['input_'.$id] ) ) {
					gform_delete_meta($entry_id, $id );
					gform_update_meta( $entry_id, $id, $_POSTED['input_'.$id] );
					$appointmentData = $_POSTED['input_'.$id];
					$location_name = isset( $appointmentData['location'] ) ? get_the_title($appointmentData['location']) : '';
					$service_name = isset( $appointmentData['service'] ) ? get_the_title($appointmentData['service']) : '';
					$staff_name = isset( $appointmentData['staff'] ) ? get_the_title($appointmentData['staff']) : '';
					gform_update_meta( $entry_id, 'location_name', $location_name );
					gform_update_meta( $entry_id, 'service_name', $service_name );
					gform_update_meta( $entry_id, 'staff_name', $staff_name );
				}

				$fname_id = isset( $form_settings['registration_field']['fname'] ) ? str_replace( '.', '_', $form_settings['registration_field']['fname'] ) : '';
				$lname_id = isset( $form_settings['registration_field']['lname'] ) ? str_replace( '.', '_', $form_settings['registration_field']['lname'] ) : '';
				$email_id = isset( $form_settings['registration_field']['email'] ) ? $form_settings['registration_field']['email'] : '';
				$phone_id = isset( $form_settings['registration_field']['phone'] ) ? $form_settings['registration_field']['phone'] : '';

				if ( isset( $_POST['input_' . $fname_id] ) ) {
					$data['fname'] = $_POST['input_' . $fname_id];
				} else {
					$data['fname'] = '';
				}

				if ( isset( $_POST['input_' . $lname_id] ) ) {
					$data['lname'] = $_POST['input_' . $lname_id];
				} else {
					$data['lname'] = '';
				}

				if ( isset( $_POST['input_' . $email_id] ) ) {
					$data['email'] = $_POST['input_' . $email_id];
				} else {
					$data['email'] = '';
				}

				if ( isset( $_POST['input_' . $phone_id] ) ) {
					$data['phone'] = $_POST['input_' . $phone_id];
				} else {
					$data['phone'] = '';
				}

				// echo '<pre>$data$data';
				// print_r( $data );
				// echo '</pre>';

				$wp_user = GFB_Customer::gfb_create_customer( $data );

				if ( is_array( $appointmentData ) && isset( $appointmentData['service'] ) ) {
					$service_data = GFB_Service::get_service_by_id( $appointmentData['service'] );
				}

				if ( is_array( $appointmentData ) && isset( $appointmentData['staff'] ) ) {
					$staff_data = GFB_Staff::get_staff_by_id( $appointmentData['staff'] );					
				}				

				$total_id = $_POSTED['input_' . $id]['total_id'];
				$cost = floatval( preg_replace('/[^\d\.]/', '', $_POSTED['input_' . $total_id] ) );				
				
				$args = apply_filters( 'gfb_add_appointment_args', array(						
					'post_title' => isset( $appointmentData['date'] ) && isset( $appointmentData['time'] ) ? $appointmentData['date'] . ' ' . $appointmentData['time'] : '',
					'post_type' => 'gfb-appointment',
					'post_status' => 'draft',
					'wp_error'    => true,
					'meta_input'  => array(
						'form_id' => $form_id,
						'entry_id' => $entry_id,
						'wp_user' => $wp_user,
						'gfb_user' => get_user_meta( $wp_user, 'gfb_user', true ),
						'location_id' => isset( $appointmentData['location'] ) ? $appointmentData['location'] : '',
						'location_name' => $location_name,
						'service_id' => isset( $appointmentData['service'] ) ? $appointmentData['service'] : '',
						'service_name' => $service_name,
						'staff_id' => isset( $appointmentData['staff'] ) ? $appointmentData['staff'] : '',
						'staff_name' => $staff_name,
						'date' => isset( $appointmentData['date'] ) ? $appointmentData['date'] : '',
						'time' => isset( $appointmentData['time'] ) ? $appointmentData['time'] : '',
						'slot' => isset( $appointmentData['slot'] ) ? $appointmentData['slot'] : 1,
						// 'cost' => isset( $appointmentData['cost'] ) ? $appointmentData['cost'] : 0,
						'cost' => $cost,
						'status' => ! empty( $payment_status ) && ('Paid' == $payment_status || 'COMPLETED' == $payment_status) ? 'confirmed' : 'pending'
					)
				));

				$appointment_id = wp_insert_post( $args );

				if ( $appointment_id ) {

					// echo '<pre>_POSTED_POSTED';
					// print_r( $_POSTED );
					// echo '</pre>';

					//if is fullday booking
					if ( isset($appointmentData['gfb_booking_type']) && ! empty($appointmentData['gfb_booking_type']) ) {
						update_post_meta($appointment_id, 'booking_type', $appointmentData['gfb_booking_type']);
					}

					gform_update_meta( $entry_id, 'gfb_appointment', $appointment_id );					
					/* EMAIL NOTIFICATION START */					
					GFB_Email::send_notification( $appointment_id, 'template_8', 'staff' ); # template_8
					GFB_Email::send_notification( $appointment_id, 'template_5', 'admin' ); # template_5
					if ( ! empty( $payment_status ) && ('Paid' == $payment_status || 'COMPLETED' == $payment_status) ) {
						GFB_Email::send_notification( $appointment_id, 'template_1', 'customer' ); # template_1
					}
					/* EMAIL NOTIFICATION END */

					/* ADD to Google Calendar START */
					$gfb_settings = GFB_Setting::get_settings();
					
					if ( isset($gfb_settings['gfb_gcal_switch']) && 'enabled' === $gfb_settings['gfb_gcal_switch'] ) {
						$userdata = get_user_by('id', $wp_user);
						$fullname = $userdata->first_name . ' ' . $userdata->last_name;
						$gcal_data = get_post_meta( $appointmentData['staff'], 'gcal', true );
						$summary = ('all' != $appointmentData['service']) ? 'App# ' . $appointment_id . ' ' . get_the_title($appointmentData['service']) : '';
						$description = sprintf('%1$s: %2$s <br/> %3$s: %4$s', __('Customer Name', 'gfb'), $fullname, __('Slot Qty', 'gfb'), $appointmentData['slot'] );
						$appointmentTime = explode('-', $appointmentData['time']);
						$startTime = isset($appointmentTime[0]) ? trim($appointmentTime[0]) : '';
						$endTime = isset($appointmentTime[1]) ? trim($appointmentTime[1]) : '';
						$event_time = array();
						$event_time['start_time'] = $appointmentData['date'] . ' ' . $startTime . ':00';
						$event_time['end_time'] = $appointmentData['date'] . ' ' . $endTime . ':00';
						// Set your timezone
						if ( '' != $staff_data['timezone'] ) {
							$staff_timezone = gfb_timezone_string($appointmentData['staff']); //Staff timezone.
						} else {
							$staff_timezone = gfb_timezone_string(); //WordPress timezone.
						}						
						
						$startTime = $event_time['start_time'];
						$endTime = $event_time['end_time'];

						date_default_timezone_set($staff_timezone);

						$startTime = strtotime($startTime);
						$endTime = strtotime($endTime);

						date_default_timezone_set('UTC');

						$startTime = date('Y-m-d H:i:s__', $startTime);
						$endTime = date('Y-m-d H:i:s__', $endTime);
						
						$startTime = str_replace(' ', 'T', $startTime);
						$startTime = str_replace('__', '.000Z', $startTime);

						$endTime = str_replace(' ', 'T', $endTime);
						$endTime = str_replace('__', '.000Z', $endTime);						

						$event_time['start_time'] = $startTime;
						$event_time['end_time'] = $endTime;						

						if ( isset( $gcal_data['access_token'] ) && isset( $gcal_data['expires_at'] ) ) {							
							require_once GFB_PATH . '/includes/classes/class-gfb-gcal-api.php';
							$capi = new GFB_CAL_API();
							if ( (time() + 300) >= $gcal_data['expires_at'] ) { //access token is expired, create new one.								
								try {									
									// Get the access token 
									$data = $capi->RefreshAccessToken($gfb_settings['gfb_gcal_client_id'], $gfb_settings['gfb_gcal_secret_key'], $gcal_data['refresh_token']);									
									// Save the access token as a session variable
									if ( isset($data['access_token']) ) {
										//update refresh access token to staff.
										update_post_meta( $appointmentData['staff'], 'gcal', $data );

										// Get user calendar timezone
										$user_timezone = $capi->GetUserCalendarTimezone($data['access_token']);

										// Create event on primary calendar
										$event_id = $capi->CreateCalendarEvent('primary', $summary, $description, $staff_timezone, $event_time, $user_timezone, $data['access_token']);
										update_post_meta($appointment_id, 'gcal_event_id', $event_id);
									}
									
								} catch(Exception $e) {
									update_post_meta( $appointment_id, 'refresh_token_exception', $e->getMessage());
									exit();
								}

							} else {

								// Get user calendar timezone
								$user_timezone = $capi->GetUserCalendarTimezone($gcal_data['access_token']);

								// Create event on primary calendar
								$event_id = $capi->CreateCalendarEvent('primary', $summary, $description, $staff_timezone, $event_time, $user_timezone, $gcal_data['access_token']);
								update_post_meta($appointment_id, 'gcal_event_id', $event_id);
							}
						}
					}
					/* ADD to Google Calendar END */

				}
			}
		}

		public function register_appointment_posttype() {
			$labels = array(
				'name'                  => _x( 'Appointment', 'Post type general name', 'gfb' ),
				'singular_name'         => _x( 'Appointment', 'Post type singular name', 'gfb' ),
				'menu_name'             => _x( 'Appointment', 'Admin Menu text', 'gfb' ),
				'name_admin_bar'        => _x( 'Appointment', 'Add New on Toolbar', 'gfb' ),
				'add_new'               => __( 'Add Appointment', 'gfb' ),
				'add_new_item'          => __( 'Add New Appointment', 'gfb' ),
				'new_item'              => __( 'New Appointment', 'gfb' ),
				'edit_item'             => __( 'Edit Appointment', 'gfb' ),
				'view_item'             => __( 'View Appointment', 'gfb' ),
				'all_items'             => __( 'Appointments', 'gfb' ),
				'search_items'          => __( 'Search Appointment', 'gfb' ),
				'parent_item_colon'     => __( 'Parent Appointment:', 'gfb' ),
				'not_found'             => __( 'No Appointment found.', 'gfb' ),
				'not_found_in_trash'    => __( 'No Appointment found in Trash.', 'gfb' ),
				'featured_image'        => _x( 'Appointment Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'gfb' ),
				'set_featured_image'    => _x( 'Set Appointment image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'remove_featured_image' => _x( 'Remove Appointment image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'use_featured_image'    => _x( 'Use as Appointment image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'archives'              => _x( 'Appointment archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'gfb' ),
				'insert_into_item'      => _x( 'Insert into Appointment', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'gfb' ),
				'uploaded_to_this_item' => _x( 'Uploaded to this Appointment', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'gfb' ),
				'filter_items_list'     => _x( 'Filter Appointment list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'gfb' ),
				'items_list_navigation' => _x( 'Appointment list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'gfb' ),
				'items_list'            => _x( 'Appointment list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'gfb' ),
			);

			$show_in_menu = false;

			if ( is_user_logged_in() ) {				
				if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
					$show_in_menu = true;
					$labels['all_items'] = __( 'Appointment', 'gfb' );
				} else {
					$show_in_menu = 'gfb';
				}
			}

			$args = array(
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => $show_in_menu,
				'menu_icon'          => 'dashicons-calendar-alt',
				'show_in_rest'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'gfb-appointment' ),
				'capability_type'    => 'gfb-appointment',
				// 'capabilities' => array(
				// 	'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				// ),
				'has_archive'        => false,
				'hierarchical'       => false,
				'supports'           => array( 'title' ),
				'menu_position'      => 0			
			);

			register_post_type( 'gfb-appointment', $args );			
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Appointment();
			}

			return self::$instance;
		}
	}

	GFB_Appointment::get_instance();
}

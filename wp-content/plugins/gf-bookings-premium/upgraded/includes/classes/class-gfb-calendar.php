<?php
/**
 * GFB Calendar Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Calendar' ) ) {
	
	class GFB_Calendar {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {			

			add_action('gfb_after_menu_register', array($this, 'register_menu'));

			add_action('admin_enqueue_scripts', array($this, 'register_calendar_admin_assets'));

			add_action('wp_ajax_gfb_month_changer', array($this, 'gfb_month_changer'));
			add_action('wp_ajax_nopriv_gfb_month_changer', array($this, 'gfb_month_changer'));

			add_action('wp_ajax_gfb_show_slots_by_date', array($this, 'gfb_show_slots_by_date'));
			add_action('wp_ajax_nopriv_gfb_show_slots_by_date', array($this, 'gfb_show_slots_by_date'));			
		}

		public function gfb_show_slots_by_date() {
			check_ajax_referer( 'gfb', 'nonce' );
			$_POSTED = $_POST;
			
			// echo '<pre>$_POSTED$_POSTED';
			// print_r( $_POSTED );
			// echo '</pre>';

			if ( isset($_POSTED['staff_id']) && ! empty($_POSTED['staff_id']) && 'all' != $_POSTED['staff_id'] ) {
				if ( isset($_POSTED['date']) && ! empty($_POSTED['date']) && 'all' != $_POSTED['date'] ) {

					// get day short name
					$daySName = strtolower( date('D', strtotime($_POSTED['date'])) );

					$formated_date = date('d, Y', strtotime($_POSTED['date']));
					$month_name = date('F', strtotime($_POSTED['date']));

					$staffData = GFB_Staff::get_staff_by_id( $_POSTED['staff_id'] );

					if ( isset( $staffData['services'] ) && is_array( $staffData['services'] ) && count( $staffData['services'] ) > 0 ) {
						if ( isset( $staffData['services'][ $_POSTED['service_id'] ]['available'] ) && 'yes' == $staffData['services'][ $_POSTED['service_id'] ]['available'] ) {
							$service_price = $staffData['services'][ $_POSTED['service_id'] ]['price'];
							// wp_die($service_price);						
							$service_price = ('' == $service_price) ? GFB_Service::get_service_by_id($_POSTED['service_id'])['price'] : $service_price;
						} else {
							echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('Currently staff is not providing this service', 'gfb') );
							wp_die();	
						}
					} else {
						echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('Currently staff is not providing this service', 'gfb') );
						wp_die();
					}

					if ( isset( $staffData['holidays'] ) && is_array( $staffData['holidays'] ) && count( $staffData['holidays'] ) > 0 ) {				
						$staff_holidays = $staffData['holidays'];
					} else {
						$staff_holidays = array();
					}

					if ( isset( $staffData['holidays'] ) && is_array( $staffData['holidays'] ) && count( $staffData['holidays'] ) > 0 && in_array( $_POSTED['date'], $staff_holidays ) ) {						
						echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('This date is marked as holiday', 'gfb') );
						wp_die();
					}

					// Set your timezone
					if ( '' != $staffData['timezone'] ) {
						date_default_timezone_set( gfb_timezone_string($_POSTED['staff_id']) ); //Staff timezone.
					} else {
						date_default_timezone_set( gfb_timezone_string() ); //WordPress timezone.
					}

					$current_time = date('Y-m-d H:i');

					if ( isset( $staffData['timeslots'] ) && is_array( $staffData['timeslots'] ) && count( $staffData['timeslots'] ) > 0 ) {
						#continue.
						$staff_timeslots = $staffData['timeslots'];
						$staff_timings = $staffData['timings'];
					} else {						
						echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('No slots is available for this staff', 'gfb') );
						wp_die();
					}

					//For fullday booking!!
					if ( isset( $staffData['booking_type'] ) && 'fullday' == $staffData['booking_type'] ) {
						//Full Day Code will be placed here!
						$response['booking_type'] = 'fullday';						
						$response['date'] = $_POSTED['date'];
						$response['time'] = '00:00 - 23:59';
						$response['service_price'] = $service_price;
						$html = '';
						$month_name = gfb_translable_name('months', strtolower($month_name));
						$placeholder = apply_filters('gfb_staff_time_slot_capacity_placeholder', __('Enter Slot Capacity', 'gfb'));
						$total_slots_consumed = (int) GFB_Appointment::gfb_all_appointment_by_date_time( $_POST['staff_id'], $_POSTED['date'], '00:00 - 23:59', 'fullday' );
						$slot_capacity = (int) $staff_timings[$daySName]['capacity'];
						$remaining_capacity = $slot_capacity - $total_slots_consumed;
						$tooltip = (1 >= $remaining_capacity) ? sprintf(__( '%1$s slot left', 'gfb' ), $remaining_capacity ) : sprintf(__( '%1$s slots left', 'gfb' ), $remaining_capacity );
						$filter_slot = apply_filters('gfb_staff_time_slot', __('Fullday', 'gfb'), $staffData);
						ob_start();
						?>						
						<h4 class="slot-title"><?php echo sprintf(__('%1$s %2$s', 'gfb'), esc_attr($month_name), esc_attr($formated_date)); ?></h4>
						<ul class="gfb-slot-list">
							<li class="gfb-tooltip">
								<span><?php echo esc_attr($filter_slot); ?></span>
								<input class="gfb-selected-capacity" type="number" placeholder="<?php echo esc_html($placeholder); ?>" min="1" max="<?php echo esc_html($remaining_capacity); ?>" />
								<input class="gfb-selected-time" type="hidden" value="00:00 - 23:59" />
								<input class="gfb-selected-date" type="hidden" value="<?php echo esc_attr(sanitize_text_field($_POSTED['date'])); ?>" />
								<input class="gfb-selected-price" type="hidden" value="<?php echo esc_attr($service_price); ?>" />
								<span class="gfb-tooltip-text"><?php echo esc_html($tooltip); ?></span>
							</li>
						</ul>
						<?php
						$html = ob_get_clean();	
						$response['html'] = $html;
						echo json_encode( $response );
						wp_die(); //terminate immediately.
					}

					// echo '<pre>$staff_timeslots$staff_timeslots';
					// print_r( $staff_timeslots );
					// echo '</pre>';

					if ( isset($staff_timeslots[$daySName]['slot']) && is_array($staff_timeslots[$daySName]['slot']) && count($staff_timeslots[$daySName]['slot']) > 0 ) {						
						$html = '';
						foreach( $staff_timeslots[$daySName]['slot'] as $key => $slot ) {
							$staff_breaks = isset($staff_timings[$daySName]['break']) ? $staff_timings[$daySName]['break'] : '';
							$time_break = false;
							if ( is_array( $staff_breaks ) && in_array( $staff_timeslots[$daySName]['start'][$key], $staff_breaks ) ) {
								$time_break = true;
							}

							$total_slots_consumed = (int) GFB_Appointment::gfb_all_appointment_by_date_time( $_POSTED['staff_id'], $_POSTED['date'], $slot );
							$slot_capacity = (int) $staff_timeslots[$daySName]['capacity'][$key];
							$remaining_capacity = $slot_capacity - $total_slots_consumed;
							//echo 'Total capacity consumed are ' . $total_slots_consumed . '<br>';
							//echo 'slot capacity are ' . $slot_capacity . '<br>';
							$check_time = date($_POSTED['date'] . ' ' . $staff_timeslots[$daySName]['start'][$key]);
							//echo 'checkTime ' . strtotime($check_time) . '<br>';
							//echo 'currentTime ' . strtotime($current_time) . '<br>';
							if ( !$time_break && strtotime($check_time) > strtotime($current_time) ) {
								$class = ( 0 >= $remaining_capacity ) ? 'gfb-unavailable' : '';
								$filter_slot = apply_filters('gfb_staff_time_slot', $slot, $staffData);
								$placeholder = apply_filters('gfb_staff_time_slot_capacity_placeholder', __('Enter Slot Capacity', 'gfb'));
								$tooltip = (1 >= $remaining_capacity) ? sprintf(__( '%1$s slot left', 'gfb' ), $remaining_capacity ) : sprintf(__( '%1$s slots left', 'gfb' ), $remaining_capacity );
								$html .= '
									<li class="' . $class . ' gfb-tooltip">
										<span>' . $filter_slot . '</span>
										<input class="gfb-selected-capacity" type="number" placeholder="' . $placeholder . '" min="1" max="' . $remaining_capacity . '" />
										<input class="gfb-selected-time" type="hidden" value="' . $slot . '" />
										<input class="gfb-selected-date" type="hidden" value="' . $_POSTED['date'] . '" />
										<input class="gfb-selected-price" type="hidden" value="' . $service_price . '" />
										<span class="gfb-tooltip-text">' . $tooltip . '</span>
									</li>
								';
							}
						}

						if ( '' == $html ) {
							echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('No slot left for the day', 'gfb') );							
							wp_die();
						}

						//ob_start();
						$month_name = gfb_translable_name('months', strtolower($month_name));
						?>
						<h4 class="slot-title"><?php echo sprintf(__('%1$s %2$s', 'gfb'), esc_attr($month_name), esc_attr($formated_date)); ?></h4>
						<ul class="gfb-slot-list">
							<?php echo $html; ?>
						</ul>
						<?php
						//ob_get_clean();

					} else {						
						echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('No slots is available for the staff', 'gfb') );
						wp_die();
					}

				} else {					
					echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('No slots is available for the staff', 'gfb') );
					wp_die();	
				}
			} else {
				echo sprintf('<p class="gfb-slot-error">%1$s</p>', __('No slots is available for the staff', 'gfb') );				
				wp_die();
			}

			wp_die();
		}

		public function gfb_month_changer() {
			check_ajax_referer( 'gfb', 'nonce' );

			$_POSTED = $_POST;

			// echo '<pre>$_POSTED$_POSTED';
			// print_r( $_POSTED );
			// echo '</pre>';
			// die('wait!');

			if ( ! isset( $_POSTED['location_id'] ) ) {
				$_POSTED['location_id'] = 'all';
			}

			if ( ! isset( $_POSTED['service_id'] ) ) {
				$_POSTED['service_id'] = 'all';
			}

			if ( ! isset( $_POSTED['staff_id'] ) ) {
				$_POSTED['staff_id'] = 'all';
			}

			if ( isset( $_POSTED['is_admin'] ) && '1' == $_POSTED['is_admin'] ) {

				// wp_die('I am in admin');

				$appointmentData = GFB_Appointment::gfb_show_appointments_on_calendar( $_POSTED );

				// echo '<pre>$appointmentData$appointmentData';
				// print_r($appointmentData);
				// echo '</pre>';

				self::render( $appointmentData );

			} else {

				if ( isset( $_POSTED['form_id'] ) && isset( $_POSTED['location_id'] ) && isset( $_POSTED['service_id'] ) && isset( $_POSTED['staff_id'] ) ) {					

					$args = array(
						'location_id' => isset($_POSTED['location_id']) ? $_POSTED['location_id'] : 'all',
						'service_id' => $_POSTED['service_id'],
						'staff_id' => $_POSTED['staff_id'],
						'prior_months' => $_POSTED['prior_months'],
					);					

					self::render_frontend( $_POSTED['form_id'], $args );
				}
			}

			wp_die();
		}

		public function register_calendar_admin_assets() {

			if ( GFB_LIVE_MODE ) {
				$minimized = '.min';
				$clear_cache = '';
			} else {
				$minimized = '';
				$clear_cache = '?t=' . gmdate('ymdhis');
			}

			$gfb_settings = GFB_Setting::get_settings();

			wp_register_style( 'gfb-calendar-style', GFB_ADMIN_CSS_URL . 'gfb-calendar-style' . $minimized . '.css', array(), GFB_VERSION . $clear_cache, 'all' );		

			wp_register_script('gfb-calendar-script', GFB_ADMIN_JS_URL . 'gfb-calendar-script' . $minimized . '.js', array('jquery'), GFB_VERSION . $clear_cache, true);
			wp_localize_script('gfb-calendar-script', 'gfb_calendar', array(
				'ajaxurl'  => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('gfb'),
				'prior_months' => '',
				'show_timeslot' => $gfb_settings['gfb_show_timeslot']
			));

			if ( is_admin() ) {

				//load assets
				wp_enqueue_style('gfb-calendar-style');
				wp_enqueue_script('gfb-calendar-script');
			}
		}

		public static function render_frontend( $form_id = '', $args = array() ) {

			if ( empty( $form_id ) ) {
				echo esc_html__('Calendar can\'t be load due to form id is missing.', 'gfb');
				return false;
			}			

			$form = GFAPI::get_form( $form_id );
			$fields = $form['fields'];
			foreach ( $fields as $field) {
		    	if ( 'gfb_appointment_calendar' === $field->type ) {		    		

		    		if ( isset($field->gfb_location_hide) && $field->gfb_location_hide ) {
		    			$location_hide = $field->gfb_location_hide;
		    		} else {
		    			$location_hide = false;
		    		}

		    		if ( isset($field->gfb_location) ) {
		    			$location_id = $field->gfb_location;
		    		}

		    		if ( isset($field->gfb_service_hide) && $field->gfb_service_hide ) {
		    			$service_hide = $field->gfb_service_hide;
		    		} else {
		    			$service_hide = false;
		    		}

		    		if ( isset($field->gfb_service) ) {
		    			$service_id = $field->gfb_service;
		    		}

		    		if ( isset($field->gfb_staff_hide) && $field->gfb_staff_hide ) {
		    			$staff_hide = $field->gfb_staff_hide;
		    		} else {
		    			$staff_hide = false;
		    		}

		    		if ( isset($field->gfb_staff) ) {
		    			$staff_id = $field->gfb_staff;
		    		}		    		

		    		break;
		    	}
		    }

		    if ( isset( $args['prior_months'] ) && ! empty( isset( $args['prior_months'] ) ) ) {
		    	$prior_months = $args['prior_months'];
			} else {
				$prior_months = 3;
			}

		    if ( isset( $args['location_id'] ) && ! empty( isset( $args['location_id'] ) ) ) {
		    	$location_id = $args['location_id'];
			}

			if ( isset( $args['service_id'] ) && ! empty( isset( $args['service_id'] ) ) ) {
		    	$service_id = $args['service_id'];
			}

			if ( isset( $args['staff_id'] ) && ! empty( isset( $args['staff_id'] ) ) ) {
		    	$staff_id = $args['staff_id'];
			}

		    if ( ! $location_hide && ('all' == $location_id || empty($location_id) ) ) {
		    	echo esc_html__('Please select location.', 'gfb');
				return false;
		    }

		    if ( empty( $service_id ) || 'all' == $service_id ) {
		    	echo esc_html__('Please select service.', 'gfb');
				return false;
		    }

		    if ( empty( $staff_id ) || 'all' == $staff_id ) {		    
		    	echo esc_html__('Please select staff.', 'gfb');
				return false;
		    }

		    $staffData = GFB_Staff::get_staff_by_id( $staff_id ); 

			// echo 'timezone is ' . gfb_timezone_string( $staff_id );			

			if ( ! $location_hide && ('all' != $location_id && !empty($location_id) ) ) {				
				$flag = false;

				if ( isset($staffData['location']) && is_array($staffData['location']) && count($staffData['location']) > 0 ) {
					foreach ( $staffData['location'] as $staff_location_id ) {
						if ( $location_id == $staff_location_id ) {
							$flag = true;
							break;
						}
					}
				} else {
					$flag = true;
				}		

				if ( !$flag ) {
					echo esc_html__('Staff currently not available to this location.', 'gfb');
					return $flag;
				}
			}

			if ( $location_hide && ('all' != $location_id && !empty($location_id) ) ) {				
				$flag = false;

				if ( isset($staffData['location']) && is_array($staffData['location']) && count($staffData['location']) > 0 ) {
					foreach ( $staffData['location'] as $staff_location_id ) {
						if ( $location_id == $staff_location_id ) {
							$flag = true;
							break;
						}
					}
				} else {
					$flag = true;
				}

				if ( !$flag ) {
					echo esc_html__('Staff currently not available to this location.', 'gfb');
					return $flag;
				}
			}

			if ( 'all' != $service_id && !empty($service_id ) ) {
				$flag = false;

				if ( isset($staffData['services']) && is_array($staffData['services']) && count($staffData['services']) > 0 ) {
					foreach ( $staffData['services'] as $staff_service_id => $staff_service_data ) {
						if ( $service_id == $staff_service_id ) {
							$flag = true;
							break;
						}
					}
				}		

				if ( !$flag ) {
					echo esc_html__('The staff is not providing this service.', 'gfb');
					return $flag;
				}
			}

			// echo '<pre>$staffData$staffData';
			// print_r( $staffData );
			// echo '</pre>';

			// Set your timezone
			if ( '' != $staffData['timezone'] ) {
				date_default_timezone_set( gfb_timezone_string($staff_id) ); //Staff timezone.
			} else {
				date_default_timezone_set( gfb_timezone_string() ); //WordPress timezone.
			}

			if ( isset( $staffData['timings'] ) && is_array( $staffData['timings'] ) && count( $staffData['timings'] ) > 0 ) {
				#continue.
				$staff_timings = $staffData['timings'];
			} else {
				echo esc_html__('No timeslot is available for this staff.', 'gfb');
				return false;
			}

			if ( isset( $staffData['holidays'] ) && is_array( $staffData['holidays'] ) && count( $staffData['holidays'] ) > 0 ) {				
				$staff_holidays = $staffData['holidays'];
			} else {
				$staff_holidays = false;
			}
			
			// Get prev & next month
			if ( isset($_POST['date']) && ! empty( $_POST['date'] ) ) {
			    $ym = $_POST['date'];
			} else {
				$ym = date('Y-m');
			}

			// Check format
			$timestamp = strtotime($ym . '-01');		
			if ($timestamp === false) {				
			    $ym = date('Y-m');
			    $timestamp = strtotime($ym . '-01');
			}			

			// Today
			$today = date('Y-m-d', time());			

			// For H3 title
			$current_month = date('F', $timestamp);
			$current_year = date('Y', $timestamp);
			$html_title = gfb_translable_name('months', strtolower($current_month)) . ' ' . $current_year;

			// Create prev & next month link     mktime(hour,minute,second,month,day,year)
			$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp) - 1, 1, date('Y', $timestamp)));
			$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp) + 1, 1, date('Y', $timestamp)));
			//echo 'prior months is ' . $prior_months . '<br>';
			$prior_next = date('Y-m', strtotime( date('Y-m-01') ." +$prior_months months" ));
			//echo 'prior months is ' . $prior_next;
			$curr = date('Y-m'); // get the current year and month
			$disabled_prev = true;
			$disabled_next = true;

			$check_prev = explode('-', $prev); // take year and month as an array to compare
			$check_curr = explode('-', $curr); // take year and month as an array to compare

			// Number of days in the month
			$day_count = date('t', $timestamp);

			//echo 'total days ' . $day_count;
			 
			// 0:Sun 1:Mon 2:Tue ...
			if ( isset( get_option('gfb_settings', array())['general']['gbf_start_of_week'] ) && 'sunday' == get_option('gfb_settings', array())['general']['gbf_start_of_week'] ) {				
				$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));
			} else {				
				$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp))) - 1;
				if ( $str == -1 ) {
					$str = 6;
				}
			}

			// Create Calendar!!
			$weeks = array();
			$week = '';

			// Add empty cell
			$week .= str_repeat('<td></td>', $str);			

			for ( $day = 1; $day <= $day_count; $day++, $str++) {				
			     
			    $date = $ym . '-' . $day;

			    $date = date( 'Y-m-d', strtotime($date) );

			    // echo $date . '<br>';    

			    // get day short name
				$daySName = strtolower( date('D', strtotime($date)) );				

				$class = 'gfb-not-available';
				$hdnInput = '';

				if ( isset( $staff_timings[$daySName] ) ) {

					// echo '<pre>$staff_holidays$staff_holidays';
					// print_r($staff_holidays);
					// echo '</pre>';					

					// echo 'date ' . $date . '<br>';
					// echo 'today ' . $today . '<br>';

					if ( false != $staff_holidays && in_array( $date, $staff_holidays ) && $date >= $today ) {
						$class = 'gfb-holiday';
					} else {
						if ( $date >= $today) {
							// echo 'booking type is ' . $staffData['booking_type'] . '<br>';
							/* Fullday */
							if ( isset( $staffData['booking_type'] ) && 'fullday' == $staffData['booking_type'] ) { 
								//check if is fullday booking type
								$total_slots_consumed = (int) GFB_Appointment::gfb_all_appointment_by_date_time( $staff_id, $date, '00:00 - 23:59', 'fullday' );
								$slot_capacity = (int) $staff_timings[$daySName]['capacity'];
								$remaining_capacity = $slot_capacity - $total_slots_consumed;
								$tooltip = (1 >= $remaining_capacity) ? sprintf(__( '%1$s slot left', 'gfb' ), $remaining_capacity ) : sprintf(__( '%1$s slots left', 'gfb' ), $remaining_capacity );								
								if ( $date > $today && $remaining_capacity > 0 ) {
									$class = 'gfb-available gfb-tooltip';
									$hdnInput = '<input type="hidden" class="gfb-hdn" value="' . $date . '" />';
									$hdnInput .= '<span class="gfb-tooltip-text">' . $tooltip . '</span>';
								}
							} else {
								$class = 'gfb-available';
								$hdnInput = '<input type="hidden" class="gfb-hdn" value="' . $date . '" />';
							}							
						}
					}
				}
			     
			    if ($today == $date) {
			        $week .= '<td class="today ' . $class . '"><span>' . $day . '</span>' . $hdnInput;
			    } else {
			        $week .= '<td class="' . $class . '">' . $day . $hdnInput;
			    }
			    $week .= '</td>';
			     
			    // End of the week OR End of the month
			    if ($str % 7 == 6 || $day == $day_count) {

			        if ($day == $day_count) {
			            // Add empty cell
			            $week .= str_repeat('<td></td>', 6 - ($str % 7));
			        }

			        $weeks[] = '<tr>' . $week . '</tr>';

			        // Prepare for new week
			        $week = '';
			    }

			}

			require_once GFB_VIEW_PATH . 'calendar.php';
		}

		public static function render( $appointmentData = array() ) {

			// echo '<pre>$appointmentData$appointmentData';
			// print_r( $appointmentData );
			// echo '</pre>';

			// Set your timezone
			date_default_timezone_set(gfb_timezone_string()); //Pacific/Auckland
			
			// Get prev & next month
			if ( isset($_POST['date']) && ! empty( $_POST['date'] ) ) {
			    $ym = $_POST['date'];
			} else {
				$ym = date('Y-m');
			}

			// Check format
			$timestamp = strtotime($ym . '-01');		
			if ($timestamp === false) {				
			    $ym = date('Y-m');
			    $timestamp = strtotime($ym . '-01');
			}			

			// Today
			$today = date('Y-m-j', time());			

			// For H3 title
			$current_month = date('F', $timestamp);
			$current_year = date('Y', $timestamp);
			$html_title = gfb_translable_name('months', strtolower($current_month)) . ' ' . $current_year;

			// Create prev & next month link     mktime(hour,minute,second,month,day,year)
			$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
			$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));
			$curr = date('Y-m'); // get the current year and month
			$disabled_prev = false;
			$disabled_next = false;

			// Number of days in the month
			$day_count = date('t', $timestamp);

			//echo 'total days ' . $day_count;
			 
			// 0:Sun 1:Mon 2:Tue ...
			if ( isset( get_option('gfb_settings', array())['general']['gbf_start_of_week'] ) && 'sunday' == get_option('gfb_settings', array())['general']['gbf_start_of_week'] ) {				
				$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));
			} else {				
				$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp))) - 1;
				if ( $str == -1 ) {
					$str = 6;
				}
			}


			//$str = date('w', $timestamp);

			//echo '<br>str ' . $str;


			// Create Calendar!!
			$weeks = array();
			$week = '';

			// Add empty cell
			$week .= str_repeat('<td></td>', $str);

			for ( $day = 1; $day <= $day_count; $day++, $str++) {
			     
			    $date = $ym . '-' . $day;			    
			     
			    if ($today == $date) {
			        $week .= '<td class="today"><span>' . $day . '</span>';
			    } else {
			        $week .= '<td>' . $day;
			    }

			    if ( is_array($appointmentData) && count( $appointmentData ) > 0 ) {
			    	foreach( $appointmentData as $appointment_id ) {
			    		$app_date = get_post_meta($appointment_id, 'date', true);
			    		$check_app_date = date('Y-m-j', strtotime($app_date));
			    		//$app_date = ! empty( $app_date ) ? date('Y-m-d', strtotime($app_date)) : '';			    		
			    		if ( ! empty( $check_app_date ) && $check_app_date == $date ) {
			    			ob_start();
			    			require GFB_ADMIN_VIEW_PATH . 'appointment-block.php';
			    			$week .= ob_get_clean();			    			
			    		}
			    	}
			    }

			    $week .= '</td>';
			     
			    // End of the week OR End of the month
			    if ($str % 7 == 6 || $day == $day_count) {

			        if ($day == $day_count) {
			            // Add empty cell
			            $week .= str_repeat('<td></td>', 6 - ($str % 7));
			        }

			        $weeks[] = '<tr>' . $week . '</tr>';

			        // Prepare for new week
			        $week = '';
			    }

			}

			require_once GFB_ADMIN_VIEW_PATH . 'admin-calendar.php';
		}

		public function register_menu() {

			add_submenu_page(
            	'gfb',
                __( 'Calendar - Gravity Booking', 'gfb' ),
                __( 'Calendar', 'gfb' ),
                'manage_options',
                'gfb-calendar',
                array( get_class($this), 'gfb_calendar_page' ),
                0
            );
		}

		public static function gfb_calendar_page() {
			
			$gfb_settings = GFB_Setting::get_settings();

			require_once GFB_ADMIN_VIEW_PATH . 'admin-calendar-top-filters.php';

			echo '<div id="gfb-fancy-calendar">';
				self::render();
			echo '</div>';
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Calendar();
			}

			return self::$instance;
		}
	}

	GFB_Calendar::get_instance();
}

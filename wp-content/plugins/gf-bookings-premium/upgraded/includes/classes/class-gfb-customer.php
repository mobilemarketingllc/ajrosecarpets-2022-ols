<?php
/**
 * GFB Customer Class
 *
 * @package GFB.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Customer' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Customer {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $checkPostType = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {			
			// Create GFB Customer role.
			add_role(
				'gfb-customer', //  System name of the role.
				__( 'GFB Customer', 'gfb'  ), // Display name of the role.		 
				array(		    					
					'read'         => true,  // true allows this capability
					'edit_posts'   => false,
					'delete_posts' => false, // Use false to explicitly deny
					'upload_files' => true
				)
			);

			self::$checkPostType = isset( $_REQUEST['post_type'] ) && ! empty( sanitize_text_field($_REQUEST['post_type']) ) ? sanitize_text_field($_REQUEST['post_type']) : '';

			add_action( 'init', array($this, 'register_customer_posttype') );
			add_filter( 'manage_gfb-customer_posts_columns', array( $this, 'gfb_modify_column_names') );
			add_action( 'manage_gfb-customer_posts_custom_column', array( $this, 'gfb_add_custom_column'), 9, 2 );

			add_action( 'deleted_user', array( $this, 'delete_gfb_customer_if_user_deleted'), 99, 1 );

			add_action( 'wp_ajax_gfb_add_customer', array( $this, 'gfb_add_customer' ) );
			add_action( 'wp_ajax_gfb_edit_customer', array( $this, 'gfb_edit_customer' ) );
			add_action( 'wp_ajax_gfb_delete_customer', array( $this, 'gfb_delete_customer' ) );
			add_action( 'wp_ajax_gfb_show_customer_data', array( $this, 'gfb_show_customer_data' ) );
			add_action( 'wp_ajax_gfb_cancel_appointment_by_customer', array( $this, 'gfb_cancel_appointment_by_customer' ) );
			add_action( 'wp_enqueue_scripts', array($this, 'register_frontend_assets') );

			add_action( 'init', array( $this, 'gfb_register_customer_appointment_shortcode') );			

			if ( 'gfb-customer' === self::$checkPostType ) {				

				add_action( 'admin_footer', array( $this, 'highlight_menu' ) );

				add_filter('post_row_actions', '__return_empty_array');

				add_action('manage_posts_extra_tablenav', array($this, 'gfb_manage_posts_extra_tablenav'));

				add_filter('months_dropdown_results', '__return_empty_array');

				add_filter( 'bulk_actions-edit-gfb-customer', '__return_empty_array' ); //

				add_action( 'views_edit-gfb-customer',  function ( $views ) {

					unset($views['all']);
					unset($views['publish']);
    				unset($views['trash']);
    				unset($views['draft']);

					return $views;
				});

				// the function that filters posts
				add_action( 'pre_get_posts', array( $this, 'gfb_filter_query' ) );
			}
			
		}

		public function register_frontend_assets() {
			
			if ( GFB_LIVE_MODE ) {
				$minimized = '.min';
				$clear_cache = '';
			} else {
				$minimized = '';
				$clear_cache = '?t=' . gmdate('ymdhis');
			}

			wp_register_script( 'gfb-sweetalert-script', '//cdn.jsdelivr.net/npm/sweetalert2@11', array('jquery'), GFB_VERSION . $clear_cache, true );
			wp_register_script( 'gfb-frontend-script', GFB_JS_URL . 'gfb-frontend-script' . $minimized . '.js', array('jquery', 'gfb-sweetalert-script'), GFB_VERSION . $clear_cache, true );
			wp_localize_script( 'gfb-frontend-script', 'gfb_frontend', array(
				'ajaxurl'  => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('gfb'),
				'title' => __('Appointment Cancellation', 'gfb'),
				'confirm' => __('Confirm', 'gfb'),
				'cancel' => __('No, I don\'t want', 'gfb'),
			));	
		}

		public function gfb_filter_query( $admin_query ) {

			if ( is_admin() && $admin_query->is_main_query() ) {

				if ( is_user_logged_in() ) {

					if( absint($admin_query->query_vars['s']) ) {
			            // Set the post id value
			            $admin_query->set('p', $admin_query->query_vars['s']);

			            // Reset the search value
			            $admin_query->set('s', '');
			        }			
				}				
			}			

			return $admin_query;
		}

		// public function gfb_add_bulk_delete( $actions ) {

		// 	unset( $actions['edit'] );
		// 	unset( $actions['trash'] );
		// 	$actions['delete'] = __('Delete All', 'gfb');

		// 	return $actions;
		// }

		public function gfb_register_customer_appointment_shortcode() {
			add_shortcode( 'gfb_customer_appointments', array( $this, 'gfb_customer_appointments') );
			add_shortcode( 'gfb_customer_appointment', array( $this, 'gfb_customer_appointments') );
		}

		public function gfb_customer_appointments() {

			if ( is_admin() ) {
				return;
			}

			wp_enqueue_script('gfb-sweetalert-script');
			wp_enqueue_script('gfb-frontend-script');

			$redirect_url = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
			ob_start();
			if ( is_user_logged_in() ) {
				$user = wp_get_current_user();	
				$user_id = $user->ID;				
				if ( isset( $user->roles ) && in_array( 'gfb-customer', $user->roles ) ) {					
					require_once GFB_VIEW_PATH . 'customer-appointments.php';
				} else {
					// Translators: %1$s and %2$s are tags.
					echo sprintf( esc_html__( 'You are logged in with a different role. %1$sClick here%2$s to log in with a gfb-customer role.', 'gfb' ), '<a href="' . esc_url( wp_login_url($redirect_url) ) . '">', '</a>' );
				}
			} else {
				// Translators: %1$s and %2$s are tags.
				echo sprintf( esc_html__( '%1$sClick here%2$s to login.', 'gfb' ), '<a href="' . esc_url( wp_login_url($redirect_url) ) . '">', '</a>' );
			}

			return ob_get_clean();
		}

		public static function get_customer_by_id( $customer_id ) {

			if ( ! empty( $customer_id ) ) {
				$customer = get_post( $customer_id );

				if ( $customer && 'gfb-customer' == $customer->post_type ) {
					return $customer;
				}				
			}

			return false;
		}

		public function gfb_cancel_appointment_by_customer() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(			
				'status'  => 'error'
			);

			$data = $_POST;

			if ( isset($data['post_id']) && ! empty( $data['post_id'] ) ) {
    
			    update_post_meta( $data['post_id'], 'status', $data['gfb_status'] );
			    update_post_meta( $data['post_id'], 'cancel_reason', $data['gfb_reason'] );
			    			        
		        GFB_Email::send_notification( $data['post_id'], 'template_3', 'customer' ); # template_3
	        	GFB_Email::send_notification( $data['post_id'], 'template_6', 'admin' ); # template_6
	        	GFB_Email::send_notification( $data['post_id'], 'template_7', 'staff' ); # template_7

			    $response['message'] = sprintf(__( 'Your appointment # %1$s has been cancelled.', 'gfb' ), $data['post_id']);
				$response['status'] = 'success';
			    
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_show_customer_data() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Failed to cancel the appointment.', 'gfb' ),	
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-customer' == $post->post_type ) {

					$user_id = get_post_meta( $data['post_id'], 'wp_user', true );
					$userdata = get_userdata( $user_id );
					if ( $userdata ) {
						$phone = get_post_meta( $data['post_id'], 'phone', true);
						$gender = get_post_meta( $data['post_id'], 'gender', true);
						$dob = get_post_meta( $data['post_id'], 'dob', true);
						$info = get_post_meta( $data['post_id'], 'info', true);

						$response['fname'] = $userdata->first_name;
						$response['lname'] = $userdata->last_name;
						$response['email'] = $userdata->user_email;
						//$response['password'] = $userdata->login_pass;
						$response['phone'] = $phone;
						$response['gender'] = $gender;
						$response['dob'] = $dob;
						$response['info'] = $info;
						$response['status'] = 'success';
					}
				}
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function delete_gfb_customer_if_user_deleted( $user_id ) {			

			$get_customer = get_posts( array(
				'post_type' => 'gfb-customer',
				'numberposts' => -1,
				'meta_query' => array(
					array(
						'key'   => 'wp_user',
						'value' => $user_id
					)
				)
			));			
			
			if ( isset($get_customer[0]) && $get_customer[0] ) {
				wp_delete_post( $get_customer[0]->ID, true );
			}			
		}

		public function gfb_delete_customer() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Customer not deleted.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {
				$user_id = get_post_meta( $data['post_id'], 'wp_user', true );
				$userdata = get_userdata( $user_id );
				if ( $userdata ) {
					wp_delete_user( $userdata->ID );
					$response['message'] = esc_html__( 'Customer deleted successfully.', 'gfb' );
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_edit_customer() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;			

			if ( ! empty( $data['post_id'] ) && ! empty( trim( $data['fname'] ) ) && ! empty( trim( $data['lname'] ) ) && ! empty( trim( $data['email'] ) ) ) {

				$user_id = get_post_meta( $data['post_id'], 'wp_user', true );

				if ( ! $user_id ) {
					echo wp_json_encode( $response );
					wp_die();
				}

				$user_args = array(
					'ID'           => trim( $user_id ),					
					'user_login'   => trim( $data['email'] ),
					'user_email'   => trim( $data['email'] ),
					'display_name' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
					'first_name'   => trim( $data['fname'] ),
					'last_name'    => trim( $data['lname'] ),
					'role' 		   => 'gfb-customer',
					'meta_input'  => array(						
						'info' => trim( $data['info'] ),
						'gender' => trim( $data['gender'] ),
						'phone' => trim( $data['phone'] ),
						'dob' => trim( $data['dob'] ),
					)
				);

				if ( ! empty( $data['password'] ) ) {
					$user_args['user_pass'] = trim( $data['password'] );
				}

				$wp_user = wp_update_user($user_args);

				if ( ! is_wp_error( $wp_user ) ) {

					$gfb_user = get_user_meta( $wp_user, 'gfb_user', true );

					$args = apply_filters( 'gfb_edit_customer_args', array(
						'ID'         => $gfb_user,
						'post_title' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
						'post_type' => 'gfb-customer',
						'post_status' => 'publish',
						'wp_error'    => true,
						'meta_input'  => array(
							'wp_user' => $wp_user,
							'info' => trim( $data['info'] ),
							'gender' => trim( $data['gender'] ),
							'phone' => trim( $data['phone'] ),
							'dob' => trim( $data['dob'] )							
						)
					));					

					$gfb_user = wp_update_post( $args );

					if ( ! is_wp_error( $gfb_user ) ) {

						update_user_meta( $wp_user, 'gfb_user', $gfb_user );
						$response['message'] = esc_html__( 'Customer updated successfully.', 'gfb' );
						$response['status'] = 'success';

						/* EMAIL NOTIFICATION STARTS */
						if ( true === apply_filters('gfb_edit_customer_personal_details_notification', true) ) {
							$body = __('Your account details has been updated, below are the credentials to login', 'gfb');
							$data['password'] = ! empty($data['password']) ? $data['password'] : __('password not changed', 'gfb');
							$response['notification'] = GFB_Email::new_customer_notification( $wp_user, $body, $data['password'] );
						}
						/* EMAIL NOTIFICATION ENDS */

					} else {

						$response['message'] = $wp_user->get_error_message();
					}

				} else {

					$response['message'] = $wp_user->get_error_message();

				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public static function gfb_create_customer( $data = array() ) {

			if ( ! empty( trim( $data['fname'] ) ) && ! empty( trim( $data['email'] ) ) ) {
				
				$user = get_user_by('email', trim( $data['email'] ) );

				if ( $user ) {
					$role = ( array ) $user->roles;
					if (in_array('administrator', $role)) {
						return $user->ID;
					}
					
					$gfb_user_id = get_user_meta( $user->ID, 'gfb_user', true);
					if ( ! empty( $gfb_user_id ) ) {
						return $user->ID;
					} else {
						$args = apply_filters( 'gfb_add_customer_args', array(
							'post_title' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
							'post_type' => 'gfb-customer',
							'post_status' => 'publish',
							'wp_error'    => true,
							'meta_input'  => array(
								'wp_user' => $user->ID,
								'phone' => trim( $data['phone'] )
							)
						));

						$gfb_user_id = wp_insert_post( $args );

						if ( ! is_wp_error( $gfb_user_id ) ) {
							update_user_meta( $user->ID, 'gfb_user', $gfb_user_id);
							
							if ( ! in_array( 'administrator', $user->roles) ) {
								$user->add_role( 'gfb-customer' );
							}

							return $user->ID;
						}
					}
				} else {
					$user_args = array(										
						'user_login'   => trim( $data['email'] ),
						'user_email'   => trim( $data['email'] ),
						'display_name' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
						'first_name'   => trim( $data['fname'] ),
						'last_name'    => trim( $data['lname'] ),
						'role' 		   => 'gfb-customer',
						'meta_input'  => array(
							'phone' => trim( $data['phone'] )
						)
					);

					$user_args['user_pass'] = isset( $data['password'] ) && ! empty( $data['password'] ) ? trim($data['password']) : gfb_randon_pass();

					$wp_user = wp_insert_user($user_args);

					if ( ! is_wp_error( $wp_user ) ) {					

						$args = apply_filters( 'gfb_add_customer_args', array(
							'post_title' => trim( $data['email'] ),
							'post_type' => 'gfb-customer',
							'post_status' => 'publish',
							'wp_error'    => true,
							'meta_input'  => array(
								'wp_user' => $wp_user,							
								'phone' => trim( $data['phone'] )							
							)
						));

						$gfb_user = wp_insert_post( $args );					

						if ( ! is_wp_error( $gfb_user ) ) {

							update_user_meta( $wp_user, 'gfb_user', $gfb_user );							

							/* EMAIL NOTIFICATION STARTS */
							if ( true === apply_filters('gfb_add_customer_personal_details_notification', true) ) {
								$body = __('Your account has been created, below are the credentials to login', 'gfb');
								GFB_Email::new_customer_notification( $wp_user, $body, $user_args['user_pass'] );
							}
							/* EMAIL NOTIFICATION ENDS */

							return $wp_user;
						}

					}
				}

			}

			return false;
		}

		public function gfb_add_customer() {			

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error',
				'notification' => false
			);

			$data = $_POST;

			if ( ! empty( trim( $data['fname'] ) ) && ! empty( trim( $data['lname'] ) ) && ! empty( trim( $data['email'] ) ) ) {

				
				$user_args = array(											
					'user_login'   => trim( $data['email'] ),
					'user_email'   => trim( $data['email'] ),
					'user_pass'	   => ! empty( $data['password'] ) ? trim($data['password']) : gfb_randon_pass(),
					'display_name' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
					'first_name'   => trim( $data['fname'] ),
					'last_name'    => trim( $data['lname'] ),
					'role' 		   => 'gfb-customer',
					'meta_input'  => array(						
						'info' => trim( $data['info'] ),
						'gender' => trim( $data['gender'] ),
						'phone' => trim( $data['phone'] ),
						'dob' => trim( $data['dob'] ),
					)
				);				

				$wp_user = wp_insert_user($user_args);

				if ( ! is_wp_error( $wp_user ) ) {					

					$args = apply_filters( 'gfb_add_customer_args', array(
						'post_title' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
						'post_type' => 'gfb-customer',
						'post_status' => 'publish',
						'wp_error'    => true,
						'meta_input'  => array(
							'wp_user' => $wp_user,
							'info' => trim( $data['info'] ),
							'gender' => trim( $data['gender'] ),
							'phone' => trim( $data['phone'] ),
							'dob' => trim( $data['dob'] ),
						)
					));

					$gfb_user = wp_insert_post( $args );

					if ( ! is_wp_error( $gfb_user ) ) {

						update_user_meta( $wp_user, 'gfb_user', $gfb_user );
						$response['message'] = esc_html__( 'Customer created successfully.', 'gfb' );
						$response['status'] = 'success';

						/* EMAIL NOTIFICATION STARTS */
						if ( true === apply_filters('gfb_add_customer_personal_details_notification', true) ) {
							$body = __('Your account has been created, below are the credentials to login', 'gfb');
							$response['notification'] = GFB_Email::new_customer_notification( $wp_user, $body, $user_args['user_pass'] );
						}
						/* EMAIL NOTIFICATION ENDS */

					} else {
						
						wp_delete_user( $wp_user );
						$response['message'] = $wp_user->get_error_message();

					}

				} else {

					$response['message'] = $wp_user->get_error_message();

				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_custom_column( $column, $postId ) {

			$user_id = get_post_meta( $postId, 'wp_user', true );
			$userdata = get_userdata( $user_id );			

			if ( ! $userdata ) {
				return $column;
			}
			
			switch ($column) {
				case 'id':
					echo '&nbsp;&nbsp;' . esc_attr($postId);
					break;

				case 'name':
					echo esc_attr($userdata->display_name);
					break;

				case 'phone':
					echo get_post_meta($postId, 'phone', true);
					break;
				
				case 'email':
					echo esc_attr($userdata->user_email);
					break;
				
				case 'actions':
					echo '<a href="#" data-type="customer" data-id="gfb-customer-modal" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-edit"> <i class="fa fa-edit customer--icon"></i> </a>';					
					if ( 'publish' == get_post_status($postId) ) {
						echo '<a href="#" data-type="customer" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-delete" title="Delete"> <i class="fa fa-trash customer--icon"></i>
						</a>';
					}
					break;
			}
		}

		public function gfb_modify_column_names( $columns ) {			
			unset($columns['date']);
			unset($columns['title']);
			unset($columns['cb']);
						
			$columns['id'] = __('&nbsp;&nbsp;Customer ID', 'gfb');
			$columns['name'] = __('Name', 'gfb');
			$columns['phone'] = __('Phone', 'gfb');
			$columns['email'] = __('Email', 'gfb');			
			$columns['actions'] = __('Actions', 'wc-donation');

			return $columns;
		}

		public function gfb_manage_posts_extra_tablenav( $which ) {
			if ( 'top' === $which ) {
				echo sprintf('<a href="#" data-id="gfb-customer-modal" class="gfb-add-button"><i class="fa fa-plus-circle"></i>%1$s</a>', esc_html__('Add Customers', 'gfb') );
			}
		}

		public function highlight_menu() {
			
			//adding customer modal view file
			require_once GFB_ADMIN_VIEW_PATH . 'modal/customer-modal.php';
			?>
			<script type="text/javascript">
				//debugger;					
				jQuery('#toplevel_page_gfb li, #toplevel_page_gfb li > a').removeClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-customer"]').addClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-customer"]').parent('li').addClass('current');
			</script>
			<?php
		}

		public function register_customer_posttype() {
			$labels = array(
				'name'                  => _x( 'Customer', 'Post type general name', 'gfb' ),
				'singular_name'         => _x( 'Customer', 'Post type singular name', 'gfb' ),
				'menu_name'             => _x( 'Customer', 'Admin Menu text', 'gfb' ),
				'name_admin_bar'        => _x( 'Customer', 'Add New on Toolbar', 'gfb' ),
				'add_new'               => __( 'Add Customer', 'gfb' ),
				'add_new_item'          => __( 'Add New Customer', 'gfb' ),
				'new_item'              => __( 'New Customer', 'gfb' ),
				'edit_item'             => __( 'Edit Customer', 'gfb' ),
				'view_item'             => __( 'View Customer', 'gfb' ),
				'all_items'             => __( 'Customers', 'gfb' ),
				'search_items'          => __( 'Search Customer', 'gfb' ),
				'parent_item_colon'     => __( 'Parent Customer:', 'gfb' ),
				'not_found'             => __( 'No Customer found.', 'gfb' ),
				'not_found_in_trash'    => __( 'No Customer found in Trash.', 'gfb' ),
				'featured_image'        => _x( 'Customer Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'gfb' ),
				'set_featured_image'    => _x( 'Set Customer image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'remove_featured_image' => _x( 'Remove Customer image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'use_featured_image'    => _x( 'Use as Customer image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'archives'              => _x( 'Customer archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'gfb' ),
				'insert_into_item'      => _x( 'Insert into Customer', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'gfb' ),
				'uploaded_to_this_item' => _x( 'Uploaded to this Customer', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'gfb' ),
				'filter_items_list'     => _x( 'Filter Customer list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'gfb' ),
				'items_list_navigation' => _x( 'Customer list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'gfb' ),
				'items_list'            => _x( 'Customer list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'gfb' ),
			);

			$args = array(
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => 'gfb',
				'show_in_rest'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'gfb-customer' ),
				'capability_type'    => 'post',
				'capabilities' => array(
					'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				),
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'thumbnail' ),				
			);

			register_post_type( 'gfb-customer', $args );
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Customer();
			}

			return self::$instance;
		}
	}

	GFB_Customer::get_instance();
}

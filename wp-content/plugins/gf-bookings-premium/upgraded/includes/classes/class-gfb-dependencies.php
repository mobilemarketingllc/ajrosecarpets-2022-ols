<?php
/**
 * GFB Dependency Checker
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Dependencies' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Dependencies {


		/**
		 * Active plugins
		 *
		 * @var static
		 */
		private static $active_plugins;

		/**
		 * Init the Dependencies.
		 */
		public static function init() {

			self::$active_plugins = (array) get_option( 'active_plugins', array() );

			if ( is_multisite() ) {
				self::$active_plugins = array_merge( self::$active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
			}
		}

		/**
		 * Gravity Form active checker.
		 */
		public static function active_check( $plugin_file = '' ) {

			if ( ! self::$active_plugins ) {
				self::init();
			}

			return in_array( $plugin_file, self::$active_plugins, true ) || array_key_exists( $plugin_file, self::$active_plugins );
		}
	}
}

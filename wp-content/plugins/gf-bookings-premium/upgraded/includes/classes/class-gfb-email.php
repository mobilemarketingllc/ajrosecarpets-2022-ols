<?php
/**
 * GFB Email Notification Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Email' ) ) {
	
	class GFB_Email {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $header = array();

		public static $email_templates = array();

		public static $gfb_setting = array();

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			$gfb_email_templates = get_option('gfb_email_templates', array());

			if ( is_array( $gfb_email_templates ) && count( $gfb_email_templates ) > 0 ) {						
				self::$email_templates = $gfb_email_templates;
			} else {				
				self::$email_templates = array(
					'template_1' => array(
						'id' => 1,
						'title' => __('Email to customer about appointment confirmation', 'gfb'),
						'subject' => 'Notification About Appointment Confirmation',
						'message' => wpautop( stripslashes('Dear {client_name},

	This is just an email to confirm your appointment with {staff_name}. For reference, here\'s the appointment information:

	Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name}

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_2' => array(
						'id' => 2,
						'title' => __('Email to customer about visit confirmation', 'gfb'),
						'subject' => 'Thank You For Visit',
						'message' => wpautop( stripslashes('Dear {client_name},

	This is to thank you for your visit at our place. We hope that you were satisfied with our services.
	If any issue or query, you are free to call us. Thank You.

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_3' => array(
						'id' => 3,
						'title' => __('Email to customer about appointment cancellation', 'gfb'),
						'subject' => 'Notification About Appopintment Cancellation',
						'message' => wpautop( stripslashes('Dear {client_name},

	This is just an email to inform you that your appointment with {staff_name} has been cancelled. For reference, here\'s the appointment information:

	Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name} .
	We regret about your inconvenience from our side.
	Thank You

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_4' => array(
						'id' => 4,
						'title' => __('Email to admin about customer visit', 'gfb'),
						'subject' => 'Notification About Customer Visit',
						'message' => wpautop( stripslashes('Dear Administrator,

	This is an email inform you about cancallation about the appointment with {staff_name}. For reference, here\'s the appointment information:

	Client Name: {client_name} Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name}.
	Thank You

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_5' => array(
						'id' => 5,
						'title' => __('Email to admin about appointment request', 'gfb'),
						'subject' => 'Notification About Appointment Request',
						'message' => wpautop( stripslashes('Dear Administrator,

	This is an email to inform you about a new appointment request. For reference, here\'s the appointment information:

	Client Name: {client_name} Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name}
	Thank You.

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_6' => array(
						'id' => 6,
						'title' => __('Email to admin about appointment cancellation', 'gfb'),
						'subject' => 'Notification About Appointment Cancellation',
						'message' => wpautop( stripslashes('Dear Administrator,

	This is an email to inform you about an appointment has been cancelled. For reference, here\'s the appointment information:

	Client Name: {client_name} Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name}
	Thank You.

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_7' => array(
						'id' => 7,
						'title' => __('Email to staff about appointment cancellation', 'gfb'),
						'subject' => 'Notification About Appointment Cancellation',
						'message' => wpautop( stripslashes('Dear {staff_name},

	This is an email to inform you about an appointment has been cancelled. For reference, here\'s the appointment information:

	Client Name: {client_name} Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name}
	Thank You.

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}')),
						'switch' => 'disabled'
					),
					'template_8' => array(
						'id' => 8,
						'title' => __('Email to staff about appointment request', 'gfb'),
						'subject' => 'Notification About Appointment Request',
						'message' => wpautop( stripslashes( 'Dear {staff_name},
	This is an email to inform you about a new appointment request for you. For reference, here\'s the appointment information:

	Client Name: {client_name} Date: {appointment_date}
	Time: {appointment_time}
	Service: {service_name}
	Staff: {staff_name}
	Thank You.

	Sincerely,
	{company_name} , {company_phone}
	{company_email}
	{company_website}' ) ),
						'switch' => 'disabled'
					),
				);
			}			

			add_action('gfb_before_settings_menu', array($this, 'register_menu'));

			self::$gfb_setting = GFB_Setting::get_settings();

			if ( isset(self::$gfb_setting['gfb_sender_name']) && isset(self::$gfb_setting['gfb_sender_email']) && ! empty(trim(self::$gfb_setting['gfb_sender_name'])) && ! empty(trim(self::$gfb_setting['gfb_sender_email']) ) ) {
				self::$header = array( 'Content-Type: text/html; charset=UTF-8', 'From: ' . self::$gfb_setting['gfb_sender_name'] . ' <' . self::$gfb_setting['gfb_sender_email'] . '>' );
			} else {
				self::$header = array( 'Content-Type: text/html; charset=UTF-8', 'From: ' . esc_attr( get_bloginfo( 'name' ) ) . ' <' . esc_attr( get_option( 'admin_email' ) ) . '>' );
			}

			add_action('wp_ajax_gfb_email_template_settings', array($this, 'gfb_email_template_settings') );


			// echo '<pre>';
			// print_r( self::$email_templates );
			// echo '</pre>';

		}


		public function gfb_email_template_settings() {

			check_ajax_referer( 'gfb', 'nonce' );

			$gfb_email_templates = get_option('gfb_email_templates', array());

			$response = array(
				'message' => esc_html__( 'Email Notification not saved.', 'gfb' ),
				'status'  => 'error'
			);			

			$data = $_POST;
			
			foreach ( self::$email_templates as $key => $template ) {				
				$gfb_email_templates[$key]['id'] = $template['id'];
				$gfb_email_templates[$key]['title'] = $template['title'];
				$gfb_email_templates[$key]['subject'] = $data['subject_' . $template['id']];				
				$gfb_email_templates[$key]['message'] = stripslashes($data['message_' . $template['id']]);
				$gfb_email_templates[$key]['switch'] = $data['switch_' . $template['id']];
			}

			update_option('gfb_email_templates', $gfb_email_templates );			
			$response['message'] = __('Email Notification saved successfully', 'gfb');
			$response['status'] = 'success';
			$response['settings'] = $gfb_email_templates;

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_codes() {
			
			return apply_filters( "gfb_codes", array(
				"{appointment_date}"    => esc_html__( "date of appointment", "gfb" ),
				"{appointment_time}"    => esc_html__( "time of appointment", "gfb" ),
				"{booking_ref_number}"  => esc_html__( "booking reference number of appointment", "gfb" ),
				"{client_email}"        => esc_html__( "email of client", "gfb" ),
				"{client_name}"  		=> esc_html__( "name of client", "gfb" ),
				"{client_phone}"		=> esc_html__( "contact no of client", "gfb" ),
				"{company_address}" 	=> esc_html__( "address of company", "gfb" ),
				"{company_name}" 		=> esc_html__( "name of company", "gfb" ),
				"{company_email}" 		=> esc_html__( "company email", "gfb" ),
				"{company_phone}" 		=> esc_html__( "company phone", "gfb" ),
				"{company_website}" 	=> esc_html__( "company website", "gfb" ),
				"{service_name}" 		=> esc_html__( "name of service", "gfb" ),
				"{staff_name}" 			=> esc_html__( "name of staff", "gfb" ),
				"{total_slot}" 	 	    => esc_html__( "total no. of slots", "gfb" ),
				"{total_price}" 		=> esc_html__( "total price of booking", "gfb" ),

			) );
		}

		public function register_menu() {
			add_submenu_page(
            	'gfb',
                __( 'Email Notifications - Gravity Booking', 'gfb' ),
                __( 'Email Notifications', 'gfb' ),
                'manage_options',
                'gfb-email',
                array( get_class($this), 'gfb_email_templates' ),
                10
            );
		}

		public static function gfb_email_templates() {
			require_once GFB_ADMIN_VIEW_PATH . 'email-templates.php';
		}

		public static function get_appointment_details_formatted( $appointment_id, $type = 'staff' ) {

			$data = array();
			$staff_id = get_post_meta( $appointment_id, 'staff_id', true );
            $customer_id = get_post_meta( $appointment_id, 'gfb_user', true );
            $customer_phone = get_post_meta( $customer_id, 'phone', true);
            $location_id = get_post_meta( $appointment_id, 'location_id', true );
            $service_id = get_post_meta( $appointment_id, 'service_id', true );
            $date = get_post_meta( $appointment_id, 'date', true );
            $date = apply_filters('gfb_entry_value_date_format', $date );
            $time = get_post_meta( $appointment_id, 'time', true );            
            $time = apply_filters('gfb_entry_value_time_format', $time );
            $cost = get_post_meta( $appointment_id, 'cost', true );
            $total_price = gfb_price_format( $cost );
            $total_slot = get_post_meta( $appointment_id, 'slot', true );
            $status = get_post_meta( $appointment_id, 'status', true );
            $staff_name = get_the_title($staff_id);
            $customer_user_id = get_post_meta($appointment_id, 'wp_user', true);
            $staff_user_id = get_post_meta($staff_id, 'wp_user', true);
            $staff_userdata = get_userdata( $staff_user_id );
			if ( $staff_userdata ) {
				$staff_email = $staff_userdata->user_email;
			} else {
				$staff_email = '';
			}

			$customer_userdata = get_userdata( $customer_user_id );
			if ( $customer_userdata ) {
				$customer_email = $customer_userdata->user_email;
			} else {
				$customer_email = $customer_userdata->user_email;
			}
            
            $customer_name = $customer_userdata->first_name . ' ' . $customer_userdata->last_name;
            $location_name = ('all' != $location_id) ? get_the_title($location_id) : '';
            $service_name = ('all' != $service_id) ? get_the_title($service_id) : '';            

            $data['booking_ref_number'] = $appointment_id;
            $data['customer_user_id'] = $customer_user_id;            
            $data['staff_user_id'] = $staff_user_id;
            $data['staff_id'] = $staff_id;
            $data['location_id'] = $location_id;
            $data['service_id'] = $service_id;
            $data['location_name'] = $location_name;
            $data['service_name'] = $service_name;
            $data['staff_name'] = $staff_name;
            $data['staff_email'] = $staff_email;
            $data['customer_name'] = $customer_name;
            $data['customer_email'] = $customer_email;
            $data['customer_phone'] = $customer_phone;   
            $data['date'] = $date;
            $data['time'] = $time;
            $data['status'] = $status;
            $data['total_price'] = $total_price;
            $data['total_slot'] = $total_slot;
            $data['company_addr'] = self::$gfb_setting['gfb_company_address'];
            $data['compnay_name'] = self::$gfb_setting['gfb_company_name'];
            $data['company_email'] = self::$gfb_setting['gfb_company_email'];
            $data['company_phone'] = self::$gfb_setting['gfb_company_number'];
            $data['company_website'] = self::$gfb_setting['gfb_company_website_url'];
            $data['footer'] = apply_filters('gfb_email_template_footer', '');

            return $data;
		}

		public static function send_notification( $appointment_id='', $template_id = '', $send_to = '' ) {

			$template_switch  = self::$email_templates[$template_id]['switch'];			

			if ( empty($send_to) || empty($appointment_id) || empty($template_id) || 'disabled' == $template_switch ) {
				return false;
			}

			$data = self::get_appointment_details_formatted($appointment_id);			
			
			if ( 'staff' == $send_to ) {
				$to_email = $data['staff_email'];
			}

			if ( 'customer' == $send_to ) {
				$to_email = $data['customer_email'];
			}

			if ( 'admin' == $send_to ) {
				$to_email = get_option('admin_email');
			}
				
			$template_title   = self::$email_templates[$template_id]['title'];
			$template_subject = self::$email_templates[$template_id]['subject'];
			$template_message = self::$email_templates[$template_id]['message'];

			$template_message = str_replace('{appointment_date}', $data['date'], $template_message);
			$template_message = str_replace('{appointment_time}', $data['time'], $template_message);
			$template_message = str_replace('{booking_ref_number}', $data['booking_ref_number'], $template_message);
			$template_message = str_replace('{client_email}', $data['customer_email'], $template_message);
			$template_message = str_replace('{client_name}', $data['customer_name'], $template_message);
			$template_message = str_replace('{client_phone}', $data['customer_phone'], $template_message);
			$template_message = str_replace('{company_address}', $data['company_addr'], $template_message);
			$template_message = str_replace('{company_name}', $data['compnay_name'], $template_message);
			$template_message = str_replace('{company_email}', $data['company_email'], $template_message);
			$template_message = str_replace('{company_phone}', $data['company_phone'], $template_message);
			$template_message = str_replace('{company_website}', $data['company_website'], $template_message);
			$template_message = str_replace('{service_name}', $data['service_name'], $template_message);
			$template_message = str_replace('{staff_name}', $data['staff_name'], $template_message);
			$template_message = str_replace('{total_slot}', $data['total_slot'], $template_message);
			$template_message = str_replace('{total_price}', $data['total_price'], $template_message);			
			
			ob_start();
			require GFB_VIEW_PATH . 'email-template.php';
			$message = ob_get_clean();
			
			return wp_mail( $to_email, $template_subject, $message, self::$header );			
			
		}

		public static function new_staff_notification( $user_id, $body, $pass ) {

			/* EMAIL NOTIFICATION STARTS */
			$userdata = get_userdata( $user_id );
			if ( $userdata ) {
				$to_email = $userdata->user_email;
				/**				 
				 * GFB filter.
				 * 
				 * @since 1.0.0
				 */
				$subject = apply_filters( 'gfb_new_staff_email_subject', __( 'New Staff Notification', 'gfb' ) );
				
				// Translators: %1$s %2$s, %3$s %4$s %5$s %6$s: %7$s %8$s %9$s %10$s to their strings respectively.
				$message = sprintf( '%1$s %2$s, %3$s %4$s %5$s %6$s: %7$s %8$s %9$s: %10$s %11$s %12$s: <a href="' . admin_url('edit.php?post_type=gfb-staff') . '">%13$s</a>', __('Dear', 'gfb'), esc_attr( $userdata->display_name ), '<br />', esc_attr($body), '<br />', __('Email', 'gfb'), esc_attr($userdata->user_email), '<br />', __('Password', 'gfb'), esc_attr( $pass ), '<br/>', __('Login URL', 'gfb'), __('Click here to login', 'gfb') );

				/**				 
				 * GFB filter.
				 * 
				 * @since 1.0.0
				 */
				$message = apply_filters( 'gfb_new_staff_email_message', $message);

				// sending notification email to registered user for fundraiser role.
				return wp_mail( $to_email, $subject, $message, self::$header );
			}

			return false;			
		}

		public static function new_customer_notification( $user_id, $body, $pass ) {

			/* EMAIL NOTIFICATION STARTS */
			$userdata = get_userdata( $user_id );
			if ( $userdata ) {
				$to_email = $userdata->user_email;
				/**				 
				 * GFB filter.
				 * 
				 * @since 1.0.0
				 */
				$subject = apply_filters( 'gfb_new_customer_email_subject', __( 'New Customer Notification', 'gfb' ) );
				
				// Translators: %1$s %2$s, %3$s %4$s %5$s %6$s: %7$s %8$s %9$s %10$s to their strings respectively.
				$message = sprintf( '%1$s %2$s, %3$s %4$s %5$s %6$s: %7$s %8$s %9$s: %10$s %11$s %12$s: <a href="' . esc_url( wp_login_url() ) . '">%13$s</a>', __('Dear', 'gfb'), esc_attr( $userdata->display_name ), '<br />', esc_attr($body), '<br />', __('Email', 'gfb'), esc_attr($userdata->user_email), '<br />', __('Password', 'gfb'), esc_attr( $pass ), '<br/>', __('Login URL', 'gfb'), __('Click here to login', 'gfb') );

				/**				 
				 * GFB filter.
				 * 
				 * @since 1.0.0
				 */
				$message = apply_filters( 'gfb_new_customer_email_message', $message);
				
				return wp_mail( $to_email, $subject, $message, self::$header );
			}

			return false;			
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Email();
			}

			return self::$instance;
		}
	}

	global $gfbEmail;

	$gfbEmail = GFB_Email::get_instance();
}

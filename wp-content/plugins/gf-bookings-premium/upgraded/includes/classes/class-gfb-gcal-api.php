<?php

class GFB_CAL_API {

	public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {	
		
		$url = 'https://accounts.google.com/o/oauth2/token';			
		$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if ($http_code != 200) {
			throw new Exception('Error : Failed to receieve access token');
		}
		
		$data['expires_at'] = time() + $data['expires_in'];
		return $data;
	}

	public function RefreshAccessToken($client_id, $client_secret, $refresh_token) {	
		
		$url = 'https://accounts.google.com/o/oauth2/token';
		$curlPost = 'client_id=' . $client_id . '&client_secret=' . $client_secret . '&refresh_token=' . $refresh_token . '&grant_type=refresh_token';

		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if ($http_code != 200) {
			throw new Exception('Error : Failed to receieve access token');
		}
		
		$data['refresh_token'] = $refresh_token;
		$data['expires_at'] = time() + $data['expires_in'];
		return $data;
	}

	public function GetUserCalendarTimezone($access_token) {
		$url_settings = 'https://www.googleapis.com/calendar/v3/users/me/settings/timezone';
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_settings);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));	
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);	
		$data = json_decode(curl_exec($ch), true); //echo '<pre>';print_r($data);echo '</pre>';
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if ($http_code != 200) {
			throw new Exception('Error : Failed to get timezone');
		}

		return $data['value'];
	}

	public function GetCalendarsList($access_token) {
		$url_parameters = array();

		$url_parameters['fields'] = 'items(id,summary,timeZone)';
		$url_parameters['minAccessRole'] = 'owner';

		$url_calendars = 'https://www.googleapis.com/calendar/v3/users/me/calendarList?'. http_build_query($url_parameters);
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_calendars);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));	
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if ($http_code != 200) {
			throw new Exception('Error : Failed to get calendars list');
		}
		return $data['items'];
	}

	public function CreateCalendarEvent($calendar_id, $summary, $description, $staff_timezone, $event_time, $event_timezone, $access_token, $type = 'staff') {
		$url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events';		
		// if ( 1 == $all_day) {
		// 	$curlPost['start'] = array('date' => $event_time['event_date']);
		// 	$curlPost['end'] = array('date' => $event_time['event_date']);
		// } else {
		// 	$curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $event_timezone);
		// 	$curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $event_timezone);
		// }

		$curlPost['summary'] = $summary;
		$curlPost['description'] = $description;
		if ( 'customer' == $type ) {
			$curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $event_timezone);
			$curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $event_timezone);		
		} else {
			$curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $staff_timezone);
			$curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $staff_timezone);		
		}
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_events);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json'));	
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curlPost));		
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if ($http_code != 200) {
			throw new Exception('Error : Failed to create event');
		}

		return $data['id'];
	}

	public function DeleteCalendarEvent($event_id, $calendar_id, $access_token) {
		$url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events/' . $event_id;

		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_events);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json'));		
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		if ($http_code != 200) { 
			throw new Exception('Error : Failed to delete event');
		}

		return $data;
	}

	public function revokeToken($refreshToken) {
		
		$revoke_uri = 'https://accounts.google.com/o/oauth2/revoke?token='.$refreshToken ;
			
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $revoke_uri);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type' => 'application/x-www-form-urlencoded'));	
		
		$data = json_decode(curl_exec($ch), true);

		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

		if ($http_code != 200) {
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to revoke account') );			
		}

		return $http_code;
	}
}
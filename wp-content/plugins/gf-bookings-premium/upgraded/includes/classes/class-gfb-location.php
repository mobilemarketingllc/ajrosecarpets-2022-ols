<?php
/**
 * GFB Location Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Location' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Location {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $checkPostType = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			self::$checkPostType = isset( $_REQUEST['post_type'] ) && ! empty( sanitize_text_field($_REQUEST['post_type']) ) ? sanitize_text_field($_REQUEST['post_type']) : '';

			add_action('init', array($this, 'register_location_posttype'));
			add_filter( 'manage_gfb-location_posts_columns', array( $this, 'gfb_modify_column_names') );
			add_action( 'manage_gfb-location_posts_custom_column', array( $this, 'gfb_add_custom_column'), 9, 2 );

			add_action( 'wp_ajax_gfb_add_location', array( $this, 'gfb_add_location' ) );
			add_action( 'wp_ajax_gfb_edit_location', array( $this, 'gfb_edit_location' ) );
			add_action( 'wp_ajax_gfb_delete_location', array( $this, 'gfb_delete_location' ) );
			add_action( 'wp_ajax_gfb_show_location_data', array( $this, 'gfb_show_location_data' ) );

			if ( 'gfb-location' === self::$checkPostType ) {

				add_action( 'admin_footer', array( $this, 'highlight_menu' ) );

				add_filter('post_row_actions', '__return_empty_array');

				add_action('manage_posts_extra_tablenav', array($this, 'gfb_manage_posts_extra_tablenav'));

				add_filter('months_dropdown_results', '__return_empty_array');

				add_filter( 'bulk_actions-edit-gfb-location', array($this, 'gfb_add_bulk_delete') ); // '__return_empty_array'

				add_action( 'views_edit-gfb-location',  function ( $views ) {

					unset($views['all']);
					unset($views['publish']);
    				unset($views['trash']);
    				unset($views['draft']);

					return $views;
				});

				// the function that filters posts
				add_action( 'pre_get_posts', array( $this, 'gfb_filter_query' ) );
			}
			
		}

		public function gfb_filter_query( $admin_query ) {

			if ( is_admin() && $admin_query->is_main_query() ) {

				if ( is_user_logged_in() ) {

					if( absint($admin_query->query_vars['s']) ) {
			            // Set the post id value
			            $admin_query->set('p', $admin_query->query_vars['s']);

			            // Reset the search value
			            $admin_query->set('s', '');
			        }			
				}				
			}			

			return $admin_query;
		}

		public function gfb_add_bulk_delete( $actions ) {

			unset( $actions['edit'] );
			unset( $actions['trash'] );
			$actions['delete'] = __('Delete All', 'gfb');

			return $actions;
		}

		public static function all_location( $location_id = '' ) {

			if ( ! empty( $location_id ) ) {
				$get_location = get_post( $location_id );
				if ( $get_location && 'gfb-location' == $get_location->post_type ) {					
					return $get_location;
				}
			}

			$get_location = get_posts( array(
				'post_type' => 'gfb-location',
				'numberposts' => -1,
				'status' => 'publish',

			));			

			return $get_location;
		}

		public function gfb_edit_location() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) && ! empty( trim( $data['name'] ) ) ) {

				$post = get_post( $data['post_id'] );
				if ( $post && 'gfb-location' == $post->post_type ) {
					$args = apply_filters( 'gfb_edit_staff_args', array(
						'ID'         => $data['post_id'],
						'post_title' => trim( $data['name'] ),
						'post_type' => 'gfb-location',
						'post_status' => 'publish',
						'wp_error'    => true,
						'meta_input'  => array(
							'phone' => trim( $data['phone'] ),
							'address' => trim( $data['address'] ),
							'description' => trim( $data['description'] )
						)
					));					

					$post = wp_update_post( $args );

					if ( ! is_wp_error( $post ) ) {
						
						$response['message'] = esc_html__( 'Location updated successfully.', 'gfb' );
						$response['status'] = 'success';

					} else {

						$response['message'] = $post->get_error_message();
					}
				} else {
					$response['message'] = esc_html__('Location doesn\'t exist.', 'gfb');
				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_location() {			

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error'				
			);

			$data = $_POST;

			if ( ! empty( trim( $data['name'] ) ) ) {

				$args = apply_filters( 'gfb_add_location_args', array(
					'post_title' => trim( $data['name'] ),
					'post_type' => 'gfb-location',
					'post_status' => 'publish',
					'wp_error'    => true,
					'meta_input'  => array(
						'phone' => trim( $data['phone'] ),
						'address' => trim( $data['address'] ),
						'description' => trim( $data['description'] )						
					)
				));

				$post = wp_insert_post( $args );

				if ( ! is_wp_error( $post ) ) {					
					$response['message'] = esc_html__( 'Location created successfully.', 'gfb' );
					$response['status'] = 'success';
				} else {					
					$response['message'] = $post->get_error_message();
				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_delete_location() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Location not deleted.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {
				
				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-location' == $post->post_type ) {
					wp_delete_post( $post->ID, true );
					$response['message'] = esc_html__( 'Location deleted successfully.', 'gfb' );
					$response['status'] = 'success';
				}				
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_show_location_data() {
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(			
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-location' == $post->post_type ) {

					$phone = get_post_meta( $data['post_id'], 'phone', true);
					$address = get_post_meta( $data['post_id'], 'address', true);
					$description = get_post_meta( $data['post_id'], 'description', true);

					$response['name'] = $post->post_title;
					$response['phone'] = $phone;
					$response['address'] = $address;
					$response['description'] = $description;
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_custom_column( $column, $postId ) {	
			
			switch ($column) {

				case 'id':
					echo '&nbsp;&nbsp;' . esc_attr($postId);
					break;

				case 'name':
					echo esc_html(get_the_title($postId));
					break;

				case 'phone':
					echo get_post_meta( $postId, 'phone', true );
					break;
				
				case 'address':
					echo get_post_meta( $postId, 'address', true );
					break;
				
				case 'actions':
					echo '<a href="#" data-type="location" data-id="gfb-location-modal" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-edit"> <i class="fa fa-edit customer--icon"></i></a>';
					if ( 'publish' == get_post_status($postId) ) {
						echo '<a href="#" data-type="location" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-delete" title="Delete"> <i class="fa fa-trash customer--icon"></i></a>';
					}
					break;
			}
		}

		public function gfb_modify_column_names( $columns ) {			
			unset($columns['date']);
			unset($columns['title']);
			// unset($columns['cb']);

			$columns['id'] = __('&nbsp;&nbsp;ID', 'gfb');
			$columns['name'] = __('Name', 'gfb');
			$columns['phone'] = __('Phone', 'gfb');
			$columns['address'] = __('Address', 'gfb');
			$columns['actions'] = __('Actions', 'wc-donation');

			return $columns;
		}

		public function gfb_manage_posts_extra_tablenav( $which ) {
			if ( 'top' === $which ) {
				echo sprintf('<a href="#" data-id="gfb-location-modal" class="gfb-add-button"><i class="fa fa-plus-circle"></i>%1$s</a>', esc_html__('Add Location', 'gfb') );
			}			
		}

		public function highlight_menu() {
			
			//adding customer modal view file
			require_once GFB_ADMIN_VIEW_PATH . 'modal/location-modal.php';

			?>
			<script type="text/javascript">
				//debugger;					
				jQuery('#toplevel_page_gfb li, #toplevel_page_gfb li > a').removeClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-location"]').addClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-location"]').parent('li').addClass('current');
			</script>
			<?php
		}

		public function register_location_posttype() {
			$labels = array(
				'name'                  => _x( 'Location', 'Post type general name', 'gfb' ),
				'singular_name'         => _x( 'Location', 'Post type singular name', 'gfb' ),
				'menu_name'             => _x( 'Location', 'Admin Menu text', 'gfb' ),
				'name_admin_bar'        => _x( 'Location', 'Add New on Toolbar', 'gfb' ),
				'add_new'               => __( 'Add Location', 'gfb' ),
				'add_new_item'          => __( 'Add New Location', 'gfb' ),
				'new_item'              => __( 'New Location', 'gfb' ),
				'edit_item'             => __( 'Edit Location', 'gfb' ),
				'view_item'             => __( 'View Location', 'gfb' ),
				'all_items'             => __( 'Locations', 'gfb' ),
				'search_items'          => __( 'Search Location', 'gfb' ),
				'parent_item_colon'     => __( 'Parent Location:', 'gfb' ),
				'not_found'             => __( 'No Location found.', 'gfb' ),
				'not_found_in_trash'    => __( 'No Location found in Trash.', 'gfb' ),
				'featured_image'        => _x( 'Location Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'gfb' ),
				'set_featured_image'    => _x( 'Set Location image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'remove_featured_image' => _x( 'Remove Location image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'use_featured_image'    => _x( 'Use as Location image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'archives'              => _x( 'Location archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'gfb' ),
				'insert_into_item'      => _x( 'Insert into Location', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'gfb' ),
				'uploaded_to_this_item' => _x( 'Uploaded to this Location', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'gfb' ),
				'filter_items_list'     => _x( 'Filter Location list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'gfb' ),
				'items_list_navigation' => _x( 'Location list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'gfb' ),
				'items_list'            => _x( 'Location list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'gfb' ),
			);

			$args = array(
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => 'gfb',
				'show_in_rest'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'gfb-location' ),
				'capability_type'    => 'post',
				// 'capabilities' => array(
				// 	'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				// ),
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'thumbnail' ),				
			);

			register_post_type( 'gfb-location', $args );
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Location();
			}

			return self::$instance;
		}
	}

	GFB_Location::get_instance();
}

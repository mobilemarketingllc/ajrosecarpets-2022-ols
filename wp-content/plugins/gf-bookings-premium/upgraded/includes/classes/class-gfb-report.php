<?php
/**
 * GFB Report Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Report' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Report {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;		

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {			

			add_action('gfb_after_menu_register', array($this, 'register_menu'));

			add_action('wp_ajax_gfb_report_for_appointment_status', array($this, 'gfb_report_for_appointment_status') );

			add_action('wp_ajax_gfb_report_for_staff_earning', array($this, 'gfb_report_for_staff_earning') );

			add_action('wp_ajax_gfb_report_for_staff_appointment', array($this, 'gfb_report_for_staff_appointment') );

			add_action('wp_ajax_gfb_report_for_service_earning', array($this, 'gfb_report_for_service_earning') );
		}

		public function gfb_report_for_service_earning() {
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array();
			$services = GFB_Service::all_service();
			if ( is_array( $services ) && count( $services ) > 0 ) {
				foreach( $services as $service ) {
					$appointment_ids = GFB_Appointment::gfb_appointments_by_services( $service->ID );
					$total_earning = 0;
					if ( is_array( $appointment_ids ) && count($appointment_ids) > 0 ) {
						foreach( $appointment_ids as $appointment_id ) {
							$status = get_post_meta( $appointment_id, 'status', true );
							if ( 'visited' == $status || 'confirmed' == $status ) {
								$cost = get_post_meta( $appointment_id, 'cost', true );
								$total_earning += $cost;
							}
						}
					}

					$response['service'][] = array( 'name' => get_the_title($service->ID), 'total_earning' => $total_earning );
				}
			}			

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_report_for_staff_appointment() {
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array();
			$staffs = GFB_Staff::all_staff();
			if ( is_array( $staffs ) && count( $staffs ) > 0 ) {
				foreach( $staffs as $staff ) {
					$appointment_ids = GFB_Appointment::gfb_appointments_by_staff( $staff->ID );
					if ( is_array( $appointment_ids ) ) {
						$total_appointment = count( $appointment_ids );						
					} else {
						$total_appointment = 0;
					}

					$response['staff'][] = array( 'name' => get_the_title($staff->ID), 'total_appointment' => $total_appointment );
				}
			}			

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_report_for_staff_earning() {
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array();
			$staffs = GFB_Staff::all_staff();
			if ( is_array( $staffs ) && count( $staffs ) > 0 ) {
				foreach( $staffs as $staff ) {
					$appointment_ids = GFB_Appointment::gfb_appointments_by_staff( $staff->ID );
					$total_earning = 0;
					if ( is_array( $appointment_ids ) && count($appointment_ids) > 0 ) {
						foreach( $appointment_ids as $appointment_id ) {
							$status = get_post_meta( $appointment_id, 'status', true );
							if ( 'visited' == $status || 'confirmed' == $status ) {
								$cost = get_post_meta( $appointment_id, 'cost', true );
								$total_earning += $cost;
							}
						}
					}

					$response['staff'][] = array( 'name' => get_the_title($staff->ID), 'total_earning' => $total_earning );
				}
			}			

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_report_for_appointment_status() {
			check_ajax_referer( 'gfb', 'nonce' );

			$response = array();

			$pending_appointment = GFB_Appointment::gfb_appointments_by_status('pending');
			if ( is_array($pending_appointment) ) {
				$pending_appointment = count( $pending_appointment );
			} else {
				$pending_appointment = 0;
			}

			$confirmed_appointment = GFB_Appointment::gfb_appointments_by_status('confirmed');
			if ( is_array($confirmed_appointment) ) {
				$confirmed_appointment = count( $confirmed_appointment );
			} else {
				$confirmed_appointment = 0;
			}

			$visited_appointment = GFB_Appointment::gfb_appointments_by_status('visited');
			if ( is_array($visited_appointment) ) {
				$visited_appointment = count( $visited_appointment );
			} else {
				$visited_appointment = 0;
			}

			$cancelled_appointment = GFB_Appointment::gfb_appointments_by_status('cancelled');
			if ( is_array($cancelled_appointment) ) {
				$cancelled_appointment = count( $cancelled_appointment );
			} else {
				$cancelled_appointment = 0;
			}

			$response['status'] = array(
				0 => array('label' => __('Pending', 'gfb'), 'value' => $pending_appointment),
				1 => array('label' => __('Confirmed', 'gfb'), 'value' => $confirmed_appointment),
				2 => array('label' => __('Visited', 'gfb'), 'value' => $visited_appointment),
				3 => array('label' => __('Cancelled', 'gfb'), 'value' => $cancelled_appointment)
			);

			echo wp_json_encode( $response );
			wp_die();

		}

		public function register_menu() {
			add_submenu_page(
            	'gfb',
                __( 'Dashboard - Gravity Booking', 'gfb' ),
                __( 'Dashboard', 'gfb' ),
                'manage_options',
                'gfb-report',
                array( get_class($this), 'gfb_report_page' ),
                0
            );
		}

		public static function gfb_report_page() {
			require_once GFB_ADMIN_VIEW_PATH . 'reports.php';
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Report();
			}

			return self::$instance;
		}
	}

	GFB_Report::get_instance();
}

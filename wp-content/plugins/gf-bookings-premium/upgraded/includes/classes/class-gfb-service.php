<?php
/**
 * GFB Service Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Service' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Service {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $checkPostType = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {			

			self::$checkPostType = isset( $_REQUEST['post_type'] ) && ! empty( sanitize_text_field($_REQUEST['post_type']) ) ? sanitize_text_field($_REQUEST['post_type']) : '';

			add_action('init', array($this, 'register_service_posttype'));

			add_action('init', array($this, 'register_service_taxonomy'));

			add_filter( 'manage_gfb-service_posts_columns', array( $this, 'gfb_modify_column_names') );
			add_action( 'manage_gfb-service_posts_custom_column', array( $this, 'gfb_add_custom_column'), 9, 2 );
			add_action( 'gfb_after_menu_register', array($this, 'add_service_category_menu') );

			add_action( 'wp_ajax_gfb_add_service', array( $this, 'gfb_add_service' ) );
			add_action( 'wp_ajax_gfb_edit_service', array( $this, 'gfb_edit_service' ) );
			add_action( 'wp_ajax_gfb_delete_service', array( $this, 'gfb_delete_service' ) );
			add_action( 'wp_ajax_gfb_show_service_data', array( $this, 'gfb_show_service_data' ) );

			add_action( 'parent_file', array( $this, 'menu_highlight'), 10, 1 );

			if ( 'gfb-service' === self::$checkPostType ) {

				add_action( 'admin_footer', array( $this, 'highlight_menu' ) );

				add_filter('post_row_actions', '__return_empty_array');

				add_action('manage_posts_extra_tablenav', array($this, 'gfb_manage_posts_extra_tablenav'));

				add_filter('months_dropdown_results', '__return_empty_array');

				add_filter( 'bulk_actions-edit-gfb-service', array($this, 'gfb_add_bulk_delete') ); //__return_empty_array

				add_action( 'views_edit-gfb-service',  function($views) {

					unset($views['all']);
					unset($views['publish']);
    				unset($views['trash']);
    				unset($views['draft']);    				

					return $views;
				});

				// the function that filters posts
				add_action( 'pre_get_posts', array( $this, 'gfb_filter_query' ) );			
			}

			add_filter('gfb-service-category_row_actions', function($actions) {
				unset($actions['view']);
				unset($actions['edit']);
			    return $actions;
			}, 90, 1);

			add_filter('manage_edit-gfb-service-category_columns', function($columns) {
			    if ( isset( $columns['description'] ) ) {
			        unset( $columns['description'] );   
			    }
			    return $columns;
			}, 99, 1);

			add_action( "gfb-service-category_add_form", function() {
				echo "<style> .term-description-wrap { display:none; } </style>";
				echo "<style> .term-parent-wrap { display:none; } </style>";
			});

			add_action( "gfb-service-category_edit_form", function() {
				echo "<style> .term-description-wrap { display:none; } </style>";
				echo "<style> .term-parent-wrap { display:none; } </style>";
			});

			add_action( 'wp_ajax_gfb_get_services_by_location', array( $this, 'gfb_get_services_by_location' ) );
			add_action( 'wp_ajax_nopriv_gfb_get_services_by_location', array( $this, 'gfb_get_services_by_location' ) );
			
		}

		public function gfb_filter_query( $admin_query ) {

			if ( is_admin() && $admin_query->is_main_query() ) {

				if ( is_user_logged_in() ) {

					if( absint($admin_query->query_vars['s']) ) {
			            // Set the post id value
			            $admin_query->set('p', $admin_query->query_vars['s']);

			            // Reset the search value
			            $admin_query->set('s', '');
			        }			
				}				
			}			

			return $admin_query;
		}

		public function gfb_add_bulk_delete( $actions ) {

			unset( $actions['edit'] );
			unset( $actions['trash'] );
			$actions['delete'] = __('Delete All', 'gfb');

			return $actions;
		}

		public function gfb_get_services_by_location() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(				
				'status'  => 'error',
				'selected_service_id' => ''			
			);

			$data = $_POST;

			$form  = \GFAPI::get_form($data['form_id']);

			if ( is_array( $form['fields'] ) && count($form['fields']) > 0 ) {
				foreach( $form['fields'] as $field ) {
					if ( 'gfb_appointment_calendar' == $field->type ) {
						$response['selected_service_id'] = $field->gfb_service;
						break;
					}
				}					
			}

			if ( isset($data['post_id']) && ! empty( $data['post_id'] ) && 'all' != $data['post_id'] && 'undefined' != $data['post_id'] && 'null' != $data['post_id'] ) {
				
				$post = get_post( $data['post_id'] );				
				
				if ( $post && 'gfb-location' == $post->post_type ) {
					
					$args = array(
						'post_type' => 'gfb-service',
						'numberposts' => -1,
						'status' => 'publish',
						'meta_query' => array(
							'relation' => 'OR',
							array(
								'key' => 'location',
								'value' => $post->ID,
								'compare' => 'LIKE'
							),
							array(
								'key' => 'location',								
								'compare' => 'NOT EXISTS'
							)
						)
					);

					$services = get_posts($args);

					if ( is_array( $services ) && count( $services ) > 0 ) {
						$response['data'] = $services;
						$response['status'] = 'success';						
					}

					
				}
			} else {

				$services = self::all_service();

				if ( is_array( $services ) && count( $services ) > 0 ) {
					$response['data'] = $services;
					$response['status'] = 'success';					
				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public static function all_service( $service_id = '' ) {

			if ( ! empty( $service_id ) ) {
				$get_service = get_post( $service_id );
				if ( $get_service && 'gfb-service' == $get_service->post_type ) {					
					return $get_service;
				}
			}

			$get_service = get_posts( array(
				'post_type' => 'gfb-service',
				'numberposts' => -1,
				'status' => 'publish',

			));			

			return $get_service;
		}

		public static function get_service_by_id( $service_id ) {
			$response = array();
			
			if ( ! empty( $service_id ) ) {
				$service = get_post( $service_id );
				if ( $service && 'gfb-service' == $service->post_type ) {
					$price = ! empty( get_post_meta($service->ID, 'price', true) ) ? get_post_meta($service->ID, 'price', true) : '0.00';
					$category = get_post_meta( $service->ID, 'category', true);
					$location = get_post_meta( $service->ID, 'location', true);					

					$response['name'] = $service->post_title;
					$response['price'] = $price;
					$response['category'] = $category;
					$response['location'] = $location;
				}
			}

			return $response;
		}

		public static function get_services_by_cat( $category_id = '' ) {

			$get_services = array();

			if ( ! empty( $category_id ) ) {				
				$get_services = get_posts( array(
					'post_type' => 'gfb-service',
					'numberposts' => -1,
					'status' => 'publish',
					'meta_key' => 'category',
					'meta_value' => $category_id
				));			
			}

			return $get_services;
		}

		public static function all_categories( $category_id = '' ) {

			if ( ! empty( $category_id ) ) {
				$cat_name = get_the_category_by_ID( $category_id );
				if ( ! is_wp_error($cat_name) ) {
					return get_the_category_by_ID( $category_id );
				} else {
					return false;
				}
			}

			$args = array(
               'taxonomy' => 'gfb-service-category',
               'orderby' => 'name',
               'order'   => 'ASC',
               'hide_empty' => false
            );

            return get_categories($args);
		}

		public function gfb_delete_service() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Service not deleted.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {
				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-service' == $post->post_type ) {
					wp_delete_post( $post->ID, true );
					$response['message'] = esc_html__( 'Service deleted successfully.', 'gfb' );
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_show_service_data() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(			
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-service' == $post->post_type ) {

					$price = ! empty( get_post_meta($post->ID, 'price', true) ) ? get_post_meta($post->ID, 'price', true) : '0.00';
					$category = get_post_meta( $post->ID, 'category', true);
					$location = get_post_meta( $post->ID, 'location', true);					

					$response['name'] = $post->post_title;
					$response['price'] = $price;
					$response['category'] = $category;
					$response['location'] = $location;
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_edit_service() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;			

			if ( ! empty( $data['post_id'] ) && ! empty( trim( $data['name'] ) ) && ! empty($data['category']) && 'no' != $data['category'] ) {

				$args = apply_filters( 'gfb_edit_service_args', array(
					'ID'         => trim($data['post_id']),
					'post_title' => trim( $data['name'] ),
					'post_type' => 'gfb-service',
					'post_status' => 'publish',
					'wp_error'    => true,
					'meta_input'  => array(
						'category' => trim( $data['category'] ),
						'price' => trim( $data['price'] ),
						//'location' => ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) ? $data['location'] : array(),
					)
				));					

				$gfb_service = wp_update_post( $args );

				if ( ! is_wp_error( $gfb_service ) ) {

					if ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) {
						update_post_meta( $gfb_service, 'location', $data['location'] );
					} else {
						delete_post_meta( $gfb_service, 'location' );
					}
					
					//adding category to service
					wp_set_post_terms( $gfb_service, trim( $data['category'] ), 'gfb-service-category');

					$response['message'] = esc_html__( 'Service updated successfully.', 'gfb' );
					$response['status'] = 'success';					

				} else {

					$response['message'] = $wp_user->get_error_message();
				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_service() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error'				
			);

			$data = $_POST;

			if ( ! empty( trim( $data['name'] ) ) && ! empty($data['category']) && 'no' != $data['category'] ) {

				$args = apply_filters( 'gfb_add_service_args', array(
					'post_title' => trim( $data['name'] ),
					'post_type' => 'gfb-service',
					'post_status' => 'publish',
					'wp_error'    => true,
					'meta_input'  => array(
						'category' => trim( $data['category'] ),
						'price' => trim( $data['price'] ),
						//'location' => ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) ? $data['location'] : array(),
					)
				));

				$gfb_service = wp_insert_post( $args );

				if ( ! is_wp_error( $gfb_service ) ) {

					if ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) {
						update_post_meta( $gfb_service, 'location', $data['location'] );
					}
					
					//adding category to service
					wp_set_post_terms( $gfb_service, trim( $data['category'] ), 'gfb-service-category');

					$response['message'] = esc_html__( 'Service created successfully.', 'gfb' );
					$response['status'] = 'success';

				} else {
										
					$response['message'] = $wp_user->get_error_message();

				}				

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_custom_column( $column, $postId ) {			

			$arr = RGCurrency::get_currency(GFCommon::get_currency());

			$price = ! empty( get_post_meta($postId, 'price', true) ) ? get_post_meta($postId, 'price', true) : '0.00';			
			
			switch ($column) {

				case 'id':
					echo '&nbsp;&nbsp;' . esc_attr($postId);
					break;

				case 'name':
					echo esc_attr(get_the_title($postId));
					break;

				case 'price':
					echo gfb_price_format($price);
					break;				
				
				case 'actions':
					echo '<a href="#" data-type="service" data-id="gfb-service-modal" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-edit"> <i class="fa fa-edit customer--icon"></i> </a>';					
					if ( 'publish' == get_post_status($postId) ) {
						echo '<a href="#" data-type="service" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-delete" title="Delete"> <i class="fa fa-trash customer--icon"></i>
						</a>';
					}
					break;
			}
		}

		public function gfb_modify_column_names( $columns ) {			

			unset($columns['date']);
			unset($columns['title']);
			unset($columns['taxonomy-gfb-service-category']);
			// unset($columns['cb']);
			
			$columns['id'] = __('&nbsp;&nbsp;Service ID', 'gfb');
			$columns['name'] = __('Name', 'gfb');
			$columns['price'] = __('Price', 'gfb');			
			$columns['taxonomy-gfb-service-category'] = __('Category', 'gfb');			
			$columns['actions'] = __('Actions', 'wc-donation');

			return $columns;
		}

		public function gfb_manage_posts_extra_tablenav( $which ) {
			if ( 'top' === $which ) {
				echo sprintf('<a href="#" data-id="gfb-service-modal" class="gfb-add-button"><i class="fa fa-plus-circle"></i>%1$s</a>', esc_html__('Add Service', 'gfb') );
			}			
		}

		public function highlight_menu() {			

			//adding customer modal view file
			require_once GFB_ADMIN_VIEW_PATH . 'modal/service-modal.php';			
			?>
			<script type="text/javascript">
				//debugger;					
				jQuery('#toplevel_page_gfb li, #toplevel_page_gfb li > a').removeClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-service"]').addClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-service"]').parent('li').addClass('current');
			</script>
			<?php			
		}

		public function menu_highlight( $parent_file ) {
			global $current_screen;

	        $taxonomy = $current_screen->taxonomy;
	        if ( 'gfb-service-category' === $taxonomy ) {
	            $parent_file = 'gfb';
	        }

	        return $parent_file;
		}

		public function add_service_category_menu() {
			add_submenu_page(
            	'gfb',
                __( 'Category - Gravity Booking', 'gfb' ),
                __( 'Service Categories', 'gfb' ),
                'manage_options',
                'edit-tags.php?taxonomy=gfb-service-category',
                '',
                2
            );        
		}

		public function register_service_taxonomy() {

			// Add new taxonomy, make it hierarchical (like categories)
		    $labels = array(
		        'name'              => _x( 'Category', 'taxonomy general name', 'gfb' ),
		        'singular_name'     => _x( 'Category', 'taxonomy singular name', 'gfb' ),
		        'search_items'      => __( 'Search Category', 'gfb' ),
		        'all_items'         => __( 'All Category', 'gfb' ),
		        'parent_item'       => __( 'Parent Category', 'gfb' ),
		        'parent_item_colon' => __( 'Parent Category:', 'gfb' ),
		        'edit_item'         => __( 'Edit Category', 'gfb' ),
		        'update_item'       => __( 'Update Category', 'gfb' ),
		        'add_new_item'      => __( 'Add New Category', 'gfb' ),
		        'new_item_name'     => __( 'New Category Name', 'gfb' ),
		        'menu_name'         => __( 'Category', 'gfb' ),
		    );
		 
		    $args = array(
		        'hierarchical'      => true,
		        'labels'            => $labels,
		        'show_ui'           => true,
		        'show_in_menu'      => true,
		        'show_admin_column' => true,
		        'query_var'         => true,
		        'rewrite'           => array( 'slug' => 'gfb-service-category' )		        
		    );

			register_taxonomy('gfb-service-category', array('gfb-service'), $args );
		}

		public function register_service_posttype() {
			$labels = array(
				'name'                  => _x( 'Service', 'Post type general name', 'gfb' ),
				'singular_name'         => _x( 'Service', 'Post type singular name', 'gfb' ),
				'menu_name'             => _x( 'Service', 'Admin Menu text', 'gfb' ),
				'name_admin_bar'        => _x( 'Service', 'Add New on Toolbar', 'gfb' ),
				'add_new'               => __( 'Add Service', 'gfb' ),
				'add_new_item'          => __( 'Add New Service', 'gfb' ),
				'new_item'              => __( 'New Service', 'gfb' ),
				'edit_item'             => __( 'Edit Service', 'gfb' ),
				'view_item'             => __( 'View Service', 'gfb' ),
				'all_items'             => __( 'Services', 'gfb' ),
				'search_items'          => __( 'Search Service', 'gfb' ),
				'parent_item_colon'     => __( 'Parent Service:', 'gfb' ),
				'not_found'             => __( 'No Service found.', 'gfb' ),
				'not_found_in_trash'    => __( 'No Service found in Trash.', 'gfb' ),
				'featured_image'        => _x( 'Service Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'gfb' ),
				'set_featured_image'    => _x( 'Set Service image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'remove_featured_image' => _x( 'Remove Service image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'use_featured_image'    => _x( 'Use as Service image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'archives'              => _x( 'Service archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'gfb' ),
				'insert_into_item'      => _x( 'Insert into Service', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'gfb' ),
				'uploaded_to_this_item' => _x( 'Uploaded to this Service', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'gfb' ),
				'filter_items_list'     => _x( 'Filter Service list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'gfb' ),
				'items_list_navigation' => _x( 'Service list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'gfb' ),
				'items_list'            => _x( 'Service list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'gfb' ),
			);

			$args = array(
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => 'gfb',
				'show_in_rest'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'gfb-service' ),
				'capability_type'    => 'post',
				// 'capabilities' => array(
				// 	'create_posts' => 'do_not_allow', // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				// ),
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'thumbnail' ),				
			);

			register_post_type( 'gfb-service', $args );
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Service();
			}

			return self::$instance;
		}
	}

	GFB_Service::get_instance();
}

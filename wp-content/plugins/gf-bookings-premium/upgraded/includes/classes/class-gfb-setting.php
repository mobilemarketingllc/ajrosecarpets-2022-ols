<?php
/**
 * GFB Setting Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Setting' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Setting {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $settings = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			self::$settings = get_option('gfb_settings', array());

			add_action('gfb_after_menu_register', array($this, 'register_menu'));

			add_action('wp_ajax_gfb_general_settings', array($this, 'gfb_general_settings') );
		}

		public static function get_settings() {			

			$gfb_settings = self::$settings;

			$data = array();
			
		    $data['gfb_price_policy'] = isset($gfb_settings['general']['gfb_price_policy']) ? $gfb_settings['general']['gfb_price_policy'] : 'admin';

		    $data['gfb_appointment_policy'] = isset($gfb_settings['general']['gfb_appointment_policy']) ? $gfb_settings['general']['gfb_appointment_policy'] : 'admin';

		    $data['gbf_start_of_week'] = isset($gfb_settings['general']['gbf_start_of_week']) ? $gfb_settings['general']['gbf_start_of_week'] : 'monday';

		    $data['gbf_time_duration'] = isset($gfb_settings['general']['gbf_time_duration']) ? $gfb_settings['general']['gbf_time_duration'] : '30';

		    $data['gbf_time_interval'] = isset($gfb_settings['general']['gbf_time_interval']) ? $gfb_settings['general']['gbf_time_interval'] : '15';

		    $data['gfb_show_timeslot'] = isset($gfb_settings['general']['gbf_show_timeslot']) ? $gfb_settings['general']['gbf_show_timeslot'] : 'right';

		    $data['gfb_calendar_timeslot_format'] = isset($gfb_settings['general']['gfb_calendar_timeslot_format']) ? $gfb_settings['general']['gfb_calendar_timeslot_format'] : '24';

		    $data['gfb_prior_months_book_appointment'] = isset($gfb_settings['general']['gfb_prior_months_book_appointment']) ? $gfb_settings['general']['gfb_prior_months_book_appointment'] : '3';

		    $data['gfb_cal_gradient_color_top'] = isset($gfb_settings['display']['gfb_cal_gradient_color_top']) ? $gfb_settings['display']['gfb_cal_gradient_color_top'] : '#f1f3f6';

		    $data['gfb_cal_gradient_color_bottom'] = isset($gfb_settings['display']['gfb_cal_gradient_color_bottom']) ? $gfb_settings['display']['gfb_cal_gradient_color_bottom'] : '#f1f3f6';

		    $data['gfb_cal_border_color'] = isset($gfb_settings['display']['gfb_cal_border_color']) ? $gfb_settings['display']['gfb_cal_border_color'] : '#26547c21';

			$data['gfb_cal_header_bg_color'] = isset($gfb_settings['display']['gfb_cal_header_bg_color']) ? $gfb_settings['display']['gfb_cal_header_bg_color'] : '#7C9299';

			$data['gfb_cal_header_text_color'] = isset($gfb_settings['display']['gfb_cal_header_text_color']) ? $gfb_settings['display']['gfb_cal_header_text_color'] : '#FFFFFF';


		    $data['gfb_cal_text_color'] = isset($gfb_settings['display']['gfb_cal_text_color']) ? $gfb_settings['display']['gfb_cal_text_color'] : '#6D8298';

		    $data['gfb_cal_selected_text_color'] = isset($gfb_settings['display']['gfb_cal_selected_text_color']) ? $gfb_settings['display']['gfb_cal_selected_text_color'] : '#FFFFFF';

		    $data['gfb_cal_selected_text_bg_color'] = isset($gfb_settings['display']['gfb_cal_selected_text_bg_color']) ? $gfb_settings['display']['gfb_cal_selected_text_bg_color'] : '#00D0AC';

		    $data['gfb_cal_available_days_text_color'] = isset($gfb_settings['display']['gfb_cal_available_days_text_color']) ? $gfb_settings['display']['gfb_cal_available_days_text_color'] : '#6D8298';

		    $data['gfb_cal_available_days_bg_color'] = isset($gfb_settings['display']['gfb_cal_available_days_bg_color']) ? $gfb_settings['display']['gfb_cal_available_days_bg_color'] : '#FFFFFF';

		    $data['gfb_cal_holiday_days_text_color'] = isset($gfb_settings['display']['gfb_cal_holiday_days_text_color']) ? $gfb_settings['display']['gfb_cal_holiday_days_text_color'] : '#6D8298';

			$data['gfb_cal_holiday_days_bg_color'] = isset($gfb_settings['display']['gfb_cal_holiday_days_bg_color']) ? $gfb_settings['display']['gfb_cal_holiday_days_bg_color'] : '';

		    $data['gfb_label_location'] = isset($gfb_settings['label']['gfb_label_location']) ? $gfb_settings['label']['gfb_label_location'] : 'Location';

		    $data['gfb_label_category'] = isset($gfb_settings['label']['gfb_label_category']) ? $gfb_settings['label']['gfb_label_category'] : 'Service Category';

		    $data['gfb_label_service'] = isset($gfb_settings['label']['gfb_label_service']) ? $gfb_settings['label']['gfb_label_service'] : 'Service';

		    $data['gfb_label_staff'] = isset($gfb_settings['label']['gfb_label_staff']) ? $gfb_settings['label']['gfb_label_staff'] : 'Staff';

		    $data['gfb_company_name'] = isset($gfb_settings['company']['gfb_company_name']) ? $gfb_settings['company']['gfb_company_name'] : get_bloginfo('name');
		    
		    $data['gfb_company_address'] = isset($gfb_settings['company']['gfb_company_address']) ? $gfb_settings['company']['gfb_company_address'] : '';

			$data['gfb_company_number'] = isset($gfb_settings['company']['gfb_company_number']) ? $gfb_settings['company']['gfb_company_number'] : '';

			$data['gfb_company_email'] = isset($gfb_settings['company']['gfb_company_email']) ? $gfb_settings['company']['gfb_company_email'] : '';

		    $data['gfb_company_website_url'] = isset($gfb_settings['company']['gfb_company_website_url']) ? $gfb_settings['company']['gfb_company_website_url'] : get_bloginfo('url');

		    $data['gfb_sender_name'] = isset($gfb_settings['email']['gfb_sender_name']) ? $gfb_settings['email']['gfb_sender_name'] : get_bloginfo('name');

		    $data['gfb_sender_email'] = isset($gfb_settings['email']['gfb_sender_email']) ? $gfb_settings['email']['gfb_sender_email'] : get_bloginfo('admin_email');

		    $data['gfb_gcal_switch'] = isset($gfb_settings['gcal']['gfb_gcal_switch']) ? $gfb_settings['gcal']['gfb_gcal_switch'] : 'disabled';

		    $data['gfb_gcal_client_id'] = isset($gfb_settings['gcal']['gfb_gcal_client_id']) ? $gfb_settings['gcal']['gfb_gcal_client_id'] : '';

		    $data['gfb_gcal_secret_key'] = isset($gfb_settings['gcal']['gfb_gcal_secret_key']) ? $gfb_settings['gcal']['gfb_gcal_secret_key'] : '';

		    return $data;
		}

		public function gfb_general_settings() {

			check_ajax_referer( 'gfb', 'nonce' );

			$gfb_settings = get_option('gfb_settings', array());

			$response = array(
				'message' => esc_html__( 'Setting not saved.', 'gfb' ),
				'status'  => 'error'
			);			

			$data = $_POST;

			if ( isset($data['type']) && ! empty($data['type']) ) {

				$type = $data['type'];

				foreach( $data as $k => $val ) {
					if ( 'nonce' != $k && 'action' != $k && 'type' != $k ) {
						$gfb_settings[$type][$k] = $val;
					}
				}

				update_option('gfb_settings', $gfb_settings );				
				$response['message'] = __('Settings saved successfully', 'gfb');
				$response['status'] = 'success';
				$response['settings'] = get_option('gfb_settings');
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function register_menu() {

			do_action('gfb_before_settings_menu');

			add_submenu_page(
            	'gfb',
                __( 'Settings - Gravity Booking', 'gfb' ),
                __( 'Settings', 'gfb' ),
                'manage_options',
                'gfb-setting',
                array( get_class($this), 'gfb_setting_page' ),
                30
            );
		}

		public static function gfb_setting_page() {
			require_once GFB_ADMIN_VIEW_PATH . 'setting.php';
		}

		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Setting();
			}

			return self::$instance;
		}
	}

	GFB_Setting::get_instance();
}

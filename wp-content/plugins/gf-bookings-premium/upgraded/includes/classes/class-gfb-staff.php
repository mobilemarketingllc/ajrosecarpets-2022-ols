<?php
/**
 * GFB Staff Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Staff' ) ) {
	/**
	 * Checks if WooCommerce is enabled.
	 */
	class GFB_Staff {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		private static $checkPostType = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			add_action('init', array($this, 'gfb_staff_role_management'), 10);

			self::$checkPostType = isset( $_REQUEST['post_type'] ) && ! empty( sanitize_text_field($_REQUEST['post_type']) ) ? sanitize_text_field($_REQUEST['post_type']) : '';

			add_action( 'init', array( $this, 'register_staff_posttype'), 20 );
			add_filter( 'manage_gfb-staff_posts_columns', array( $this, 'gfb_modify_column_names') );
			add_action( 'manage_gfb-staff_posts_custom_column', array( $this, 'gfb_add_custom_column'), 9, 2 );

			add_action( 'wp_ajax_gfb_add_staff', array( $this, 'gfb_add_staff' ) );
			add_action( 'wp_ajax_gfb_edit_staff', array( $this, 'gfb_edit_staff' ) );
			add_action( 'wp_ajax_gfb_delete_staff', array( $this, 'gfb_delete_staff' ) );
			add_action( 'wp_ajax_gfb_show_staff_data', array( $this, 'gfb_show_staff_data' ) );
			add_action( 'wp_ajax_gfb_add_staff_services', array( $this, 'gfb_add_staff_services' ) );
			add_action( 'wp_ajax_gfb_add_staff_timings', array( $this, 'gfb_add_staff_timings' ) );
			add_action( 'wp_ajax_gfb_add_staff_holidays', array( $this, 'gfb_add_staff_holidays' ) );
			add_action( 'wp_ajax_gfb_remove_staff_holidays', array( $this, 'gfb_remove_staff_holidays' ) );

			add_action( 'deleted_user', array( $this, 'delete_gfb_staff_if_user_deleted'), 99, 1 );

			if ( 'gfb-staff' === self::$checkPostType ) {				

				add_action( 'admin_footer', array( $this, 'highlight_menu' ) );

				add_filter('post_row_actions', '__return_empty_array');

				add_action('manage_posts_extra_tablenav', array($this, 'gfb_manage_posts_extra_tablenav'));

				add_filter('months_dropdown_results', '__return_empty_array');

				add_filter( 'bulk_actions-edit-gfb-staff', '__return_empty_array' );				

				add_action( 'views_edit-gfb-staff',  function ( $views ) {

					unset($views['all']);
					unset($views['publish']);
    				unset($views['trash']);
    				unset($views['draft']);

					return $views;
				});

				// the function that filters posts
				add_action( 'pre_get_posts', array( $this, 'gfb_filter_query' ) );		
			}

			add_action( 'wp_ajax_gfb_get_staff_by_service_and_location', array( $this, 'gfb_get_staff_by_service_and_location' ) );
			add_action( 'wp_ajax_nopriv_gfb_get_staff_by_service_and_location', array( $this, 'gfb_get_staff_by_service_and_location' ) );

			add_action('wp_dashboard_setup', array($this, 'gfb_add_meta_box_admin_screen_gcal') );			
			
			add_action('admin_menu', array($this, 'remove_profile_menu'));
		}

		//Remove Profile Menu from Dashboard
		public function remove_profile_menu() {
			if ( is_user_logged_in() ) {
				if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
					remove_menu_page('profile.php');
				}
			}
		}		

		public function gfb_add_meta_box_admin_screen_gcal() {

			$user_id = get_current_user_id();
			$user = get_user_by('id', $user_id);
			if ( in_array('gfb-staff', $user->roles ) ) {
				$gfb_settings = GFB_Setting::get_settings();

				if ( isset($gfb_settings['gfb_gcal_switch']) && 'enabled' === $gfb_settings['gfb_gcal_switch'] ) {
					$client_id = isset($gfb_settings['gfb_gcal_client_id']) && ! empty(trim($gfb_settings['gfb_gcal_client_id'])) ? $gfb_settings['gfb_gcal_client_id'] : '';
				} else {
					$client_id = '';
				}

				if ( ! empty( $client_id ) ) {
					add_meta_box('gfb-staff-gcal', 'Google Calendar Integration', array($this, 'gfb_staff_gcal_markup'), 'dashboard', 'normal', 'high', $gfb_settings);
				}
			}
		}

		public function gfb_staff_gcal_markup( $meta_id, $gfb_settings ) {

			?>
			<style>
				#gfb-gcal-connect {
				    display: inline-block;
				    color: #57B0EE;
				    line-height: 1;
				    font-size: 15px;
				    padding: 12px 24px;
				    text-align: center;
				    transition: all .3s;
				    background-color: #57B0EE;
				    border-style: solid;
				    border-width: 1px;
				    border-color: #57B0EE;
				    border-radius: 10px;
				    box-shadow: 0px 6px 18px 0px rgb(0 0 0 / 6%);				    
				    text-decoration: none;
				    color: #fff;
				    font-weight: 500;
				    padding-bottom: 13px;
				}
			</style>
			<?php

			require_once GFB_PATH . '/includes/classes/class-gfb-gcal-api.php';

			$capi = new GFB_CAL_API();

			$args = $gfb_settings['args'];	
			
			$redirect_url = admin_url();

			$gfb_user = get_user_meta( get_current_user_id(), 'gfb_user', true );

			// Google passes a parameter 'code' in the Redirect Url
			if ( isset($_GET['code']) ) {
				try {
					// Get the access token 
					$data = $capi->GetAccessToken($args['gfb_gcal_client_id'], $redirect_url, $args['gfb_gcal_secret_key'], $_GET['code']);
					
					// Save the access token as a session variable
					if ( isset($data['access_token']) ) {

						update_post_meta( $gfb_user, 'gcal', $data );
					}

					// Redirect to the page where user can create event
					
				} catch(Exception $e) {
					echo $e->getMessage();
					exit();
				}
			}			

			$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($redirect_url) . '&response_type=code&client_id=' . $args['gfb_gcal_client_id'] . '&access_type=offline&state=' . $gfb_user . '&prompt=consent';

			$gcal_data = get_post_meta($gfb_user, 'gcal', true);

			if ( isset($gcal_data['access_token']) ) {
				echo '<a id="gfb-gcal-connect" href="' . esc_url(admin_url('/?staff_id=' . $gfb_user . '&action=revoke')) . '">' . esc_html__('Disconnect Google Calendar', 'gfb') . '</a>'; 
			} else {
				echo '<a id="gfb-gcal-connect" href="' . esc_url($login_url) . '">' . esc_html__('Connect With Google Calendar', 'gfb') . '</a>';
			}

			//disconnect logic
			if ( isset( $_GET['action'] ) && 'revoke' === $_GET['action'] ) {
				$http_code = $capi->revokeToken($gcal_data['refresh_token']);
				if ( 200 == $http_code ) {
					delete_post_meta( $gfb_user, 'gcal' );
					wp_safe_redirect(admin_url());
					exit();
				}
			}
		}

		public function gfb_staff_role_management() {			
			
			// remove_role('gfb-staff');

			// Create GFB Staff role.
			add_role(
				'gfb-staff', //  System name of the role.
				__( 'GFB Staff', 'gfb'  ), // Display name of the role.
			);

			$staff_role = get_role('gfb-staff');

			if ( $staff_role ) {
				
				$staff_role->add_cap('read');

				$staff_role->add_cap('edit_gfb-staff');
				$staff_role->add_cap('edit_gfb-staffs');
				$staff_role->add_cap('edit_others_gfb-staff');
				$staff_role->add_cap('edit_others_gfb-staffs');

				$staff_role->add_cap('edit_gfb-appointment');
				$staff_role->add_cap('edit_gfb-appointments');
				$staff_role->add_cap('edit_others_gfb-appointment');
				$staff_role->add_cap('edit_others_gfb-appointments');
			}

			$admin_role = get_role('administrator');

			if ( $admin_role ) {				
				
				$admin_role->add_cap('edit_gfb-staff');
				$admin_role->add_cap('edit_gfb-staffs');
				$admin_role->add_cap('edit_others_gfb-staff');
				$admin_role->add_cap('edit_others_gfb-staffs');

				$admin_role->add_cap('edit_gfb-appointment');
				$admin_role->add_cap('edit_gfb-appointments');
				$admin_role->add_cap('edit_others_gfb-appointment');
				$admin_role->add_cap('edit_others_gfb-appointments');
			}

		}

		public function gfb_get_staff_by_service_and_location() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(				
				'status'  => 'error',
				'selected_staff_id' => ''			
			);

			$data = $_POST;

			$form  = \GFAPI::get_form($data['form_id']);

			if ( is_array( $form['fields'] ) && count($form['fields']) > 0 ) {
				foreach( $form['fields'] as $field ) {
					if ( 'gfb_appointment_calendar' == $field->type ) {
						$response['selected_staff_id'] = $field->gfb_staff;
						break;
					}
				}					
			}

			if ( isset($data['post_id']) && ! empty( $data['post_id'] ) && 'all' != $data['post_id'] && 'undefined' != $data['post_id'] ) {				
				
				$post = get_post( $data['post_id'] );				
				
				if ( $post && 'gfb-service' == $post->post_type ) {

					if ( isset($data['location_id']) && 'all' != $data['location_id'] && ! empty($data['location_id']) && 'undefined' != $data['location_id'] && 'null' != $data['location_id'] ) {						
						$check_location = array(
							'relation' => 'OR',
							array(
								'relation' => 'AND',
								array(
									'key' => 'location',
									'value' => $data['location_id'],
									'compare' => 'LIKE'
								),
								array(
									'key' => 'services',
									'value' => $post->ID,
									'compare' => 'LIKE'
								),
							),
							array(
								'relation' => 'AND',
								array(
									'key' => 'location',
									'value' => '',
									'compare' => 'NOT EXISTS'
								),
								array(
									'key' => 'services',
									'value' => $post->ID,
									'compare' => 'LIKE'
								),
							)
						);
					} else {
						$check_location = array(
							'relation' => 'OR',
							array(
								'key' => 'services',
								'value' => $post->ID,
								'compare' => 'LIKE'
							)
						);
					}
					
					$args = array(
						'post_type' => 'gfb-staff',
						'posts_per_page' => -1,
						'post_status' => 'publish',						
						'meta_query' => $check_location
					);

					$query = new WP_Query( $args );

					if ( isset( $query->posts ) && is_array( $query->posts ) && count( $query->posts ) > 0 ) {
						$response['data'] = $query->posts;
						$response['status'] = 'success';						
					}

					
				}
			} else {

				$staffs = self::all_staff();

				if ( is_array( $staffs ) && count( $staffs ) > 0 ) {
					$response['data'] = $staffs;
					$response['status'] = 'success';
				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public static function get_booking_type( $staff_id = '' ) {

			$staffData = self::get_staff_by_id( $staff_id );
    		if ( isset( $staffData['booking_type'] ) ) {
    			return $staffData['booking_type'];
    		}	
		}

		public static function all_staff( $staff_id = '' ) {

			if ( ! empty( $staff_id ) ) {
				$get_staff = get_post( $staff_id );
				if ( $get_staff && 'gfb-staff' == $get_staff->post_type ) {					
					return $get_staff;
				}
			}

			$get_staff = get_posts( array(
				'post_type' => 'gfb-staff',
				'numberposts' => -1,
				'status' => 'publish',
			));			

			return $get_staff;
		}

		public static function get_staff_by_id( $staff_id ) {

			$response = array();

			if ( ! empty( $staff_id ) ) {
				$staff = get_post( $staff_id );

				if ( $staff && 'gfb-staff' == $staff->post_type ) {					
					$user_id = get_post_meta( $staff_id, 'wp_user', true );				
					$userdata = get_userdata( $user_id );
					if ( $userdata ) {
						$phone = get_post_meta( $staff_id, 'phone', true);
						$profession = get_post_meta( $staff_id, 'profession', true);
						$location = get_post_meta( $staff_id, 'location', true);
						$info = get_post_meta( $staff_id, 'info', true);
						$services = get_post_meta( $staff_id, 'services', true);
						$timings = get_post_meta( $staff_id, 'timings', true);
						$timezone = get_post_meta( $staff_id, 'timezone', true);
						$booking_type = ! empty(get_post_meta( $staff_id, 'booking_type', true)) ? get_post_meta( $staff_id, 'booking_type', true) : 'custom';
						$time_duration = get_post_meta( $staff_id, 'time_duration', true);
						$time_interval = get_post_meta( $staff_id, 'time_interval', true);
						$time_slots = array();
						$holidays = ! empty(get_post_meta( $staff_id, 'gfb_staff_holidays', true)) ? get_post_meta( $staff_id, 'gfb_staff_holidays', true) : array();

						if ( is_array($timings) && count($timings) > 0 ) {							
							foreach( $timings as $day => $timing ) {								
								if ( isset($timing['start']) && isset($timing['end']) ) {
									$time_slots[$day] = gfb_split_time($timing['start'], $timing['end'], $time_duration, $time_interval, $timing['capacity']);
								}						
							}
						}

						$response['user_id'] = $user_id;
						$response['fname'] = $userdata->first_name;
						$response['lname'] = $userdata->last_name;
						$response['email'] = $userdata->user_email;						
						$response['phone'] = $phone;
						$response['profession'] = $profession;
						$response['location'] = $location;
						$response['info'] = $info;
						$response['services'] = $services;
						$response['timings'] = $timings;
						$response['timeslots'] = $time_slots;
						$response['timezone'] = $timezone;
						$response['booking_type'] = $booking_type;
						$response['duration'] = $time_duration;
						$response['interval'] = $time_interval;
						$response['holidays'] = $holidays;						
					}
				}				
			}

			return $response;
		}

		public function gfb_show_staff_data() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(			
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post = get_post( $data['post_id'] );				
				if ( $post && 'gfb-staff' == $post->post_type ) {

					$user_id = get_post_meta( $data['post_id'], 'wp_user', true );				
					$userdata = get_userdata( $user_id );
					if ( $userdata ) {

						$phone = get_post_meta( $data['post_id'], 'phone', true);
						$profession = get_post_meta( $data['post_id'], 'profession', true);
						$location = get_post_meta( $data['post_id'], 'location', true);
						$info = get_post_meta( $data['post_id'], 'info', true);
						$services = get_post_meta( $data['post_id'], 'services', true);
						$timings = get_post_meta( $data['post_id'], 'timings', true);
						$timezone = get_post_meta( $data['post_id'], 'timezone', true);
						$booking_type = ! empty(get_post_meta( $data['post_id'], 'booking_type', true)) ? get_post_meta( $data['post_id'], 'booking_type', true) : 'custom';
						$time_duration = get_post_meta( $data['post_id'], 'time_duration', true);
						$time_interval = get_post_meta( $data['post_id'], 'time_interval', true);
						$time_slots = array();
						$holidays = ! empty(get_post_meta( $data['post_id'], 'gfb_staff_holidays', true)) ? get_post_meta( $data['post_id'], 'gfb_staff_holidays', true) : array();
						
						if ( is_array($timings) && count($timings) > 0 ) {
							foreach( $timings as $day => $timing ) {
								//print_r( $timing );
								if ( isset($timing['start']) && isset($timing['end']) ) {
									$time_slots[$day] = gfb_split_time($timing['start'], $timing['end'], $time_duration, $time_interval, $timing['capacity']);
								}
							}
						}

						$response['fname'] = $userdata->first_name;
						$response['lname'] = $userdata->last_name;
						$response['email'] = $userdata->user_email;
						//$response['password'] = $userdata->login_pass;
						$response['phone'] = $phone;
						$response['profession'] = $profession;
						$response['location'] = $location;
						$response['info'] = $info;
						$response['services'] = $services;
						$response['timings'] = $timings;
						$response['time_slots'] = $time_slots;
						$response['timezone'] = $timezone;
						$response['booking_type'] = $booking_type;
						$response['duration'] = $time_duration;
						$response['interval'] = $time_interval;
						$response['holidays'] = $holidays;
						$response['status'] = 'success';
					}
				}
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function delete_gfb_staff_if_user_deleted( $user_id ) {			

			$get_staff = get_posts( array(
				'post_type' => 'gfb-staff',
				'numberposts' => -1,
				'meta_query' => array(
					array(
						'key'   => 'wp_user',
						'value' => $user_id
					)
				)
			));			
			
			if ( isset($get_staff[0]) && $get_staff[0] ) {
				wp_delete_post( $get_staff[0]->ID, true );
			}			
		}

		public function gfb_delete_staff() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Staff not deleted.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {
				$user_id = get_post_meta( $data['post_id'], 'wp_user', true );
				$userdata = get_userdata( $user_id );
				if ( $userdata ) {
					wp_delete_user( $userdata->ID );
					$response['message'] = esc_html__( 'Staff deleted successfully.', 'gfb' );
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();

		}

		public function gfb_edit_staff() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error'
			);

			$data = $_POST;

			// echo '<pre>';
			// print_r($data);
			// echo '</pre>';

			if ( ! empty( $data['post_id'] ) && ! empty( trim( $data['fname'] ) ) && ! empty( trim( $data['lname'] ) ) && ! empty( trim( $data['email'] ) ) ) {

				$user_id = get_post_meta( $data['post_id'], 'wp_user', true );

				if ( ! $user_id ) {
					echo wp_json_encode( $response );
					wp_die();
				}

				$user_args = array(
					'ID'           => trim( $user_id ),					
					'user_login'   => trim( $data['email'] ),
					'user_email'   => trim( $data['email'] ),
					'display_name' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
					'first_name'   => trim( $data['fname'] ),
					'last_name'    => trim( $data['lname'] ),
					'role' 		   => 'gfb-staff',
				);

				if ( ! empty( $data['password'] ) ) {
					$user_args['user_pass'] = trim( $data['password'] );
				}

				$wp_user = wp_update_user($user_args);

				if ( ! is_wp_error( $wp_user ) ) {

					$gfb_user = get_user_meta( $wp_user, 'gfb_user', true );

					$args = apply_filters( 'gfb_edit_staff_args', array(
						'ID'         => $gfb_user,
						'post_title' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
						'post_type' => 'gfb-staff',
						'post_status' => 'publish',
						'wp_error'    => true,
						'meta_input'  => array(
							'wp_user' => $user_id,
							'info' => trim( $data['info'] ),
							'profession' => trim( $data['profession'] ),
							'phone' => trim( $data['phone'] ),
							//'location' => ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) ? $data['location'] : array(),
						)
					));

					$gfb_user = wp_update_post( $args );

					if ( ! is_wp_error( $gfb_user ) ) {

						if ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) {
							update_post_meta( $gfb_user, 'location', $data['location'] );
						} else {
							delete_post_meta( $gfb_user, 'location');
							// update_post_meta( $gfb_user, 'location', array() );
						}

						update_user_meta( $wp_user, 'gfb_user', $gfb_user );
						$response['message'] = esc_html__( 'Staff updated successfully.', 'gfb' );
						$response['status'] = 'success';

						/* EMAIL NOTIFICATION STARTS */
						if ( true === apply_filters('gfb_add_staff_personal_details_notification', true) ) {
							$body = __('Your account details has been updated, below are the credentials to login', 'gfb');
							$data['password'] = ! empty($data['password']) ? $data['password'] : __('password not changed', 'gfb');
							$response['notification'] = GFB_Email::new_staff_notification( $wp_user, $body, $data['password'] );
						}
						/* EMAIL NOTIFICATION ENDS */

					} else {

						$response['message'] = $wp_user->get_error_message();
					}

				} else {

					$response['message'] = $wp_user->get_error_message();

				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_staff() {			

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Please fill all required fields.', 'gfb' ),
				'status'  => 'error',
				'notification' => false
			);

			$data = $_POST;

			if ( ! empty( trim( $data['fname'] ) ) && ! empty( trim( $data['lname'] ) ) && ! empty( trim( $data['email'] ) ) ) {

				$user = get_user_by( 'email', trim( $data['email'] ) );

				$role = ( array ) $user->roles;
				if (in_array('administrator', $role)) {
			        $response['message'] = esc_html__( 'This email address is already assigned as administrator.', 'gfb' );
			        echo wp_json_encode( $response );
					wp_die();
			    }

				$user_pass = ! empty( $data['password'] ) ? trim($data['password']) : gfb_randon_pass();

				if ( ! empty( $user ) ) {
					$wp_user = wp_update_user(
						array(
							'ID'           => $user->ID,				
							'user_pass'	   => $user_pass,
							'user_login'   => trim( $data['email'] ),
							'user_email'   => trim( $data['email'] ),
							'display_name' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
							'first_name'   => trim( $data['fname'] ),
							'last_name'    => trim( $data['lname'] ),
							'role' 		   => 'gfb-staff',
						)
					);
				} else {
					$wp_user = wp_insert_user(
						array(						
							'user_pass'	   => $user_pass,
							'user_login'   => trim( $data['email'] ),
							'user_email'   => trim( $data['email'] ),
							'display_name' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
							'first_name'   => trim( $data['fname'] ),
							'last_name'    => trim( $data['lname'] ),
							'role' 		   => 'gfb-staff',
						)
					);
				}				

				if ( ! is_wp_error( $wp_user ) ) {					

					$args = apply_filters( 'gfb_add_staff_args', array(
						'post_title' => trim( $data['fname'] ) . ' ' . trim( $data['lname'] ),
						'post_type' => 'gfb-staff',
						'post_status' => 'publish',
						'wp_error'    => true,
						'meta_input'  => array(
							'wp_user' => $wp_user,
							'info' => trim( $data['info'] ),
							'profession' => trim( $data['profession'] ),
							'phone' => trim( $data['phone'] ),
							//'location' => ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) ? $data['location'] : array(),
						)
					));

					$gfb_user = wp_insert_post( $args );

					if ( ! is_wp_error( $gfb_user ) ) {

						if ( isset( $data['location'] ) && is_array( $data['location'] ) && count( $data['location'] ) > 0 ) {
							update_post_meta( $gfb_user, 'location', $data['location'] );
						} 
						// else {
						// 	update_post_meta( $gfb_user, 'location', array() );
						// }

						update_user_meta( $wp_user, 'gfb_user', $gfb_user );
						$response['message'] = esc_html__( 'Staff created successfully.', 'gfb' );
						$response['status'] = 'success';

						/* EMAIL NOTIFICATION STARTS */
						if ( true === apply_filters('gfb_add_staff_personal_details_notification', true) ) {
							if ( isset( $data['profession'] ) && ! empty(trim( $data['profession'] )) ) {
								$profession = trim( $data['profession'] );
							} else {
								$profession = __('staff', 'gfb');
							}
							// Translators: %1$s to their strings respectively.
							$body = sprintf(__('Your are appointed as a %1$s, below are the credentials to login:', 'gfb'), $profession);
							$data['password'] = ! empty($data['password']) ? $data['password'] : __('password not changed', 'gfb');
							$response['notification'] = GFB_Email::new_staff_notification( $wp_user, $body, $data['password'] );
						}
						/* EMAIL NOTIFICATION ENDS */

					} else {
						
						wp_delete_user( $wp_user );
						$response['message'] = $wp_user->get_error_message();

					}

				} else {

					$response['message'] = $wp_user->get_error_message();

				}

			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_staff_services() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Staff services not saved.', 'gfb' ),
				'status'  => 'error',
				'notification' => false
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post_id = $data['post_id'];

				foreach( $data['gfb_staff_service'] as $key => $val ) {

					if ( !isset($val['available']) ) {
						unset($data['gfb_staff_service'][$key]);
					} /*else {
						if ( ! isset($val['price']) || (isset($val['price']) && '' == $val['price']) ) {
							$service_price = get_post_meta($key, 'price', true);
							$data['gfb_staff_service'][$key]['price'] = $service_price;
						}
					}*/
				}

				update_post_meta( $post_id, 'services', $data['gfb_staff_service'] );
				$response['message'] = esc_html__('Staff services saved successfully', 'gfb');
				$response['status'] = 'success';				
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_staff_timings() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Staff timings not saved.', 'gfb' ),
				'status'  => 'error',
				'notification' => false
			);

			$data = $_POST;

			$gfb_settings = GFB_Setting::get_settings();

			if ( ! empty( $data['post_id'] ) ) {

				$post_id = $data['post_id'];				

				foreach( $data['gfb_staff_timing'] as $key => $val ) {

					$switch = true;

					if ( !isset($val['switch']) ) {
						$switch = false;						
					}

					if ( !$switch ) {
						unset($data['gfb_staff_timing'][$key]);
					} else {

						if ( empty($val['start']) || empty($val['end']) ) {
							$data['gfb_staff_timing'][$key]['start'] = '00:00';
							$data['gfb_staff_timing'][$key]['end'] = '23:59';
						}

						if ( isset($val['capacity']) && '' == $val['capacity'] || empty($val['capacity']) ) {
							$data['gfb_staff_timing'][$key]['capacity'] = 1;
						}
					}					
				}

				update_post_meta( $post_id, 'timings', $data['gfb_staff_timing'] );
				update_post_meta( $post_id, 'timezone', $data['gfb-timezone'] );
				if ( isset($data['booking_type']) ) {
					update_post_meta( $post_id, 'booking_type', $data['booking_type'] );	
				} else {
					update_post_meta( $post_id, 'booking_type', 'custom' );
				}
				
				
				if ( isset( $data['gfb-time-slot-duration'] ) && ! empty( $data['gfb-time-slot-duration'] ) ) {
					update_post_meta( $post_id, 'time_duration', $data['gfb-time-slot-duration'] );	
				} else {
					update_post_meta( $post_id, 'time_duration', $gfb_settings['gbf_time_duration'] );	
				}
				
				if ( isset( $data['gfb-time-slot-interval'] ) && ! empty( $data['gfb-time-slot-interval'] ) ) {
					update_post_meta( $post_id, 'time_interval', $data['gfb-time-slot-interval'] );
				} else {
					update_post_meta( $post_id, 'time_interval', $gfb_settings['gbf_time_interval'] );
				}				

				$response['message'] = esc_html__('Staff timings saved successfully', 'gfb');
				$response['status'] = 'success';				
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_staff_holidays() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Staff holidays not saved.', 'gfb' ),
				'status'  => 'error',
				'notification' => false
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post_id = $data['post_id'];

				$holidays = get_post_meta($post_id, 'gfb_staff_holidays', true);
				$holidays = !is_array($holidays) ? array() : $holidays;

				if ( ! empty($data['gfb_staff_holidays']) ) {
					$dates = explode('_', $data['gfb_staff_holidays']);
					$dateFrom = isset($dates[0]) ? trim($dates[0]) : '';
					$dateTo = isset($dates[1]) ? trim($dates[1]) : '';					
					$new_holidays = array_merge($holidays, gfb_dates_in_range($dateFrom, $dateTo) );	

					// print_r( $dates );
					// print_r( $holidays );
					update_post_meta($post_id, 'gfb_staff_holidays', $new_holidays);
					$response['message'] = esc_html__('Staff holidays saved successfully', 'gfb');
					$response['holidays'] = $new_holidays;
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_remove_staff_holidays() {

			check_ajax_referer( 'gfb', 'nonce' );

			$response = array(
				'message' => esc_html__( 'Staff Holiday removed unsuccessful.', 'gfb' ),
				'status'  => 'error',
				'notification' => false
			);

			$data = $_POST;

			if ( ! empty( $data['post_id'] ) ) {

				$post_id = $data['post_id'];

				$holidays = get_post_meta($post_id, 'gfb_staff_holidays', true);
				$holidays = !is_array($holidays) ? array() : $holidays;

				if ( ! empty($data['gfb_staff_holidays']) ) {
					$dates = explode('_', $data['gfb_staff_holidays']);
					$dateFrom = isset($dates[0]) ? trim($dates[0]) : '';
					$dateTo = isset($dates[1]) ? trim($dates[1]) : '';					
					$removing_holidays = gfb_dates_in_range($dateFrom, $dateTo);

					foreach ( $removing_holidays as $remove_holiday ) {
						$key = array_search( $remove_holiday, $holidays );
						if ( false !== $key ) {
							unset($holidays[$key]);
						}
					}

					$holidays = array_values($holidays);
					
					update_post_meta($post_id, 'gfb_staff_holidays', $holidays);
					$response['message'] = esc_html__('Staff holiday removed successfully.', 'gfb');
					$response['holidays'] = $holidays;
					$response['status'] = 'success';
				}
			}

			echo wp_json_encode( $response );
			wp_die();
		}

		public function gfb_add_custom_column( $column, $postId ) {

			$user_id = get_post_meta( $postId, 'wp_user', true );
			$userdata = get_userdata( $user_id );

			if ( ! $userdata ) {
				return $column;
			}
			
			switch ($column) {

				case 'id':
					echo '&nbsp;&nbsp;' . esc_attr($postId);
					break;

				case 'name':
					echo esc_attr(get_the_title($postId));
					break;

				case 'phone':
					echo get_post_meta( $postId, 'phone', true );
					break;
				
				case 'email':
					echo esc_attr($userdata->user_email);
					break;

				case 'profession':
					echo get_post_meta( $postId, 'profession', true );
					break;

				case 'service':
					$staff_services = get_post_meta( $postId, 'services', true );
					if ( is_array($staff_services) && count($staff_services) > 0 ) {
						echo esc_html__('Assigned', 'gfb');
					} else {
						echo esc_html__('Not assigned', 'gfb');
					}
					
					break;
				
				case 'actions':
					echo '<a href="#" data-type="staff" data-id="gfb-staff-modal" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-edit"> <i class="fa fa-edit customer--icon"></i></a>';
					if ( is_user_logged_in() ) {				
						if ( current_user_can( 'administrator' ) ) {				
							if ( 'publish' == get_post_status($postId) ) {
								echo '<a href="#" data-type="staff" data-post_id="' . esc_attr( $postId ) . '" class="wc-dashicons gfb-delete" title="Delete"> <i class="fa fa-trash customer--icon"></i></a>';
							}
						}
					}
					break;
			}
		}

		public function gfb_modify_column_names( $columns ) {			
			unset($columns['date']);
			unset($columns['title']);
			unset($columns['cb']);

			$columns['id'] = __('&nbsp;&nbsp;Staff ID', 'gfb');
			$columns['name'] = __('Name', 'gfb');
			$columns['phone'] = __('Phone', 'gfb');
			$columns['email'] = __('Email', 'gfb');
			$columns['profession'] = __('Profession', 'gfb');
			$columns['service'] = __('Service', 'gfb');
			$columns['actions'] = __('Actions', 'wc-donation');

			return $columns;
		}

		public function gfb_manage_posts_extra_tablenav( $which ) {
			if ( is_user_logged_in() ) {				
				if ( current_user_can( 'administrator' ) ) {
					if ( 'top' === $which ) {
						echo sprintf('<a href="#" data-id="gfb-staff-modal" class="gfb-add-button"><i class="fa fa-plus-circle"></i>%1$s</a>', esc_html__('Add Staff', 'gfb') );
					}
				}
			}			
		}

		public function highlight_menu() {

			//adding customer modal view file
			require_once GFB_ADMIN_VIEW_PATH . 'modal/staff-modal.php';
			
			?>
			<script type="text/javascript">			
				jQuery('#toplevel_page_gfb li, #toplevel_page_gfb li > a').removeClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-staff"]').addClass('current');
				jQuery('#toplevel_page_gfb a[href="edit.php?post_type=gfb-staff"]').parent('li').addClass('current');
			</script>
			<?php
		}

		public function register_staff_posttype() {
			$labels = array(
				'name'                  => _x( 'Staff', 'Post type general name', 'gfb' ),
				'singular_name'         => _x( 'Staff', 'Post type singular name', 'gfb' ),
				'menu_name'             => _x( 'Staff', 'Admin Menu text', 'gfb' ),
				'name_admin_bar'        => _x( 'Staff', 'Add New on Toolbar', 'gfb' ),
				'add_new'               => __( 'Add Staff', 'gfb' ),
				'add_new_item'          => __( 'Add New Staff', 'gfb' ),
				'new_item'              => __( 'New Staff', 'gfb' ),
				'edit_item'             => __( 'Edit Staff', 'gfb' ),
				'view_item'             => __( 'View Staff', 'gfb' ),
				'all_items'             => __( 'Staff', 'gfb' ),
				'search_items'          => __( 'Search Staff', 'gfb' ),
				'parent_item_colon'     => __( 'Parent Staff:', 'gfb' ),
				'not_found'             => __( 'No Staff found.', 'gfb' ),
				'not_found_in_trash'    => __( 'No Staff found in Trash.', 'gfb' ),
				'featured_image'        => _x( 'Staff Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'gfb' ),
				'set_featured_image'    => _x( 'Set Staff image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'remove_featured_image' => _x( 'Remove Staff image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'use_featured_image'    => _x( 'Use as Staff image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'gfb' ),
				'archives'              => _x( 'Staff archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'gfb' ),
				'insert_into_item'      => _x( 'Insert into Staff', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'gfb' ),
				'uploaded_to_this_item' => _x( 'Uploaded to this Staff', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'gfb' ),
				'filter_items_list'     => _x( 'Filter Staff list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'gfb' ),
				'items_list_navigation' => _x( 'Staff list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'gfb' ),
				'items_list'            => _x( 'Staff list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'gfb' ),
			);

			$show_in_menu = 'gfb';		

			if ( is_user_logged_in() ) {				
				if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
					$show_in_menu = true;					
				}
			}

			$args = array(
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => $show_in_menu,
				'menu_icon'          => 'dashicons-businessperson',
				'show_in_rest'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => 'gfb-staff' ),
				'capability_type'    => 'gfb-staff',
				// 'capabilities' => array(
				// 	'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				// ),
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'thumbnail' ),				
			);

			register_post_type( 'gfb-staff', $args );
		}

		public function gfb_filter_query( $admin_query ) {

			if ( is_admin() && $admin_query->is_main_query() ) {

				if ( is_user_logged_in() ) {
					if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
						$current_user_id = get_current_user_id();

						$cond_arr[] = array(
							array(
								'key'     => 'wp_user',
								'value'   => $current_user_id,
								'compare' => '='								
							),
						);

						if ( isset( $cond_arr ) ) {
							$admin_query->set(
								'meta_query',
								$cond_arr
							);							
						}						
					}

					if( absint($admin_query->query_vars['s']) ) {
			            // Set the post id value
			            $admin_query->set('p', $admin_query->query_vars['s']);

			            // Reset the search value
			            $admin_query->set('s', '');
			        }			
				}				
			}			

			return $admin_query;
		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Staff();
			}

			return self::$instance;
		}
	}

	GFB_Staff::get_instance();
}

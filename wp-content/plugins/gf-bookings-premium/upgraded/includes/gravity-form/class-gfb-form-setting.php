<?php
/**
 * GFB Form Setting Class
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'GFB_Form_Setting' ) ) {
	
	class GFB_Form_Setting {		
		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance = null;

		/**
		 * Init the Dependencies.
		 */
		private function __construct() {

			//adding gfb setting menu for each gf form.
			add_filter('gform_form_settings_menu', array($this, 'gfb_form_settings_menu_tab'));
			//adding setting page for gfb setting menu.
			add_action('gform_form_settings_page_gfb_settings_page', array($this, 'gfb_form_settings_menu_page'));
			//saving form setting page values
			add_action('admin_init', array($this, 'gfb_save_form_settings_menu'));

			add_action('gform_field_standard_settings', array($this, 'gfb_appointment_calendar_fields'), 10);
    		add_action('gform_editor_js', array($this, 'gfb_appointment_calendar_fields_script'));    		
		}

		public function gfb_appointment_calendar_fields( $position ) {			

			if ( 50 == $position ) {
				// echo $position . '<br>';
				?>
				<li class="gfb_location_setting field_setting gfb_field_setting">
                    <label for="gfb_location">
                        <?php _e('Select Location', 'gfb'); ?><br>
                        <?php
                        $all_locations = GFB_Location::all_location();                        
                        if ( is_array( $all_locations ) && count( $all_locations ) > 0 ) {
                        	?>
                        	<select name="gfb_location" id="gfb_location" class="gfb_location" onchange="SetFieldProperty( 'gfb_location', this.value );">
                        		<option value="all"><?php _e('All locations', 'gfb'); ?></option>
                        		<?php
                        		foreach ($all_locations as $value) {
                        			echo '<option value="' . esc_attr($value->ID) . '" >' . esc_attr($value->post_title) . '</option>';
                        		}
                        		?>
                        	</select>
                        	<?php
                        }
                        ?>
                    </label>

                    <input type="checkbox" name="gfb_location_hide" id="gfb_location_hide" value="yes" onclick="SetFieldProperty('gfb_location_hide', this.checked);">
                    <label for="gfb_location_hide">
                    	<?php _e('Hide location field from front-end'); ?>
                    </label>
				</li>

				<li class="gfb_service_setting field_setting gfb_field_setting">
                    <label for="gfb_service">
                        <?php _e('Select Service', 'gfb'); ?><br>
                        <?php
                        $all_service = GFB_Service::all_service();                        
                        if ( is_array( $all_service ) && count( $all_service ) > 0 ) {
                        	?>
                        	<select name="gfb_service" id="gfb_service" class="gfb_service" onchange="SetFieldProperty( 'gfb_service', this.value );">
                        		<option value="all"><?php _e('All Service', 'gfb'); ?></option>
                        		<?php
                        		foreach ($all_service as $value) {
                        			echo '<option value="' . esc_attr($value->ID) . '" >' . esc_attr($value->post_title) . '</option>';
                        		}
                        		?>
                        	</select>
                        	<?php
                        }
                        ?>
                    </label>

                    <input type="checkbox" name="gfb_service_hide" id="gfb_service_hide" value="yes" onclick="SetFieldProperty('gfb_service_hide', this.checked);">
                    <label for="gfb_service_hide">
                    	<?php _e('Hide Service field from front-end'); ?>
                    </label>
				</li>

				<li class="gfb_staff_setting field_setting gfb_field_setting">
                    <label for="gfb_staff">
                        <?php _e('Select Staff', 'gfb'); ?><br>
                        <?php
                        $all_staff = GFB_Staff::all_staff();                        
                        if ( is_array( $all_staff ) && count( $all_staff ) > 0 ) {
                        	?>
                        	<select name="gfb_staff" id="gfb_staff" class="gfb_staff" onchange="SetFieldProperty( 'gfb_staff', this.value );">
                        		<option value="all"><?php _e('All Staff', 'gfb'); ?></option>
                        		<?php
                        		foreach ($all_staff as $value) {
                        			$user_id = get_post_meta( $value->ID, 'wp_user', true );				
									$userdata = get_userdata( $user_id );
									if ( $userdata ) {
                        				echo '<option value="' . esc_attr($value->ID) . '" >' . esc_attr($value->post_title) . '</option>';
                        			}
                        		}
                        		?>
                        	</select>
                        	<?php
                        }
                        ?>
                    </label>

                    <input type="checkbox" name="gfb_staff_hide" id="gfb_staff_hide" value="yes" onclick="SetFieldProperty('gfb_staff_hide', this.checked);">
                    <label for="gfb_staff_hide">
                    	<?php _e('Hide Staff field from front-end'); ?>
                    </label>
				</li>
				<?php
			}
		}

		public function gfb_appointment_calendar_fields_script() {
			?>
			<script type="text/javascript">				
				jQuery(document).bind("gform_load_field_settings", function (event, field, form) {

					
					fieldSettings.gfb_appointment_calendar += '.enable_enhanced_ui_setting, .label_setting, .error_message_setting, .label_placement_setting, .admin_label_setting, .size_setting, .css_class_setting, .gfb_location_setting, .gfb_service_setting, .gfb_staff_setting, .rules_setting';

					// console.log(fieldSettings.gfb_appointment_calendar);

					if ( 'gfb_appointment_calendar' == field.type ) {

						//console.log(field);
						
						jQuery('.gfb_field_setting').show();						

						if (typeof field["gfb_location"] !== "undefined") {                            
                            jQuery("#gfb_location").val(field["gfb_location"]).trigger('change');
                        } else {
                            jQuery("#gfb_location").val('all').trigger('change');
                        }

                        if (field["gfb_location_hide"]) {
                            jQuery("#gfb_location_hide").attr("checked", "checked");
                        }

						if (typeof field["gfb_service"] !== "undefined") {
                            //alert(field["gfb_service"]);                         
                            jQuery("#gfb_service").val(field["gfb_service"]).trigger('change');
                        } else {
                            jQuery("#gfb_service").val('all').trigger('change');
                        }

                        if (field["gfb_service_hide"]) {
                            jQuery("#gfb_service_hide").attr("checked", "checked");
                        }

                        if (typeof field["gfb_staff"] !== "undefined") {
                            //alert(field["cardholder_name_label"]);
                            jQuery("#gfb_staff").val(field["gfb_staff"]).trigger('change');
                        } else {
                            jQuery("#gfb_staff").val('all').trigger('change');
                        }

                        if (field["gfb_staff_hide"]) {
                            jQuery("#gfb_staff_hide").attr("checked", "checked");
                        }

					}
				});
			</script>
			<?php
		}

		public function gfb_save_form_settings_menu() {

			if ( ! rgpost('gfb_form_id') ) {
				return;
			}

			if ( isset( $_POST['gfb_setting'] ) ) {
				$form_id = rgpost('gfb_form_id');
				$settings = get_option('gfb_form_settings_' . $form_id); 
				$settings['registration_field'] = array(
            		'fname' => trim( rgpost('fname') ),
            		'lname' => trim( rgpost('lname') ),
            		'email' => trim( rgpost('email') ),
            		'phone' => trim( rgpost('phone') )
            	);

            	update_option('gfb_form_settings_' . $form_id, $settings);
			}
		}

		public function gfb_form_settings_menu_page() {

			$form_id = RGForms::get('id');
			$form   = \GFAPI::get_form($form_id);
			$all_fields = array();
			foreach ( $form['fields'] as $field ) {
				if ( $field->is_conditional_logic_supported() ) {
					$inputs = $field->get_entry_inputs();

					if ( $inputs ) {
						
						// $choices = array();

						foreach ( $inputs as $input ) {
							if ( rgar( $input, 'isHidden' ) ) {
								continue;
							}
							$all_fields[] = array(
								'value' => $input['id'],
								'label' => GFCommon::get_label( $field, $input['id'], true ) . ' Name'
							);
						}						

					} else {
						$all_fields[] = array( 'value' => $field->id, 'label' => GFCommon::get_label( $field ) );
					}

				}
			}

            $settings = get_option('gfb_form_settings_' . $form_id);           
            if ( false == $settings ) {            	
            	$settings['registration_field'] = array(
            		'fname' => '',
            		'lname' => '',
            		'email' => '',
            		'phone' => ''            		
            	);
            }

			GFFormSettings::page_header();
            require_once GFB_ADMIN_VIEW_PATH . 'gfb-form-setting.php';
            GFFormSettings::page_footer();
		}

		public function gfb_form_settings_menu_tab( $menu_items ) {

			$menu_items[] = array(
                'name' => 'gfb_settings_page',
                'icon' => 'dashicons dashicons-calendar-alt',
                'label' => __('GFB Setting')
            );

            return $menu_items;

		}


		/**
		 * Singleton Class Instatnce.
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new GFB_Form_Setting();
			}

			return self::$instance;
		}
	}

	GFB_Form_Setting::get_instance();
}

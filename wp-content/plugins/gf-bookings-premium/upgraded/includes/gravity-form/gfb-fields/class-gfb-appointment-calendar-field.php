<?php
/**
 * GFB Calendar Field Class
 *
 * @package GFB.
 */
defined( 'ABSPATH' ) or exit; // Exit if accessed directly

if ( class_exists( 'GFForms' ) ) {

	class GFB_Appointment_Calendar extends GF_Field {
		
		public $type = 'gfb_appointment_calendar';

		public function get_form_editor_field_title() {
			return esc_attr__( 'GFB Calendar', 'gfb' );
		}

		/*
		* Where to assign this widget
		*/
		public function get_form_editor_button() {
			return array(				
				'group' => 'gfb_field',
				'text'  => $this->get_form_editor_field_title()
			);
		}
		/*
		* Add button to the group
		*/
		public function add_button( $field_groups ) {
			$field_groups = $this->gfb_appointment_group( $field_groups );
			return parent::add_button( $field_groups );
		}
		/*
		* Add our group
		*/
		public function gfb_appointment_group( $field_groups ) {
			foreach ( $field_groups as $field_group ) {
				if ( $field_group['name'] == 'gfb_field' ) {
					return $field_groups;
				}
			}
			$field_groups[] = array(
				'name'   => 'gfb_field',
				'label'  => __( 'GFB Field', 'gfb' ),
				'fields' => array()
			);

			return $field_groups;
		}

		/*
		* Widget settings
		*/
		function get_form_editor_field_settings() {
			return array(				
				'label_setting',
				'size_setting',
				'css_class_setting',
				'rules_setting'			
			);
		}

		public function is_conditional_logic_supported() {
			return true;
		}

		/**
		 * Field Markup
		 */
		public function get_field_input( $form, $value = '', $entry = null ) {

		    $form_id         = $form['id'];
		    $lead_id         = isset($entry['id']) ? $entry['id'] : '';
		    $is_entry_detail = $this->is_entry_detail();
		    $id              = (int) $this->id;
		    $fields = $form['fields'];
		    $size = 'small';
		    $class = '';
		    $gfb_settings = GFB_Setting::get_settings();
		    $location_val = is_array($value) && isset($value['location']) ? $value['location'] : '';
		    $service_val = is_array($value) && isset($value['service']) ? $value['service'] : '';
		    $staff_val = is_array($value) && isset($value['staff']) ? $value['staff'] : '';
		    $date_val = is_array($value) && isset($value['date']) ? $value['date'] : 0;
			$time_val = is_array($value) && isset($value['time']) ? $value['time'] : 0;
			$cost_val = is_array($value) && isset($value['cost']) ? $value['cost'] : 0;
			$slot_qty = is_array($value) && isset($value['slot']) ? $value['slot'] : 0;
			$total_id = 0;
			$booking_type = is_array($value) && isset($value['gfb_booking_type']) ? $value['gfb_booking_type'] : '';

			foreach ( $form['fields'] as $field ) {		        
		        if ( 'total' === $field->type ) {
	                $total_id = $field['id'];
	                break;
	            }
		    }
		 
		    foreach ( $fields as $field) {

		    	// echo 'all field is listed ' . $field->type . '<br>';

		    	if ( 'gfb_appointment_calendar' === $field->type ) {
		    		
		    		$size = $field->size;
		    		$class = $field->cssClass;

		    		if ( isset($field->gfb_location_hide) && $field->gfb_location_hide ) {
		    			$location_hide = $field->gfb_location_hide;
		    		} else {
		    			$location_hide = false;
		    		}

		    		if ( empty($location_val) && isset($field->gfb_location) ) {
		    			$location_val = $field->gfb_location;
		    		}

		    		if ( isset($field->gfb_service_hide) && $field->gfb_service_hide ) {
		    			$service_hide = $field->gfb_service_hide;
		    		} else {
		    			$service_hide = false;
		    		}

		    		if ( empty($service_val) && isset($field->gfb_service) ) {
		    			$service_val = $field->gfb_service;
		    		}

		    		if ( isset($field->gfb_staff_hide) && $field->gfb_staff_hide ) {
		    			$staff_hide = $field->gfb_staff_hide;
		    		} else {
		    			$staff_hide = false;
		    		}

		    		if ( empty($staff_val) && isset($field->gfb_staff) ) {
		    			$staff_val = $field->gfb_staff;
		    		}

		    		break;
		    	}
		    }		    

		    ob_start();		    

		    if ( is_admin() && $this->is_entry_edit() ) {
				$appointmentData = gform_get_meta( $lead_id, $id, true );
				?>
				<p><?php esc_html_e('Booking fields are not editable', 'gfb'); ?></p>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[location]' value='<?php echo esc_attr($appointmentData['location']); ?>'>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[service]' value='<?php echo esc_attr($appointmentData['service']); ?>'>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[staff]' value='<?php echo esc_attr($appointmentData['staff']); ?>'>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[date]' value='<?php echo esc_attr($appointmentData['date']); ?>'>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[time]' value='<?php echo esc_attr($appointmentData['time']); ?>'>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[slot]' value='<?php echo esc_attr($appointmentData['slot']); ?>'>
				<input type='hidden' name='input_<?php echo esc_attr($id); ?>[cost]' value='<?php echo esc_attr($appointmentData['cost']); ?>'>
				<?php
			} else if ( ! is_admin() ) {
			    		    	
		    	$all_locations = GFB_Location::all_location();
                if ( is_array( $all_locations ) && count( $all_locations ) > 0 ) {
                	?>
                	<div style="margin-bottom: 15px; <?php echo (true === $location_hide) ? 'display: none' : ''; ?>" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
	                	<select class="<?php echo esc_attr($size) . ' ' . esc_attr($class); ?> gfb_location" id="gfb_location_<?php echo esc_attr($form_id); ?>">
	                		<option value="all"><?php esc_html_e($gfb_settings['gfb_label_location']); ?></option>
	                		<?php
	                		foreach ($all_locations as $value) {
	                			echo '<option value="' . esc_attr($value->ID) . '" ' . selected($value->ID, $location_val, false) . '>' . esc_attr($value->post_title) . '</option>';
	                		}
	                		?>
	                	</select>
	                </div>
                	<?php
                }


	            $all_service = GFB_Service::all_service();                        
                if ( is_array( $all_service ) && count( $all_service ) > 0 ) {
                	?>
                	<div style="margin-bottom: 15px; <?php echo (true === $service_hide) ? 'display: none' : ''; ?>" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
	                	<select class="<?php echo esc_attr($size) . ' ' . esc_attr($class); ?> gfb_service" id="gfb_service_<?php echo esc_attr($form_id); ?>">
	                		<option value="all"><?php esc_html_e($gfb_settings['gfb_label_service']); ?></option>
	                		<?php
	                		foreach ($all_service as $value) {
	                			echo '<option value="' . esc_attr($value->ID) . '" ' . selected($value->ID, $service_val, false) . '>' . esc_attr($value->post_title) . '</option>';
	                		}
	                		?>
	                	</select>
	                </div>
                	<?php
                }


	            $all_staff = GFB_Staff::all_staff();
                if ( is_array( $all_staff ) && count( $all_staff ) > 0 ) {
                	?>
                	<div style="margin-bottom: 15px; <?php echo (true === $staff_hide) ? 'display: none' : ''; ?>" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
	                	<select class="<?php echo esc_attr($size) . ' ' . esc_attr($class); ?> gfb_staff" id="gfb_staff_<?php echo esc_attr($form_id); ?>">
	                		<option value="all"><?php esc_html_e($gfb_settings['gfb_label_staff']); ?></option>
	                		<?php
	                		foreach ($all_staff as $value) {
	                			$user_id = get_post_meta( $value->ID, 'wp_user', true );				
								$userdata = get_userdata( $user_id );
								if ( $userdata ) {
	                				echo '<option value="' . esc_attr($value->ID) . '" ' . selected($value->ID, $staff_val, false) . '>' . esc_attr($value->post_title) . '</option>';
	                			}
	                		}
	                		?>
	                	</select>
	                </div>
                	<?php
                }

	            ?>
	            <div style="margin-bottom: 15px;" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
	            	<div id="gfb-fancy-calendar">
	            		<?php
	            		GFB_Calendar::render_frontend( $form_id );
	            		?>
	            	</div>	            	
            		<input type="hidden" id="gfb_form_id" name="gfb_form_id" class="gfb_form_id" value="<?php echo esc_attr($form_id); ?>" />
            		<input type="hidden" id="gfb_hdn_location_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[location]" class="gfb_location_input" value="<?php echo esc_attr($location_val); ?>" />
            		<input type="hidden" id="gfb_hdn_service_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[service]" class="gfb_service_input" value="<?php echo esc_attr($service_val); ?>" />
            		<input type="hidden" id="gfb_hdn_staff_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[staff]" class="gfb_staff_input" value="<?php echo esc_attr($staff_val); ?>" />
            		<input type="hidden" id="gfb_date_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[date]" class="gfb_date_input" value="<?php echo esc_attr($date_val); ?>" />
            		<input type="hidden" id="gfb_time_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[time]" class="gfb_time_input" value="<?php echo esc_attr($time_val); ?>" />
            		<input type="hidden" id="gfb_slot_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[slot]" class="gfb_slot_input" value="<?php echo esc_attr($slot_qty); ?>" />
            		<input type="hidden" id="gfb_cost_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[cost]" class="gfb_cost_input" value="<?php echo esc_attr($cost_val); ?>" />            		
            		<input type="hidden" id="gfb_booking_type_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[gfb_booking_type]" value="<?php echo esc_attr($booking_type); ?>" />
            		<input type="hidden" id="gfb_total_field_id_<?php echo esc_attr($form_id); ?>" name="input_<?php echo esc_attr($id); ?>[total_id]" value="<?php echo esc_attr($total_id); ?>" />
	            </div>
	            <?php
		    	
			} else {
				?>
				<div style="margin-bottom: 15px;" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
						<select class="<?php echo esc_attr($size); ?>">
			        		<option value="all"><?php esc_html_e($gfb_settings['gfb_label_location']); ?></option>	        		
			        	</select>
			    </div>
			    <div style="margin-bottom: 15px;" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">		        	
		        	<select class="<?php echo esc_attr($size); ?>">
		        		<option value="all"><?php esc_html_e($gfb_settings['gfb_label_service']); ?></option>	        		
		        	</select>
		        </div>
		        <div style="margin-bottom: 15px;" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
		        	<select class="<?php echo esc_attr($size); ?>">
		        		<option value="all"><?php esc_html_e($gfb_settings['gfb_label_staff']); ?></option>	        		
		        	</select>		        	
		        </div>
		        <div style="margin-bottom: 15px;" class="ginput_container ginput_container_select ginput_container_gfb_appointment_calendar" id="gf_gfb_appointment_calendar_<?php echo esc_attr($form_id); ?>">
		        	<?php echo esc_html__('Calendar will be display on front-end.', 'gfb'); ?>
		        </div>
				<?php
			}

		    return ob_get_clean();
		}

		/**
		 * Is Entry Edit
		 */
		public function is_entry_edit() {
			if ( rgget( 'page' ) == 'gf_entries' && rgget( 'view' ) == 'entry' && rgpost( 'screen_mode' ) == 'edit' ) {
				return true;
			}

			return false;
		}

		/**
		 * Validation Failed
		 */
		private function validationFailed( $message = '' ) {			
			$this->failed_validation = true;			
			$this->validation_message = empty( $this->errorMessage ) ? $message : $this->errorMessage;
		}

		/**
		 * Validation
		 */
		public function validate( $value, $form ) {
			$form_id = absint( $form['id'] );
			$fields = $form['fields'];
			$location_hide = false;
			foreach ( $fields as $field) {
		    	if ( 'gfb_appointment_calendar' === $field->type ) {		    		

		    		if ( isset($field->gfb_location_hide) && $field->gfb_location_hide ) {
		    			$location_hide = $field->gfb_location_hide;
		    		}

		    		break;
		    	}
		    }

			if ( is_array( $value ) ) {
				$location_condition = isset( $value['location'] ) && ! empty( $value['location'] ) && 'all' != $value['location'] && 'no' != $value['location'] ? true : false;
				$service_condition = isset( $value['service'] ) && ! empty( $value['service'] ) && 'all' != $value['service'] && 'no' != $value['service'] ? true : false;
				$staff_condition = isset( $value['staff'] ) && ! empty( $value['staff'] ) && 'all' != $value['staff'] && 'no' != $value['staff'] ? true : false;
				$date_condition = isset( $value['date'] ) && ! empty( $value['date'] ) && '' != $value['date'] ? true : false;
				$time_condition = isset( $value['time'] ) && ! empty( $value['time'] ) && '' != $value['time'] ? true : false;
				$slot_condition = isset( $value['slot'] ) && ! empty( $value['slot'] ) && '' != $value['slot'] ? true : false;

				// echo '<pre>$value$value';
				// print_r($value);
				// echo '</pre>';

				// foreach ( $form['fields'] as $field ) {		        
			    //     if ( 'total' === $field->type ) {
		        //         $value['cost'] = $_POST['input_' . $field['id']];		                
		        //         break;
		        //     }
			    // }

				if ( !$location_hide && !$location_condition ) {
					$this->validationFailed( __('Location field is required.', 'gfb') );
					return;
				}

				if ( !$service_condition ) {
					$this->validationFailed( __('Service field is required.', 'gfb') );
					return;
				}

				if ( !$staff_condition ) {
					$this->validationFailed( __('Staff field is required.', 'gfb') );
					return;
				}

				if ( !$date_condition && !$time_condition ) {
					$this->validationFailed( __('Appointment Date & Time is required.', 'gfb') );
					return;
				}
				
				if ( !$slot_condition ) {
					$this->validationFailed( __('Slot cannot be less than 1.', 'gfb') );
					return;
				} else {
					$staffData = GFB_Staff::get_staff_by_id( $value['staff'] );
					$daySName = strtolower( date('D', strtotime($value['date']) ) );
					if ( isset( $staffData['timeslots'] ) && is_array( $staffData['timeslots'] ) && count( $staffData['timeslots'] ) > 0 ) {
						#continue.
						$staff_timeslots = $staffData['timings'];
						if ( isset( $staffData['booking_type'] ) && 'fullday' == $staffData['booking_type'] ) { //check if is fullday booking type
							$total_slots_consumed = (int) GFB_Appointment::gfb_all_appointment_by_date_time( $value['staff'], $value['date'], '00:00 - 23:59', 'fullday' );
							$slot_capacity = (int) $staff_timeslots[$daySName]['capacity'];
							$remaining_capacity = $slot_capacity - $total_slots_consumed;
						} else { //custom slots
							$total_slots_consumed = (int) GFB_Appointment::gfb_all_appointment_by_date_time( $value['staff'], $value['date'], $value['time'] );
							$slot_capacity = (int) $staff_timeslots[$daySName]['capacity'];
							$remaining_capacity = $slot_capacity - $total_slots_consumed;
						}

						if ( $value['slot'] > $remaining_capacity ) {
							$this->validationFailed( __('Slot capacity exceeded.', 'gfb') );
							return;
						}
					}					
				}
								
			}

		}

		/**
		* Make this function as void() so double meta should not saved.
		*/
		public function get_value_save_entry( $value, $form, $input_name, $lead_id, $lead ) {			

			#continue
			if ( is_array( $value ) ) {

				return $value;
			}

			return $value;
		}

		/**
		* Show service title entry single
		*/
		public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {

			$value = unserialize($value);

			// echo '<pre>$value$value';
			// print_r( $value );
			// echo '</pre>';

			$html = '';
			if ( is_array( $value ) ) {	
				foreach( $value as $k => $val ) {
					if ( 'location' == $k && 'all' != $val ) {
						$html .= sprintf(__( 'Location Name: %1$s', 'gfb'), get_the_title($val) );
						$html .= '<br/>';
						$html .= sprintf(__( 'Location ID: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'service' == $k ) {
						$html .= sprintf(__( 'Service Name: %1$s', 'gfb'), get_the_title($val) );
						$html .= '<br/>';
						$html .= sprintf(__( 'Service ID: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'staff' == $k ) {
						$html .= sprintf(__( 'Staff Name: %1$s', 'gfb'), get_the_title($val) );
						$html .= '<br/>';
						$html .= sprintf(__( 'Staff ID: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'date' == $k ) {
						$val = apply_filters('gfb_entry_value_date_format', $val );						
						$html .= sprintf(__( 'Appointment Date: %1$s', 'gfb'), $val );
						$html .= '<br/>';
					}

					if ( 'time' == $k ) {
	                    $val = apply_filters('gfb_entry_value_time_format', $val );
	                    if ( isset($value['gfb_booking_type']) && 'fullday' === $value['gfb_booking_type'] ) {
	                        $html .= sprintf(__( 'Appointment Time: %1$s', 'gfb'), __('Fullday', 'gfb') );
	                    } else {
	                        $html .= sprintf(__( 'Appointment Time: %1$s', 'gfb'), $val );   
	                    }
	                    $html .= '<br/><br/>';
	                }

					if ( 'slot' == $k ) {
						$html .= sprintf(__( 'Slot Capacity: %1$s', 'gfb'), $val );						
						$html .= '<br/><br/>';
					}

					if ( 'cost' == $k ) {
						$html .= sprintf(__( 'Total Cost: %1$s', 'gfb'), gfb_price_format($val) );
						$html .= '<br/>';
					}
				}
			}

			return $html;			
		}

		/**
		* Merge tag, on notifications, confirmations
		*/
		public function get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br ) {

			$data = $_POST;

			if ( isset($data['input_'.$input_id]) ) {
				$value = apply_filters( 'gfb_get_value_merge_tag', $data['input_'.$input_id]);
				do_action( 'gfb_get_value_merge_tag', $value );
			}

			$html = '';
			if ( is_array( $value ) ) {	
				foreach( $value as $k => $val ) {
					if ( 'location' == $k && 'all' != $val ) {
						$html .= sprintf(__( 'Location Name: %1$s', 'gfb'), get_the_title($val) );
						$html .= '<br/>';
						$html .= sprintf(__( 'Location ID: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'service' == $k ) {
						$html .= sprintf(__( 'Service Name: %1$s', 'gfb'), get_the_title($val) );
						$html .= '<br/>';
						$html .= sprintf(__( 'Service ID: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'staff' == $k ) {
						$html .= sprintf(__( 'Staff Name: %1$s', 'gfb'), get_the_title($val) );
						$html .= '<br/>';
						$html .= sprintf(__( 'Staff ID: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'date' == $k ) {
						$val = apply_filters('gfb_entry_value_date_format', $val );
						$html .= sprintf(__( 'Appointment Date: %1$s', 'gfb'), $val );
						$html .= '<br/><br/>';
					}

					if ( 'time' == $k ) {
	                    $val = apply_filters('gfb_entry_value_time_format', $val );
	                    if ( isset($value['gfb_booking_type']) && 'fullday' === $value['gfb_booking_type'] ) {
	                        $html .= sprintf(__( 'Appointment Time: %1$s', 'gfb'), __('Fullday', 'gfb') );
	                    } else {
	                        $html .= sprintf(__( 'Appointment Time: %1$s', 'gfb'), $val );   
	                    }
	                    $html .= '<br/><br/>';
	                }

					if ( 'slot' == $k ) {
						$html .= sprintf(__( 'Slot Capacity: %1$s', 'gfb'), $val );						
						$html .= '<br/><br/>';
					}

					if ( 'cost' == $k ) {
						$html .= sprintf(__( 'Total Cost: %1$s', 'gfb'), gfb_price_format($val) );
						$html .= '<br/><br/>';
					}
				}
			}

			return $html;
		}
		
		public function get_form_inline_script_on_page_render( $form ) {			
			$fields = $form['fields'];
			$form_id = absint( $form['id'] );
			$id = '';			
			$gfb_settings = GFB_Setting::get_settings();
			$flag = false;
			$gfb_location = '';
			$gfb_service = '';
			$gfb_staff = '';
			foreach ( $fields as $field) {
		    	if ( 'gfb_appointment_calendar' === $field->type ) {		    		
		    		$id = $field->id;		    		
		    		$gfb_location = $field->gfb_location;
					$gfb_service = $field->gfb_service;
					$gfb_staff = $field->gfb_staff;
		    		$flag = true;
		    		break;
		    	}
		    }

		    if ( $flag ) {

		    	?>
		    	<style>
		    		:root {
		    			--gfb-cal-bg-gradient1: <?php echo esc_attr($gfb_settings['gfb_cal_gradient_color_top']); ?>;
		    			--gfb-cal-bg-gradient2: <?php echo esc_attr($gfb_settings['gfb_cal_gradient_color_bottom']); ?>;
		    			--gfb-cal-border-color: <?php echo esc_attr($gfb_settings['gfb_cal_border_color']); ?>;
						--gfb-cal-header-bg-color: <?php echo esc_attr($gfb_settings['gfb_cal_header_bg_color']); ?>;
						--gfb-cal-header-txt-color: <?php echo esc_attr($gfb_settings['gfb_cal_header_text_color']); ?>;
		    			--gfb-cal-txt-color: <?php echo esc_attr($gfb_settings['gfb_cal_text_color']); ?>;
		    			--gfb-cal-sel-day-txt-color: <?php echo esc_attr($gfb_settings['gfb_cal_selected_text_color']); ?>;
		    			--gfb-cal-sel-day-bg-color: <?php echo esc_attr($gfb_settings['gfb_cal_selected_text_bg_color']); ?>;
		    			--gfb-cal-avl-day-txt-color: <?php echo esc_attr($gfb_settings['gfb_cal_available_days_text_color']); ?>;
		    			--gfb-cal-avl-day-bg-color: <?php echo esc_attr($gfb_settings['gfb_cal_available_days_bg_color']); ?>;
		    			--gfb-cal-hol-day-txt-color: <?php echo esc_attr($gfb_settings['gfb_cal_holiday_days_text_color']); ?>;
		    			--gfb-cal-hol-day-bg-color: <?php echo esc_attr($gfb_settings['gfb_cal_holiday_days_bg_color']); ?>;
		    		}

		    		.gfb-table-container .gfb-cal-table,
		    		.gfb-table-container #gfb-timeslots-table-container {
		    			background-image: linear-gradient(var(--gfb-cal-bg-gradient1), var(--gfb-cal-bg-gradient2)) !important;
		    		}

		    		.gfb-cal-container .gfb-cal-table {
					    color: var(--gfb-cal-txt-color);
					    font-size: 13px;
					    width: 100%;
					    max-width: 560px;
					    max-height: 372px;
					    box-shadow: 0px 8px 59px -13px rgb(0 0 0 / 21%);
					    -webkit-box-shadow: 0px 8px 59px -13px rgb(0 0 0 / 21%);
					    -moz-box-shadow: 0px 8px 59px -13px rgb(0 0 0 / 21%);
					    -ms-box-shadow: 0px 8px 59px -13px rgb(0 0 0 / 21%);
					    -o-box-shadow: 0px 8px 59px -13px rgb(0 0 0 / 21%);
					}

		    		.gfb-cal-container h3 {
		    			background: var(--gfb-cal-header-bg-color)!important;
					    color: var(--gfb-cal-header-txt-color)!important;
					    border: 1px solid var(--gfb-cal-border-color)!important;					    
					    border-bottom: 0!important;
					    font-size:  13px;
					    border-radius: 0!important;
					}

					.gfb-cal-container .gfb-month-changer {						
					    color: var(--gfb-cal-header-txt-color)!important;					    
					    font-size:  13px;					    
					}

					.gfb-cal-table-bordered tbody tr td {
						border: 0.5px solid var(--gfb-cal-border-color)!important;
						width: 80px;
					}

					.gfb-cal-table-bordered thead tr {
						background: var(--gfb-cal-header-bg-color)!important;
					    color: var(--gfb-cal-header-txt-color)!important;
					    border-left: 1px solid var(--gfb-cal-border-color)!important;
					    border-right: 1px solid var(--gfb-cal-border-color)!important;
					}
					.gfb-cal-table-bordered thead tr th {						
					    border: 0!important;
					    padding: 12px!important;
					    font-weight: normal!important;
					}
		    	</style>
		    	<?php

		    	if ( GFB_LIVE_MODE ) {
					$minimized = '.min';
					$clear_cache = '';
				} else {
					$minimized = '';
					$clear_cache = '?t=' . gmdate('ymdhis');
				}

		    	wp_enqueue_style( 'gfb-font-awesome-style', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), GFB_VERSION . $clear_cache, 'all' );

				wp_enqueue_style( 'gfb-calendar-style', GFB_ADMIN_CSS_URL . 'gfb-calendar-style' . $minimized . '.css', array(), GFB_VERSION . $clear_cache, 'all' );		

				wp_register_script('gfb-calendar-script', GFB_ADMIN_JS_URL . 'gfb-calendar-script' . $minimized . '.js', array('jquery'), GFB_VERSION . $clear_cache, true);
				wp_localize_script('gfb-calendar-script', 'gfb_calendar', array(
					'ajaxurl'  => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('gfb'),
					'prior_months' => $gfb_settings['gfb_prior_months_book_appointment'],
					'show_timeslot' => $gfb_settings['gfb_show_timeslot']
				));

				wp_enqueue_script('gfb-calendar-script');

				wp_register_script( 'gfb-common-script', GFB_COMMON_URL . 'gfb-common-script' . $minimized . '.js', array('jquery'), GFB_VERSION . $clear_cache, true );
				wp_localize_script( 'gfb-common-script', 'gfb_common', array(
					'ajaxurl'  => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('gfb'),
					'form_id' => $form_id,
					'prior_months' => $gfb_settings['gfb_prior_months_book_appointment'],
					'is_admin' => 'no',
					'loadingText' => __('loading...', 'gfb'),
					'all_service_text' => $gfb_settings['gfb_label_service'],
					'no_service_text' => __('No Service Found', 'gfb'),
					'all_location_text' => $gfb_settings['gfb_label_location'],
					'no_location_text' => __('No Location Found', 'gfb'),
					'all_staff_text' => $gfb_settings['gfb_label_staff'],
					'no_staff_text' => __('No Staff Found', 'gfb'),
					'default_location' => isset( $_POST['input_' . $id ] ) && is_array( $_POST['input_' . $id ] ) && isset( $_POST['input_' . $id ]['location'] ) ? sanitize_text_field( $_POST['input_' . $id ]['location'] ) : $gfb_location,
					'default_service' => isset( $_POST['input_' . $id ] ) && is_array( $_POST['input_' . $id ] ) && isset( $_POST['input_' . $id ]['service'] ) ? sanitize_text_field( $_POST['input_' . $id ]['service'] ) : $gfb_service,
					'default_staff' => isset( $_POST['input_' . $id ] ) && is_array( $_POST['input_' . $id ] ) && isset( $_POST['input_' . $id ]['staff'] ) ? sanitize_text_field( $_POST['input_' . $id ]['staff'] ) : $gfb_staff,
				));

				wp_enqueue_script('gfb-common-script');
		    }
		}

		public function get_form_editor_inline_script_on_page_render() {
			$script ="			
			gform.addFilter('gform_form_editor_can_field_be_added', function (canFieldBeAdded, type) {				
				if (type == 'gfb_appointment_calendar') {					
					if (GetFieldsByType(['gfb_appointment_calendar']).length > 0) {
						alert(" . json_encode( esc_html__( 'Only one Booking Appointment Calendar field can be added to the form', 'gfb' ) ) . ");
						return false;
					}
				}
				return canFieldBeAdded;
			});" . PHP_EOL;

			return $script;
		}



	} // end class
	GF_Fields::register( new GFB_Appointment_Calendar() );
} // end if

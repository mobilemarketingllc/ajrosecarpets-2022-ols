<?php
/*
*
* All common functions for gravity booking.
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_filter('gform_get_field_value', function( $value, $entry, $field ) {
	if ( isset($field->type) && 'gfb_appointment_calendar' == $field->type && isset($_GET['page']) && 'gf_entries' === $_GET['page'] && ! isset($_GET['view']) ) {
		$html = '';
		$value = unserialize($value);
		if ( is_array( $value ) ) {	
			return get_the_title($value['service']);
		}
	}

	return $value;
}, 10, 3);

if ( ! function_exists('gfb_translable_name') ) {
	function gfb_translable_name( $type = '', $string = '' ) {		
		
		if ( 'weeks_short' == $type ) {
			$weeks = array(
				'sunday' => __('SUN', 'gfb'),
				'monday' => __('MON', 'gfb'),
				'tuesday' => __('TUE', 'gfb'),
				'wednesday' => __('WED', 'gfb'),
				'thursday' => __('THU', 'gfb'),
				'friday' => __('FRI', 'gfb'),
				'saturday' => __('SAT', 'gfb'),
			);

			if ( empty( $string ) ) {
				return $weeks;
			} else {
				return isset($weeks[$string]) ? $weeks[$string] : $string;
			}
		}

		if ( 'weeks' == $type ) {
			$weeks = array(
				'sunday' => __('Sunday', 'gfb'),
				'monday' => __('Monday', 'gfb'),
				'tuesday' => __('Tuesday', 'gfb'),
				'wednesday' => __('Wednesday', 'gfb'),
				'thursday' => __('Thursday', 'gfb'),
				'friday' => __('Friday', 'gfb'),
				'saturday' => __('Saturday', 'gfb'),
			);

			if ( empty( $string ) ) {
				return $weeks;
			} else {
				return isset($weeks[$string]) ? $weeks[$string] : $string;
			}
		}

		if ( 'months_short' == $type ) {
			$months = array(
				'january' => __('JAN', 'gfb'),
				'february' => __('FEB', 'gfb'),
				'march' => __('MAR', 'gfb'),
				'april' => __('APR', 'gfb'),
				'may' => __('MAY', 'gfb'),
				'june' => __('JUN', 'gfb'),
				'july' => __('JUL', 'gfb'),
				'august' => __('AUG', 'gfb'),
				'september' => __('SEP', 'gfb'),
				'october' => __('OCT', 'gfb'),
				'november' => __('NOV', 'gfb'),
				'december' => __('DEC', 'gfb'),
			);

			if ( empty( $string ) ) {
				return $months;
			} else {
				return isset($months[$string]) ? $months[$string] : $string;
			}

		}

		if ( 'months' == $type ) {
			$months = array(
				'january' => __('January', 'gfb'),
				'february' => __('February', 'gfb'),
				'march' => __('March', 'gfb'),
				'april' => __('April', 'gfb'),
				'may' => __('May', 'gfb'),
				'june' => __('June', 'gfb'),
				'july' => __('July', 'gfb'),
				'august' => __('August', 'gfb'),
				'september' => __('September', 'gfb'),
				'october' => __('October', 'gfb'),
				'november' => __('November', 'gfb'),
				'december' => __('December', 'gfb'),
			);

			if ( empty( $string ) ) {
				return $months;
			} else {
				return isset($months[$string]) ? $months[$string] : $string;
			}

		}
	}
}

if ( ! function_exists('gfb_field_exist') ) {
	function gfb_field_exist( $form ) {
		if ( isset( $form['fields'] ) ) {
			foreach ( $form['fields'] as $field ) {
		    	if ( 'gfb_appointment_calendar' === $field->type ) {		    		
		    		return $field->id;		 
		    	}
		    }		    
		} else {
			return false;
		}
	}
}

if ( ! function_exists('gfb_timezone_string') ) {
	function gfb_timezone_string( $staff_id = '' ) {		

		if ( ! empty( $staff_id ) ) {			

			$offset  = get_post_meta( $staff_id, 'timezone', true );
			if ( str_contains($offset, 'UTC') ) {
				$offset = (float) str_replace('UTC', '', $offset);				

			    $hours   = (int) $offset;			    
			    $minutes = ( $offset - $hours );			    
			 
			    $sign      = ( $offset < 0 ) ? '+' : '-';
			    $abs_hour  = abs( $hours );
			    $abs_mins  = abs( $minutes * 60 );
			    $tz_offset = sprintf( 'Etc/GMT%s%s', $sign, $abs_hour );
				
			    return $tz_offset;
			} else {
				return $offset;
			}
		}

	    $timezone_string = get_option( 'timezone_string' );
	 
	    if ( $timezone_string ) {
	        return $timezone_string;
	    }
	 
	    $offset  = (float) get_option( 'gmt_offset' );
	    $hours   = (int) $offset;
	    $minutes = ( $offset - $hours );
	 
	    $sign      = ( $offset < 0 ) ? '+' : '-';
	    $abs_hour  = abs( $hours );
	    $abs_mins  = abs( $minutes * 60 );
	    $tz_offset = sprintf( 'Etc/GMT%s%s', $sign, $abs_hour );
	 
	    return $tz_offset;
	}
}

if ( ! function_exists('gfb_currency_symbol') ) {
	function gfb_currency_symbol() {
		// $arr = GFB_Main::$gfb_currency;
		$arr = RGCurrency::get_currency(GFCommon::get_currency());
		if ( is_array($arr) && isset($arr['symbol_left']) && ! empty($arr['symbol_left']) ) {
			return $arr['symbol_left'];
		}

		if ( is_array($arr) && isset($arr['symbol_right']) && ! empty($arr['symbol_right']) ) {
			return $arr['symbol_right'];
		}

	}
}

if ( ! function_exists('gfb_price_format') ) {
	function gfb_price_format($price, $currency = true, $echo = false) {
		$price = (float) $price;
		$arr = RGCurrency::get_currency(GFCommon::get_currency());

		if ( is_array($arr) && isset($arr['symbol_left']) && ! empty($arr['symbol_left']) ) {
			if ( $echo ) {
				if ( $currency ) {
					echo  esc_attr($arr['symbol_left']) . number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] );
				} else {
					echo  number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] );
				}
			} else {
				if ( $currency ) {
					return  esc_attr($arr['symbol_left']) . number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] );
				} else {
					return  number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] );
				}
			}
			
		}

		if ( is_array($arr) && isset($arr['symbol_right']) && ! empty($arr['symbol_right']) ) {
			if ( $echo ) {
				if ( $currency ) {
					echo number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] ) . esc_attr($arr['symbol_right']);
				} else {
					echo number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] );
				}
			} else {
				if ( $currency ) {
					return number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] ) . esc_attr($arr['symbol_right']);	
				} else {
					return number_format($price, $arr['decimals'], $arr['decimal_separator'], $arr['thousand_separator'] );
				}
			}
			
		}

		return $price;
	}
}

/**
* Create time slots between two given times
* Created by developer53 for future purpose
*/
if ( ! function_exists('gfb_split_time') ) {
	function gfb_split_time( $StartTime='', $EndTime='', $duration = 30, $interval = 15, $capacity = 1 ) {
	    
	    $ReturnArray = array ();// Define output	    
	    $StartTime    = strtotime($StartTime); //Get Timestamp
	    $EndTime      = strtotime($EndTime); //Get Timestamp

	    if ( '' != $duration && '' != $interval ) {

	    	$AddMins  = $duration * 60;
		    $gap = $interval * 60;
		    $i = 1;

		    while ( $StartTime < $EndTime ) { //Run loop  
		    	
		    	if ( $i > 1 ) {
					$StartTime += $gap;		
		    	}
		    	
		        $ReturnArray['slot'][] = date ( "G:i", $StartTime ) . ' - ' . date ( "G:i", ( $StartTime + $AddMins ) );
		        $ReturnArray['capacity'][] = $capacity;
		        // $ReturnArray['add_break'][] = 'false';
		        $ReturnArray['start'][] = date ( "G:i", $StartTime );
		        $ReturnArray['end'][] = date ( "G:i", ( $StartTime + $AddMins ) );
		        $StartTime += $AddMins; //Endtime check

		        $i++;
		    }
		}

	    return $ReturnArray;
	}
}

if ( ! function_exists('gfb_dates_in_range') ) {
	function gfb_dates_in_range($date1, $date2, $format = 'Y-m-d' ) {

		if ( empty($date1) || empty($date2) ) {
			return array();
		}

  		$dates = array();
  		$current = strtotime($date1);
  		$date2 = strtotime($date2);
  		$stepVal = '+1 day';
  		while( $current <= $date2 ) {
        	$dates[] = date($format, $current);
        	$current = strtotime($stepVal, $current);
  		}
  		return $dates;
   }
}

add_filter('gfb_staff_time_slot', function ($slot, $staff_data) {

	$gfb_settings = get_option('gfb_settings', array());
	$gfb_calendar_timeslot_format = isset($gfb_settings['general']['gfb_calendar_timeslot_format']) ? $gfb_settings['general']['gfb_calendar_timeslot_format'] : '24';

	if ( '12' == $gfb_calendar_timeslot_format && 'custom' == $staff_data['booking_type'] ) {

		$slot = explode('-', $slot);	
		$start_time = isset($slot[0]) ? trim($slot[0]) : '';
		$end_time = isset($slot[1]) ? trim($slot[1]) : '';
		if ( ! empty($start_time) && ! empty($end_time) ) {
			$start_time = date('h:i a', strtotime($start_time));
			$end_time = date('h:i a', strtotime($end_time));
			return $start_time . ' - ' . $end_time;			
		}
	}

	return $slot;

}, 10, 2);

// add_filter('gfb_appointment_time_format', function ($slot) {

// 	$slot = explode('-', $slot);
// 	$start_time = isset($slot[0]) ? trim($slot[0]) : '';
// 	$end_time = isset($slot[1]) ? trim($slot[1]) : '';
// 	if ( ! empty($start_time) && ! empty($end_time) ) {
// 		$start_time = date('h:i a', strtotime($start_time));
// 		$end_time = date('h:i a', strtotime($end_time));
// 		//return $start_time . ' - ' . $end_time;
// 		return $start_time;
// 	}
	
// 	return $slot;

// }, 10, 1);

// add_filter('gfb_appointment_date_format', function ($date) {

// 	if ( ! empty($date) ) {		
// 		return date('d-M-Y', strtotime($date));
// 	}
	
// 	return $slot;

// }, 10, 1);

// add_filter( 'gform_entry_field_value', function ( $value, $field, $entry, $form ) {

// 	// echo '<pre>';
// 	// print_r($field->type);
// 	// echo '</pre>';
// 	//wp_die('wait');
	        
//     if ( 'gfb_appointment_calendar' === $field->type ) {
//         $field_id = $field['id'];
//         $value = gform_get_meta($entry['id'], $field_id);
//         $html = '';
//         if ( is_array( $value ) ) { 
//             foreach( $value as $k => $val ) {
//                 if ( 'location' == $k && 'all' != $val ) {
//                     $html .= sprintf(__( 'Location Name: %1$s', 'gfb'), get_the_title($val) );
//                     $html .= '<br/>';
//                     $html .= sprintf(__( 'Location ID: %1$s', 'gfb'), $val );
//                     $html .= '<br/><br/>';
//                 }

//                 if ( 'service' == $k ) {
//                     $html .= sprintf(__( 'Service Name: %1$s', 'gfb'), get_the_title($val) );
//                     $html .= '<br/>';
//                     $html .= sprintf(__( 'Service ID: %1$s', 'gfb'), $val );
//                     $html .= '<br/><br/>';
//                 }

//                 if ( 'staff' == $k ) {
//                     $html .= sprintf(__( 'Staff Name: %1$s', 'gfb'), get_the_title($val) );
//                     $html .= '<br/>';
//                     $html .= sprintf(__( 'Staff ID: %1$s', 'gfb'), $val );
//                     $html .= '<br/><br/>';
//                 }

//                 if ( 'date' == $k ) {
//                     $val = apply_filters('gfb_entry_value_date_format', $val );
//                     $html .= sprintf(__( 'Appointment Date: %1$s', 'gfb'), $val );                                
//                     $html .= '<br/>';
//                 }

//                 if ( 'time' == $k ) {
//                     $val = apply_filters('gfb_entry_value_time_format', $val );
//                     if ( isset($value['gfb_booking_type']) && 'fullday' === $value['gfb_booking_type'] ) {
//                         $html .= sprintf(__( 'Appointment Time: %1$s', 'gfb'), __('Fullday', 'gfb') );
//                     } else {
//                         $html .= sprintf(__( 'Appointment Time: %1$s', 'gfb'), $val );   
//                     }
//                     $html .= '<br/><br/>';
//                 }

//                 if ( 'slot' == $k ) {
//                     $html .= sprintf(__( 'Slot Capacity: %1$s', 'gfb'), $val );                     
//                     $html .= '<br/><br/>';
//                 }

//                 if ( 'cost' == $k ) {
//                     $html .= sprintf(__( 'Total Cost: %1$s', 'gfb'), gfb_price_format($val) );
//                     $html .= '<br/>';
//                 }
//             }

//             return $html;

//         } else {
//         	return $value;
//         }
//     }
 
//     return $value;
// }, 10, 4 );

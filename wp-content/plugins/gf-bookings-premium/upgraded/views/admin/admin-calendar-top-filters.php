<?php
/**
 * GFB Top Filter Admin Calendar View
 *
 * @package GFB.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>
<div class="main-reports--div">
    <div class="calander-content">
        <div class="calander-top-heading">
            <h2><?php echo esc_html__('Calendar', 'gfb'); ?></h2>
        </div>
        <div class="calander-top-loaction">
            <?php
            $all_locations = GFB_Location::all_location();
            if (is_array($all_locations) && count($all_locations) > 0) {
                ?>
                <select class="gfb_location">
                    <option value="all"><?php esc_html_e($gfb_settings['gfb_label_location']); ?></option>
                    <?php
                    foreach ($all_locations as $value) {
                        echo '<option value="' . esc_attr($value->ID) . '" ' . selected($value->ID, $location_val, false) . '>' . esc_attr($value->post_title) . '</option>';
                    }
                    ?>
                </select>
                <?php
            }
            ?>

            <?php
            $all_service = GFB_Service::all_service();
            if (is_array($all_service) && count($all_service) > 0) {
                ?>
                <select class="gfb_service">
                    <option value="all"><?php esc_html_e($gfb_settings['gfb_label_service']); ?></option>
                    <?php
                    foreach ($all_service as $value) {
                        echo '<option value="' . esc_attr($value->ID) . '" ' . selected($value->ID, $service_val, false) . '>' . esc_attr($value->post_title) . '</option>';
                    }
                    ?>
                </select>
                <?php
            }
            ?>

            <?php
            $all_staff = GFB_Staff::all_staff();
            if (is_array($all_staff) && count($all_staff) > 0) {
                ?>
                <select class="gfb_staff">
                    <option value="all"><?php esc_html_e($gfb_settings['gfb_label_staff']); ?></option>
                    <?php
                    foreach ($all_staff as $value) {
                        $user_id = get_post_meta($value->ID, 'wp_user', true);
                        $userdata = get_userdata($user_id);
                        if ($userdata) {
                            echo '<option value="' . esc_attr($value->ID) . '" ' . selected($value->ID, $staff_val, false) . '>' . esc_attr($value->post_title) . '</option>';
                        }
                    }
                    ?>
                </select>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="calander-slots-color">
        <ul>
            <li class="pending">Pending</li>
            <li class="confirmed">Confirmed</li>
            <li class="visited">Visited</li>
            <li class="cancelled">Cancelled</li>
        </ul>
    </div>

</div>
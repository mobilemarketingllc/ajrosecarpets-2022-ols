<?php
/**
 * GFB Calendar Admin View
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="gfb-cal-container">
    <h3>
        <?php
        // Set your timezone
        if ( isset($_POST['staff_id']) && 'all' != $_POST['staff_id'] ) {
            $show_timezone = gfb_timezone_string($staff_id);
        } else {
            $show_timezone = gfb_timezone_string();
        }
        ?>
        <span class="gfb-timezone"><?php echo sprintf('%1$s: %2$s', __('Timezone', 'gfb'), esc_attr($show_timezone) ); ?></span>
        <?php
        if ( $disabled_prev ) {
            if ( $prev >= $curr ) {
                ?>
                <a href="#" class="gfb-month-changer" data-month="<?php echo esc_attr($prev); ?>"><i class="fa fa-solid fa-caret-left"></i></a> 
                <?php
            }
        } else {
            ?>
            <a href="#" class="gfb-month-changer" data-month="<?php echo esc_attr($prev); ?>"><i class="fa fa-solid fa-caret-left"></i></a> 
            <?php
        }
        
        $cal_title = apply_filters('gfb_calendar_title', $html_title);
        echo esc_html( $cal_title );        

        if ( $disabled_next ) {
            if ( $next <= $prior_next ) {
                ?>
                <a href="#" class="gfb-month-changer" data-month="<?php echo esc_attr($next); ?>"><i class="fa fa-solid fa-caret-right"></i></a>
                <?php
            }
        } else {
            ?>
            <a href="#" class="gfb-month-changer" data-month="<?php echo esc_attr($next); ?>"><i class="fa fa-solid fa-caret-right"></i></a>
            <?php
        }
        ?>        
    </h3>
    <div class="gfb-table-container">        
        <table class="gfb-cal-table gfb-cal-table-bordered">
            <tr>
                <?php
                $weekNames = gfb_translable_name('weeks');
                if ( isset( get_option('gfb_settings', array())['general']['gbf_start_of_week'] ) && 'sunday' == get_option('gfb_settings', array())['general']['gbf_start_of_week'] ) {
                    ?>
                    <th><?php echo esc_html($weekNames['sunday']); ?></th>
                    <th><?php echo esc_html($weekNames['monday']); ?></th>
                    <th><?php echo esc_html($weekNames['tuesday']); ?></th>
                    <th><?php echo esc_html($weekNames['wednesday']); ?></th>
                    <th><?php echo esc_html($weekNames['thursday']); ?></th>
                    <th><?php echo esc_html($weekNames['friday']); ?></th>
                    <th><?php echo esc_html($weekNames['saturday']); ?></th>
                    <?php
                } else {
                    ?>                    
                    <th><?php echo esc_html($weekNames['monday']); ?></th>
                    <th><?php echo esc_html($weekNames['tuesday']); ?></th>
                    <th><?php echo esc_html($weekNames['wednesday']); ?></th>
                    <th><?php echo esc_html($weekNames['thursday']); ?></th>
                    <th><?php echo esc_html($weekNames['friday']); ?></th>
                    <th><?php echo esc_html($weekNames['saturday']); ?></th>
                    <th><?php echo esc_html($weekNames['sunday']); ?></th>
                    <?php
                }
                ?>                
            </tr>
            <?php
                foreach ($weeks as $week) {
                    echo $week;
                }
            ?>
        </table>        
    </div>
</div>

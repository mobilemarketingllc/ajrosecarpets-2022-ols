<?php
/**
 * GFB Calendar Appointment View
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$status = get_post_meta( $appointment_id, 'status', true );
$wp_user = get_post_meta( $appointment_id, 'wp_user', true );
$userdata = get_userdata($wp_user);
if ( $userdata ) {
    $user_first_name = $userdata->first_name;
} else {
    $user_first_name = '';
}
$staff_id = get_post_meta( $appointment_id, 'staff_id', true );
$staff_name = get_the_title( $staff_id );
$service_id = get_post_meta( $appointment_id, 'service_id', true );
$service_name = get_the_title($service_id);
$time = get_post_meta( $appointment_id, 'time', true );
$time = apply_filters('gfb_staff_time_slot', $time);
$fullday_or_custom = ! empty( get_post_meta($appointment_id, 'booking_type', true) ) ? get_post_meta($appointment_id, 'booking_type', true) : $time;
?>
<div class="gfb-appointment-block  gfb-tooltip">
    <span class="gfb-<?php echo esc_attr($status); ?>"><?php echo sprintf(__('App # %1$s', 'gfb'), $appointment_id); ?></span>
    <span class="gfb-tooltip-text">
        <?php echo sprintf(__('Customer: %1$s %2$s Service: %3$s %4$s Staff: %5$s %6$s Time: %7$s', 'gfb'), esc_attr($user_first_name), '<br/>', esc_attr($service_name), '<br/>', esc_attr($staff_name), '<br/>', esc_attr($fullday_or_custom) ); ?>
    </span>
</div>
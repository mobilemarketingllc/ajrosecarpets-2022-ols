<?php
/**
 * GFB Customer Appointment View Front-end
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<div class="gfb-appointment-filter-container">
    <div id="reportrange">
        <i class="fa fa-calendar"></i>&nbsp;
        <span></span> <i class="fa fa-caret-down"></i>
        <input type="hidden" name="date_from" readonly />
        <input type="hidden" name="date_to" readonly />
    </div>    

</div>
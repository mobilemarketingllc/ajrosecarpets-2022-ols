<?php
/**
 * GFB 4 Block Data Reports
 *
 * @package GFB.
 */

if ( is_user_logged_in() ) {
    if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
        return false;
    }
}
?>

<div class="main-reports--div">
    <div class="reports--content">
        <div class="top--heading">
            <h2><?php echo esc_html__('Reports', 'gfb'); ?></h2>
        </div>
    </div>
    <div class="reports-top-tabs">
        <div class="reports-tab-boxes">
            <div class="reports-tabs-sec">
                <ul class="reports-tab-list">
                    <li>
                    <span class="reports-tab-icon">
                        <img src="<?php echo GFB_IMAGE_URL; ?>customer.png" alt="">
                    </span>
                    <span class="reports-tab-contents">
                        <h6><?php echo esc_html__('Total Customers', 'gfb'); ?></h6>
                        <h3><?php echo wp_count_posts('gfb-customer')->publish; ?></h3>
                    </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="reports-tab-boxes">
            <div class="reports-tabs-sec">
                <ul class="reports-tab-list">
                    <li>
                    <span class="reports-tab-icon">
                        <img src="<?php echo GFB_IMAGE_URL; ?>services.png" alt="">
                    </span>
                        <span class="reports-tab-contents">
                        <h6><?php echo esc_html__('Total Services', 'gfb'); ?></h6>
                        <h3><?php echo wp_count_posts('gfb-service')->publish; ?></h3>
                    </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="reports-tab-boxes">
            <div class="reports-tabs-sec">
                <ul class="reports-tab-list">
                    <li>
                    <span class="reports-tab-icon">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Stuff.png" alt="">
                    </span>
                        <span class="reports-tab-contents">
                        <h6><?php echo esc_html__('Total Staff', 'gfb'); ?></h6>
                        <h3><?php echo wp_count_posts('gfb-staff')->publish; ?></h3>
                    </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="reports-tab-boxes" >
            <div class="reports-tabs-sec">
                <ul class="reports-tab-list">
                    <li>
                    <span class="reports-tab-icon">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Today-Collection.png" alt="">
                    </span>
                        <span class="reports-tab-contents">
                        <h6><?php echo esc_html__('Total Collection', 'gfb'); ?></h6>
                        <h3><?php gfb_price_format(GFB_Appointment::gfb_total_collection(), true, true); ?></h3>
                    </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * GFB Email Template View
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

global $gfbEmail;
?>
<div class="main-reports--div">
    <div class="reports--content">
        <div class="top--heading">
            <h2><?php echo esc_html__('Email Notification', 'gfb'); ?></h2>
        </div>
    </div>
    <div class="gfb_wrap">        

		<div class="gfb_maintab-content">
        	<h4 class="gfb_section-title"><?php _e('Codes used for email notification', 'gfb'); ?></h4>
            
            <table class="gfb_maintable_codes">
            	<tbody>
                	<tr>
						<td style="width:33%;">
                        	<table class="gfb_maintable_codes">
							<?php 
                            $codes = $gfbEmail->gfb_codes(); 
                            $index = 0;                                
                            foreach($codes as $code => $desc){
								
								if ($index > 0 and $index % 5 == 0) {
									echo '</table></td>';
									echo '<td style="width:33%;"><table class="gfb_maintable_codes">';
								}
								
								echo '<tr>';
                                    echo '<td><strong>'.$code.'</strong></td>';
                                    echo '<td>'.$desc.'</td>';
                                echo '</tr>';
                                $index++;
                            }
                            ?>
                            </table>
						</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br />
        <div class="gfb_maintab-content">
        	<form name="app_notification_form" id="app_notification_form" method="post">        		

        		<div id="accordion">

        			<?php
        			foreach( GFB_Email::$email_templates as $template ) {
        				?>
        			
	        			<h4 class="gfb_section-title"><?php esc_html_e($template['title']); ?></h4>
	        			<div>
	        				<div class="form-section">
			                    <div class="form-group-elements">
			                    	<label><?php echo esc_html__('Enable Template', 'gfb'); ?></label>
					                <input type="radio" name="switch_<?php esc_attr_e( $template['id'] ); ?>" id="switch_enabled" value="enabled" <?php checked('enabled', $template['switch'], true); ?> /> <?php esc_html_e('Enable', 'gfb'); ?>&nbsp;&nbsp;
					                <input type="radio" name="switch_<?php esc_attr_e( $template['id'] ); ?>" id="switch_disabled" value="disabled" <?php checked('disabled', $template['switch'], true); ?> />  <?php esc_html_e('Disable', 'gfb'); ?>
			                    </div>

			                    <div class="form-group-elements">
			                        <label class=""><?php _e('Subject', 'gfb'); ?></label>
			                        <input type="text" name="subject_<?php esc_attr_e( $template['id'] ); ?>" id="subject_<?php esc_attr_e( $template['id'] ); ?>" class="form-control" value="<?php esc_attr_e( $template['subject'] ); ?>" />
			                    </div>
			                    
			                    <div class="form-group-elements">
			                        <label class=""><?php _e('Message', 'gfb'); ?></label>
			                        <?php    
			                            $content = $template['message'];
			                            $editor_id = 'message_' . esc_attr( $template['id'] );
			                            $name = 'message_' . esc_attr( $template['id'] );
			                            
			                            $settings = array(
			                                'textarea_name' => $name,
			                                'media_buttons' => false,
			                                'editor_height' => 384,
			                            );
			                                  
			                            wp_editor( $content, $editor_id, $settings );                        
			                        ?>
			                    </div>		                
			                </div>        				
	        			</div>
	        			<?php
	        		}
	        		?>
        		</div>

        		<div class="form-group-elements">
			    	<div class="form-element email-notify-submit">
			        	<?php submit_button("Save Notifications"); ?>
			        </div>
			    </div>

        	</form>        	
        </div>
       
	</div>
</div>

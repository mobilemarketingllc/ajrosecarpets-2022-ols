<?php
// echo '<pre>$all_fields$all_fields';
// print_r($all_fields);
// echo '</pre>';
?>

<?php if (!empty($_POST)): ?>
    <div id="after_update_dialog" class="updated below-h2">
        <p>
            <strong><?php _e('GFB settings updated successfully.', 'gfb'); ?></strong>
        </p>
    </div>
<?php endif; ?>

<h3><span><i class="fa fa-cogs"></i> <?php _e('Gravity Booking Settings', 'gfb'); ?></span></h3>

<p><?php _e('Connect form fields to register customer when form is submitted. This is mandatory to set this setting, otherwise appointment will not create.', 'gfb'); ?></p>

<?php
if ( is_array($all_fields) && count($all_fields) > 0 ) {	
	?>
	<form method="post" class="modal--form">
		<input type="hidden" value="<?php echo $form_id; ?>" name="gfb_form_id"/>

		<div class="form-col-full form-col-max-25">
	        <label for="gfb-fname"><?php echo esc_html__('First Name*', 'gfb'); ?></label>
	        <select name="fname" id="gfb-fname">
	        	<option value=""><?php _e( '--Select field--', 'gfb' ); ?></option>
	        	<?php
	        	foreach ( $all_fields as $field ) {
	        		echo '<option value="' . esc_attr( $field['value'] ) . '" ' . selected($settings['registration_field']['fname'], $field['value'], false) . '>' . esc_attr( $field['label'] ) . '</option>';
	        	}
	        	?>
	        </select>
	    </div>

	    <div class="form-col-full form-col-max-25">
	        <label for="gfb-lname"><?php echo esc_html__('Last Name', 'gfb'); ?></label>
	        <select name="lname" id="gfb-lname">
	        	<option value=""><?php _e( '--Select field--', 'gfb' ); ?></option>
	        	<?php
	        	foreach ( $all_fields as $field ) {
	        		echo '<option value="' . esc_attr( $field['value'] ) . '" ' . selected($settings['registration_field']['lname'], $field['value'], false) . '>' . esc_attr( $field['label'] ) . '</option>';
	        	}
	        	?>
	        </select>
	    </div>

	    <div class="form-col-full form-col-max-25">
	        <label for="gfb-email"><?php echo esc_html__('Email*', 'gfb'); ?></label>
	        <select name="email" id="gfb-email">
	        	<option value=""><?php _e( '--Select field--', 'gfb' ); ?></option>
	        	<?php
	        	foreach ( $all_fields as $field ) {
	        		echo '<option value="' . esc_attr( $field['value'] ) . '" ' . selected($settings['registration_field']['email'], $field['value'], false) . '>' . esc_attr( $field['label'] ) . '</option>';
	        	}
	        	?>
	        </select>
	    </div>

	    <div class="form-col-full form-col-max-25">
	        <label for="gfb-phone"><?php echo esc_html__('Phone', 'gfb'); ?></label>
	        <select name="phone" id="gfb-phone">
	        	<option value=""><?php _e( '--Select field--', 'gfb' ); ?></option>
	        	<?php
	        	foreach ( $all_fields as $field ) {
	        		echo '<option value="' . esc_attr( $field['value'] ) . '" ' . selected($settings['registration_field']['phone'], $field['value'], false) . '>' . esc_attr( $field['label'] ) . '</option>';
	        	}
	        	?>
	        </select>
	    </div>

	    <input type="submit" name="gfb_setting" class="form-col-max-25 save--modal" value="Update Setting"/>
	</form>
	<?php
} else {
	esc_html_e('Create form first then you will be able to see settings.', 'gfb');
}

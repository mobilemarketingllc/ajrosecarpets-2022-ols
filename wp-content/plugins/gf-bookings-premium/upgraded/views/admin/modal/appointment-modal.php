<?php
/**
 * GFB Appointment Modal
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
//Will continue tomorrow from here 02 OCT 2022
?>

<div id="gfb-appointment-modal" class="modal">
    <div class="modal__content">
        <h2><?php echo esc_html__('Appointment Details', 'gfb'); ?></h2>
        <table>
            <thead>
                <tr>
                    <th><?php echo esc_html__('Booking ID', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Location', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Service', 'gfb'); ?></th>                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="gfb-appointment_id"></td>
                    <td id="gfb-location_name"></td>
                    <td id="gfb-service_name"></td>
                </tr>
            </tbody>
        </table>

        <table>
            <thead>
                <tr>
                    <th><?php echo esc_html__('Staff', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Customer', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Date, Time', 'gfb'); ?></th>                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="gfb-staff_name"></td>
                    <td id="gfb-customer_name"></td>
                    <td id="gfb-schedule"></td>
                </tr>
            </tbody>
        </table>

        <table>
            <thead>
                <tr>
                    <th><?php echo esc_html__('No. of Slot', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Cost', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Booking Type', 'gfb'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="gfb-slot"></td>
                    <td id="gfb-cost"></td>
                    <td id="gfb-booking_type"></td>
                </tr>
            </tbody>
        </table>

        <table>
            <thead>
                <tr>
                    <th><?php echo esc_html__('Form ID', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Entry ID', 'gfb'); ?></th>
                    <th><?php echo esc_html__('Status', 'gfb'); ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="gfb-form_id"></td>
                    <td id="gfb-entry_id"></td>
                    <td id="gfb-status"></td>
                </tr>
            </tbody>
        </table>

        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

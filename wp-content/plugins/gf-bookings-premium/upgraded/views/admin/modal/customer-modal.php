<?php
/**
 * GFB Customer Staff
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div id="gfb-customer-modal" class="modal">
    <div class="modal__content gfb_fancy_scroll">
        <h2><?php echo esc_html__('Customer Details', 'gfb'); ?></h2>
        <form id="gfb-customer-form" autocomplete="off" class="modal--form">
            <input type="hidden" class="post_id" name="post_id" value="" />
            <div class="form-col-one">
                <label for="gfb-fname"><?php echo esc_html__('First Name*', 'gfb'); ?></label>
                <input type="text" id="gfb-fname" name="fname" required>
            </div>
            <div class="form-col-two">
                <label for="gfb-lname"><?php echo esc_html__('Last Name*', 'gfb'); ?></label>
                <input type="text" id="gfb-lname" name="lname" required>
            </div>
            <div class="form-col-one">
                <label for="gfb-phone"><?php echo esc_html__('Phone', 'gfb'); ?></label>
                <input type="tel" id="gfb-phone" name="phone">
            </div>
            <div class="form-col-two">
                <label for="gfb-email"><?php echo esc_html__('Email*', 'gfb'); ?></label>
                <input type="email" id="gfb-email" name="email" required>
            </div>
            <div class="form-col-one">
                <label for="gfb-gender"><?php echo esc_html__('Gender', 'gfb'); ?></label>
                <select name="gender" id="gfb-gender">
                    <option value="no"><?php echo esc_html__('Prefer not to respond', 'gfb'); ?></option>
                    <option value="male"><?php echo esc_html__('Male', 'gfb'); ?></option>
                    <option value="female"><?php echo esc_html__('Female', 'gfb'); ?></option>
                </select>
            </div>
            <div class="form-col-two">
                <label for="gfb-dob"><?php echo esc_html__('Date of Birth', 'gfb'); ?></label>
                <input type="date" id="gfb-dob" name="dob">
            </div>
            <div class="form-col-full">
                <label for="password"><?php echo esc_html__('Password', 'gfb'); ?></label>
                <input type="password" id="gfb-password" name="password">
            </div>
            <div class="form-col-full">
                <label for="info"><?php echo esc_html__('Note', 'gfb'); ?></label>
                <textarea id="gfb-info" name="info"></textarea>
            </div>

            <input class="save--modal" type="submit" value="Save">

            <div class="gfb-response"></div>
        </form>

        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

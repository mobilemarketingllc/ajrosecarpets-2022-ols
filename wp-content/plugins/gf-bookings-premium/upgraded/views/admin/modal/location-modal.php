<?php
/**
 * GFB Location Modal
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div id="gfb-location-modal" class="modal">
    <div class="modal__content gfb_fancy_scroll">
        <h2><?php echo esc_html__('Location Details', 'gfb'); ?></h2>
        <form id="gfb-location-form" autocomplete="off" class="modal--form">
            <input type="hidden" class="post_id" name="post_id" value="" />
            <div class="form-col-one">
                <label for="fname"><?php echo esc_html__('Name*', 'gfb'); ?></label>
                <input type="text" id="gfb-name" name="name" required>
            </div>
            <div class="form-col-two">
                <label for="phone"><?php echo esc_html__('Phone', 'gfb'); ?></label>
                <input type="tel" id="gfb-phone" name="phone" >
            </div>            

            <div class="form-col-full">
                <label for="gfb-address"><?php echo esc_html__('Address', 'gfb'); ?></label>
                <textarea id="gfb-address" name="address" ></textarea>
            </div>

            <div class="form-col-full">
                <label for="description"><?php echo esc_html__('Description', 'gfb'); ?></label>
                <textarea id="gfb-description" name="description"></textarea>
            </div>

            <input class="save--modal" type="submit" value="Save">

            <div class="gfb-response"></div>
        </form>

        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

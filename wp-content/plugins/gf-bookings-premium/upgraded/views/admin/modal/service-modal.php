<?php
/**
 * GFB Service Modal
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div id="gfb-service-modal" class="modal">
    <div class="modal__content gfb_fancy_scroll">
        <h2><?php echo esc_html__('Service Details', 'gfb'); ?></h2>
        <form id="gfb-service-form" autocomplete="off" class="modal--form">
            <input type="hidden" class="post_id" name="post_id" value="" />
            <div class="form-col-full">
                <label for="gfb-name"><?php echo esc_html__('Service Name*', 'gfb'); ?></label>
                <input type="text" id="gfb-name" name="name" required>
            </div>            
            <div class="form-col-full">
                <label for="gfb-price"><?php echo esc_html__('Service Price', 'gfb'); ?></label>
                <input type="text" pattern="^\d*(\.\d{0,2})?$" class="gfb-number" id="gfb-price" name="price">
            </div>
            <div class="form-col-full">
                <label for="gfb-category"><?php echo esc_html__('Service Category*', 'gfb'); ?></label>
                <select name="category" id="gfb-category" required>
                    <option value=""><?php echo esc_html__('Select a Category', 'gfb'); ?></option>
                    <?php
                    $service_categories = GFB_Service::all_categories();

                    if ( is_array($service_categories) && count($service_categories) > 0 ) {
                        foreach( $service_categories as $service_category ) {
                            echo '<option value="' . $service_category->term_id . '">' . $service_category->name . '</option>';
                        }                        
                    }
                    ?>
                </select>
            </div>

            <?php 
            $all_location = GFB_Location::all_location();

            if ( is_array($all_location) && count($all_location) > 0 ) {
                ?>
                <div class="form-col-full">
                    <label for="location"><?php echo esc_html__('Location', 'gfb'); ?></label>
                    <select name="location[]" id="gfb-location" multiple>
                        <?php
                        foreach ( $all_location as $location ) {
                            echo '<option value="' . esc_attr( $location->ID ) . '">' . $location->post_title . '</option>';
                        }
                        ?>                        
                    </select>
                </div>
                <?php
            }
            ?>

            <input class="save--modal" type="submit" value="Save">

            <div class="gfb-response"></div>
        </form>

        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

<?php
/**
 * GFB Staff Modal
 *
 * @package GFB.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<?php //delete_post_meta( 3509, 'gfb_staff_holidays' ); 
//echo '<pre>gfb_staff_dayoffgfb_staff_dayoff'; print_r(get_post_meta(3509, 'gfb_staff_holidays', true)); echo '</pre>'; ?>
<div id="gfb-staff-modal" class="modal">
    <div class="modal__content gfb_fancy_scroll">
        <div class="staff_tabs">
            <ul class="tabs">
                <li class="active"><a href="#details"><?php echo esc_html__('Details', 'gfb'); ?></a></li>
                <li><a href="#services"><?php echo esc_html__('Services', 'gfb'); ?></a></li>
                <li><a href="#timings"><?php echo esc_html__('Timings', 'gfb'); ?></a></li>
                <li><a href="#add_holidays"><?php echo esc_html__('Add Holidays', 'gfb'); ?></a></li>
                <li><a href="#remove_holidays"><?php echo esc_html__('Remove Holidays', 'gfb'); ?></a></li>
            </ul>
            <div class="tabsContainer">
                <div class="tabWrapper">                    
                    <div id="details" class="tabContent">
                        <form id="gfb-staff-form-details" autocomplete="off" class="modal--form">
                            <input type="hidden" class="post_id" name="post_id" value=""/>
                            <div class="form-col-one">
                                <label for="fname"><?php echo esc_html__('First Name*', 'gfb'); ?></label>
                                <input type="text" id="gfb-fname" name="fname" value="" required>
                            </div>
                            <div class="form-col-two">
                                <label for="lname"><?php echo esc_html__('Last Name*', 'gfb'); ?></label>
                                <input type="text" id="gfb-lname" name="lname" value="" required>
                            </div>
                            <div class="form-col-one">
                                <label for="email"><?php echo esc_html__('Email*', 'gfb'); ?></label>
                                <input type="email" id="gfb-email" name="email" value="" required>
                            </div>
                            <div class="form-col-two">
                                <label for="password"><?php echo esc_html__('Password', 'gfb'); ?></label>
                                <input type="password" id="gfb-password" name="password" value="">
                            </div>
                            <div class="form-col-one">
                                <label for="phone"><?php echo esc_html__('Phone', 'gfb'); ?></label>
                                <input type="tel" id="gfb-phone" name="phone" value="">
                            </div>
                            <div class="form-col-two">
                                <label for="profession"><?php echo esc_html__('Profession', 'gfb'); ?></label>
                                <input type="text" id="gfb-profession" name="profession" value="">
                            </div>

                            <?php
                            $all_location = GFB_Location::all_location();

                            if (is_array($all_location) && count($all_location) > 0) {
                                ?>
                                <div class="form-col-full">
                                    <label for="location"><?php echo esc_html__('Location', 'gfb'); ?></label>
                                    <select name="location[]" id="gfb-location" multiple>
                                        <?php
                                        foreach ($all_location as $location) {
                                            echo '<option value="' . esc_attr($location->ID) . '">' . $location->post_title . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            ?>

                            <label for="info"><?php echo esc_html__('Note', 'gfb'); ?></label>
                            <textarea id="gfb-info" name="info"></textarea>

                            <input class="save--modal" type="submit" value="Save">

                            <div class="gfb-response"></div>
                        </form>
                    </div>
                    <div id="services" class="tabContent">

                        <?php 
                        // echo '<pre>all_categoriesall_categories';
                        // print_r(GFB_Service::all_categories());
                        // echo '</pre>';                        

                        $categories = GFB_Service::all_categories();
                        
                        if ( is_array( $categories ) && count($categories) > 0 ) {
                            ?>
                            <form id="gfb-staff-form-services" autocomplete="off" class="modal--form">
                                <input type="hidden" class="post_id" name="post_id" value=""/>
                                <?php
                                foreach( $categories as $cat ) {
                                    if ( 0 < $cat->category_count ) {
                                        ?>
                                        <label for="<?php echo esc_attr($cat->slug); ?>" class="gfb-cat-title"><?php echo esc_attr($cat->name); ?></label>
                                        <div class="gfb-timer">
                                            <?php
                                            $services = GFB_Service::get_services_by_cat($cat->term_id);
                                            // echo '<pre>servicesservices' . $cat->cat_ID;
                                            // print_r($services);
                                            // echo '</pre>';
                                            if ( is_array( $services ) && count($services) > 0 ) {
                                                foreach( $services as $service ) {
                                                    $service_price = get_post_meta($service->ID, 'price', true);
                                                    ?>
                                                    <div class="gfb-timer-box">
                                                        <div class="gfb-timer-col-row">
                                                            <div class="timer-box-col-one">
                                                                <label for="root-canal"><?php echo esc_attr($service->post_title); ?></label>
                                                                <input type="checkbox" id="gfb_staff_service_aval_<?php echo esc_attr($service->ID); ?>" name="gfb_staff_service[<?php echo esc_attr($service->ID); ?>][available]" value="yes" />
                                                            </div>
                                                            <div class="timer-box-col-two">
                                                                <?php
                                                                if ( current_user_can( 'gfb-staff' ) && ! current_user_can( 'administrator' ) ) {
                                                                    $gfb_settings = GFB_Setting::get_settings();
                                                                    if ( isset($gfb_settings['gfb_price_policy']) && 'staff' == $gfb_settings['gfb_price_policy'] ) {
                                                                        ?>
                                                                        <input type="text" class="gfb-number" pattern="^\d*(\.\d{0,2})?$" id="gfb_staff_service_price_<?php echo esc_attr($service->ID); ?>" name="gfb_staff_service[<?php echo esc_attr($service->ID); ?>][price]" value="" placeholder="<?php echo esc_attr(gfb_price_format($service_price, false)); ?>" />
                                                                        <?php /* Translators: %1$s is currency symbol */?>
                                                                        <small><?php echo sprintf(__('(in %1$s)', 'gfb'), gfb_currency_symbol()); ?></small>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <input type="text" class="gfb-number" pattern="^\d*(\.\d{0,2})?$" id="gfb_staff_service_price_<?php echo esc_attr($service->ID); ?>" value="" placeholder="<?php echo esc_attr(gfb_price_format($service_price, false)); ?>" disabled />
                                                                        <?php /* Translators: %1$s is currency symbol */?>
                                                                        <small><?php echo sprintf(__('(in %1$s)', 'gfb'), gfb_currency_symbol()); ?></small>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <input type="text" class="gfb-number" pattern="^\d*(\.\d{0,2})?$" id="gfb_staff_service_price_<?php echo esc_attr($service->ID); ?>" name="gfb_staff_service[<?php echo esc_attr($service->ID); ?>][price]" value="" placeholder="<?php echo esc_attr(gfb_price_format($service_price, false)); ?>" />
                                                                    <?php /* Translators: %1$s is currency symbol */?>
                                                                    <small><?php echo sprintf(__('(in %1$s)', 'gfb'), gfb_currency_symbol()); ?></small>
                                                                    <?php                                                                    
                                                                }
                                                                ?>                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>                                
                                <input class="save--modal" type="submit" value="Save">
                                <div class="gfb-response"></div>
                            </form>
                            <?php
                        } else {
                            echo esc_html__('No Services found.', 'gfb');
                        }
                        ?>
                    </div>
                    <div id="timings" class="tabContent">
                        <?php 
                        $general_settings = GFB_Setting::get_settings();
                        // echo '<pre>$general_settings$general_settings';
                        // print_r($general_settings);
                        // echo '</pre>';

                        // echo gfb_timezone_string();
                        ?>
                        <form id="gfb-staff-form-timings" autocomplete="off" class="modal--form">
                            <input type="hidden" class="post_id" name="post_id" value=""/>
                            <div class="form-col-full">
                                <label for="gfb-timezone"><?php echo esc_html__('Select Time Zone', 'gfb'); ?></label>
                                <select name="gfb-timezone" id="gfb-timezone">
                                    <?php echo wp_timezone_choice(''); ?>
                                </select>
                            </div>
                            <div class="form-col-full">
                                <label for="gfb-booking_type"><?php echo esc_html__('Booking Type', 'gfb'); ?></label>
                                <select name="booking_type" id="gfb-booking_type">                                    
                                    <option value="custom"><?php echo esc_html__('Custom Slots', 'gfb'); ?></option>
                                    <option value="fullday"><?php echo esc_html__('Full Day', 'gfb'); ?></option>
                                </select>
                                <small><?php esc_html_e('If Full day is selected, Time slots will be ignored.', 'gfb'); ?></small>
                            </div>
                            <div class="form-col-full">
                                <label for="gfb-duration"><?php echo esc_html__('Time Slot Duration', 'gfb'); ?></label>
                                <input type="number" id="gfb-duration" name="gfb-time-slot-duration" min="1" data-general="<?php echo esc_attr( $general_settings['gbf_time_duration'] ); ?>" placeholder="<?php echo esc_attr( $general_settings['gbf_time_duration'] ); ?>">
                                <small><?php esc_html_e('Each time slot duration. Ex: 180 = 3 hours.  Default is set as general settings.', 'gfb'); ?></small>
                            </div>
                            <div class="form-col-full">
                                <label for="gfb-interval"><?php echo esc_html__('Time Slot Interval', 'gfb'); ?></label>
                                <input type="number" id="gfb-interval" name="gfb-time-slot-interval" min="0" data-general="<?php echo esc_attr( $general_settings['gbf_time_interval'] ); ?>" placeholder="<?php echo esc_attr( $general_settings['gbf_time_interval'] ); ?>">
                                <small><?php esc_html_e('Gap between each time slots. Ex: 60 = 1 hour. Default is set as general settings.', 'gfb'); ?></small>
                            </div>                          
                            <div class="form-time-slot-sec">
                                <label for="add-time-slot"><?php echo esc_html__('Add Time Slot', 'gfb'); ?></label>
                                <div class="form-time-slot-box">
                                    <div class="form-time-slots-title-row">
                                        <div class="form-time-slots-title">
                                            <span><?php echo esc_html__('Start Time', 'gfb'); ?></span>
                                        </div>
                                        <div class="form-time-slots-title">
                                            <span><?php echo esc_html__('End Time', 'gfb'); ?></span>
                                        </div>
                                        <div class="form-time-slots-title">
                                            <span><?php echo esc_html__('Slot Capacity', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <!-- MONDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_mon_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_mon_switch" name="gfb_staff_timing[mon][switch]" value="1" />
                                            <span><?php echo esc_html__('Mon', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[mon][start]" id="gfb_staff_timing_mon_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[mon][end]" id="gfb_staff_timing_mon_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[mon][capacity]" id="gfb_staff_timing_mon_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="mon"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-mon" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[mon][break][]" id="gfb_staff_timing_mon_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                    <!-- TUESDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_tue_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_tue_switch" name="gfb_staff_timing[tue][switch]" value="1" />
                                            <span><?php echo esc_html__('Tue', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[tue][start]" id="gfb_staff_timing_tue_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[tue][end]" id="gfb_staff_timing_tue_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[tue][capacity]" id="gfb_staff_timing_tue_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="tue"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-tue" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[tue][break][]" id="gfb_staff_timing_tue_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                    <!-- WEDNESDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_wed_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_wed_switch" name="gfb_staff_timing[wed][switch]" value="1" />
                                            <span><?php echo esc_html__('Wed', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[wed][start]" id="gfb_staff_timing_wed_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[wed][end]" id="gfb_staff_timing_wed_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[wed][capacity]" id="gfb_staff_timing_wed_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="wed"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-wed" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[wed][break][]" id="gfb_staff_timing_wed_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                    <!-- THURSDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_thu_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_thu_switch" name="gfb_staff_timing[thu][switch]" value="1" />
                                            <span><?php echo esc_html__('Thu', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[thu][start]" id="gfb_staff_timing_thu_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[thu][end]" id="gfb_staff_timing_thu_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[thu][capacity]" id="gfb_staff_timing_thu_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="thu"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-thu" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[thu][break][]" id="gfb_staff_timing_thu_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                    <!-- FRIDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_fri_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_fri_switch" name="gfb_staff_timing[fri][switch]" value="1" />
                                            <span><?php echo esc_html__('Fri', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[fri][start]" id="gfb_staff_timing_fri_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[fri][end]" id="gfb_staff_timing_fri_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[fri][capacity]" id="gfb_staff_timing_fri_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="fri"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-fri" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[fri][break][]" id="gfb_staff_timing_fri_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                    <!-- SATURDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_sat_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_sat_switch" name="gfb_staff_timing[sat][switch]" value="1" />
                                            <span><?php echo esc_html__('Sat', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[sat][start]" id="gfb_staff_timing_sat_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[sat][end]" id="gfb_staff_timing_sat_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[sat][capacity]" id="gfb_staff_timing_sat_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="sat"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-sat" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[sat][break][]" id="gfb_staff_timing_sat_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                    <!-- SUNDAY -->
                                    <div class="form-time-slots-content-row">
                                        <label class="form-time-slots-days" for="gfb_staff_timing_sun_switch">
                                            <input type="checkbox" class="gfb_staff_time_toggle" id="gfb_staff_timing_sun_switch" name="gfb_staff_timing[sun][switch]" value="1" />
                                            <span><?php echo esc_html__('Sun', 'gfb'); ?></span>
                                        </label>
                                        <div class="form-start-time-slots">
                                            <input type="time" name="gfb_staff_timing[sun][start]" id="gfb_staff_timing_sun_start">
                                        </div>
                                        <div class="form-end-time-slots">
                                            <input type="time" name="gfb_staff_timing[sun][end]" id="gfb_staff_timing_sun_end">
                                        </div>
                                        <div class="form-slots-capacity">
                                            <input type="number" name="gfb_staff_timing[sun][capacity]" id="gfb_staff_timing_sun_capacity" min="0">
                                        </div>
                                        <div class="add-break-btn">
                                            <span data-id="sun"><i class="fa fa-plus-circle"></i> <?php echo esc_html__('Add Break', 'gfb'); ?></span>
                                        </div>
                                    </div>
                                    <div id="break-sun" class="form-time-slots-break-box">
                                        <div class="form-col-full">
                                            <label for="add-break"><?php echo esc_html__('Add Break', 'gfb'); ?></label>
                                            <select name="gfb_staff_timing[sun][break][]" id="gfb_staff_timing_sun_break" class="gfb-slot-break" multiple></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input class="save--modal" type="submit" value="Save">
                            <div class="gfb-response"></div>
                        </form>
                    </div>
                    <div id="add_holidays" class="tabContent">                        
                        <form id="gfb-staff-form-add-holidays" autocomplete="off" class="modal--form">
                            <input type="hidden" class="post_id" name="post_id" value=""/>
                            <div class="datepicker_box">
                                <input type="hidden" name="gfb_staff_holidays" id="datepicker">
                            </div>
                            <!-- <input class="save--modal" type="submit" value="Save"> -->
                            <div class="gfb-response"></div>
                        </form>

                    </div>
                    <div id="remove_holidays" class="tabContent">                        
                        <form id="gfb-staff-form-remove-holidays" autocomplete="off" class="modal--form">
                            <input type="hidden" class="post_id" name="post_id" value=""/>
                            <div class="datepicker_box">
                                <input type="hidden" name="gfb_staff_holidays" id="datepicker-2">
                            </div>
                            <!-- <input class="save--modal" type="submit" value="Save"> -->
                            <div class="gfb-response"></div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

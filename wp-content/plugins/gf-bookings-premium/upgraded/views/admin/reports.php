<?php
/**
 * GFB Reports View
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

$gfb_reports = get_option('gfb_reports', array());

?>

<div class="container">
    <div class="gf_booking_overlayDiv"></div>
    <?php
    //add 4 block on top of the page.
    require_once GFB_ADMIN_VIEW_PATH . 'block_data_reports.php';
    ?>
    <div class="reports-graph-div">
        <div class="graph-container">
            <div class="graph-rows">                
                <div class="graphs-column graphs-column-half" style="width: 50%">
                    <div class="graph-column-box">
                        <div class="graph-box-sec">
                            <div class="graph-box-upper">
                                <p><?php echo esc_html__('Appointment Status Chart', 'gfb'); ?></p>
                            </div>
                            <div id="gfb-overlay" style="position: absolute;"></div>
                            <canvas id="graph-1" class="graph-box-middle"></canvas>
                        </div>
                    </div>
                </div>
                <div class="graphs-column graphs-column-half" style="width: 50%">
                    <div class="graph-column-box">
                        <div class="graph-box-sec">
                            <div class="graph-box-upper">
                                <p><?php echo esc_html__('Staff Wise Earning', 'gfb'); ?></p>
                            </div>
                            <div id="gfb-overlay" style="position: absolute;"></div>
                            <canvas id="graph-2" class="graph-box-middle"></canvas>
                        </div>
                    </div>
                </div>
                <div class="graphs-column graphs-column-half" style="width: 50%">
                    <div class="graph-column-box">
                        <div class="graph-box-sec">
                            <div class="graph-box-upper">
                                <p><?php echo esc_html__('Staff Wise Total no. of Appointments', 'gfb'); ?></p>
                            </div>
                            <div id="gfb-overlay" style="position: absolute;"></div>
                            <canvas id="graph-3" class="graph-box-middle"></canvas>
                        </div>
                    </div>
                </div>
                <div class="graphs-column graphs-column-half" style="width: 50%">
                    <div class="graph-column-box">
                        <div class="graph-box-sec">
                            <div class="graph-box-upper">
                                <p><?php echo esc_html__('Service Wise Total Earning', 'gfb'); ?></p>
                            </div>
                            <div id="gfb-overlay" style="position: absolute;"></div>
                            <canvas id="graph-4" class="graph-box-middle"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

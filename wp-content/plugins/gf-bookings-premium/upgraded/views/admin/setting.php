<?php
/**
 * GFB Setting View
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

$gfb_settings = get_option('gfb_settings', array());

$gfb_price_policy = isset($gfb_settings['general']['gfb_price_policy']) ? $gfb_settings['general']['gfb_price_policy'] : '';

$gfb_appointment_policy = isset($gfb_settings['general']['gfb_appointment_policy']) ? $gfb_settings['general']['gfb_appointment_policy'] : '';

$gbf_start_of_week = isset($gfb_settings['general']['gbf_start_of_week']) ? $gfb_settings['general']['gbf_start_of_week'] : '';

$gbf_time_duration = isset($gfb_settings['general']['gbf_time_duration']) ? $gfb_settings['general']['gbf_time_duration'] : '';

$gbf_time_interval = isset($gfb_settings['general']['gbf_time_interval']) ? $gfb_settings['general']['gbf_time_interval'] : '';

$gfb_calendar_timeslot_format = isset($gfb_settings['general']['gfb_calendar_timeslot_format']) ? $gfb_settings['general']['gfb_calendar_timeslot_format'] : '';

$gbf_show_timeslot = isset($gfb_settings['general']['gbf_show_timeslot']) ? $gfb_settings['general']['gbf_show_timeslot'] : '';

$gfb_prior_months_book_appointment = isset($gfb_settings['general']['gfb_prior_months_book_appointment']) ? $gfb_settings['general']['gfb_prior_months_book_appointment'] : '';

$gfb_cal_gradient_color_top = isset($gfb_settings['display']['gfb_cal_gradient_color_top']) ? $gfb_settings['display']['gfb_cal_gradient_color_top'] : '#f1f3f6';

$gfb_cal_gradient_color_bottom = isset($gfb_settings['display']['gfb_cal_gradient_color_bottom']) ? $gfb_settings['display']['gfb_cal_gradient_color_bottom'] : '#f1f3f6';

$gfb_cal_border_color = isset($gfb_settings['display']['gfb_cal_border_color']) ? $gfb_settings['display']['gfb_cal_border_color'] : '#26547c21';

$gfb_cal_header_bg_color = isset($gfb_settings['display']['gfb_cal_header_bg_color']) ? $gfb_settings['display']['gfb_cal_header_bg_color'] : '#7C9299';

$gfb_cal_header_text_color = isset($gfb_settings['display']['gfb_cal_header_text_color']) ? $gfb_settings['display']['gfb_cal_header_text_color'] : '#FFFFFF';

$gfb_cal_text_color = isset($gfb_settings['display']['gfb_cal_text_color']) ? $gfb_settings['display']['gfb_cal_text_color'] : '#6D8298';

$gfb_cal_selected_text_color = isset($gfb_settings['display']['gfb_cal_selected_text_color']) ? $gfb_settings['display']['gfb_cal_selected_text_color'] : '#FFFFFF';

$gfb_cal_selected_text_bg_color = isset($gfb_settings['display']['gfb_cal_selected_text_bg_color']) ? $gfb_settings['display']['gfb_cal_selected_text_bg_color'] : '#00D0AC';

$gfb_cal_available_days_text_color = isset($gfb_settings['display']['gfb_cal_available_days_text_color']) ? $gfb_settings['display']['gfb_cal_available_days_text_color'] : '#6D8298';

$gfb_cal_available_days_bg_color = isset($gfb_settings['display']['gfb_cal_available_days_bg_color']) ? $gfb_settings['display']['gfb_cal_available_days_bg_color'] : '#FFFFFF';

$gfb_cal_holiday_days_text_color = isset($gfb_settings['display']['gfb_cal_holiday_days_text_color']) ? $gfb_settings['display']['gfb_cal_holiday_days_text_color'] : '#6D8298';

$gfb_cal_holiday_days_bg_color = isset($gfb_settings['display']['gfb_cal_holiday_days_bg_color']) ? $gfb_settings['display']['gfb_cal_holiday_days_bg_color'] : '';

$gfb_label_location = isset($gfb_settings['label']['gfb_label_location']) ? $gfb_settings['label']['gfb_label_location'] : '';

$gfb_label_category = isset($gfb_settings['label']['gfb_label_category']) ? $gfb_settings['label']['gfb_label_category'] : '';

$gfb_label_service = isset($gfb_settings['label']['gfb_label_service']) ? $gfb_settings['label']['gfb_label_service'] : '';

$gfb_label_staff = isset($gfb_settings['label']['gfb_label_staff']) ? $gfb_settings['label']['gfb_label_staff'] : '';

$gfb_company_name = isset($gfb_settings['company']['gfb_company_name']) ? $gfb_settings['company']['gfb_company_name'] : '';

$gfb_company_address = isset($gfb_settings['company']['gfb_company_address']) ? $gfb_settings['company']['gfb_company_address'] : '';
$gfb_company_number = isset($gfb_settings['company']['gfb_company_number']) ? $gfb_settings['company']['gfb_company_number'] : '';

$gfb_company_email = isset($gfb_settings['company']['gfb_company_email']) ? $gfb_settings['company']['gfb_company_email'] : '';

$gfb_company_website_url = isset($gfb_settings['company']['gfb_company_website_url']) ? $gfb_settings['company']['gfb_company_website_url'] : '';

$gfb_sender_name = isset($gfb_settings['email']['gfb_sender_name']) ? $gfb_settings['email']['gfb_sender_name'] : '';

$gfb_sender_email = isset($gfb_settings['email']['gfb_sender_email']) ? $gfb_settings['email']['gfb_sender_email'] : '';

$gfb_gcal_switch = isset($gfb_settings['gcal']['gfb_gcal_switch']) ? $gfb_settings['gcal']['gfb_gcal_switch'] : 'disabled';

$gfb_gcal_client_id = isset($gfb_settings['gcal']['gfb_gcal_client_id']) ? $gfb_settings['gcal']['gfb_gcal_client_id'] : '';

$gfb_gcal_secret_key = isset($gfb_settings['gcal']['gfb_gcal_secret_key']) ? $gfb_settings['gcal']['gfb_gcal_secret_key'] : '';
?>
<div class="container">
    <div class="gf_booking_overlayDiv"></div>
    <div class="main-setting--div">        
        <div class="setting--content">
            <div class="top--heading">
                <h2><?php echo esc_html__('Settings', 'gfb'); ?></h2>
            </div>

            <div class="gf_booking_card--r1">
                <div class="gfb_item_box">
                    <div class="gf_booking_card--content" id="gf_booking_cardOne">
                        <img src="<?php echo GFB_IMAGE_URL; ?>General-setting.png" alt="">
                        <h4><?php echo esc_html__('General Settings', 'gfb'); ?></h4>
                        <p><?php echo esc_html__('Use these settings to define plugin general settings and default settings for your services and appointments.', 'gfb'); ?></p>
                    </div>
                </div>
                <div class="gfb_item_box">
                    <div class="gf_booking_card--content" id="gf_booking_cardtwo">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Untitled-2.png" alt="">
                        <h4><?php echo esc_html__('Display Settings', 'gfb'); ?></h4>
                        <p><?php echo esc_html__('Use these settings to define the color scheme of the calendar on front-end.', 'gfb'); ?> </p>
                    </div>
                </div>
                <div class="gfb_item_box">
                    <div class="gf_booking_card--content" id="gf_bookings_appointments">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Appointments.png" alt="">
                        <h4><?php echo esc_html__('Labels', 'gfb'); ?></h4>
                        <p><?php echo esc_html__('Use these settings to change the labels on front-end.', 'gfb'); ?></p>
                    </div>
                </div>
                     
                <div class="gfb_item_box">
                    <div class="gf_booking_card--content" id="gf_booking_company_detail">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Company.png" alt="">
                        <h4><?php echo esc_html__('Company', 'gfb'); ?></h4>
                        <p><?php echo esc_html__('Use these settings to add your company details.', 'gfb'); ?> </p>
                    </div>
                </div>
                <div class="gfb_item_box">
                    <div class="gf_booking_card--content" id="gf_booking_email_configuration">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Email-Configuration.png" alt="">
                        <h4><?php echo esc_html__('Email Configuration', 'gfb'); ?></h4>
                        <p><?php echo esc_html__('Use this settings to configure email work flow.', 'gfb'); ?> </p>
                    </div>
                </div>
                <div class="gfb_item_box">
                    <div class="gf_booking_card--content" id="gf_booking_google_calender">
                        <img src="<?php echo GFB_IMAGE_URL; ?>Google-Calender.png" alt="">
                        <h4><?php echo esc_html__('Google Calender', 'gfb'); ?></h4>
                        <p><?php echo esc_html__('Use this settings to integrate Google Calendar.', 'gfb'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="gf_booking_sidebar">
    <span class="close_icon"></span>
    <div class="gfb_booking_setting--inner gfb_fancy_scroll gf_booking_general--inner boxsizing closed">
        <h3 class="gfb-setting--title"><?php esc_html_e('General Settings', 'gfb'); ?></h3>
        <form class="gfb_setting_form">
            <input type="hidden" name="type" value="general" />

            <div class="gfb-field-wrapper">
                <label for="gfb_price_policy"><?php echo esc_html__('Price Policy', 'gfb'); ?></label>
                <select name="gfb_price_policy" id="gfb_price_policy">                    
                    <option value="admin" <?php selected('admin', $gfb_price_policy, true); ?>><?php esc_html_e('Admin', 'gfb'); ?></option>
                    <option value="staff" <?php selected('staff', $gfb_price_policy, true); ?>><?php esc_html_e('Staff', 'gfb'); ?></option>                    
                </select>
                <small><?php esc_html_e('Allow staff/admin to give rights to set the price for any service. Default is Admin', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_appointment_policy"><?php echo esc_html__('Appointment Policy', 'gfb'); ?></label>
                <select name="gfb_appointment_policy" id="gfb_appointment_policy">                    
                    <option value="admin" <?php selected('admin', $gfb_appointment_policy, true); ?>><?php esc_html_e('Admin', 'gfb'); ?></option>
                    <option value="staff" <?php selected('staff', $gfb_appointment_policy, true); ?>><?php esc_html_e('Staff', 'gfb'); ?></option>                    
                </select>
                <small><?php esc_html_e('Allow staff/admin to give rights to confirm appointments assigned to them. Default is Admin', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gbf_start_of_week"><?php echo esc_html__('Start Day of Week', 'gfb'); ?></label>
                <select name="gbf_start_of_week" id="gbf_start_of_week">                    
                    <option value="monday" <?php selected('monday', $gbf_start_of_week, true); ?>><?php esc_html_e('Monday', 'gfb'); ?></option>
                    <option value="sunday" <?php selected('sunday', $gbf_start_of_week, true); ?>><?php esc_html_e('Sunday', 'gfb'); ?></option>
                </select>
                <small><?php esc_html_e('This will set your calendar day start with.  Default is Monday', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gbf_time_duration"><?php echo esc_html__('Time Slot Duration (minutes)', 'gfb'); ?></label>
                <input type="number" min="1" name="gbf_time_duration" id="gbf_time_duration" value="<?php echo esc_attr($gbf_time_duration); ?>" />                    
                <small><?php esc_html_e('Each time slot duration. Ex: 180 = 3 hours.  Default is 30 mins', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="staff"><?php echo esc_html__('Time Slot Interval (minutes)', 'gfb'); ?></label>
                <input type="number" min="0" name="gbf_time_interval" id="gbf_time_interval" value="<?php echo esc_attr($gbf_time_interval); ?>" />                    
                <small><?php esc_html_e('Gap between each time slots. Ex: 60 = 1 hour. Default is 15 mins', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_calendar_timeslot_format"><?php echo esc_html__('Timeslot Format on Calendar', 'gfb'); ?></label>
                <select name="gfb_calendar_timeslot_format" id="gfb_calendar_timeslot_format">                    
                    <option value="24" <?php selected('24', $gfb_calendar_timeslot_format, true); ?>><?php esc_html_e('24 Hours', 'gfb'); ?></option>
                    <option value="12" <?php selected('12', $gfb_calendar_timeslot_format, true); ?>><?php esc_html_e('12 Hours', 'gfb'); ?></option>
                </select>
                <small><?php esc_html_e('This will set the time format of the Timeslots on the calendar. Default is 24 Hours', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gbf_show_timeslot"><?php echo esc_html__('Timeslots on Calendar', 'gfb'); ?></label>
                <select name="gbf_show_timeslot" id="gbf_show_timeslot">                    
                    <option value="right" <?php selected('right', $gbf_show_timeslot, true); ?>><?php esc_html_e('Right', 'gfb'); ?></option>
                    <option value="bottom" <?php selected('bottom', $gbf_show_timeslot, true); ?>><?php esc_html_e('Bottom', 'gfb'); ?></option>
                </select>
                <small><?php esc_html_e('This will set the position of the Timeslots on the calendar. Default is Right', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="staff"><?php echo esc_html__('Prior Months to Book Appointment', 'gfb'); ?></label>
                <input type="number" min="0" name="gfb_prior_months_book_appointment" id="gfb_prior_months_book_appointment" value="<?php echo esc_attr($gfb_prior_months_book_appointment); ?>" />                    
                <small><?php esc_html_e('No. of months allowed to book appointments. It cannot be zero.  Default is 3', 'gfb'); ?></small>
            </div>

            <button><?php esc_html_e('Save', 'gfb'); ?></button>
        </form>
    </div>

    <div class="gfb_booking_setting--inner gfb_fancy_scroll gf_booking_display--inner boxsizing losed">
        <h3 class="gfb-setting--title"><?php esc_html_e('Display Settings', 'gfb'); ?></h3>
        <form class="gfb_setting_form">
            <input type="hidden" name="type" value="display" />

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_gradient_color_top"><?php echo esc_html__('Calendar Background Color Gradient', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_gradient_color_top" id="gfb_cal_gradient_color_top" value="<?php echo esc_attr($gfb_cal_gradient_color_top); ?>" />
                <input type="color" name="gfb_cal_gradient_color_bottom" id="gfb_cal_gradient_color_bottom" value="<?php echo esc_attr($gfb_cal_gradient_color_bottom); ?>" />
                <small><?php esc_html_e('Default is #f1f3f6, #f1f3f6', 'gfb'); ?></small>         
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_border_color"><?php echo esc_html__('Calendar Border Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_border_color" id="gfb_cal_border_color" value="<?php echo esc_attr($gfb_cal_border_color); ?>" />  
                <small><?php esc_html_e('Default is #26547c21', 'gfb'); ?></small>               
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_header_bg_color"><?php echo esc_html__('Calendar Header Background Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_header_bg_color" id="gfb_cal_header_bg_color" value="<?php echo esc_attr($gfb_cal_header_bg_color); ?>" />  
                <small><?php esc_html_e('Default is #7C9299', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_header_text_color"><?php echo esc_html__('Calendar Header Text Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_header_text_color" id="gfb_cal_header_text_color" value="<?php echo esc_attr($gfb_cal_header_text_color); ?>" />  
                <small><?php esc_html_e('Default is #FFFFFF', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_text_color"><?php echo esc_html__('Calendar Text Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_text_color" id="gfb_cal_text_color" value="<?php echo esc_attr($gfb_cal_text_color); ?>" />  
                <small><?php esc_html_e('Default is #6D8298', 'gfb'); ?></small>               
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_selected_text_color"><?php echo esc_html__('Calendar Selected Text Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_selected_text_color" id="gfb_cal_selected_text_color" value="<?php echo esc_attr($gfb_cal_selected_text_color); ?>" /> 
                <small><?php esc_html_e('Default is #FFFFFF', 'gfb'); ?></small>                
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_selected_text_bg_color"><?php echo esc_html__('Calendar Selected Text Background Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_selected_text_bg_color" id="gfb_cal_selected_text_bg_color" value="<?php echo esc_attr($gfb_cal_selected_text_bg_color); ?>" />   
                <small><?php esc_html_e('Default is #00D0AC', 'gfb'); ?></small>              
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_available_days_text_color"><?php echo esc_html__('Calendar Available Days Text Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_available_days_text_color" id="gfb_cal_available_days_text_color" value="<?php echo esc_attr($gfb_cal_available_days_text_color); ?>" />  
                <small><?php esc_html_e('Default is #6D8298', 'gfb'); ?></small>               
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_available_days_bg_color"><?php echo esc_html__('Calendar Available Days Background Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_available_days_bg_color" id="gfb_cal_available_days_bg_color" value="<?php echo esc_attr($gfb_cal_available_days_bg_color); ?>" />
                <small><?php esc_html_e('Default is #FFFFFF', 'gfb'); ?></small> 
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_holiday_days_text_color"><?php echo esc_html__('Calendar Holiday Days Text Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_holiday_days_text_color" id="gfb_cal_holiday_days_text_color" value="<?php echo esc_attr($gfb_cal_holiday_days_text_color); ?>" />  
                <small><?php esc_html_e('Default is #6D8298', 'gfb'); ?></small>               
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_cal_holiday_days_bg_color"><?php echo esc_html__('Calendar Holiday Days Background Color', 'gfb'); ?></label>
                <input type="color" name="gfb_cal_holiday_days_bg_color" id="gfb_cal_holiday_days_bg_color" value="<?php echo esc_attr($gfb_cal_holiday_days_bg_color); ?>" />
                <small><?php esc_html_e('Default is empty', 'gfb'); ?></small>
            </div>

            <button><?php esc_html_e('Save', 'gfb'); ?></button>
        </form>
    </div>

    <div class="gfb_booking_setting--inner gfb_fancy_scroll gf_booking_apointments_listing boxsizing closed">
        <h3 class="gfb-setting--title"><?php esc_html_e('Label Settings', 'gfb'); ?></h3>
        <form class="gfb_setting_form">
            <input type="hidden" name="type" value="label" />            

            <div class="gfb-field-wrapper">
                <label for="gfb_label_location"><?php echo esc_html__('Location', 'gfb'); ?></label>
                <input type="text" name="gfb_label_location" id="gfb_label_location" value="<?php echo esc_attr($gfb_label_location); ?>" />                
            </div>

            <div class="gfb-field-wrapper" style="display: none;">
                <label for="gfb_label_category"><?php echo esc_html__('Service Category', 'gfb'); ?></label>
                <input type="text" name="gfb_label_category" id="gfb_label_category" value="<?php echo esc_attr($gfb_label_category); ?>" />                
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_label_service"><?php echo esc_html__('Service', 'gfb'); ?></label>
                <input type="text" name="gfb_label_service" id="gfb_label_service" value="<?php echo esc_attr($gfb_label_service); ?>" />                
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_label_staff"><?php echo esc_html__('Staff', 'gfb'); ?></label>
                <input type="text" name="gfb_label_staff" id="gfb_label_staff" value="<?php echo esc_attr($gfb_label_staff); ?>" />                
            </div>

            <button><?php esc_html_e('Save', 'gfb'); ?></button>
        </form> 
    </div>    

    <div class="gfb_booking_setting--inner gfb_fancy_scroll gf_booking_compnay_detail boxsizing closed">
        <h3 class="gfb-setting--title"><?php esc_html_e('Company Settings', 'gfb'); ?></h3>
        <form class="gfb_setting_form">
            <input type="hidden" name="type" value="company" />

            <div class="gfb-field-wrapper">
                <label for="gfb_company_name"><?php echo esc_html__('Name', 'gfb'); ?></label>
                <input type="text" name="gfb_company_name" id="gfb_company_name" value="<?php echo esc_attr($gfb_company_name); ?>" />
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_company_address"><?php echo esc_html__('Address', 'gfb'); ?></label>
                <textarea name="gfb_company_address" id="gfb_company_address" cols="30" rows="10"><?php echo esc_attr($gfb_company_address); ?></textarea>                
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_company_number"><?php echo esc_html__('Phone Number', 'gfb'); ?></label>
                <input type="text" name="gfb_company_number" id="gfb_company_number" value="<?php echo esc_attr($gfb_company_number); ?>" />                
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_company_email"><?php echo esc_html__('Email ID', 'gfb'); ?></label>
                <input type="email" name="gfb_company_email" id="gfb_company_email" value="<?php echo esc_attr($gfb_company_email); ?>" />                
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_company_website_url"><?php echo esc_html__('Website URL', 'gfb'); ?></label>
                <input type="url" name="gfb_company_website_url" id="gfb_company_website_url" value="<?php echo esc_attr($gfb_company_website_url); ?>" />                
            </div>

            <button><?php esc_html_e('Save', 'gfb'); ?></button>
        </form>
    </div>   

    <div class="gfb_booking_setting--inner gfb_fancy_scroll gf_booking_email_configuration boxsizing closed">
        <h3 class="gfb-setting--title"><?php esc_html_e('Email Configuration', 'gfb'); ?></h3>
        <form class="gfb_setting_form">
            <input type="hidden" name="type" value="email" />

            <div class="gfb-field-wrapper">
                <label for="gfb_sender_name"><?php echo esc_html__('Sender Name', 'gfb'); ?></label>
                <input type="text" name="gfb_sender_name" id="gfb_sender_name" value="<?php echo esc_attr($gfb_sender_name); ?>" />
            </div>            

            <div class="gfb-field-wrapper">
                <label for="gfb_sender_email"><?php echo esc_html__('Sender Email', 'gfb'); ?></label>
                <input type="email" name="gfb_sender_email" id="gfb_sender_email" value="<?php echo esc_attr($gfb_sender_email); ?>" />
                <small><?php esc_html_e('Enter email address from which sender will send the email. Email should be of domain.', 'gfb'); ?></small>
            </div>

            <button><?php esc_html_e('Save', 'gfb'); ?></button>
        </form>
    </div>
    
    <div class="gfb_booking_setting--inner gfb_fancy_scroll gf_booking_google_calander boxsizing closed">
        <h3 class="gfb-setting--title"><?php esc_html_e('Google Calender', 'gfb'); ?></h3>
        <form class="gfb_setting_form">
            <input type="hidden" name="type" value="gcal" />

            <div class="gfb-field-wrapper">
                <label><?php echo esc_html__('Enable Google Calendar', 'gfb'); ?></label>
                <input type="radio" name="gfb_gcal_switch" id="gfb_gcal_switch_enabled" value="enabled" <?php checked('enabled', $gfb_gcal_switch, true); ?> /> <?php esc_html_e('Enable', 'gfb'); ?>&nbsp;&nbsp;
                <input type="radio" name="gfb_gcal_switch" id="gfb_gcal_switch_disabled" value="disabled" <?php checked('disabled', $gfb_gcal_switch, true); ?> />  <?php esc_html_e('Disable', 'gfb'); ?>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_gcal_client_id"><?php echo esc_html__('Client ID', 'gfb'); ?></label>
                <input type="text" name="gfb_gcal_client_id" id="gfb_gcal_client_id" value="<?php echo esc_attr($gfb_gcal_client_id); ?>" />
                <small><?php esc_html_e('Enter Client Id.', 'gfb'); ?></small>
            </div>

            <div class="gfb-field-wrapper">
                <label for="gfb_gcal_secret_key"><?php echo esc_html__('Secret Key', 'gfb'); ?></label>
                <input type="text" name="gfb_gcal_secret_key" id="gfb_gcal_secret_key" value="<?php echo esc_attr($gfb_gcal_secret_key); ?>" />
                <small><?php esc_html_e('Enter Secret Key.', 'gfb'); ?></small>
            </div>

            <button><?php esc_html_e('Save', 'gfb'); ?></button>

            <div class="gfb-info">
                <h4>Instructions</h4>
                <p>To find your client ID and client secret, do the following:</p>
                <ol>
                    <li>Go to the Google Developers Console.</li>
                    <li>Login to your gmail account.</li>
                    <li>Select a project, or create a new one.</li>
                    <li>Click in the upper left part to see a sliding sidebar. Next, click API Manager. In the list of APIs look for Calendar API and make sure it is enabled.</li>
                    <li>In the sidebar on the left, select Credentials.</li>
                    <li>Go to OAuth consent screen tab and give a name to the product, then click Save.</li>
                    <li>Go to Credentials tab and in New credentials drop-down menu select OAuth client ID.</li>
                    <li>Select Web application and create your project's OAuth 2.0 credentials by providing the necessary information. For Authorized redirect URIs enter the Redirect URI '<?php echo esc_url( admin_url() ); ?>'. Click Create.</li>
                    <li>If you want to enable add to my calendar functionality then you will need to insert another Authorized Redirect URI i.e. "<?php echo esc_url( site_url() ); ?>/{url_where_form_placed}"</li>
                    <li>In the popup window look for the Client ID and Client secret. Use them in the form below on this page.</li>
                    <li>Login with Staff Members, on dashboard click Connect.</li>
                </ol>
            </div>
        </form>
    </div>
</div>

<?php
/**
 * GFB Customer Appointment View Front-end
 *
 * @package GFB.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$appointments = GFB_Appointment::gfb_get_appointments_by_customer( $user_id );
if ( is_array( $appointments ) && count($appointments) > 0 ) {
    // echo '<pre>$appointments$appointments';
    // print_r( $appointments );
    // echo '</pre>';
    ?>
    <table>
        <thead>
            <tr>
                <th><?php echo esc_html__('ID', 'gfb'); ?></th>
                <th><?php echo esc_html__('Date/Time', 'gfb'); ?></th>
                <th><?php echo esc_html__('Location', 'gfb'); ?></th>
                <th><?php echo esc_html__('Service', 'gfb'); ?></th>
                <th><?php echo esc_html__('Staff', 'gfb'); ?></th>
                <th><?php echo esc_html__('Slots', 'gfb'); ?></th>
                <th><?php echo esc_html__('Payment', 'gfb'); ?></th>
                <th><?php echo esc_html__('Status', 'gfb'); ?></th>
            </tr>    
        </thead>
        <tbody>        
        <?php
        foreach( $appointments as $appointment ) {
            $date = get_post_meta( $appointment->ID, 'date', true );
            $date = apply_filters('gfb_appointment_date_format', $date);
            $time = get_post_meta( $appointment->ID, 'time', true );
            $time = apply_filters('gfb_appointment_time_format', $time);
            $location = get_post_meta( $appointment->ID, 'location_id', true );            
            $location = (! empty($location) && 'all' != $location) ? get_the_title( $location ) : $location;
            $service = get_post_meta( $appointment->ID, 'service_id', true );            
            $service = get_the_title( $service );
            $staff = get_post_meta( $appointment->ID, 'staff_id', true );            
            $staff = get_the_title( $staff );
            $capacity = get_post_meta( $appointment->ID, 'slot', true );
            $cost = get_post_meta( $appointment->ID, 'cost', true );
            $cost = gfb_price_format($cost);
            $status = get_post_meta( $appointment->ID, 'status', true );
            ?>
            <tr>
                <td><?php echo esc_attr($appointment->ID); ?></td>
                <td><?php echo esc_attr($date) . ' ' . esc_attr($time); ?></td>
                <td>
                    <?php
                    if ('all' == $location) {
                        echo '-';
                    } else {
                        echo esc_attr($location);
                    }
                    ?>
                </td>
                <td><?php echo esc_attr($service); ?></td>
                <td><?php echo esc_attr($staff); ?></td>
                <td><?php echo esc_attr($capacity); ?></td>
                <td><?php echo esc_attr($cost); ?></td>
                <?php
                if ( 'pending' == $status ) {
                    ?>
                    <td>                        
                        <form class="gfb-app-cancel-form" id="gfb-app-cancel-form-<?php echo esc_attr($appointment->ID); ?>" action="" method="post">
                            <input type="hidden" name="gfb-app-id" value="<?php echo esc_attr($appointment->ID); ?>" />
                            <input type="hidden" name="gfb-status" value="cancelled" />                            
                            <input type="submit" name="gfb-change-status" value="<?php echo esc_html__('Cancel Request', 'gfb'); ?>">
                        </form>
                    </td>
                    <?php
                } else {
                    ?>
                    <td><?php echo esc_attr(strtoupper($status)); ?></td>
                    <?php
                }
                ?>
                
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
} else {
    echo '<p>' . esc_html__('No appointment found.', 'gfb') . '</p>';
}

?>
<?php

/**
 * Plugin.
 * @package reactplug
 * @wordpress-plugin
 * Plugin Name:     MM Product Listing
 * Description:     Wordpress plugin using React js for product listing
 * Author:          MM
 * Author URL:      https://mobile-marketing.agency/
 * Version:         1.0.42
 */
require_once(ABSPATH . "wp-includes/pluggable.php");
require_once(ABSPATH . "/wp-load.php");

/**  Plugin updater checker integration **/
require 'plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
  'https://bitbucket.org/mobilemarketingllc/mm-product-listing',
  __FILE__,
  'mm-product-listing'
);

$myUpdateChecker->setAuthentication(array(
  'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
  'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));

$myUpdateChecker->setBranch('master');


function displayProductList($atts)
{
  global $wpdb;
  $category = isset($atts['category']) ? explode(",", $atts['category']) : "";
  $inStock = isset($atts['in_stock']) ? $atts['in_stock'] : "";
  $rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));

  if (get_option("instocksyncseprate") == "1" && $inStock == "1") {
    $categories = array(
      "carpet" => "instock_carpet",
      "hardwood" => "instock_hardwood",
      "laminate" => "instock_laminate",
      "tile" => "instock_tile",
      "lvt" => "instock_lvt",
      "area_rugs" => "instock_area_rugs",
      "sheet" => "instock_sheet_vinyl"
    );
  } else {
    $categories = array(
      "carpet" => "carpeting",
      "hardwood" => "hardwood_catalog",
      "laminate" => "laminate_catalog",
      "tile" => "tile_catalog",
      "lvt" => "luxury_vinyl_tile",
      "area_rugs" => "area_rugs",
      "paint" => "paint_catalog",
      "sheet" => "sheet_vinyl"
    );
  }
  $cat = is_array($category) && count($category) == 1 ? $category[0] : "";
  $rewrite_slug = isset($rewrite_rule[$categories[$cat]]["rewrite_slug"]) ? $rewrite_rule[$categories[$cat]]["rewrite_slug"] : "";


  $title = isset($atts['title']) ? $atts['title'] : "BROWSE " . ucwords($atts['category']);
  $productFacets = get_option('productFacets') ? unserialize(get_option('productFacets')) : array();
  $productFacets = isset($productFacets[$category[0]]) ? $productFacets[$category[0]] : array();
  unset($atts['category'], $atts['in_stock'], $atts['title']);
  $arr = array(
    'category' => $category,
    'inStock' => $inStock,
    'title' => $title,
    'plpFacets' => $productFacets,
    'rewrite_slug' => $rewrite_slug
  );
  $arr = array_merge($arr, $atts);
  $fav_skus = "";
  if (is_user_logged_in()) {
    $table_fav = $wpdb->prefix . 'favorite_posts';
    $fav_sql = "SELECT product_id FROM $table_fav WHERE user_id =" . get_current_user_id();

    $fav_skus = $wpdb->get_results($fav_sql, ARRAY_N);
    if ($fav_skus) {
      $arr["fav_skus"] = array_merge(...$fav_skus);
    }
  }

  ob_start();
  wp_localize_script('wp-product-filter-react', 'wpProductCategory', $arr);
?>
  <div id="mm-product-list"></div>
<?php return ob_get_clean();
}

add_shortcode('MM-PRODUCT-LIST', 'displayProductList');



function displayPopularInStock($atts)
{
  $category = isset($atts['category']) ? explode(",", $atts['category']) : "";
  $inStock = isset($atts['in_stock']) ? $atts['in_stock'] : "";
  $rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));

  if (get_option("instocksyncseprate") == "1" && $inStock == "1") {
    $categories = array(
      "carpet" => "instock_carpet",
      "hardwood" => "instock_hardwood",
      "laminate" => "instock_laminate",
      "tile" => "instock_tile",
      "lvt" => "instock_lvt",
      "area_rugs" => "instock_area_rugs",
      "sheet" => "instock_sheet_vinyl"
    );
  } else {
    $categories = array(
      "carpet" => "carpeting",
      "hardwood" => "hardwood_catalog",
      "laminate" => "laminate_catalog",
      "tile" => "tile_catalog",
      "lvt" => "luxury_vinyl_tile",
      "area_rugs" => "area_rugs",
      "paint" => "paint_catalog",
      "sheet" => "sheet_vinyl"
    );
  }
  $cat = is_array($category) && count($category) == 1 ? $category[0] : "";
  $rewrite_slug = isset($rewrite_rule[$categories[$cat]]["rewrite_slug"]) ? $rewrite_rule[$categories[$cat]]["rewrite_slug"] : "";

  $productFacets = get_option('productFacets') ? unserialize(get_option('productFacets')) : array();
  $productFacets = isset($productFacets[$category[0]]) ? $productFacets[$category[0]] : array();
  unset($atts['category'], $atts['in_stock'], $atts['title']);

  $arr = array(
    'category' => $category,
    'inStock' => $inStock,
    'plpFacets' => $productFacets,
    'rewrite_slug' => $rewrite_slug
  );
  $arr = array_merge($arr, $atts);


  ob_start();
  wp_localize_script('wp-product-filter-react', 'wpProductInSTock', $arr);
?>
  <div id="mm-popular-in-stock"></div>
<?php return ob_get_clean();
}
add_shortcode('POPULAR-INSTOCK', 'displayPopularInStock');

function displayFeaturedProducts($atts)
{
  $category = isset($atts['category']) ? explode(",", $atts['category']) : "";
  ob_start();
  wp_localize_script('wp-product-filter-react', 'wpProductFeatured', array(
    'category' => $category
  ));
?>
  <div id="mm-featured-products"></div>
<?php return ob_get_clean();
}
add_shortcode('FEATURED-PRODUCTS', 'displayFeaturedProducts');


function displayProductLoop($atts)
{
  $category = isset($atts['category']) ? explode(",", $atts['category']) : "";
  $inStock = isset($atts['in_stock']) ? $atts['in_stock'] : "";
  $rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));
  $limit = isset($atts['limit']) ? $atts['limit'] : "";
  if (get_option("instocksyncseprate") == "1" && $inStock == "1") {
    $categories = array(
      "carpet" => "instock_carpet",
      "hardwood" => "instock_hardwood",
      "laminate" => "instock_laminate",
      "tile" => "instock_tile",
      "lvt" => "instock_lvt",
      "area_rugs" => "instock_area_rugs",
      "sheet" => "instock_sheet_vinyl"
    );
  } else {
    $categories = array(
      "carpet" => "carpeting",
      "hardwood" => "hardwood_catalog",
      "laminate" => "laminate_catalog",
      "tile" => "tile_catalog",
      "lvt" => "luxury_vinyl_tile",
      "area_rugs" => "area_rugs",
      "paint" => "paint_catalog",
      "sheet" => "sheet_vinyl"
    );
  }
  $cat = is_array($category) && count($category) == 1 ? $category[0] : "";
  $rewrite_slug = isset($rewrite_rule[$categories[$cat]]["rewrite_slug"]) ? $rewrite_rule[$categories[$cat]]["rewrite_slug"] : "";

  unset($atts['category'], $atts['in_stock'], $atts['limit']);
  $arr = array(
    'category' => $category,
    'inStock' => $inStock,
    'rewrite_slug' => $rewrite_slug,
    'limit' => $limit
  );
  $arr = array_merge($arr, $atts);
  ob_start();
  wp_localize_script('wp-product-filter-react', 'wpProductCategory', $arr);
?>
  <div id="mm-product-loop"></div>
<?php return ob_get_clean();
}
add_shortcode('PRODUCT-LOOP', 'displayProductLoop');

// Register REST API endpoint
require_once plugin_dir_path(__FILE__) . 'product-filter-endpoint.php';

add_action('wp_enqueue_scripts', 'enq_mm_scripts');
function enq_mm_scripts()
{
  global $post;

  $files = glob("wp-content/plugins/mm-product-listing/build/static/js/main.*.js");
  $file =  !empty($files) ? $files[0] : null;
  $file = $file ? basename($file, ".js") : null;

  wp_register_script(
    'wp-product-filter-react',
    plugin_dir_url(__FILE__) . 'build/static/js/' . $file . ".js",
    ['wp-element'],
    rand(), // Change this to null for production
    true
  );
  $stylefiles = glob("wp-content/plugins/mm-product-listing/build/static/css/main.*.css");
  $stylefile =  !empty($stylefiles) ? $stylefiles[0] : null;
  $stylefile = $stylefile ? basename($stylefile, ".css") : null;

  wp_register_style(
    'wp-product-filter-react-style',
    plugin_dir_url(__FILE__) . 'build/static/css/' . $stylefile . ".css"
  );
  wp_enqueue_style('wp-product-filter-react-style');

  $show_financing = get_option('sh_get_finance');
  $getcouponbtn = get_option('getcouponbtn');
  $getcouponreplace = get_option('getcouponreplace');
  $getcouponreplacetext = get_option('getcouponreplacetext');
  $getcouponreplaceurl = get_option('getcouponreplaceurl');
  $pdp_get_finance = get_option('pdp_get_finance');
  $getfinancereplace = get_option('getfinancereplace');
  $getfinancereplaceurl = get_option('getfinancereplaceurl');
  $getfinancetext = get_option('getfinancetext');
  $getcoupon_link = get_option('getcoupon_link');
  $plpshowprice = get_option('plpshowprice');
  $showpricestrik =
    get_option('plpshowpricestrike') ? get_option('plpshowpricestrike') : "";
  $showretailandspecialprice =
    get_option('plpshowretailandspecialprice') ? get_option('plpshowretailandspecialprice') : "";
  $showrugsbutton = get_option('arearugbutton');
  $rugsbrands = get_option('area_rugs_brands');
  $rugpageurl = get_option('rugpageurl');
  $arearugshowsku = get_option('arearugshowsku');
  $plpestimatebutton = get_option('plpestimatebutton');
  $plpestimatebuttonlink = get_option('plpestimatebuttonlink');
  $pdpshowform = get_option('pdpshowform');
  $pdpshowforminstock = get_option('pdpshowforminstock') ? get_option('pdpshowforminstock') : "";
  $pdplayout = get_option('layoutopotion') ? get_option('layoutopotion') : 'default';
  $isworkbook = get_option('isworkbook') ? get_option('isworkbook') : 0;
  $showpdpsamplebtn = get_option('showpdpsamplebtn') ? get_option('showpdpsamplebtn') : 0;
  $showsamplelabel = get_option('showsamplelabel') ? get_option('showsamplelabel') : 0;
  $hideinstockcoupon = get_option('hideinstockcoupon') ? get_option('hideinstockcoupon') : 0;
  $site_code = get_option('SITE_CODE') ? get_option('SITE_CODE') : "";
  $showcallforprice = get_option('showcallforprice') ? get_option('showcallforprice') : 0;
  $showsharebtn = get_option('showsharebtn') ? get_option('showsharebtn') : 0;

  wp_localize_script('wp-product-filter-react', 'wpProductFilter', array(
    'apiEndpoint' => rest_url('products/v1/list'),
    'siteInfo' => [
      'current_user_id' => is_user_logged_in() ? get_current_user_id() : "",
      'getcouponbtn' => $getcouponbtn,
      'getcouponreplace' => $getcouponreplace,
      'getcouponreplacetext' => $getcouponreplacetext,
      'getcouponreplaceurl' => $getcouponreplaceurl,
      'pdp_get_finance' => $pdp_get_finance,
      'show_financing' => $show_financing,
      'getfinancereplace' => $getfinancereplace,
      'getfinancereplaceurl' => $getfinancereplaceurl,
      'getfinancetext' => $getfinancetext,
      'getcoupon_link' => $getcoupon_link,
      'plpshowprice' => $plpshowprice,
      'showpricestrik' => $showpricestrik,
      'showretailandspecialprice' => $showretailandspecialprice,
      'showrugsbutton' => $showrugsbutton,
      'rugsbrands' => $rugsbrands,
      'rugpageurl' => $rugpageurl,
      'currentPageID' => isset($post->ID) ? $post->ID : "",
      'arearugshowsku' => $arearugshowsku,
      'plpestimatebutton' => $plpestimatebutton,
      'plpestimatebuttonlink' => $plpestimatebuttonlink,
      'pdpshowform' => $pdpshowform,
      'pdpshowforminstock' => $pdpshowforminstock,
      'pdplayout' => $pdplayout,
      'isworkbook' => $isworkbook,
      'showpdpsamplebtn' => $showpdpsamplebtn,
      'showsamplelabel' => $showsamplelabel,
      'hideinstockcoupon' => $hideinstockcoupon,
      'site_code' => $site_code,
      'showcallforprice' => $showcallforprice,
      'showsharebtn' => $showsharebtn
    ]
  ));
  wp_enqueue_script('wp-product-filter-react');
}

function displayAllianceProductList($atts)
{

  $category = isset($atts['category']) ? explode(",", $atts['category']) : "";
  $inStock = isset($atts['in_stock']) ? $atts['in_stock'] : "";
  $rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));

  if (get_option("instocksyncseprate") == "1" && $inStock == "1") {
    $categories = array(
      "carpet" => "instock_carpet",
      "hardwood" => "instock_hardwood",
      "laminate" => "instock_laminate",
      "tile" => "instock_tile",
      "lvt" => "instock_lvt",
      "area_rugs" => "instock_area_rugs"
    );
  } else {
    $categories = array(
      "carpet" => "carpeting",
      "hardwood" => "hardwood_catalog",
      "laminate" => "laminate_catalog",
      "tile" => "tile_catalog",
      "lvt" => "luxury_vinyl_tile",
      "area_rugs" => "area_rugs",
      "paint" => "paint_catalog",
      "sheet" => "sheet_vinyl"
    );
  }
  $cat = is_array($category) && count($category) == 1 ? $category[0] : "";
  $rewrite_slug = isset($rewrite_rule[$categories[$cat]]["rewrite_slug"]) ? $rewrite_rule[$categories[$cat]]["rewrite_slug"] : "";

  $title = isset($atts['title']) ? $atts['title'] : "BROWSE " . ucwords($atts['category']);
  $productFacets = get_option('productFacets') ? unserialize(get_option('productFacets')) : array();
  $productFacets = isset($productFacets[$category[0]]) ? $productFacets[$category[0]] : array();
  $pdplayout = get_option('layoutopotion') ? get_option('layoutopotion') : 'default';

  $collectionFacetList = array();
  if ($pdplayout == "alliance") {
    $collectionFacets = get_option('collectionFacets') ? unserialize(get_option('collectionFacets')) : array();
    $collectionFacetList = isset($collectionFacets[$category[0]]) ? $collectionFacets[$category[0]] : array();
  }
  unset($atts['category'], $atts['in_stock'], $atts['title']);
  $subTitle = isset($atts['sub_title']) ? $atts['sub_title'] : "";

  $arr = array(
    'category' => $category,
    'inStock' => $inStock,
    'title' => $title,
    'plpFacets' => $productFacets,
    'subTitle' => $subTitle,
    'rewrite_slug' => $rewrite_slug,
    'collectionFacets' => $collectionFacetList
  );

  $arr = array_merge($arr, $atts);

  ob_start();
  wp_localize_script('wp-product-filter-react', 'wpProductCategory', $arr);
?>
  <div id="mm-alliance-list"></div>
<?php return ob_get_clean();
}

add_shortcode('ALLIANCE-PRODUCTS-LIST', 'displayAllianceProductList');

<?php
add_action('rest_api_init', function () {
  register_rest_route('products/v1', '/list', [
    'methods' => 'GET',
    'callback' => 'get_filtered_products'
  ]);
});

function arrange_product_data_order($products, $keyToCheck, $values)
{
  $matchedElements = [];
  foreach ($values as $valueToCheck) {
    if ($valueToCheck) {
      foreach ($products as $key => $subArray) {
        if (isset($subArray[$keyToCheck]) && strtolower($subArray[$keyToCheck]) == strtolower($valueToCheck)) {
          $matchedElements[] = $subArray;
          unset($products[$key]);
        }
      }
    }
  }

  $products = array_merge($matchedElements, $products);

  return $products;
}

function sort_brands_by_alphabate($array, $compare)
{
  return strcmp($array["brand_facet"], $compare["brand_facet"]);
}
function sort_brands_by_collection($array, $compare)
{
  return strcmp($array["collection_facet"], $compare["collection_facet"]);
}
function sort_products_by_price_asc($array, $compare)
{
  return strcmp($array["price"], $compare["price"]);
}
function sort_products_by_price_desc($array, $compare)
{
  return strcmp($compare["price"], $array["price"]);
}

// function add_brand_collection_color($products)
// {
//   $new_products = array();
//   foreach ($products as $product) {
//     $new_products[] = array_merge($product, array("brand_collection_color" => $product['brand_collection'] . " " . $product['color']));
//   }

//   return $new_products;
// }

function set_product_data($category)
{
  $products = array();
  if ($category == "all") {
    $categories = array('carpet', 'hardwood', 'laminate', 'lvt', 'tile', 'paint', 'sheet');
    if (get_option("arearugsync") == 1) {
      $categories = array_merge($categories, array("area_rugs"));
    }
    foreach ($categories as $cat) {
      $json_file_path = wp_upload_dir()['basedir'] . '/sfn-data/' . trim($cat) . '.json';

      if (file_exists($json_file_path)) {
        $products = array_merge($products, json_decode(file_get_contents($json_file_path), true));
      }
    }
  } else {
    $json_file_path = wp_upload_dir()['basedir'] . '/sfn-data/' . trim($category) . '.json';

    if (!file_exists($json_file_path)) {
      return new WP_Error('no_file', 'Products file not found', array('status' => 404));
    }
    $products = json_decode(file_get_contents($json_file_path), true);
    if (is_array($products) && count($products) > 0) {
      usort($products, 'sort_brands_by_collection');
      usort($products, 'sort_brands_by_alphabate');
    }

    $collection_order = is_array(get_option("product_collection_order")) ? get_option("product_collection_order") : unserialize(get_option("product_collection_order"));

    if (is_array($collection_order) && isset($collection_order[$category])) {
      $products = arrange_product_data_order($products, 'collection_facet', $collection_order[$category]);
    }

    $brands_order = is_array(get_option("product_brand_order")) ? $brands_order = get_option("product_brand_order") : unserialize(get_option("product_brand_order"));

    if (is_array($brands_order) && isset($brands_order[$category])) {
      $products = arrange_product_data_order($products, 'brand_facet', $brands_order[$category]);
    }
    if (is_array($products) && count($products) > 0) {
      $products = arrange_product_data_order($products, 'in_stock', array(1));
    }
  }

  // if (is_array($products) && count($products) > 0) {
  //   if ($category == "area_rugs") {
  //     $products = add_brand_collection_color($products);
  //   }
  // }
  return $products;
}

function get_filtered_products(WP_REST_Request $request)
{
  global $post;
  $category = $request->get_param('category') ? explode(",", $request->get_param('category')) : "";
  $in_stock = $request->get_param('in_stock') != "" ? $request->get_param('in_stock') : "";
  if ($category) {
    $products = array();
    $redis = new Redis();
    $redis->connect('127.0.0.1', 6379);
    $prefix = date("Ymd");

    if ($redis->get('product_date') != $prefix) {
      $redis->set("all", json_encode($products));
      $redis->set("carpet", json_encode($products));
      $redis->set("hardwood", json_encode($products));
      $redis->set("laminate", json_encode($products));
      $redis->set("lvt", json_encode($products));
      $redis->set("tile", json_encode($products));
      $redis->set("area_rugs", json_encode($products));
      $redis->set("paint", json_encode($products));
      $redis->set("sheet", json_encode($products));
    }
    if (!is_array($category) || (is_array($category) && count($category) == 1)) {
      $category = !is_array($category) ? $category : $category[0];
      $products = json_decode($redis->get($category), true);

      if (!is_array($products) || count($products) == 0) {
        $products = set_product_data($category);
        if (is_array($products) && count($products) > 0) {
          $redis->set($category, json_encode($products));
          $redis->set('product_date', $prefix);
        }
      }
    } else {
      foreach ($category as $cat) {
        $prod = json_decode($redis->get($cat), true);
        if (!is_array($prod) || count($prod) == 0) {
          $prod = set_product_data($cat);
          if (is_array($prod) && count($prod) > 0) {
            $redis->set($cat, json_encode($prod));
            $redis->set('product_date', $prefix);
          }
        }
        $products = array_merge($products, $prod);
      }
    }

    if (is_array($products) && count($products) > 0 && $in_stock == 1 && get_option("plpshowprice") == 1 && get_option("product_order_by_price") && get_option("product_order_by_price") != "any") {
      if (get_option("product_order_by_price") == "desc") {
        usort($products, 'sort_products_by_price_desc');
      } else {
        usort($products, 'sort_products_by_price_asc');
      }
    }

    $page = $request->get_param('paged') ?: 1;
    $limit = $request->get_param('limit') ?: 12;
    $sort = $request->get_param('sort') ?: '';

    $group_by = $request->get_param('group_by') ?: 'brand_collection';
    $globalSearch = $request->get_param('globalSearch') ? true : false;
    $pageName = $request->get_param('pageName') ?: "";
    $filters = array_filter($request->get_params());
    if ($pageName != "pdp") {
      $filters['in_stock'] = $in_stock;
    }
    $show_size_on_pdp = isset($filters["show_size_on_pdp"]) ? $filters["show_size_on_pdp"] : "";

    if (
      $request->get_param('brand_collection') != "" &&
      get_option("nogroupbysize") == "1"
      && is_array(get_option("nogroupbysize_cat")) && $category != "all" && in_array($category, get_option("nogroupbysize_cat"))
    ) {
      $filters["sku"] = $filters['current_sku'];
    }

    unset($filters['paged'], $filters['limit'], $filters['sort'], $filters['group_by'], $filters['pageName'], $filters['globalSearch'], $filters["show_size_on_pdp"], $filters["current_sku"]);
    if (isset($filters['in_stock']) && $filters['in_stock'] === "") {
      unset($filters['in_stock']);
    }


    $active_filters = $filters;


    unset($filters['category']);


    $filtered_products = array_filter($products, function ($product) use ($filters) {
      if (!isset($product['swatch']) || $product['swatch'] == "") {
        return false;
      }
      if (!isset($product['status']) || $product['status'] != "active") {
        return false;
      }
      if (is_array($filters) && count($filters) > 0) {
        foreach ($filters as $key => $value) {
          if ($key == "search") {
            $search_string = $product['name'] . " " . $product['sku'] . " " . $product['collection_facet'] . " " . $product['brand_facet'];

            if (isset($product['color_facet'])) {
              $search_string .= " " . $product['color_facet'];
            }
            if (isset($product['style_facet'])) {
              $search_string .= " " . $product['style_facet'];
            }
            if (isset($product['style'])) {
              $search_string .= " " . $product['style'];
            }
            if (isset($product['fiber'])) {
              $search_string .= " " . $product['fiber'];
            }
            if (isset($product['fiber_facet'])) {
              $search_string .= " " . $product['fiber_facet'];
            }
            if (isset($product['material'])) {
              $search_string .= " " . $product['material'];
            }
            if (isset($product['material_facet'])) {
              $search_string .= " " . $product['material_facet'];
            }

            if (!preg_match("/" . strtolower($value) . "/", strtolower($search_string))) {
              return false;
            }
          } else {
            if (
              $key == "in_stock" && $product['in_stock'] != $value
            ) {
              return false;
            }

            if ($value == "" || !isset($product[$key])) {
              return false;
            }

            if ($key == "brand_collection") {
              $facet_arr[0] = strtolower($value);
            } else {
              $facet_arr = explode(",", strtolower($value));

              if ($key == "fiber" || $key == "fiber_facet" || $key == "lifestyle") {
                if ($value != "" && isset($product[$key]) && !in_array(strtolower($product[$key]), $facet_arr)) {
                  $pval = strtolower($product[$key]);
                  $result = array_filter($facet_arr, function ($item) use ($pval) {
                    return strpos($item, $pval) !== false;
                  });
                  if (!empty($result)) {
                    return true;
                  } else {
                    return false;
                  }
                }
              }
            }

            if ($value != "" && isset($product[$key]) && !in_array(strtolower($product[$key]), $facet_arr)) {
              return false;
            }
          }
        }
      }
      return true;
    });


    if ($sort) {
      usort($filtered_products, function ($a, $b) use ($sort) {
        if ($sort === 'low-to-high') {
          return $a['price'] - $b['price'];
        } elseif ($sort === 'high-to-low') {
          return $b['price'] - $a['price'];
        }
        return 0;
      });
    }
    if ($globalSearch) {
      $products_group_by = $filtered_products;
    } else {
      $distinct_column = "";
      // var_dump(get_option("groupbysize"), get_option("groupbysize_cat"));
      if (
        get_option("groupbysize") == "1"
        && is_array(get_option("groupbysize_cat")) && $category != "all" && in_array($category, get_option("groupbysize_cat")) && $show_size_on_pdp != "yes"
      ) {
        $distinct_column = "color";
      } else {
        $distinct_column = "";
      }
      if (
        get_option("nogroupbysize") == "1"
        && is_array(get_option("nogroupbysize_cat")) && $category != "all" && in_array($category, get_option("nogroupbysize_cat"))
      ) {
        $group_by = "sku";
      }
      $products_group_by = array_group_by($filtered_products, $group_by, $distinct_column);
    }
    $total_products = count($products_group_by);
    $total_pages = ceil($total_products / $limit);
    $offset = ($page - 1) * $limit;

    if ($globalSearch) {
      $filter_counts = array();
    } else {
      $filter_counts = generate_filter_counts($products_group_by, $active_filters);
    }
    $paged_products_group_by = array_slice($products_group_by, $offset, $limit);

    return [
      'products_group_by' => $paged_products_group_by,
      'total' => $total_products,
      'paged' => $page,
      'total_pages' => $total_pages,
      'filter_counts' => $filter_counts,
    ];
  }
}

function generate_filter_counts($products, $active_filters)
{
  $counts = [
    'category' => [],
    'color_facet' => [],
    'brand_facet' => [],
    'sub_brand' => [],
    'collection_facet' => [],
    "application_facet" => [],
    "color_variation_facet" => [],
    "construction_facet" => [],
    "installation_facet" => [],
    "location_facet" => [],
    "species_facet" => [],
    "surface_texture_facet" => [],
    "style_facet" => [],
    "design" => [],
    "shade_facet" => [],
    "shape_facet" => [],
    "look_facet" => [],
    "material" => [],
    "material_facet" => [],
    "fiber" => [],
    "backing_facet" => [],
    "sample_available_facet" => [],
    "visualizable" => [],
    "height" => [],
    "width" => [],
    "thickness_facet" => [],
    "group" => []
  ];

  foreach ($products as $collections) {
    foreach ($collections as $product) {
      foreach ($counts as $key => &$count) {
        if (isset($product[$key])) {
          if (!isset($count[$product[$key]])) {
            $count[$product[$key]] = 0;
          }
          if (should_count_product($product, $active_filters, $key)) {
            if ($product[$key]) {
              $count[$product[$key]]++;
            } else {
              unset($count[$product[$key]]);
            }
          }
        }
      }
    }
  }
  return $counts;
}

function should_count_product($product, $active_filters, $current_filter_key)
{
  if (is_array($active_filters) && count($active_filters) > 0) {
    foreach ($active_filters as $key => $value) {
      $facet_arr = explode(",", strtolower($value));
      if ($key !== $current_filter_key && isset($product[$key]) && !in_array(strtolower($product[$key]), $facet_arr)) {
        return false;
      }
    }
  }
  return true;
}

if (!function_exists('array_group_by')) {
  function array_group_by(array $array, $key, $distinct_column = "")
  {
    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key)) {
      trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
      return null;
    }

    $func = (!is_string($key) && is_callable($key) ? $key : null);
    $_key = $key;

    $grouped = [];
    $added_arr = [];
    foreach ($array as $value) {
      $key = null;

      if (is_callable($func)) {
        $key = call_user_func($func, $value);
      } elseif (is_object($value) && property_exists($value, $_key)) {
        $key = $value->{$_key};
      } elseif (isset($value[$_key])) {
        $key = $value[$_key];
      }

      if ($key === null) {
        continue;
      }

      if ($distinct_column != "" && isset($value[$distinct_column]) && trim($value[$distinct_column]) != "") {

        if (isset($added_arr[$value[$_key]]) && in_array($value[$distinct_column], $added_arr[$value[$_key]])) {
          continue;
        } else {
          $added_arr[$value[$_key]][] = $value[$distinct_column];
        }
      }
      // var_dump($value[$_key], $added_arr, $added_arr[$value[$_key]] . ":" . $value[$skip_column]);
      $grouped[$key][] = $value;
    }
    if (func_num_args() > 3) {
      $args = func_get_args();

      foreach ($grouped as $key => $value) {
        $params = array_merge([$value], array_slice($args, 2, func_num_args()));
        $grouped[$key] = call_user_func_array('array_group_by', $params);
      }
    }
    return $grouped;
  }
}

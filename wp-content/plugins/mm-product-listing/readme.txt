=== MM Product Listing ===
Contributors: mmteam
Tags: product listing, theme customization
Requires at least: 4.6
Tested up to: 8.0
Stable tag: 1.0.42
Requires PHP: 8
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A WordPress MM Product Listing (as a plugin).

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

A WordPress MM Product Listing (as a plugin)

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about MM Product Listing? =

A WordPress MM Product Listing (as a plugin)



== Changelog ==
1.0.41
* Order sample functionality

1.0.40
* Alliance sites full setup

1.0.39
* Retail Price and Special price for PLP and PDP

1.0.38
* Redirection rules and Inspiration gallery functionality

1.0.37
* Show all instock and normal colors available on PDP

1.0.36
* Form on instock pdp

1.0.35
* Design facet and routing rules

1.0.34
* Alliance PDP logos

1.0.33
* New PDP layout for Instock with form and PDP logos for alliance sites

1.0.32
* Material facet , show sku for area rugs plp+pdp

1.0.31
* option for remove group by , sheet vinyl catalog

1.0.30
* Featured row with facets , kingsman logo

1.0.29
* CPTUI rewrite rule in search

1.0.28
* Color slider changes

1.0.27
* Area Rugs catalog , Group by size , c_limit to images

1.0.26
* Featured row, instock row update

1.0.25
* Added Routing rules

1.0.24
* LVT ,LVP and sheet vinyl

1.0.23
* Combine product category on PLP (Hardsurface)

1.0.22
* Fixed Filters empty issue

1.0.21
* Fixed syntax error

1.0.20
* Fixed syntax error

1.0.19
* Changes For SEO

1.0.18
* Change for Custom Carpet Center routing rule

1.0.17
* Added Look facet , masland logo for PDP

1.0.16 =
* Fixed Color variations and facets issue

1.0.15 =
* Color slider urls not working

1.0.14 =
* Color slider url

1.0.13 =
* Instock route using rewrite rule

1.0.12 =
* React route changes

1.0.11 =
* In stock with specific facets

1.0.10 =
* Per page changes , css changes

1.0.9 =
* Fixed repeated warnings 

1.0.8 =
* Global search in additional columns

1.0.7 =
* Paint catalog and bug fixed

1.0.6 =
* Create Rug button, Posts/pages in global search

1.0.5 =
* Another Plugin updater testing

1.0.4 =
* Plugin updater testing

1.0.3 =
* add posts/pages in Golbal search

1.0.2 =
* Add build file on plugin updater

1.0.1 =

* Test plugin updater

1.0.0 =

* initial launch

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
import React from "react";

export default function GroupFacet({ handleFilterClick, productGroup }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productGroup = sortObject(productGroup);

  return (
    <div class="facet-wrap facet-display">
      <strong>Designer Program</strong>
      <div className="facetwp-facet">
        {Object.keys(productGroup).map((group, i) => {
          if (group && productGroup[group] > 0) {
            return (
              <div>
                <span
                  id={`group-filter-${i}`}
                  key={i}
                  data-value={`${group.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("group", e.target.dataset.value)
                  }>
                  {" "}
                  {group} {` (${productGroup[group]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}

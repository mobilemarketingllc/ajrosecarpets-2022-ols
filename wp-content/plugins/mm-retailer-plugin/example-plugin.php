<?php

//require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
require_once(ABSPATH . "wp-includes/class-wpdb.php");
require_once plugin_dir_path(__FILE__) . 'class-logger.php';

class Example_Background_Processing
{

	use WP_Example_Logger;
	/**
	 * @var WP_Example_Request
	 */
	public $process_single;

	/**
	 * @var WP_Example_Process
	 */
	public $process_all;

	/**
	 * Example_Background_Processing constructor.
	 */
	public function __construct()
	{
		add_action('plugins_loaded', array($this, 'init'));
		add_action('admin_bar_menu', array($this, 'admin_bar'), 100);
		add_action('init', array($this, 'process_handler'));
	}

	/**
	 * Init
	 */
	public function init()
	{


		require_once plugin_dir_path(__FILE__) . 'class-logger.php';
		require_once plugin_dir_path(__FILE__) . 'async-requests/class-example-request.php';
		require_once plugin_dir_path(__FILE__) . 'background-processes/class-example-process.php';

		$this->process_single = new WP_Example_Request();
		$this->process_all    = new WP_Example_Process();
	}

	/**
	 * Admin bar
	 *
	 * @param WP_Admin_Bar $wp_admin_bar
	 */
	public function admin_bar($wp_admin_bar)
	{
		if (! current_user_can('manage_options')) {
			return;
		}
	}

	/**
	 * Process handler
	 */
	public function process_handler()
	{

		if (isset($_GET['process'])) {

			if ('all' === $_GET['process']) {

				$this->handle_all($pro_cat, $pro_brand);
			}
			if ('allcsv' === $_GET['process']) {

				$this->handle_all_csv($pro_cat, $pro_brand);
			}
		}
	}


	// Function to filter selected columns
	function get_selected_columns($handle, $columns)
	{
		return array_map(function ($item) use ($columns) {
			$filtered = [];
			foreach ($columns as $column) {
				if (array_key_exists($column, $item)) {
					$filtered[$column] = $item[$column];
				}
			}
			return $filtered;
		}, $handle);
	}



	/**
	 * Handle all
	 */

	public function handle_all($pro_cat, $pro_brand, $handle = array())
	{
		write_log("inside hanle all" . $pro_cat);

		global $wpdb;

		if (isset($_GET['product_brand'])) {
			if ($_GET['main_category'] != '') {
				$pro_cat = $_GET['main_category'];
			}
		}

		if (isset($_GET['product_brand'])) {
			if ($_GET['product_brand']) {
				$pro_brand = $_GET['product_brand'];
			}
		}


		if (count($handle) == 0) {
			$file_name = $pro_cat == "area_rugs" ? "area_rugs_" . trim($pro_brand) . '.json' : $pro_brand . '.json';
			$upload_dir = wp_upload_dir();
			$upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name;
			$strJsonFileContents = file_get_contents($upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name);

			$handle_data = str_replace('&quot;', '"', $strJsonFileContents);

			$handle = json_decode(($handle_data), true);
		}

		$selected_columns = ['sku', 'name', 'status', 'swatch', 'url_key', 'manufacturer', 'in_stock'];

		$filtered_data = $this->get_selected_columns($handle, $selected_columns);

		switch (json_last_error()) {
			case JSON_ERROR_NONE:
				echo "No errors";
				break;
			case JSON_ERROR_DEPTH:
				echo "Maximum stack depth exceeded";
				break;
			case JSON_ERROR_STATE_MISMATCH:
				echo "Invalid or malformed JSON";
				break;
			case JSON_ERROR_CTRL_CHAR:
				echo "Control character error";
				break;
			case JSON_ERROR_SYNTAX:
				echo "Syntax error";
				break;
			case JSON_ERROR_UTF8:
				echo "Malformed UTF-8 characters";
				break;
			default:
				echo "Unknown error";
				break;
		}

		$_session['sfn_post_type'] = $pro_cat;

		if ($filtered_data) {
			$k = 1;
			foreach ($filtered_data  as $row) {
				write_log('current loop ->' . $k);

				$valuesData = $this->insert_product($row, $pro_cat);

				if ($k % 1000 == 0) {
					sleep(3);
				}

				$k++;
			}
		}


		$product_sync_table = $wpdb->prefix . "sfn_sync";
		$status_sync = 'success - ' . $k;
		$product_json =  json_decode(get_option('product_json'));

		$categorymapping = array(
			"carpeting" => "carpet",
			"hardwood" => "hardwood_catalog",
			"laminate" => "laminate_catalog",
			"lvt" => "luxury_vinyl_tile",
			"tile" => "tile_catalog",
			"sheet" => "sheet_vinyl",
			"area_rugs" => "area_rugs",
		);

		$b_category = array_search($pro_cat, $categorymapping);

		$brand_array = array_reverse(getArrayFiltered('productType', $b_category, $product_json));

		if ($pro_cat != 'carpeting') {

			$wpdb->query("DELETE  FROM {$product_sync_table} WHERE product_category = '{$pro_cat}'");

			foreach ($brand_array as $brand) {

				$brand_list .= $brand->manufacturer . ',';
			}

			$brand_list = rtrim($brand_list, ',');

			$wpdb->insert($product_sync_table, array('product_category' => $pro_cat, 'product_brand' => $brand_list, 'sync_status' => $status_sync), array('%s', '%s', '%s'));
		} else {

			$wpdb->query("DELETE  FROM {$product_sync_table} WHERE product_brand = '{$pro_brand}' and  product_category = '{$pro_cat}'");
			$wpdb->insert($product_sync_table, array('product_category' => $pro_cat, 'product_brand' => $pro_brand, 'sync_status' => $status_sync), array('%s', '%s', '%s'));
		}


		write_log('Sync Completed - ' . $pro_brand);
	}
}

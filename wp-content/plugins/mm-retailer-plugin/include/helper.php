<?php

function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    if (strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber) - 10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+' . $countryCode . ' (' . $areaCode . ') ' . $nextThree . '-' . $lastFour;
    } elseif (strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '(' . $areaCode . ') ' . $nextThree . '-' . $lastFour;
    } elseif (strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree . '-' . $lastFour;
    }

    return $phoneNumber;
}

function storelocation_address($arg)
{

    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
    $locations = "<ul class='storename'><li>";


    for ($i = 0; $i < count($website->locations); $i++) {
        $location_name = isset($website->locations[$i]->name) ? $website->locations[$i]->name : "";

        $location_ids = isset($website->locations[$i]->id) ? $website->locations[$i]->id : "";

        if ($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name != '') {


            if (in_array(trim($location_name), $arg) || in_array(trim($location_ids), $arg)) {

                $location_name = get_bloginfo('name');

                $location_address_url =  $location_name;
                $location_address_url  .= isset($website->locations[$i]->address) ? $website->locations[$i]->address . " " : "";
                $location_address_url .= isset($website->locations[$i]->city) ? $website->locations[$i]->city . " " : "";
                $location_address_url .= isset($website->locations[$i]->state) ? $website->locations[$i]->state . " " : "";
                $location_address_url .= isset($website->locations[$i]->postalCode) ? $website->locations[$i]->postalCode . " " : "";

                $location_address  = isset($website->locations[$i]->address) ? "<p>" . $website->locations[$i]->address . "</p>" : "";
                $location_address .= isset($website->locations[$i]->city) ? "<p>" . $website->locations[$i]->city . ", " : "<p>";
                $location_address .= isset($website->locations[$i]->state) ? $website->locations[$i]->state : "";
                $location_address .= isset($website->locations[$i]->postalCode) ? " " . $website->locations[$i]->postalCode . "</p>" : "</p>";

                $location_inline_address = '';
                $location_inline_address .= isset($website->locations[$i]->address) ? $website->locations[$i]->address . ", " : "";
                $location_inline_address .= isset($website->locations[$i]->city) ? $website->locations[$i]->city . ", " : "";
                $location_inline_address .= isset($website->locations[$i]->state) ? $website->locations[$i]->state : "";
                $location_inline_address .= isset($website->locations[$i]->postalCode) ? " " . $website->locations[$i]->postalCode : "";

                $location_phone  = "";
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                    $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                } else if (isset($website->locations[$i]->phone) && $website->locations[$i]->forwardingPhone = "") {

                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                } else {

                    $forwarding_tel = "#";
                    $forwarding_phone  = formatPhoneNumber(8888888888);
                }


                if (in_array("loc", $arg)) {

                    if (in_array('nolink', $arg)) {
                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address">' . $location_address . '</div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';
                    } else {

                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address"><a href="https://maps.google.com/maps/?q=' . urlencode($location_address_url) . '" target="_blank" rel="noopener" ' . '>' . $location_address . '</a></div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';
                    }
                }
                if (in_array("inline_loc", $arg)) {

                    if (in_array('nolink', $arg)) {
                        $locations .= '<p class="inline_address">' . $location_inline_address . '</p>';
                    } else {
                        $locations .= '<p class="inline_address"><a href="https://maps.google.com/maps/?q=' . urlencode($location_address_url) . '" target="_blank" rel="noopener" ' . '>' . $location_inline_address . '</a></p>';
                    }
                }
                if (in_array("forwardingphone", $arg)) {

                    if (in_array('nolink', $arg)) {
                        $locations .= "<div class='phone'><span>" . $forwarding_phone . "</span></div>";
                    } else {
                        $locations .= "<div class='phone'><a href='tel:" . $forwarding_tel . "'><span>" . $forwarding_phone . "</span></a></div>";
                    }
                }
                if (in_array("ohrs", $arg)) {
                    $locations .= '<div class="store-opening-hrs-container">';
                    $locations .= '<ul class="store-opening-hrs">';

                    for ($j = 0; $j < count($website->locations[$i]->hours); $j++) {

                        //   write_log( $website->locations[$i]->hours[$j]->day);

                        if (isset($website->locations[$i]->hours[$j]->hours)) {

                            $locations .= '<li>' . $website->locations[$i]->hours[$j]->day . ': <span>' . $website->locations[$i]->hours[$j]->hours . '</span></li>';
                        }
                    }
                    $locations .= '</ul>';
                    $locations .= '</div>';
                }
                if (in_array("dir", $arg)) {

                    $locations .= '<div class="direction">';
                    $locations .= '<a href="https://maps.google.com/maps/?q=' . urlencode($location_address_url) . '" target="_blank" rel="noopener" class="fl-button" role="button">
                                            <span class="button-text">Get Directions</span></a>';
                    $locations .= '</div>';
                }
                if (in_array("dirlink", $arg)) {

                    $locations .= 'https://maps.google.com/maps/?q=' . urlencode($location_address_url);
                }
                if (in_array("map", $arg)) {

                    $locations .= '<div class="map-container">
                        <iframe src="https://www.google.com/maps/embed/v1/place?key=' . GOOGLE_MAP_KEY . '&amp;q=' . urlencode($location_address_url) . '"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                        </div>';
                }
                if (in_array("phone", $arg)) {
                    $locations .= "<div class='phone'><a href='tel:" . $location_tel . "'>" . "<span>" . $location_phone . "</span></a></div>";
                }
            }
        }
    }
    if ((!in_array("dir", $arg)) && (!in_array("dirlink", $arg))) {
        $locations .= '</li>';
        $locations .= '</ul>';
    }

    return $locations;
}
add_shortcode('storelocation_address', 'storelocation_address');


function contact_phonenumber($atts)
{
    if (get_option('api_contact_phone')) {
        return '<a href="tel:' . get_option('api_contact_phone') . '" target="_self" class="fl-icon-text-link fl-icon-text-wrap">' . get_option('api_contact_phone') . '</a>';
    } else {
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
    }
}
add_shortcode('contactPhnumber', 'contact_phonenumber');


function getSocailIcons()
{
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if (isset($details)) {
        $return  = '<ul class="social-icons">';
        foreach ($details as $key => $value) {
            if ($value->group == "social") {

                if ($value->platform == "linkedIn") {
                    $value->platform = "linkedin";
                }


                if ($value->active && $value->platform == "twitter") {

                    $return  .= '<li><a href="' . $value->url . '" target="_blank" rel="noopener" title="' . $value->platform . '"><i class="fa-brands fa-x-' . strtolower($value->platform) . '"></i></a></li>';
                } elseif ($value->active) {

                    $return  .= '<li><a href="' . $value->url . '" target="_blank" rel="noopener" title="' . $value->platform . '"><i class="fab fa-' . strtolower($value->platform) . '"></i></a></li>';
                }
            }
        }
        $return  .= '</ul>';
    }
    return $return;
}
add_shortcode('getSocailIcons', 'getSocailIcons');


function create_post_type()
{
    register_post_type(
        'store-locations',
        array(
            'labels' => array(
                'name' => __('Store Locations'),
                'singular_name' => __('Store Location')
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}
add_action('init', 'create_post_type');

function contactInformation($atts)
{
    $contacts = json_decode(get_option('website_json'));
    $info = "";
    if (is_array($contacts->contacts)) {
        if (in_array("withicon", $atts)) {
            for ($i = 0; $i < count($contacts->contacts); $i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon icon-link' href='mailto:" . $contacts->contacts[$i]->email . "'>" . $contacts->contacts[$i]->email . "</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon icon-link' href='tel:" . preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone) . "'>" . formatPhoneNumber($contacts->contacts[$i]->phone) . "</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        } else {
            for ($i = 0; $i < count($contacts->contacts); $i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:" . $contacts->contacts[$i]->email . "'>" . $contacts->contacts[$i]->email . "</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:" . preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone) . "'>" . formatPhoneNumber($contacts->contacts[$i]->phone) . "</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
}
add_shortcode('contactInformation', 'contactInformation');


function locationInformation($atts)
{
    $website = json_decode(get_option('website_json'));

    for ($i = 0; $i < count($website->locations); $i++) {

        if ($website->locations[$i]->type == 'store') {

            if (in_array($website->locations[$i]->name, $atts)) {
                if (in_array("license", $atts)) {
                    $info  .= "<div class='store-license'>" . $website->locations[$i]->licenseNumber . "</div>";
                }
                if (in_array("phone", $atts)) {
                    $info  .= "<a class='phone-icon-with icon-link-with' href='tel:" . preg_replace('/[^0-9]/', '', $website->locations[$i]->phone) . "'>" . formatPhoneNumber($website->locations[$i]->phone) . "</a>";
                }
            }
        }
    }
    return $info;
}
add_shortcode('locationInformation', 'locationInformation');


function hookSharpStrings()
{

    $website_json =  json_decode(get_option('website_json'));
    $sharpspringDomainId = $sharpspringTrackingId = "";
    for ($j = 0; $j < count($website_json->sites); $j++) {

        if ($website_json->sites[$j]->instance == ENV) {

            $sharpspringTrackingId = $website_json->sites[$j]->sharpspringTrackingId;
            $sharpspringDomainId = $website_json->sites[$j]->sharpspringDomainId;
        }
    }

    if (isset($sharpspringTrackingId) && trim($sharpspringTrackingId) != "") {

?>
        <script type="text/javascript">
            var _ss = _ss || [];

            _ss.push(['_setDomain', 'https://<?php echo $sharpspringDomainId; ?>.marketingautomation.services/net']);

            _ss.push(['_setAccount', '<?php echo $sharpspringTrackingId; ?>']);

            _ss.push(['_trackPageView']);

            (function() {

                var ss = document.createElement('script');

                ss.type = 'text/javascript';
                ss.async = true;

                ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php echo $sharpspringDomainId; ?>.marketingautomation.services/client/ss.js?ver=1.1.1';

                var scr = document.getElementsByTagName('script')[0];

                scr.parentNode.insertBefore(ss, scr);

            })();
        </script>
    <?php
    }
}
add_action('wp_head', 'hookSharpStrings');

function getRetailerInfo($arg)
{

    $website = json_decode(get_option('website_json'));

    for ($i = 0; $i < count($website->locations); $i++) {

        if ($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name != '') {

            $website = json_decode(get_option('website_json'));
            if (in_array('city', $arg)) {
                $info =  isset($website->locations[$i]->city) ? "<span class='city retailer'>" . $website->locations[$i]->city . "</span>" : "";
            } else if (in_array('state', $arg)) {
                $info =  isset($website->locations[$i]->state) ? "<span class='state retailer'>" . $website->locations[$i]->state . "</span>" : "";
            } else if (in_array('zipcode', $arg)) {
                $info =  isset($website->locations[$i]->postalCode) ? "<span class='zipcode retailer'>" . $website->locations[$i]->postalCode . "</span>" : "";
            } else if (in_array('legalname', $arg)) {
                $info =  isset($website->name) ? "<span class='name retailer'>" . $website->name . "</span>" : "";
            } else if (in_array('address', $arg)) {
                $info =  isset($website->locations[$i]->address) ? "<span class='street_address retailer'>" . $website->locations[$i]->address . "</span>" : "";
            } else if (in_array('phone', $arg)) {
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (in_array('nolink', $arg)) {
                    $info =  isset($website->locations[$i]->phone) ? "<span class='phone retailer'>" . $location_phone . "</span>" : "";
                } else {

                    $info =  isset($website->locations[$i]->phone) ? "<a href='tel:" . $location_tel . "' class='phone retailer'>" . "<span>" . $location_phone . "</span></a>" : "";
                }
            } else if (in_array('forwarding_phone', $arg)) {
                if (isset($website->locations[$i]->forwardingPhone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                }
                if (in_array('nolink', $arg)) {
                    $info =  isset($website->locations[$i]->forwardingPhone) ? "<span class='phone retailer'>" . $location_phone . "</span>" : "";
                } else {

                    $info =  isset($website->locations[$i]->forwardingPhone) ? "<a href='tel:" . $location_tel . "' class='phone retailer' >" . "<span>" . $location_phone . "</span></a>" : "";
                }
            } else if (in_array('companyname', $arg)) {
                for ($j = 0; $j < count($website->sites); $j++) {

                    if ($website->sites[$j]->instance == ENV) {
                        $info =  isset($website->sites[$j]->name) ? "" . $website->sites[$j]->name . "" : "";
                        break;
                    }
                }
            } else if (in_array('site_url', $arg)) {
                for ($j = 0; $j < count($website->sites); $j++) {

                    if ($website->sites[$j]->instance == ENV) {
                        $info =  isset($website->sites[$j]->url) ? "<span class='site_url retailer'>" . $website->sites[$j]->url . "</span>" : "";
                        break;
                    }
                }
            } else if (in_array('seo_location', $arg)) {

                if ($website->seoLocation != '') {

                    $info =   str_replace(array("\n", "\r"), '', $website->seoLocation);
                } else {

                    $info =   $website->locations[$i]->city . ', ' . $website->locations[$i]->state;
                }
                break;
            }
        }
    }

    return $info;
}

add_shortcode('Retailer', 'getRetailerInfo');

//filters for shordcode work in seo title and descriptions
add_filter('wpseo_title', 'do_shortcode');
add_filter('wpseo_metadesc', 'do_shortcode');

function updateSaleCoupanInformation($arg)
{
    $promos = json_decode(get_option('promos_json'));

    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);


    if (isset($_GET['promo']) && !empty($_GET['promo'])) {

        $promo_code = $_GET['promo'];
    } else {

        $promo_code = array_search($high_priprity, $salespriority);
    }


    $div = '';

    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";
    $loc_country = $website_json->locations[0]->country;
    if ($loc_country == 'US') {
        $en = 'en_us';
    } elseif ($loc_country == 'CA') {
        $en = 'en_ca';
    }


    $seleinformation = json_decode(get_option('saleinformation'));
    //update_option('saleinformation',json_encode($seleinformation));
    $seleinformation = (object)$seleinformation;

    $salebannerdata = json_decode(get_option('salebannerdata'));

    if (in_array('rugshop', $arg) && in_array('salebanner', $arg)) {


        foreach ($salebannerdata as $bannerinfo) {


            if ($bannerinfo->rugShop == 'true') {

                $banner_img_deskop = $bannerinfo->images->desktop;
                $banner_img_mobile = $bannerinfo->images->mobile;

                if ($bannerinfo->link == '') {

                    $banner_link = 'https://rugs.shop/' . $en . '/?store=' . $rugAffiliateCode;
                } else {

                    $banner_link = 'https://rugs.shop/' . $en . '/' . $bannerinfo->link . '?store=' . $rugAffiliateCode;
                }
            }
        }

        $div = "<div class='salebanner banner-deskop fullwidth rugshopbanner'><a target='_blank' rel='noopener' href='" . $banner_link . "'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/" . $banner_img_deskop . " 4025w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/" . $banner_img_deskop . " 3019w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . " 2013w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . " 1006w' 
            
            src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . "'  style='width:100%;text-align:center' alt='sales' />
            </a></div>";
        $div .= "<div class='salebanner banner-mobile rugshopbanner-mobile'><a target='_blank' rel='noopener' href='" . $banner_link . "'><img src='" . $banner_img_mobile . "' alt='sales' /></a></div>";
    } else {


        if (in_array('full', $arg) || in_array('salebanner', $arg)) {

            if ($salebannerdata != '') {

                foreach ($salebannerdata as $bannerinfo) {


                    if ($bannerinfo->priority == $high_priprity) {

                        $banner_img_deskop = $bannerinfo->images->desktop;
                        $banner_img_mobile = $bannerinfo->images->mobile;
                        $banner_link = $bannerinfo->link;
                    }
                }

                if (@$banner_img_deskop != '' && $banner_img_mobile != '') {

                    $div = "<div class='salebanner banner-deskop fullwidth'><a href='" . $banner_link . "'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/" . $banner_img_deskop . " 4025w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/" . $banner_img_deskop . " 3019w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . " 2013w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . " 1006w' 
                    
                    src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . "'  style='width:100%;text-align:center' alt='sales' />
                    </a></div>";


                    $div .= "<div class='salebanner banner-mobile'><a href='" . $banner_link . "'><img src='" . $banner_img_mobile . "' alt='sales' /></a></div>";
                }
            }
        }
        if (in_array('fixed', $arg) || in_array('salebanner', $arg)) {
            if ($salebannerdata != '') {

                foreach ($salebannerdata as $bannerinfo) {

                    if ($bannerinfo->priority == $high_priprity) {

                        $banner_img_deskop =  $bannerinfo->images->desktop;
                        $banner_img_mobile = $bannerinfo->images->mobile;
                        $banner_link = $bannerinfo->link;
                    }
                }

                if (@$banner_img_deskop != '' && $banner_img_mobile != '') {

                    $div = "<div class='salebanner banner-deskop fixed'><a href='" . $banner_link . "'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/" . $banner_img_deskop . " 4025w, 
                        https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/" . $banner_img_deskop . " 3019w, 
                        https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . " 2013w, 
                        https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . " 1006w' 
                        
                        src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/" . $banner_img_deskop . "'  style='width:100%;text-align:center' alt='sales' />
                        </a></div>";


                    $div .= "<div class='salebanner banner-mobile'><a href='" . $banner_link . "'><img src='" . $banner_img_mobile . "' alt='sales' /></a></div>";
                }
            }
        }
    }
    if (in_array('categorybanner', $arg)) {

        if (isset($seleinformation->category_banner_img_deskop) && $seleinformation->category_banner_img_deskop != "") {
            $div = "<div class='salebanner ts banner-deskop'><a href='" . $seleinformation->banner_link . "'><img src='" . $seleinformation->category_banner_img_deskop . "' alt='sales' /></a></div>";
        }
        if (isset($seleinformation->category_banner_img_mobile) && $seleinformation->category_banner_img_mobile != "")
            $div .= "<div class='salebanner banner-mobile'><a href='" . $seleinformation->banner_link . "'><img src='" . $seleinformation->category_banner_img_mobile . "' alt='sales' /></a></div>";
    }
    if (in_array('heading', $arg)) {
        if (isset($seleinformation->$promo_code->saveupto_doller)) {
            $price = isset($seleinformation->$promo_code->saveupto_doller) ? $seleinformation->$promo_code->saveupto_doller : "";
            if (@$_COOKIE['keyword'] == ""  && @$_COOKIE['brand'] == "") {

                $div = '<h1 class="googlekeyword">' . $price . '</h1>';
            } else {
                $keyword = $_COOKIE['keyword'];
                $brand = $_COOKIE['brand'];
                $div = '<h1 class="googlekeyword">' . $price . ' on ' . $brand . ' ' . $keyword . '<h1>';
            }
        } else {
            $div = '<h1 class="googlekeyword">Check back soon for new promotions</h1>';
        }
    }


    if (in_array('subheading', $arg)) {

        if (isset($seleinformation->$promo_code->subheading)) {

            $div = $seleinformation->$promo_code->subheading;
        } else {

            $div = 'Fill out this form and get your coupon!';
        }
    }



    if (in_array('form_subheading', $arg)) {
        // write_log($seleinformation);
        // write_log($salebannerdata);
        if (isset($seleinformation->$promo_code->formTitle)) {
            $div = $seleinformation->$promo_code->formTitle;
        } else {
            $div = "Fill out the form below and we'll get back to you shortly.";
        }
    }

    if (in_array('content', $arg)) {
        if ($seleinformation->$promo_code->formType != "none") {
            $string = isset($seleinformation->$promo_code->content) ? $seleinformation->$promo_code->content : "";
            $div = '<div class="form-content">' . $string . '</div>';
        } else {
            $div = '<div class="form-content">Although we aren’t running a specific promotion right now, there are still ways to save! Stop by our showroom and meet with one of our flooring experts to learn more about our exceptional products. Our professional team can help you find the ideal flooring for any room in your home.</div>';
        }
    }
    if (in_array('image_onform', $arg)) {

        if ((in_array('backgrondimage', $arg) || in_array('background_img', $arg)) && isset($seleinformation->$promo_code->image_onform) && isset($seleinformation->$promo_code->image_onform_mobile)) {
            $div = "<div class='salebanner floor-coupon-img  banner-deskop'  id='backgrondimage' ><img src='" . $seleinformation->$promo_code->image_onform . "' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile' ><img src='" . $seleinformation->$promo_code->image_onform_mobile . "' alt='sales' /></div>";
        } else {

            $div = "<div class='salebanner floor-coupon-img  banner-deskop' id='nosalebgelem'><img src='https://mmllc-images.s3.us-east-2.amazonaws.com/beautifall-financing/nopromo-desktop.jpg' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'><img src='https://mmllc-images.s3.us-east-2.amazonaws.com/beautifall-financing/no-promo-mobile.jpg' alt='sales' /></div>";
        }
    }

    if (in_array('message', $arg) && isset($seleinformation->$promo_code->message)) {

        $div = "<div class='message'>" . $seleinformation->$promo_code->message . "</div>";
    }
    if (in_array('navigation', $arg) && isset($seleinformation->navigation_img)) {
        if (in_array('image', $arg)) {
            $div = "<div class='navigation_img'><a href='" . $seleinformation->navigation_link . "'><img src='" . $seleinformation->navigation_img . "' alt='sales' /></a></div>";
        } else if (in_array('text', $arg)) {
            $div = "<div class='navigation_img'><a href='" . $seleinformation->navigation_link . "'>'" . $seleinformation->navigation_text . "</a></div>";
        }
    }
    if (in_array('homepage_banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                if (in_array('rugshop', $arg)) {

                    $div = "<div class='rugshop floor-coupon-img' id='homepage-banner-bg' data-bg='" . $seleinformation->background_image . "'><a href='https://rugs.shop/$en/?store=" . $rugAffiliateCode . "' rel='noopener' target='_blank'><img class='salebanner-slide' src='" . $slide->slider_coupan_img . "' alt='sales' /></a></div>";
                    break;
                } else {

                    $div = "<div class='coupon floor-coupon-img' id='homepage-banner-bg' data-bg='" . $seleinformation->background_image . "'><a href='/flooring-coupon/'><img class='salebanner-slide' src='" . $slide->slider_coupan_img . "' alt='sales' /></a></div>";
                    break;
                }
            }
        }
    }
    if (in_array('banner', $arg)) {
        if (isset($seleinformation->$promo_code->slider) && count($seleinformation->$promo_code->slider) > 0) {
            foreach ($seleinformation->$promo_code->slider as $slide) {
                $div = "<div class='banner-image' style=''><a href='" . $slide->link . "'><img class='salebanner-slide' src='" . $slide->slider_coupan_img . "' alt='sales' /></a></div>";
                break;
            }
        }
    }


    if (in_array('print_coupon', $arg)) {
        $salebannerdata = json_decode(get_option('salebannerdata'));
        if (is_array($promos) && count($promos) > 0) {
            foreach ($promos as $promo) {
                if ($promo->formName == "Shaw Coupon") {
                    if (is_array($promo->widgets) && count($promo->widgets) > 0) {
                        foreach ($promo->widgets as $widget) {
                            if (is_array($widget->images) && count($widget->images) > 0) {
                                foreach ($widget->images as $image) {
                                    if ($image->type == "print") {

                                        if (strpos($image->url, "http") == false) {
                                            $img = "https://" . $image->url;
                                        } else {
                                            $img = $image->url;
                                        }
                                        $div = "<a href='" . $img . "' target='_blank' rel='noopener' class='print_coupon_btn btn fl-button'>PRINT COUPON</a>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (in_array('popup_img', $arg)) {
        $salebannerdata = json_decode(get_option('salebannerdata'));
        if (is_array($promos) && count($promos) > 0) {
            foreach ($promos as $promo) {
                if ($promo->formName == "Shaw Coupon") {
                    if (is_array($promo->widgets) && count($promo->widgets) > 0) {
                        foreach ($promo->widgets as $widget) {
                            if ($widget->type == "popup" && is_array($widget->images) && count($widget->images) > 0) {
                                foreach ($widget->images as $image) {
                                    if ($image->type == "graphic") {
                                        if (strpos($image->url, "http") == false) {
                                            $img = "https://" . $image->url;
                                        } else {
                                            $img = $image->url;
                                        }
                                        $div = "<a href='/flooring-coupon/?promo=" . $promo->promoCode . "' class='popup_img_link'><img class='popup_img' src='" . $img . "' alt='Shaw Popup Image' width='850' height='315'></a>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return $div;
}

add_shortcode('coupon', 'updateSaleCoupanInformation');

function copyrightstext($arg)
{
    $result = "<p>Copyright ©" . do_shortcode('[fl_year]') . " " . get_bloginfo() . ". All Rights Reserved.</p>";
    return $result;
}

add_shortcode('copyrights', 'copyrightstext');


//Colorwall Home Page Banner Shortcode

function colorwall_home_banner($arg)
{
    $product_json =  json_decode(get_option('product_json'));

    foreach ($product_json as $data) {
        foreach ($data as $key => $value) {
            if ($key == "manufacturer" &&   $value == "coretec")
                $isCoretec = true;
        }
    }
    $coretec_colorwall =  get_option('coretec_colorwall');
    if ($coretec_colorwall == '1' && $isCoretec) {

        $content = do_shortcode('[fl_builder_insert_layout slug="colorwall-mobile"]');
        $content .=  do_shortcode('[fl_builder_insert_layout slug="colorwall-desktop-banner"]');

        return $content;
    }
}

add_shortcode('colorwallhomebanner', 'colorwall_home_banner');


//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumbs()
{
    if (function_exists('yoast_breadcrumb') && ! is_front_page()) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
    }
}
add_action('fl_content_open', 'bbtheme_yoast_breadcrumbs');

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter('wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail');

function wpse_100012_override_yoast_breadcrumb_trail($links)
{
    global $post;

    if (is_singular('hardwood_catalog')) {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/hardwood/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -2, $breadcrumb);
    } elseif (is_singular('carpeting')) {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/carpet/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -2, $breadcrumb);
    } elseif (is_singular('luxury_vinyl_tile')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/vinyl/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -2, $breadcrumb);
    } elseif (is_singular('laminate_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/laminate/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -2, $breadcrumb);
    } elseif (is_singular('tile_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/tile/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -2, $breadcrumb);
    } elseif (is_singular('solid_wpc_waterproof')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice($links, 1, -2, $breadcrumb);
    }

    return $links;
}


//This filter is executed before displaying each field and can be used to dynamically populate fields with a default value.

add_filter('gform_field_value_salepromocode', 'populate_salepromocode');
function populate_salepromocode($value)
{
    global $wp;
    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;


    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);

    $p_code = 'nopromo';

    if (isset($_GET['promo']) && $_GET['promo'] != '') {
        $found_promo = false;
        foreach ($saleinformation as $promo) {
            if ($promo->promoCode == $_GET['promo']) {
                $found_promo = true;
                break;
            }
        }
        if ($found_promo == false) {
            $url = home_url($wp->request);
            wp_redirect($url);
            exit;
        }

        $p_code = $_GET['promo'];
    } elseif (is_array($saleinformation) && count($saleinformation) > 0) {


        foreach ($saleinformation as $promo) {

            if ($promo->priority == $high_priprity && $promo->formType != "none") {

                $p_code = $promo->promoCode;
                break;
            } else if ($promo->formType == "none") {

                $p_code = "nopromo";
            }
        }
    } else {

        $p_code = "nopromo";
    }

    return $p_code;
}

add_filter('gform_field_value_saleformtype', 'populate_saleformtype');
function populate_saleformtype($value)
{
    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);
    if (is_array($saleinformation) && count($saleinformation) > 0) {
        foreach ($saleinformation as $sale) {

            if (isset($_GET['promo'])) {
                if ($sale->promoCode == $_GET['promo']) {

                    return $sale->formType;
                }
            } else {

                if ($sale->priority == $high_priprity) {

                    return $sale->formType;
                }
            }
        }
    } else {
        return "defaultfrm";
    }
}

// Add Yoast SEO sitemap to virtual robots.txt file
function surbma_yoast_seo_sitemap_to_robotstxt_function($output)
{

    $homeURL = get_home_url();
    $output = '';
    $output .= 'User-agent: *' . PHP_EOL;
    if (get_option('blog_public') == '0') {
        $output .= 'Disallow: /' . PHP_EOL;
    } else {
        $output .= 'Disallow: /wp-admin/' . PHP_EOL;
        $output .= "Allow: /wp-admin/admin-ajax.php" . PHP_EOL;
        $output .= "Sitemap: $homeURL/sitemap_index.xml" . PHP_EOL;
    }


    return $output;
}
add_filter('robots_txt', 'surbma_yoast_seo_sitemap_to_robotstxt_function', 10, 1);



//accessiBe script hook with cde parameter
$website_json_data = json_decode(get_option('website_json'));

foreach ($website_json_data->sites as $site_cloud) {

    if ($site_cloud->instance == 'prod') {

        if ($site_cloud->accessibe == 'true') {

            add_action('fl_body_close', 'accessiBe_custom_footer_js');
        }
    }
}

function accessiBe_custom_footer_js()
{

    echo "<script > (function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/api/app/assets/js/acsb.js';
    script.async = true;
    script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink: '',
            feedbackLink: '',
            footerHtml: 'Enable powered by <a href=https://enablemysite.com/ target=_blank>enablemysite</a>',
            hideMobile: false,
            hideTrigger: false,
            language: 'en',
            position: 'left',
            leadColor: '#c8c8c8',
            triggerColor: '#146ff8',
            triggerRadius: '50%',
            triggerPositionX: 'left',
            triggerPositionY: 'bottom',
            triggerIcon: 'wheel_chair4',
            triggerSize: 'medium',
            triggerOffsetX: 20,
            triggerOffsetY: 20,
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'left',
                triggerPositionY: 'center',
                triggerOffsetX: 0,
                triggerOffsetY: 0,
                triggerRadius: '0'
            }
        });
    };
}(document, 'script')); </script>";
}

add_action('fl_body_close', 'mmsession_custom_footer_js');
function mmsession_custom_footer_js()
{

    echo "<script src='https://session.mm-api.agency/js/mmsession.js'></script>";
}

//chat meter Shortcode code start here

function chatmeter_function($arg)
{
    $website_json =  json_decode(get_option('website_json'));


    foreach ($website_json->sites as $site_chat) {

        if ($site_chat->instance == 'prod') {

            return $site_chat->chatmeter;
        }
    }
}

add_shortcode('chat_meter', 'chatmeter_function');

//chat meter Shortcode code end here

//Wells Fargo Financing link Shortcode code start here

function wells_fargo_function($arg)
{
    $website_json =  json_decode(get_option('website_json'));

    $finlink = '';

    if ($website_json->financeSync || $website_json->financeWF) {

        if ($website_json->financeWF) {
            $finlink = $website_json->financeWF;
        } else {
            $finlink = $website_json->financeSync;
        }

        $content = '<a href="' . $finlink . '" target="_blank" rel="noopener" class="fl-button financeSync" role="button" rel="noopener">
                         <span class="fl-button-text">' . $arg[0] . '</span>
                    </a>';
    } else {

        $content = '';
    }

    return $content;
}

add_shortcode('wells_fargo_finance', 'wells_fargo_function');



function wells_fargo_function_link($arg)
{
    $website_json =  json_decode(get_option('website_json'));

    $finlink = '';

    if ($website_json->financeSync || $website_json->financeWF) {

        if ($website_json->financeWF) {
            $finlink = $website_json->financeWF;
        } else {
            $finlink = $website_json->financeSync;
        }
    } else {

        $finlink = '';
    }

    return $finlink;
}

add_shortcode('wells_fargo_finance_link', 'wells_fargo_function_link');

//Synchrony Financing link Shortcode code start here

function synchrony_function($arg)
{
    $website_json =  json_decode(get_option('website_json'));

    $finlink = '';

    if ($website_json->financeSync || $website_json->financeWF) {

        if ($website_json->financeSync) {
            $finlink = $website_json->financeSync;
        } else {
            $finlink = $website_json->financeWF;
        }

        $content = '<a href="' . $website_json->financeSync . '" target="_blank" rel="noopener" class="fl-button financeSync" role="button" rel="noopener">
                         <span class="fl-button-text">' . $arg[0] . '</span>
                    </a>';
    } else {

        $content = '';
    }

    return $content;
}

add_shortcode('synchrony_finance', 'synchrony_function');


function synchrony_function_link($arg)
{
    $website_json =  json_decode(get_option('website_json'));

    $finlink = '';

    if ($website_json->financeSync || $website_json->financeWF) {

        if ($website_json->financeSync) {
            $finlink = $website_json->financeSync;
        } else {
            $finlink = $website_json->financeWF;
        }
    } else {

        $finlink = '';
    }

    return $finlink;
}

add_shortcode('synchrony_finance_link', 'synchrony_function_link');

//Area rug affilate Button

function arearug_affiliate_button($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";

    $loc_country = $website_json->locations[0]->country;
    if ($loc_country == 'US') {
        $en = 'en_us';
    } elseif ($loc_country == 'CA') {
        $en = 'en_ca';
    }

    if ($rugAffiliateCode) {

        $areabtn = '<div class="rugshopbtn-wrapper">';
        $areabtn .= '<a href="https://rugs.shop/' . $en . '/?store=' . $rugAffiliateCode . '" target="_blank" rel="noopener" class="fl-button rugshopbtn" role="button">
                            <span class="button-text">' . $arg[0] . '</span></a>';
        $areabtn .= '</div>';
    }
    return $areabtn;
}

add_shortcode('rugshop_button', 'arearug_affiliate_button');

//Area rug affilate link
function arearug_affiliate_link($arg)
{

    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";
    $loc_country = $website_json->locations[0]->country;
    if ($loc_country == 'US') {
        $en = 'en_us';
    } elseif ($loc_country == 'CA') {
        $en = 'en_ca';
    }

    if ($rugAffiliateCode) {

        $arealnk = '<a href="https://rugs.shop/' . $en . '/?store=' . $rugAffiliateCode . '" target="_blank" rel="noopener" class="rugshoplink">' . $arg[0] . '</a>';
    }


    return $arealnk;
}

add_shortcode('rugshop_link', 'arearug_affiliate_link');

//Area rug affilate code
function arearug_affiliate_code($arg)
{

    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";

    if ($rugAffiliateCode) {

        return $rugAffiliateCode;
    } else {

        return '';
    }
}
add_shortcode('affiliate_code', 'arearug_affiliate_code');



//Check image path for swatch and gallery image

function get_image_url($image, $height, $width)
{

    if (get_option('cloudinary', true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false) {


        if (strpos($image, 's7.shawimg.com') !== false) {
            if (strpos($image, 'http') === false) {
                $image = "http://" . $image;
            }
        } else {
            if (strpos($image, 'http') === false) {
                $image = "https://" . $image;
            }
            $image = "https://mm-media-res.cloudinary.com/image/fetch/h_" . $height . ",w_" . $width . ",c_limit/" . $image . "";
        }
    } else {

        $image = 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_' . $height . ',w_' . $width . ',q_auto' . $image;
    }

    return $image;
}

//Single gallery image of product

function single_gallery_image_product($post_id, $height, $width)
{


    if (get_field('gallery_room_images', $post_id)) {

        // loop through the rows of data

        $gallery_image = get_field('gallery_room_images', $post_id);

        $gallery_i = explode("|", $gallery_image);


        foreach ($gallery_i as  $key => $value) {


            $room_image =  get_image_url($value, '1000', '1600');

            break;
        }
    }

    return $room_image;
}

function thumb_gallery_images_in_plp_loop($value)
{

    return  get_plp_image_url($value, '222', '222');
}

function get_plp_image_url($image, $height, $width)
{

    if (get_option('cloudinary', true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false) {


        if (strpos($image, 's7.shawimg.com') !== false) {
            if (strpos($image, 'http') === false) {
                $image = "http://" . $image;
            }
        } else {
            if (strpos($image, 'http') === false) {
                $image = "https://" . $image;
            }
            $image = "https://mm-media-res.cloudinary.com/image/fetch/h_" . $height . ",w_" . $width . ",c_fill/" . $image . "";
        }
    } else {

        $image = 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_' . $height . ',w_' . $width . ',q_auto' . $image;
    }

    return $image;
}


//Cloudinary Swatch image high

function swatch_image_product($post_id, $height, $width)
{

    $image = get_post_meta($post_id, 'swatch_image_link', true) ? get_post_meta($post_id, 'swatch_image_link', true) : "http://placehold.it/168x123?text=No+Image";
    return  get_image_url($image, $height, $width);
}

//Cloudinary Swatch image thumbnail 
function swatch_image_product_thumbnail($post_id, $height)
{


    $image = get_post_meta($post_id, 'swatch_image_link', true) ? get_post_meta($post_id, 'swatch_image_link', true) : "http://placehold.it/168x123?text=No+Image";

    return  get_image_url($image, $height, '222');
}

//New PLP DB Query Cloudinary Swatch image thumbnail 
function newplp_swatch_image_product_thumbnail($image, $height)
{


    //$image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 

    return  get_image_url($image, $height, '222');
}

//Cloudinary high resolution gallery image 
function high_gallery_images_in_loop($value)
{

    return  get_image_url($value, '1000', '1600');
}

//Cloudinary thumnail gallery image 
function thumb_gallery_images_in_loop($value)
{

    return  get_image_url($value, '222', '222');
}


///Seo optimization function according semrush audit report

/**
 * Dequeue the jQuery UI script.
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
function wpdocs_dequeue_script()
{
    wp_dequeue_script('bb-header-footer');
}
add_action('wp_print_scripts', 'wpdocs_dequeue_script', 100);

/**
 * Dequeue BB header foooter css and added into grandchild styles.css
 */

function remove_bbhf_css()
{
    wp_dequeue_style('bbhf-style-css');
}
add_action('wp_print_styles', 'remove_bbhf_css', 100);

/**
 * Removed emoji css and js
 */

function disable_emoji_feature()
{

    // Prevent Emoji from loading on the front-end
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    // Remove from admin area also
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');

    // Remove from RSS feeds also
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');

    // Remove from Embeds
    remove_filter('embed_head', 'print_emoji_detection_script');

    // Remove from emails
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');

    // Disable from TinyMCE editor. Currently disabled in block editor by default
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');

    /** Finally, prevent character conversion too
     ** without this, emojis still work 
     ** if it is available on the user's device
     */

    add_filter('option_use_smilies', '__return_false');
}

function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        $plugins = array_diff($plugins, array('wpemoji'));
    }
    return $plugins;
}

add_action('init', 'disable_emoji_feature');

/**
 * //Remove JQuery migrate
 */

function remove_jquery_migrate($scripts)
{
    if (! is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        if ($script->deps) {
            // Check whether the script has any dependencies

            $script->deps = array_diff($script->deps, array('jquery-migrate'));
        }
    }
}
add_action('wp_default_scripts', 'remove_jquery_migrate');

//Custom link shortcode for blog post
function custom_link_shortcode($arg)
{

    $page = url_to_postid($arg[0]);

    if ($page) {

        return $arg[0];
    } else {
        return '/';
    }
}
add_shortcode('Link', 'custom_link_shortcode');


if (!wp_next_scheduled('roomvo_csv_integration_cronjob')) {

    wp_schedule_event(time() +  20800, 'daily', 'roomvo_csv_integration_cronjob');
}
add_action('roomvo_csv_integration_cronjob', 'roomvo_csv_integration');


//Roomvo csv integration
function roomvo_csv_integration()
{

    $data = array();

    $brandmapping = array(

        "carpeting",
        "hardwood_catalog",
        "laminate_catalog",
        "luxury_vinyl_tile",
        "tile_catalog"

    );

    $website_json_data = json_decode(get_option('website_json'));

    foreach ($website_json_data->sites as $site_cloud) {

        if ($site_cloud->instance == 'prod') {

            if ($site_cloud->roomvo == 'true') {

                $isroomvo = 'true';
            }
        }
    }

    if ($isroomvo == 'true') {

        $protocols = array('https://', 'https://www.', 'www.', 'http://', 'http://www.');
        $domain = str_replace($protocols, '', home_url());

        $upload_dir = wp_get_upload_dir();
        $file = $upload_dir['basedir'] . '/sfn-data/product_file.csv';

        // write_log($file);

        $file = fopen($file, 'w');

        // save the column headers
        fputcsv($file, array('Manufacturer', 'Manufacturer SKU Number', 'product page URL', 'Dealer name', 'Dealer website domain', 'Surface Applicability'));

        foreach ($brandmapping as $product_post) {

            //  write_log('<----------'.$product_post.' Started--------->');
            sleep(1);

            // Sample data. This can be fetched from mysql too

            global $wpdb;
            $product_table = $wpdb->prefix . "posts";
            $products_data = $wpdb->get_results("SELECT ID FROM $product_table WHERE post_type = '$product_post' AND post_status = 'publish'");


            $i = 1;
            foreach ($products_data as $product) {

                $manufacturer = get_the_excerpt($product->ID);

                $sku = get_post_field('post_content', $product->ID);
                $product_url = get_permalink($product->ID);
                $dealer_name = $website_json_data->name;
                $domain_url = $domain;
                $manufacturer = str_replace(' ', '', strtolower($manufacturer));

                fputcsv($file, array($manufacturer, $sku, $product_url, $dealer_name, $domain_url, 'Floor'));

                if ($i % 5000 == 0) {
                    write_log($i . '----5000');
                    ob_flush();
                    sleep(5);
                }

                $i++;

                // }

            }

            wp_reset_query();

            //  write_log('<----------'.$product_post.' Ended--------->');

        }

        $file_size =  $upload_dir['basedir'] . '/sfn-data/product_file.csv';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.roomvo.com/pro/api/ext/update_dealer_mappings",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array(
                'requester' => 'mobilemarketing',
                'api_key' => 'cac7fb3c-7996-499d-8729-96a3547515b8',
                'dealer_name' => $website_json_data->name,
                'dealer_domain' => $domain_url,
                'email' => 'devteam.agency@gmail.com',
                'product_file' => new CURLFILE($file_size)
            ),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //  write_log('<----------response--------->');
        // write_log( $response);
        //  write_log('<----------response--------->');

    } else {

        //  write_log('<----------NO ROOMVO CDE SWITCH TRUE--------->');
    }
}

add_filter('the_excerpt', 'do_shortcode');
add_filter('get_the_excerpt', 'shortcode_unautop');
add_filter('get_the_excerpt', 'do_shortcode');


/*Thank you page image*/
function thankyou_responseimage()
{

    $promos = json_decode(get_option('promos_json'));

    foreach ($promos as $promo) {

        foreach ($promo->widgets as $widget) {

            if ($widget->type == 'thanks') {

                $realArray = (array)$widget;
                $realArray1 = (array)$realArray['images'][0];
                $realArray2 = (array)$realArray['images'][01];

                // $thanks =  array($promo->promoCode => $realArray1['url']);
                $thanks =  array($realArray1['url'], $realArray2['url']);
            }
        }
    }

    if ($_GET['promocode'] != '') {

        $content = '<img class="fl-photo-img thankuimage-desktop" src="https://' . $thanks[0] . '" alt="thank-you-image"  title="thank_you_image">
        <img class="fl-photo-img thankuimage-mobile" src="https://' . $thanks[1] . '" alt="thank-you-image"  title="thank_you_image">';

        return $content;
    }
}
//add_shortcode( 'thankyou-responseimage', 'thankyou_responseimage' );


function add_facebook_verify_meta_tags()
{

    $social_report = json_decode(get_option('social_report'));

    foreach ($social_report as $report) {

        if ($report->type == 'fbVerify') {

            echo '<meta name="' . $report->username . '" content="' . $report->accountId . '" />' . "\n";
        }

        //chat code
        if ($report->type == 'chat' && $report->active == '1') {

            if ($report->platform == 'tidio') {

                echo "<script src=" . $report->url . " async></script>" . "\n";
            } else if ($report->platform == 'podium' && $report->script != '') {

                echo $report->script;
            } else if ($report->platform == 'zendesk' && $report->key != '') {

                echo '<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=' . $report->key . '"> </script>' . "\n";
            }
        }
    }
}
add_action('wp_head', 'add_facebook_verify_meta_tags', 2);


//Custom Banner home page banner hook
function bbtheme_custom_alert_banner()
{
    $website_json =  json_decode(get_option('website_json'));
    $content = '';

    foreach ($website_json->sites as $site) {

        if ($site->instance == 'prod') {

            if ($site->bannerStartDate <= date("Y-m-d") &&  $site->bannerEndDate > date("Y-m-d")) {

                if ($site->bannerOverrideText != '' &&  is_front_page()) {

                    $content = '<div class="custombanneralert bannerOverrideText" style="background-color:#000">' . $site->bannerOverrideText . '</div>';
                }
            } elseif ($site->bannerEndDate < date("Y-m-d") && $site->bannerStartDate < date("Y-m-d")) {

                if ($site->bannerOverrideText != '' &&  is_front_page() && $site->banner != '') {

                    $content = '<div class="custombanneralert" style="background-color:#000">' . $site->banner . '</div>';
                } elseif ($site->bannerOverrideText == '' &&  is_front_page() && $site->banner != '') {

                    $content = '<div class="custombanneralert" style="background-color:#000">' . $site->banner . '</div>';
                }
            }
        }
    }
    echo $content;
}
add_action('fl_content_open', 'bbtheme_custom_alert_banner');


//Accessibility employee shortcode
function accessibility_shortcodes($arg)
{

    $website_json =  json_decode(get_option('website_json'));

    $content = '';

    foreach ($website_json->contacts as $contact) {

        if ($contact->accessibility == '1') {

            if (in_array("phone", $arg)) {

                $content = '<a href="tel:' . $contact->phone . '">' . $contact->phone . '</a>';
            }
            if (in_array("email", $arg)) {

                $content = '<a href="mailto:' . $contact->email . '">' . $contact->email . '</a>';
            }
        }
    }

    return  $content;
}
add_shortcode('accessibility', 'accessibility_shortcodes');


//Accessibility employee shortcode
function accessibility_contact_shortcodes()
{

    $website_json =  json_decode(get_option('website_json'), true);

    $filter = 1;

    $new_array = array_filter($website_json['contacts'], function ($var) use ($filter) {
        return ($var['accessibility'] == $filter);
    });

    $new_array = array_values($new_array);

    $content = '';

    if (!empty($new_array)) {

        if ($new_array[0]['phone'] != '' && $new_array[0]['email'] != '') {

            $content = 'at <a href="tel:' . $new_array[0]['phone'] . '">' . $new_array[0]['phone'] . '</a> or <a href="mailto:' . $new_array[0]['email'] . '">' . $new_array[0]['email'] . '</a>';
        } else {

            $content = '<a href="/contact-us/">here</a>';
        }
    } else {

        $content = '<a href="/contact-us/">here</a>';
    }



    return  $content;
}
add_shortcode('accessibility_contact', 'accessibility_contact_shortcodes');

// Swell chat widget code
function swell_chat_function()
{

    $social_report = json_decode(get_option('social_report'));

    foreach ($social_report as $report) {

        if ($report->type == 'chat' && $report->active == '1' && $report->platform == 'swell' && $report->script != '') {

            $script = str_replace('\"', '', $report->script);

            echo $report->script;
        }
    }
}

add_action('wp_footer', 'swell_chat_function');


// Swell review widget code 

function swell_review_function()
{

    $social_report = json_decode(get_option('social_report'));

    foreach ($social_report as $report) {

        if ($report->type == 'review' && $report->active == '1' && $report->platform == 'swell' && $report->script != '') {

            return $report->script;
        }
    }
}

add_shortcode('swell_review', 'swell_review_function');


//Custom Banner home page banner hook
function astra_custom_alert_banner_grand()
{

    $website_json =  json_decode(get_option('website_json'));
    $content = '';

    foreach ($website_json->sites as $site) {

        if ($site->instance == 'prod') {

            if ($site->bannerStartDate <= date("Y-m-d") &&  $site->bannerEndDate > date("Y-m-d")) {

                if ($site->bannerOverrideText != '' &&  is_front_page()) {

                    $content = '<div class="custombanneralert" style="background-color:#000">' . $site->bannerOverrideText . '</div>';
                }
            } elseif ($site->bannerEndDate < date("Y-m-d") && $site->bannerStartDate < date("Y-m-d")) {

                if ($site->bannerOverrideText != '' &&  is_front_page() && $site->banner != '') {

                    $content = '<div class="custombanneralert" style="background-color:#000">' . $site->banner . '</div>';
                } elseif ($site->bannerOverrideText == '' &&  is_front_page() && $site->banner != '') {

                    $content = '<div class="custombanneralert" style="background-color:#000">' . $site->banner . '</div>';
                }
            }
        }
    }
    echo $content;
}
add_action('astra_header_after', 'astra_custom_alert_banner_grand');

add_action('template_redirect', 'sku_redirect');
function sku_redirect()
{

    global $wpdb;
    $wp_posts = $wpdb->prefix . "posts";

    if (isset($_GET['sku']) && !$_GET['roomvoStartVisualizer']) {

        $sku  = $_GET['sku'];

        $sql = "SELECT guid FROM $wp_posts WHERE post_content='" . $sku . "' AND post_status = 'publish'";

        $results = $wpdb->get_results($sql);

        //    $postfound_id =  $results[0]->post_id;             

        //    $dest_url = get_permalink($postfound_id);
        $dest_url = $results[0]->guid;
        wp_redirect($dest_url, 301);
    }
}


// Birdeye review integration

//Birdeye rating count and average
add_shortcode('birdeye_review_ratecount', 'birdeye_review_rate_count');

function birdeye_review_rate_count()
{

    $social_report = json_decode(get_option('social_report'));

    foreach ($social_report as $report) {

        if ($report->type == 'review' && $report->active == '1' && $report->platform == 'Birdeye' && $report->accountId != '') {

            $accountId = $report->accountId;
            $apikey = $report->key;
            $write_url =  $report->url;
        }
    }

    $curlr = curl_init();

    curl_setopt_array($curlr, array(
        CURLOPT_URL => 'https://api.birdeye.com/resources/v1/review/businessId/159076090341369/?api_key=8R0eyLARag6Ieo6ZSceaOaM2eYrwNmvg',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
    "subBusinessIds": [' . $accountId . '],
    "sources": ["google"],
    "statuses": ["published"]
}
',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Accept: application/json'
        ),
    ));

    $response_list = curl_exec($curlr);

    curl_close($curlr);

    $response_list = json_decode($response_list);



    $arr = json_decode(json_encode($response_list), true);
    $counts = array_count_values(array_column($arr, 'rating'));
    //write_log($arr);
    write_log($counts);
    // var_dump($response_list);
    $reviewCount = 0;
    foreach ($counts as $key => $value) {
        $reviewCount += $value;
    }

    write_log($reviewCount);

    $levels = array('0', '1', '2', '3', '4', '5');
    $rating_response = [];
    foreach ($levels as $level) {
        $rating_response[$level] = [];

        $rating_response[$level]['rating'] = $level;
        if (array_key_exists($level, $counts)) {
            $rating_response[$level]['count'] = $counts[$level];
        } else {
            $rating_response[$level]['count'] = 0;
        }
        // $rating_response[$level]['count'] = $count_r;

    }

    write_log($rating_response);

    $content = '<div class="BirdEye">
<div class="BirdEye-bars">
    <div class="container">
        <div class="birdEye-row">
            <div class="birdEye-col">
                <div class="birdEye-bars-wrap">';

    $rating_array = array();

    $rating_response = array_reverse($rating_response);


    foreach ($rating_response as $rate) {

        if ($rate['rating'] != 0) {

            $rating_array[] = $rate['count'];

            $percentage = round(($rate['count'] / $reviewCount) * 100);

            $content .= '<div class="birdEye-bar-wrap">
                            <label for="star' . $rate['rating'] . '" data-value="' . $rate['count'] . '">' . $rate['rating'] . ' STAR REVIEWS</label>
                            <progress id="star' . $rate['rating'] . '" value="' . $percentage . '" max="100"> ' . $rate['count'] . ' </progress>
                        </div>';
        }
    }

    $AR = (1 * $rating_array[4]) + (2 * $rating_array[3]) + (3 * $rating_array[2]) + (4 * $rating_array[1]) + (5 * $rating_array[0]);
    $daverage = $AR / $reviewCount;
    //   var_dump($daverage);
    $average =  number_format($daverage, 1);

    //  var_dump($average);

    $content .= '</div>
            </div>
            <div class="birdEye-col total-reviews-wrap">
                    <div class="birdEye-total-wrap">
                            <span class="birdEye-total-outoff">' . $average . '</span>
                            
                            <div class="birdEye-starWrap-total" data-rating="' . $average . '">';



    $content .= ' <i class="fa fa-star no-fill" aria-hidden="true"></i>
                                <i class="fa fa-star no-fill" aria-hidden="true"></i>
                                <i class="fa fa-star no-fill" aria-hidden="true"></i>
                                <i class="fa fa-star no-fill" aria-hidden="true"></i>
                                <i class="fa fa-star no-fill" aria-hidden="true"></i>';




    $content .= '</div>
                            <span class="total-reviews">' . $reviewCount . ' REVIEWS</span>

                    </div>
                    <div class="reviewPara">
                        <p>Our customers love the quality flooring and service they receive. Learn more about the fantastic experiences they have had.</p>
                        <a href="' . $write_url . '" target="_blank" class="birdEye-writeReview fl-button" role="button" aria-label="WRITE A REVIEW">
                            WRITE A REVIEW
                        </a>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>';

    return $content;
}

// Birdeye Review List
add_shortcode('birdeye_review_list', 'birdeye_review_listing');

function birdeye_review_listing()
{

    $social_report = json_decode(get_option('social_report'));

    foreach ($social_report as $report) {

        if ($report->type == 'review' && $report->active == '1' && $report->platform == 'Birdeye' && $report->accountId != '') {

            $accountId = $report->accountId;
            $apikey = $report->key;
            $write_url =  $report->url;
        }
    }

    $curlr = curl_init();

    curl_setopt_array($curlr, array(
        CURLOPT_URL => 'https://api.birdeye.com/resources/v1/review/businessId/159076090341369/?api_key=8R0eyLARag6Ieo6ZSceaOaM2eYrwNmvg',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
    "subBusinessIds": [' . $accountId . '],
    "sources": ["google"],
    "statuses": ["published"]
}
',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Accept: application/json'
        ),
    ));

    $response_list = curl_exec($curlr);

    curl_close($curlr);

    $response_list = json_decode($response_list);


    $content = '<div class="BirdEye-review-exp">
                 <div class="birdEye-review-wrap container">';

    foreach ($response_list as $listitem) {
        $date = strtotime($listitem->reviewDate);
        $reviewdate = date('Y-m-d H:i:s', $date);
        $agotime = timeAgo($reviewdate);
        $content .= '<div class="birdEye-review">
                        <h4 class="birdEye-person">' . $listitem->reviewer->nickName . '</h4>
                        <span class="birdEye-review-time">' . $agotime . '</span>';


        $content .= '<div class="birdEye-starWrap" data-rating="' . $listitem->rating . '">';



        $content .= '<i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>';



        $content .= '</div><p>' . $listitem->comments . '</p>';

        if ($listitem->response != '') {

            $content .= '<div class="review_response"><span>Response from business</span><div class="resp_text">' . $listitem->response . '</div></div>';
        }
        $content .= '</div>';
    }

    $content .= '</div></div>';

    return $content;
}


////Time ago function

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed;
    $minutes    = round($time_elapsed / 60);
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400);
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640);
    $years      = round($time_elapsed / 31207680);
    // Seconds
    if ($seconds <= 60) {
        return "just now";
    }
    //Minutes
    else if ($minutes <= 60) {
        if ($minutes == 1) {
            return "one minute ago";
        } else {
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if ($hours <= 24) {
        if ($hours == 1) {
            return "an hour ago";
        } else {
            return "$hours hrs ago";
        }
    }
    //Days
    else if ($days <= 7) {
        if ($days == 1) {
            return "yesterday";
        } else {
            return "$days days ago";
        }
    }
    //Weeks
    else if ($weeks <= 4.3) {
        if ($weeks == 1) {
            return "a week ago";
        } else {
            return "$weeks weeks ago";
        }
    }
    //Months
    else if ($months <= 12) {
        if ($months == 1) {
            return "a month ago";
        } else {
            return "$months months ago";
        }
    }
    //Years
    else {
        if ($years == 1) {
            return "one year ago";
        } else {
            return "$years years ago";
        }
    }
}

////assent color integration
function my_custom_css()
{
    $accent = get_theme_mod('fl-accent');

    ?>
    <style>
        :root {
            --birdEye-primary-color: <?php echo $accent; ?>
        }
    </style>
<?php
}

add_action('wp_head', 'my_custom_css');

//audio eye integration
add_action('fl_body_close', 'accessibility_audioeye_footer_js');
function accessibility_audioeye_footer_js()
{

    $social_report = json_decode(get_option('social_report'));

    foreach ($social_report as $report) {

        if ($report->type == 'accessibility' && $report->active == '1' && $report->platform == 'audioeye' && $report->key != '') {

            echo $report->key;
        }
    }
}

//Refer url hook

add_filter('gform_field_value_refurl', 'populate_referral_url');

function populate_referral_url($form)
{
    // Grab URL from HTTP Server Var and put it into a variable
    $refurl = @$_SERVER['HTTP_REFERER'];
    GFCommon::log_debug(__METHOD__ . "(): HTTP_REFERER value returned by the server: {$refurl}");

    // Return that value to the form
    return esc_url_raw($refurl);
}

//Roomvo script
add_action('fl_body_close', 'roomvo_custom_footer_js');
function roomvo_custom_footer_js()
{

    $website_json_data = json_decode(get_option('website_json'));

    foreach ($website_json_data->sites as $site_cloud) {

        if ($site_cloud->instance == 'prod') {

            if ($site_cloud->roomvo == 'true') {

                echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";
            }
        }
    }
}

// store hours new shortcode
function mm_storelocation_short_hours($arg)
{
    
    $website = json_decode(get_option('website_json'));
   
    for ($i=0;$i < count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name) ? $website->locations[$i]->name:"";

        $location_ids = isset($website->locations[$i]->id) ? $website->locations[$i]->id:"";
       
        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){

            
            if (in_array(trim($location_name), $arg) || in_array(trim($location_ids), $arg)){

                if($website->locations[$i]->name == 'Main' || $website->locations[$i]->name=="MAIN"){

                    $location_name = get_bloginfo( 'name' );
                    
                }else{
                  
                    $location_name =  isset($website->locations[$i]->name) ? $website->locations[$i]->name." ":"";

                }
                $locations = "<ul class='storename'><li>";
                $locations .= '<div class="store-opening-hrs-container">';
                $locations .= '<ul class="store-opening-hrs">';

                $grouped_hours = [];
                $days_of_week = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

                foreach ($website->locations[$i]->hours as $day_hours) {
                    if (isset($day_hours->hours)) {
 
                        $found = false;
                        foreach ($grouped_hours as $index => $group) {
                            if ($group['hours'] === $day_hours->hours) {
                                $grouped_hours[$index]['days'][] = $day_hours->day;
                                $found = true;
                                break;
                            }
                        }

                        if (!$found) {
                            $grouped_hours[] = [
                                'hours' => $day_hours->hours,
                                'days' => [$day_hours->day],
                            ];
                        }
                    }
                }


                foreach ($grouped_hours as $group) {
                  
                    if (count($group['days']) > 1) {
                        $first_day = $group['days'][0]; 
                        $last_day = end($group['days']);
                        
                        $locations .= '<li>' . $first_day . ' - ' . $last_day . ': <span>' . $group['hours'] . '</span></li>';
                    } else {
                     
                        $locations .= '<li>' . $group['days'][0] . ': <span>' . $group['hours'] . '</span></li>';
                    }
                }   

                $locations .= '</ul>';
                $locations .= '</div>';    
                $locations .= '</li></ul>';
                
            } 

        }        
    }

    return $locations;
}
add_shortcode('mm_storelocation_short_hours', 'mm_storelocation_short_hours');

/* 
Hide Draft Pages from the menu
 */
function filter_draft_pages_from_menu_new($items, $args)
{
    foreach ($items as $ix => $obj) {
        if ('draft' == get_post_status($obj->object_id)) {
            unset($items[$ix]);
        }
    }
    return $items;
}
add_filter('wp_nav_menu_objects', 'filter_draft_pages_from_menu_new', 10, 2);


//current promoition shortcode
function current_promos_function($arg)
{
    ob_start();
    $seleinformation = json_decode(get_option('salesliderinformation'));

    if (isset($seleinformation)) {

        usort($seleinformation, 'compare_cde_some_objects');
    }

    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";

    $loc_country = $website_json->locations[0]->country;
    if ($loc_country == 'US') {
        $en = 'en_us';
    } elseif ($loc_country == 'CA') {
        $en = 'en_ca';
    }
?>

    <ul class="current_promos">
        <?php
        if (isset($seleinformation)) {
            foreach ($seleinformation as $slide) {

                if ($slide->isRugShop == 'true') {
                    $target = "_blank";
                    if ($rugAffiliateCode != '') {
                        if ($slide->slide_link != '') {
                            $slide->slide_link = 'https://rugs.shop/' . $en . '/' . trim($slide->slide_link, '/') . '/?store=' . $rugAffiliateCode;
                        } else {

                            $slide->slide_link = 'https://rugs.shop/' . $en . '/?store=' . $rugAffiliateCode;
                        }
                    } else {
                        $slide->slide_link = 'https://rugs.shop/';
                    }
                } else {
                    $target = "_self";
                    $slide->slide_link = $slide->slide_link;
                }

                $slide_coupon_img = "https://mm-media-res.cloudinary.com/image/fetch/h_440,w_440,c_limit/" . $slide->slider->slider_coupan_img;

        ?>
                <li class="current_promo_item">
                    <a href="<?php echo $slide->slide_link; ?>" target="<?php echo $target; ?>">
                        <img class="current_promo_image" src="<?php echo $slide_coupon_img; ?>" alt="<?php echo $slide->name; ?>">
                    </a>
                </li>
            <?php
            }
            ?>

        <?php
        } ?>

    </ul>

<?php return ob_get_clean();
}

add_shortcode('CURRENT_PROMOS', 'current_promos_function');


add_shortcode('blog-url', 'get_blog_fn');

function get_blog_fn($atts)
{

    $cat_name = $atts;
    $rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));
    $rewrite_slug = array_column($rewrite_rule, 'rewrite_slug');
    // print_r($rewrite_slug);

    foreach ($rewrite_slug as $slug) {
        if ($cat_name[0] == 'carpet' && strpos($slug, "carpet")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'vinyl' && strpos($slug, "vinyl") && strpos($slug, "sheet") == false) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'waterproof' && strpos($slug, "waterproof")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'hardwood' && strpos($slug, "hardwood")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'laminate' && strpos($slug, "laminate")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'tile' && strpos($slug, "tile")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'arearugs' && strpos($slug, "rugs")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'sheet' && strpos($slug, "sheet") && strpos($slug, "vinyl")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'paint' && strpos($slug, "paint")) {
            $landing_url = str_replace("/products", "/", $slug);
        } else if ($cat_name[0] == 'stone' && strpos($slug, "stone")) {
            $landing_url = str_replace("/products", "/", $slug);
        }
    }
    return $landing_url;
}


function shopofarearug($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";
    $data = array(
        array(
            'url' => 'https://rugs.shop/area-rugs/rug-colors/?store=' . $rugAffiliateCode,
            'image_url' => plugins_url('../img/Color.png', __FILE__),
            'title' => "Shop by Color"
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/rug-styles/?store=' . $rugAffiliateCode,
            'image_url' => plugins_url('../img/style.png', __FILE__),
            'title' => "Shop by Style"
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/rug-sizes/?store=' . $rugAffiliateCode,
            'image_url' => plugins_url('../img/Size.png', __FILE__),
            'title' => "Shop by Size"
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/rug-patterns/?store=' . $rugAffiliateCode,
            'image_url' => plugins_url('../img/patterns.png', __FILE__),
            'title' => "Shop by Patterns"
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/rug-brands/?store=' . $rugAffiliateCode,
            'image_url' => plugins_url('../img/brands.png', __FILE__),
            'title' => "Shop by Brands"
        ),
        array(
            'url' => 'https://rugs.shop/on-sale/?store=' . $rugAffiliateCode,
            'image_url' => plugins_url('../img/sale.png', __FILE__),
            'title' => "Area Rug Sale"
        ),

    );
    $result = "";
    $result .= '<div class="products-list column-3"><ul class="product-list">';
    for ($i = 0; $i < count($data); $i++) {
        $result .= '<li class="product-li-three-row">
                    <div class="product-inner">
                        <div class="product-img-holder">
                            <a href="' . $data[$i]['url'] . '" target="_blank" rel="noopener" itemprop="url" class="">
                                <img src="' . $data[$i]['image_url'] . '" alt="' . $data[$i]['title'] . '" />
                            </a>
                        </div>
                        <h3 class="fl-callout-title"><a href="' . $data[$i]['url'] . '" rel="noopener" target="_blank">' . $data[$i]['title'] . '</a></h3>
                    </div>
                </li>';
    }
    $result .= '</ul></div>';
    return $result;
}

add_shortcode('shoparearug', 'shopofarearug');

//Tranding Product ShortCode

function arearugProductList($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode) ? $website_json->rugAffiliateCode : "";

    $data = array(
        array(
            'url' => 'https://rugs.shop/area-rugs/?store=' . $rugAffiliateCode,
            'image_url' => 'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price' => '99.00',
            'title' => "PETRA MULTI",
            'collection' => 'Spice Market',
            'brand' => 'Karastan'
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/?store=' . $rugAffiliateCode,
            'image_url' => 'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price' => '499.00',
            'title' => "Esperance Seaglass",
            'collection' => 'Titanium Collection',
            'brand' => 'Karastan'
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/?store=' . $rugAffiliateCode,
            'image_url' => 'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price' => '156.00',
            'title' => "Nirvana Indigo",
            'collection' => 'Cosmopolitan',
            'brand' => 'Karastan'
        ),
        array(
            'url' => 'https://rugs.shop/area-rugs/?store=' . $rugAffiliateCode,
            'image_url' => 'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price' => '62.40',
            'title' => "7139a",
            'collection' => 'Andora',
            'brand' => 'Oriental Weavers'
        )
    );
    $result = "";
    $result .= '<div class="products-list"><ul class="product-list">';
    for ($i = 0; $i < count($data); $i++) {
        $result .= '<li class="product-li">
            <div class="product-inner">
                <div class="product-img-holder">
                    <a href="' . $data[$i]['url'] . '" target="_blank" rel="noopener"><img src="' . $data[$i]['image_url'] . '" alt="' . $data[$i]['title'] . '" /></a>
                </div>
                <div class="product-info">
                    <h6>' . $data[$i]['collection'] . '</h6>
                    <h4><a href="' . $data[$i]['url'] . '" target="_blank" rel="noopener">' . $data[$i]['title'] . '</a></h4>

                    <div class="price-section">
                        <span>Starting at</span>
                        <span class="price"> $' . $data[$i]['price'] . '</span>
                        <span class="sale">On Sale</span>
                    </div>

                    <div class="button-section">
                        <a href="' . $data[$i]['url'] . '" class="button" target="_blank" rel="noopener">Buy Now</a>
                        <div class="brand-wrap">By ' . $data[$i]['brand'] . '</div>
                    </div>
                </div>
            </div>
        </li>';
    }
    $result .= '</ul></div>';
    return $result;
}

add_shortcode('area_rug_trading_products', 'arearugProductList');

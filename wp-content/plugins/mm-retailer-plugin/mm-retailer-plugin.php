<?php
/*
Plugin Name: MM Retailer Plugin
Plugin URI: https://mobile-marketing.agency
Description: Retailer Setting Plugin
Author: MM
Version: 1.30
Author URI: https://mobile-marketing.agency
*/


include(dirname(__FILE__) . '/include/constant.php');
include(dirname(__FILE__) . '/include/apicaller.php');
include(dirname(__FILE__) . '/include/helper.php');
include(dirname(__FILE__) . '/include/track-leads.php');

/** GTM */
include(dirname(__FILE__) . '/include/gtm-manager.php');

include(dirname(__FILE__) . '/velocity-sales.php');

require_once(ABSPATH . "wp-includes/pluggable.php");
require_once(ABSPATH . "/wp-load.php");


require_once plugin_dir_path(__FILE__) . 'example-plugin.php';
new Example_Background_Processing();

global $wpdb;

$module_update = 1;
$product_check_table = $wpdb->prefix . "product_check";
$product_sync_table = $wpdb->prefix . "sfn_sync";
$posts_table = $wpdb->prefix . "posts";
$postmeta_table = $wpdb->prefix . "postmeta";

//Redis 
function reset_product_data($category)
{

    $redis = new Redis();
    $redis->connect('127.0.0.1', 6379);

    $redis->set("all", json_encode($products)); // use this always
    $products = array();
    // below is category specific: 
    if ($category == "all" || $category == "carpet") {
        $redis->set("carpet", json_encode($products));
    } elseif ($category == "all" || $category == "hardwood") {
        $redis->set("hardwood", json_encode($products));
    } elseif ($category == "all" || $category == "laminate") {
        $redis->set("laminate", json_encode($products));
    } elseif ($category == "all" || $category == "lvt") {
        $redis->set("lvt", json_encode($products));
    } elseif ($category == "all" || $category == "tile") {
        $redis->set("tile", json_encode($products));
    } elseif ($category == "all" || $category == "area_rugs") {
        $redis->set("area_rugs", json_encode($products));
    } elseif ($category == "all" || $category == "paint") {
        $redis->set("paint", json_encode($products));
    } elseif ($category == "all" || $category == "sheet") {
        $redis->set("sheet", json_encode($products));
    }
}


/** Theme global js **/
function wpc_theme_global()
{

    define('postpercol', '3');
    include(dirname(__FILE__) . '/styles.php');
    // define( 'productdetail_layout', 'box' );    
    define('productdetail_layout', 'box');
    wp_enqueue_style('lightbox-style', plugins_url('css/lightgallery.min.css', __FILE__));
    wp_enqueue_script('lightbox-js', plugins_url('js/lightgallery-all.min.js', __FILE__));
    wp_enqueue_script('script-js', plugins_url('js/script.min.js', __FILE__));
    //gtm_head_script();
}
add_action('wp_head', 'wpc_theme_global');

function admin_style()
{
    wp_enqueue_style('admin-styles', plugins_url('css/admin.min.css', __FILE__));
    wp_enqueue_script('script', plugins_url('/js/retailer_v1.min.js', __FILE__));
}
add_action('admin_enqueue_scripts', 'admin_style');
// This filter replaces a complete file from the parent theme or child theme with your file (in this case the archive page).
// Whenever the archive is requested, it will use YOUR archive.php instead of that of the parent or child theme.
//add_filter ('archive_template', create_function ('', 'return plugin_dir_path(__FILE__)."archive.php";'));

function wpc_theme_add_headers()
{
    wp_enqueue_style('font-awesome-styles', plugins_url('css/font-awesome.min.css', __FILE__));
    wp_enqueue_style('brands', plugins_url('css/brands.css', __FILE__));
    wp_enqueue_style('frontend-styles', plugins_url('css/styles.min.css', __FILE__));
}
// These two lines ensure that your CSS is loaded alongside the parent or child theme's CSS
add_action('init', 'wpc_theme_add_headers');

// In the rest of your plugin, add your normal actions and filters, just as you would in functions.php in a child theme.

function get_custom_post_type_template($single_template)
{
    global $post;

    if ($post->post_type != 'post') {
        $single_template = dirname(__FILE__) . '/product-listing-templates/single-' . $post->post_type . '.php';
    }
    return $single_template;
}

if (get_option('is_own_templates') != '1') {

    add_filter('single_template', 'get_custom_post_type_template');
}

//Code for adding Menu
//Nikhil Chinchane: 7 Jan 2019
//
function add_my_menu()
{
    add_menu_page(
        'Retailer Settings', // page title 
        'Retailer Settings', // menu title
        'manage_options', // capability
        'retailer-settings',  // menu-slug
        'my_menu_page',   // function that will render its output
        plugins_url('/img/theme-option-menu-icon.png', __FILE__)   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );

    add_submenu_page('retailer-settings', __('Retailer Product Data'), __('Retailer Product Data'), 'manage_options', 'retailer_product_data', 'retailer_product_data_html');
    add_submenu_page('retailer-settings', __('Retailer Promotion'), __('Retailer Promotion'), 'manage_options', 'promotion', 'promotion_func');
    add_submenu_page('retailer-settings', __('Documentation'), __('Documentation'), 'manage_options', 'documentation', 'documentation_func');
}
add_action('admin_menu', 'add_my_menu');

function changeTimeZone($dateString, $timeZoneSource = null, $timeZoneTarget = null)
{
    if (empty($timeZoneSource)) {
        $timeZoneSource = date_default_timezone_get();
    }
    if (empty($timeZoneTarget)) {
        $timeZoneTarget = date_default_timezone_get();
    }

    $dt = new DateTime($dateString, new DateTimeZone($timeZoneSource));
    $dt->setTimezone(new DateTimeZone($timeZoneTarget));

    return $dt->format("Y-m-d H:i:s a");
}

function overwritemodeules()
{
    global $module_update;
    // echo $wp_root_path = str_replace('/wp-content/themes', '', get_theme_root());echo "<br>";

    $theme_directory = get_template_directory() . "-child";
    echo "<br>"; ///var/www/html/wp-content/themes/astra-child
    $plugin_directory = plugin_dir_path(__FILE__);

    //check child directory is created for not

    if (is_dir($theme_directory)) {
        // Check fl-builder modules is exist for not        '

        if (is_dir($theme_directory . "/fl-builder")) {

            rename($theme_directory . "/fl-builder", $theme_directory . "/fl-builder" . time());
            var_dump(xcopy($plugin_directory . "fl-builder", $theme_directory . "/fl-builder"));
        } else {
            echo "NOT EXIST";
            var_dump(xcopy($plugin_directory . "fl-builder", $theme_directory . "/fl-builder"));
        }
    } else {

        if (is_dir(get_template_directory() . "/fl-builder")) {
            rename(get_template_directory() . "/fl-builder", get_template_directory() . "/fl-builder" . time());
            var_dump(xcopy($plugin_directory . "fl-builder", get_template_directory() . "/fl-builder"));
        } else {
            var_dump(xcopy($plugin_directory . "fl-builder", get_template_directory() . "/fl-builder"));
        }

        //mkdir($theme_directory);

    }
    if (get_site_option('overwrite_module') !== false) {
        update_option('overwrite_module', $module_update);
    } else {

        add_option('overwrite_module', $module_update);
    }
}

add_action('plugins_loaded', 'overwritemodulesintheme');
function overwritemodulesintheme()
{


    global $module_update;
    if (get_site_option('overwrite_module') !== false) {
        if (get_site_option('overwrite_module') != $module_update) {
            overwritemodeules();
        }
    } else {

        overwritemodeules();
    }
}




add_action('sale_hook_new', 'sale_api_call');
add_action('init', 'single_event');

function single_event()
{
    if (!wp_next_scheduled('sale_hook_new')) {
        // Schedule the event
        wp_schedule_single_event(time() + 600, "sale_hook_new", array("2"));
    }
}

function sale_api_call()
{
    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {
        callSaleAPI($apiObj, $result);
        sync_websiteinfo_cde($apiObj, $result);
    }
    //Change for daily one sync mail
    $time = date("H");
    if ($time >= 1 && $time < 2) {
        //wp_mail( 'ram@mobile-marketing.agency', 'SINGLE EVENT  Sale API Called for '. get_bloginfo(), 'SINGLE EVENT '.date("Y-m-d h:i:s",time()) );
    }
}


function getIntervalTime($day)
{

    date_default_timezone_set("Asia/Kolkata");
    $time = (int)time();
    $daytime = (int)strtotime('next ' . $day);
    return ($daytime + 34200) - $time;
}


/**  Cron job timing **/

add_filter('cron_schedules', 'example_add_cron_interval');

function example_add_cron_interval($schedules)
{

    $schedules['one_min'] = array(
        'interval' => 3600,
        'display' => esc_html__('Every one hour'),
    );
    $schedules['each_monday'] = array(
        'interval' => getIntervalTime('monday'),
        'display' => esc_html__('Each Monday'),
    );
    $schedules['each_tuesday'] = array(
        'interval' => getIntervalTime('tuesday'),
        'display' => esc_html__('Each Tuesday'),
    );
    $schedules['each_wednesday'] = array(
        'interval' => getIntervalTime('wednesday'),
        'display' => esc_html__('Each Wednesday'),
    );
    $schedules['each_thursday'] = array(
        'interval' => getIntervalTime('thursday'),
        'display' => esc_html__('Each Thursday'),
    );
    $schedules['each_friday'] = array(
        'interval' => getIntervalTime('friday'),
        'display' => esc_html__('Each Friday'),
    );
    $schedules['each_saturday'] = array(
        'interval' => getIntervalTime('saturday'),
        'display' => esc_html__('Each Saturday'),
    );
    $schedules['each_sunday'] = array(
        'interval' => getIntervalTime('sunday'),
        'display' => esc_html__('Each Sunday'),
    );
    $schedules['monthly'] = array(
        'interval' => MONTH_IN_SECONDS,
        'display'  => __('Once Monthly')
    );

    return $schedules;
}

/**  Blog sync Function **/

function blog_function()
{

    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {
        callBlogpostAPI($apiObj, $result);
    }
    //wp_mail( 'devteam.agency@gmail.com', 'Blog API Called for '. get_bloginfo(), 'WP cron run at '.date("Y-m-d h:i:s",time()) );
}


add_action('CDE_blog_sync', 'blog_function');

function CDE_sync_function()
{

    //updated fields for block country

    global $wpdb;
    $redirect_table = $wpdb->prefix . 'redirection_items';

    $data_serial = array('CA', 'US', 'UM');
    update_option('blockcountry_backendbanlist', $data_serial);
    update_option('blockcountry_banlist', $data_serial);
    update_option('blockcountry_backendbanlist_inverse', 'on');
    update_option('blockcountry_banlist_inverse', 'on');
    update_option('blockcountry_frontendwhitelist', '45.55.43.154;114.143.174.138;1.22.231.34/32;');
    update_option('blockcountry_backendwhitelist', '54.191.137.17;114.143.174.138;1.22.231.34/32;');

    $wpdb->query('delete from ' . $redirect_table . ' where id in (Select id from (SELECT action_data , CONCAT(match_url  ,"/") as url , id FROM ' . $redirect_table . '  as t) as p  where p.action_data= p.url )');
    $wpdb->query('delete from ' . $redirect_table . ' where id in (Select id from (SELECT action_data , CONCAT(match_url  ,"") as url , id FROM ' . $redirect_table . '  as t) as p  where p.action_data= p.url )');


    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {
        callSaleAPI($apiObj, $result);
        sync_websiteinfo_cde($apiObj, $result);
        get_option('CDE_LAST_SYNC_TIME') ? update_option('CDE_LAST_SYNC_TIME', time()) : add_option('CDE_LAST_SYNC_TIME', time());
    }
}
add_action('CDE_sync_cronjob', 'CDE_sync_function');
add_action('init', 'register_cron_delete_event');



// Function which will register the event
function register_cron_delete_event()
{

    // Make sure this event hasn't been scheduled
    if (!wp_next_scheduled('CDE_sync_cronjob')) {
        // Schedule the event

        wp_schedule_event(time(), 'one_min', 'CDE_sync_cronjob');
    }
    if (!wp_next_scheduled('CDE_blog_sync')) {
        // Schedule the event

        wp_schedule_event(time(), 'daily', 'CDE_blog_sync');
    }
    // Make sure this event hasn't been scheduled
    if (!wp_next_scheduled('sfn_bb_autoimport_hook')) {
        // Schedule the event

        wp_schedule_event(time(), 'one_min', 'sfn_bb_autoimport_hook');
    }



    $product_json =  json_decode(get_option('product_json'));
    $carpet_array = getArrayFiltered('productType', 'carpet', $product_json);
    $carpet_barray = json_decode(json_encode($carpet_array), true);

    if (is_array($carpet_array) && count($carpet_array) > 0) {


        if (array_search('Mohawk', array_column($carpet_array, 'manufacturer')) != '') {

            if (! wp_next_scheduled('sync_carpet_monday_event_mohawk')) {

                wp_schedule_event(strtotime('06:00:00'), 'daily', 'sync_carpet_monday_event_mohawk');
            }
        } else {

            wp_clear_scheduled_hook('sync_carpet_monday_event_mohawk');
        }

        if (array_search('Shaw', array_column($carpet_array, 'manufacturer')) != '') {


            if (! wp_next_scheduled('sync_carpet_tuesday_event_shaw')) {

                wp_schedule_event(strtotime('10:00:00'), 'daily', 'sync_carpet_tuesday_event_shaw');
            }
        } else {

            wp_clear_scheduled_hook('sync_carpet_tuesday_event_shaw');
        }
    } else {

        wp_clear_scheduled_hook('sync_carpet_monday_event_mohawk');
        wp_clear_scheduled_hook('sync_carpet_tuesday_event_shaw');
        wp_clear_scheduled_hook('sync_carpet_saturday_event_dreamweaver');
    }

    $sat_array = json_decode(json_encode($carpet_array), True);

    $satbrand_array = removeElementWithValue($sat_array, "manufacturer", 'Mohawk');
    $satbrand_array = removeElementWithValue($sat_array, "manufacturer", 'Shaw');

    if (!empty($satbrand_array)) {

        if (! wp_next_scheduled('sync_carpet_saturday_event_dreamweaver')) {

            wp_schedule_event(strtotime('14:00:00'), 'daily', 'sync_carpet_saturday_event_dreamweaver');
        }
    } else {

        wp_clear_scheduled_hook('sync_carpet_saturday_event_dreamweaver');
    }

    $paint_array = getArrayFiltered('productType', 'paint', $product_json);
    $paint_barray = json_decode(json_encode($paint_array), true);

    if (is_array($paint_barray) && count($paint_barray) > 0) {

        if (! wp_next_scheduled('sync_paint_friday_event') && get_option('showpaint') == '1') {

            wp_schedule_event(time() + 2700, 'daily', 'sync_paint_friday_event');
        }
    }

    $hard_array = getArrayFiltered('productType', 'hardwood', $product_json);
    $hard_barray = json_decode(json_encode($hard_array), true);

    if (is_array($hard_barray) && count($hard_barray) > 0) {

        if (! wp_next_scheduled('sync_hardwood_wednesday_event')) {

            wp_schedule_event(strtotime('09:00:00'), 'daily', 'sync_hardwood_wednesday_event');
        }
    }

    $lam_array = getArrayFiltered('productType', 'laminate', $product_json);
    $lam_barray = json_decode(json_encode($lam_array), true);

    if (is_array($lam_barray) && count($lam_barray) > 0) {

        if (! wp_next_scheduled('sync_laminate_thursday_event')) {

            wp_schedule_event(strtotime('18:00:00'), 'daily', 'sync_laminate_thursday_event');
        }
    }

    $lvt_array = getArrayFiltered('productType', 'lvt', $product_json);
    $lvt_barray = json_decode(json_encode($lvt_array), true);

    if (is_array($lvt_barray) && count($lvt_barray) > 0) {

        if (! wp_next_scheduled('sync_lvt_thursday_event')) {

            wp_schedule_event(strtotime('5:00:00'), 'daily', 'sync_lvt_thursday_event');
        }
    }

    $tile_array = getArrayFiltered('productType', 'tile', $product_json);
    $tile_barray = json_decode(json_encode($tile_array), true);

    if (is_array($tile_barray) && count($tile_barray) > 0) {

        if (! wp_next_scheduled('sync_tile_friday_event')) {

            wp_schedule_event(strtotime('04:00:00'), 'daily', 'sync_tile_friday_event');
        }
    }
}
register_activation_hook(__FILE__, 'my_activation');

/**  Function for activate cron job  **/
function my_activation()
{
    if (! wp_next_scheduled('CDE_sync_cronjob')) {
        wp_schedule_event(time(), 'one_min', 'CDE_sync_cronjob');
    }
    if (! wp_next_scheduled('CDE_blog_sync')) {
        wp_schedule_event(time(), 'daily', 'CDE_blog_sync');
    }
    if (! wp_next_scheduled('sfn_bb_autoimport_hook')) {
        wp_schedule_event(time(), 'one_min', 'sfn_bb_autoimport_hook');
    }
}


register_deactivation_hook(__FILE__, 'my_deactivation');

function my_deactivation()
{
    wp_clear_scheduled_hook('my_hourly_event');
}


if (isset($_POST['overright_module']) && $_POST['overright_module'] == 1) {
    // echo $wp_root_path = str_replace('/wp-content/themes', '', get_theme_root());echo "<br>";

    $theme_directory = get_template_directory() . "-child";
    echo "<br>";
    $plugin_directory = plugin_dir_path(__FILE__);

    //check child directory is created for not

    if (is_dir($theme_directory)) {
        // Check fl-builder modules is exist for not        '

        if (is_dir($theme_directory . "/fl-builder")) {

            rename($theme_directory . "/fl-builder", $theme_directory . "/fl-builder" . time());
            var_dump(xcopy($plugin_directory . "fl-builder", $theme_directory . "/fl-builder"));
        } else {
            echo "NOT EXIST";
            var_dump(xcopy($plugin_directory . "fl-builder", $theme_directory . "/fl-builder"));
        }
    } else {

        if (is_dir(get_template_directory() . "/fl-builder")) {
            rename(get_template_directory() . "/fl-builder", get_template_directory() . "/fl-builder" . time());
            var_dump(xcopy($plugin_directory . "fl-builder", get_template_directory() . "/fl-builder"));
        } else {
            var_dump(xcopy($plugin_directory . "fl-builder", get_template_directory() . "/fl-builder"));
        }

        //mkdir($theme_directory);

    }
}
//react plp  
if (isset($_POST['showpaint'])) {

    (get_option('showpaint') !== null) ? update_option('showpaint', $_POST['showpaint']) : add_option('showpaint', $_POST['showpaint']);
}

if (isset($_POST['rugpageurl'])) {

    (get_option('rugpageurl') !== null) ? update_option('rugpageurl', $_POST['rugpageurl']) : add_option('rugpageurl', $_POST['rugpageurl']);
}

if (isset($_POST['arearugbutton'])) {

    (get_option('arearugbutton') !== null) ? update_option('arearugbutton', $_POST['arearugbutton']) : add_option('arearugbutton', $_POST['arearugbutton']);
}

if (isset($_POST['instocksyncseprate'])) {

    (get_option('instocksyncseprate') !== null) ? update_option('instocksyncseprate', $_POST['instocksyncseprate']) : add_option('instocksyncseprate', $_POST['instocksyncseprate']);
}

if (isset($_POST['allinstock'])) {

    (get_option('allinstock') !== null) ? update_option('allinstock', $_POST['allinstock']) : add_option('allinstock', $_POST['allinstock']);
}
if (isset($_POST['arearugsync'])) {

    (get_option('arearugsync') !== null) ? update_option('arearugsync', $_POST['arearugsync']) : add_option('arearugsync', $_POST['arearugsync']);
}
if (isset($_POST['sheetsync'])) {

    (get_option('sheetsync') !== null) ? update_option('sheetsync', $_POST['sheetsync']) : add_option('sheetsync', $_POST['sheetsync']);
}

if (isset($_POST['arearugs_brand'])) {

    (get_option('arearugs_brand') !== null) ? update_option('arearugs_brand', $_POST['arearugs_brand']) : add_option('arearugs_brand', $_POST['arearugs_brand']);
}
if (isset($_POST['isworkbook'])) {

    (get_option('isworkbook') !== null) ? update_option('isworkbook', $_POST['isworkbook']) : add_option('isworkbook', $_POST['isworkbook']);
}
if (isset($_POST['showpdpsamplebtn'])) {

    (get_option('showpdpsamplebtn') !== null) ? update_option('showpdpsamplebtn', $_POST['showpdpsamplebtn']) : add_option('showpdpsamplebtn', $_POST['showpdpsamplebtn']);
}
if (isset($_POST['showsamplelabel'])) {

    (get_option('showsamplelabel') !== null) ? update_option('showsamplelabel', $_POST['showsamplelabel']) : add_option('showsamplelabel', $_POST['showsamplelabel']);
}

if (isset($_POST['nogroupbysize'])) {

    (get_option('nogroupbysize') !== null) ? update_option('nogroupbysize', $_POST['nogroupbysize']) : add_option('nogroupbysize', $_POST['nogroupbysize']);
}
if (isset($_POST['nogroupbysize_cat'])) {

    (get_option('nogroupbysize_cat') !== null) ? update_option('nogroupbysize_cat', $_POST['nogroupbysize_cat']) : add_option('nogroupbysize_cat', $_POST['nogroupbysize_cat']);
}


if (isset($_POST['groupbysize'])) {

    (get_option('groupbysize') !== null) ? update_option('groupbysize', $_POST['groupbysize']) : add_option('groupbysize', $_POST['groupbysize']);
}
if (isset($_POST['groupbysize_cat'])) {

    (get_option('groupbysize_cat') !== null) ? update_option('groupbysize_cat', $_POST['groupbysize_cat']) : add_option('groupbysize_cat', $_POST['groupbysize_cat']);
}
if (isset($_POST['groupbysizepdp'])) {

    (get_option('groupbysizepdp') !== null) ? update_option('groupbysizepdp', $_POST['groupbysizepdp']) : add_option('groupbysizepdp', $_POST['groupbysizepdp']);
}

if (isset($_POST['postsgloblesearch'])) {

    (get_option('postsgloblesearch') !== null) ? update_option('postsgloblesearch', $_POST['postsgloblesearch']) : add_option('postsgloblesearch', $_POST['postsgloblesearch']);
}

// if (isset($_POST['sampleavailable'])) {

//     (get_option('sampleavailable') !== null) ? update_option('sampleavailable', $_POST['sampleavailable']) : add_option('sampleavailable', $_POST['sampleavailable']);
// }
if (isset($_POST['hideinstockcoupon'])) {

    (get_option('hideinstockcoupon') !== null) ? update_option('hideinstockcoupon', $_POST['hideinstockcoupon']) : add_option('hideinstockcoupon', $_POST['hideinstockcoupon']);
}
if (isset($_POST['pdpshowform'])) {

    (get_option('pdpshowform') !== null) ? update_option('pdpshowform', $_POST['pdpshowform']) : add_option('pdpshowform', $_POST['pdpshowform']);
}
if (isset($_POST['softsurfacepdpshowforminstock'])) {

    (get_option('softsurfacepdpshowforminstock') !== null) ? update_option('softsurfacepdpshowforminstock', $_POST['softsurfacepdpshowforminstock']) : add_option('softsurfacepdpshowforminstock', $_POST['softsurfacepdpshowforminstock']);
}

if (isset($_POST['pdpshowforminstock'])) {

    (get_option('pdpshowforminstock') !== null) ? update_option('pdpshowforminstock', $_POST['pdpshowforminstock']) : add_option('pdpshowforminstock', $_POST['pdpshowforminstock']);
}

if (isset($_POST['plpshowprice'])) {

    (get_option('plpshowprice') !== null) ? update_option('plpshowprice', $_POST['plpshowprice']) : add_option('plpshowprice', $_POST['plpshowprice']);
}

if (isset($_POST['product_order_by_price'])) {

    (get_option('product_order_by_price') !== null) ? update_option('product_order_by_price', $_POST['product_order_by_price']) : add_option('product_order_by_price', $_POST['product_order_by_price']);
}
if (isset($_POST['plpshowretailandspecialprice'])) {

    (get_option('plpshowretailandspecialprice') !== null) ? update_option('plpshowretailandspecialprice', $_POST['plpshowretailandspecialprice']) : add_option('plpshowretailandspecialprice', $_POST['plpshowretailandspecialprice']);
}
if (isset($_POST['plpshowpricestrike'])) {

    (get_option('plpshowpricestrike') !== null) ? update_option('plpshowpricestrike', $_POST['plpshowpricestrike']) : add_option('plpshowpricestrike', $_POST['plpshowpricestrike']);
}
if (isset($_POST['showcallforprice'])) {

    (get_option('showcallforprice') !== null) ? update_option('showcallforprice', $_POST['showcallforprice']) : add_option('showcallforprice', $_POST['showcallforprice']);
}
if (isset($_POST['showsharebtn'])) {

    (get_option('showsharebtn') !== null) ? update_option('showsharebtn', $_POST['showsharebtn']) : add_option('showsharebtn', $_POST['showsharebtn']);
}

if (isset($_POST['arearugshowsku'])) {

    (get_option('arearugshowsku') !== null) ? update_option('arearugshowsku', $_POST['arearugshowsku']) : add_option('arearugshowsku', $_POST['arearugshowsku']);
}
if (isset($_POST['plpestimatebutton'])) {

    (get_option('plpestimatebutton') !== null) ? update_option('plpestimatebutton', $_POST['plpestimatebutton']) : add_option('plpestimatebutton', $_POST['plpestimatebutton']);
}

if (isset($_POST['plpestimatebuttonlink'])) {

    (get_option('plpestimatebuttonlink') !== null) ? update_option('plpestimatebuttonlink', $_POST['plpestimatebuttonlink']) : add_option('plpestimatebuttonlink', $_POST['plpestimatebuttonlink']);
}

if (isset($_POST['layoutopotion'])) {

    (get_option('layoutopotion') !== null) ? update_option('layoutopotion', $_POST['layoutopotion']) : add_option('layoutopotion', $_POST['layoutopotion']);
}

if (isset($_POST['is_own_templates'])) {

    (get_option('is_own_templates') !== null) ? update_option('is_own_templates', $_POST['is_own_templates']) : add_option('is_own_templates', $_POST['is_own_templates']);
}

if (isset($_POST['sh_get_finance'])) {

    (get_option('sh_get_finance') !== null) ? update_option('sh_get_finance', $_POST['sh_get_finance']) : add_option('sh_get_finance', $_POST['sh_get_finance']);
}

if (isset($_POST['pdp_get_finance'])) {

    (get_option('pdp_get_finance') !== null) ? update_option('pdp_get_finance', $_POST['pdp_get_finance']) : add_option('pdp_get_finance', $_POST['pdp_get_finance']);
}

if (isset($_POST['hide_getcoupon_button'])) {

    (get_option('hide_getcoupon_button') !== null) ? update_option('hide_getcoupon_button', $_POST['hide_getcoupon_button']) : add_option('hide_getcoupon_button', $_POST['hide_getcoupon_button']);
}

if (isset($_POST['getfinancereplace'])) {

    (get_option('getfinancereplace') !== null) ? update_option('getfinancereplace', $_POST['getfinancereplace']) : add_option('getfinancereplace', $_POST['getfinancereplace']);
}

if (isset($_POST['getfinancetext'])) {

    (get_option('getfinancetext') !== null) ? update_option('getfinancetext', $_POST['getfinancetext']) : add_option('getfinancetext', $_POST['getfinancetext']);
}

if (isset($_POST['getfinancereplaceurl'])) {

    (get_option('getfinancereplaceurl') !== null) ? update_option('getfinancereplaceurl', $_POST['getfinancereplaceurl']) : add_option('getfinancereplaceurl', $_POST['getfinancereplaceurl']);
}
if (isset($_POST['plplayout'])) {

    (get_option('plplayout') !== null) ? update_option('plplayout', $_POST['plplayout']) : add_option('plplayout', $_POST['plplayout']);
}
if (isset($_POST['plpproductimg'])) {

    (get_option('plpproductimg') !== null) ? update_option('plpproductimg', $_POST['plpproductimg']) : add_option('plpproductimg', $_POST['plpproductimg']);
}
if (isset($_POST['getcouponbtn'])) {

    (get_option('getcouponbtn') !== null) ? update_option('getcouponbtn', $_POST['getcouponbtn']) : add_option('getcouponbtn', $_POST['getcouponbtn']);
}
if (isset($_POST['getcouponreplace'])) {

    (get_option('getcouponreplace') !== null) ? update_option('getcouponreplace', $_POST['getcouponreplace']) : add_option('getcouponreplace', $_POST['getcouponreplace']);
}

if (isset($_POST['getcouponreplacetext'])) {

    (get_option('getcouponreplacetext') !== null) ? update_option('getcouponreplacetext', $_POST['getcouponreplacetext']) : add_option('getcouponreplacetext', $_POST['getcouponreplacetext']);
}
if (isset($_POST['getcouponreplaceurl'])) {

    (get_option('getcouponreplaceurl') !== null) ? update_option('getcouponreplaceurl', $_POST['getcouponreplaceurl']) : add_option('getcouponreplaceurl', $_POST['getcouponreplaceurl']);
}

if (isset($_POST['siteid']) && $_POST['siteid'] != "" && isset($_POST['clientcode']) && $_POST['clientcode'] != ""  && isset($_POST['clientsecret']) && $_POST['clientsecret'] != "") {
    $post_7 = get_post(445994);
    $actio = 'duplicate';

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => $_POST['clientcode'], 'client_secret' => $_POST['clientsecret']);
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {

        get_option('CLIENT_CODE') ? update_option('CLIENT_CODE', $_POST['clientcode']) : add_option('CLIENT_CODE', $_POST['clientcode']);
        get_option('SITE_CODE') ? update_option('SITE_CODE', $_POST['siteid']) : add_option('SITE_CODE', $_POST['siteid']);
        get_option('CLIENTSECRET') ? update_option('CLIENTSECRET', $_POST['clientsecret']) : add_option('CLIENTSECRET', $_POST['clientsecret']);
        get_option('ACCESS_TOKEN') ? update_option('ACCESS_TOKEN', $result['access_token']) : add_option('ACCESS_TOKEN', $result['access_token']);
        get_option('CDE_ENV') ? update_option('CDE_ENV', $_POST['instance-select']) : add_option('CDE_ENV', $_POST['instance-select']);
        get_option('CDE_LAST_SYNC_TIME') ? update_option('CDE_LAST_SYNC_TIME', time()) : add_option('CDE_LAST_SYNC_TIME', time());
        //API Call for Social Icon
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $sociallinks = $apiObj->call(BASEURL . get_option('SITE_CODE') . "/" . SOCIALURL, "GET", $inputs, $headers);
        $socialreport = $apiObj->call(BASEURL . get_option('SITE_CODE') . "/" . SOCIALURLREPORT, "GET", $inputs, $headers);

        //write_log(BASEURL.get_option('SITE_CODE')."/".SOCIALURLREPORT);
        // write_log($socialreport);
        if (isset($sociallinks['success']) && $sociallinks['success'] == 1) {
            $social_json =  json_encode($sociallinks['result']);
            get_option('social_links') || get_option('social_links') == "" ? update_option('social_links', $social_json) : add_option('social_links', $social_json);
        } else {
            $msg = $sociallinks['message'];
            $_SESSION['error'] = "Error SOCIAL LINKS";
            $_SESSION["error_desc"] = $msg;
            update_option('social_links', '');
            //echo $msg;

        }

        if (isset($socialreport['success']) && $socialreport['success'] == 1) {
            $socialreport_json =  json_encode($socialreport['result']);
            get_option('social_report') || get_option('social_report') == "" ? update_option('social_report', $socialreport_json) : add_option('social_report', $socialreport_json);
        }

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);


        if (isset($website['success']) && $website['success']) {

            $sfn_acc = $website['result']['sfn'];
            $coretec_color = $website['result']['options']['colorWall'];
            $covid = $website['result']['options']['covid'];


            $website_json = json_encode($website['result']);
            get_option('website_json') ? update_option('website_json', $website_json) : add_option('website_json', $website_json);
            update_option('sfn_account', $sfn_acc);
            update_option('covid', $covid);

            $website_json_data =  json_decode(get_option('website_json'));

            foreach ($website_json_data->sites as $site_cloud) {

                if ($site_cloud->instance == 'prod') {

                    if ($site_cloud->cloudinary == 'true') {

                        update_option('cloudinary', 'true');
                    } else {

                        update_option('cloudinary', 'false');
                    }
                }
            }

            if (! function_exists('post_exists')) {
                require_once(ABSPATH . 'wp-admin/includes/post.php');
            }


            for ($i = 0; $i < count($website['result']['locations']); $i++) {
                $location_name = isset($website['result']['locations'][$i]['name']) ? $website['result']['locations'][$i]['name'] : "";

                if (post_exists($location_name) == 0 && $location_name != "") {

                    $array = array(
                        'post_title' => $location_name,
                        'post_type' => 'store-locations',
                        'post_content'  => "LOCATIONS",
                        'post_status'   => 'publish',
                        'post_author'   => 0,
                    );
                    //  $post_id = wp_insert_post( $array );
                }
            }

            if (isset($website['result']['sites'])) {
                for ($k = 0; $k < count($website['result']['sites']); $k++) {
                    if ($website['result']['sites'][$k]['instance'] == ENV) {
                        update_option('blogname', $website['result']['sites'][$k]['name']);
                        $gtmid = $website['result']['sites'][$k]['gtmId'];
                        get_option('gtm_script_insert') ? update_option('gtm_script_insert', $gtmid) : update_option('gtm_script_insert', $gtmid);
                    }
                }
            }
            //get_option( 'gtm_script_insert', )

        } else {
            $msg = $website['message'];
            //echo $msg;
        }
        //API Call for getting Contact INFO
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $contacts = $apiObj->call(BASEURL . CONTACTURL . get_option('SITE_CODE'), "GET", $inputs, $headers);

        if (isset($contacts['success']) && $contacts['success']) {

            $contacts_json = json_encode($contacts['result']);
            get_option('contacts_json') ? update_option('contacts_json', $contacts_json) : add_option('contacts_json', $contacts_json);

            $phone = isset($contacts['result'][0]['phone']) ? $contacts['result'][0]['phone'] : '';

            get_option('api_contact_phone') ? update_option('api_contact_phone', $phone) : update_option('api_contact_phone', $phone);
        } else {
            $msg = $contacts['message'];
            $_SESSION['error'] = "Error Contact Info";
            $_SESSION["error_desc"] = $msg;
        }


        //API Call For getting Promos Information
        //PROMOSAPI
        callSaleAPI($apiObj, $result);

        //API Call for geting product brand details:

        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $products = $apiObj->call(SOURCEURL . get_option('SITE_CODE') . "/" . PRODUCTURL, "GET", $inputs, $headers);


        if (isset($products)) {

            $product_json = json_encode($products);
            update_option('product_json', $product_json);
        } else {
            $msg = $contacts['message'];
            $_SESSION['error'] = "Product Brand API";
            $_SESSION["error_desc"] = $msg;
        }

        //API Call for geting site blog post:

    }
} else {
    $msg = "Please fill all fields";
    //echo $msg;
}

if (isset($_POST['blogsync'])) {

    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    // echo AUTHURL;
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);
    // print_r($result);
    callBlogpostAPI($apiObj, $result);
}


function callBlogpostAPI($apiObj, $result)
{

    $inputs = array();
    $headers = array('authorization' => "bearer " . $result['access_token']);
    $blogposts = $apiObj->call(BLOGURL . get_option('SITE_CODE') . "/", "GET", $inputs, $headers);


    if (isset($blogposts['result'])) {

        foreach ($blogposts['result'] as $blog_post) {

            $date_stamp = strtotime($blog_post['startDate']);
            $enddate_stamp = strtotime($blog_post['endDate']);
            $current_date = date('Y-m-d');

            $postdate = date("Y-m-d H:i:s", $date_stamp);
            $post_sync_date = date("Y-m-d", $date_stamp);
            $post_end_date = date("Y-m-d", $enddate_stamp);

            //   write_log($blog_post['url'].'<------Current date<------>'.$current_date.'<----->Sync Date----->'.$blog_post['startDate'].'<-------End Date------>'.$blog_post['endDate']);


            $args = array(
                'post_type'   => 'post',
                'meta_key' => 'blogId',
                'meta_value' => $blog_post['blogId'],
                'post_status' => 'publish',
                'numberposts' => 1
            );

            $my_posts = get_posts($args);

            //  write_log($my_posts);

            if ($my_posts) {

                //     write_log('Post already exists - Update Content');

                $blog_excerpt = wp_trim_words($blog_post['text'], 50);
                $blog_excerpt_arr = explode(" ", $blog_excerpt);
                if ($blog_excerpt_arr[count($blog_excerpt_arr) - 1][0] == "[") {
                    $blog_excerpt = wp_trim_words($blog_post['text'], 49);
                }

                $already_post = array(
                    'ID'           => $my_posts[0]->ID,
                    'post_content' => $blog_post['text'],
                    'post_name' => $blog_post['url'],
                    'post_title' => $blog_post['name'],
                    'post_excerpt'  => $blog_excerpt
                );

                // Update the post into the database
                wp_update_post($already_post);


                if ($blog_post['metaTitle'] != null) {

                    update_post_meta($my_posts[0]->ID, '_yoast_wpseo_title', $blog_post['metaTitle']);
                }

                if ($blog_post['metaDesc'] != null) {

                    update_post_meta($my_posts[0]->ID, '_yoast_wpseo_metadesc', $blog_post['metaDesc']);
                }

                update_post_meta($my_posts[0]->ID, 'cde_blog_post', 'true');
                update_post_meta($my_posts[0]->ID, 'blogId',  $blog_post['blogId']);
            } else {

                //    write_log('Post do not exists - Insert Post');
                if ($current_date == $post_sync_date || $current_date > $post_sync_date) {

                    if ($current_date < $blog_post['endDate'] || $blog_post['endDate'] == '') {

                        $idObj = get_category_by_slug('blog');
                        if ($idObj) {
                            $blogid = $idObj->term_id;
                        }
                        // Gather post data.
                        $site_post = array(
                            'post_title'    => $blog_post['name'],
                            'post_name'     => $blog_post['url'],
                            'post_content'  => $blog_post['text'],
                            'post_excerpt'  => wp_trim_words($blog_post['text'], 50),
                            'post_type'     => 'post',
                            'post_category' => array(@$blogid),
                            'post_status'   => 'publish',
                            'post_author'  => 1,
                            'post_date'  => $postdate,
                            'comment_status' => 'closed',   // if you prefer
                            'ping_status' => 'closed'

                        );

                        $blog_id = wp_insert_post($site_post);
                        $tags = array($blog_post['tags']); // Array of Tags to add

                        wp_set_post_tags($blog_id, $tags); // Set tags to Post                       


                        if ($blog_post['metaTitle'] != null) {

                            update_post_meta($blog_id, '_yoast_wpseo_title', $blog_post['metaTitle']);
                        }

                        if ($blog_post['metaDesc'] != null) {

                            update_post_meta($blog_id, '_yoast_wpseo_metadesc', $blog_post['metaDesc']);
                        }

                        update_post_meta($blog_id, 'cde_blog_post', 'true');
                        update_post_meta($blog_id, 'blogId', $blog_post['blogId']);



                        // Add Featured Image to Post
                        if ($blog_post['images'][0]['url']) {

                            $image_url        = 'http://' . $blog_post['images'][0]['url'];
                            $extension        = pathinfo($image_url);
                            $image_name       = $blog_post['url'] . '.' . $extension['extension'];
                            $upload_dir       = wp_upload_dir(); // Set upload folder
                            $image_data       = file_get_contents($image_url); // Get image data
                            $unique_file_name = wp_unique_filename($upload_dir['path'], $image_name); // Generate unique name
                            $filename         = basename($unique_file_name); // Create image file name

                            // Check folder permission and define file location
                            if (wp_mkdir_p($upload_dir['path'])) {
                                $file = $upload_dir['path'] . '/' . $filename;
                            } else {
                                $file = $upload_dir['basedir'] . '/' . $filename;
                            }

                            // Create the image  file on the server
                            file_put_contents($file, $image_data);

                            $filename = $file; // Full path

                            // The ID of the post this attachment is for.
                            $parent_post_id = $blog_id;

                            // Check the type of file. We'll use this as the 'post_mime_type'.
                            $filetype = wp_check_filetype(basename($filename), null);

                            // Get the path to the upload directory.
                            $wp_upload_dir = wp_upload_dir();

                            // Prepare an array of post data for the attachment.
                            $attachment = array(
                                'guid'           => $wp_upload_dir['url'] . '/' . basename($filename),
                                'post_mime_type' => $filetype['type'],
                                'post_title'     => preg_replace('/\.[^.]+$/', '', basename($filename)),
                                'post_content'   => '',
                                'post_status'    => 'inherit'
                            );

                            // Insert the attachment.
                            $attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);

                            // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
                            require_once(ABSPATH . 'wp-admin/includes/image.php');

                            // Generate the metadata for the attachment, and update the database record.
                            $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                            wp_update_attachment_metadata($attach_id, $attach_data);


                            // And finally assign featured image to post
                            set_post_thumbnail($blog_id, $attach_id);
                        }
                    }
                }
            }


            if ($blog_post['endDate'] != '' &&  $current_date > $blog_post['endDate']) {

                //  write_log('Delete Post condition----->'.$my_posts[0]->ID);
                wp_delete_post(@$my_posts[0]->ID, true);
            }
        }
    }
}


function callSaleAPI($apiObj, $result)
{
    $inputs = array();
    $headers = array('authorization' => "bearer " . $result['access_token']);
    $promos = $apiObj->call(PROMOSAPI . get_option('SITE_CODE'), "GET", $inputs, $headers);
    $website_info = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;

    if (!empty($salespriority)) {

        $high_priprity = min($salespriority);
    }

    $saleslider = array();
    $saleData = array();
    $salepriority = array();
    $saleBanner = array();
    $slide_brands = '';
    $e = 0;

    if (isset($promos['success']) && $promos['result'] == '') {

        update_option('salesliderinformation', '');
        update_option('salebannerdata', '');
    }
    if (isset($promos['success']) && isset($promos['result'])) {

        update_option('salesliderinformation', '');
        update_option('salebannerdata', '');

        if (count($promos['result']) > 0) {

            $promos_json = json_encode($promos['result']);
            get_option('promos_json') || get_option('promos_json') == null ? update_option('promos_json', $promos_json) : add_option('promos_json', $promos_json);
            $promos = json_decode(get_option('promos_json'));

            $k = 0;

            $y = 0;

            foreach ($promos as $promo) {


                // if($promo->franchiseName != $website_info['result']['franchiseName'] ){                  

                //          continue;
                //  }

                $salepriority[$promo->promoCode] =  $promo->priority;

                $saleData[$y]['sale_name'] = $promo->name;
                $saleData[$y]['promoCode'] = $promo->promoCode;
                $saleData[$y]['promoType'] = $promo->promoType;
                $saleData[$y]['formType'] = $promo->formType;
                $saleData[$y]['name'] = $promo->name;
                $saleData[$y]['startDate'] = $promo->startDate;
                $saleData[$y]['endDate'] = $promo->endDate;
                $saleData[$y]['isRugShop'] = $promo->rugShop;
                $saleData[$y]['getCoupon'] = $promo->getCoupon;
                $saleData[$y]['brandList'] = $promo->brandList;


                foreach ($promo->widgets as $widget) {


                    if ($widget->type == "slide" && in_array(get_option('SITE_CODE'), $widget->clientCodes)) {

                        $saleslider[$k]['promoCode'] = $promo->promoCode;
                        $saleslider[$k]['name']     = $promo->name;
                        $saleslider[$k]['priority'] = $promo->priority;
                        $saleslider[$k]['promoType'] = $promo->promoType;
                        $saleslider[$k]['formType'] = $promo->formType;
                        $saleslider[$k]['startDate'] = $promo->startDate;
                        $saleslider[$k]['endDate'] = $promo->endDate;
                        $saleslider[$k]['slide_link'] = $widget->link;
                        $saleslider[$k]['isRugShop'] = $promo->rugShop;
                        $saleslider[$k]['getCoupon'] = $promo->getCoupon;
                        $saleslider[$k]['brandList'] = $promo->brandList;

                        if ($promo->brandList != '') {
                            foreach ($promo->brandList as $promobrand) {

                                if ($promo->priority ==  $high_priprity) {

                                    $slide_brands .= sanitize_title($promobrand) . ',';
                                }
                            }
                        }



                        $Slidearray = json_decode(json_encode($widget->images), True);

                        foreach ($widget->images as $slide) {

                            if ($slide->type == "graphic") {
                                //  $slider['slider_coupan_img'] =  $slide->url;
                                $saleslider[$k]['slider']['slider_coupan_img'] = "https://" . $slide->url;
                                $saleslider[$k]['slider']['link'] = $widget->link;
                                $saleslider[$k]['slider']['order'] = $widget->order;
                            }

                            if ($slide->type == "background") {
                                //  $slider['slider_bg_img'] =  $slide->url;
                                $saleslider[$k]['slider']['slider_bg_img'] = "https://" . $slide->url;
                                $saleslider[$k]['background_image'] = "https://" . $slide->url;
                            }
                        }
                        $k++;
                    }
                }


                $y++;
            }

            $slider_json = json_encode($saleslider);
            update_option('salesliderinformation', $slider_json);
            $slide_brands = rtrim($slide_brands, ",");
            update_option('salesbrand', $slide_brands);
            update_option('salespriority', json_encode($salepriority));

            $sliderdata_json = json_encode($saleData);
            update_option('saleconfiginformation', $salepriority);


            $s = 0;

            foreach ($promos as $promo) {


                // if($promo->franchiseName != $website_info['result']['franchiseName'] ){                  

                //     continue;
                //  }

                $seleinformation[$promo->promoCode]['sale_name'] = $promo->name;
                $seleinformation[$promo->promoCode]['promoCode'] = $promo->promoCode;
                $seleinformation[$promo->promoCode]['name'] = $promo->name;
                $seleinformation[$promo->promoCode]['priority'] = $promo->priority;
                $seleinformation[$promo->promoCode]['promoType'] = $promo->promoType;
                $seleinformation[$promo->promoCode]['formType'] = $promo->formType;
                $seleinformation[$promo->promoCode]['formName'] = $promo->formName;
                $seleinformation[$promo->promoCode]['formTitle'] = $promo->formTitle;
                $seleinformation[$promo->promoCode]['brandList'] = $promo->brandList;
                $seleinformation[$promo->promoCode]['startDate'] = $promo->startDate;
                $seleinformation[$promo->promoCode]['endDate'] = $promo->endDate;
                $seleinformation[$promo->promoCode]['isRugShop'] = $promo->rugShop;
                $seleinformation[$promo->promoCode]['getCoupon'] = $promo->getCoupon;
                $seleinformation[$promo->promoCode]['brandList'] = $promo->brandList;

                foreach ($promo->widgets as $widget) {



                    if ($widget->type == "message") {
                        $i = 0;
                        $seleinformation[$promo->promoCode]['message_link'] = $widget->link;
                        foreach ($widget->texts as $text) {
                            if ($text->type == "copy") {
                                $seleinformation[$promo->promoCode]['message'] = $text->text;
                            }
                        }
                        $i++;
                    }
                    if ($widget->type == "navigation") {
                        $i = 0;
                        $seleinformation[$promo->promoCode]['navigation_link'] = $widget->link;
                        foreach ($widget->images as $slide) {

                            if ($slide->type == "graphic") {

                                $seleinformation[$promo->promoCode]['navigation_img'] = "https://" . $slide->url;
                            }
                        }
                        foreach ($widget->texts as $text) {
                            if ($text->type == "copy") {
                                $seleinformation[$promo->promoCode]['navigation_text'] = $text->text;
                            }
                        }
                        $i++;
                    }

                    if ($widget->type == "banner") {
                        $i = 0;
                        $seleinformation[$promo->promoCode]['banner_link'] = $widget->link;
                        foreach ($widget->images as $slide) {

                            if ($widget->name == "Category Banner, Category Page ") {

                                if ($slide->type == "mobile") {

                                    $seleinformation[$promo->promoCode]['category_banner_img_mobile'] = "https://" . $slide->url;
                                }
                                if ($slide->type == "desktop") {

                                    $seleinformation[$promo->promoCode]['category_banner_img_deskop'] = "https://" . $slide->url;
                                }
                            } else {

                                if ($slide->type == "mobile") {

                                    $seleinformation[$promo->promoCode]['banner_img_mobile'] = "https://" . $slide->url;
                                }
                                if ($slide->type == "desktop") {

                                    $seleinformation[$promo->promoCode]['banner_img_deskop'] = "https://" . $slide->url;
                                }
                            }
                        }
                        $i++;
                    }
                    if ($widget->type == "banner") {



                        $saleBanner[$e]['name'] = $widget->name;
                        $saleBanner[$e]['type'] = $widget->type;
                        $saleBanner[$e]['link'] =  $widget->link;
                        $saleBanner[$e]['priority'] =  $promo->priority;
                        $saleBanner[$e]['promoCode'] =  $promo->promoCode;
                        $saleBanner[$e]['rugShop'] =  $promo->rugShop;
                        $saleBanner[$e]['name'] =  $promo->name;

                        foreach ($widget->images as $banner) {

                            $saleBanner[$e]['images'][$banner->type] = "https://" . $banner->url;
                            $saleBanner[$e]['size'] = $banner->size;

                            if ($banner->size == 'fullwidth') {


                                $seleinformation[$promo->promoCode]['banner']['fullwidth']['size'] = $banner->size;
                                $seleinformation[$promo->promoCode]['banner']['fullwidth']['link'] = $widget->link;
                                if ($banner->type == 'desktop') {
                                    $seleinformation[$promo->promoCode]['banner']['fullwidth']['desktop_url'] = $banner->url;
                                }
                                if ($banner->type == 'mobile') {
                                    $seleinformation[$promo->promoCode]['banner']['fullwidth']['mobile_url'] = $banner->url;
                                }
                            }

                            if ($banner->size == 'fixedwidth') {


                                $seleinformation[$promo->promoCode]['banner']['fixedwidth']['size'] = $banner->size;
                                $seleinformation[$promo->promoCode]['banner']['fixedwidth']['link'] = $widget->link;
                                if ($banner->type == 'desktop') {
                                    $seleinformation[$promo->promoCode]['banner']['fixedwidth']['desktop_url'] = $banner->url;
                                }
                                if ($banner->type == 'mobile') {
                                    $seleinformation[$promo->promoCode]['banner']['fixedwidth']['mobile_url'] = $banner->url;
                                }
                            }
                        }
                        $e++;
                    }
                    if ($widget->type == "slide") {
                        $i = 0;
                        $seleinformation[$promo->promoCode]['slide_link'] = $widget->link;
                        foreach ($widget->images as $slide) {

                            if ($slide->type == "graphic") {

                                $seleinformation[$promo->promoCode]['slider'][$i]['slider_coupan_img'] = "https://" . $slide->url;
                                $seleinformation[$promo->promoCode]['slider'][$i]['link'] = $widget->link;
                            }
                            if ($slide->type == "background") {

                                $seleinformation[$promo->promoCode]['slider'][$i]['slider_bg_img'] = "https://" . $slide->url;
                                $seleinformation[$promo->promoCode]['background_image'] = "https://" . $slide->url;
                            }
                        }
                        $i++;
                    }
                    if ($widget->type == "landing") {

                        $seleinformation[$promo->promoCode]['landing_link'] = $widget->link;
                        foreach ($widget->images as $slide) {

                            if ($slide->type == "graphic") {
                                $seleinformation[$promo->promoCode]['slider_coupan_img'] = "https://" . $slide->url;
                                $seleinformation[$promo->promoCode]['image_onform'] = "https://" . $slide->url;
                            }

                            if ($slide->type == "background") {

                                $seleinformation[$promo->promoCode]['background_image_landing'] = "https://" . $slide->url;
                            }
                            if ($slide->type == "mobile") {

                                $seleinformation[$promo->promoCode]['image_onform_mobile'] = "https://" . $slide->url;
                            }
                        }

                        foreach ($widget->texts as $text) {

                            if (strcasecmp($text->type, "copy") == 0) {

                                $seleinformation[$promo->promoCode]['content'] = $text->text;
                            }

                            if (strcasecmp($text->type, "heading") == 0) {

                                $seleinformation[$promo->promoCode]['saveupto_doller'] = $text->text;
                            }

                            if (strcasecmp($text->type, "subheading") == 0) {

                                $seleinformation[$promo->promoCode]['subheading'] = $text->text;
                            }
                        }
                    }
                } //Widget

                $s++;
            }
            //$seleinformation = json_decode(get_option('saleinformation'));

            // echo $seleinformation;
            //exit;
            update_option('saleinformation', json_encode($seleinformation));
            update_option('salebannerdata', json_encode($saleBanner));
        } else {
            $_SESSION['error'] = "Promos Information API";
            $_SESSION["error_desc"] = "We didn't get sale information from API OR Sale is not assigned to this site";
            get_option('promos_json') ? update_option('promos_json', "") : "";
            get_option('saleinformation') ? update_option('saleinformation', "") : "";
            update_option('salesliderinformation', '');
            update_option('salebannerdata', '');
        }

        //get coupon show/hide
        $salecoupon =  array();
        foreach ($promos as $sale) {

            if ($sale->getCoupon == '1') {

                $salecoupon[] =  $sale->getCoupon;
            }
        }
        //
        // write_log($salecoupon);

        if (count($salecoupon) == 0) {
            // write_log( "Array is empty");
            update_option('getcouponbtn', 0);
            // update_option('getcouponreplace', 1);
        } else {
            //  write_log( "Array is non- empty");
            update_option('getcouponbtn', 1);
            update_option('getcouponreplace', 0);
        }

        $saleinformation = json_decode(get_option('saleinformation'));
        $saleinformation = (array)$saleinformation;
        $getcoupon_link = '';
        $salePrio =  array();
        $i = 0;
        foreach ($saleinformation as $key => $value) {
            if ($value->getCoupon == '1' && $value->isRugShop == '') {

                $salePrio[$i] = array("priority" => $value->priority, "promoCode" => $value->promoCode, "slide_link" => @$value->slide_link, "startDate" => $value->startDate);
                $i++;
            }
        }

        $vc_array_value = array();
        $vc_array_name = array();
        foreach ($salePrio as $key => $row) {
            $vc_array_value[$key] = $row['priority'];
            $vc_array_name[$key] = $row['startDate'];
        }
        array_multisort($vc_array_value, SORT_ASC, $vc_array_name, SORT_ASC, $salePrio);

        // write_log($salePrio);

        if (count($salePrio) > 0) {

            $getcoupon_link = $salePrio['0']['slide_link'];
        }
        update_option('getcoupon_link', $getcoupon_link);
    } else {

        $msg = $promos['message'];
        $_SESSION['error'] = "Promos Information API";
        $_SESSION["error_desc"] = $msg;
        get_option('promos_json') ? update_option('promos_json', "") : "";
        get_option('saleinformation') ? update_option('saleinformation', "") : "";
        update_option('salesliderinformation', '');
    }
}



/**  Plugin updater checker integration **/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/mobilemarketingllc/mm-retailer-plugin',
    __FILE__,
    'mm-retailer-plugin'
);

$myUpdateChecker->setAuthentication(array(
    'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
    'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));



//Optional: Set the branch that contains the stable release.
if (ENV == 'dev' || ENV == 'staging') {
    //var_dump($myUpdateChecker->getBranch());
    // echo '<pre>';
    $myUpdateChecker->setBranch('main');
    // exit;

} else if (ENV == 'prod') {
    $myUpdateChecker->setBranch('main');
}


function documentation_func()
{
?>
    <div class="wrap" id="grandchild-backend">
        <h2>ShortCodes - Documentation </h2>
        <div class="description"></div>
        <table class="widefat striped" border="1" style="border-collapse:collapse;">
            <thead>
                <tr>
                    <th width="30%"><strong>Shortcode</strong></th>
                    <th width="35%"><strong>Example</strong></th>
                    <th width="30%"><strong>Description</strong></th>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Sale information</th>
                </tr>
                <tr class="saleapi">
                    <td> [coupon "homepage_banner"]</td>
                    <td> [coupon "homepage_banner"]</td>
                    <td>Use this shortcode if there is no slider on homepage.</td>
                </tr>
                <tr class="saleapi">
                    <td> [coupon "banner"]</td>
                    <td> [coupon "banner"]</td>
                    <td>Use this shortcode for display only banner.</td>
                </tr>
                <tr class="saleapi">
                    <td> [coupon "navigation" "text"]</td>
                    <td> [coupon "navigation" "text"]</td>
                    <td>Top for navigation text (Save up to $1000)</td>
                </tr>
                <tr class="saleapi">
                    <td> [coupon "navigation" "image"]</td>
                    <td> [coupon "navigation" "image"]</td>
                    <td>In navigation if there is banner images of sale.</td>
                </tr>
                <tr class="saleapi">
                    <td> [coupon "message"]</td>
                    <td> [coupon "message"]</td>
                    <td>Sale Message Block ( Save $1,000 OFF Your Next Flooring Purchase)</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "salebanner" "full"]</td>
                    <td>[coupon "salebanner" "full"] OR [coupon "salebanner"]</td>
                    <td>Display the promo banner images having Full width and height (2800 by 200).</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "salebanner" "fixed"]</td>
                    <td>[coupon "salebanner" "fixed"]</td>
                    <td>Display the promo banner images having Fixed width and height (1800 by 180).</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "heading"]</td>
                    <td>[coupon "heading"]</td>
                    <td>Display the heading (Save upto $1000 text ).</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "subheading"]</td>
                    <td>[coupon "subheading"]</td>
                    <td>Display the subheading (Fill out below form... ).</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "content"]</td>
                    <td>[coupon "content"]</td>
                    <td>Display the text below the form on flooring coupon page.</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "image_onform" "background_img"]</td>
                    <td>[coupon "image_onform" "background_img"]</td>
                    <td>Display the image beside the form on flooring coupon page.(background_img is option if we requied backgrond image)</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "print_coupon"]</td>
                    <td>[coupon "print_coupon"]</td>
                    <td>Display the print image button with print coupon button</td>
                </tr>
                <tr class="saleapi">
                    <td>[coupon "popup_img"]</td>
                    <td>[coupon "popup_img"]</td>
                    <td>Display the popup image with coupon landing page link</td>
                </tr>

                <tr class="saleapi header-tr">
                    <th colspan="3">Area Rug Information</th>
                </tr>
                <tr class="arearug">
                    <td>[areagallery]</td>
                    <td>[areagallery]</td>
                    <td>Display the Area Rug Image Gallery.</td>
                </tr>
                <tr class="arearug">
                    <td>
                        <div class="copy">[shoparearug]</div>
                    </td>
                    <td>[shoparearug]</td>
                    <td>Display Area rugs six categories with area rug site link.</td>
                </tr>
                <tr class="arearug">
                    <td>[area_rug_trading_products]</td>
                    <td>[area_rug_trading_products]</td>
                    <td>Display Area rugs trading products with area rug site link.</td>
                </tr>
                <tr class="arearug">
                    <td>[rugshop_button "title"]</td>
                    <td>[rugshop_button "SHOP RUGS"]</td>
                    <td>Display button with area rugshop affiliate site link.</td>
                </tr>
                <tr class="arearug">
                    <td>[rugshop_link "title"]</td>
                    <td>[rugshop_link "SHOP RUGS"]</td>
                    <td>Display anchor tag with area rugshop affiliate site link.</td>
                </tr>
                <tr class="arearug">
                    <td>[affiliate_code]</td>
                    <td>https://rugs.shop/en_us/oriental-weavers-henderson-625w-grey-625w9/?store=[affiliate_code]</td>
                    <td>Return area rugshop affiliate code only.</td>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Store Information</th>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "dir" "Location Name / Location ID"] </td>
                    <td>[storelocation_address "dir" "CDE Location Name OR Location ID" ]</td>
                    <td>Display button with "Get Direction" text link to the store address</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "dirlink" "Location Name / Location ID"] </td>
                    <td>[storelocation_address "dirlink" "CDE Location Name OR Location ID" ]</td>
                    <td>Returns text link of the store address</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "map" "Location Name / Location ID"] </td>
                    <td>[storelocation_address "map" "CDE Location Name OR Location ID" ]</td>
                    <td>Display map of the store address</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "loc" "Location Name / Location ID"] </td>
                    <td>[storelocation_address "loc" "CDE Location Name OR Location ID" ]</td>
                    <td>Display the store address with having link to the google map.</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "loc" "Location Name / Location ID" "nolink"] </td>
                    <td>[storelocation_address "loc" "CDE Location Name OR Location ID" "nolink"]</td>
                    <td>Display the store address with having without link to the google map.</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "inline_loc" "Location Name / Location ID"] </td>
                    <td>[storelocation_address "inline_loc" "CDE Location Name OR Location ID" "nolink"]</td>
                    <td>Display the store address in single line with having link to the google map.</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "inline_loc" "Location Name / Location ID" "nolink"] </td>
                    <td>[storelocation_address "inline_loc" "CDE Location Name OR Location ID" "nolink"]</td>
                    <td>Display the store address in single line with having without link to the google map.</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "forwardingphone" "Location Name / Location ID"] </td>
                    <td>[storelocation_address "forwardingphone" "CDE Location Name OR Location ID"]</td>
                    <td>Display the forwading number of the store with link.</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "forwardingphone" "Location Name / Location ID" "nolink"] </td>
                    <td>[storelocation_address "forwardingphone" "CDE Location Name OR Location ID" "nolink"]</td>
                    <td>Display the forwading number of the store without link.</td>
                </tr>
                <tr class="address">
                    <td>[storelocation_address "ohrs" "Location Name / Location ID"]</td>
                    <td>[storelocation_address "ohrs" "CDE Location Name OR Location ID"]</td>
                    <td>Display the store location opening and closing hours.</td>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Retailer Information</th>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "city"]</td>
                    <td>[Retailer "city"]</td>
                    <td>Display the social icons.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "state"]</td>
                    <td>[Retailer "state"]</td>
                    <td>Display the state of first store address which are added.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "zipcode"]</td>
                    <td>[Retailer "zipcode"]</td>
                    <td>Display the zipcode of first store address which are added.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "legalname"]</td>
                    <td>[Retailer "legalname"]</td>
                    <td>Display the legalname of site.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "address"]</td>
                    <td>[Retailer "address"]</td>
                    <td>Display the address line of site.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "phone" "nolink"]</td>
                    <td>[Retailer "phone"]</td>
                    <td>Display the phone without of site.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "phone"]</td>
                    <td>[Retailer "phone"]</td>
                    <td>Display the phone of with link.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "forwarding_phone"]</td>
                    <td>[Retailer "forwarding_phone"]</td>
                    <td>Display the forwarding phone with link of tel.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "forwarding_phone" "nolink"]</td>
                    <td>[Retailer "forwarding_phone" "nolink"]</td>
                    <td>Display the Forwarding phone without the link .</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "companyname"]</td>
                    <td>[Retailer "companyname"]</td>
                    <td>Display the Company Name of site.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "site_url"]</td>
                    <td>[Retailer "site_url"]</td>
                    <td>Display the Site URL of site.</td>
                </tr>
                <tr class="retailer">
                    <td>[Retailer "seo_location"]</td>
                    <td>[Retailer "seo_location"]</td>
                    <td>Display seo location attribute of site.</td>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Site Information</th>
                </tr>
                <tr class="social">
                    <td>[copyrights]</td>
                    <td>[copyrights]</td>
                    <td>Display the copyrights text use in footer.</td>
                </tr>
                <tr class="social">
                    <td>[getSocailIcons]</td>
                    <td>[getSocailIcons]</td>
                    <td>Display the social icons.</td>
                </tr>
                <tr class="social">
                    <td>[chat_meter]</td>
                    <td>[chat_meter]</td>
                    <td>Display Reviews of location</td>
                </tr>
                <tr class="social">
                    <td>[wells_fargo_finance "button text"]</td>
                    <td>[wells_fargo_finance "Apply for financing"]</td>
                    <td>Display wells fargo finance link button</td>
                </tr>
                <tr class="social">
                    <td>[wells_fargo_finance_link]</td>
                    <td>[wells_fargo_finance_link]</td>
                    <td>Returns wells fargo finance link</td>
                </tr>
                <tr class="social">
                    <td>[synchrony_finance "button text"]</td>
                    <td>[synchrony_finance "Apply for financing"]</td>
                    <td>Display synchrony finance link button</td>
                </tr>
                <tr class="social">
                    <td>[synchrony_finance_link]</td>
                    <td>[synchrony_finance_link]</td>
                    <td>Returns synchrony finance link</td>
                </tr>
                <tr class="social">
                    <td>[Link "site respective url"]</td>
                    <td>[Link "/about/location/"]</td>
                    <td>Returns provided link if page/post exists in site , if not then return home page url (/)</td>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Accessibility</th>
                </tr>
                <tr class="social">
                    <td>[accessibility "phone"]</td>
                    <td>[accessibility "phone"]</td>
                    <td>Returns phone number from Accessibility Contact </td>
                </tr>
                <tr class="social">
                    <td>[accessibility "email"]</td>
                    <td>[accessibility "email"]</td>
                    <td>Returns email address from Accessibility Contact </td>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Swell Integration</th>
                </tr>
                <tr class="social">
                    <td>[swell_review]</td>
                    <td>[swell_review]</td>
                    <td>Integrate swell review widget </td>
                </tr>
                <tr class="saleapi header-tr">
                    <th colspan="3">Birdeye Review Integration</th>
                </tr>
                <tr class="social">
                    <td>[birdeye_review_ratecount]</td>
                    <td>[birdeye_review_ratecount]</td>
                    <td>Integrate Birdeye review average and star rating </td>
                </tr>
                <tr class="social">
                    <td>[birdeye_review_list]</td>
                    <td>[birdeye_review_list]</td>
                    <td>Integrate Birdeye review list </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    </div>
    <?php
}

//Create Form for client code and site id:
//Create Form for client code and site id:
function Calling_API_form($site, $clientcode)
{


    if (isset($_SESSION['error'])) {
    ?>
        <div class="notice notice-info error-info is-dismissible woo-info">
            <div class="info-image">
                <p> <img src='<?php echo plugins_url("/img/info-alt.png", __FILE__) ?>' width="48px" /></p>
            </div>
            <div class="info-descriptions">
                <div class="info-descriptions-title">
                    <h3><strong><?php echo ucfirst($_SESSION["error"]); ?></strong></h3>
                </div>
                <p><?php echo $_SESSION["error_desc"]; ?></p>
                <p><b>Please Contact Plugin Development Team for further details</b></p>
            </div>
        </div>
    <?php
    }

    $is_own_templates = get_option('is_own_templates');

    $sh_get_finance = get_option('sh_get_finance');

    $area_sync =  get_option('arearugsync');
    $arearugbrand =  get_option('arearugs_brand') ? get_option('arearugs_brand') : array();
    $groupbysize_cat =  get_option('groupbysize_cat') ? get_option('groupbysize_cat') : array();
    $nogroupbysize_cat =  get_option('nogroupbysize_cat') ? get_option('nogroupbysize_cat') : array();
    $rughtml = "";
    $product_json =  json_decode(get_option('product_json'));
    $rugs_array = getArrayFiltered('productType', 'rugs', $product_json);

    //  print_r($arearugbrand);

    if ($rugs_array != '') {

        foreach ($rugs_array as $rugs) {

            if ($arearugbrand != '') {

                $rughtml .= '<label>
                                <input type="checkbox" name="arearugs_brand[]" value="' . esc_attr($rugs->manufacturer) . '" ' . (in_array($rugs->manufacturer, $arearugbrand) ? 'checked' : '') . '  />
                                ' . ucfirst($rugs->manufacturer) . '
                            </label>';
            } else {

                $rughtml .= '<label>
                                <input type="checkbox" name="arearugs_brand[]" value="' . esc_attr($rugs->manufacturer) . '" />
                                ' . ucfirst($rugs->manufacturer) . '
                            </label>';
            }
        }
    }

    $hide_getcoupon_button = get_option('hide_getcoupon_button');
    $default_tab = null;
    $tab = isset($_GET['tab']) ? $_GET['tab'] : $default_tab;

    $global_categories  = array(
        'carpet',
        'hardwood',
        'laminate',
        'lvt',
        'tile',
        'area_rugs',
        'paint',
        'sheet'
    );

    $form = '
                <div id="wpcontent1" class="client_info_wrap">
                    <div  class="wrap">
        
                        <nav class="nav-tab-wrapper">
                            <a href="?page=retailer-settings" class="nav-tab  ' . ($tab === null ? 'nav-tab-active' : '') . ' ">Retailer Settings</a>
                            <a href="?page=retailer-settings&tab=retailer-info" class="nav-tab  ' . ($tab === 'retailer-info' ? 'nav-tab-active' : '') . ' ">Retailer Info</a>
                            <a href="?page=retailer-settings&tab=react-settings-for-plp-pdp" class="nav-tab ' . ($tab === 'react-settings-for-plp-pdp' ? 'nav-tab-active' : '') . ' ">React Settings for PLP/PDP</a>
                            <a href="?page=retailer-settings&tab=brand-order-settings" class="nav-tab ' . ($tab === 'brand-order-settings' ? 'nav-tab-active' : '') . ' ">React Brand Order Settings</a>
                            <a href="?page=retailer-settings&tab=collection-order-settings" class="nav-tab ' . ($tab === 'collection-order-settings' ? 'nav-tab-active' : '') . '  ">React Collection Order Settings</a>
                            <a href="?page=retailer-settings&tab=facets-for-plp" class="nav-tab ' . ($tab === 'facets-for-plp' ? 'nav-tab-active' : '') . '">React Facets For PLP</a>    
                            
                        </nav>
                        <div class="tab-content">';

    switch ($tab):
        case 'react-settings-for-plp-pdp':


            $form .= '<form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
    
    <table class="form-table">
        <tr>
            <th ><h4>React Settings for PLP/PDP</h4></th>
        </tr>
    </table >
    
    <table class="form-table">
      
    <tr>
    <td>
      <div style="width: 100%;" class="wrap tab react-settings-plp-pdp">
        
        <div id="instock-settings" >
            <h2>Instock Settings</h2>    
            <table width="100%" >
                    <tr>
                        <td width="50%" style="vertical-align: top!important;">
                            <table class="in-table" >
                                <tr>
                                    
                                    <td class="form-group">
                                        <label  class="labelBlock">Sync instock product in seprate post type?</label>
                                        <select name="instocksyncseprate" >
                                            <option value="0" ' . (get_option('instocksyncseprate') != 1 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('instocksyncseprate') == 1 ? 'selected=selected' : null) . '>Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                 <tr>
                                    
                                    <td class="form-group">
                                        <label  class="labelBlock">Show all instock and normal colors available on PDP</label>
                                        <select name="allinstock" >
                                            <option value="0" ' . (get_option('allinstock') != 1 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('allinstock') == 1 ? 'selected=selected' : null) . '>Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td class="form-group">
                                        <label  class="labelBlock">Hide GET COUPON button/link from PLP/PDP</label>
                                        <select name="hideinstockcoupon" >
                                            <option value="0" ' . (get_option('hideinstockcoupon') != 1 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('hideinstockcoupon') == 1 ? 'selected=selected' : null) . '>Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="2" style="padding: 0px!important;  vertical-align: top;">
                                            <table class="inner-table" width="100%">
                                                <tr>
                                                    
                                                    <td class="form-group">  
                                                        <label class="labelBlock">Do you want to show form on instock page?</label>                 
                                                        <select name="pdpshowform" id="pdpshowform" onchange="hideOptions( \'' . ".row_showforminstock" . '\')">
                                                        <option value="0" ' . (get_option('pdpshowform') == 0 ? 'selected=selected' : null) . '>No</option>
                                                        <option value="1" ' . (get_option('pdpshowform') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                                    </select>
                                                    </td>
                                                </tr>';
            $saved_shortcode_form = get_option('pdpshowforminstock', '');
            $saved_shortcode_softsurface_form = get_option('softsurfacepdpshowforminstock', '');

            $form .= ' <tr class="row_showforminstock" ' . (get_option('pdpshowform') == 1 ? null : 'style="display:none"') . '>
                                                
                                                <td class="form-group"> 
                                                    <label class="labelBlock"> Soft Surface PDP Form Shortcode </label> 
                                                    <input type="text" name="softsurfacepdpshowforminstock" id="softsurfacepdpshowforminstock" value="' . esc_attr($saved_shortcode_softsurface_form) . '" size="0" placeholder="Enter soft surface shortcode here" />
                                                </td>
                                            </tr>';
            $form .= ' <tr class="row_showforminstock" ' . (get_option('pdpshowform') == 1 ? null : 'style="display:none"') . '>
                                                
                                                <td class="form-group"> 
                                                    <label class="labelBlock">Hard Surface PDP Form Shortcode </label> 
                                                    <input type="text" name="pdpshowforminstock" id="pdpshowforminstock" value="' . esc_attr($saved_shortcode_form) . '" size="0" placeholder="Enter shortcode here" />
                                                </td>
                                            </tr>';


            $form .= '</table>
                                        </td>
                                </tr>
                            </table>
                        </td> 
                        <td width="50%">
                            <table class="in-table"  width="100%">
                                <tr>
                                    <td colspan="2" style="padding: 0px!important;">
                                    <table class="inner-table">
                                        <tr>
                                        
                                            <td class="form-group">   
                                                <label class="labelBlock">Do you want to Show request estimate button for instock PLP?</label>                
                                                <select name="plpestimatebutton" id="plpestimatebutton">
                                                <option value="0" ' . (get_option('plpestimatebutton') == 0 ? 'selected=selected' : null) . '>No</option>
                                                <option value="1" ' . (get_option('plpestimatebutton') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                            </select>
                                            </td>
                                        </tr>';
            $saved_estimate_button_link = get_option('plpestimatebuttonlink', '');
            $form .= '<tr class="row_showestimatebuttonlink"  ' . (get_option('plpestimatebutton') == 1 ? null : 'style="display:none"') . '>
                                            
                                            <td class="form-group"> 
                                                <label class="labelBlock">Request estimate button link :</label>
                                                <input type="text" name="plpestimatebuttonlink" id="plpestimatebuttonlink" value="' . esc_attr($saved_estimate_button_link) . '" size="0" placeholder="Enter your link here" />
                                            </td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td>       
                                        <label class="labelBlock">Do you want to show price?</label>
                                        <select name="plpshowprice" id="plpshowprice">
                                            <option value="0" ' . (get_option('plpshowprice') == 0 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('plpshowprice') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                        </select>
                                    </td>
                                </tr>
                                 <tr class="row_showprice" ' . (get_option('plpshowprice') == 1 ? null : 'style="display:none"') . '>
                                    <td>  
                                        <label class="labelBlock">Show price order</label>
                                        <select name="product_order_by_price" id="product_order_by_price">
                                            <option value="any" ' . (get_option('product_order_by_price') == "noorder" ? 'selected=selected' : null) . '>Any</option>
                                            <option value="asc" ' . (get_option('product_order_by_price') == "asc" ? 'selected=selected' : null) . '>Price low to high</option>
                                             <option value="desc" ' . (get_option('product_order_by_price') == "desc" ? 'selected=selected' : null) . '>Price high to low</option>  
                                        </select>                
                                    </td>
                                </tr>
                                <tr class="row_showprice" ' . (get_option('plpshowprice') == 1 ? null : 'style="display:none"') . '>
                                    <td>  
                                        <label class="labelBlock">Show retail and special price for hardsurface?</label>
                                        <select name="plpshowretailandspecialprice" id="plpshowretailandspecialprice">
                                            <option value="0" ' . (get_option('plpshowretailandspecialprice') == 0 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('plpshowretailandspecialprice') == 1 ? 'selected=selected' : null) . '>Yes</option>  
                                        </select>                
                                    </td>
                                </tr>
                                <tr class="row_showprice" ' . (get_option('plpshowprice') == 1 ? null : 'style="display:none"') . '>
                                    <td>  
                                        <label class="labelBlock">Show strike through price?</label>
                                        <select name="plpshowpricestrike" id="plpshowpricestrike">
                                            <option value="0" ' . (get_option('plpshowpricestrike') == 0 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('plpshowpricestrike') == 1 ? 'selected=selected' : null) . '>Yes</option>  
                                        </select>                
                                    </td>
                                </tr>
                                
                                <tr class="row_showprice">
                                    <td>  
                                        <label class="labelBlock">Do you want to show Call For Price on PDP?</label>
                                        <select name="showcallforprice" id="showcallforprice">
                                            <option value="0" ' . (get_option('showcallforprice') == 0 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('showcallforprice') == 1 ? 'selected=selected' : null) . '>Yes</option>  
                                        </select>                
                                    </td>
                                </tr>
                                <tr class="row_showprice">
                                    <td>  
                                        <label class="labelBlock">Do you want to show Share platforms on in-stock PDP?</label>
                                        <select name="showsharebtn" id="showsharebtn">
                                            <option value="0" ' . (get_option('showsharebtn') == 0 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('showsharebtn') == 1 ? 'selected=selected' : null) . '>Yes</option>  
                                        </select>                
                                    </td>
                                </tr>
                            </table>
                        </td>       
                    </tr>
            </table>        
        </div>    
        <div id="rug-settings" > 
        <h2>Area rug Settings</h2> 
           <table width="100%">
                <tr>
                    <td style="padding: 0px!important;" width="50%">
                        <table >
                            <tr>
                                <td style="padding: 0px!important;" colspan="2">
                                    <table class="inner-table">
                                        <tr>
                                            
                                            <td class="form-group">
                                                <label class="labelBlock">Sync area rug products?</label>
                                                <select name="arearugsync" id="arearugsync">
                                                    <option value="0" ' . (get_option('arearugsync') != 1 ? 'selected=selected' : null) . '>No</option>
                                                    <option value="1" ' . (get_option('arearugsync') == 1 ? 'selected=selected' : null) . '>Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr id="areabrand" style="display:' . (get_option('arearugsync') != 1 ? 'none' : null) . ';">
                                                
                                                <td class="form-group">
                                                <label class="labelBlock">Please select Brands which you want to sync?</label>
                                                ' . $rughtml . '
                                                </td>
                                        </tr>
                                    </table>
                                </td>       
                            </tr>
                            <tr>
                                <td class="form-group">  
                                    <label class="labelBlock">Do you want to show sku on PLP and PDP for Area Rugs?</label>                 
                                    <select name="arearugshowsku" id="arearugshowsku">
                                    <option value="0" ' . (get_option('arearugshowsku') == 0 ? 'selected=selected' : null) . '>No</option>
                                    <option value="1" ' . (get_option('arearugshowsku') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                </select>
                                </td>
                            </tr>
    
                        </table> 
    
                        
                    </td>
                    <td style="padding: 0px!important;" width="50%">';
            $area_rug_results = array("Karastan", "Stanton", "Nourison", "Anderson Tuftex", "Masland", "Fabrica", "Phenix", "Lasting Luxury", "Dixie Home", "Dreamweaver", "Mohawk", "Shaw Floors", "Shaw", "Beaulieu");
            $saved_data = get_option('Area_rugs_brands', []);
            $saved_rug_link = get_option('rugpageurl', '');


            $form .= '  <table class="inner-table">
                        <tr>
                            
                            <td class="form-group">   
                                <label class="labelBlock">Show area rug button?</label>                
                                <select name="arearugbutton" id="arearugbutton">
                                    <option value="0" ' . (get_option('arearugbutton') == 0 ? 'selected=selected' : null) . '>No</option>
                                    <option value="1" ' . (get_option('arearugbutton') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                </select>
                            </td>
                        </tr>
                        <tr class="row_area_rug_url" ' . (get_option('arearugbutton') == 1 ? null : 'style="display:none"') . '>
                            <td class="form-group">
                                 <label class="labelBlock">Rug Page Url :</label>
                                <input type="text" name="rugpageurl" id="rugpageurl" value="' . esc_attr($saved_rug_link) . '" size="0" placeholder="Enter your link here" />
                            </td>
                        </tr>
                        <tr class="row_area_rug_button" ' . (get_option('arearugbutton') == 1 ? null : 'style="display:none"') . '>       
                                 <td>
                                            <label class="labelBlock">Select Brands :</label>
                                            <div class="checkbox-container">';
            // Generate checkboxes for excerpts
            if (!empty($area_rug_results)) {

                foreach ($area_rug_results as $row) {
                    $is_checked = in_array($row, $saved_data) ? 'checked' : '';
                    $form .= '<label>
                                                                                <input type="checkbox" name="carpeting_excerpts[]" value="' . esc_attr($row) . '" ' . $is_checked . '>
                                                                                ' . esc_html($row) . '
                                                                            </label><br>';
                }
            } else {
                $form .= '<p>No brands available.</p>';
            }
            $form .= '</div>
                                 </td>
                            </tr>
                    </table>
                    
                    </td>
                </tr> 
                   
            </table>
        </div>
        <div id="paint-settings"  > 
        <h2>Paint Settings</h2> 
             <table>
                    <tr>
                      
                        <td class="form-group">
                            <label class="labelBlock">Do you want show paint?</label>
                            <select name="showpaint" >
                                <option value="0" ' . (get_option('showpaint') == 0 ? 'selected=selected' : null) . '>No</option>
                                <option value="1" ' . (get_option('showpaint') == 1 ? 'selected=selected' : null) . '>Yes</option>
                            </select>
                        </td>           
                    </tr>                        
             </table>                          
        </div>  
        <div id="sheet-settings"  > 
        <h2>Sheet Settings</h2> 
             <table>
                    <tr>
                        <td class="form-group">
                            <label class="labelBlock">Sync Sheet products?</label>
                            <select name="sheetsync" id="sheetsync">
                                <option value="0" ' . (get_option('sheetsync') != 1 ? 'selected=selected' : null) . '>No</option>
                                <option value="1" ' . (get_option('sheetsync') == 1 ? 'selected=selected' : null) . '>Yes</option>
                            </select>
                        </td>          
                    </tr>                        
             </table>                          
        </div>
        <div id="other-settings"  > 
        <h2>Other settings</h2> 
             <table width="100%">
                    <tr>
                        <td width="50%" style="vertical-align: top; padding: 0px!important;" >
                             <table class="table100">
                                <tr>
                                
                                    <td class="form-group">
                                        <label class="labelBlock">Show posts/pages in global search result?</label>
                                        <select name="postsgloblesearch" >
                                            <option value="0" ' . (get_option('postsgloblesearch') != 1 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('postsgloblesearch') == 1 ? 'selected=selected' : null) . '>Yes</option>
                                        </select>
                                    </td>         
                                </tr>  
                                 <tr>
                                            
                                                <td class="form-group">    
                                                    <label class="labelBlock">Do you want Workbook? </label>               
                                                    <select name="isworkbook" id="isworkbook">
                                                    <option value="0" ' . (get_option('isworkbook') == 0 ? 'selected=selected' : null) . '>No</option>
                                                    <option value="1" ' . (get_option('isworkbook') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                                </select>
                                                </td>
                                            </tr>  
                                 <tr>        
                                    <td class="form-group">    
                                        <label class="labelBlock">Do you want sample available button on PDP? </label>               
                                            <select name="showpdpsamplebtn" id="showpdpsamplebtn">
                                            <option value="0" ' . (get_option('showpdpsamplebtn') == 0 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('showpdpsamplebtn') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                        </select>
                                    </td>
                                </tr> 
                                <tr>        
                                    <td class="form-group">    
                                        <label class="labelBlock">Do you want to show Sample Available label on PLP/PDP? </label>               
                                            <select name="showsamplelabel" id="showsamplelabel">
                                            <option value="0" ' . (get_option('showsamplelabel') != 1 ? 'selected=selected' : null) . '>No</option>
                                            <option value="1" ' . (get_option('showsamplelabel') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                        </select>
                                    </td>
                                </tr>  
                                <tr>
                                    <td colspan="2" style="padding: 0px!important;">
                                        <table class="inner-table">
                                            <tr>
                                                
                                                <td class="form-group">   
                                                    <label class="labelBlock">Do you want Remove Group by Collection/Size? </label>                
                                                    <select name="nogroupbysize" id="nogroupbysize">
                                                    <option value="0" ' . (get_option('nogroupbysize') == 0 ? 'selected=selected' : null) . '>No</option>
                                                    <option value="1" ' . (get_option('nogroupbysize') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                                </select>
                                                </td>
                                            </tr>';
            $nogroupbysize = "";
            if (get_option('nogroupbysize') != '' && is_array($nogroupbysize_cat)) {

                foreach ($global_categories as $cat) {
                    $nogroupbysize .= '<label><input type="checkbox" name="nogroupbysize_cat[]" value="' . $cat . '" ' . (in_array($cat, $nogroupbysize_cat) ? 'checked' : '') . '/>' . ucfirst($cat) . '</label>';
                }
            }

            $groupbysize = "";
            if (get_option('groupbysize') != '' && is_array($groupbysize_cat)) {

                foreach ($global_categories as $cat) {
                    $groupbysize .= '<label><input type="checkbox" name="groupbysize_cat[]" value="' . $cat . '" ' . (in_array($cat, $groupbysize_cat) ? 'checked' : '') . '/>' . ucfirst($cat) . '</label>';
                }
            }

            $form .= '
                                            <tr id="nogroupbysizecat" style="display:' . (get_option('nogroupbysize') != 1 ? 'none' : null) . ';">
                                                <td class="form-group"> 
                                                    <label class="labelBlock">If yes select categories</label>
                                                    <div class="checkbox-container">' . $nogroupbysize . ' </div>                  
                                                </td>
                                            </tr>
    
                                        </table>
                                    </td>
                                </tr>        
                                
                             </table>       
                        </td>   
                        <td width="50%"  style="vertical-align: top;  padding: 0px!important;">
                             <table class="table100">
                              
                                <tr>
                                    <td colspan="2">
                                        <table class="inner-table">
                                           
                                            <tr>
                                            
                                                <td class="form-group">    
                                                    <label class="labelBlock">Do you want Group by size? </label>               
                                                    <select name="groupbysize" id="groupbysize">
                                                    <option value="0" ' . (get_option('groupbysize') == 0 ? 'selected=selected' : null) . '>No</option>
                                                    <option value="1" ' . (get_option('groupbysize') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                                </select>
                                                </td>
                                            </tr>
                                            <tr id="groupbysizecat" style="display:' . (get_option('groupbysize') != 1 ? 'none' : null) . ';">
                                             
                                                <td class="form-group">
                                                    <label class="labelBlock">If yes select categories</label>
                                                    <div class="checkbox-container">' . $groupbysize . ' </div>                   
                                                </td>
                                            </tr>
                                            <tr>
                                            
                                                <td class="form-group">    
                                                    <label class="labelBlock">Do you want to show sizes available on PDP? </label>               
                                                    <select name="groupbysizepdp" id="groupbysizepdp">
                                                    <option value="0" ' . (get_option('groupbysizepdp') == 0 ? 'selected=selected' : null) . '>No</option>
                                                    <option value="1" ' . (get_option('groupbysizepdp') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                                                </select>
                                                </td>
                                            </tr>
                                        </table>    
                                    </td>
                                </tr>
                                
                               
                             </table>     
                        </td>              
                    </tr>                
    
                                    
             </table>                          
        </div>
        </div>
       </td>                     
    </tr>
    <tr> 
        <td class="form-group">
            <button id="syncdata" type="submit" name="save_facet_area_rug" value="save_facet_area_rug" class="button button-primary button-hero" >Save</button>
        </td>
    </tr>
    </table>    
    </form>';



            break;
        case 'brand-order-settings':
            // Retrieve saved options
            $product_types = ['carpet', 'hardwood', 'laminate', 'lvt', 'tile'];
            $saved_product_brand_order = get_option('product_brand_order', []); // Get the nested array of product brand orders
            $selected_product_types = get_option('selected_product_types', []); // Get selected product types from options table

            $form .= '<form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                    <table class="form-table">
                       <tr>
                    <th colspan="2"><h4>Brand Order Settings</h4></th>
                </tr>
                <tr>
                    <td class="form-group">
                       <table class="product-types"> <tr >  <td> 
                                
                       <div class="checkbox-container">
                        <h4>Select Product Types:</h4>';
            foreach ($global_categories as $type) {
                $form .= '<label>
                                <input type="checkbox" name="product_types[]" value="' . esc_attr($type) . '" ' . (in_array($type, $selected_product_types) ? 'checked' : '') . '  />
                                ' . ucfirst($type) . '
                            </label>';
            }

            $form .= '</div> </td></tr></table>';
            $form .= ' <div style="width: 100%;" class="wrap tab brand-order-wrap"><nav class="nav-tab-wrapper"> ';

            $countNum = 1;

            foreach ($product_types as $type) {
                $brandName = $type . '-brand';
                $form .= '<a type="button" class="tablinks nav-tab ' . (($countNum == 1) ? 'active' : '') . '" name="product_types[]" onclick="openRetailTab(event, \'' . "brand-order-wrap" . '\',\'' . $brandName . '\')" value="' . esc_attr($type) . '" style="display: ' . (in_array($type, $selected_product_types) ? 'block' : 'none') . ';">
                                ' . ucfirst($type) . '
                            </a>';
                $countNum++;
            }
            $form .= '</nav>';

            $countNum = 1;
            foreach ($product_types as $type) {



                $form .= '<div id="' . $type . '-brand" class="tabcontent" style="display: ' . (($countNum == 1) && (in_array($type, $selected_product_types)) ?  'block' : 'none') . ';" ><div id="' . $type . '-fields" class="product-fields" style="display: ' . (in_array($type, $selected_product_types) ? 'block' : 'none') . ';">
                                <h3 style="margin-top: 0px;">' . ucfirst($type) . ' Brands</h3>';

                // Check if any brand order data exists for this type
                if (isset($saved_product_brand_order[$type]) && !empty($saved_product_brand_order[$type])) {
                    foreach ($saved_product_brand_order[$type] as $input_value) {
                        $form .= '<div class="input-group">
                                        <input type="text" name="product_' . $type . '_brand_order[]" value="' . esc_attr($input_value) . '" />
                                        <button type="button" onclick="deleteField(this)" class="button button-danger"><i class="dashicons dashicons-minus"></i>Delete</button>
                                    </div>';
                    }
                } else {
                    $form .= '<div class="input-group">
                                    <input type="text" name="product_' . $type . '_brand_order[]" value="" placeholder="Enter Brand Name" />
                                    <button type="button" onclick="deleteField(this)" class="button button-danger"><i class="dashicons dashicons-minus"></i> Delete</button>
                                </div>';
                }

                $form .= '<button type="button" onclick="addMoreFields(\'' . $type . '\')" class="button button-success"><i class="dashicons dashicons-plus-alt2"></i>Add More</button>
                            </div></div>';
                if (($countNum == 1) && (in_array($type, $selected_product_types))) {
                    $countNum++;
                }
            }


            $form .= '</div></td>
                </tr>
        
                        <tr>
                            <td class="form-group">
                                <button id="syncdata" name="save_brand_order" type="submit" class="button button-primary button-hero" >Save</button>
                            </td>
                        </tr>
                    </table>    
                </form>';

            // Retailer page main tab break and case
            break;
        case 'collection-order-settings':

            $collection_types = ['carpet', 'hardwood', 'laminate', 'lvt', 'tile'];
            $saved_product_collection_order = get_option('product_collection_order', []); // Get the nested array of collection orders
            $selected_collection_types = get_option('selected_collection_types', []); // Get selected collection types from options table

            $form .= '<form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                        <table class="form-table">
                           <tr>
                                <th colspan="2"><h4>Collection Order Settings</h4></th>
                            </tr>
                            <tr>
                                <td class="form-group">
                                    <table class="product-types"> <tr >  <td> 
                                                
                                    <div class="checkbox-container">
                                        <h4>Select Product Types:</h4>';

            foreach ($global_categories as $type) {
                $form .= '<label>
                                                        <input type="checkbox" name="collection_types[]" value="' . esc_attr($type) . '" ' . (in_array($type, $selected_collection_types) ? 'checked' : '') . '  />
                                                        ' . ucfirst($type) . '
                                                    </label>';
            }

            $form .= '</div> </td></tr></table>';
            $form .= ' <div style="width: 100%;" class="wrap tab collection-order-wrap"><nav class="nav-tab-wrapper"> ';

            $countNum = 1;

            foreach ($collection_types as $type) {
                $collectionName = $type . '-collection';
                $form .= '<a type="button" class="tablinks nav-tab ' . (($countNum == 1) ? 'active' : '') . '" name="product_types[]" onclick="openRetailTab(event, \'' . "collection-order-wrap" . '\',\'' . $collectionName . '\')" value="' . esc_attr($type) . '" style="display: ' . (in_array($type, $selected_collection_types) ? 'block' : 'none') . ';">
                                        ' . ucfirst($type) . '
                                    </a>';
                $countNum++;
            }
            $form .= '</nav>';

            $countNum = 1;
            foreach ($collection_types as $type) {
                $form .= '<div <div id="' . $type . '-collection" class="tabcontent" style="display: ' . (($countNum == 1) && (in_array($type, $selected_collection_types)) ? 'block' : 'none') . ';" ><div id="' . $type . '-collection-fields" class="collection-fields" style="display: ' . (in_array($type, $selected_collection_types) ? 'block' : 'none') . ';">
                                    <h3>' . ucfirst($type) . ' Collection</h3>';

                // Check if any brand order data exists for this type
                if (isset($saved_product_collection_order[$type]) && !empty($saved_product_collection_order[$type])) {
                    foreach ($saved_product_collection_order[$type] as $input_value) {
                        $form .= '<div class="input-group">
                                            <input type="text" name="collection_' . $type . '_order[]" value="' . esc_attr($input_value) . '" />
                                            <button type="button" onclick="deleteCollectionField(this)" class="button button-danger"><i class="dashicons dashicons-minus"></i>Delete</button>
                                        </div>';
                    }
                } else {
                    $form .= '<div class="input-group">
                                        <input type="text" name="collection_' . $type . '_order[]" value="" placeholder="Enter Collection Name" />
                                        <button type="button" onclick="deleteCollectionField(this)" class="button button-danger"><i class="dashicons dashicons-minus"></i>Delete</button>
                                    </div>';
                }

                $form .= '<button type="button" onclick="addMoreCollectionFields(\'' . $type . '\')" class="button button-success"><i class="dashicons dashicons-plus-alt2"></i>Add More</button>
                                </div></div>';



                if (($countNum == 1) && (in_array($type, $selected_collection_types))) {
                    $countNum++;
                }
            }


            $form .= '</div></td>
                    </tr>
            
                            <tr>
                                <td class="form-group">
                                    <button id="syncdata" name="save_collection_order" type="submit" class="button button-primary  button-hero" >Save</button>
                                </td>
                            </tr>
                        </table>    
                    </form>';

            // Retailer page main tab break and case
            break;
        case 'facets-for-plp':

            $productFacets = get_option('productFacets') ? unserialize(get_option('productFacets')) : array();
            // var_dump($productFacets);
            $selected_facets = array();
            foreach ($productFacets as $cat => $facet) {
                if (is_array($facet) && count($facet) > 0) {
                    foreach ($facet as $index => $facetvalue) {
                        $selected_facets[$cat][$facetvalue] = "checked=checked";
                    }
                }
            }
            // var_dump($selected_facets);
            $Categories  = array(
                'carpet',
                'hardwood',
                'laminate',
                'lvt',
                'tile',
                'area_rugs',
                'paint',
                'sheet'
            );
            $pdplayout = get_option('layoutopotion') ? get_option('layoutopotion') : 'default';
            if ($pdplayout == "alliance") {
                $facets = array(
                    'search' => "Search",
                    'brand_facet' => "Brand",
                    'collection_facet' => "Collection",
                    'color_facet' => "Color",
                    'style_facet' => "Style",
                    'species_facet' => "Species",
                    'construction_facet' => "Construction",
                    'installation_method' => "Installation Method",
                    'look_facet' => "Look",
                    'shade_facet' => "Shade",
                    // 'shape_facet' => "Shape",                    
                    'material_facet' => "Material",

                );
            } else {
                $facets = array(
                    'search' => "Search",
                    'brand_facet' => "Brand",
                    'sub_brand' => "Product Line",
                    'color_facet' => "Color",
                    'collection_facet' => "Collection",
                    'application_facet' => "Application",
                    'style_facet' => "Style",
                    'color_variation_facet' => "Color Variation",
                    'construction_facet' => "Construction",
                    'installation_method' => "Installation Method",
                    'location_facet' => "Location",
                    'species_facet' => "Species",
                    'design' => "Design",
                    'shade_facet' => "Shade",
                    'shape_facet' => "Shape",
                    'surface_texture_facet' => "Texture",
                    'look_facet' => "Look",
                    'fiber' => "Fiber",
                    'finish_facet' => "Finish",
                    'edge' => "Edge",
                    'material' => "Material",
                    'material_facet' => "Material Facet",
                    'backing_facet' => "Backing",
                    'waterproof' => "Waterproof",
                    'sample_available_facet' =>  "Sample Available",
                    'visualizable_facet' =>  "Visualizable",
                    'height' => "Height",
                    'width' => "Width",
                    'thickness_facet' => "Thickness",
                    'group' => "Designer Program"
                );
            }



            $form .= '<form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
            <table class="form-table">
                <tr>
                    <th colspan="2"><h4>Facets For PLP</h4></th>
                </tr><tr><td>';

            $form .= '<table class="form-table facets"> <tr class="form-table facets-tr"><th> Facet</th>';
            foreach ($Categories as $category) {
                $form .= '<th>' . ucfirst($category) . '</th>';
            }
            $form .= '</tr>';

            foreach ($facets as $facet_val => $facet_title) {

                $form .= '<tr><th width="150px" class="facetTitle">' . $facet_title . '</th>';
                foreach ($Categories as $category) {

                    $form .= '<td><div class="facet_input_wrap"><input type="checkbox" id="' . $category . '-' . $facet_val . '" name="productFacets[' . $category . '][]" value="' . $facet_val . '" id="' . $facet_val . '" ' . @$selected_facets[$category][$facet_val] . '>
                            </div></td>';
                }
                $form .= '</tr>';
            }
            $form .= '</table>';


            // $form .= '</table>';
            $form .= '</td></tr>';
            if ($pdplayout == "alliance") {
                $collectionFacets = get_option('collectionFacets') ? unserialize(get_option('collectionFacets')) : array();
                // var_dump($productFacets);
                $selected_collection_facets = array();
                foreach ($collectionFacets as $cat => $facet) {
                    if (is_array($facet) && count($facet) > 0) {
                        foreach ($facet as $index => $facetvalue) {
                            $selected_collection_facets[$cat][$facetvalue] = "checked=checked";
                        }
                    }
                }
                $form .= '<tr>
                    <th colspan="2"><h4>Facets For Collections Pages</h4></th>
                </tr><tr><td>';

                $form .= '<table class="form-table facets"> <tr class="form-table facets-tr"><th> Facet</th>';
                foreach ($Categories as $category) {
                    $form .= '<th>' . ucfirst($category) . '</th>';
                }
                $form .= '</tr>';

                foreach ($facets as $facet_val => $facet_title) {

                    $form .= '<tr><th width="150px" class="facetTitle">' . $facet_title . '</th>';
                    foreach ($Categories as $category) {

                        $form .= '<td><div class="facet_input_wrap"><input type="checkbox" id="' . $category . '-' . $facet_val . '" name="collectionFacets[' . $category . '][]" value="' . $facet_val . '" id="' . $facet_val . '" ' . @$selected_collection_facets[$category][$facet_val] . '>
                            </div></td>';
                    }
                    $form .= '</tr>';
                }
                $form .= '</table>';


                // $form .= '</table>';
                $form .= '</td></tr>';
            }
            $form .= '<tr>
                    
                    <td class="form-group">
                        <button id="syncdata" type="submit" name="save_facet" value="save_facet" class="button button-primary button-hero" >Save</button>
                    </td>
                </tr>
            </table>    
        </form>';

            // Retailer page main tab break and case        
            break;
        case 'retailer-info':


            $website_json =  json_decode(get_option('website_json'));
            $details = json_decode(get_option('social_links'));
            if ($website_json || $details) {
                $form .=  '<table class="widefat striped" border="1" style="border-collapse:collapse;">
                        <thead>
                            <tr>
                                <th width="25%"><strong>Name</strong></th>
                                <th width="75%"><strong>Values</strong></th>
                            </tr>
                        </thead>
                        <tbody><tr><th colspan="2"><strong>Social Platforms</strong></th></tr>';
                foreach ($details as $key => $value) {
                    $form .=  '<tr><td>' . ucfirst($value->platform) . '</td><td>' . $value->url . '</td></tr>';
                }
                $form .= '<tr><th colspan="2"><strong>Site information (' . ENV . ')</strong></th></tr>';
                $website =  $website_json;
                $website_info =  $website_json;

                foreach ($website_info as $key => $value) {

                    if (!is_array($value)) {

                        $form .= '<tr><td>' . ucfirst($key) . '</td><td> ' . ($value) . '</td></tr>';
                        if ($key == 'sfn') {
                            break;
                        }
                    }
                }

                for ($i = 0; $i < count($website->sites); $i++) {
                    if ($website->sites[$i]->instance == ENV) {
                        foreach ($website->sites[$i] as $key => $value) {
                            if ($key == 'chatmeter' && $value != "") {
                                $form .= '<tr><td> ' . ucfirst($key) . '</td><td>1</td></tr>';
                            } elseif ($key == 'chatmeter' && $value = "") {
                                $form .= '<tr><td> ' . ucfirst($key) . '</td><td>0</td></tr>';
                            } else {
                                $form .= '<tr><td> ' . ucfirst($key) . '</td><td> ' . ($value) . '</td></tr>';
                            }
                        }
                    }
                }
                $form .= '<tr><th colspan="2"><strong>Locations</strong></th></tr>';
                $website =  $website_json;
                for ($i = 0; $i < count($website->locations); $i++) {

                    $location_name = isset($website_json->locations[$i]->name) ? $website->locations[$i]->name : "";
                    $location_id = isset($website_json->locations[$i]->id) ? $website->locations[$i]->id : "";
                    $location = '';
                    //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
                    $location_address  = isset($website->locations[$i]->address) ? $website->locations[$i]->address : "";
                    $location_address .= isset($website->locations[$i]->city) ? " , " . $website->locations[$i]->city : "";
                    $location_address .= isset($website->locations[$i]->state) ? " , " . $website->locations[$i]->state : "";
                    $location_address .= isset($website->locations[$i]->postalCode) ? " , " . $website->locations[$i]->postalCode : "";



                    $location_phone = isset($website->locations[$i]->phone) ? $website->locations[$i]->phone : "";
                    $form .= '<tr><td>Name</td><td><b> ' . $location_name . '</b></td></tr>';
                    $form .= '<tr><td>ID</td><td><b> ' . $location_id . ' </b></td></tr>';
                    $form .= '<tr><td>Address</td><td> ' . $location_address . '</td></tr>';
                    $form .= '<tr><td>Phone</td><td> ' . $website->locations[$i]->phone . ' </td></tr>';
                    $form .= '<tr><td>Forwarding phone</td><td>' . $website->locations[$i]->forwardingPhone . '</td></tr>';
                    $form .= '<tr><td>License Number</td><td>' . $website->locations[$i]->licenseNumber . '</td></tr>';
                    $weekdays = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");

                    $openinghrs = '<ul style="display:block;">';
                    for ($j = 0; $j < count($weekdays); $j++) {
                        $location .= $website->locations[$i]->monday;
                        if (isset($website->locations[$i]->{$weekdays[$j]})) {
                            $openinghrs .= '<li>' . ucfirst($weekdays[$j]) . ' : <span>' . $website->locations[$i]->{$weekdays[$j]} . '</span></li>';
                        }
                    }
                    $openinghrs .=  '</ul>';
                    $form .=  '<tr><td>Opening Hrs</td><td> ' . $openinghrs . ' </td></tr>';
                }
                $contacts = json_decode(get_option('website_json'));
                $form .=  '<tr><th colspan="2"><strong>Contacts</strong></th></tr>';

                if (is_array($contacts->contacts)) {

                    for ($j = 0; $j < count($contacts->contacts); $j++) {
                        foreach ($contacts->contacts[$j] as $key => $value) {

                            if (!is_array($value)) {

                                $form .=  '<tr><td>' . @ucfirst($key) . '</td><td> ' . @$value . '</td></tr>';
                            }
                            // write_log($key); write_log('value'.$value);
                        }
                    }
                }


                $form .=  '</tbody>
                    </table>';
            }




            break;
        default:
            $form .= '<form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                        <table class="form-table">
                            <tr>
                                <th colspan="2"><h4>Enter Retailer Info</h4></th>
                            </tr>
                            ' . (isset($msg) ? '
                            <tr>
                                <td colspan="2">
                                <span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> ' . $msg . '</span>
                                    ' . (!isset($_POST['siteid']) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Site ID cannot be blank</span>' : null) . '
                                    ' . (!isset($_POST['clientcode']) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Client Code cannot be blank</span>' : null) . '
                                </td>
                            </tr>
                            ' : null) . '
                            <tr>
                                <td width="150px">
                                    <label for="siteid">Select environment <strong>*</strong></label>
                                </td>';
            $cde = (get_option('CDE_ENV')) && get_option('CDE_ENV') != "" ? get_option('CDE_ENV') : "";
            $lasttimesync = get_option('CDE_LAST_SYNC_TIME') ? date("F j, Y, g:i a", get_option('CDE_LAST_SYNC_TIME')) : "Not yet sync";
            if ($cde != "") {
                $form .= '<td class="form-group">
                                    <select name="instance-select">
                                        <option value="prod" ' . ($cde == "prod" ? 'selected=selected' : null) . '>Production</option>
                                        <option value="staging" ' . ($cde == "staging" ? 'selected=selected' : null) . '>Staging</option>
                                        <option value="dev"  ' . ($cde == "dev" ? 'selected=selected' : null) . '>Development</option>
                                    </select>
                                </td></tr>';
            } else {
                $form .= '<td class="form-group">
                                    <select name="instance-select" required>
                                        <option value="">Choose Environment</option>
                                        <option value="prod">Production</option>
                                        <option value="staging">Staging</option>
                                        <option value="dev">Development</option>
                                    </select>
                                </td></tr>';
            }


            $form .= '<tr>
                                <td width="150px">
                                    <label for="siteid">Client Code <strong>*</strong></label>
                                </td>
                                <td class="form-group">
                                    <input type="text" name="siteid" value="' . ((get_option('SITE_CODE')) ? get_option('SITE_CODE') : null) . '" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="clientcode">Client Username <strong>*</strong></label>
                                </td>
                                <td class="form-group">
                                    <input type="text" name="clientcode" value="' . (get_option('CLIENT_CODE') ? get_option('CLIENT_CODE') : null) . '" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="clientsecret">Client Secret <strong>*</strong></label>
                                </td>
                                <td class="form-group">
                                    <input type="text" name="clientsecret" value="' . (get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null) . '" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="form-group adp20">
                                    <button id="syncdata" type="submit" class="button button-primary button-hero" >Sync</button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:right">Last sync time : ' . $lasttimesync . '</td>
                            </tr>
                            
                        </table>    
                    </form>
        
                    <form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                    <table class="form-table">
                        <tr>
                            <th colspan="2"><h4>PDP Layout Settings</h4></th>
                        </tr>
                        <tr>
                            <td width="150px">
                                <label for="clientsecret">Product Detail Layout <strong>*</strong></label>
                            </td>
                            <td class="form-group">
                                <select name="layoutopotion" >
                                    <option value="0" ' . (get_option('layoutopotion') == 0 ? 'selected=selected' : null) . '>Default</option>
                                    <option value="alliance" ' . (get_option('layoutopotion') == 'alliance' ? 'selected=selected' : null) . '>Alliance Sites</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        <td width="150px">
                            <label for="clientsecret">Use your own theme templates <strong>*</strong></label>
                        </td>
                        <td class="form-group">                   
                            <select name="is_own_templates" >
                            <option value="0" ' . (get_option('is_own_templates') == 0 ? 'selected=selected' : null) . '>No</option>
                            <option value="1" ' . (get_option('is_own_templates') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">
                            <label for="clientsecret">Show "Get Financing Button" on PLP pages? <strong>*</strong></label>
                        </td>
                        <td class="form-group">                    
                            <select name="sh_get_finance" >
                            <option value="0" ' . (get_option('sh_get_finance') == 0 ? 'selected=selected' : null) . '>No</option>
                            <option value="1" ' . (get_option('sh_get_finance') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px">
                            <label for="clientsecret">Hide "Get Financing Button" on PDP pages?<strong>*</strong></label>
                        </td>
                        <td class="form-group">                    
                            <select name="pdp_get_finance" >
                            <option value="0" ' . (get_option('pdp_get_finance') == 0 ? 'selected=selected' : null) . '>No</option>
                            <option value="1" ' . (get_option('pdp_get_finance') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                        </select>
                        </td>
                    </tr>
        
                    <tr>
                    <td width="150px">
                        <label for="clientsecret">Do you want to change Get financing text with another? <strong>*</strong></label>
                    </td>
                    <td class="form-group">                   
                        <select name="getfinancereplace" id="getfinancereplace">
                        <option value="0" ' . (get_option('getfinancereplace') == 0 ? 'selected=selected' : null) . '>No</option>
                        <option value="1" ' . (get_option('getfinancereplace') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                    </select>
                    </td>
                    </tr>
        
                    <tr class="row_fin" ' . (get_option('getfinancereplace') == 1 ? null : 'style="display:none"') . '>
                    <td width="150px">
                        <label for="clientsecret">Button Text for replace(Get financing) on PLP and PDP pages? <strong>*</strong></label>
                    </td>
                    <td class="form-group">                    
                        <input type="text" name="getfinancetext" value="' . (get_option('getfinancetext') ? get_option('getfinancetext') : null) . '" />
                    </td>
                    </tr>
        
                    <tr class="row_fin" ' . (get_option('getfinancereplace') == 1 ? null : 'style="display:none"') . '>
                    <td width="150px">
                        <label for="clientsecret">Button url for replace(Get Financing) on PLP and PDP pages? <strong>*</strong></label>
                    </td>
                    <td class="form-group">                    
                        <input type="text" name="getfinancereplaceurl" value="' . (get_option('getfinancereplaceurl') ? get_option('getfinancereplaceurl') : null) . '" />
                    </td>
                    </tr>
        
                    <tr>
                            <td></td>
                            <td class="form-group adp20">
                                <button id="syncdata" type="submit" class="button button-primary button-hero" >Save</button>
                            </td>
                        </tr>
                    </table>    
                    </form>
        
                    <form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                    <table class="form-table">
                    <tr>
                        <th colspan="2"><h4>GET COUPON Button Setting</h4></th>
                    </tr>
                    <tr>
                        <td width="150px">
                            <label for="clientsecret">Show Get Coupon button on PLP and PDP pages <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <select name="getcouponbtn" disabled>
                                <option value="1" ' . (get_option('getcouponbtn') == 1 ? 'selected=selected' : null) . '>Yes</option>
                                <option value="0" ' . (get_option('getcouponbtn') == 0 ? 'selected=selected' : null) . '>No</option>                            
                            </select>
                        </td>
                    </tr>
        
                    <tr>
                    <td width="150px">
                        <label for="clientsecret">Show Custom Button on PLP and PDP pages <strong>*</strong></label>
                    </td>
                    <td class="form-group">                   
                        <select name="getcouponreplace" id="getcouponreplace">
                        <option value="0" ' . (get_option('getcouponreplace') == 0 ? 'selected=selected' : null) . '>No</option>
                        <option value="1" ' . (get_option('getcouponreplace') == 1 ? 'selected=selected' : null) . '>Yes</option>                    
                    </select>
                    </td>
                    </tr>
        
                    <tr class="row_dim" ' . (get_option('getcouponreplace') == 1 ? null : 'style="display:none"') . '>
                    <td width="150px">
                        <label for="clientsecret">Custom Button Text <strong>*</strong></label>
                    </td>
                    <td class="form-group">                    
                        <input type="text" name="getcouponreplacetext" value="' . (get_option('getcouponreplacetext') ? get_option('getcouponreplacetext') : null) . '" />
                    </td>
                    </tr>
        
                    <tr class="row_dim" ' . (get_option('getcouponreplace') == 1 ? null : 'style="display:none"') . '>
                    <td width="150px">
                        <label for="clientsecret">Custom Button URL <strong>*</strong></label>
                    </td>
                    <td class="form-group">                    
                        <input type="text" name="getcouponreplaceurl" value="' . (get_option('getcouponreplaceurl') ? get_option('getcouponreplaceurl') : null) . '" />
                    </td>
                    </tr>
        
        
                    <tr>
                        <td></td>
                        <td class="form-group adp20">
                            <button id="syncdata" type="submit" class="button button-primary button-hero" >Save</button>
                        </td>
                    </tr>
                    </table>    
        
                    </form>
        
                    <form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                    <table class="form-table">
                        <tr>
                            <th colspan="2"><h4>PLP Layout Settings</h4></th>
                        </tr>
                            <tr>
                                <td width="150px">
                                    <label>PLP Layout <strong>*</strong></label>
                                </td>
                                <td class="form-group">
                                    <select name="plplayout" >
                                        <option value="0" ' . (get_option('plplayout') != 1 ? 'selected=selected' : null) . '>Clean Version</option>
                                        <option value="1" ' . (get_option('plplayout') == 1 ? 'selected=selected' : null) . '>Card Version - Popup</option>
                                    </select>
                                </td>
                        </tr>
                        <tr>
                            <td width="150px">
                                <label>Use product images<strong>*</strong></label>
                            </td>
                            <td class="form-group">                   
                                <select name="plpproductimg" >
                                    <option value="0" ' . (get_option('plpproductimg') != 0 ? 'selected=selected' : null) . '>Swatch Images</option>
                                    <option value="1" ' . (get_option('plpproductimg') == 1 ? 'selected=selected' : null) . '>Gallery Images</option>                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="form-group adp20">
                                <button id="syncdata" type="submit" class="button button-primary button-hero" >Save</button>
                            </td>
                        </tr>
                    </table>    
                    </form>
        
        
                    <form name="pluginname" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">
                    <table class="form-table">
                        <tr>
                            <td colspan="2"><h4>Blog Post Sync Settings</h4></td>
                            <td class="form-group">
                                <button id="syncdata" type="submit" class="button button-primary button-hero" >Sync Blog Post</button>
                            </td>
                        </tr>
                    <input type="hidden" name="blogsync" value="yes">
                        <tr>
                            
                            
                        </tr>
                    </table>    
                    </form> ';
            break;
    endswitch;
    ?>



<?php

    $form .= '</div></div></div>';
    echo $form;
    unset($_SESSION["error"]);
    unset($_SESSION["error_desc"]);
}
//functions for facet checkbox plp/pdp

function save_facet_settings()
{
    if (isset($_POST['save_facet'])) {
        $str = serialize($_POST['productFacets']);
        get_option('productFacets') || get_option('productFacets') == "" ? update_option('productFacets', $str) : add_option('productFacets', $str);
    }
}
add_action('admin_init', 'save_facet_settings');




function save_area_rug_settings()
{
    // Handle form submission
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['save_facet_area_rug'])) {
        // Save selected carpeting excerpts
        if (isset($_POST['carpeting_excerpts']) && is_array($_POST['carpeting_excerpts'])) {
            $selected_excerpts = array_map('sanitize_text_field', $_POST['carpeting_excerpts']);
            update_option('Area_rugs_brands', $selected_excerpts);
        } else {
            update_option('Area_rugs_brands', []); // Save empty array if no checkbox is selected
        }

        // Add success message
        echo '<div class="updated notice"><p>Brands saved successfully.</p></div>';
    }
}
add_action('admin_init', 'save_area_rug_settings');



function Brand_order_settings()
{

    // Handle form submission and save options
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['save_brand_order'])) {
        $product_types = ['carpet', 'hardwood', 'laminate', 'lvt', 'tile'];

        // Save checkbox selections (selected product types)
        $selected_product_types = isset($_POST['product_types']) ? $_POST['product_types'] : [];
        update_option('selected_product_types', $selected_product_types);

        // Initialize an array to hold all product brand inputs
        $product_brand_order = [];

        // Save brand input fields for each selected product type
        foreach ($product_types as $type) {
            if (in_array($type, $selected_product_types)) {
                $inputs = isset($_POST["product_{$type}_brand_order"]) ? array_filter(array_map('sanitize_text_field', $_POST["product_{$type}_brand_order"])) : [];
                if (!empty($inputs)) {
                    $product_brand_order[$type] = $inputs; // Store inputs for each product type
                }
            }
        }

        // Save all product brand orders as a nested array
        update_option('product_brand_order', $product_brand_order);

        // Redirect to avoid form resubmission after refresh
        wp_redirect($_SERVER['REQUEST_URI']);
        exit;
    }
}
add_action('admin_init', 'Brand_order_settings');



function Collection_order_settings()
{

    // Handle form submission and save options
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['save_collection_order'])) {
        $collection_types = ['carpet', 'hardwood', 'laminate', 'lvt', 'tile'];

        // Save checkbox selections (selected collection types)
        $selected_collection_types = isset($_POST['collection_types']) ? $_POST['collection_types'] : [];
        update_option('selected_collection_types', $selected_collection_types);

        // Initialize an array to hold all collection inputs
        $product_collection_order = [];

        // Save collection input fields for each selected collection type
        foreach ($collection_types as $type) {
            if (in_array($type, $selected_collection_types)) {
                $inputs = isset($_POST["collection_{$type}_order"]) ? array_filter(array_map('sanitize_text_field', $_POST["collection_{$type}_order"])) : [];
                if (!empty($inputs)) {
                    $product_collection_order[$type] = $inputs; // Store inputs for each collection type
                }
            }
        }

        // Save all collection orders as a nested array
        update_option('product_collection_order', $product_collection_order);

        // Redirect to avoid form resubmission after refresh
        wp_redirect($_SERVER['REQUEST_URI']);
        exit;
    }
}
add_action('admin_init', 'Collection_order_settings');



function getRetailerInformation()
{
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			' . get_option('retailer_details') . '
                    
</div>
	';
}
add_shortcode('getRetailerInformation', 'getRetailerInformation');

function my_menu_page()
{
?>
    <?php
    if (isset($_GET['tab'])) {
        $active_tab = $_GET['tab'];
    } else {
        $active_tab = 'tab_one';
    }
    ?>
    <div class="wrap" id="grandchild-backend">
        <h2>Retailer Settings <?php echo get_option('timezone_string'); ?></h2>
        <div class="description"></div>

        <?php Calling_API_form('', ''); ?>
        <?php settings_errors(); ?>

    </div>
<?php
}



function retailer_product_data_html()
{

?>
    <div id="wpcontent1" class="client_info_wrap syncdata-status">


        <table class="form-table table-bordered table-hover widefat striped ">
            <tbody>
                <tr>
                    <th colspan="5" style="vertical-align: middle !important;">
                        <h2>Retailer Flooring Data</h2>
                    </th>



                    <th colspan="5" style="text-align: right !important;">
                        <input type="hidden" name="reset_key" value="reset_value">
                        <a class="button button-primary  button-hero" href="/wp-admin/admin.php?page=retailer_product_data&reset_key=reset_value">Reset Product Object</a>
                    </th>

                </tr>

                <tr>
                    <th class="form-group" style="width: 10px;">#</th>
                    <th class="form-group">Product Type</th>
                    <th class="form-group" colspan="5">Brand</th>
                    <th class="form-group">Last Sync</th>
                    <th class="form-group">Last Sync Date/Time</th>


                </tr>

                <?php

                global $wpdb;
                $product_sync_table = $wpdb->prefix . "sfn_sync";
                $quer_status =  "SELECT * FROM {$product_sync_table} ORDER BY product_category ASC";
                $resultbrand = $wpdb->get_results($quer_status);
                $i = 1;
                foreach ($resultbrand as $synclist) {

                ?>
                    <tr class="hover-table">

                        <td class="form-group"><?php echo $i; ?></td>
                        <td class="form-group"><?php echo $synclist->product_category; ?></td>
                        <td class="form-group" colspan="5"><?php echo $synclist->product_brand; ?></td>
                        <td class="form-group"><?php echo $synclist->sync_status; ?></td>
                        <td class="form-group"><?php echo $synclist->syn_datetime; ?></td>


                    </tr>
                <?php

                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="syncwrap">
        <table class="form-table table-bordered table-hover widefat striped" style="table-layout: fixed;">
            <tbody>
                <tr>

                    <th colspan="5">
                        <h2>Sync All Products</h2>
                    </th>

                </tr>
                <tr>
                    <form id="myForm" action="" method="POST">
                        <input type="hidden" id="anchorId" name="sync_action1" value="sync_products">
                        <!-- <tr>
                    <td class="form-group"><button class="product-sync-button button button-primary" data-product-type="carpet">Sync Carpet Products</button></td>
                </tr> -->
                <tr>
                    <td><button class="product-sync-button button button-primary  button-hero" data-product-type="carpet">Sync Carpet Products</button></td>
                    <td><button class="product-sync-button button button-primary  button-hero" data-product-type="hardwood">Sync Hardwood Products</button></td>
                    <td><button class="product-sync-button button button-primary  button-hero" data-product-type="laminate">Sync Laminate Products</button></td>
                    <td><button class="product-sync-button button button-primary  button-hero" data-product-type="lvt">Sync LVT Products</button></td>
                    <td><button class="product-sync-button button button-primary  button-hero" data-product-type="tile">Sync Tile Products</button></td>
                </tr>
                <tr>

                </tr>
                <tr>

                </tr>
                <tr>

                </tr>
                </form>
                </tr>
            </tbody>
        </table>
    </div>
<?php

}

//reset button code 
if (isset($_GET['reset_key']) && $_GET['reset_key'] === 'reset_value') {
    reset_product_data("all");
}




/**  Product Sync Mannual Trigger function **/

if (isset($_POST['api_product_data_category']) && isset($_POST['api_product_data_brand'])) {

    new Example_Background_Processing();

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    if (! is_dir($upload_dir)) {
        mkdir($upload_dir, 0700);
    }

    $sfnapi_category_array = array_keys($brandmapping, $_POST['api_product_data_category']);
    $sfnapi_category = $sfnapi_category_array[0];

    $permfile = $upload_dir . '/' . $_POST['api_product_data_category'] . '_' . $_POST['api_product_data_brand'] . '.csv';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/' . $sfnapi_category . '/' . $_POST['api_product_data_brand'] . '.csv?' . SFN_STATUS_PARAMETER;

    $tmpfile = download_url($res, $timeout = 900);

    //var_dump($tmpfile);
    if (is_file($tmpfile)) {

        copy($tmpfile, $permfile);
        unlink($tmpfile);
        require_once plugin_dir_path(__FILE__) . 'example-plugin.php';
        $data_url =   admin_url('/admin.php?page=retailer_product_data&process=allcsv&main_category=' . $_POST['api_product_data_category'] . '&product_brand=' . $_POST['api_product_data_brand'] . '');
        //  write_log($data_url);

    } else {

        $_SESSION['error'] = "Couldn't find products for this category";
    }


?>
    <script>
        window.location.href = "<?php echo $data_url; ?>";
    </script>
<?php exit();
    //  write_log($_GET);
}
// }

if (isset($_GET['copy']) && $_GET['copy'] == 1) {

    echo get_template_directory_uri() . "-child";
    var_dump(xcopy("/var/www/html/wp-content/plugins/mm-retailer-plugin/fl-builder", "/var/www/html/wp-content/themes/bb-theme-child/fl-builder"));
    exit;
}

/**
 * Copy a file, or recursively copy a folder and its contents
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @param       int      $permissions New folder creation permissions
 * @return      bool     Returns true on success, false on failure
 */
function xcopy($source, $dest, $permissions = 0755)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest, $permissions);
    }

    // Loop through the folder
    $dir = dir($source);

    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        xcopy("$source/$entry", "$dest/$entry", $permissions);
    }

    // Clean up
    $dir->close();
    return true;
}


function compare_some_objects($a, $b)
{
    return $b->order - $a->order;
}

function compare_cde_some_objects($a, $b)
{
    return $a->slider->order - $b->slider->order;
}

function compare_sale_some_objects($a, $b)
{
    return $a['days'] - $b['days'];
}


/**  Array Filter function **/

function getArrayFiltered($aFilterKey, $aFilterValue, $array)
{
    $filtered_array = array();
    foreach ($array as $value) {
        if (isset($value->$aFilterKey)) {
            if ($value->$aFilterKey == $aFilterValue) {
                $filtered_array[] = $value;
            }
        }
    }

    return $filtered_array;
}




/**  Move Page Template files to child theme directory **/

function file_replace()
{

    $plugin_dir_land_fl = plugin_dir_path(__FILE__) . 'fl-builder/modules/content-slider/includes/frontend.php';
    $theme_dir_land_fl = get_stylesheet_directory() . '/fl-builder/modules/content-slider/includes/frontend.php';

    if (!copy($plugin_dir_land_fl, $theme_dir_land_fl)) {
        echo "failed to copy $plugin_dir_land_fl to $theme_dir_land_fl...\n";
    }

    //Accessibility Page
    $plugin_dir_accessibility = plugin_dir_path(__FILE__) . 'product-listing-templates/accessibility.php';
    $theme_dir_accessibility = get_stylesheet_directory() . '/accessibility.php';

    if (!copy($plugin_dir_accessibility, $theme_dir_accessibility)) {
        echo "failed to copy $plugin_dir_accessibility to $theme_dir_accessibility...\n";
    }

    //search Page
    $plugin_dir_search = plugin_dir_path(__FILE__) . 'product-listing-templates/search.php';
    $theme_dir_search = get_stylesheet_directory() . '/search.php';

    if (!copy($plugin_dir_search, $theme_dir_search)) {
        echo "failed to copy $plugin_dir_search to $theme_dir_search...\n";
    }

    //coretec Page
    $plugin_dir_coretec = plugin_dir_path(__FILE__) . 'product-listing-templates/coretec_no_colorwall.php';
    $theme_dir_coretec = get_stylesheet_directory() . '/coretec_no_colorwall.php';

    if (!copy($plugin_dir_coretec, $theme_dir_coretec)) {
        echo "failed to copy $plugin_dir_coretec to $theme_dir_coretec...\n";
    }

    //Floorte
    $plugin_dir_floorte = plugin_dir_path(__FILE__) . 'product-listing-templates/floorte_hardwood.php';
    $theme_dir_floorte = get_stylesheet_directory() . '/floorte_hardwood.php';

    if (!copy($plugin_dir_floorte, $theme_dir_floorte)) {
        echo "failed to copy $plugin_dir_floorte to $theme_dir_floorte...\n";
    }
}

add_action('wp_loaded', 'file_replace');


add_action('sfn_bb_autoimport_hook', 'autoimport_caller');
function autoimport_caller()
{
    autoimport();
    accessibility_autoimport();
    add_accessibility_page();
}

/**  Auto Import Function for Coretec Colorwall /  Coretec Template **/

function autoimport()
{

    // get the file
    require_once plugin_dir_path(__FILE__) . '/autoimport/autoimporter.php';

    if (! class_exists('Auto_Importer')) {
        die('Auto_Importer not found');
    }
    // call the function
    $args = array(
        'file'        => plugin_dir_path(__FILE__) . '/autoimport/coretec.xml'
    );


    if (get_page_by_title('Template Coretec 2025', OBJECT, 'fl-builder-template') == '') {

        auto_import($args);
    }

    $arg = array(
        'file'        => plugin_dir_path(__FILE__) . '/autoimport/floorte.xml'
    );


    if (get_page_by_title('Template Floorte 2025', OBJECT, 'fl-builder-template') == '') {

        auto_import($arg);
    }
}


//Function to add Meta Tags in Header for select in iphone
function add_meta_tags()
{
    echo '<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">';
}
add_action('wp_head', 'add_meta_tags');
//Function to add Meta Tags in Header without Plugin

function sync_websiteinfo_cde($apiObj, $result)
{

    //API Call for getting website INFO

    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $headers = array('authorization' => "bearer " . $result['access_token']);
    $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);

    $products = $apiObj->call(SOURCEURL . get_option('SITE_CODE') . "/" . PRODUCTURL, "GET", $inputs, $headers);


    if (isset($products)) {

        $product_json = json_encode($products);
        update_option('product_json', $product_json);
    } else {
        $msg = $contacts['message'];
        $_SESSION['error'] = "Product Brand API";
        $_SESSION["error_desc"] = $msg;
    }

    //facebook domain verification

    $socialreport = $apiObj->call(BASEURL . get_option('SITE_CODE') . "/" . SOCIALURLREPORT, "GET", $inputs, $headers);

    if (isset($socialreport['success']) && $socialreport['success'] == 1) {
        $socialreport_json =  json_encode($socialreport['result']);
        get_option('social_report') || get_option('social_report') == "" ? update_option('social_report', $socialreport_json) : add_option('social_report', $socialreport_json);
    }

    $sociallinks = $apiObj->call(BASEURL . get_option('SITE_CODE') . "/" . SOCIALURL, "GET", $inputs, $headers);

    if (isset($sociallinks['success']) && $sociallinks['success'] == 1) {
        $social_json =  json_encode($sociallinks['result']);
        get_option('social_links') || get_option('social_links') == "" ? update_option('social_links', $social_json) : add_option('social_links', $social_json);
    } else {
        $msg = $sociallinks['message'];
        $_SESSION['error'] = "Error SOCIAL LINKS";
        $_SESSION["error_desc"] = $msg;
        update_option('social_links', '');
        //echo $msg;

    }


    if (isset($website['success']) && $website['success']) {
        $sfn_acc = $website['result']['sfn'];
        $coretec_color = $website['result']['options']['colorWall'];
        $covid = $website['result']['options']['covid'];
        $website_json = json_encode($website['result']);
        update_option('website_json', $website_json);
        update_option('sfn_account', $sfn_acc);
        update_option('coretec_colorwall', $coretec_color);

        if (isset($website['result']['sites'])) {
            for ($k = 0; $k < count($website['result']['sites']); $k++) {
                if ($website['result']['sites'][$k]['instance'] == ENV) {
                    update_option('blogname', $website['result']['sites'][$k]['name']);
                    $gtmid = $website['result']['sites'][$k]['gtmId'];
                    get_option('gtm_script_insert') ? update_option('gtm_script_insert', $gtmid) : update_option('gtm_script_insert', $gtmid);
                }
            }
        }
        //get_option( 'gtm_script_insert', )

    } else {
        $msg = $website['message'];
        //echo $msg;
    }
}

///remove multidimensional array element by value 
function removeElementWithValue($array, $key, $value)
{
    foreach ($array as $subKey => $subArray) {
        if ($subArray[$key] == $value) {
            unset($array[$subKey]);
        }
    }
    return $array;
}


//merge all carpet brand files into single carpet file
function merge_carpet_brands_files_into_carpet()
{

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $sfn_data_folder = $upload_dir . '/sfn-data';
    $product_json =  json_decode(get_option('product_json'));
    $carpet_array = getArrayFiltered('productType', 'carpet', $product_json);
    write_log($carpet_array);
    $merged_data = [];
    $json_files = glob($sfn_data_folder . '/carpeting*.json');

    write_log($json_files);

    foreach ($carpet_array as $carpet) {

        $carpet_file = $sfn_data_folder . '/carpeting_' . $carpet->manufacturer . '.json';

        if (in_array($carpet_file, $json_files)) {


            $json_content = file_get_contents($carpet_file);
            $decoded_data = json_decode($json_content, true);
            if (is_array($decoded_data)) {
                $merged_data = array_merge_recursive($merged_data, $decoded_data);
            }
        }
    }

    $carpet_file_path = $sfn_data_folder . '/carpet.json';

    $write_result = file_put_contents($carpet_file_path, json_encode($merged_data, JSON_PRETTY_PRINT));
    if ($write_result === false) {
        echo "failed to create carpet file";
    } else {
        echo "file created successfully!!" . $carpet_file_path;
    }
}


//merge all carpet brand files into single area rugs file
function merge_carpet_brands_files_into_arearugs()
{

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $sfn_data_folder = $upload_dir . '/sfn-data';

    $merged_data = [];
    $json_files = glob($sfn_data_folder . '/area*.json');

    if ($json_files) {
        foreach ($json_files as $file) {

            $json_content = file_get_contents($file);
            $decoded_data = json_decode($json_content, true);
            if (is_array($decoded_data)) {
                $merged_data = array_merge_recursive($merged_data, $decoded_data);
            }

            // write_log($merged_data);
            // exit();
        }
    }
    // Save the merged data to carpet.json
    $rugs_file_path = $sfn_data_folder . '/area_rugs.json';

    $write_result = file_put_contents($rugs_file_path, json_encode($merged_data, JSON_PRETTY_PRINT));
    if ($write_result === false) {
        echo "failed to create carpet file";
    } else {
        echo "file created successfully!!" . $rugs_file_path;
    }
}


/** Carpet Mohawk Product sync Cron job **/

add_action('sync_carpet_monday_event_mohawk', 'do_this_monday_carpet_mohawk', 10, 2);

function do_this_monday_carpet_mohawk()
{
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    $product_sync_table = $wpdb->prefix . "sfn_sync";

    write_log('Carpet batch mohawk sync started');

    $permfile = $upload_dir . '/carpeting_Mohawk.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/carpet/Mohawk.json' . SFN_STATUS_PARAMETER;


    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_brand = 'mohawk' and  product_category = 'carpeting'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'carpeting', 'product_brand' => 'mohawk', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }

    $strJsonFileContents = file_get_contents($permfile);

    $handle = json_decode($strJsonFileContents, true);

    if (!empty($handle)) {

        $obj = new Example_Background_Processing();
        set_time_limit(0);
        $obj->handle_all('carpeting', 'mohawk', $handle);
    }

    write_log('Sync Completed for mohawk Carpet brands');
    merge_carpet_brands_files_into_carpet();
    reset_product_data('carpet');
}


/** Carpet Shaw Product sync Cron job **/

add_action('sync_carpet_tuesday_event_shaw', 'do_this_tuesday_carpet_shaw', 10, 2);

function do_this_tuesday_carpet_shaw()
{
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    $table_posts = $wpdb->prefix . 'posts';
    $product_sync_table = $wpdb->prefix . "sfn_sync";

    write_log('Carpet batch shaw sync started');
    $permfile = $upload_dir . '/carpeting_Shaw.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/carpet/Shaw.json?' . SFN_STATUS_PARAMETER;

    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_brand = 'shaw' and  product_category = 'carpeting'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'carpeting', 'product_brand' => 'shaw', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }

    $strJsonFileContents = file_get_contents($permfile);

    $handle = json_decode($strJsonFileContents, true);


    if (!empty($handle)) {

        $obj = new Example_Background_Processing();
        set_time_limit(0);
        $obj->handle_all('carpeting', 'shaw', $handle);
        write_log('Carpet Shaw batch sync ended');
    }

    write_log('Sync Completed for shaw Carpet brands');
    merge_carpet_brands_files_into_carpet();
    reset_product_data('carpet');
}


/** Carpet Dreamweaver Product sync Cron job **/

add_action('sync_carpet_saturday_event_dreamweaver', 'do_this_saturday_carpet_dreamweaver', 10, 2);

function do_this_saturday_carpet_dreamweaver()
{
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    $product_json =  json_decode(get_option('product_json'));
    $c_array = getArrayFiltered('productType', 'carpet', $product_json);

    $carpet_array = json_decode(json_encode($c_array), True);

    $carpet_array = removeElementWithValue($carpet_array, "manufacturer", 'Mohawk');
    $carpet_array = removeElementWithValue($carpet_array, "manufacturer", 'Shaw');


    foreach ($carpet_array as $carpet) {
        if ($carpet['manufacturer'] == 'dixiehome') {

            $carpet_manufacturer = 'Dixie Home';
        } elseif ($carpet['manufacturer'] == 'dreamweaver') {

            $carpet_manufacturer = 'Dream Weaver';
        } else {

            $carpet_manufacturer = ucfirst($carpet['manufacturer']);
        }

        $permfile = $upload_dir . '/carpeting_' . $carpet['manufacturer'] . '.json';
        $res = SOURCEURL . get_option('SITE_CODE') . '/www/carpet/' . $carpet['manufacturer'] . '.json?' . SFN_STATUS_PARAMETER;

        $tmpfile = download_url($res, $timeout = 900);

        merge_carpet_brands_files_into_carpet();

        if (is_wp_error($tmpfile)) {

            $status_sync = 'Failed';

            $wpdb->query("DELETE  FROM $product_sync_table WHERE product_brand = " . $carpet['manufacturer'] . " and  product_category = 'carpeting'");

            $wpdb->insert(@$product_sync_table, array('product_category' => 'carpeting', 'product_brand' => $carpet['manufacturer'], 'sync_status' => $status_sync), array('%s', '%s', '%s'));

            exit();
        } else {
            if (is_file($tmpfile)) {
                copy($tmpfile, $permfile);
                unlink($tmpfile);
            }
        }

        Write_log('file downloaded path -' . $permfile);

        $strJsonFileContents = file_get_contents($permfile);

        $handle = json_decode($strJsonFileContents, true);

        if (!empty($handle)) {

            $obj = new Example_Background_Processing();
            set_time_limit(0);
            $obj->handle_all('carpeting', $carpet['manufacturer'], $handle);
            write_log('Carpet ' . $carpet['manufacturer'] . ' batch sync ended');
        }
    }

  //  merge_carpet_brands_files_into_carpet();
    reset_product_data('carpet');
    write_log('Sync Completed for all other Carpet brands');
}


/** Hardwood Product sync Cron job **/

add_action('sync_hardwood_wednesday_event', 'do_this_wednesday_hardwood', 10, 2);

function do_this_wednesday_hardwood()
{

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    $table_posts = $wpdb->prefix . 'posts';

    $permfile = $upload_dir . '/hardwood' . '.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/hardwood' . '.json?' . SFN_STATUS_PARAMETER;
    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_category = 'hardwood_catalog'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'hardwood_catalog', 'product_brand' => 'Hardwood Brand', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }


    $obj = new Example_Background_Processing();
    $obj->handle_all('hardwood_catalog', 'hardwood');
    reset_product_data('hardwood');

    write_log('Sync Completed for all other hardwood brands');
}

/** Laminate Product sync Cron job **/

add_action('sync_laminate_thursday_event', 'do_this_thursday_laminate', 10, 2);

function do_this_thursday_laminate()
{

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';
    $table_posts = $wpdb->prefix . 'posts';

    $permfile = $upload_dir . '/laminate' . '.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/laminate' . '.json?' . SFN_STATUS_PARAMETER;
    write_log($res);
    $tmpfile = download_url($res, $timeout = 900);
    write_log($tmpfile);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_category = 'laminate_catalog'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'laminate_catalog', 'product_brand' => 'Laminate Brand', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }

    $obj = new Example_Background_Processing();
    $obj->handle_all('laminate_catalog', 'laminate');
    reset_product_data('laminate');

    write_log('Sync Completed for all other laminate brands');
}


/** LVT Product sync Cron job **/


add_action('sync_lvt_thursday_event', 'do_this_thursday_lvt', 10, 2);

function do_this_thursday_lvt()
{

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    $table_posts = $wpdb->prefix . 'posts';

    $permfile = $upload_dir . '/lvt' . '.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/lvt' . '.json?' . SFN_STATUS_PARAMETER;

    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_category = 'luxury_vinyl_tile'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'luxury_vinyl_tile', 'product_brand' => 'LVT Brand', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }

    $obj = new Example_Background_Processing();
    $obj->handle_all('luxury_vinyl_tile', 'lvt');
    reset_product_data('lvt');

    write_log('Sync Completed for all other lvt brands');
}

/** Tile Product sync Cron job **/

add_action('sync_tile_friday_event', 'do_this_friday_tile', 10, 2);

function do_this_friday_tile()
{

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    $table_posts = $wpdb->prefix . 'posts';

    $permfile = $upload_dir . '/tile' . '.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/tile' . '.json?' . SFN_STATUS_PARAMETER;

    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_category = 'tile_catalog'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'tile_catalog', 'product_brand' => 'Tile Brand', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }


    $obj = new Example_Background_Processing();
    $obj->handle_all('tile_catalog', 'tile');
    reset_product_data('tile');

    write_log('Sync Completed for all other tile brands');
}

/** Paint Product sync Cron job **/

add_action('sync_paint_friday_event', 'do_this_friday_paint', 10, 2);

function do_this_friday_paint()
{
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $paint_dir = $upload_dir . '/paint-data';
    $upload_dir = $upload_dir . '/sfn-data';
    $table_posts = $wpdb->prefix . 'posts';

    $permfile = $upload_dir . '/paint' . '.json';
    // $res = $paint_dir . '/paint' . '.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/paint.json?' . SFN_STATUS_PARAMETER;
    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_category = 'paint_catalog'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'paint_catalog', 'product_brand' => 'Paint Brand', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }

    $obj = new Example_Background_Processing();
    $obj->handle_all('paint_catalog', 'paint');
    reset_product_data('paint');

    write_log('Sync Completed for all other paint brands');
}


// /** Area rugs Product sync Cron job **/

add_action('admin_init', 'arearugs_settings');
function arearugs_settings()
{

    if (get_option('arearugsync') == '1' &&  get_option('arearugs_brand') != '') {

        if (!wp_next_scheduled('sync_area_rugs_friday_event')) {

            wp_schedule_event(time() + 2700, 'daily', 'sync_area_rugs_friday_event');
        }
    } else {

        wp_clear_scheduled_hook('sync_area_rugs_friday_event');
    }
}

add_action('sync_area_rugs_friday_event', 'do_this_friday_area_rugs', 10, 2);

function do_this_friday_area_rugs()
{
    //write_log("inside rugs");
    $rugs_array =  maybe_unserialize(get_option('arearugs_brand'));
    // write_log($rugs_array);

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';


    foreach ($rugs_array as $rugs) {
        $permfile = $upload_dir . '/area_rugs_' . $rugs . '.json';
        $res = SOURCEURL . get_option('SITE_CODE') . '/www/rugs/' . $rugs . '.json?' . SFN_STATUS_PARAMETER;

        $tmpfile = download_url($res, $timeout = 900);

        if (is_wp_error($tmpfile)) {

            $status_sync = 'Failed';

            $wpdb->query("DELETE  FROM $product_sync_table WHERE product_brand = " . $rugs . " and  product_category = 'area_rugs'");

            $wpdb->insert(@$product_sync_table, array('product_category' => 'area_rugs', 'product_brand' => $rugs, 'sync_status' => $status_sync), array('%s', '%s', '%s'));

            exit();
        } else {
            if (is_file($tmpfile)) {
                copy($tmpfile, $permfile);
                unlink($tmpfile);
            }
        }

        // Write_log('file downloaded path -' . $permfile);

        $strJsonFileContents = file_get_contents($permfile);

        $handle = json_decode($strJsonFileContents, true);

        if (!empty($handle)) {

            $obj = new Example_Background_Processing();
            set_time_limit(0);
            $obj->handle_all('area_rugs', $rugs, $handle);
            // write_log('Rugs ' . $rugs . ' batch sync ended');
        }


        //  write_log('auto_sync - sync_area_rugs_friday_event-' . $rugs);

        // $obj = new Example_Background_Processing();
        //  $obj->handle_all('area_rugs', $rugs);

        //  write_log('Sync Completed - ' . $rugs);
    }

    merge_carpet_brands_files_into_arearugs();
    reset_product_data('area_rugs');
    // write_log('Sync Completed for all other area_rugs brands');
}


// /** Sheet Product sync Cron job **/

add_action('admin_init', 'sheetsync_settings');
function sheetsync_settings()
{

    if (get_option('sheetsync') == '1') {

        if (!wp_next_scheduled('sync_sheetvinyl_friday_event')) {

            wp_schedule_event(time() + 7700, 'daily', 'sync_sheetvinyl_friday_event');
        }
    } else {

        wp_clear_scheduled_hook('sync_sheetvinyl_friday_event');
    }
}


add_action('sync_sheetvinyl_friday_event', 'do_this_friday_sheetvinyl', 10, 2);

function do_this_friday_sheetvinyl()
{

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    $table_posts = $wpdb->prefix . 'posts';

    $permfile = $upload_dir . '/sheet' . '.json';
    $res = SOURCEURL . get_option('SITE_CODE') . '/www/sheet' . '.json?' . SFN_STATUS_PARAMETER;

    $tmpfile = download_url($res, $timeout = 900);

    if (is_wp_error($tmpfile)) {

        $status_sync = 'Failed';

        $wpdb->query("DELETE  FROM $product_sync_table WHERE product_category = 'sheet_vinyl'");

        $wpdb->insert(@$product_sync_table, array('product_category' => 'sheet_vinyl', 'product_brand' => 'Sheet Vinyl Brand', 'sync_status' => $status_sync), array('%s', '%s', '%s'));

        exit();
    } else {
        if (is_file($tmpfile)) {
            copy($tmpfile, $permfile);
            unlink($tmpfile);
        }
    }


    $obj = new Example_Background_Processing();
    $obj->handle_all('sheet_vinyl', 'sheet');
    reset_product_data('sheet');

    write_log('Sync Completed for all other sheet vinyl brands');
}

function check_product_group_redirection()
{
    global $wpdb;
    $table_redirect_group = $wpdb->prefix . 'redirection_groups';
    $datum = $wpdb->get_results("SELECT * FROM $table_redirect_group WHERE name = 'Products'");
    if ($wpdb->num_rows > 0) {

        write_log('check_product_group_redirection - Product Group Exists');
    } else {

        $wpdb->insert(
            $table_redirect_group,
            array(
                'name' => 'Products', //replaced non-existing variables $lq_name, and $lq_descrip, with the ones we set to collect the data - $name and $description
                'tracking' => '1',
                'module_id' => '1',
                'status' => 'enabled',
                'position' => '2'
            ),
            array('%s', '%s', '%s', '%s', '%s') //replaced %d with %s - I guess that your description field will hold strings not decimals
        );
    }
}

//301 redirect added from 404 logs table

if (!wp_next_scheduled('404_redirection_301_log_cronjob')) {

    $interval =  getIntervalTime('friday');
    wp_schedule_event(time() +  17800, 'daily', '404_redirection_301_log_cronjob');
}

add_action('404_redirection_301_log_cronjob', 'custom_404_redirect_hook');



function check_404($url)
{
    $headers = get_headers($url, 1);
    if ($headers[0] != 'HTTP/1.1 200 OK') {
        return true;
    } else {
        return false;
    }
}

// custom 301 redirects from  404 logs table
function custom_404_redirect_hook()
{
    global $wpdb;
    // write_log('in function');

    $table_redirect = $wpdb->prefix . 'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix . 'redirection_groups';

    $data_404 = $wpdb->get_results("SELECT * FROM $table_name");
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;

    if ($data_404) {
        foreach ($data_404 as $row_404) {

            if (strpos($row_404->url, 'carpet') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {
                //  write_log($row_404->url);      

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {


                    $destination_url_carpet = '/flooring/carpet/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_carpet,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');

                    //// write_log( 'carpet 301 added ');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }
            } else if (strpos($row_404->url, 'hardwood') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                // write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');
                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {


                    $destination_url_hardwood = '/flooring/hardwood/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_hardwood,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // write_log( 'hardwood 301 added ');

            } else if (strpos($row_404->url, 'laminate') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                //   write_log($row_404->url); 

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {


                    $destination_url_laminate = '/flooring/laminate/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_laminate,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                //write_log( 'laminate 301 added ');


            } else if ((strpos($row_404->url, 'luxury-vinyl') !== false || strpos($row_404->url, 'vinyl') !== false) && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                // write_log($row_404->url);  

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {


                    if (strpos($row_404->url, 'luxury-vinyl') !== false) {
                        $destination_url_lvt = '/flooring/luxury-vinyl/products/';
                    } elseif (strpos($row_404->url, 'vinyl') !== false) {
                        $destination_url_lvt = '/flooring/';
                    }

                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_lvt,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // write_log( 'luxury-vinyl 301 added ');
            } else if (strpos($row_404->url, 'tile') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                // write_log($row_404->url); 

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = check_404($url_404);

                if ($checkalready == '' && $headers_res == 'false') {

                    $destination_url_tile = '/flooring/tile/products/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url_tile,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // write_log( 'tile 301 added ');
            }
        }
    }
}

//Delete duplicate 301 redirects 

if (!wp_next_scheduled('301_redirection_duplicate_delete_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', '301_redirection_duplicate_delete_cronjob');
}

add_action('301_redirection_duplicate_delete_cronjob', 'Custom301_redirection_duplicate_delete');

// Delete duplicate 301 redirects entries
function Custom301_redirection_duplicate_delete()
{

    global $wpdb;
    // write_log('Delete duplicate 301 redirects in function');
    $table_redirect = $wpdb->prefix . 'redirection_items';

    // write_log($check_already);

    $check_already = $wpdb->query('SELECT url, COUNT(url) FROM ' . $table_redirect . ' GROUP BY url HAVING COUNT(url) > 1');

    //write_log($check_already);

    $wpdb->query('DELETE t1 FROM ' . $table_redirect . ' t1 INNER JOIN ' . $table_redirect . ' t2 WHERE t1.id < t2.id AND t1.url= t2.url;');

    //write_log('Deleted duplicate');

}


/** Auto Import Function for roomvo Template **/

function roomvo_autoimport()
{

    require_once plugin_dir_path(__FILE__) . '/autoimport/autoimporter.php';

    if (! class_exists('Auto_Importer')) {
        die('Auto_Importer not found');
    }
    // call the function
    $args = array(
        'file'        => plugin_dir_path(__FILE__) . '/autoimport/roomvo_template.xml'
    );

    if (get_page_by_title('New Roomvo Visualizer Template', OBJECT, 'fl-builder-template') == '') {

        auto_import($args);
    }
}

add_action('GDPR_database_update_cronjob', 'gdpr_setting_databse');

if (!wp_next_scheduled('GDPR_database_update_cronjob')) {
    // Schedule the event

    wp_schedule_event(strtotime('09:00:00'), 'daily', 'GDPR_database_update_cronjob');
}

function gdpr_setting_databse()
{
    global $wpdb;

    //write_log('gdpr_setting_databse');

    $gdpr_table = $wpdb->prefix . "gdpr_cc_options";

    $sql_gdpr_delete = "DROP TABLE IF EXISTS $gdpr_table";

    $wpdb->query($sql_gdpr_delete);

    $sql_gdpr = "CREATE TABLE $gdpr_table (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `option_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
        `option_value` longtext CHARACTER SET utf8,
        `site_id` int(11) DEFAULT NULL,
        `extras` longtext CHARACTER SET utf8,
        PRIMARY KEY (`id`)
      ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

    $wpdb->query($sql_gdpr);
    $sql_gdpr_privacy = "/privacy-policy/";
    $sql_gdpr_termsconditions = "/terms-and-conditions/";
    $sql_gdpr_insert = "INSERT INTO $gdpr_table (`id`, `option_key`, `option_value`, `site_id`, `extras`) VALUES
    (1, 'moove_gdpr_infobar_visibility', 'visible', 1, NULL),
(2, 'moove_gdpr_reject_button_enable', '1', 1, NULL),
(3, 'moove_gdpr_close_button_enable', '1', 1, NULL),
(4, 'moove_gdpr_colour_scheme', '1', 1, NULL),
(5, 'moove_gdpr_nonce', 'b85aaf05c3', 1, NULL),
(6, '_wp_http_referer', '/wp-admin/admin.php?page=moove-gdpr&tab=cookie-policy', 1, NULL),
    (7,	'moove_gdpr_info_bar_content',	'<p>Our site uses cookies to improve your experience. By using our site, you acknowledge and accept our use of cookies.</p>\n<p> Please read our <a href=" . $sql_gdpr_privacy . ">privacy policy</a> and the <a href=" . $sql_gdpr_termsconditions . ">terms and conditions</a> for more information.</p>\n',	1,	NULL),
   (8, 'moove_gdpr_infobar_accept_button_label', 'Accept', 1, NULL),
   (9, 'moove_gdpr_infobar_reject_button_label', 'Reject', 1, NULL),
   (10, 'moove_gdpr_infobar_position', 'bottom', 1, NULL),
   (11, 'moove_gdpr_modal_powered_by_disable', '0', 1, NULL),
   (12, 'moove_gdpr_plugin_layout', 'v1', 1, NULL),
   (13, 'moove_gdpr_modal_save_button_label', 'Save Changes', 1, NULL),
   (14, 'moove_gdpr_modal_allow_button_label', 'Enable All', 1, NULL),
   (15, 'moove_gdpr_modal_enabled_checkbox_label', 'Enabled', 1, NULL),
   (16, 'moove_gdpr_modal_disabled_checkbox_label', 'Disabled', 1, NULL),
   (17, 'moove_gdpr_consent_expiration', '60', 1, NULL),
   (18, 'moove_gdpr_modal_powered_by_label', 'Powered by', 1, NULL),
   (19, 'moove_gdpr_brand_colour', '#e22c2c', 1, NULL),
   (20, 'moove_gdpr_brand_secondary_colour', '#000000', 1, NULL),
   (21, 'moove_gdpr_company_logo', '/wp-content/plugins/gdpr-cookie-compliance/dist/images/gdpr-logo.png', 1, NULL),
   (22, 'moove_gdpr_logo_position', 'left', 1, NULL),
   (23, 'moove_gdpr_button_style', 'rounded', 1, NULL),
   (24, 'moove_gdpr_plugin_font_type', '3', 1, NULL),
   (25, 'moove_gdpr_plugin_font_family', '\'Montserrat\', sans-serif', 1, NULL),
   (26, 'moove_gdpr_cdn_url', '', 1, NULL),
   (27, 'moove_gdpr_accept_button_enable', '1', 1, NULL),
   (28, 'moove_gdpr_settings_button_enable', '1', 1, NULL),
   (29, 'gdpr_close_button_bhv', '1', 1, NULL),
   (30, 'gdpr_close_button_bhv_redirect', '', 1, NULL),
   (31, 'gdpr_accesibility', '0', 1, NULL),
   (32, 'gdpr_bs_buttons_order', '[\"accept\",\"reject\",\"settings\",\"close\"]', 1, NULL),
   (33, 'moove_gdpr_infobar_settings_button_label', 'Settings', 1, NULL),
   (34, 'gdpr_initialization_delay', '2000', 1, NULL),
   (35, 'moove_gdpr_third_party_cookies_enable_first_visit', '1', 1, NULL),
   (36, 'moove_gdpr_third_party_cookies_enable', '1', 1, NULL),
   (37, 'moove_gdpr_performance_cookies_tab_title', '3rd Party Cookies', 1, NULL),
   (38, 'moove_gdpr_performance_cookies_tab_content', '<p>This website uses Google Analytics to collect anonymous information such as the number of visitors to the site, and the most popular pages. Keeping this cookie enabled helps us to improve our website.</p>', 1, NULL),
   (39, 'moove_gdpr_third_party_header_scripts', '', 1, NULL),
   (40, 'moove_gdpr_third_party_body_scripts', '', 1, NULL),
   (41, 'moove_gdpr_third_party_footer_scripts', '', 1, NULL),
   (42, 'moove_gdpr_cookie_policy_enable', '1', 1, NULL),
   (43, 'moove_gdpr_cookie_policy_tab_nav_label', 'Cookie Policy', 1, NULL),
   (44, 'moove_gdpr_cookies_policy_tab_content', '<p>For more information about our cookie policy, please go to our <a href=" . $sql_gdpr_privacy . " target=\"_blank\">Privacy Policy</a></p>', 1, NULL),
   (45, 'moove_gdpr_advanced_cookies_enable_first_visit', '1', 1, NULL),
   (46, 'moove_gdpr_advanced_cookies_enable', '1', 1, NULL),
   (47, 'moove_gdpr_advanced_cookies_tab_title', 'Additional Cookies', 1, NULL),
   (48, 'moove_gdpr_advanced_cookies_tab_content', '<p>This website uses the following additional cookies:</p><p>(List the cookies that you are using on the website here.)</p>', 1, NULL),
   (49, 'moove_gdpr_advanced_cookies_header_scripts', '', 1, NULL),
   (50, 'moove_gdpr_advanced_cookies_body_scripts', '', 1, NULL),
   (51, 'moove_gdpr_advanced_cookies_footer_scripts', '', 1, NULL),
   (52, 'moove_gdpr_privacy_overview_tab_title', 'Privacy Overview', 1, NULL),
   (53, 'moove_gdpr_privacy_overview_tab_content', '<p>This website uses cookies so that we can provide you with the best user experience possible. Cookie information is stored in your browser and performs functions such as recognizing you when you return to our website and helping our team understand which sections of the website you find most interesting and useful.</p>', 1, NULL),
   (54, 'moove_gdpr_strictly_necessary_cookies_functionality', '1', 1, NULL),
   (55, 'moove_gdpr_strictly_necessary_cookies_tab_title', 'Strictly Necessary Cookies', 1, NULL),
   (56, 'moove_gdpr_strict_necessary_cookies_tab_content', '<p>Strictly Necessary Cookie should be enabled at all times so that we can save your preferences for cookie settings.</p>', 1, NULL),
   (57, 'moove_gdpr_strictly_necessary_cookies_warning', 'If you disable this cookie, we will not be able to save your preferences. This means that every time you visit this website you will need to enable or disable cookies again.', 1, NULL),
   (58, 'moove_gdpr_modal_strictly_secondary_notice', '<p>Please enable Strictly Necessary Cookies first so that we can save your preferences!</p>\n', 1, NULL)";

    write_log($sql_gdpr_insert);

    $wpdb->query($sql_gdpr_insert);
}


/*Thank you content*/
function display_thankyoumsg()
{

    if ($_GET['promocode'] == 'shwbtfllncct2021') {
        $post_contet_data = '<style>.thank-you-hide {display:none !important;}.thank-you-show {display:block !important;}</style>';
        $post_contet_data .= '[fl_builder_insert_layout slug="beautifall-us-thank-you"]';
    } else if ($_GET['promocode'] == 'shwbtfllfrct2021') {
        $post_contet_data = '<style>.thank-you-hide {display:none !important;}.thank-you-show {display:block !important;}</style>';
        $post_contet_data .= '[fl_builder_insert_layout slug="beautifall-ca-thank-you"]';
    } else {
        $post_contet_data = '<style>.thank-you-hide {display:block !important;}.thank-you-show {display:none !important;}</style>';
        $post_contet_data .= '[fl_builder_insert_layout slug="thankyou-content"]';
    }
    //$post_contet_data ='';
    return $post_contet_data;
}
add_shortcode('display_thankyoumsg', 'display_thankyoumsg');


if (! wp_next_scheduled('seo_titles_update_job')) {

    wp_schedule_event(strtotime('09:00:00'), 'daily', 'seo_titles_update_job');
}
add_action('seo_titles_update_job', 'do_seo_titles_update', 10, 2);

function do_seo_titles_update()
{

    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);

    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {

        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $seo_titles = $apiObj->call(BASEURL . get_option('SITE_CODE') . "/" . SEOURL, "GET", $inputs, $headers);

        // write_log($seo_titles['result']);


        foreach ($seo_titles['result'] as $seo) {

            $page_url = home_url() . '' . $seo['page'];

            $search_page =    url_to_postid($page_url);

            if ($seo['seoTitle'] == '/' && $seo['page'] == 'Home') {
                $search_page = get_option('page_on_front');
                // $pageID = get_option('page_on_front');
                // update_post_meta( $pageID, '_yoast_wpseo_title', $seo['seoTitle'] );
            }

            update_post_meta($search_page, '_yoast_wpseo_title', $seo['seoTitle']);
            update_post_meta($search_page, '_yoast_wpseo_metadesc', $seo['seoDescription']);
        }
    }
}

function get_companyname()
{
    $website = json_decode(get_option('website_json'));

    for ($j = 0; $j < count($website->sites); $j++) {
        return  $website->sites[$j]->name;
    }
}

// define the action for register yoast_variable replacments
function register_custom_yoast_variables()
{
    wpseo_register_var_replacement("%%[Retailer 'companyname']%%", 'get_companyname', 'advanced', 'some help text');
}

// Add action
add_action('wpseo_register_extra_replacements', 'register_custom_yoast_variables');


/**Add accessibility Page **/

function add_accessibility_page()
{

    $check_accessibility =  get_page_by_path('accessibility ', OBJECT, 'page');
    $metatitle = "Accessibility";
    $metadesc = "";
    if ($check_accessibility == '') {


        // Gather post data.
        $post_content_data = '[fl_builder_insert_layout slug="accessibility-new-template"]';

        $accessibility_post = array(
            'post_title'    => 'Accessibility',
            'post_name'     => 'accessibility',
            'post_content'  => $post_content_data,
            'post_type'     => 'page',
            'post_status'   => 'publish',
            'post_author'  => 1,
            'comment_status' => 'closed',   // if you prefer
            'ping_status' => 'closed'

        );

        // Insert the post into the database.
        $add_accessibility = wp_insert_post($accessibility_post);

        update_post_meta($add_accessibility, '_wp_page_template', 'accessibility.php');
        update_post_meta($add_accessibility, '_fl_builder_enabled', '1');
        update_post_meta($add_accessibility, '_yoast_wpseo_title', $metatitle);
        update_post_meta($add_accessibility, '_yoast_wpseo_metadesc', $metadesc);
    }
}


/** Auto Import Function for Floorte Template **/

function accessibility_autoimport()
{

    require_once plugin_dir_path(__FILE__) . '/autoimport/autoimporter.php';

    if (! class_exists('Auto_Importer')) {
        die('Auto_Importer not found');
    }
    // call the function
    $args = array(
        'file'        => plugin_dir_path(__FILE__) . '/autoimport/accessibility_page.xml'
    );


    if (get_page_by_title('Accessibility New Template', OBJECT, 'fl-builder-template') == '') {

        auto_import($args);
    }
}


function copyDirectory($source, $destination)
{
    if (!is_dir($destination)) {
        mkdir($destination, 0755, true);
    }
    $files = scandir($source);
    foreach ($files as $file) {
        if ($file !== '.' && $file !== '..') {
            $sourceFile = $source . '/' . $file;
            $destinationFile = $destination . '/' . $file;
            if (is_dir($sourceFile)) {
                copyDirectory($sourceFile, $destinationFile);
            } else {
                copy($sourceFile, $destinationFile);
            }
        }
    }
}

//backup file cron job
if (! wp_next_scheduled('product_jsonfile_backup')) {

    wp_schedule_event(strtotime('09:00:00'), 'daily', 'product_jsonfile_backup');
}
add_action('product_jsonfile_backup', 'do_product_jsonfile_backup_function', 10, 2);

function do_product_jsonfile_backup_function()
{
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];

    $sourceDirectory = $upload_dir . '/sfn-data';
    $destinationDirectory =  $upload_dir . '/backup-data';
    copyDirectory($sourceDirectory, $destinationDirectory);
}

//react sync code function
function sync_my_products()
{
    if (isset($_GET['sync_action1']) && $_GET['sync_action1'] === 'sync_products') {
        if ($_GET['category'] == 'carpet') {
            do_this_monday_carpet_mohawk();
        }
        if ($_GET['category'] == 'hardwood') {
            do_this_wednesday_hardwood();
        }
        if ($_GET['category'] == 'laminate') {
            do_this_thursday_laminate();
        }
        if ($_GET['category'] == 'lvt') {
            do_this_thursday_lvt();
        }
        if ($_GET['category'] == 'tile') {
            do_this_friday_tile();
        }
    }
}
add_action('admin_init', 'sync_my_products');

if (!wp_next_scheduled('compare_onsite_products_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', 'compare_onsite_products_cronjob');
}

add_action('compare_onsite_products_cronjob', 'compare_onsite_products');


function compare_onsite_products()
{

    write_log('compare_csv_onsite_products Started');

    write_log($delete_posttype . '----------' . $carpet_manufacturer);

    global $wpdb;
    $product_table = $wpdb->prefix . "posts";
    $table_redirect = $wpdb->prefix . 'redirection_items';
    $table_group = $wpdb->prefix . 'redirection_groups';
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;
    $table_posts = $wpdb->prefix . 'posts';
    $table_meta = $wpdb->prefix . 'postmeta';

    $brandmapping = array(

        "hardwood_catalog" => "hardwood",
        "laminate_catalog" => "laminate",
        "luxury_vinyl_tile" => "lvt",
        "tile_catalog" => "tile",
        "paint_catalog" => "paint",
        "area_rugs" => "rugs"

    );

    foreach ($brandmapping as $key => $val) {

        write_log('Comparing product with json file for------>' . $key);


        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $upload_dir = $upload_dir . '/sfn-data';
        $file_name = $val . '.json';


        $upload_dir = wp_upload_dir();
        $i = 0;
        $upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name;

        $strJsonFileContents = file_get_contents($upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name);

        $handle_data = str_replace('&quot;', '"', $strJsonFileContents);

        $handle = json_decode(($handle_data), true);

        $searchposts = $wpdb->get_results("SELECT ID,post_content FROM $table_posts WHERE post_type = '$key'");

        foreach ($searchposts as $find) {

            write_log('Search for----' . $find->post_content);

            $sku = $find->post_content;

            $key = array_search($sku, array_column($handle, 'sku'));

            if (!empty($key) || $key === 0) {

                write_log('Product found in Json');
            } else {

                $source_url = wp_make_link_relative(get_the_guid($find->ID));
                $match_url = rtrim($source_url, '/');

                $post_type =  get_post_type($find->ID);

                $urlmapping = array(
                    "/flooring/carpet/products/" => "carpeting",
                    "/flooring/hardwood/products/" => "hardwood_catalog",
                    "/flooring/laminate/products/" => "laminate_catalog",
                    "/flooring/" => "luxury_vinyl_tile",
                    "/flooring/tile/products/" => "tile_catalog",
                    "/flooring/paint/products/" => "paint_catalog",
                    "/flooring/area-rugs/products/" => "area_rugs",
                    "/flooring/waterproof/" => "solid_wpc_waterproof"
                );

                $destination_url = array_search($post_type, $urlmapping);

                write_log($destination_url);

                $data = array(
                    "url" => $source_url,
                    "match_url" => $match_url,
                    "match_data" => "",
                    "action_code" => "301",
                    "action_type" => "url",
                    "action_data" => $destination_url,
                    "match_type" => "url",
                    "title" => $sku,
                    "regex" => "true",
                    "group_id" => '1',
                    "position" => "1",
                    "last_access" => current_time('mysql'),
                    "status" => "enabled"
                );

                $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                $wpdb->insert($table_redirect, $data, $format);

                write_log('Product need to delete');

                wp_delete_post($find->ID);
            }
            //exit;
        }
    }


    write_log('compare_csv_onsite_products ended');
}

if (!wp_next_scheduled('compare_onsite_carpet_products_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', 'compare_onsite_carpet_products_cronjob');
}

add_action('compare_onsite_carpet_products_cronjob', 'compare_onsite_carpet_products');

function compare_onsite_carpet_products()
{

    write_log('compare_csv_onsite_carpet_products Started');

    write_log($delete_posttype . '----------' . $carpet_manufacturer);

    global $wpdb;
    $product_table = $wpdb->prefix . "posts";
    $table_redirect = $wpdb->prefix . 'redirection_items';
    $table_group = $wpdb->prefix . 'redirection_groups';
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;
    $table_posts = $wpdb->prefix . 'posts';
    $table_meta = $wpdb->prefix . 'postmeta';

    $brandmapping = array(

        "carpeting" => "carpet"

    );

    foreach ($brandmapping as $key => $val) {

        write_log('Comparing product with json file for------>' . $key);


        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $upload_dir = $upload_dir . '/sfn-data';
        $file_name = $val . '.json';


        $upload_dir = wp_upload_dir();
        $i = 0;
        $upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name;

        $strJsonFileContents = file_get_contents($upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name);

        $handle_data = str_replace('&quot;', '"', $strJsonFileContents);

        $handle = json_decode(($handle_data), true);

        $searchposts = $wpdb->get_results("SELECT ID,post_content FROM $table_posts WHERE post_type = '$key'");

        foreach ($searchposts as $find) {

            write_log('Search for----' . $find->post_content);

            $sku = $find->post_content;

            $key = array_search($sku, array_column($handle, 'sku'));

            if (!empty($key) || $key === 0) {

                write_log('Product found in Json');
            } else {

                $source_url = wp_make_link_relative(get_the_guid($find->ID));
                $match_url = rtrim($source_url, '/');

                $post_type =  get_post_type($find->ID);

                $urlmapping = array(
                    "/flooring/carpet/products/" => "carpeting"
                );

                $destination_url = array_search($post_type, $urlmapping);

                write_log($destination_url);

                $data = array(
                    "url" => $source_url,
                    "match_url" => $match_url,
                    "match_data" => "",
                    "action_code" => "301",
                    "action_type" => "url",
                    "action_data" => $destination_url,
                    "match_type" => "url",
                    "title" => $sku,
                    "regex" => "true",
                    "group_id" => '1',
                    "position" => "1",
                    "last_access" => current_time('mysql'),
                    "status" => "enabled"
                );

                $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                $wpdb->insert($table_redirect, $data, $format);

                write_log('Product need to delete');

                wp_delete_post($find->ID);
            }
            //exit;
        }
    }


    write_log('compare_csv_onsite_carpet_products ended');
}


add_action('admin_footer', 'enqueue_sync_script');

function enqueue_sync_script()
{
?>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            const productButtons = document.querySelectorAll('.product-sync-button');

            productButtons.forEach(button => {
                button.addEventListener('click', function() {
                    const productType = this.getAttribute('data-product-type');
                    this.disabled = true;
                    this.classList.add('syncing');
                    this.innerText = 'Syncing...';

                    // Send AJAX request to trigger background sync process
                    jQuery.ajax({
                        url: ajaxurl,
                        method: 'POST',
                        data: {
                            action: 'sync_product_data',
                            product_type: productType,
                        },
                        success: function(response) {
                            if (response.success) {
                                alert(`Background sync started for ${productType}.`);
                            } else {
                                alert('Failed to start sync.');
                            }
                            button.disabled = false;
                            button.classList.remove('syncing');
                            button.innerText = 'Sync ' + productType.charAt(0).toUpperCase() + productType.slice(1);
                        },
                        error: function() {
                            alert('An error occurred while starting the sync.');
                            button.disabled = false;
                            button.classList.remove('syncing');
                            button.innerText = 'Sync ' + productType.charAt(0).toUpperCase() + productType.slice(1);
                        }
                    });
                });
            });
        });
    </script>
<?php
}

add_action('wp_ajax_nopriv_background_sync_product_data', 'background_sync_product_data');
add_action('wp_ajax_background_sync_product_data', 'background_sync_product_data');
function background_sync_product_data()
{
    // Ensure the product type is passed via POST
    if (isset($_POST['product_type'])) {
        $product_type = sanitize_text_field($_POST['product_type']);

        // Call the actual sync function based on the product type
        sync_product_data($product_type);
    }

    exit; // Exit to allow true background processing without blocking the page
}


add_action('wp_ajax_sync_product_data', 'handle_sync_product_data');
function handle_sync_product_data()
{
    // Ensure product type is passed in the request
    if (isset($_POST['product_type'])) {
        $product_type = sanitize_text_field($_POST['product_type']);

        // Trigger background sync using wp_remote_post
        $url = admin_url('admin-ajax.php?action=background_sync_product_data');
        $response = wp_remote_post($url, array(
            'blocking' => false, // Non-blocking request to run in the background
            'body'     => array(
                'product_type' => $product_type, // Pass product type as POST data
            ),
        ));

        if (is_wp_error($response)) {
            wp_send_json_error('Failed to start sync.');
        } else {
            wp_send_json_success("Sync started for {$product_type}. It’s running in the background.");
        }
    } else {
        wp_send_json_error('Invalid product type.');
    }
}

function sync_product_data($product_type)
{
    // Add sync logic based on product type
    switch ($product_type) {
        case 'carpet':
            // Sync carpet data
            break;
        case 'hardwood':
            do_this_wednesday_hardwood();
            break;
        case 'laminate':
            do_this_thursday_laminate();
            break;
        case 'tile':
            do_this_friday_tile();
            break;
        case 'lvt':
            do_this_thursday_lvt();
            break;
    }
    // Response back to AJAX
    wp_send_json_success("Sync completed for {$product_type}!");
}


if (!wp_next_scheduled('mm_retailer_mysql_bin_log_job')) {
    // schedule an event
    wp_schedule_event(time(), 'twicedaily', 'mm_retailer_mysql_bin_log_job');
}

add_action('mm_retailer_mysql_bin_log_job', 'mm_retailer_mysql_bin_log_job_function');

function mm_retailer_mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("0 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
    // write_log($sql_delete);
}

if (!wp_next_scheduled('orphan_mysql_bin_log_job')) {
    // schedule an event
    wp_schedule_event(time(), 'bimonthly', 'orphan_mysql_bin_log_job');
}

add_action('orphan_mysql_bin_log_job', 'orphan_mysql_bin_log_job_function');

function orphan_mysql_bin_log_job_function()
{

    global $wpdb;
    $sql_orphan = "DELETE wp_postmeta FROM wp_postmeta
LEFT JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id
WHERE wp_posts.ID IS NULL";
    $delete_orphan = $wpdb->get_results($sql_orphan);
}

<?php get_header();
$rewrite_rule = maybe_unserialize(get_option("cptui_post_types"));
$rewrite_slug = "";
if (get_option("instocksyncseprate") == "1") {
	$rewrite_slug = $rewrite_rule["instock_lvt"]["rewrite_slug"];
}
$show_size_on_pdp = "no";
if (
	get_option("groupbysize") == "1" && is_array(get_option("groupbysize_cat"))	&& in_array("lvt", get_option("groupbysize_cat")) && get_option("groupbysizepdp") == "1"
) {
	$show_size_on_pdp = "yes";
}
$isworkbook = get_option('isworkbook') ? get_option('isworkbook') : 0;
global $wpdb;
$fav = array();
if (is_user_logged_in() && $isworkbook == "1") {
	$table_fav = $wpdb->prefix . 'favorite_posts';
	$fav_sql = "SELECT * FROM $table_fav WHERE user_id = " . get_current_user_id() . " AND  product_id = '" . get_the_content() . "'";

	$check_fav = $wpdb->get_results($fav_sql);

	if (!empty($check_fav)) {
		$fav['link_action'] = 'rem_fav';
		$fav['icon_class'] = 'fa fa-heart ';
		$fav['fav_text'] = "Remove From Favorite";
	} else {
		$fav['link_action'] = 'add_Fav ';
		$fav['icon_class'] = 'fa fa-heart-o';
		$fav['fav_text'] = "Add as Favorite";
	}
}
wp_localize_script('wp-product-filter-react', 'wpProductCategory', array(
	'category' => "lvt",
	'sku' => get_the_content(),
	'rewrite_slug' => $rewrite_slug,
	'show_size_on_pdp' => $show_size_on_pdp,
	'fav_details' => $fav
)); ?>

<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12 ">
			<div id="mm-product-details"></div>
		</div>
		<?php //FLTheme::sidebar('right'); 
		?>
	</div>
</div>
<?php
$pdpshowform = get_option('pdpshowform') ? get_option('pdpshowform') : 0;
if ($pdpshowform == 1) {
	$form_shortcode =	get_option('pdpshowforminstock') ? get_option('pdpshowforminstock') : "";
	if ($form_shortcode != "") {
		$gravity_form_html = do_shortcode($form_shortcode);
?>
		<div id="gravity-form-root" data-gravity-form="<?php echo esc_attr($gravity_form_html); ?>"></div>
<?php
	}
}
?>
<?php get_footer(); ?>
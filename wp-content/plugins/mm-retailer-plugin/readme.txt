=== Grand Child Theme ===
Contributors: mmteam
Tags: product listing, theme customization
Requires at least: 4.6
Tested up to: 8.0
Stable tag: 1.30
Requires PHP: 8.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A WordPress MM Retailer Plugin Theme (as a plugin).

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

A WordPress MM Retailer Plugin Theme (as a plugin)

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about Grand Child Plugin? =

A WordPress MM Retailer Plugin Theme (as a plugin)



== Changelog ==
1.30 =
* Add to favorite button on PDP

1.29 =
* Changes related to short Hours

1.28 =

* Alliance PLP and collection pages facets

1.27 =

* Area Rugs shortcode 

1.26 =

* Blog shortcode - RR, get direction shortcode - SP 

1.25 =

* sync tile,slug update

1.24 =

* Show all instock and normal colors available on PDP

1.23 =

* Instock PDP changes

1.22 =

* Backend PLP settings changes

1.21 =

* Backend PDP settings changes

1.20 =

* Area rugs PDP changes

1.19 =

* Sheet vinyl integration

1.18 =

* Product cron and compare product changes

1.17 =

* Area rug group by size integration

1.16 =

* Area rug settings and group by size

1.15 =

* Draft pages menu changes

1.14 =

* Color facet changes

1.13 =

* Mysql log cron job changes

1.12 =

* Look facet integration

1.11 =

* Coretec link changes

1.10 =

* Coretec css changes

1.9 =

* Coretec temp changes

1.8 =

* Css changes for PDP

1.7 =

* Area rugs sync integration coretec, floorte template integration

1.6 =

* Rooomvo script integration and instock tempaltes

1.5 =

* Instock Product sync changes

1.4 =

* Product sync changes and Backend design changes

1.3 =

* initial launch

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
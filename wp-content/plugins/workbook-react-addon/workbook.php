<?php

/** 
 * Plugin Name: Workbook React Addon
 * Text Domain: workbook-technology 
 * Description: Workbook Functionlity Add-Ons For MM Product List Plugin by Mobile Marketing
 * Author: Mobile Marketing
 * Version: 1.0.0 
 * Author URI: https://mobile-marketing.agency/
 */
if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly 
}
require_once(ABSPATH . "wp-includes/pluggable.php");
require_once(ABSPATH . "/wp-load.php");

$plugin_dir_url = str_replace('/var/www/html', '', plugin_dir_path(__FILE__));
define('plugin_dir', $plugin_dir_url);

// Include Sripts / CSS here
function enqueue_workbook_script()
{
    wp_enqueue_script('workbook-pl', plugin_dir . 'js/workbook.js', array('jquery'), 1.1, true);
    wp_enqueue_style('workbook-pl', plugin_dir . 'css/workbook.css', false, '1.0', 'all'); // Inside a plugin
}
add_action('wp_enqueue_scripts', 'enqueue_workbook_script');

// Include Admin Sripts / CSS here
function workbook_admin_style()
{
    wp_enqueue_style('ip-admin-styles', plugin_dir . 'css/admin.css');
}
add_action('admin_enqueue_scripts', 'workbook_admin_style');

// Create Database Tables
function create_the_workbook_tables()
{
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    $table_name = $wpdb->prefix . 'favorite_posts';

    $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
	id int(11) NOT NULL AUTO_INCREMENT,
	user_id int(11) NOT NULL,
	product_id TEXT NOT NULL,
	note TEXT NOT NULL,
	PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}

register_activation_hook(__FILE__, 'create_the_workbook_tables');

// WorkBook Functionality Start Here 

// Add Favorite Product
add_action('wp_ajax_nopriv_add_fav_product', 'add_fav_product');
add_action('wp_ajax_add_fav_product', 'add_fav_product');
function add_fav_product()
{
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta($post_id, 'is_fav', $is_fav);
    if ($result == false) {
        $result1 = add_post_meta($post_id, 'is_fav', $is_fav);
    }
}

// Add Favorite Products in AJAX
add_action('wp_ajax_nopriv_add_favroiute', 'add_favroiute');
add_action('wp_ajax_add_favroiute', 'add_favroiute');
function add_favroiute()
{
    global $wpdb;
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        $data = array(
            'user_id' => $_POST['user_id'],
            'product_id' => $_POST['post_id']
        );
        $wpdb->insert('wp_favorite_posts', $data);
        return true;
        if (class_exists('\LiteSpeed\Purge')) {
            do_action('litespeed_purge_all');
        }
        wp_die();
    } else {
        return false;
        wp_die();
    }
}

// Remove Favorite Products in AJAX
add_action('wp_ajax_nopriv_remove_favroiute', 'remove_favroiute');
add_action('wp_ajax_remove_favroiute', 'remove_favroiute');
function remove_favroiute()
{
    global $wpdb;
    $table_fav = $wpdb->prefix . 'favorite_posts';
    if (is_user_logged_in()) {
        $wpdb->get_results("DELETE FROM $table_fav WHERE user_id = " . $_POST['user_id'] . " and product_id = '" . $_POST['post_id'] . "'");
        return true;
        wp_die();
    } else {
        return false;
        wp_die();
    }
}

// Favorite Products Shortcode + Listing

add_shortcode('favorite_products', 'favorite_products_function');

function favorite_products_function()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'favorite_posts';
    $table_posts = $wpdb->prefix . 'posts';
    $site_url = get_site_url();
    $content = "";

    $fav_sql = 'SELECT product_id FROM ' . $table_name . ' WHERE user_id = ' . get_current_user_id() . '';
    $check_fav = $wpdb->get_results($fav_sql);

    $fav_products = array();
    foreach ($check_fav as $fav) {
        $fav_sql = "SELECT ID,post_title, guid ,post_type,post_content FROM " . $table_posts . " WHERE post_content = '" . $fav->product_id . "'";
        $check_id = $wpdb->get_results($fav_sql);
        if (!is_array($check_id) || count($check_id) == 0) {
            continue;
        }
        if (isset($fav_products[$check_id[0]->post_type])) {
            $post_arr = $fav_products[$check_id[0]->post_type];
            $fav_arr[0]['ID'] = $check_id[0]->ID;
            $fav_arr[0]['post_title'] = $check_id[0]->post_title;
            $fav_arr[0]['guid'] = $check_id[0]->guid;
            $fav_arr[0]['post_content'] = $check_id[0]->post_content;
            $fav_products[$check_id[0]->post_type] = array_merge($post_arr, $fav_arr);
        } else {
            $fav_products[$check_id[0]->post_type][0]['ID'] = $check_id[0]->ID;
            $fav_products[$check_id[0]->post_type][0]['post_title'] = $check_id[0]->post_title;
            $fav_products[$check_id[0]->post_type][0]['guid'] = $check_id[0]->guid;
            $fav_products[$check_id[0]->post_type][0]['post_content'] = $check_id[0]->post_content;
        }
    }

    $post_array = array(
        'carpeting' => array('title' => "Carpet", 'category' => "carpet"),
        'hardwood_catalog' => array('title' => "Hardwood", 'category' => "hardwood"),
        'laminate_catalog' => array('title' => "Laminate", 'category' => "laminate"),
        'luxury_vinyl_tile' => array('title' => "Luxury Vinyl Tile", 'category' => "lvt"),
        'tile_catalog' => array('title' => "Tile", 'category' => "tile"),
        'area_rugs' => array('title' => "Area Rugs", 'category' => "area_rugs"),
        'instock_carpet' => array('title' => "Instock Carpet", 'category' => "carpet"),
        'instock_hardwood' => array('title' => "Instock Hardwood", 'category' => "hardwood"),
        'instock_laminate' => array('title' => "Instock Laminate", 'category' => "laminate"),
        'instock_lvt' => array('title' => "Instock Luxury Vinyl Tile", 'category' => "lvt"),
        'instock_tile' => array('title' => "Instock Tile", 'category' => "tile")
    );

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h1>MY FAVORITE PRODUCTS </h1>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';

    if (!empty($check_fav)) {
        foreach ($post_array as $posttype => $posts) {
            if (!isset($fav_products[$posttype])) {
                continue;
            }
            $products = $fav_products[$posttype];

            $post_type_name = $posts['title'];

            $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ($products) {
                $content .= '<h3>' . $post_type_name . '</h3>';

                foreach ($products as $product) {

                    $sku = $product['post_content'];
                    $note_sql = "SELECT note FROM $table_name WHERE user_id = '" . get_current_user_id() . "' and product_id='" . $sku . "'";

                    $check_note = $wpdb->get_results($note_sql, ARRAY_A);

                    $api_url_first = $site_url . '/wp-json/products/v1/list?sku=' .  $sku . '&category=' . $posts['category'];
                    $response_first = wp_remote_get($api_url_first);

                    if (is_wp_error($response_first)) {
                        error_log('API Request Failed: ' . $response_first->get_error_message());
                        continue;
                    }

                    $content_first = wp_remote_retrieve_body($response_first);
                    $current_product_first = json_decode($content_first, true);

                    $products_group_by = $current_product_first['products_group_by'];
                    if (is_array($products_group_by) && !empty($products_group_by)) {
                        $collection_name = key($products_group_by);
                        $first_group_products = $products_group_by[$collection_name];
                        $first_product = $first_group_products[0];
                    } else {
                        continue;
                    }

                    $current_swatch =  $first_product['swatch'];


                    $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
                    $content .= '<div class="fl-post-grid-image prod_like_wrap">';

                    $image = 'https://mm-media-res.cloudinary.com/image/fetch/h_400,w_400,c_limit/https://' . $current_swatch;

                    // $manufacturer = $product->manufacturer;

                    $content .= '<a href="' . $product['guid'] . '" title="' . $product['post_title'] . '"><img crossOrigin="Anonymous" class="list-pro-image" src="' . $image . '" alt="' . $product['post_title'] . '" /></a>';

                    $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="' . $product['guid'] . '" title="' . $product['post_title'] . '">VIEW PRODUCT</a>';
                    // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                    if ($check_note[0]['note'] == '' || $check_note[0]['note'] == null) {
                        $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="' . $sku . '">ADD A NOTE</a>';
                    }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="' . $sku . '" class="remove-parent" data-user_id="' . get_current_user_id() . '" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if ($check_note[0]['note'] != '' || $check_note[0]['note'] != null) {
                        $content .= '<a class="view_note_fav" data-note="' . $check_note[0]['note'] . '" id="view_note_fun" href="javascript:void(0)" data-productid="' . $sku . '"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>';
                        $content .= '<div id="' . $sku . '" style="display:none">' . $check_note[0]['note'] . '</div>';
                    }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>' . $product['post_title'] . '</span></h4>';
                    $content .= '</div>';

                    //wpfp_remove_favorite_link(get_the_ID());
                    $content .= "</div></div>";
                }
            }
            $content .= "</div></div>";
        }
    } else {
        $content .= '<p class="text-center"><em>Currently there is no Favorite Products Available</em></p>';
    }
    $content .= '</div></div>';

    return $content;
}

// Remove Favorite Products from Listing
add_action('wp_ajax_nopriv_remove_favroiute_list', 'remove_favorite_posts_function');
add_action('wp_ajax_remove_favroiute_list', 'remove_favorite_posts_function');
function remove_favorite_posts_function()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'favorite_posts';
    $table_posts = $wpdb->prefix . 'posts';
    $site_url = get_site_url();
    $content = "";
    $wpdb->get_results("DELETE FROM $table_name WHERE product_id = '" . $_POST['post_id'] . "' and user_id = '" . get_current_user_id() . "'");
    $fav_sql = "SELECT product_id FROM $table_name WHERE user_id = '" . get_current_user_id() . "'";
    $check_fav = $wpdb->get_results($fav_sql);
    $fav_products = array();


    $fav_products = array();
    foreach ($check_fav as $fav) {
        $fav_sql = "SELECT ID,post_title, guid ,post_type,post_content FROM " . $table_posts . " WHERE post_content = '" . $fav->product_id . "'";
        $check_id = $wpdb->get_results($fav_sql);
        if (!is_array($check_id) || count($check_id) == 0) {
            continue;
        }
        if (isset($fav_products[$check_id[0]->post_type])) {
            $post_arr = $fav_products[$check_id[0]->post_type];
            $fav_arr[0]['ID'] = $check_id[0]->ID;
            $fav_arr[0]['post_title'] = $check_id[0]->post_title;
            $fav_arr[0]['guid'] = $check_id[0]->guid;
            $fav_arr[0]['post_content'] = $check_id[0]->post_content;
            $fav_products[$check_id[0]->post_type] = array_merge($post_arr, $fav_arr);
        } else {
            $fav_products[$check_id[0]->post_type][0]['ID'] = $check_id[0]->ID;
            $fav_products[$check_id[0]->post_type][0]['post_title'] = $check_id[0]->post_title;
            $fav_products[$check_id[0]->post_type][0]['guid'] = $check_id[0]->guid;
            $fav_products[$check_id[0]->post_type][0]['post_content'] = $check_id[0]->post_content;
        }
    }

    $post_array = array(
        'carpeting' => array('title' => "Carpet", 'category' => "carpet"),
        'hardwood_catalog' => array('title' => "Hardwood", 'category' => "hardwood"),
        'laminate_catalog' => array('title' => "Laminate", 'category' => "laminate"),
        'luxury_vinyl_tile' => array('title' => "Luxury Vinyl Tile", 'category' => "lvt"),
        'tile_catalog' => array('title' => "Tile", 'category' => "tile"),
        'area_rugs' => array('title' => "Area Rugs", 'category' => "area_rugs"),
        'instock_carpet' => array('title' => "Instock Carpet", 'category' => "carpet"),
        'instock_hardwood' => array('title' => "Instock Hardwood", 'category' => "hardwood"),
        'instock_laminate' => array('title' => "Instock Laminate", 'category' => "laminate"),
        'instock_lvt' => array('title' => "Instock Luxury Vinyl Tile", 'category' => "lvt"),
        'instock_tile' => array('title' => "Instock Tile", 'category' => "tile")
    );

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h1>MY FAVORITE PRODUCTS </h1>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';

    if (!empty($check_fav)) {
        foreach ($post_array as $posttype => $posts) {
            if (!isset($fav_products[$posttype])) {
                continue;
            }
            $products = $fav_products[$posttype];

            $post_type_name = $posts['title'];

            $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ($products) {
                $content .= '<h3>' . $post_type_name . '</h3>';

                foreach ($products as $product) {

                    $sku = $product['post_content'];
                    $note_sql = "SELECT note FROM $table_name WHERE user_id = '" . get_current_user_id() . "' and product_id='" . $sku . "'";

                    $check_note = $wpdb->get_results($note_sql, ARRAY_A);

                    $api_url_first = $site_url . '/wp-json/products/v1/list?sku=' .  $sku . '&category=' . $posts['category'];
                    $response_first = wp_remote_get($api_url_first);

                    if (is_wp_error($response_first)) {
                        error_log('API Request Failed: ' . $response_first->get_error_message());
                        continue;
                    }

                    $content_first = wp_remote_retrieve_body($response_first);
                    $current_product_first = json_decode($content_first, true);

                    $products_group_by = $current_product_first['products_group_by'];
                    if (is_array($products_group_by) && !empty($products_group_by)) {
                        $collection_name = key($products_group_by);
                        $first_group_products = $products_group_by[$collection_name];
                        $first_product = $first_group_products[0];
                    } else {
                        continue;
                    }

                    $current_swatch =  $first_product['swatch'];


                    $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
                    $content .= '<div class="fl-post-grid-image prod_like_wrap">';

                    $image = 'https://mm-media-res.cloudinary.com/image/fetch/h_400,w_400,c_limit/https://' . $current_swatch;

                    // $manufacturer = $product->manufacturer;

                    $content .= '<a href="' . $product['guid'] . '" title="' . $product['post_title'] . '"><img crossOrigin="Anonymous" class="list-pro-image" src="' . $image . '" alt="' . $product['post_title'] . '" /></a>';

                    $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="' . $product['guid'] . '" title="' . $product['post_title'] . '">VIEW PRODUCT</a>';
                    // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                    if ($check_note[0]['note'] == '' || $check_note[0]['note'] == null) {
                        $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="' . $sku . '">ADD A NOTE</a>';
                    }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="' . $sku . '" class="remove-parent" data-user_id="' . get_current_user_id() . '" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if ($check_note[0]['note'] != '' || $check_note[0]['note'] != null) {
                        $content .= '<a class="view_note_fav" data-note="' . $check_note[0]['note'] . '" id="view_note_fun" href="javascript:void(0)" data-productid="' . $sku . '"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>';
                        $content .= '<div id="' . $sku . '" style="display:none">' . $check_note[0]['note'] . '</div>';
                    }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>' . $product['post_title'] . '</span></h4>';
                    $content .= '</div>';

                    //wpfp_remove_favorite_link(get_the_ID());
                    $content .= "</div></div>";
                }
            }
            $content .= "</div></div>";
        }
    } else {
        $content .= '<p class="text-center"><em>Currently there is no Favorite Products Available</em></p>';
    }
    $content .= '</div></div>';

    echo $content;
    if (class_exists('\LiteSpeed\Purge')) {
        do_action('litespeed_purge_all');
    }

    wp_die();
}

// Add Note to Measurement Tools
add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');
function add_note_form()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'favorite_posts';
    $fav_sql = "UPDATE $table_name SET note = '" . $_POST['note'] . "' WHERE user_id = '" . get_current_user_id() . "' and product_id = '" . $_POST['addnote_productid'] . "'";
    $check_fav = $wpdb->get_results($fav_sql);
}
// WorkBook Functionality End Here 
<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri() . "/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.min.js", "", "", 1);
    wp_enqueue_script("PDF_script", "https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js", "", "", 1);
    wp_enqueue_script("shareBox-script", get_stylesheet_directory_uri() . "/resources/sharebox/needsharebutton.js", "", "", 1);
    wp_enqueue_style('Share button Style', get_stylesheet_directory_uri() . '/resources/sharebox/needsharebutton.css', array(), '0.1.0', 'all');
    wp_enqueue_script("child-script", get_stylesheet_directory_uri() . "/script.js", "", "", 1);
});
//add method to register event to WordPress init

add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');


function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);
}

//Facet Title Hook
// add_filter('facetwp_shortcode_html', function ($output, $atts) {
//     if (isset($atts['facet'])) {
//         $output = '<div class="facet-wrap"><strong>' . $atts['title'] . '</strong>' . $output . '</div>';
//     }
//     return $output;
// }, 10, 2);

add_action('wp_ajax_nopriv_paid_search_check', 'paid_search_check', 1);
add_action('wp_ajax_paid_search_check', 'paid_search_check', 1);

function paid_search_check()
{

    if (isset($_POST['gclid']) && $_POST['gclid'] != null) {
        setcookie('paid_search', 'yes', time() + (86400 * 30), "/");
    }
    wp_die();
}

// IP location FUnctionality
if (!wp_next_scheduled('cde_preferred_location_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

//add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location()
{

    global $wpdb;

    if (!function_exists('post_exists')) {
        require_once(ABSPATH . 'wp-admin/includes/post.php');
    }

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);

        // write_log($website['result']['locations']);

        for ($i = 0; $i < count($website['result']['locations']); $i++) {

            if ($website['result']['locations'][$i]['type'] == 'store' && $website['result']['locations'][$i]['address'] != null) {

                $location_name = isset($website['result']['locations'][$i]['city']) ? $website['result']['locations'][$i]['name'] : "";

                //  write_log($location_name);

                $found_post = post_exists($location_name, '', '', 'store-locations');

                // write_log('found_post----'.$found_post);

                if ($found_post == 0) {

                    $array = array(
                        'post_title' => $location_name,
                        'post_type' => 'store-locations',
                        'post_content'  => "",
                        'post_status'   => 'publish',
                        'post_author'   => 0,
                    );
                    $post_id = wp_insert_post($array);

                    //  write_log( $location_name.'---'.$post_id);

                    update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']);

                    $location_address_url =  "AJ Rose Carpets+";
                    $location_address_url  .= isset($website['result']['locations'][$i]['address']) ? $website['result']['locations'][$i]['address'] . " " : "";
                    $location_address_url .= isset($website['result']['locations'][$i]['city']) ? $website['result']['locations'][$i]['city'] . " " : "";
                    $location_address_url .= isset($website['result']['locations'][$i]['state']) ? $website['result']['locations'][$i]['state'] . " " : "";
                    $location_address_url .= isset($website['result']['locations'][$i]['postalCode']) ? $website['result']['locations'][$i]['postalCode'] . " " : "";


                    update_post_meta($post_id, 'map_url', "https://maps.google.com/maps/?q=" . urlencode($location_address_url) . "");
                } else {

                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']);
                }
            }
        }
    }
}



//get ipaddress of visitor

function getUserIpAddr()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    if (strstr($ipaddress, ',')) {
        $tmp = explode(',', $ipaddress, 2);
        $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long
function get_storelisting()
{

    global $wpdb;
    $content = "";
    $phone = '';
    $content_list = "";
    if (isset($_COOKIE['preferred_store'])) {

        $store_location = $_COOKIE['preferred_store'];
    } else {

        $store_location = '';
    }

    $sql =  "SELECT * FROM wp_posts WHERE post_type = 'store-locations' AND post_status = 'publish'";

    $storeposts = $wpdb->get_results($sql);

    $storeposts_array = json_decode(json_encode($storeposts), true);
    $storeSet = "";

    if ($store_location == '') {
        $store_location = $storeposts_array['0']['ID'];
        $storeSet = "No";
    } else {
        $key = array_search($store_location, array_column($storeposts_array, 'ID'));
    }

    if ($storeSet == "No") {
        $content = "CHOOSE A LOCATION";
    } else {
        $content = get_the_title($store_location);
    }

    if ($_POST['paid_search'] == 'yes' || $_POST['gclid'] != "NA") {

        if (get_field('paid_search_phone_number', $store_location) != '') {

            $phone = get_field('paid_search_phone_number', $store_location);
        } else {

            $phone = get_field('phone', $store_location);
        }
    } else if ($_POST['paid_search'] == 'NA' || $_POST['gclid'] == "NA") {

        if ($storeSet == "No") {
            $phone = "";
        } else {
            $phone = get_field('phone', $store_location);
        }
    }

    foreach ($storeposts as $post) {

        $phone_no = '';
        $directionURL =    get_field('map_url', $post->ID);

        $content_list .= '<div class="store_wrapper o' . get_field('list_order', $post->ID) . '" id ="' . $post->ID . '">
                	
                    <h5 class="title-prefix">' . get_the_title($post->ID) . '</h5>';

        if (get_field('address', $post->ID)) {
            $content_list .= '<h5 class="store-add"> ' . get_field('address', $post->ID) . '<br />' . get_field('city', $post->ID) . ', ' . get_field('state', $post->ID) . ' ' . get_field('postal_code', $post->ID) . '</h5>';
        }



        if (get_field(strtolower(date("l")), $post->ID) == 'CLOSED') {

            $content_list .= '<p class="store-hour">CLOSED TODAY</p>';
        } else {

            $openuntil = explode("-", get_field(strtolower(date("l")), $post->ID));

            if ($post->ID != 1896972) {
                $content_list .= '<p class="store-hour">OPEN UNTIL ' . str_replace(' ', '', $openuntil[1]) . '</p>';
            }
        }


        if ($_POST['paid_search'] == 'yes' || $_POST['gclid'] != "NA") {

            if (get_field('paid_search_phone_number', $post->ID) != '') {

                $phone_no = get_field('paid_search_phone_number', $post->ID);
            } else {

                $phone_no = get_field('phone', $post->ID);
            }
        } else if ($_POST['paid_search'] == 'NA' || $_POST['gclid'] == "NA") {

            $phone_no = get_field('phone', $post->ID);
        }


        $content_list .= '<p class="store-phone"><a href="tel:' . $phone_no . '">' . $phone_no . '</a> </p>';

        if (get_field('map_url', $post->ID)) {
            $content_list .= '<a href="' . get_field('map_url', $post->ID) . '" target="_blank" class="store-cta-link view_location"> GET DIRECTION </a>';
        }

        $content_list .= '<a href="' . get_field('store_url', $post->ID) . '" data-id="' . $post->ID . '" data-storename="' . get_the_title($post->ID) . '" data-distance="' . round(@$post->distance, 1) . '" data-phone="' . $phone_no . '" target="_self" data-link="' . get_field('store_url', $post->ID) . '" class="store-cta-link more">LEARN MORE  </a>';

        $content_list .= '<a data-id="' . $post->ID . '" data-storename="' . get_the_title($post->ID) . '" data-distance="' . round(@$post->distance, 1) . '" data-phone="' . $phone_no . '" target="_self" data-link="' . get_field('store_url', $post->ID) . '" class="store-cta-link choose_location">CHOOSE LOCATION <input type="checkbox" class="currentLoc" style="visibility: hidden;" /> </a>';


        $content_list .= '</div>';
    }


    $data = array();

    $data['header'] = $content;
    $data['phone'] = $phone;
    $data['list'] = $content_list;
    $data['storeSet'] = $storeSet;

    echo json_encode($data);
    wp_die();
}


add_action('wp_ajax_nopriv_get_storelisting', 'get_storelisting', 2);
add_action('wp_ajax_get_storelisting', 'get_storelisting', 2);



//choose this location FUnctionality

add_action('wp_ajax_nopriv_choose_location', 'choose_location');
add_action('wp_ajax_choose_location', 'choose_location');

function choose_location()
{
    $storeN = get_the_title($_POST['store_id']);
    $content .= '<p>You’re Shopping</p>
      <h3 class="header_location_name">' .  $storeN . '</h3>
	 <div class="phone"><a class="mobile_phone" href="tel:' . $_POST['phone'] . '">' . $_POST['phone'] . '</a></div>	
      <div class="storeLink"><a href="/about/locations/">View All Locations<br></a></div>';

    echo $content;

    wp_die();
}


// add_action('wp_ajax_nopriv_add_fav_product', 'add_fav_product');
// add_action('wp_ajax_add_fav_product', 'add_fav_product');

// function add_fav_product()
// {
//     $is_fav = $_POST['is_fav'];
//     $post_id = $_POST['post_id'];

//     $result = update_post_meta($post_id, 'is_fav', $is_fav);
//     if ($result == false) {
//         $result1 = add_post_meta($post_id, 'is_fav', $is_fav);
//     }
// }

add_action('wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert');
add_action('wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert');

function base64_to_jpeg_convert()
{
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file = $upload_dir['basedir'] . '/measure/' . uniqid() . '.png';

    $img = $_POST['imagedata'];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);

    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        //  write_log( 'Personal Message For '. $current_user->ID ) ;

        $data = array(
            'userid' => $current_user->ID,
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file
        );

        $wpdb->insert('wp_measure_images', $data);

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        // write_log($measureimages);

        if (count($measureimages) > 0) {
            $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

            $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

            foreach ($measureimages as $img) {



                $img_path = str_replace('/var/www/html', '', $img->image_path);

                $image_url = home_url() . '' . $img_path;

                $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
                $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

                $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="' . home_url() . '' . $img_path . '" /></a>';

                $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="' . home_url() . '' . $img_path . '" onclick="openPopUp(this)" data-title="' . $img->image_name . '">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="' . $img->id . '" data-userid = "' . $current_user->ID . '"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                $content .= '</div> </div>';
                $content .= '<h3><span>' . $img->image_name . '</span></h3>';

                $content .= "</div></div>";
            }

            $content .= "</div></div>";

            echo $content;
        }

        wp_die();
    }
}

function measurement_tool_images($arg)
{
    global $wpdb;

    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        // write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");
        $content = "";

        if (count($measureimages) > 0) {

            $content .= '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
            $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

            foreach ($measureimages as $img) {



                $img_path = str_replace('/var/www/html', '', $img->image_path);

                $image_url = home_url() . '' . $img_path;

                $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
                $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

                $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="' . home_url() . '' . $img_path . '" /></a>';

                $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="' . home_url() . '' . $img_path . '" onclick="openPopUp(this)" data-title="' . $img->image_name . '">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0);" data-id="' . $img->id . '" data-userid = "' . $current_user->ID . '"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                $content .= '</div> </div>';
                $content .= '<h3><span>' . $img->image_name . '</span></h3>';

                $content .= "</div></div>";
            }

            $content .= "</div></div>";
        }
        return $content;
    }
}
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action('wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images');
add_action('wp_ajax_delete_measureimg', 'delete_measurement_images');

function delete_measurement_images()
{
    global $wpdb;

    $mid = $_POST['mimg_id'];
    $uid = $_POST['user_id'];

    if (is_user_logged_in()) {
        // $current_user = wp_get_current_user();
        write_log('Personal Message For ' . $current_user->ID);

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = ' . $uid . ' and id = ' . $mid . '');


        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $uid");

        // write_log($measureimages);


        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach ($measureimages as $img) {

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="' . home_url() . '' . $img_path . '" title="#" class="wplightbox"><img class="measure_img" src="' . home_url() . '' . $img_path . '" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="' . home_url() . '' . $img_path . '" data-title="' . $img->image_name . '" onclik="openPopUp(e)">VIEW</a>';
            $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0);" data-id="' . $img->id . '" data-userid = "' . $current_user->ID . '"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            $content .= '</div> </div>';
            $content .= '<h3><span>' . $img->image_name . '</span></h3>';

            $content .= "</div></div>";
        }

        $content .= "</div></div>";

        echo $content;
    }

    wp_die();
}

add_action('wp_ajax_nopriv_add_favroiute', 'add_favroiute');
add_action('wp_ajax_add_favroiute', 'add_favroiute');

// function add_favroiute()
// {

//     global $wpdb;

//     if (is_user_logged_in()) {
//         $current_user = wp_get_current_user();
//         // write_log( 'Personal Message For '. $current_user->ID ) ;

//         $data = array(
//             'user_id' => $_POST['user_id'],
//             'product_id' => $_POST['post_id']
//         );

//         $wpdb->insert('wp_favorite_posts', $data);

//         return true;

//         wp_die();
//     } else {

//         return false;

//         wp_die();
//     }
// }

// add_action('wp_ajax_nopriv_remove_favroiute', 'remove_favroiute');
// add_action('wp_ajax_remove_favroiute', 'remove_favroiute');

// function remove_favroiute()
// {

//     global $wpdb;

//     if (is_user_logged_in()) {
//         $current_user = wp_get_current_user();
//         // write_log( 'Personal Message For '. $current_user->ID ) ;

//         $data = array(
//             'user_id' => $_POST['user_id'],
//             'product_id' => $_POST['post_id']
//         );

//         $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = ' . $_POST['user_id'] . ' and product_id = ' . $_POST['post_id'] . '');

//         return true;

//         wp_die();
//     } else {

//         return false;

//         wp_die();
//     }
// }


// function ajrose_favorite_posts_function()
// {

//     global $wpdb;

//     $content = "";

//     $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = ' . get_current_user_id() . '';

//     $check_fav = $wpdb->get_results($fav_sql);

//     // write_log($check_fav);

//     $fav_products = array();

//     foreach ($check_fav as $fav) {

//         $fav_products[] = $fav->product_id;
//     }

//     // write_log('dsfsd'.$fav_products);

//     $favorite_post_ids = array_reverse($fav_products);
//     $page = intval(get_query_var('paged'));

//     $post_array = array('carpeting', 'hardwood_catalog', 'laminate_catalog', 'luxury_vinyl_tile', 'tile_catalog', 'instock_hardwood', 'instock_carpet', 'instock_lvt');

//     $brandmapping = array(
//         "Carpet" => "carpeting",
//         "Hardwood" => "hardwood_catalog",
//         "Laminate" => "laminate_catalog",
//         "Tile" => "tile_catalog",
//         "Vinyl" => "luxury_vinyl_tile",
//         "Instock Hardwood" => "instock_hardwood",
//         "Instock Carpet" => "instock_carpet",
//         "Instock Vinyl" => "instock_lvt"
//     );




//     $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
//                     <div class="shareAndPrint hideShare">
//                         <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
//                         <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
//                     </div>
//                 <div class="row statiContent">
//                     <div class="col-lg-12">
//                         <h1>MY FAVORITE PRODUCTS </h1>
//                         <p>Our product catalog is home to thousands of products. 
//                         Collect your favorites and save them here to help you select the right product for your home.</p>
//                     </div>
//                 </div>';

//     if (!empty($check_fav)) {


//         foreach ($post_array as $posttype) {

//             $qry = array('post__in' => $favorite_post_ids, 'posts_per_page' => $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
//             // custom post type support can easily be added with a line of code like below.
//             //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
//             $qry['post_type'] = array($posttype);

//             query_posts($qry);

//             $post_type_name = array_search($posttype, $brandmapping);

//             $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
//             if (have_posts()) :
//                 $content .= '<h3>' . $post_type_name . '</h3>';
//             endif;
//             while (have_posts()) : the_post();

//                 $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = ' . get_current_user_id() . ' and product_id=' . get_the_ID() . '';

//                 $check_note = $wpdb->get_results($note_sql, ARRAY_A);
//                 //   write_log('check_note');
//                 //   write_log($check_note[0]['note']);

//                 $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
//                 $content .= '<div class="fl-post-grid-image prod_like_wrap">';

//                 $image = swatch_image_product_thumbnail(get_the_ID(), '222', '222');

//                 $sku = get_post_meta(get_the_ID(), 'sku', true);
//                 $manufacturer = get_post_meta(get_the_ID(), 'manufacturer', true);

//                 $content .= '<a href="' . get_permalink() . '" title="' . get_the_title() . '"><img crossOrigin="Anonymous" class="list-pro-image" src="' . $image . '" alt="' . get_the_title() . '" /></a>';

//                 $content .= '<div class="favButtons button-wrapper">
//                     <a class="button fl-button" href="' . get_permalink() . '" title="' . get_the_title() . '">VIEW PRODUCT</a>';
//                 // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
//                 if ($check_note[0]['note'] == '' || $check_note[0]['note'] == null) {
//                     $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="' . get_the_ID() . '">ADD A NOTE</a>';
//                 }
//                 $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="' . get_the_ID() . '" class="remove-parent" data-user_id="' . get_current_user_id() . '" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

//                 if ($check_note[0]['note'] != '' || $check_note[0]['note'] != null) {
//                     $content .= '<a class="view_note_fav" data-note="' . $check_note[0]['note'] . '" id="view_note_fun" href="javascript:void(0)" data-productid="' . get_the_ID() . '"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>';
//                     $content .= '<div id="' . get_the_ID() . '" style="display:none">' . $check_note[0]['note'] . '</div>';
//                 }
//                 $content .= '</div></div> </div>';
//                 $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
//                 $content .= '<h4><span>' . get_the_title() . '</span></h4>';
//                 $content .= '</div>';

//                 //wpfp_remove_favorite_link(get_the_ID());
//                 $content .= "</div></div>";
//             endwhile;
//             $content .= "</div></div>";
//         }
//         wp_reset_postdata();
//         echo '</div></div>';

//         return $content;
//     }
// }
// add_shortcode('ajrose_favorite_posts', 'ajrose_favorite_posts_function');


// add_action('wp_ajax_nopriv_remove_favroiute_list', 'remove_ajrose_favorite_posts_function');
// add_action('wp_ajax_remove_favroiute_list', 'remove_ajrose_favorite_posts_function');
// function remove_ajrose_favorite_posts_function()
// {

//     global $wpdb;

//     $content = "";

//     $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = ' . $_POST['post_id'] . ' and user_id = ' . get_current_user_id() . '');

//     $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = ' . get_current_user_id() . '';

//     $check_fav = $wpdb->get_results($fav_sql);

//     // write_log($check_fav);

//     $fav_products = array();

//     foreach ($check_fav as $fav) {

//         $fav_products[] = $fav->product_id;
//     }

//     // write_log('dsfsd'.$fav_products);

//     $favorite_post_ids = array_reverse($fav_products);
//     $page = intval(get_query_var('paged'));

//     $post_array = array('carpeting', 'hardwood_catalog', 'laminate_catalog', 'luxury_vinyl_tile', 'tile_catalog');

//     $brandmapping = array(
//         "Carpet" => "carpeting",
//         "Hardwood" => "hardwood_catalog",
//         "Laminate" => "laminate_catalog",
//         "Luxury Vinyl Tile" => "luxury_vinyl_tile",
//         "Ceramic Tile" => "tile_catalog",
//         "Waterproof" => "solid_wpc_waterproof"
//     );




//     $content .= '<div class="favProWarpper printWrap">
//                     <div class="shareAndPrint hideShare">
//                         <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
//                         <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
//                     </div>
//                 <div class="row statiContent">
//                     <div class="col-lg-12">
//                         <h1>MY FAVORITE PRODUCTS </h1>
//                         <p>Our product catalog is home to thousands of products. 
//                         Collect your favorites and save them here to help you select the right product for your home.</p>
//                     </div>
//                 </div>';

//     if (!empty($check_fav)) {


//         foreach ($post_array as $posttype) {

//             $qry = array('post__in' => $favorite_post_ids, 'posts_per_page' => $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
//             // custom post type support can easily be added with a line of code like below.
//             //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
//             $qry['post_type'] = array($posttype);

//             query_posts($qry);

//             $post_type_name = array_search($posttype, $brandmapping);

//             $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
//             if (have_posts()) :
//                 $content .= '<h3>' . $post_type_name . '</h3>';
//             endif;
//             while (have_posts()) : the_post();

//                 $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = ' . get_current_user_id() . ' and product_id=' . get_the_ID() . '';

//                 $check_note = $wpdb->get_results($note_sql, ARRAY_A);
//                 //write_log('check_note');
//                 //  write_log($check_note[0]['note']);

//                 $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
//                 $content .= '<div class="fl-post-grid-image prod_like_wrap">';

//                 $image = swatch_image_product_thumbnail(get_the_ID(), '222', '222');

//                 $sku = get_post_meta(get_the_ID(), 'sku', true);
//                 $manufacturer = get_post_meta(get_the_ID(), 'manufacturer', true);

//                 $content .= '<a href="' . get_permalink() . '" title="' . get_the_title() . '"><img crossOrigin="Anonymous" class="list-pro-image" src="' . $image . '" alt="' . get_the_title() . '" /></a>';

//                 $content .= '<div class="favButtons button-wrapper">
//                     <a class="button fl-button" href="' . get_permalink() . '" title="' . get_the_title() . '">VIEW PRODUCT</a>';
//                 // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
//                 if ($check_note[0]['note'] == '' || $check_note[0]['note'] == null) {
//                     $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="' . get_the_ID() . '">ADD A NOTE</a>';
//                 }
//                 $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="' . get_the_ID() . '" class="remove-parent" data-user_id="' . get_current_user_id() . '" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

//                 if ($check_note[0]['note'] != '' || $check_note[0]['note'] != null) {
//                     $content .= '<a class="view_note_fav" data-note="' . $check_note[0]['note'] . '" id="view_note_fun" href="javascript:void(0)" data-productid="' . get_the_ID() . '"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>';
//                     $content .= '<div id="' . get_the_ID() . '" style="display:none">' . $check_note[0]['note'] . '</div>';
//                 }
//                 $content .= '</div></div> </div>';
//                 $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
//                 $content .= '<h4><span>' . get_the_title() . '</span></h4>';
//                 $content .= '</div>';

//                 //wpfp_remove_favorite_link(get_the_ID());
//                 $content .= "</div></div>";
//             endwhile;
//             wp_reset_postdata();
//             $content .= "</div></div>";
//         }

//         $content .= '</div></div>';


//         echo $content;

//         wp_die();
//     }
// }



// add_action('wp_ajax_add_note_form', 'add_note_form');
// add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

// function add_note_form()
// {
//     global $wpdb;

//     $fav_sql = 'UPDATE wp_favorite_posts SET note = "' . $_POST['note'] . '" WHERE user_id = ' . get_current_user_id() . ' and product_id = ' . $_POST['addnote_productid'] . '';

//     $check_fav = $wpdb->get_results($fav_sql);

//     echo $fav_sql;
// }


// Instock code start here 


add_filter('gform_pre_render_16', 'populate_product_color');
add_filter('gform_pre_validation_16', 'populate_product_color');
add_filter('gform_pre_submission_filter_16', 'populate_product_color');
add_filter('gform_admin_pre_render_16', 'populate_product_color');

add_filter('gform_pre_render_17', 'populate_product_color');
add_filter('gform_pre_validation_17', 'populate_product_color');
add_filter('gform_pre_submission_filter_17', 'populate_product_color');
add_filter('gform_admin_pre_render_17', 'populate_product_color');

add_filter('gform_pre_render_18', 'populate_product_color');
add_filter('gform_pre_validation_18', 'populate_product_color');
add_filter('gform_pre_submission_filter_18', 'populate_product_color');
add_filter('gform_admin_pre_render_18', 'populate_product_color');


function populate_product_color($form)
{

    foreach ($form['fields'] as &$field) {
        if ($field->id == 2) {

            $current_sku = get_the_content();
            $post_type = get_post_type();

            $brandmapping = array(
                "carpeting" => "carpet",
                "hardwood_catalog" => "hardwood",
                "laminate_catalog" => "laminate",
                "luxury_vinyl_tile" => "lvt",
                "tile_catalog" => "tile",
                "instock_carpet" => "carpet",
                "instock_hardwood" => "hardwood",
                "instock_laminate" => "laminate",
                "instock_lvt" => "lvt",
                "instock_tile" => "tile",
            );

            $current_category = $brandmapping[$post_type];

            $api_url_first = 'https://ajrosecarpets.com/wp-json/products/v1/list?sku=' .  $current_sku . '&category=' . $current_category . '&pageName=pdp';
            $response_first = wp_remote_get($api_url_first);

            if (is_wp_error($response_first)) {
                error_log('API Request Failed: ' . $response_first->get_error_message());
                continue;
            }

            $content_first = wp_remote_retrieve_body($response_first);
            $current_product_first = json_decode($content_first, true);

            $products_group_by = $current_product_first['products_group_by'];
            if (is_array($products_group_by) && !empty($products_group_by)) {
                $collection_name = key($products_group_by);
                $first_group_products = $products_group_by[$collection_name];
                $first_product = $first_group_products[0];
            }

            $current_brand_collection_name =  $first_product['brand_collection'];
            $instock =  $first_product['in_stock'];

            $api_url = 'https://ajrosecarpets.com/wp-json/products/v1/list?brand_collection=' . $current_brand_collection_name . '&category=' . $current_category . '&in_stock=' . $instock;
            $response = wp_remote_get($api_url);

            if (is_wp_error($response)) {
                error_log('API Request Failed: ' . $response->get_error_message());
                continue;
            }

            $body = wp_remote_retrieve_body($response);
            $data = json_decode($body, true);

            $products_group_by = $data['products_group_by'];

            $colors = [];
            $current_product_color = '';
            if ($current_brand_collection_name && isset($products_group_by[$current_brand_collection_name])) {
                foreach ($products_group_by[$current_brand_collection_name] as $product) {
                    if (isset($product['name']) && !in_array($product['name'], $colors)) {
                        $colors[] = $product['name'];
                    }

                    if ($product['sku'] == $current_sku) {
                        $current_product_color = $product['name'];
                    }
                }

                $field->choices = [];
                foreach ($colors as $color) {
                    $choice = ['text' => $color, 'value' => $color];

                    if ($color === $current_product_color) {
                        $choice['isSelected'] = true;
                    }

                    $field->choices[] = $choice;
                }
            }
        }
    }

    return $form;
}



add_action('wp_ajax_nopriv_get_colorlisting', 'get_colorlisting_function');
add_action('wp_ajax_get_colorlisting', 'get_colorlisting_function');
function get_colorlisting_function()
{

    global $wpdb;
    $flooringtype = get_post_type($_POST['product_id']);
    $collection =  get_post_meta($_POST['product_id'], 'collection', true);
    $satur = array('Masland', 'Dixie Home');

    if ($collection != NULL) {

        if ($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

            $familycolor = get_post_meta($_POST['product_id'], 'style', true);
            $key = 'style';
        } else {

            $familycolor = $collection;
            $key = 'collection';
        }
    } else {

        if (in_array(get_post_meta($_POST['product_id'], 'brand', true), $satur)) {
            $familycolor = get_post_meta($_POST['product_id'], 'design', true);
            $key = 'design';
        }
    }

    $args = array(
        'post_type'      => $flooringtype,
        'posts_per_page' => -1,
        'post_status'    => 'publish',
        'meta_query'     => array(
            array(
                'key'     => $key,
                'value'   => $familycolor,
                'compare' => '='
            ),
            array(
                'key' => 'swatch_image_link',
                'value' => '',
                'compare' => '!='
            )
        )
    );

    $the_query = new WP_Query($args);


    $choices = array(); // Set up blank array


    // loop over each select item at add value/option to $choices array

    while ($the_query->have_posts()) {
        $the_query->the_post();

        $image = swatch_image_product_thumbnail(get_the_ID(), '222', '222');

        $choices[] = array(get_post_meta(get_the_ID(), 'color', true) => $image);
    }
    wp_reset_postdata();

    //  write_log($choices);

    echo  json_encode($choices);

    wp_die();
}




// add_filter('gform_pre_render_16', 'populate_product_orderlocation_form');
// add_filter('gform_pre_validation_16', 'populate_product_orderlocation_form');
// add_filter('gform_pre_submission_filter_16', 'populate_product_orderlocation_form');
// add_filter('gform_admin_pre_render_16', 'populate_product_orderlocation_form');

// add_filter('gform_pre_render_17', 'populate_product_orderlocation_form');
// add_filter('gform_pre_validation_17', 'populate_product_orderlocation_form');
// add_filter('gform_pre_submission_filter_17', 'populate_product_orderlocation_form');
// add_filter('gform_admin_pre_render_17', 'populate_product_orderlocation_form');

// add_filter('gform_pre_render_18', 'populate_product_orderlocation_form');
// add_filter('gform_pre_validation_18', 'populate_product_orderlocation_form');
// add_filter('gform_pre_submission_filter_18', 'populate_product_orderlocation_form');
// add_filter('gform_admin_pre_render_18', 'populate_product_orderlocation_form');


// function populate_product_orderlocation_form($form)
// {

//     foreach ($form['fields'] as &$field) {

//         // Only populate field ID 12
//         if ($field['id'] == 15) {

//             $args = array(
//                 'post_type'      => 'store-locations',
//                 'posts_per_page' => -1,
//                 'post_status'    => 'publish'
//             );

//             $locations =  get_posts($args);

//             $choices = array();

//             foreach ($locations as $location) {


//                 $title = get_the_title($location->ID);

//                 $choices[] = array('text' => $title, 'value' => $title);
//             }
//             wp_reset_postdata();

//             //write_log($choices);

//             // Set placeholder text for dropdown
//             $field->placeholder = 'Choose Location --';

//             // Set choices from array of ACF values
//             $field->choices = $choices;
//         }
//     }
//     return $form;
// }

add_filter('gform_pre_render_21', 'populate_product_location_form');
add_filter('gform_pre_validation_21', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_21', 'populate_product_location_form');
add_filter('gform_admin_pre_render_21', 'populate_product_location_form');

add_filter('gform_pre_render_4', 'populate_product_location_form');
add_filter('gform_pre_validation_4', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_4', 'populate_product_location_form');
add_filter('gform_admin_pre_render_4', 'populate_product_location_form');

add_filter('gform_pre_render_20', 'populate_product_location_form');
add_filter('gform_pre_validation_20', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_20', 'populate_product_location_form');
add_filter('gform_admin_pre_render_20', 'populate_product_location_form');

add_filter('gform_pre_render_29', 'populate_product_location_form');
add_filter('gform_pre_validation_29', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_29', 'populate_product_location_form');
add_filter('gform_admin_pre_render_29', 'populate_product_location_form');

add_filter('gform_pre_render_26', 'populate_product_location_form');
add_filter('gform_pre_validation_26', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_26', 'populate_product_location_form');
add_filter('gform_admin_pre_render_26', 'populate_product_location_form');

add_filter('gform_pre_render_6', 'populate_product_location_form');
add_filter('gform_pre_validation_6', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_6', 'populate_product_location_form');
add_filter('gform_admin_pre_render_6', 'populate_product_location_form');

add_filter('gform_pre_render_16', 'populate_product_location_form');
add_filter('gform_pre_validation_16', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_16', 'populate_product_location_form');
add_filter('gform_admin_pre_render_16', 'populate_product_location_form');

add_filter('gform_pre_render_17', 'populate_product_location_form');
add_filter('gform_pre_validation_17', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_17', 'populate_product_location_form');
add_filter('gform_admin_pre_render_17', 'populate_product_location_form');

add_filter('gform_pre_render_19', 'populate_product_location_form');
add_filter('gform_pre_validation_19', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_19', 'populate_product_location_form');
add_filter('gform_admin_pre_render_19', 'populate_product_location_form');

add_filter('gform_pre_render_22', 'populate_product_location_form');
add_filter('gform_pre_validation_22', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_22', 'populate_product_location_form');
add_filter('gform_admin_pre_render_22', 'populate_product_location_form');

add_filter('gform_pre_render_23', 'populate_product_location_form');
add_filter('gform_pre_validation_23', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_23', 'populate_product_location_form');
add_filter('gform_admin_pre_render_23', 'populate_product_location_form');

add_filter('gform_pre_render_24', 'populate_product_location_form');
add_filter('gform_pre_validation_24', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_24', 'populate_product_location_form');
add_filter('gform_admin_pre_render_24', 'populate_product_location_form');

add_filter('gform_pre_render_25', 'populate_product_location_form');
add_filter('gform_pre_validation_25', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_25', 'populate_product_location_form');
add_filter('gform_admin_pre_render_25', 'populate_product_location_form');

add_filter('gform_pre_render_18', 'populate_product_location_form');
add_filter('gform_pre_validation_18', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_18', 'populate_product_location_form');
add_filter('gform_admin_pre_render_18', 'populate_product_location_form');

add_filter('gform_pre_render_27', 'populate_product_location_form');
add_filter('gform_pre_validation_27', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_27', 'populate_product_location_form');
add_filter('gform_admin_pre_render_27', 'populate_product_location_form');

add_filter('gform_pre_render_11', 'populate_product_location_form');
add_filter('gform_pre_validation_11', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_11', 'populate_product_location_form');
add_filter('gform_admin_pre_render_11', 'populate_product_location_form');

add_filter('gform_pre_render_14', 'populate_product_location_form');
add_filter('gform_pre_validation_14', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_14', 'populate_product_location_form');
add_filter('gform_admin_pre_render_14', 'populate_product_location_form');

add_filter('gform_pre_render_28', 'populate_product_location_form');
add_filter('gform_pre_validation_28', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_28', 'populate_product_location_form');
add_filter('gform_admin_pre_render_28', 'populate_product_location_form');

add_filter('gform_pre_render_10', 'populate_product_location_form');
add_filter('gform_pre_validation_10', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_10', 'populate_product_location_form');
add_filter('gform_admin_pre_render_10', 'populate_product_location_form');

add_filter('gform_pre_render_30', 'populate_product_location_form');
add_filter('gform_pre_validation_30', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_30', 'populate_product_location_form');
add_filter('gform_admin_pre_render_30', 'populate_product_location_form');

add_filter('gform_pre_render_35', 'populate_product_location_form');
add_filter('gform_pre_validation_35', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_35', 'populate_product_location_form');
add_filter('gform_admin_pre_render_35', 'populate_product_location_form');

add_filter('gform_pre_render_3', 'populate_product_location_form');
add_filter('gform_pre_validation_3', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_3', 'populate_product_location_form');
add_filter('gform_admin_pre_render_3', 'populate_product_location_form');

add_filter('gform_pre_render_32', 'populate_product_location_form');
add_filter('gform_pre_validation_32', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_32', 'populate_product_location_form');
add_filter('gform_admin_pre_render_32', 'populate_product_location_form');

add_filter('gform_pre_render_33', 'populate_product_location_form');
add_filter('gform_pre_validation_33', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_33', 'populate_product_location_form');
add_filter('gform_admin_pre_render_33', 'populate_product_location_form');

//dd_filter('gform_pre_render_43', 'populate_product_location_form');
//add_filter('gform_pre_validation_43', 'populate_product_location_form');
//add_filter('gform_pre_submission_filter_43', 'populate_product_location_form');
//add_filter('gform_admin_pre_render_43', 'populate_product_location_form');

add_filter('gform_pre_render_43', 'populate_product_location_form');
add_filter('gform_pre_validation_43', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_43', 'populate_product_location_form');
add_filter('gform_admin_pre_render_43', 'populate_product_location_form');

add_filter('gform_pre_render_46', 'populate_product_location_form');
add_filter('gform_pre_validation_46', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_46', 'populate_product_location_form');
add_filter('gform_admin_pre_render_46', 'populate_product_location_form');

add_filter('gform_pre_render_41', 'populate_product_location_form');
add_filter('gform_pre_validation_41', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_41', 'populate_product_location_form');
add_filter('gform_admin_pre_render_41', 'populate_product_location_form');


add_filter('gform_pre_render_41', 'populate_product_location_form');
add_filter('gform_pre_validation_41', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_41', 'populate_product_location_form');
add_filter('gform_admin_pre_render_41', 'populate_product_location_form');

add_filter('gform_pre_render_38', 'populate_product_location_form');
add_filter('gform_pre_validation_38', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_38', 'populate_product_location_form');
add_filter('gform_admin_pre_render_38', 'populate_product_location_form');

add_filter('gform_pre_render_47', 'populate_product_location_form');
add_filter('gform_pre_validation_47', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_47', 'populate_product_location_form');
add_filter('gform_admin_pre_render_47', 'populate_product_location_form');

function populate_product_location_form($form)
{

    foreach ($form['fields'] as &$field) {

        // Only populate field ID 12
        if ($field->type != 'select' || strpos($field->cssClass, 'populate-store') === false) {
            continue;
        }

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );

        $locations =  get_posts($args);

        $choices = array();
        $choicesFilter = array();


        array_push($choicesFilter, array('order' => 0, 'loc' => 'Shop at Home'));

        foreach ($locations as $location) {

            $title = get_the_title($location->ID);
            $orderID = get_post_meta($location->ID, 'list_order', true);

            if ($title != "Shop From Home") {
                array_push($choicesFilter, array('order' => $orderID, 'loc' => $title));
            }
        }

        $orders = array_column($choicesFilter, 'order');
        array_multisort($orders, SORT_ASC, $choicesFilter);

        foreach ($choicesFilter as $location) {
            $choices[] = array('text' => $location['loc'], 'value' => $location['loc']);
        }



        wp_reset_postdata();



        // Set placeholder text for dropdown
        $field->placeholder = 'Preferred Location';

        // Set choices from array of ACF values
        $field->choices = $choices;
    }
    return $form;
}




//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter("wpseo_breadcrumb_links", "override_yoast_breadcrumb_pdp_page");

function override_yoast_breadcrumb_pdp_page($links)
{
    $post_type = get_post_type();

    if (is_singular('instock_carpet')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/',
            'text' => 'In-Stock',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/in-stock-carpet/',
            'text' => 'Instock Carpet Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    if (is_singular('instock_hardwood')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/',
            'text' => 'In-Stock',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/in-stock-hard-surface/',
            'text' => 'Instock Hard Surface Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }
    if (is_singular('instock_lvt')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/',
            'text' => 'In-Stock',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/in-stock-hard-surface/',
            'text' => 'Instock Hard Surface Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }


    return $links;
}





add_filter('gform_field_validation_39_32', 'custom_validation', 20, 4);
add_filter('gform_field_validation_42_32', 'custom_validation', 20, 4);
//add_filter( 'gform_field_validation_13_15', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_33_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_35_20', 'custom_validation', 20, 4 );
// add_filter( 'gform_field_validation_36_20', 'custom_validation', 20, 4 );

function custom_validation($result, $value, $form, $field)
{
    if ($result['is_valid']) {
        $acceptable_zips = array(
            '01432',
            '01434',
            '01450',
            '01451',
            '01460',
            '01463',
            '01464',
            '01467',
            '01503',
            '01504',
            '01505',
            '01510',
            '01516',
            '01519',
            '01523',
            '01525',
            '01529',
            '01532',
            '01534',
            '01536',
            '01545',
            '01560',
            '01568',
            '01569',
            '01581',
            '01588',
            '01590',
            '01701',
            '01702',
            '01719',
            '01720',
            '01721',
            '01730',
            '01731',
            '01740',
            '01741',
            '01742',
            '01745',
            '01746',
            '01747',
            '01748',
            '01749',
            '01752',
            '01754',
            '01756',
            '01757',
            '01760',
            '01770',
            '01772',
            '01773',
            '01775',
            '01776',
            '01778',
            '01801',
            '01803',
            '01810',
            '01821',
            '01824',
            '01826',
            '01827',
            '01830',
            '01832',
            '01833',
            '01834',
            '01835',
            '01840',
            '01841',
            '01843',
            '01844',
            '01845',
            '01850',
            '01851',
            '01852',
            '01854',
            '01860',
            '01862',
            '01863',
            '01864',
            '01867',
            '01876',
            '01879',
            '01880',
            '01886',
            '01887',
            '01890',
            '01901',
            '01902',
            '01904',
            '01905',
            '01906',
            '01907',
            '01908',
            '01913',
            '01915',
            '01921',
            '01922',
            '01923',
            '01929',
            '01930',
            '01938',
            '01940',
            '01944',
            '01945',
            '01949',
            '01950',
            '01951',
            '01952',
            '01960',
            '01966',
            '01969',
            '01970',
            '01982',
            '01983',
            '01984',
            '01985',
            '02019',
            '02021',
            '02025',
            '02026',
            '02030',
            '02032',
            '02035',
            '02038',
            '02043',
            '02047',
            '02048',
            '02050',
            '02052',
            '02053',
            '02054',
            '02056',
            '02061',
            '02062',
            '02066',
            '02067',
            '02071',
            '02072',
            '02081',
            '02090',
            '02093',
            '02108',
            '02109',
            '02111',
            '02110',
            '02113',
            '02114',
            '02115',
            '02116',
            '02118',
            '02119',
            '02120',
            '02121',
            '02122',
            '02124',
            '02125',
            '02126',
            '02127',
            '02128',
            '02129',
            '02130',
            '02131',
            '02132',
            '02135',
            '02136',
            '02138',
            '02139',
            '02140',
            '02141',
            '02142',
            '02143',
            '02144',
            '02145',
            '02148',
            '02149',
            '02150',
            '02151',
            '02152',
            '02155',
            '02163',
            '02169',
            '02170',
            '02171',
            '02176',
            '02180',
            '02184',
            '02186',
            '02188',
            '02189',
            '02190',
            '02191',
            '02199',
            '02203',
            '02210',
            '02215',
            '02301',
            '02302',
            '02322',
            '02324',
            '02330',
            '02332',
            '02333',
            '02338',
            '02339',
            '02341',
            '02343',
            '02346',
            '02347',
            '02351',
            '02356',
            '02357',
            '02359',
            '02360',
            '02364',
            '02366',
            '02367',
            '02368',
            '02370',
            '02375',
            '02379',
            '02382',
            '02420',
            '02421',
            '02445',
            '02446',
            '02451',
            '02452',
            '02453',
            '02457',
            '02458',
            '02459',
            '02460',
            '02461',
            '02462',
            '02464',
            '02465',
            '02466',
            '02467',
            '02468',
            '02472',
            '02474',
            '02476',
            '02478',
            '02481',
            '02482',
            '02492',
            '02493',
            '02494',
            '02538',
            '02558',
            '02571',
            '02576',
            '02702',
            '02703',
            '02715',
            '02717',
            '02718',
            '02719',
            '02720',
            '02721',
            '02723',
            '02724',
            '02725',
            '02726',
            '02738',
            '02739',
            '02740',
            '02743',
            '02745',
            '02746',
            '02747',
            '02760',
            '02762',
            '02763',
            '02764',
            '02766',
            '02767',
            '02769',
            '02770',
            '02777',
            '02779',
            '02780',
            '02790',
            '02878'
        );

        // $zip_value = rgar( $value, $field->id . '20' );
        // var_dump($value,$zip_value, $acceptable_zips);
        if (!in_array($value, $acceptable_zips)) {
            $result['is_valid'] = false;
            $result['message']  = 'Unfortunately we are unable to schedule an appointment in your area at this time.';
        } else {
            $result['is_valid'] = true;
            $result['message']  = 'Good News! We are able to schedule an appointment to your area.';
        }
    }

    return $result;
}


// wp_deregister_script( 'jquery' );
// wp_register_script(
//     'jquery',
//     'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js', 
//     null, // Dependencies - none
//     null  // Version - use null to prevent WP adding version argument "?ver" in URL
// );



//check out flooring FUnctionality

// add_action('wp_ajax_nopriv_check_out_flooring', 'check_out_flooring');
// add_action('wp_ajax_check_out_flooring', 'check_out_flooring');

// function check_out_flooring()
// {

//     $arg = array();
//     if ($_POST['product'] == 'carpeting') {

//         $posttype = array('carpeting');
//     } elseif ($_POST['product'] == 'hardwood_catalog,laminate_catalog,luxury_vinyl_tile') {

//         $posttype = array('hardwood_catalog', 'laminate_catalog', 'luxury_vinyl_tile');
//     } elseif ($_POST['product'] == 'tile_catalog' || $_POST['product'] == 'tile') {

//         // $posttype = array('tile_catalog','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','carpeting');
//         $posttype = array('tile_catalog');
//     }

//     $parameters =  explode(",", $_POST['imp_thing']);

//     foreach ($parameters as $para) {
//         if ($para == 'hypoallergenic') {

//             $arg['hypoallergenic'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50
//             );
//         } elseif ($para == 'diy_friendly') {

//             $arg['diy_friendly'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     'relation' => 'OR',
//                     array(
//                         'key'     => 'installation_facet',
//                         'value'   => 'Locking',
//                         'compare' => '=',

//                     ),
//                     array(
//                         'key'     => 'installation_method',
//                         'value'   => 'Locking',
//                         'compare' => '=',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'aesthetic_beauty') {

//             $arg['aesthetic_beauty'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     'relation' => 'OR',
//                     array(
//                         'key'     => 'brand',
//                         'value'   => 'Anderson Tuftex',
//                         'compare' => '=',

//                     ),
//                     array(
//                         'key'     => 'brand',
//                         'value'   => 'Karastan',
//                         'compare' => '=',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'eco_friendly') {
//             $arg['eco_friendly'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     array(
//                         'key'     => 'lifestyle',
//                         'value'   => 'Eco',
//                         'compare' => 'LIKE',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'pet_friendly') {

//             $arg['pet_friendly'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     'relation' => 'OR',
//                     array(
//                         'key'     => 'lifestyle',
//                         'value'   => 'Pet',
//                         'compare' => 'LIKE',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'easy_to_clean') {

//             $arg['easy_to_clean'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     'relation' => 'OR',
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'SmartStrand',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'R2x',
//                         'compare' => 'Like',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'durability') {

//             $arg['durability'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     'relation' => 'OR',
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'SmartStrand',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'R2x',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'Stainmaster',
//                         'compare' => 'Like',

//                     ),
//                 ),
//             );
//         } elseif ($para == '') {

//             $arg['colors'] = array(
//                 'post_type'      => array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'carpeting', 'tile'),
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//             );
//         } elseif ($para == 'stain_resistant') {

//             $arg['stain_resistant'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     'relation' => 'OR',
//                     array(
//                         'key'     => 'lifestyle',
//                         'value'   => 'Stain Resistant',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'R2x',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'Stainmaster',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => 'SmartStrand',
//                         'compare' => 'Like',

//                     ),
//                     array(
//                         'key'     => 'Collection',
//                         'value'   => 'Bellera',
//                         'compare' => 'Like',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'budget_friendly') {

//             $arg['budget_friendly'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50,
//                 'meta_query'     => array(
//                     array(
//                         'key'     => 'fiber',
//                         'value'   => array('SmartStrand', 'Stainmaster', 'Bellera', 'Pet R2x'),
//                         'compare' => 'IN',

//                     ),
//                 ),
//             );
//         } elseif ($para == 'color') {

//             $arg['color'] = array(
//                 'post_type'      => $posttype,
//                 'post_status'    => 'publish',
//                 'orderby'        => 'rand',
//                 'posts_per_page' => 50
//             );
//         }
//     }

//     $i = 1;

//     $find_posts = array();
//     foreach ($parameters as $para) {
//         $wp_query[$i] = new WP_Query($arg[$para]);


//         $find_posts = array_merge($find_posts, $wp_query[$i]->posts);

//         $i++;
//     }

//     //print_r($find_posts);
//     if (!$find_posts) {

//         $args = array(
//             'post_type'      => $posttype,
//             'post_status'    => 'publish',
//             'posts_per_page' => 50,
//             'orderby'        => 'rand'
//         );
//         $find_posts   = new WP_Query($args);
//         // var_dump($args , $find_posts);
//         $find_posts = $find_posts->posts;
//     }

//     if ($find_posts) {

//         $content = "";

//         foreach ($find_posts as $post) {

//             $image = swatch_image_product_thumbnail($post->ID, '222', '222');
//             $sku = get_field("sku", $post->ID);

//             $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
//                         <div class="card">
//                             <div class="productImgWrap">
//                                 <img src="' . $image . '" alt="Product Image" style="width:100%!important">
//                             </div>
//                             <a href="javascript:void(0)" class="closebtn"  >&times;</a>
//                             <div class="pro_title">
//                                 <h4>' . get_field("collection", $post->ID) . '</h4> 
//                                 <h3>' . get_field("color", $post->ID) . '</h3>
//                                 <p class="brand"><small><i>' . get_field("brand", $post->ID) . '</i></small></p>
//                                 <!-- <a class="orderSamplebtn fl-button btnAddAction cart-action"  href="javascript:void(0)">
//                                 Order sample
//                                 </a> -->
//                                 <a class="prodLink" href="' . get_permalink($post->ID) . '">See info</a>
//                             </div>

//                         </div>
//                     </div>';
//         }

//         $content .= '<div class="text-center buttonWrap" data-search="' . $_POST['imp_thing'] . '">
//                     <a class="button" href="javascript:void(0)">view all results</a>
//                     <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
//                 </div>';
//     }

//     echo $content;
//     wp_die();
// }


// Instock compare product FUnctionality
// if (!wp_next_scheduled('instock_compare_csv_onsite_products_cronjob')) {

//     wp_schedule_event(time() +  17800, 'daily', 'instock_compare_csv_onsite_products_cronjob');
// }

// add_action('instock_compare_csv_onsite_products_cronjob', 'instock_compare_csv_onsite_products');

// function instock_compare_csv_onsite_products()
// {

//     $instock_post = array(
//         "instock_carpet" => "carpeting",
//         "instock_hardwood" => "hardwood_catalog",
//         "instock_lvt" => "luxury_vinyl_tile"
//     );

//     $instock_parent_post = array(
//         // "carpeting",
//         //  "hardwood_catalog",
//         "luxury_vinyl_tile"
//     );

//     write_log('compare_csv_onsite_products Started');

//     foreach ($instock_parent_post as $parent_post) {

//         global $wpdb;
//         $product_table = $wpdb->prefix . "posts";
//         $table_redirect = $wpdb->prefix . 'redirection_items';
//         $table_group = $wpdb->prefix . 'redirection_groups';
//         $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
//         $redirect_group =  $datum[0]->id;
//         $table_posts = $wpdb->prefix . 'posts';
//         $table_meta = $wpdb->prefix . 'postmeta';

//         $brandmapping = array(
//             "carpet" => "carpeting",
//             "hardwood" => "hardwood_catalog",
//             "laminate" => "laminate_catalog",
//             "lvt" => "luxury_vinyl_tile",
//             "tile" => "tile_catalog",
//             "waterproof" => "solid_wpc_waterproof"
//         );

//         $posttype = array_search($parent_post, $brandmapping);

//         $upload = wp_upload_dir();
//         $upload_dir = $upload['basedir'];
//         $upload_dir = $upload_dir . '/sfn-data';

//         $permfile = $upload_dir . '/all_' . $parent_post . '.json';
//         $res = SOURCEURL . get_option('SITE_CODE') . '/www/' . $posttype . '.json?' . SFN_STATUS_PARAMETER;
//         write_log($res);

//         $tmpfile = download_url($res, $timeout = 900);

//         if (is_file($tmpfile)) {
//             copy($tmpfile, $permfile);
//             unlink($tmpfile);
//         }

//         $file_name = 'all_' . $parent_post . '.json';
//         $upload_dir = wp_upload_dir();
//         $i = 0;
//         $upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name;

//         $strJsonFileContents = file_get_contents($upload_dir['basedir'] . '/' . 'sfn-data/' . $file_name);

//         $handle_data = str_replace('&quot;', '"', $strJsonFileContents);

//         $handle = json_decode(($handle_data), true);

//         $delete_posttype = array_search($parent_post, $instock_post);

//         $hardwood = $wpdb->get_results("SELECT ID FROM $table_posts WHERE post_type = '$delete_posttype'");


//         write_log("SELECT ID FROM $table_posts WHERE post_type = '$delete_posttype'");

//         //  exit;

//         foreach ($hardwood as $hard) {

//             write_log('Search for----' . $hard->ID);

//             $sku = get_post_meta($hard->ID, 'sku', true);

//             $id = searchForsku($hard->ID, $sku, $handle);



//             if ($id != 'found sku in data') {


//                 $source_url = wp_make_link_relative(get_the_guid($id));
//                 $match_url = rtrim($source_url, '/');

//                 write_log($source_url);

//                 $post_type =  get_post_type($id);

//                 $urlmapping = array(
//                     "/flooring/carpet/products/" => "carpeting",
//                     "/flooring/hardwood/products/" => "hardwood_catalog",
//                     "/flooring/laminate/products/" => "laminate_catalog",
//                     "/flooring/" => "luxury_vinyl_tile",
//                     "/flooring/tile/products/" => "tile_catalog",
//                     "/flooring/waterproof/" => "solid_wpc_waterproof"
//                 );

//                 $destination_url = array_search($post_type, $urlmapping);

//                 write_log($destination_url);

//                 $data = array(
//                     "url" => $source_url,
//                     "match_url" => $match_url,
//                     "match_data" => "",
//                     "action_code" => "301",
//                     "action_type" => "url",
//                     "action_data" => $destination_url,
//                     "match_type" => "url",
//                     "title" => $sku,
//                     "regex" => "true",
//                     "group_id" => '1',
//                     "position" => "1",
//                     "last_access" => current_time('mysql'),
//                     "status" => "enabled"
//                 );



//                 $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

//                 $wpdb->insert($table_redirect, $data, $format);

//                 write_log('Product need to delete');

//                 //  exit;

//                 wp_delete_post($id);
//             }
//         }
//     }

//     write_log('compare_csv_onsite_products ended');
// }

// function set_email_campaigns_fun()
// {
//     $forms = GFAPI::get_forms();

//     var_dump($forms);
//     die();
//     if (is_array($forms) && count($forms) > 0) {
//         foreach ($forms as $f) {
//             $form = GFAPI::get_form($f->ID);
//             $new_field_id = GFFormsModel::get_next_field_id($form['fields']);

//             $properties['type'] = 'hidden';

//             $field = GF_Fields::create($properties);
//             $properties['id'] = $new_field_id;
//             $properties['label'] = 'PROMOCODE';

//             $field->id = $new_field_id;
//             $field->label = 'PROMOCODE';

//             $form['fields'][] = $field;

//             GFAPI::update_form($form);
//         }
//     }
// }
// add_action('gform_loaded', 'set_email_campaigns_fun', 10, 0);


// add_filter( 'gform_field_validation_13_15', 'zip_validation', 20, 4 );

// function zip_validation( $result, $value, $form, $field ) {
// 	if ( $result['is_valid'] ) {
//     $acceptable_zips = array(
//       '01432','01434','01450','01451','01460','01463','01464','01467','01503','01504','01505','01510','01516','01519','01523','01525',	
//       '01529','01532','01534','01536','01545','01560','01568','01569','01581','01588','01590','01701','01702','01719','01720','01721',
//       '01730','01731','01740','01741','01742','01745','01746','01747','01748','01749','01752','01754','01756','01757',
//       '01760','01770','01772','01773','01775','01776','01778','01801','01803','01810','01821','01824','01826','01827','01830','01832',
//       '01833','01834','01835','01840','01841','01843','01844','01845','01850','01851','01852','01854','01860','01862','01863','01864',
//       '01867','01876','01879','01880','01886','01887','01890','01901','01902','01904','01905','01906','01907','01908','01913','01915',
//       '01921','01922','01923','01929','01930','01938','01940','01944','01945','01949','01950','01951','01952','01960','01966','01969',
//       '01970','01982','01983','01984','01985','02019','02021','02025','02026','02030','02032','02035','02038','02043','02047','02048',
//       '02050','02052','02053','02054','02056','02061','02062','02066','02067','02071','02072','02081','02090','02093','02108','02109',
//       '02110','02111','02113','02114','02115','02116','02118','02119','02120','02121','02122','02124','02125','02126','02127','02128',
//       '02129','02130','02131','02132','02134','02135','02136','02138','02139','02140','02141','02142','02143','02144','02145','02148',
//       '02149','02150','02151','02152','02155','02163','02169','02170','02171','02176','02180','02184','02186','02188','02189','02190',
//       '02191','02199','02203','02210','02215','02301','02302','02322','02324','02330','02332','02333','02338','02339','02341','02343',
//       '02346','02347','02351','02356','02357','02359','02360','02364','02366','02367','02368','02370','02375','02379','02382','02420',
//       '02421','02445','02446','02451','02452','02453','02457','02458','02459','02460','02461','02462','02464','02465','02466','02467',
//       '02468','02472','02474','02476','02478','02481','02482','02492','02493','02494','02538','02558','02571','02576','02702','02703',
//       '02715','02717','02718','02719','02720','02721','02723','02724','02725','02726','02738','02739','02740','02743','02745','02746',
//       '02747','02760','02762','02763','02764','02766','02767','02769','02770','02777','02779','02780','02790','02878');

//     foreach ( $acceptable_zips as $zip ) {

//         if ( strpos ( $value , $zip ) !== FALSE ) {

//             $result['is_valid'] = true;
//             $result['message']  = 'Good News! We are able to reach to your area.';

//             break;

//         }else{

//             $result['is_valid'] = false;
//             $result['message']  = 'Unfortunately we are unable to schedule an appointment in your area at this time.'; 
//         }
//     }



// }

// return $result;
// }



//zip code 

add_filter('gform_field_validation_13_15', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_31_19', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_33_16', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_4_14', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_16_14', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_17_14', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_18_14', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_14_21', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_20_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_21_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_29_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_26_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_46_13', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_22_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_23_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_24_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_25_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_27_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_28_17', 'custom_zip_validation', 20, 4);
add_filter('gform_field_validation_30_18', 'custom_zip_validation', 20, 4);
// add_filter( 'gform_field_validation_6_17', 'custom_zip_validation', 20, 4 );
// add_filter( 'gform_field_validation_11_16', 'custom_zip_validation', 20, 4 );
// add_filter( 'gform_field_validation_10_17', 'custom_zip_validation', 20, 4 );


function custom_zip_validation($result, $value, $form, $field)
{
    if ($result['is_valid']) {
        $acceptable_zips = array(
            '01432',
            '01434',
            '01450',
            '01451',
            '01460',
            '01463',
            '01464',
            '01467',
            '01503',
            '01504',
            '01505',
            '01510',
            '01516',
            '01519',
            '01523',
            '01525',
            '01529',
            '01532',
            '01534',
            '01536',
            '01545',
            '01560',
            '01568',
            '01569',
            '01581',
            '01588',
            '01590',
            '01701',
            '01702',
            '01719',
            '01720',
            '01721',
            '01730',
            '01731',
            '01740',
            '01741',
            '01742',
            '01745',
            '01746',
            '01747',
            '01748',
            '01749',
            '01752',
            '01754',
            '01756',
            '01757',
            '01760',
            '01770',
            '01772',
            '01773',
            '01775',
            '01776',
            '01778',
            '01801',
            '01803',
            '01810',
            '01821',
            '01824',
            '01826',
            '01827',
            '01830',
            '01832',
            '01833',
            '01834',
            '01835',
            '01840',
            '01841',
            '01843',
            '01844',
            '01845',
            '01850',
            '01851',
            '01852',
            '01854',
            '01860',
            '01862',
            '01863',
            '01864',
            '01867',
            '01876',
            '01879',
            '01880',
            '01886',
            '01887',
            '01890',
            '01901',
            '01902',
            '01904',
            '01905',
            '01906',
            '01907',
            '01908',
            '01913',
            '01915',
            '01921',
            '01922',
            '01923',
            '01929',
            '01930',
            '01938',
            '01940',
            '01944',
            '01945',
            '01949',
            '01950',
            '01951',
            '01952',
            '01960',
            '01966',
            '01969',
            '01970',
            '01982',
            '01983',
            '01984',
            '01985',
            '02019',
            '02021',
            '02025',
            '02026',
            '02030',
            '02032',
            '02035',
            '02038',
            '02043',
            '02047',
            '02048',
            '02050',
            '02052',
            '02053',
            '02054',
            '02056',
            '02061',
            '02062',
            '02066',
            '02067',
            '02071',
            '02072',
            '02081',
            '02090',
            '02093',
            '02108',
            '02109',
            '02110',
            '02111',
            '02113',
            '02114',
            '02115',
            '02116',
            '02118',
            '02119',
            '02120',
            '02121',
            '02122',
            '02124',
            '02125',
            '02126',
            '02127',
            '02128',
            '02129',
            '02130',
            '02131',
            '02132',
            '02134',
            '02135',
            '02136',
            '02138',
            '02139',
            '02140',
            '02141',
            '02142',
            '02143',
            '02144',
            '02145',
            '02148',
            '02149',
            '02150',
            '02151',
            '02152',
            '02155',
            '02163',
            '02169',
            '02170',
            '02171',
            '02176',
            '02180',
            '02184',
            '02186',
            '02188',
            '02189',
            '02190',
            '02191',
            '02199',
            '02203',
            '02210',
            '02215',
            '02301',
            '02302',
            '02322',
            '02324',
            '02330',
            '02332',
            '02333',
            '02338',
            '02339',
            '02341',
            '02343',
            '02346',
            '02347',
            '02351',
            '02356',
            '02357',
            '02359',
            '02360',
            '02364',
            '02366',
            '02367',
            '02368',
            '02370',
            '02375',
            '02379',
            '02382',
            '02420',
            '02421',
            '02445',
            '02446',
            '02451',
            '02452',
            '02453',
            '02457',
            '02458',
            '02459',
            '02460',
            '02461',
            '02462',
            '02464',
            '02465',
            '02466',
            '02467',
            '02468',
            '02472',
            '02474',
            '02476',
            '02478',
            '02481',
            '02482',
            '02492',
            '02493',
            '02494',
            '02538',
            '02558',
            '02571',
            '02576',
            '02702',
            '02703',
            '02715',
            '02717',
            '02718',
            '02719',
            '02720',
            '02721',
            '02723',
            '02724',
            '02725',
            '02726',
            '02738',
            '02739',
            '02740',
            '02743',
            '02745',
            '02746',
            '02747',
            '02760',
            '02762',
            '02763',
            '02764',
            '02766',
            '02767',
            '02769',
            '02770',
            '02777',
            '02779',
            '02780',
            '02790',
            '02878'
        );

        foreach ($acceptable_zips as $zip) {

            if (strpos($value, $zip) !== FALSE) {

                $result['is_valid'] = true;
                $result['message']  = 'Good News! We are able to reach to your area.';

                break;
            } else {

                $result['is_valid'] = false;
                $result['message']  = 'The zip code you entered is out of our service area. Please give us a call if you need further assistance.';
            }
        }
    }

    return $result;
}


// add_filter('facetwp_template_html', function ($output, $class) {

//     $prod_list = $class->query;
//     ob_start();

//     if (isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1) {

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-instock-loop.php';

//     } else {

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-ajrose-carpets.php';
//     }

//     require_once $dir;
//     return ob_get_clean();
// }, 10, 2);

// remove_all_filters( 'facetwp_query_args');

// add_filter('facetwp_query_args', function ($query_args, $class) {

//     $query_args['posts_per_page'] = '12';
//   //print_r($query_args['post_type']);

//     if ((@$query_args['post_type'][1] != "instock_lvt") && (@$query_args['post_type'][0] != "instock_hardwood") ) {
//         $query_args['meta_key'] = 'collection';
//     }
//     if ((@$query_args['post_type'][1] == "instock_hardwood") && (@$query_args['post_type'][0] == "instock_lvt") ) {
//          add_filter('posts_groupby', 'query_group_by_filter');
//      }
//     if ((@$query_args['post_type'] != "fl-builder-template" && @$query_args['post_type'] != "post" && @$query_args['post_type'] != "page") &&  @wp_count_posts(@$query_args['post_type'])->publish > 1) {
//         add_filter('posts_groupby', 'query_group_by_filter');
//     }  elseif (is_array(@$query_args['post_type'])) {
//       // add_filter('posts_groupby', 'query_group_by_filter');
//     }

//     return $query_args;
// }, 10, 2);

//single instock post type template

// add_filter( 'single_template', 'childtheme_get_custom_post_type_template' );

// function childtheme_get_custom_post_type_template($single_template) {
//     global $post;

//     if ($post->post_type != 'post'){  

//         $single_template = get_stylesheet_directory().'/product-listing-templates/single-'.$post->post_type.'.php';        

//     }
//     return $single_template;
// }



function storelocation_dir_url($arg)
{

    $website = json_decode(get_option('website_json'));

    for ($i = 0; $i < count($website->locations); $i++) {
        $location_name = isset($website->locations[$i]->name) ? $website->locations[$i]->name : "";

        $location_ids = isset($website->locations[$i]->id) ? $website->locations[$i]->id : "";

        if ($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name != '') {


            if (in_array(trim($location_name), $arg) || in_array(trim($location_ids), $arg)) {

                if ($website->locations[$i]->name == 'Main' || $website->locations[$i]->name == "MAIN") {

                    $location_name = get_bloginfo('name');
                } else {

                    $location_name =  isset($website->locations[$i]->name) ? $website->locations[$i]->name . " " : "";
                }


                $location_address_url =  $location_name;
                $location_address_url  .= isset($website->locations[$i]->address) ? $website->locations[$i]->address . " " : "";
                $location_address_url .= isset($website->locations[$i]->city) ? $website->locations[$i]->city . " " : "";
                $location_address_url .= isset($website->locations[$i]->state) ? $website->locations[$i]->state . " " : "";
                $location_address_url .= isset($website->locations[$i]->postalCode) ? $website->locations[$i]->postalCode . " " : "";

                $locations .= 'https://maps.google.com/maps/?q=' . urlencode($location_address_url);
            }
        }
    }

    return $locations;
}
add_shortcode('storelocation_dir_url', 'storelocation_dir_url');

/**
 * Hide Draft Pages from the menu
 */
// function filter_draft_pages_from_menu_new($items, $args)
// {
//     foreach ($items as $ix => $obj) {
//         if ('draft' == get_post_status($obj->object_id)) {
//             unset($items[$ix]);
//         }
//     }
//     return $items;
// }
// add_filter('wp_nav_menu_objects', 'filter_draft_pages_from_menu_new', 10, 2);

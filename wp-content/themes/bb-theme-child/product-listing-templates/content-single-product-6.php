<div class="product-detail-layout-6 childtheme">
<?php
global $post;
$flooringtype = $post->post_type; 
$meta_values = get_post_meta( get_the_ID() );
$brand = $meta_values['brand'][0] ;
$sku = $meta_values['sku'][0];
$manufacturer = $meta_values['manufacturer'][0];
$image = swatch_image_product(get_the_ID(),'600','400');
$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');


$getcoupon_link = get_option('getcoupon_link');

if(get_option('salesbrand')!=''){
	$slide_brands = rtrim(get_option('salesbrand'), ",");
	$brandonsale = array_filter(explode(",",$slide_brands));
	$brandonsale = array_map('trim', $brandonsale);
}
?>
<?php
	$collection = $meta_values['collection'][0];    
	$satur = array('Masland','Dixie Home');  
	
if($collection != NULL ){
	
	if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

		$familycolor = $meta_values['style'][0];
		$key = 'style';

	}else{

		$familycolor = $meta_values['collection'][0];    
		$key = 'collection';
		
	}	
}else{	
	
	if (in_array($meta_values['brand'][0], $satur)){
		$familycolor = $meta_values['design'][0];    
		$key = 'design';
	  }

}

	$args = array(
		'post_type'      => $flooringtype,
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'meta_query'     => array(
			array(
				'key'     => $key,
				'value'   => $familycolor,
				'compare' => '='
			),
			array(
				'key' => 'swatch_image_link',
				'value' => '',
				'compare' => '!='
				)
		)
	);										

	$the_query = new WP_Query( $args );
	
?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back" itemprop="text">
			
				<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 col-sm-12 product-swatch">   
					

						<?php 
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images-2.php';
							include( $dir );
						?>
						
					
						<div class="clearfix"></div>
				</div>
				<div class="col-md-5 col-sm-12 product-box">
					<div class="row">
						<div class="col-md-6">
							<?php if(array_key_exists("parent_collection",$meta_values)){ ?>
							<h4><?php echo $meta_values['parent_collection'][0]; ?></h4>
							<?php } ?>

							<?php if(array_key_exists("collection",$meta_values)){?>
							<h1 class="fl-post-title" itemprop="name"><?php echo $meta_values['collection'][0]; ?></h1>
							<?php } ?>

							<?php if(array_key_exists("color",$meta_values)){ ?>
							<h2 class="fl-post-title" itemprop="name"><?php  echo $meta_values['color'][0]; ?></h2>
							<?php } ?>

							<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
							
							
								

						</div>							
						<div class=" col-md-6 text-right">
						<?php 					
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
							include_once( $dir );
						?>
						</div>
							<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-md-12">	
						<div class="product-colors">
								<ul>
									<li class="found"><?php echo $the_query ->found_posts; ?></li>
									<li class="colors">Colors Available</li>
								</ul>
							</div>

							<div id="product-colors">
								<?php
									$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider-1.php';
									include( $dir );
								?>
							</div>
						</div>	
					</div>		


					
					<div class="button-wrapper">

					<div class="dual-button">
						<a href="/contact-us/" class="button contact-btn">CONTACT US</a>	
						<?php if($pdp_get_finance != 1 || $pdp_get_finance == '' ){?>						
							<a href="<?php if($getfinancereplace ==1){ echo $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext ;}else{ echo 'Financing'; } ?></a>	
						<?php } ?>		
					</div>
						
					<?php if(get_option('getcouponbtn') == 1){ ?>
											<a href="<?php echo $getcoupon_link ;?>" target="_self" class="button alt getcoupon-btn" role="button" >GET COUPON</a>
            		<?php } ?>

				<?php  if($getcouponreplace == 1 && $getcouponreplaceurl !='' && $getcouponreplacetext !=''){ ?>

					<a href="<?php echo $getcouponreplaceurl ;?>" target="_self" class="button alt custompdpbtn getcoupon-btn" role="button">
					<?php if($getcouponreplace ==1){ echo $getcouponreplacetext ;}?>
					</a>

				<?php } ?>	
						
					
						
						<?php if (is_user_logged_in()) { 

					$fav_sql = 'SELECT * FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id = '. get_the_ID().'';

					$check_fav = $wpdb->get_results($fav_sql);         

					if(!empty($check_fav)) {
						$link_action = 'rem_fav';
						$icon_class = 'fa fa-heart';
						$text = "Remove From Favorite";    
					}else{
						$link_action = 'add_Fav';
						$icon_class = 'fa fa-heart-o';
						$text = "Add as Favorite"; 
					}
					?> 

					<a class="button alt fl-button pdpFavbutton <?php echo $link_action; ?>"  data-user="<?php echo get_current_user_id(); ?>" data-id="<?php echo get_the_ID(); ?>">
						<i class="<?php echo $icon_class; ?>" aria-hidden="true"  > </i>
						<span class="rf" style="color: #fff;">Remove From Favorite</span>
						<span class="af" style="color: #fff;">Add as Favorite</span>
					</a>

					<?php } ?>

					</div>						
						<div class="clearfix"></div>
				</div>
					<div class="clearfix"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="product-attributes-wrap">
							<?php 
								$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
								include( $dir );
							?>
						</div>		
				</div>
			</div>			

				<div class="clearfix"></div>
			
			
		</div>
	</article>
</div>
<?php
			$title = get_the_title();
			$last_date = date('Y') . '-12-31';

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'Brand','name'=>$brand), 
			'offers'=>array('@type'=>'offer','url'=> home_url(),'priceCurrency'=>'USD','price'=>'00', 'availability' => 'InStock','priceValidUntil'=>$last_date),'author'=>array('@type'=>'Organization','name'=>get_bloginfo()));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>	

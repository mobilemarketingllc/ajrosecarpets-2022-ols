<?php
global $post;
$flooringtype = $post->post_type; 
$meta_values = get_post_meta( get_the_ID() );
$brand = $meta_values['brand'][0] ;
$sku = $meta_values['sku'][0];
$color = $meta_values['color'][0];
$manufacturer = $meta_values['manufacturer'][0];
$room_image =  single_gallery_image_product(get_the_ID(),'1000','1600');
$room_image_mobile =  single_gallery_image_product(get_the_ID(),'400','400');

$image_600 = swatch_image_product(get_the_ID(),'600','400');

$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');

if(get_option('salesbrand')!=''){
	$slide_brands = rtrim(get_option('salesbrand'), ",");
	$brandonsale = array_filter(explode(",",$slide_brands));
	$brandonsale = array_map('trim', $brandonsale);
}


?>
</div></div></div>

<style type="text/css">
.breadcrumbs{ display:none;}
<?php if($room_image) {	?>
.product-banner-img{
	background-image:url('<?php echo $room_image; ?>')
}
@media(max-width:767px){
	.product-banner-img{
	background-image:url('<?php echo $room_image_mobile; ?>')
	}	
}
<?php } ?>
/* product slider query*/
<?php
											$collection = $meta_values['collection'][0];    
											$satur = array('Masland','Dixie Home');  
											
										if($collection != NULL ){
											
											if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

												$familycolor = $meta_values['style'][0];
												$key = 'style';

											}else{

												$familycolor = $meta_values['collection'][0];    
												$key = 'collection';
												
											}	

											
										}else{	
											
											if (in_array($meta_values['brand'][0], $satur)){
												$familycolor = $meta_values['design'][0];    
												$key = 'design';
											  }

										}	

											$args = array(
												'post_type'      => $flooringtype,
												'posts_per_page' => -1,
												'post_status'    => 'publish',
												'meta_query'     => array(
													array(
														'key'     => $key,
														'value'   => $familycolor,
														'compare' => '='
													),
													array(
														'key' => 'swatch_image_link',
														'value' => '',
														'compare' => '!='
														)
												)
											);										
									
											$the_query = new WP_Query( $args );

										
										
                                            
                                        ?>

</style>
<div class="container childtheme">
	<div class="row">
		<div class="fl-content product col-sm-12 instockPdp childtheme" data-productid ="<?php the_ID(); ?>" data-color ="<?php echo $color; ?>" data-productname ="<?php echo get_the_title(get_the_ID()); ?>" >
			<div class="product-detail-layout-default instock-layout">
                <article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
                    <div class="fl-post-content clearfix grey-back" itemprop="text">
											

						<div class="row">
                            <div class="col-md-6 col-sm-12 product-swatch">
								
                                <div class="product-swatch-inner">
                                    <?php 
                                        $dir = get_stylesheet_directory().'/product-listing-templates/includes/product-instock-images.php';
                                        include( $dir );
                                    ?>
										
									<!-- Product Slider Start -->
										
									<div class="product-variations">
										<!-- <h3>Color Variations</h3> -->

										<div class="product-colors col-md-12">
										<?php ?>
											<ul>
												<li class="color-count" style="font-size:14px;"><?php echo $the_query->found_posts; ?> Color(s) Available</li>
											</ul>
										<?php ?>
									</div>	
									<div class="clearfix"></div>
										<div class="color_variations_slider_default-instock">
											<div class="slides">
												<?php
													while ($the_query->have_posts()) {
														
													$the_query->the_post();

													$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
													
													$style = "padding: 5px;";
												?>
												<div class="slide col-md-3 col-sm-3 col-xs-6 color-box" data-product-color="<?php  the_field('color'); ?>">
													<figure class="color-boxs-inner">
														<div class="color-boxs-inners">
															<a href="<?php the_permalink(); ?>">
																<img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
															</a>
															<br />
															<small><?php  the_field('color'); ?></small>
														</div>
													</figure>
												</div>
												<?php
													} 										
													wp_reset_postdata();
												?>
											</div>
										</div>
									</div>
<!-- Product Slider End -->		
									<div class="clearfix"></div>				
                                    <div class=" button-wrapper-default">                                        
										<?php  if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){ ?>
											<a href="<?php if($getcouponreplace == 1){ echo $getcouponreplaceurl;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<?php if($getcouponreplace ==1){ echo $getcouponreplacetext;}else{ echo 'GET COUPON'; }?>
											</a>
            							<?php } ?>
                                        <a href="/contact-us/" class="button contact-btn">CONTACT US</a>
										<?php if($pdp_get_finance != 1 || $pdp_get_finance == '' ){?>						
											<a href="<?php if($getfinancereplace ==1){ echo $getfinancereplaceurl;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext;}else{ echo 'Financing'; } ?></a>	
										<?php } ?>

																												
											<?php if (is_user_logged_in()) { 

					$fav_sql = 'SELECT * FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id = '. get_the_ID().'';

					$check_fav = $wpdb->get_results($fav_sql);         

					if(!empty($check_fav)) {
						$link_action = 'rem_fav';
						$icon_class = 'fa fa-heart';
						$text = "Remove From Favorite";    
					}else{
						$link_action = 'add_Fav';
						$icon_class = 'fa fa-heart-o';
						$text = "Add as Favorite"; 
					}
					?> 

					<a class="button alt fl-button pdpFavbutton <?php echo $link_action; ?>"  data-user="<?php echo get_current_user_id(); ?>" data-id="<?php echo get_the_ID(); ?>">
						<i class="<?php echo $icon_class; ?>" aria-hidden="true"  > </i>
						<span class="rf" style="color: #fff;">Remove From Favorite</span>
						<span class="af" style="color: #fff;">Add as Favorite</span>
					</a>

					<?php } ?>
									
										<?php  roomvo_script_integration($manufacturer,$sku,get_the_ID()); ?>	
									</div>
									<!--Product attributes  -->

									<div class="clearfix"></div>
									<div id="product-attributes-wrap">
										<?php 
											$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
											include( $dir );
										?>
									</div>

									<!--Product attributes  End here-->
                                </div>
                            </div>
                                
                            <div class="col-md-6 col-sm-12 product-box pdp2-leftbox">
                                <div class="row">
								<div class="col-md-5  col-sm-6  col-xs-12 col-12">
                                        <?php  if(array_key_exists("parent_collection",$meta_values)){ ?>
                                        <h4>
                                            <?php echo $meta_values['parent_collection'][0]; ?> </h4>
                                        <?php } ?>
                                        <h2 class="fl-post-title" itemprop="name">
                                        <?php echo $meta_values['brand'][0]; ?> <?php echo $meta_values['collection'][0]; ?>
                                        </h2>
                                        <h1 class="fl-post-title" itemprop="name">
                                            <?php echo $meta_values['color'][0]; ?>
										</h1>
										<div class="price"><strong><sup>$</sup><?php if($meta_values['price'][0]){echo str_replace("$", "", $meta_values['price'][0]);}else{echo str_replace("$", "", $meta_values['Price'][0]);};  ?></strong>&nbsp;/<sub>sf</sub></div>
										
                                        <?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
                                    </div>

                                    <div class="product-colors col-md-7  col-sm-6  col-xs-12 col-12">
									<!--Product color php --> 
										
										
                                        <!-- <?php
                                            $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
                                            include_once( $dir );      
											?> -->
										<div class="custom_note_wrap"><?php echo $meta_values['custom_note'][0]; ?></div>	
										<!-- <div style="min-height: 50px;"></div>	 -->
										<div class="row">
											<div class="col-lg-6 MarkInstock">

												<img class="Instock-mark" src=" <?php echo get_stylesheet_directory_uri().'/images/comment.png'; ?> " alt="Instock-Mark" />
												<span>IN-STOCK</span>
											</div>
											<div class="col-lg-6 share">
												<div>SHARE</div>
												<ul>
													<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"> <img class="Instock-mark" src=" <?php echo get_stylesheet_directory_uri().'/images/share/facebook.jpg'; ?> " alt="Facebook Share" /> </a></li>
													<li><a href="https://twitter.com/share?url=<?php the_permalink(); ?>" target="_blank"><img class="Instock-mark" src=" <?php echo get_stylesheet_directory_uri().'/images/share/twitter.jpg'; ?> " alt="Twitter Share" /> </a> </li>
													<li id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></li>
												</ul>
											</div>	
										</div>	
										
                                    </div>
									
                                    
                                    
                                        <div class="clearfix"></div>
                                </div>
									<div class="clearfix"></div>
									
									<!--prodct slider color-->		
										<div class="clearfix"></div>
									<div class="responsive-button-wrapper-default">
									<?php  if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){  ?>
											<a href="<?php if($getcouponreplace == 1){ echo $getcouponreplaceurl;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<?php if($getcouponreplace ==1){ echo $getcouponreplacetext;}else{ echo 'GET COUPON'; }?>
											</a>
            						<?php } ?>
                                        <a href="/contact-us/" class="button contact-btn">CONTACT US</a>
										<?php if($pdp_get_finance != 1 || $pdp_get_finance  == '' ){?>						
											<a href="<?php if($getfinancereplace ==1 ){ echo  $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext ;}else{ echo 'Get Financing'; } ?></a>	
										<?php } ?>	

										
										
											<?php if (is_user_logged_in()) { 

					$fav_sql = 'SELECT * FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id = '. get_the_ID().'';

					$check_fav = $wpdb->get_results($fav_sql);         

					if(!empty($check_fav)) {
						$link_action = 'rem_fav';
						$icon_class = 'fa fa-heart';
						$text = "Remove From Favorite";    
					}else{
						$link_action = 'add_Fav';
						$icon_class = 'fa fa-heart-o';
						$text = "Add as Favorite"; 
					}
					?> 

					<a class="button alt fl-button pdpFavbutton <?php echo $link_action; ?>"  data-user="<?php echo get_current_user_id(); ?>" data-id="<?php echo get_the_ID(); ?>">
						<i class="<?php echo $icon_class; ?>" aria-hidden="true"  > </i>
						<span class="rf" style="color: #fff;">Remove From Favorite</span>
						<span class="af" style="color: #fff;">Add as Favorite</span>
					</a>

					<?php } ?>
										
										<?php  roomvo_script_integration($manufacturer,$sku,get_the_ID()); ?>

                                    </div>


                            <div class="clearfix"></div>
							<!-- <div class="custom_note"><?php //echo $meta_values['custom_note'][0]; ?></div> -->
							<!--Product Attribute -->
							<div class="formWrapperPdp">
								<?php 

								if($flooringtype == 'instock_carpet'){
								
								  echo do_shortcode('[fl_builder_insert_layout slug="instock-pdp-form-order-softsurface"]');
								
								}else{

									echo do_shortcode('[fl_builder_insert_layout slug="instock-pdp-form-order-hardsurface"]');

								}?>
							</div>
				
                            </div>
                                <div class="clearfix"></div>
                        </div>
                            <div class="clearfix"></div>
                        
                    </div>
                </article>
            </div>
			<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image_600,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>
			<script>

jQuery(document).ready(function() {

	var num = 0;
	function changeTheBorder(select){
		console.log(select);
		var selectedValue = jQuery(select).children("option:selected").val();
		if(selectedValue){
			jQuery('.color_variations_slider_default-instock .slick-track > .slide').each(function(){
				if(jQuery(this).attr('data-product-color') == selectedValue){
					jQuery(this).find('img.swatch-img').css('border' , '2px solid #0079c1');
				}else{
					jQuery(this).find('img.swatch-img').css('border' , '0px solid transparent');
				}
			})
				
		}else{
			jQuery('.color_variations_slider_default-instock .slick-track > .slide').each(function(){
				jQuery(this).find('img.swatch-img').css('border' , '0px solid transparent');
			})
		}
	}
	
	
	jQuery('.PDP_form .uabb-tabs-nav > ul > li').each(function(){
		jQuery(this).click(function(){
			num = jQuery(this).attr('data-index');
			var selected = jQuery('.product_color_select2 select');
			if(num == 0){
				selected = jQuery('.product_color_select2 select');
				changeTheBorder(selected[0]);
			}else{
				selected = jQuery('.product_color_select select');
				changeTheBorder(selected[0]);
			}
			
		})
	});

	jQuery('.product_color_select select').change(function(){
			changeTheBorder(this);
	});
	jQuery('.product_color_select2 select').change(function(){
			changeTheBorder(this);
	});

	function firstLoad(){
		if(num == 0){
			var selected = jQuery('.product_color_select2 select');
			var selectedtwo = jQuery('.product_color_select select');
			var options = jQuery('.product_color_select2 select option');
			var currentValue = jQuery('.instockPdp').attr('data-productname');
			options.each(function(){
				if(jQuery(this).attr('value') ===  currentValue){
					selected.val(currentValue);
					selectedtwo.val(currentValue);
					// jQuery(this).attr('selected', 'true');
					changeTheBorder(selected[0]);
					changeTheBorder(selectedtwo[0]);
				}
			});

			
		}
	}
	
	firstLoad();

	// var pro_id = jQuery(".instockPdp").attr("data-productid");
	// jQuery.ajax({
	// 	type: "POST",
	// 	url: "/wp-admin/admin-ajax.php",
	// 	data: 'action=get_colorlisting&product_id='+ pro_id,

	// 	success: function(data) {
	// 		// attributeAdd(JSON.parse(data));
	// 		//jQuery('.product_color_select select option img').css('height' , ' 35px ');
	// 	}
	// });
});

			</script>
    <div class="product-plp-grid product-grid swatch childtheme" itemscope itemtype="http://schema.org/ItemList">

    <div class="row product-row">
    <?php 
    global $wpdb;
    $show_financing = get_option('sh_get_finance');
    $col_class = 'col-lg-4 col-md-4 col-sm-6 ';
    $salebrand = get_option('salesbrand');
    if($salebrand !=''){
        $slide_brands = rtrim($salebrand, ",");
        $brandonsale = array_filter(explode(",",$slide_brands));
        $brandonsale = array_map('trim', $brandonsale);
    }
    $K = 1;

$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');
$getcoupon_link = get_option('getcoupon_link');

while ( $prod_list->have_posts() ): $prod_list->the_post(); 
      //collection field
      $meta_values = get_post_meta( get_the_ID() );  
      $collection = $meta_values['collection'][0];
      $brand =  $meta_values['brand'][0];      
      $flooringtype = get_post_type();
      $product_url = get_the_permalink();
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

    <meta itemprop="position" content="<?php echo $K; echo $prod_list->ID;?>" />
        <?php // FLPostGridModule::schema_meta(); ?>
        <?php if($meta_values['swatch_image_link'][0]) { ?>
            <div class="fl-post-grid-image">

            <?php 
            if (is_user_logged_in()) { 

                $fav_sql = 'SELECT * FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id = '. get_the_ID().'';

                $check_fav = $wpdb->get_results($fav_sql);         

                if(!empty($check_fav)) {
                    $link_action = 'rem_fav';
                    $icon_class = 'fa fa-heart';
                }else{
                    $link_action = 'add_Fav';
                    $icon_class = 'fa fa-heart-o';
                }
            ?> 
                <div class="favProdPLP">
                    <a  class="favProdPdp <?php echo $link_action; ?>" data-user="<?php echo get_current_user_id(); ?>" data-id="<?php echo get_the_ID(); ?>"><i class="<?php echo $icon_class; ?>" aria-hidden="true"></i></a> 
                </div>
            <?php } ?> 


                <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                  <?php 
                  
                  if( get_option('plpproductimg') !='' && get_option('plpproductimg') == '1' ){
                    $gallery_images = $meta_values['gallery_room_images'][0];
                    $gallery_img = explode("|",$gallery_images);
                    $image =  $gallery_img[0] ? thumb_gallery_images_in_plp_loop($gallery_img[0]) : swatch_image_product_thumbnail(get_the_ID(),'222','222');;
                     
                  }else{
                    $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                  }	
					?>
            <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
            <?php if($flooringtype == 'instock_hardwood' || $flooringtype == 'instock_lvt' || $flooringtype == 'instock_carpet'){?>

<img class="instockImgPlp" src=" <?php echo get_stylesheet_directory_uri().'/images/instock.png'; ?> " alt="In Stock" /> 

<?php } ?>
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
        <h4><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?><span> <?php  echo $meta_values['collection'][0];  ?></span> <span><?php echo $meta_values['style'][0]; ?> </span> <?php } else{ ?> <span><?php echo $meta_values['collection'][0]; ?></span> <span><?php echo $meta_values['brand'][0]; ?> </span><?php } ?> </h4> <!-- <h2 class="fl-post-grid-title" itemprop="name">
                <a href="<?php the_permalink(); ?>" itemprop="url" title="<?php the_title_attribute(); ?>"><?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { echo get_field('color'); } else{ echo get_field('color'); }?></a>
            </h2> -->

            <?php
                $collection = $meta_values['collection'][0]; 
                                                             
                
                if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                    $familycolor = $meta_values['style'][0];
                    $key = 'style';

                }else{

                    $familycolor = $meta_values['collection'][0];    
                    $key = 'collection';
                }	
                
                
                            remove_filter('posts_groupby', 'query_group_by_filter'); 

                            $familycolor = addslashes($familycolor);
                            
                         
                            $table_posts = $wpdb->prefix.'posts';
                            $table_meta = $wpdb->prefix.'postmeta';	
                
                           // $coll_sql = "SELECT post_id FROM $table_meta WHERE meta_key = '$key' AND meta_value ='$familycolor'";

                           $coll_sql = "select distinct($table_meta.post_id)  
                                        FROM $table_meta
                                        inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                        WHERE post_type = '$flooringtype' AND meta_key = '$key' AND meta_value = '$familycolor'";

                           $data_collection = $wpdb->get_results($coll_sql);                           
                           
                ?>
            <div class="product-variations1">
    <h5><?php echo count($data_collection);?> COLORS AVAILABLE</h5>
   
   <div class="product-color-variationPlp plp_slide_wrap">
   <?php if($data_collection !='') { ?>
        <div class="slides row">
            <?php
            $i= 0 ;
         
            foreach($data_collection as $data_pro) { 

                if($i==6){break;}


              //  $swatch_image = get_field('swatch_image_link',$data_pro->post_id);

                $swatch_image = get_post_meta($data_pro->post_id,'swatch_image_link',true);

                $image = newplp_swatch_image_product_thumbnail($swatch_image,'50','50');      
                
                $product_url = get_the_permalink($data_pro->post_id);

                $product_title = get_the_title($data_pro->post_id);
                
               
                $style = "padding: 5px;";
            ?>
            <div class="slide color-box1 col-lg-2 col-sm-2 col-xs-2 col-2">
                <figure class="color-boxs-inner">
					<div class="color-boxs-inners">
                    <?php if($i==5){ ?>
                        <a href="<?php echo $product_url; ?>" class="view_more_link link">+</a>
                    
                        
                    <?php }else{ ?>
                        <a href="<?php echo $product_url ; ?>">
                            <img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php echo $product_title; ?>" alt="<?php echo $product_title; ?>" title="<?php echo $product_title; ?>" width="100" height="100" />
                        </a>
                    <?php } ?>
                    </div>
                </figure>
            </div>
            <?php
            $i++;
    } ?>
        </div>
        <?php } ?>
    </div>

</div>

            <a class="fl-button plp_box_btn" href="<?php the_permalink(); ?>">VIEW PRODUCT</a><br>
            <!-- <a class="fl-button" href="#">SEE IN ROOM</a> <br> -->
           
            <?php if(get_option('getcouponbtn') == 1){ ?>

						<a href="<?php echo $getcoupon_link; ?>" target="_self" class="link getcouponbtnlink" role="button" ><span class="fl-button-text">GET COUPON</span></a>
            <?php } ?>

            <?php  if($getcouponreplace == 1 && $getcouponreplaceurl !='' && $getcouponreplacetext !=''){ ?>

                <a href="<?php echo $getcouponreplaceurl ;?>" target="_self" class="linkcustompdpbtnlink" role="button">
                <span class="fl-button-text"><?php if($getcouponreplace ==1){ echo $getcouponreplacetext ;}?></span>
                </a>

            <?php } ?>	

           <?php if($show_financing == 1){?>
            <a href="<?php if($getfinancereplace == 1){ echo $getfinancereplaceurl;}else{ echo '/flooring-financing/'; } ?>" target="_self" class="link" role="button" >
                <span class="fl-button-text"><?php if( $getfinancereplace == '1'){ echo $getfinancetext ;}else{ echo 'Get Financing'; } ?></span>
            </a>
            <br />
           <?php } ?>
           
           
          
           
        </div>
    </div>
    </div>
<?php  $K++; endwhile; wp_reset_postdata();?>
</div>
</div>

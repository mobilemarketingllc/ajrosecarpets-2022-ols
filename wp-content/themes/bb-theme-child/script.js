jQuery(document).on("click", "#google_search_icon", function () {
  $("#google_search_field").toggle();
});

////floor finder js start here
// addEvent();
// const data = {};

// function addEvent(e) {
//     let getActive = document.querySelector(".flooringFinder > section.active");
//     if (getActive) {
//         let getCards = getActive.querySelectorAll(".card:not(.deactivate)");
//         for (let i = 0; i < getCards.length; i++) {
//             getCards[i].addEventListener("click", showNext);
//         }
//     }
// }

// function addEventToNextBtn(e) {
//     let getActive = document.querySelector(".flooringFinder section.active .next > a");
//     getActive.addEventListener("click", showNext);

// }

// bootstrap_alert = function() {}
// bootstrap_alert.warning = function(message) {
//     $('.flooringFinder section.active .next #alert_placeholder').html('<div class="alert alert-danger" role="alert">' + message + '</div>')
// }

// function showNext(e) {

//     $("html, body").animate({ scrollTop: 0 }, "slow");
//     let getSec = document.querySelector(".flooringFinder section.active");

//     if (getSec) {
//         let getIndex = getSec.getAttribute("data-index-value");
//         if (getIndex <= 6 && getIndex > 1 && getSec.querySelectorAll("input:checked").length < 1) {
//             return bootstrap_alert.warning('Please Select Any One of Above');
//         } else if (getIndex <= 6 && getIndex > 1) {
//             let inputData = getSec.querySelectorAll("input:checked");
//             let gettype = getSec.getAttribute("data-section");
//             if (getSec.querySelectorAll("input:checked").length > 1) {
//                 let arr = [];
//                 for (val of inputData) {
//                     arr.push(val.value);
//                 }
//                 data[gettype] = arr;
//             } else {
//                 data[gettype] = inputData[0].value;
//             }
//         } else if (getIndex == 1) {
//             if (e.currentTarget.getAttribute("data-look") === 'carpet') {
//                 let card = [`.card[data-room="bathroom"]`, `.card[data-room="kitchen"]`, `.card[data-room="laundryRoom"]`, `.card[data-color-carpet="reds"]`, `.card[data-color-carpet="violets"]`];
//                 let cards = getElements(card);
//                 deactivateCard(cards);

//                 let getActive = document.querySelectorAll(".flooringFinder .typeOfCarpet");
//                 getActive.forEach(function(item) {
//                     item.classList.remove('hidden');
//                     item.classList.add('show');
//                 });
//                 data['typeOfLook'] = 'carpeting';

//             } else if (e.currentTarget.getAttribute("data-look") === 'woodLook') {
//                 let getActive = document.querySelectorAll(".flooringFinder .typeOfWood");
//                 getActive.forEach(function(item) {
//                     item.classList.remove('hidden');
//                     item.classList.add('show');
//                 });
//                 data['typeOfLook'] = ['hardwood_catalog', 'laminate_catalog', 'luxury_vinyl_tile'];

//             } else if (e.currentTarget.getAttribute("data-look") === 'tileOrStone') {
//                 let getActive = document.querySelectorAll(".flooringFinder .typeOfTile");
//                 getActive.forEach(function(item) {
//                     item.classList.remove('hidden');
//                     item.classList.add('show');
//                 });
//                 data['typeOfLook'] = 'tile';
//             }
//         }
//         if (getIndex <= 6) {
//             // alert(getIndex);
//             let nextSection = document.querySelector(`section[data-index-value='${1 + + getIndex}']`);
//             let getActive = document.querySelector(".flooringFinder section.active");
//             getActive.classList.remove('active');
//             if (getIndex < 6) {
//                 nextSection.classList.add('active');
//                 nextSection.classList.contains('multiselect') ? multiselect() : singleselect();
//                 getIndex < 5 ? addEventToNextBtn() : console.log('not applying');

//             } else {
//                 document.querySelector(".resultWrap").classList.add('active');
//                 //console.log(data);
//             }
//         }
//     } else {
//         alert('somthing wrong');
//     }

//     function getElements(ele) {
//         let arr = [];
//         for (let i = 0; i < ele.length; i++) {
//             arr.push(document.querySelector(ele[i]));
//         }
//         return arr;
//     }

//     function deactivateCard(cards) {
//         cards.forEach(function(item) {
//             item.classList.add('deactivate');
//         });
//     }

// }

// function multiselect() {
//     let getActive = document.querySelectorAll(".flooringFinder section.active.multiselect");
//     let getCards = getActive[0].querySelectorAll(".card:not(.deactivate)");
//     for (let i = 0; i < getCards.length; i++) {
//         getCards[i].addEventListener("click", function() {
//             let getChec = getCards[i].querySelector("input[type=checkbox]");
//             if (getChec.checked) {
//                 getCards[i].classList.remove('shadow');
//                 getChec.checked = false;
//             } else {
//                 getCards[i].classList.add('shadow');
//                 getChec.checked = true;
//             }
//         });
//     }
// }

// function singleselect() {
//     let getActive = document.querySelector(".flooringFinder section.active.singleselect");

//     let getCards = getActive.querySelectorAll(".card:not(.deactivate)");
//     for (let i = 0; i < getCards.length; i++) {
//         getCards[i].addEventListener("click", function() {

//             let getChec = getCards[i].querySelector("input[type=checkbox]");
//             if (getChec.checked) {
//                 getCards[i].classList.remove('shadow');
//                 getChec.checked = false;
//             } else {
//                 let chec = getActive.querySelectorAll("input:checked");
//                 if (chec.length < 1) {
//                     getCards[i].classList.add('shadow');
//                     getChec.checked = true;
//                 } else if (chec.length === 1) {
//                     chec[0].checked = false;
//                     chec[0].parentElement.classList.remove('shadow');
//                     getCards[i].classList.add('shadow');
//                     getChec.checked = true;
//                 }
//             }
//         });
//     }
// }

// jQuery(document).ready(function() {
//     // Get flooring finder data
//     jQuery('.flooringFinder section .getProData > a').on('click', function() {
//         showNext();
//         var mySelectedData = data;

//         jQuery.ajax({
//             type: "POST",
//             url: "/wp-admin/admin-ajax.php",
//             data: 'action=check_out_flooring&product=' + mySelectedData.typeOfLook + '&style=' + mySelectedData.typeOf + '&design_style=' + mySelectedData.designStyle + '&room=' + mySelectedData.room + '&imp_thing=' + mySelectedData.impThing + '&color=' + mySelectedData.color,
//             success: function(data) {
//                 jQuery(".flooringFinderajax ").html(data);
//                 if (data) {
//                     loadMoreData();
//                 }
//             }

//         });

//     });

// 	 function loadMoreData() {
//         let allPr = document.querySelectorAll(".resultWrap.active .flooringFinderajax > .singlePro");
//         let count = 0;
//         jQuery(allPr).each(function(index, ele) {
//             count++;
//             count > 5 ? jQuery(ele).css("display", "none") : jQuery(ele).css("display", "block");

//         });

//         document.querySelector(".resultWrap.active .flooringFinderajax > .buttonWrap > .button").addEventListener("click", function(e) {
//             let allPr = document.querySelectorAll(".resultWrap.active .flooringFinderajax > .singlePro");
//             jQuery(allPr).each(function(index, ele) {
//                 jQuery(ele).css("display", "block");
//             });
//             jQuery(".resultWrap.active .flooringFinderajax > .buttonWrap > .button").css("display", "none");
//         });

//     }

// });

//floor finder js End here

jQuery(document).ready(function () {
  let searchParams = new URLSearchParams(window.location.search);
  let param = searchParams.get("gclid");
  console.log(param);

  if (param != null) {
    jQuery.ajax({
      type: "POST",
      url: "/wp-admin/admin-ajax.php",
      data: "action=paid_search_check&gclid=" + param,
      success: function (response) {
        console.log(response);

        // jQuery.cookie("paid_search", response, { expires: 1, path: '/' });
        // location.reload(true)
      },
    });
  }
});

// // IP location js  //

var $ = jQuery;
jQuery(document).ready(function () {
  var mystore_loc = jQuery(".header_location_name").html();
  jQuery("#input_29_32").val(mystore_loc);
  let searchParams = new URLSearchParams(window.location.search);
  let param = searchParams.get("gclid");

  if (param == null) {
    param = "NA";
  }

  if (typeof $.cookie("paid_search") === "undefined") {
    var paid_search = "NA";
  } else {
    var paid_search = jQuery.cookie("paid_search");
  }

  jQuery.ajax({
    type: "POST",
    url: "/wp-admin/admin-ajax.php",
    data:
      "action=get_storelisting&gclid=" + param + "&paid_search=" + paid_search,
    dataType: "JSON",
    success: function (response) {
      var posts = JSON.parse(JSON.stringify(response));
      var header_data = posts.header;
      var header_phone = posts.phone;
      var header_telphone = "tel:" + posts.phone;
      var list_data = posts.list;
      var isStore_set = posts.storeSet;

      var mystore_loc = jQuery.cookie("preferred_storename");

      // jQuery(".locationSelector .uabb-marketing-title").html(header_data);

      // 				if(header_data == "") {
      // 				   jQuery(".header_location_name").html(mystore_loc);
      // 				 }else {
      // 					jQuery(".header_location_name").html(header_data);
      // 				 }

      console.log(posts.header + " data " + posts.phone);
      if (isStore_set == "No") {
        jQuery(".header_location_name").html(header_data);
      } else {
        jQuery(".header_location_name").html(header_data);
        jQuery(".phone .mobile_phone").html(header_phone);
        jQuery(".phone a.mobile_phone").attr("href", header_telphone);

        jQuery(".mobile a").html(header_phone);
        jQuery(".header_store .mobile a").attr("href", header_telphone);

        jQuery(".phone .mobile_phone").addClass("icon");
        jQuery(".header_store .mobile a").addClass("icon");
      }

      jQuery("#ajaxstorelisting #storeLocation > .content").html(list_data);
    },
  });
});

jQuery(document).on("click", ".choose_location", function () {
  var mystore = jQuery(this).attr("data-id");
  var distance = jQuery(this).attr("data-distance");
  var storename = jQuery(this).attr("data-storename");

  var storePhone = jQuery(this).attr("data-phone");

  jQuery.cookie("preferred_store", null, { path: "/" });
  jQuery.cookie("preferred_distance", null, { path: "/" });
  jQuery.cookie("preferred_storename", null, { path: "/" });

  jQuery.cookie("preferred_store", mystore, { expires: 1, path: "/" });
  jQuery.cookie("preferred_distance", distance, { expires: 1, path: "/" });
  jQuery.cookie("preferred_storename", storename, { expires: 1, path: "/" });

  jQuery(".header_store .contentFlyer .mobile_phone").html(storePhone);
  jQuery(".header_store .contentFlyer .mobile_phone").attr(
    "href",
    "tel:" + storePhone + ""
  );

  jQuery("#input_29_32").val(storename);

  if (jQuery(".populate-store select").length > 0) {
    var mystore_loc = jQuery.cookie("preferred_storename");
    if (mystore_loc == "Shop From Home") {
      mystore_loc = "Shop at Home";
    }
    jQuery(".populate-store select").val(mystore_loc);
  }

  jQuery("span.uabb-offcanvas-close").trigger("click");

  if (
    jQuery(".phone .mobile_phone.icon").length == 0 ||
    jQuery(".header_store .mobile a.icon").length == 0
  ) {
    jQuery(".phone .mobile_phone").addClass("icon");
    jQuery(".header_store .mobile a").addClass("icon");
  }
});
//flyer event assign function
function addFlyerEvent() {}

jQuery(document).ready(function () {
  var mystore_loc = jQuery.cookie("preferred_storename");
  if (mystore_loc == "Shop From Home") {
    mystore_loc = "Shop at Home";
  }
  //console.log(mystore_loc);
	var myStoreSelect = jQuery(".populate-store select").val() || "";

	if (myStoreSelect.length < 1) {
		jQuery(".populate-store select").val(mystore_loc);
	}

  jQuery(".custom_searchHeader .fl-icon").click(function () {
    jQuery(".custom_searchModule").slideToggle();
  });

  jQuery(".color_variations_slider_1 > .slides").slick({
    dots: false,
    infinite: false,
    speed: 300,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    prevArrow:
      '<a href="javascript:void(0)" class="arrow slick-prev">Previous</a>',
    nextArrow: '<a href="javascript:void(0)" class="arrow slick-next">Next</a>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
});
var customSearch = jQuery(
  ".custom_searchHeader .fl-icon , .custom_searchModule"
);

jQuery(document).mouseup(function (e) {
  if (
    !customSearch.is(e.target) &&
    customSearch.has(e.target).length === 0 &&
    jQuery(".custom_searchModule").css("display") == "block"
  ) {
    jQuery(".custom_searchModule").slideToggle();
  }
});

// function addFavProduct(is_fav, sku, prod_id) {

//     var queryString = "";
//     queryString = 'action=add_fav_product&sku=' + sku + '&post_id=' + prod_id + '&is_fav=' + is_fav;
//     jQuery.ajax({
//         url: "/wp-admin/admin-ajax.php",
//         data: queryString,
//         type: "POST",
//         dataType: "json",
//         success: function(response) {

//             if (is_fav == 0) {
//                 is_fav1 = 1;
//             } else {
//                 is_fav1 = 0;
//             }

//             $(".is_fav" + sku).attr("onClick", "addFavProduct('" + is_fav1 + "','" + sku + "','" + prod_id + "')");
//             $(".is_fav" + sku).removeClass("is_fav0");
//             $(".is_fav" + sku).removeClass("is_fav1");
//             $(".is_fav" + sku).toggleClass("is_fav" + is_fav1);
//         },
//         error: function() {}
//     });
// }

function getDataUrl(img) {
  // Create canvas
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  // Set width and height
  canvas.width = img.width;
  canvas.height = img.height;
  // Draw the image
  ctx.drawImage(img, 0, 0);
  return canvas.toDataURL("image/jpeg");
}
function getDataUrlTwo(img) {
  // Create canvas
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  // Set width and height
  canvas.width = 200;
  canvas.height = 200;
  // Draw the image
  ctx.drawImage(img, 0, 0);
  return canvas.toDataURL("image/jpeg");
}
function provideUrls() {
  if (document.querySelector(".printDOc")) {
    document
      .querySelector(".printDOc")
      .addEventListener("click", function (event) {
        event.preventDefault();
        var img = document.querySelectorAll(".printWrap img");

        img.forEach(async function (ele) {
          ele.setAttribute("crossOrigin", "Anonymous");
          ele.src = await getDataUrlTwo(ele);
        });

        document.querySelector(".hideShare").style.display = "none";
        printDiv();
        document.querySelector(".hideShare").style.display = "flex";
      });
  }
}
function provideUrlsTwo() {
  document.querySelector(".printDOc").addEventListener("click", function () {
    var img = document.querySelector(".printWrap img");
    img.src = getDataUrl(img);
    printDiv();
  });
}
window.onload = setTimeout(provideUrls, 1000);

function printDiv() {
  var printContents = document.querySelector(".printWrap");
  var opt = {
    margin: 0.3,
    filename: "download.pdf",
    image: { type: "jpeg", quality: 0.98 },
    html2canvas: { scale: 2 },
    jsPDF: { unit: "in", format: "letter", orientation: "portrait" },
  };

  // New Promise-based usage:
  html2pdf().from(printContents).set(opt).save();
}

jQuery(document).on("click", ".deletemeasure", function () {
  var img_id = jQuery(this).attr("data-id");
  var uid = jQuery(this).attr("data-userid");

  jQuery.ajax({
    type: "POST",
    url: "/wp-admin/admin-ajax.php",
    data: "action=delete_measureimg&mimg_id=" + img_id + "&user_id=" + uid,

    success: function (data) {
      jQuery("#mesureMentprintMe").html(data);
    },
  });
});

function openPopUp(e) {
  let title = e.currentTarget;
  let element = document.querySelector(".overlayMeasure");
  element ? element.remove() : console.log("not findout");

  var product = `<div class="overlayMeasure"><div class='measurepopup '>
    <div class='row'>
    <div class='col-lg-6'>
    <h3>${e.getAttribute("data-title")}</h3>
    </div>
    <div class='col-lg-6 sharingBox hideShare'>
    <a href='#' id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin">
    </a>
            <a href='#' class="printDOc">
                <i class='fa fa-print' aria-hidden='true'></i>
            </a>
            <a href='javascript:void(0);' onclick="closePopup()">
                <i class='fa fa-times' aria-hidden='true'></i>
            </a>	
        </div>
    </div>
    
    <div id="measureprint " class="printWrap"> 
     <img src='${e.getAttribute("data-img")}' </div></div> </div>`;

  jQuery("body").append(product);
  provideUrlsTwo();
  //new needShareDropdown(document.getElementById('share-button-3'));

  new needShareDropdown(document.getElementById("share-button-3"));
  removeInnerHtml();
}

function removeInnerHtml() {
  var button = document.querySelector(".need-share-button_button");
  if (button) {
    button.innerHTML = "";
  }
}
removeInnerHtml();

window.onload = function checkShareBox() {
  var button = document.querySelector("#share-button-3");
  if (button) {
    new needShareDropdown(document.getElementById("share-button-3"));
    removeInnerHtml();
  }
};

function closePopup() {
  document.querySelector(".overlayMeasure").remove();
}

var debounce = (info, delay) => {
  let debounceTimer;
  return function () {
    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(() => addRemovefavAjax(info), delay);
  };
};

// function addRemovefavAjax(classcontainer) {
//     //console.log('run run');
//     if (classcontainer.status == 'add') {
//         jQuery.ajax({
//             type: "POST",
//             url: "/wp-admin/admin-ajax.php",
//             data: 'action=add_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,

//             success: function(data) {
//                 jQuery(classcontainer.element).removeClass("add_Fav");
//                 jQuery(classcontainer.element).addClass("rem_fav");
//                 jQuery(classcontainer.element).children("i").removeClass("fa-heart-o");
//                 jQuery(classcontainer.element).children("i").addClass("fa-heart");

//             }
//         });
//     } else {
//         jQuery.ajax({
//             type: "POST",
//             url: "/wp-admin/admin-ajax.php",
//             data: 'action=remove_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,

//             success: function(data) {
//                 jQuery(classcontainer.element).removeClass("rem_fav");
//                 jQuery(classcontainer.element).addClass("add_Fav");
//                 jQuery(classcontainer.element).children("i").removeClass("fa-heart");
//                 jQuery(classcontainer.element).children("i").addClass("fa-heart-o");

//             }
//         });
//     }

// }

// jQuery(document).on('click', '.favProdPdp', function(event) {
//     event.stopPropagation();
//     //console.log(event)
//     var current = this;
//     var post_id = jQuery(this).attr("data-id");

//     var classContainer = {
//         postId: jQuery(this).attr("data-id"),
//         UserId: jQuery(this).attr("data-user"),
//         element: this
//     }

//     if (jQuery(current).hasClass("add_Fav")) {
//         classContainer.status = "add";
//         var add = debounce(classContainer, 300);
//         add();
//     } else {
//         classContainer.status = "remove";
//         var remove = debounce(classContainer, 300);
//         remove();
//     }
// });

// jQuery(document).on('click', '#rem_fav', function() {
//     jQuery.ajax({
//         type: "POST",
//         url: "/wp-admin/admin-ajax.php",
//         data: 'action=remove_favroiute_list&post_id=' + jQuery(this).attr("data-id") + '&user_id=' + jQuery(this).attr("data-user_id"),

//         success: function(data) {
//             jQuery("#ajaxreplace").html(data);
//             location.reload();
//         }
//     });
// });

// jQuery(".favProWarpper .prod_like_wrap").hover(function(){
// 		   jQuery(this).find(".favButtons").stop(true,true).css({"top":"0px"}).animate({"top":"0px", "opacity": "1"},200);
// 		},function(){
// 		   jQuery(this).find(".favButtons").animate({"top":"150%", "opacity": "0"},200, function() {
// 			   jQuery(this).css({"top":"150%"});
// 		   });
// 		});

// jQuery(".add_note_fav").each(function() {
//     jQuery(this).on("click", function() {
//         var proid = jQuery(this).attr("data-productid");
//         jQuery('#addnote_productid').val(proid);
//     });
// })
// jQuery(".view_note_fav").each(function() {
//     jQuery(this).on("click", function() {
//         var note_val = jQuery(this).attr("data-note");
//         jQuery('.view_note_wrapper-overlay .uabb-modal-content-data p').text(note_val);
//     });

// });
// jQuery(document).on('click', '.pdpFavbutton', function(event) {
//     event.stopPropagation();
//     //console.log(event)
//     var current = this;
//     var post_id = jQuery(this).attr("data-id");

//     var classContainer = {
//         postId: jQuery(this).attr("data-id"),
//         UserId: jQuery(this).attr("data-user"),
//         element: this
//     }

//     if (jQuery(current).hasClass("add_Fav")) {
//         classContainer.status = "add";
//         var add = debounce(classContainer, 300);
//         add();
//     } else {
//         classContainer.status = "remove";
//         var remove = debounce(classContainer, 300);
//         remove();
//     }
// });

// jQuery('#add_note_form').submit(function(e) {
//     e.preventDefault();

//     var message = jQuery('textarea#message').val();
//     var proid = jQuery('#addnote_productid').val();
//     //alert('message');
//     message = jQuery.trim(message);
//     if (message) {
//         jQuery.ajax({
//             data: { action: 'add_note_form', note: message, addnote_productid: proid },
//             type: 'post',
//             url: "/wp-admin/admin-ajax.php",
//             success: function(data) {
//                 //console.log(data);
//                 location.reload();
//             }
//         });
//     }
// })

function ValidateTextarea(e) {
  //console.log('Hiii');
}

// room_inspiration_link
jQuery(document).on("click", ".room_inspiration_link", function (e) {
  e.preventDefault();
  roomvo.startStandaloneVisualizer(
    null,
    null,
    null,
    "&has_room_style_transfer_list=1"
  );
});

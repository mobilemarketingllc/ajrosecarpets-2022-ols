<?php

/*
Template Name: Landing Page Template

*/

get_header(); 
?>
<?php 
       
    $short = get_the_content();
    echo do_shortcode($short);  
      
?>
<?php get_footer(); ?>

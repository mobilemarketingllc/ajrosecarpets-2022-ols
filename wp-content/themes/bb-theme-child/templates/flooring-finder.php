<?php /* Template Name: Flooring Finder */ 
 get_header(); 
?>

<main class="Mainwrapper flooringFinder">
    <section data-section="look" data-index-value="1" class="active">
        <div class="row pages">
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <h4>FIND YOUR FLOORING STYLE</h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <span><span style="color: #000;"><b>1</b></span> of 6</span>
                    </div>
        </div><!--row end -->
        <div class="container contCenter ">
            <div class="gridWrapper">
                
                <div class="row title">
                    <h2 class=" text-center"> What type of look do you like?</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-look="carpet">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1/CarpetLook.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1/CarpetLook_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Carpet Look</h4> 
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-look="woodLook">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1/woodLook.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1/woodLook_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Wood Look</h4> 
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-look="tileOrStone">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1/tileLook.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1/tileLook_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Tile or stone Look</h4> 
                            </div>
                            
                        </div>
                    </div>
                </div><!--row end -->
                 <div class="row text-center buttonWrap">
                    <a href="/contact-us/" class="button">I'M NOT SURE</a>
                </div><!--row end -->

            </div>
        </div>
        <div class="progressBar">
                    <div class="bar">
                        
                    </div>
        </div><!--row end -->
    </section>
    <section data-section="typeOf" data-index-value="2" class="singleselect">
        <div class="row pages">
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <h4>FIND YOUR FLOORING STYLE</h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <span><span style="color: #000;"><b>2</b></span> of 6</span>
                    </div>
        </div><!--row end -->
        <div class="container contCenter typeOfCarpet hidden">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What type of carpet are you interested in?</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpetAccents.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpetAccents_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>ACCENTS</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="accents" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpetPattern.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpetPattern_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>PATTERNS</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="patterns" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpettonal.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpettonal_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>TONAL</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="tonal" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpetSolid.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/carpet/carpetSolid_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>SOLID</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="solid" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>
        
        <div class="container contCenter typeOfWood hidden">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What type of Wood are you interested in?</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwSmooth.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwSmooth_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>SMOOTH TEXTURE</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="smooth texture" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwHandscraped.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwHandscraped_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>HANDSCRAPED</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="handscraped" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwWiredBrushed.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwWiredBrushed_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>WIRE BRUSH</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="wire brush" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwDistressed.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/wood/hwDistressed_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>RECLAIMED</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="reclaimed" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>

        <div class="container contCenter typeOfTile hidden">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What type of Tile or Stone are you interested in?</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileMosaic.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileMosaic_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>MOSAIC</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="mosaic" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileStone.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileStone_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>STONE</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="stone" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileSubway.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileSubway_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>SUBWAY / RECTANGLE</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="subway / rectangle" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileGlass.jpg" alt="Product Image" style="width:100%!important">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/2/tile/tileGlass_hover.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Glass</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="glass" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>
        <div class="row text-center buttonWrap next">
        <div id = "alert_placeholder"></div>
                    <a class="button">NEXT</a>
        </div><!--row end -->
        <div class="progressBar">
            <div class="bar">                
            </div>
        </div><!--row end -->
    </section>
    <section data-section="designStyle" data-index-value="3" class="singleselect">
        <div class="row pages">
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <h4>FIND YOUR FLOORING STYLE</h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <span><span style="color: #000;"><b>3</b></span> of 6</span>
                    </div>
        </div><!--row end -->
        <div class="container contCenter">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What's your design style?</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/traditional.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>traditional</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="traditional" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/transitional.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>transitional</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="transitional" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/vintage.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>vintage</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="vintage" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/modern.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>mid-century modern</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="mid-century modern" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/industrial.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>industrial</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="industrial" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/coastal.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>coastal</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="coastal" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/farmhouse.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>modern farmhouse</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="modern farmhouse" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/rustic.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>rustic</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="rustic" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/bohemian.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>bohemian</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="bohemian" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/3/victorian.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>victorian</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="victorian" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>
        <div class="row text-center buttonWrap next">
                <div id = "alert_placeholder"></div>
                <a class="button">NEXT</a>
        </div><!--row end -->
        <div class="progressBar">
            <div class="bar">                
            </div>
        </div><!--row end -->
    </section>
    <section data-section="room" data-index-value="4" class="singleselect">
        <div class="row pages">
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <h4>FIND YOUR FLOORING STYLE</h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <span><span style="color: #000;"><b>4</b></span> of 6</span>
                    </div>
        </div><!--row end -->
        <div class="container contCenter">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">Which room are you shopping for?</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/bedroom.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Bedroom</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="bedroom" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/livingroom.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>living room</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="living room" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/diningroom.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>dining room</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="dining room" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-room="kitchen">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/kitchen.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>kitchen</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="kitchen" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-room="bathroom">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/bathroom.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>bathroom</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="bathroom" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/closet.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>closet</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="closet" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-room="laundryRoom">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/laundryroom.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>laundry room</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="laundry room" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/entryway.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>entryway</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="entryway" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/commercial.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>commercial property</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="commercial property" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/4/multi.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>multiple rooms  </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="multiple rooms" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>
        <div class="row text-center buttonWrap next">
                <div id = "alert_placeholder"></div>
                <a class="button">NEXT</a>
        </div><!--row end -->
        <div class="progressBar">
            <div class="bar">                
            </div>
        </div><!--row end -->
    </section>
    <section data-section="impThing" data-index-value="5" class="multiselect" role="form">
        <div class="row pages">
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <h4>FIND YOUR FLOORING STYLE</h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <span><span style="color: #000;"><b>5</b></span> of 6</span>
                    </div>
        </div><!--row end -->
        <div class="container contCenter">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What's most important in your new flooring?</h2>
                    <h2 class=" text-center">Choose as many as you like.</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/beauty.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Aesthetic/Beauty </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="aesthetic_beauty" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/color.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>color </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="color" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/stainResistant.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>stain resistant</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="stain_resistant" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/diyFriendly.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>diy friendly</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="diy_friendly" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/duarable.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>durability</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="durability" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/easyToClean.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>easy to clean </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="easy_to_clean" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/petFriendly.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>pet friendly</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="pet_friendly" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/ecoFriendly.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>eco-friendly</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="eco_friendly" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/budgetFriendly.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Budget friendly</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="budget_friendly" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/5/waterproof.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>waterproof</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="hypoallergenic" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>
        <div class="row text-center buttonWrap next">
                <div id = "alert_placeholder"></div>
                <a class="button">NEXT</a>
        </div><!--row end -->
        <div class="progressBar">
            <div class="bar">                
            </div>
        </div><!--row end -->
    </section>
    <section data-section="color" data-index-value="6" class="multiselect" role="form">
        <div class="row pages">
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <h4>FIND YOUR FLOORING STYLE</h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                        <span><span style="color: #000;"><b>6</b></span> of 6</span>
                    </div>
        </div><!--row end -->
        <div class="container contCenter typeOfCarpet hidden">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What color carpet are you looking for? Choose as many as you like.</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/beige.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Beiges</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="beiges" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/blues.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>blues</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="blues" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/browns.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>browns</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="browns" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/golds.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>golds</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="golds" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/gray.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>greys </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="greys" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/greens.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>greens</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="greens" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/oranges.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>oranges</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="oranges" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-color-carpet="reds">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/reds.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>reds</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="reds" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card" data-color-carpet="violets">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/violet.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>violets</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="violets" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>

        <div class="container contCenter typeOfWood hidden">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What color wood are you looking for? Choose as many as you like.</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Black</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="beiges" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/beige.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Beige</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="beige" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/browns.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Brown</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="brown" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/golds.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Gold</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="gold" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/greens.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Green </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="green" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/gray.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Grey</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="grey" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Natural</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="natural" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/oranges.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Orange</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="orange" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>White</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="white" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>

        <div class="container contCenter typeOfTile hidden">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">What color Tile or Stone are you looking for? Choose as many as you like.</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/beige.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Beige</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="beiges" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Black</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="black" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/blues.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Blue</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="blue" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/browns.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Brown</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="brown" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/greens.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Green </h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="green" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/gray.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Grey</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="grey" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/6/reds.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Red</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="red" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>Rust</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="rust" />
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>White</h4> 
                            </div>
                            <input type="checkbox" class="checkbox" value="white" />
                        </div>
                    </div>
                </div><!--row end -->
                 

            </div>
        </div>
        <div class="row text-center buttonWrap getProData">
                <div id = "alert_placeholder"></div>
                <a class="button">CHECK OUT MY FLOORING </a>
        </div><!--row end -->
        <div class="progressBar">
            <div class="bar">                
            </div>
        </div><!--row end -->
    </section>
     <section data-section="result" class="resultWrap">
        <div class="container contCenter result">
            <div class="gridWrapper">
                <div class="row title">
                    <h2 class=" text-center">Your Results</h2>
                </div><!--row end -->
                <div class="row  align-items-center prodGrid wrap flooringFinderajax">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>artisic affinity</h4> 
                                <h3>Twine</h3>
                                <p class="brand"><small><i>Karastan</i></small></p>
                                <!-- <a class="orderSamplebtn" href="#">Order sample</a> -->
                                <a class="prodLink" href="#">See info</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>artisic affinity</h4> 
                                <h3>Twine</h3>
                                <p class="brand"><small><i>Karastan</i></small></p>
                                <!-- <a class="orderSamplebtn" href="#">Order sample</a> -->
                                <a class="prodLink" href="#">See info</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>artisic affinity</h4> 
                                <h3>Twine</h3>
                                <p class="brand"><small><i>Karastan</i></small></p>
                                <!-- <a class="orderSamplebtn" href="#">Order sample</a> -->
                                <a class="prodLink" href="#">See info</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>artisic affinity</h4> 
                                <h3>Twine</h3>
                                <p class="brand"><small><i>Karastan</i></small></p>
                                <!-- <a class="orderSamplebtn" href="#">Order sample</a> -->
                                <a class="prodLink" href="#">See info</a>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/templates/images/1.jpg" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>artisic affinity</h4> 
                                <h3>Twine</h3>
                                <p class="brand"><small><i>Karastan</i></small></p>
                                <!-- <a class="orderSamplebtn" href="#">Order sample</a> -->
                                <a class="prodLink" href="#">See info</a>
                            </div>
                            
                        </div>
                    </div>
                <div class="text-center buttonWrap ">
                    <a class="button" href="javascript:void(0)" >view all results</a>
                    <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
                </div><!--row end -->

                </div><!--row end -->
                 
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>